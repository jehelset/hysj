if(NOT HYSJ_WITH_PYTHON)
  message(FATAL_ERROR "Hysj: python is required to build the documentation.")
endif()
hysj_add_documentation(
  TARGET hysj-documentation
  COMPONENT documentation
  NAME "hysj"
  DEPENDS hysj hysj-python 
  RUNTIME_COMPONENTS runtime python)
hysj_install_documentation(
  COMPONENT documentation)
