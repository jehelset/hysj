file(GLOB_RECURSE SOURCES CONFIGURE_DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/*.cpp)

set(TARGETS)
foreach(SOURCE ${SOURCES})
  cmake_path(RELATIVE_PATH SOURCE BASE_DIRECTORY ${CMAKE_CURRENT_LIST_DIR} OUTPUT_VARIABLE TMP)
  cmake_path(REMOVE_EXTENSION TMP)
  cmake_path(GET TMP PARENT_PATH PATH)
  string(REPLACE "/" "_" TARGET_SUFFIX ${TMP})
  set(TARGET hysj-benchmarks-${TARGET_SUFFIX})
  hysj_add_executable(TARGET ${TARGET} COMPONENT benchmarks ${SOURCE})
  set_target_properties(${TARGET} PROPERTIES
    RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/${PATH})
  target_link_libraries(${TARGET}
    PRIVATE hysj fmt::fmt-header-only benchmark::benchmark)
  list(APPEND TARGETS ${TARGET})
endforeach()

list(LENGTH TARGETS TARGET_COUNT)

if(${TARGET_COUNT} EQUAL 0)
  add_custom_target(hysj-benchmarks)
else()
  set(TMP ${TARGETS})
  list(POP_FRONT TMP FIRST_TARGET)
  set(TARGET_COMMANDS)
  list(APPEND TARGET_COMMANDS "$<TARGET_FILE:${FIRST_TARGET}>")
  foreach(TARGET ${TMP})
    list(APPEND TARGET_COMMANDS COMMAND "$<TARGET_FILE:${TARGET}>")
  endforeach()
  add_custom_target(hysj-benchmarks ${TARGET_COMMANDS})
  add_dependencies(hysj-benchmarks ${TARGETS})
endif()

