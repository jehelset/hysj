#include<chrono>
#include<functional>
#include<ranges>

#include<benchmark/benchmark.h>
#include<fmt/core.h>
#include<kumi/tuple.hpp>

#include<hysj/codes.hpp>
#include<hysj/devices/vulkan.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/functional/bind.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::devices::vulkan{

auto benchmark = [](benchmark::State& state, auto inputs) { 
  auto [devidx, max_local_size] = inputs;
  
  codes::code code;

  using namespace codes::factories;

  auto N = std::vector{64 * 4, 64 * 4, 64 * 8};

  auto E = ranges::vec(N, bind<>(lift((codes::icst32)), std::ref(code)));
  auto I = ranges::vec(E, bind<>(lift((codes::itr)), std::ref(code)));
  auto T = ranges::vec(N, [&](auto n){ return icst32(code, n - 1); });
  auto J = ranges::vec(std::views::zip(T, I), 
                       bind<>(lift((std::apply)), 
                              bind<>(sub, std::ref(code))));
  auto P = ranges::vec(std::views::zip(E, I),
                       bind<>(
                         lift((std::apply)),
                         [&](auto e, auto i){
                           return lt(code, i, div(code, e, code.builtins.lits.two32));
                         }));
  auto A = ranges::vec(std::views::zip(P, I, J),
                       bind<>(
                         lift((std::apply)),
                         [&](auto p, auto i, auto j){
                           return sel(code, p, {j,i});
                         }));
  auto B = ranges::vec(std::views::zip(P, I, J),
                       bind<>(
                         lift((std::apply)),
                         [&](auto p,auto i, auto j){
                           return sel(code, p, {i,j});
                         }));
    
  auto c2 = icst32(code, 2),
    c3 = icst32(code, 3),
    c9 = icst32(code, 9),
    c14 = icst32(code, 14),
    c23 = icst32(code, 23),
    cN = icst32(code, 16);

  auto g = 
    div(code, 
        bxor(code, {bnot(code, add(code, {div(code, A[1],c3),c14})),
            add(code, {div(code, B[2],c3),c14})}),
        add(code, {c2,div(code, A[0],c9)}));
  auto f = 
    mod(code, 
        add(code, {bxor(code, {c23,bnot(code, g)}),div(code, g,c14)}),
        cN);
 
  auto [t,b] = obs(code, f);

  auto dev = codes::dev(code);

  auto main = codes::on(code, dev, t);

  auto api = codes::api(code, {main});
  compile(code, api);

  auto env_vk = hysj::vkenv::make();
  {
    auto &dev_vk = env_vk.devs[devidx];
    dev_vk.spirv.max_local_size = max_local_size;
    dev_vk.sym = dev;
  }

  auto exe_vk = devs::load(env_vk, code, api);

  for (auto _:state){
    auto start = std::chrono::high_resolution_clock::now();
    devs::run(exe_vk, {main});
    auto end = std::chrono::high_resolution_clock::now();
    auto elapsed =
      std::chrono::duration_cast<std::chrono::duration<double>>(
        end - start);
    state.SetIterationTime(elapsed.count());
  }
    
};

}
#include<hysj/tools/epilogue.hpp>

int main(int argc, char** argv) {
  {
    auto env = hysj::vkenv::make();

    for(auto &&[devidx, dev]:std::views::enumerate(env.devs)){
      auto max_local_sizes = 
        std::views::take_while(
          std::views::transform(
            std::views::iota(hysj::integer{0}),
            [](auto i){
              return 1 << i;
            }),
          [&](auto i){
            return i <= dev.spirv.max_compute_workgroup_size[0];
          });
      for(auto max_local_size:max_local_sizes)
        benchmark::RegisterBenchmark(fmt::format("devices/vulkan/local_size(device = {}, max_local_size = {})", dev.name, max_local_size),
                                     hysj::vkdevs::benchmark, 
                                     kumi::make_tuple(devidx, max_local_size))
          ->UseManualTime()
          ->Iterations(64)
          ->DisplayAggregatesOnly(true);
    }
  }

  benchmark::Initialize(&argc, argv);
  benchmark::RunSpecifiedBenchmarks();
  benchmark::Shutdown();
}
