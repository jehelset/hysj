#pragma once

#include<hysj/codes/grammar.hpp>
#include<hysj/codes/code.hpp>
#include<hysj/tools/reflection.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::linear_algebra{

HYSJ_EXPORT
codes::taskfam
swap_rows(code &, codes::exprfam matrix, codes::exprfam row0, codes::exprfam row1);

HYSJ_EXPORT
codes::taskfam
find_pivot(code &, codes::exprfam matrix, codes::exprfam first_row, codes::exprfam col, codes::exprfam pivot);

HYSJ_EXPORT
codes::taskfam
gaussian_elimination_with_partial_pivoting(code &, codes::exprfam matrix, codes::exprfam pivots);

HYSJ_EXPORT
codes::taskfam
pivot_rows(code &, codes::exprfam matrix, codes::exprfam pivots);

HYSJ_EXPORT
codes::taskfam
upper_triangular_solve(code &, codes::exprfam U, codes::exprfam x);

HYSJ_EXPORT
codes::taskfam
lower_triangular_solve(code &, codes::exprfam L, codes::exprfam x, codes::exprfam b);

HYSJ_EXPORT
codes::taskfam
lower_upper_solve(code &, codes::exprfam LU, codes::exprfam x,codes::exprfam Pb);

HYSJ_EXPORT
codes::taskfam
linear_solve(code &, codes::exprfam A, codes::exprfam x,codes::exprfam b);

}
#include<hysj/tools/epilogue.hpp>
