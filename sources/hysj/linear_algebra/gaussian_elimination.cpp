#include<hysj/linear_algebra/gaussian_elimination.hpp>

#include<stdexcept>

#include<hysj/codes/code.hpp>
#include<hysj/codes/grammar.hpp>
#include<hysj/codes/factories.hpp>
#include<hysj/codes/compile.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::linear_algebra{

codes::taskfam
swap_rows(code &code, codes::exprfam matrix, codes::exprfam row0, codes::exprfam row1){

  using namespace codes::factories;

  const auto width = iwidth(code, matrix);

  const auto col = ivar(code, width);

  const auto size = [&] -> codes::exprfam {
    const auto R = orank(code, matrix);
    if(R < 2)
      return {codes::oone(code, col)};
    else{
      HYSJ_ASSERT(R == 2);
      return {odims(code, matrix)[1]};
    }
  }(); 

  const auto temporary = tmp_for(code, matrix);
  const auto zero = codes::ozero(code, col);

  const auto init_iterator = put(code, col, zero);
  const auto fini_iterator = if_(code, {lnot(code, lt(code, col, size))}, {code.builtins.brk});
  const auto next_iterator = put(code, col, inc(code, col));

  const auto element0 = rot(code, matrix, {row0, col});
  const auto element1 = rot(code, matrix, {row1, col});
  return {
    then(
      code,
      {
        init_iterator,
        loop(
          code,
          then(
            code,
            {
              fini_iterator,
              put(code, temporary, element0),
              put(code, element0, element1),
              put(code, element1, temporary),
              next_iterator
            }))
      })
  };
}

codes::taskfam
find_pivot(code &code, codes::exprfam matrix, codes::exprfam first_row, codes::exprfam col, codes::exprfam pivot){

  using namespace codes::factories;

  const auto N = orank(code, matrix);

  HYSJ_ASSERT(N < 3);

  const auto width = iwidth(code, matrix);
  const auto size = N != 0 ? codes::expr(odims(code, matrix)[0]) : codes::expr(codes::oone(code, matrix));

  const auto row = ivar(code, width);

  const auto init_iterator = put(code, row, inc(code, first_row));
  const auto fini_iterator = if_(code, {lnot(code, lt(code, row, size))}, {code.builtins.brk});
  const auto next_iterator = put(code, row, inc(code, row));

  const auto init_pivot = put(code, pivot, first_row);

  const auto element0 = rot(code, matrix, {row, col});
  const auto element1 = rot(code, matrix, {pivot, col});

  const auto next_pivot = sel(
    code,
    lt(code, abs(code, element1), abs(code, element0)),
    {pivot, row});
  const auto update_pivot = put(code, pivot, next_pivot);

  return {
    then(
      code,
      {
        init_pivot,
        init_iterator,
        loop(
          code,
          then(
            code,
            {
              fini_iterator,
              update_pivot,
              next_iterator
            }))
      })
  };
}

codes::taskfam
gaussian_elimination_with_partial_pivoting(code &code, codes::exprfam matrix, codes::exprfam pivots){
  using namespace codes::factories;
  compile(code, matrix);

  const auto N = orank(code, matrix);

  HYSJ_ASSERT(N < 3);
  HYSJ_ASSERT(orank(code, pivots) < 2);

  const auto size = N != 0 ? oexts(code, matrix)[0] : 1_i;

  HYSJ_ASSERT(N < 2 || size == oexts(code, matrix)[1]);
  HYSJ_ASSERT(orank(code, pivots) == 0 || size == oexts(code, pivots)[0]);
  
  const auto width = iwidth(code, matrix);
  const auto dim = N != 0 ? codes::expr(odims(code, matrix)[0]) : codes::expr(codes::oone(code, matrix));

  std::array<codes::exprfam, 3> iterator;
  for(auto &i:iterator)
    i = codes::expr(ivar(code, width));
  std::array<codes::taskfam, 3> next_iterator;
  for(auto &&[n, i]:std::views::zip(next_iterator, iterator))
    n = codes::task(put(code, i, inc(code, i)));
  std::array<codes::taskfam, 3> fini_iterator;
  for(auto &&[n, i]:std::views::zip(fini_iterator, iterator))
    n = codes::task(if_(code, {lnot(code, lt(code, i, dim))}, {code.builtins.brk}));

  const auto zero = codes::ozero(code, iterator[0]);
  const auto init_iterator0 = put(code, iterator[0], zero);
  const auto pivot0 = rot(code, pivots, iterator[0]);

  const auto update_trailing_matrix = [&]{
    const auto init_iterator = inc(code, iterator[0]);
    const auto init_iterator1 = put(code, iterator[1], init_iterator);
    const auto init_iterator2 = put(code, iterator[2], init_iterator);

    const auto element0 = rot(code, matrix, {iterator[0], iterator[0]});
    const auto element1 = rot(code, matrix, {iterator[1], iterator[0]});

    const auto temporary = tmp_for(code, matrix);
    const auto l = div(code, element1, element0);

    const auto element2 = rot(code, matrix, {iterator[0], iterator[2]});
    const auto element3 = rot(code, matrix, {iterator[1], iterator[2]});
    const auto u = sub(code, element3, mul(code, {temporary, element2}));
    
    return then(
      code,
      {
        init_iterator1,
        loop(
          code,
          then(
            code,
            {
              fini_iterator[1],
              put(code, temporary, l),
              put(code, element1, temporary),
              init_iterator2,
              loop(
                code,
                then(
                  code,
                  {
                    fini_iterator[2],
                    put(code, element3, u),
                    next_iterator[2]
                  })),
              next_iterator[1]
            }))
      });
  }();
  
  return {
    then(
      code,
      {
        init_iterator0,
        loop(
          code,
          then(
            code,
            {
              fini_iterator[0],
              linear_algebra::find_pivot(code, matrix, iterator[0], iterator[0], {pivot0}),
              linear_algebra::swap_rows(code, matrix, iterator[0], {pivot0}),
              update_trailing_matrix,
              next_iterator[0]
            }))
      })
  };
}

codes::taskfam
pivot_rows(code &code, codes::exprfam matrix, codes::exprfam pivots){


  const auto N = orank(code, matrix);
  HYSJ_ASSERT(N < 3);
  HYSJ_ASSERT(orank(code, pivots) < 2);

  using namespace codes::factories;

  const auto width = iwidth(code, matrix);
  const auto size = N != 0 ? codes::expr(odims(code, matrix)[0]) : codes::expr(codes::oone(code, matrix));

  const auto row = ivar(code, width);
  const auto zero = codes::ozero(code, row);

  const auto pivot = rot(code, pivots, {row});

  const auto init_iterator = put(code, row, zero);
  const auto fini_iterator = if_(code, {lnot(code, lt(code, row, size))}, {code.builtins.brk});
  const auto next_iterator = put(code, row, inc(code, row));

  //FIXME: for_loop - jeh
  return {
    then(
      code,
      {
        init_iterator,
        loop(
          code,
          then(
            code,
            {
              fini_iterator,
              swap_rows(code, matrix, {row}, {pivot}),
              next_iterator
            }))
      })
  };

}


codes::taskfam
upper_triangular_solve(code &code, codes::exprfam matrix, codes::exprfam vector){
  
  const auto N = orank(code, matrix);
  HYSJ_ASSERT(N < 3);
  HYSJ_ASSERT(orank(code, vector) < 2);

  using namespace codes::factories;

  const auto width = iwidth(code, matrix);
  const auto size = N != 0 ? codes::expr(odims(code, matrix)[0]) : codes::expr(codes::oone(code, matrix));

  const auto row = ivar(code, width);
  const auto col = ivar(code, width);
  const auto zero = codes::ozero(code, row);

  const auto init_row = put(code, row, dec(code, size));
  const auto fini_row = if_(code, {eq(code, row, zero)}, {code.builtins.brk});
  const auto next_row = put(code, row, dec(code, row));

  const auto vector0 = rot(code, vector, {row});
  const auto vector1 = rot(code, vector, {col});
  const auto matrix0 = rot(code, matrix, {row, row});
  const auto matrix1 = rot(code, matrix, {row, col});

  const auto next_elem = put(code, vector0, sub(code, vector0, mul(code, {matrix1, vector1})));
  const auto fini_elem = put(code, vector0, div(code, vector0, matrix0));

  const auto init_col = put(code, col, inc(code, row));
  const auto fini_col = if_(code, {eq(code, col, size)}, {code.builtins.brk});
  const auto next_col = put(code, col, inc(code, col));

  //FIXME: for_loop - jeh
  //FIXME:
  //  j = itr(code, col)
  //  con(add(rot(A, row, j)), j)
  return {
    then(
      code,
      {
        init_row,
        loop(
          code,
          then(
            code,
            {
              init_col,
              loop(
                code,
                then(
                  code,
                  {
                    fini_col,
                    next_elem,
                    next_col
                  })),
              fini_elem,
              fini_row,
              next_row
            }))
      })
  };
  
}

codes::taskfam
lower_triangular_solve(code &code, codes::exprfam matrix, codes::exprfam vector, codes::exprfam rhs){
  
  const auto N = orank(code, matrix);
  HYSJ_ASSERT(N < 3);
  HYSJ_ASSERT(orank(code, vector) < 2);
  HYSJ_ASSERT(orank(code, rhs) < 2);

  using namespace codes::factories;

  const auto width = iwidth(code, matrix);
  const auto size = N == 2 ? codes::expr(odims(code, matrix)[0]) : codes::expr(codes::oone(code, matrix));

  const auto row = ivar(code, width);
  const auto col = ivar(code, width);
  const auto zero = codes::ozero(code, row);

  const auto init_row = put(code, row, zero);
  const auto fini_row = if_(code, {eq(code, row, size)}, {code.builtins.brk});
  const auto next_row = put(code, row, inc(code, row));

  const auto vector0 = rot(code, vector, {row});
  const auto rhs0 = rot(code, rhs, {row});
  const auto vector1 = rot(code, vector, {col});
  const auto matrix1 = rot(code, matrix, {row, col});

  const auto init_elem = put(code, vector0, rhs0);
  const auto next_elem = put(code, vector0, sub(code, vector0, mul(code, {matrix1, vector1})));

  const auto init_col = put(code, col, zero);
  const auto fini_col = if_(code, {eq(code, col, row)}, {code.builtins.brk});
  const auto next_col = put(code, col, inc(code, col));

  return {
    then(
      code,
      {
        init_row,
        loop(
          code,
          then(
            code,
            {
              fini_row,
              init_elem,
              init_col,
              loop(
                code,
                then(
                  code,
                  {
                    fini_col,
                    next_elem,
                    next_col
                  })),
              next_row
            }))
      })
  };
  
}

codes::taskfam
lower_upper_solve(code &code, codes::exprfam LU, codes::exprfam x, codes::exprfam Pb){
  using namespace codes::factories;
  return {
    then(code,
         {
           lower_triangular_solve(code, LU, x, Pb), //Ly = Pb 
           upper_triangular_solve(code, LU, x),  //Ux = y
         })
  };
}

codes::taskfam
linear_solve(code &code, codes::exprfam A, codes::exprfam x, codes::exprfam b){

  using namespace codes::factories;

  compile(code, std::array{A, x, b});

  const auto N = orank(code, A);
  HYSJ_ASSERT(N < 3);
  HYSJ_ASSERT(orank(code, x) < 2);
  HYSJ_ASSERT(orank(code, b) < 2);

  const auto size = N != 0 ? oexts(code, A)[0] : 1_i;

  HYSJ_ASSERT(N < 2 || size == oexts(code, A)[1]);
  HYSJ_ASSERT(orank(code, x) == 0 || size == oexts(code, x)[0]);
  HYSJ_ASSERT(orank(code, b) == 0 || size == oexts(code, b)[0]);
  
  const auto width = iwidth(code, A);
  const auto p = [&]{
    if(N == 2)
      return ivar(code, width, {codes::expr(oitrs(code, A)[0])});
    return ivar(code, width);
  }();

  compile(code, p);

  return {
    then(code,
         {
           gaussian_elimination_with_partial_pivoting(code, A, {p}),
           pivot_rows(code, b, {p}),
           lower_triangular_solve(code, A, x, b), //Ly = Pb 
           upper_triangular_solve(code, A, x),  //Ux = y
         })
  };

}

} //hysj::linear_algebra
#include<hysj/tools/epilogue.hpp>
