#pragma once

#include<algorithm>
#include<ranges>
#include<utility>

#include<kumi/tuple.hpp>

#include<hysj/codes/code.hpp>
#include<hysj/codes/cells.hpp>
#include<hysj/codes/grammar.hpp>
#include<hysj/tools/functional/unwrap.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/tag_invoke.hpp>
#include<hysj/tools/type_traits.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::devices{

namespace concepts {

  template<typename D>
  concept exe = requires(D &&d,codes::taskfam t){
    { auto(d.code) } -> std::same_as<code>;
    d.run(t);
  };

  template<typename E>
  concept env = requires(E &&e,cells &m,code c,codes::apisym a){
    { e.load(m, std::move(c), a) } -> exe;
  };

}


template<typename E>
using exe_t = decltype(std::declval<E>().load(std::declval<cells &>(), std::declval<code>(), std::declval<codes::apisym>()));

auto alloc(concepts::env auto &&e, codes::code &c, codes::apisym a){
  compile(c, a);
  auto l = [&] -> std::optional<std::size_t> {
    if constexpr(requires{e.min_alignment();})
      return e.min_alignment();
    else
      return {};
  }();
  return alloc(c, a, l);
}
auto exe(concepts::env auto &&e, codes::code c, codes::apisym a){
  compile(c, a);
  auto l = [&] -> std::optional<std::size_t> {
    if constexpr(requires{e.min_alignment();})
      return e.min_alignment();
    else
      return {};
  }();
  auto m = alloc(c, a, l);
  auto x = e.load(m, std::move(c), a);
  return kumi::make_tuple(std::move(m), std::move(x));
}

} //hysj::devices
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

namespace devs = devices;

} //hysj
#include<hysj/tools/epilogue.hpp>
