#include<hysj/devices/vulkan.hpp>

#include<algorithm>
#include<array>
#include<ranges>
#include<iterator>
#include<span>
#include<utility>
#include<vector>

#include<fmt/core.h>

#include<vulkan/vulkan.h>
#include<vulkan/vulkan.hpp>
#include<vulkan/vulkan_enums.hpp>
#include<vulkan/vulkan_raii.hpp>

#include<hysj/binaries/spirv.hpp>
#include<hysj/codes.hpp>
#include<hysj/tools/coroutines/trees.hpp>
#include<hysj/tools/functional/apply.hpp>
#include<hysj/tools/functional/bind.hpp>
#include<hysj/tools/functional/optional.hpp>
#include<hysj/tools/graphs/algorithms/deeporders.hpp>
#include<hysj/tools/graphs/degree.hpp>
#include<hysj/tools/graphs/elements.hpp>
#include<hysj/tools/graphs/properties.hpp>
#include<hysj/tools/preprocessor.hpp>
#include<hysj/tools/ranges/cat.hpp>
#include<hysj/tools/ranges/fold.hpp>
#include<hysj/tools/ranges/keep.hpp>
#include<hysj/tools/types.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::devices::vulkan{

namespace _{
  struct bin{
    static constexpr vk::ShaderStageFlagBits shader_stage_flag_bit = vk::ShaderStageFlagBits::eCompute;

    vk::raii::DescriptorSetLayout descriptor_set_layout;
    vk::raii::DescriptorSet descriptor_set;
    vk::raii::ShaderModule shader_module;
    vk::raii::PipelineLayout pipeline_layout;
    vk::raii::Pipeline pipeline;
  };
}

struct bin::impl_ : _::bin{};

bin::bin() = default;
bin::bin(bin &&) = default;
bin &bin::operator=(bin &&) = default;
bin::~bin() = default;


namespace _{

  struct dev{
    vk::raii::PhysicalDevice physical_device;
  };

}

struct dev::impl_ : _::dev{};

dev::dev() = default;
dev::dev(dev &&) = default;
dev &dev::operator=(dev &&) = default;
dev::~dev() = default;

namespace _{
  struct slab{
    vk::raii::Buffer buffer;
    vk::raii::DeviceMemory memory;
  };
  struct buf : slab{
    codes::exprfam sym;
  };
  struct ctrl{
    static constexpr vk::DescriptorType buf_descriptor_type = vk::DescriptorType::eStorageBuffer;
    std::uint32_t queue_family_index, buf_type_index, bus_type_index;
    vk::raii::Device device;
    vk::raii::Queue queue;
    vk::raii::PipelineCache pipeline_cache;
    vk::raii::CommandPool command_pool;
    vk::raii::DescriptorPool descriptor_pool;

    std::array<std::optional<_::slab>, codes::typetab.size()> buses;
    std::vector<_::buf> buffers;

    auto &bus(this auto &c, codes::type t){
      return c.buses.at(codes::typeindex(t).value());
    }
    auto &buf(this auto &c, codes::exprfam e){
      auto it = std::ranges::find(c.buffers, e, &_::buf::sym);
      if(it == std::ranges::end(c.buffers))
        throw std::out_of_range(fmt::format("{}", e));
      return *it;
    }
  };
}

struct ctrl::impl_ : _::ctrl{};

ctrl::ctrl() = default;
ctrl::ctrl(ctrl &&) = default;
ctrl &ctrl::operator=(ctrl &&) = default;
ctrl::~ctrl() = default;

namespace _{
 
  struct cmd{
    vk::raii::CommandBuffer command_buffer;
    vk::raii::Event event;
  };
  struct submit{
    std::vector<cmd> cmds;
    vk::raii::CommandBuffer final_command_buffer;
    std::optional<vk::raii::Semaphore> signal_semaphore;
    std::vector<vk::SemaphoreSubmitInfo> wait_semaphore_info;
    std::vector<vk::CommandBufferSubmitInfo> command_buffer_info;
    vk::SemaphoreSubmitInfo signal_semaphore_info;
    vk::SubmitInfo2 submit_info;
  };

}

struct submit::impl_ : _::submit{};

submit::submit() = default;
submit::submit(submit &&) = default;
submit &submit::operator=(submit &&) = default;
submit::~submit() = default;

main::main() = default;
main::main(main &&) = default;
main &main::operator=(main &&) = default;
main::~main() = default;

exe::exe() = default;
exe::exe(exe &&) = default;
exe &exe::operator=(exe &&) = default;
exe::~exe() = default;

namespace _{
  struct env{
    vk::raii::Context context;
    vk::raii::Instance instance;
  };
}

struct env::impl_ : _::env{};

env::env() = default;
env::env(env &&) = default;
env &env::operator=(env &&) = default;
env::~env() = default;

namespace _{

  auto make_buffer(ctrl &ctrl_vk,std::uint32_t size_vk,
                   vk::BufferUsageFlags usage_vk){
    HYSJ_ASSERT(size_vk > 0);
    vk::BufferCreateInfo create_info{
      vk::BufferCreateFlags(),
      size_vk,
      usage_vk,
      vk::SharingMode::eExclusive,
      1,
      &ctrl_vk.queue_family_index
    };
    return ctrl_vk.device.createBuffer(create_info);
  }
  auto make_buffer(ctrl &ctrl_vk, auto size_vk){
    return make_buffer(ctrl_vk, size_vk, 
                  vk::BufferUsageFlagBits::eStorageBuffer
                  | vk::BufferUsageFlagBits::eShaderDeviceAddress
                  | vk::BufferUsageFlagBits::eTransferSrc
                  | vk::BufferUsageFlagBits::eTransferDst);
  }
  auto make_memory(ctrl &ctrl_vk,const vk::MemoryAllocateInfo &vk_info){
    return ctrl_vk.device.allocateMemory(vk_info);
  }
  auto make_memory(ctrl &ctrl_vk,vk::raii::Buffer &buffer_vk){
    vk::MemoryAllocateFlagsInfo memory_flags_vk{
      vk::MemoryAllocateFlagBits::eDeviceAddress
    };
    auto memory_reqs_vk = buffer_vk.getMemoryRequirements();
    return make_memory(ctrl_vk,vk::MemoryAllocateInfo(memory_reqs_vk.size,ctrl_vk.bus_type_index,&memory_flags_vk));
  }

  auto make_command_buffer(ctrl &ctrl_vk){
    vk::CommandBufferAllocateInfo info_vk(*ctrl_vk.command_pool, vk::CommandBufferLevel::ePrimary,1);
    return std::move(ctrl_vk.device.allocateCommandBuffers(info_vk).front());
  }

}

void exe::run(codes::taskfam m){
  auto &main_vk = this->main(m);
  //FIXME: prep per dev submit & wait on all...
  for(auto &submit_vk:main_vk.submits){
    submit_vk.impl->signal_semaphore = [&]{
      vk::SemaphoreTypeCreateInfo type_info_vk{
        vk::SemaphoreType::eTimeline,
        0
      };
      vk::SemaphoreCreateInfo info_vk{
        vk::SemaphoreCreateFlags{},
        &type_info_vk
      };
      auto &ctrl_vk = this->ctrl(submit_vk.dev);
      return ctrl_vk.impl->device.createSemaphore(info_vk);
    }();
    submit_vk.impl->signal_semaphore_info = vk::SemaphoreSubmitInfo(submit_vk.impl->signal_semaphore.value(), 1);
  }
  for(auto submit_vert:graphs::vertex_ids(main_vk.schedule.graph))
    std::ranges::copy(
      views::map(
        graphs::viadjacent(main_vk.schedule.graph, submit_vert),
        [&](auto await_vert){
          auto &await_vk = main_vk.submits.at(await_vert);
          return vk::SemaphoreSubmitInfo(await_vk.impl->signal_semaphore.value(), 1);
        }),
      main_vk.submits.at(submit_vert).impl->wait_semaphore_info.data());


  auto job_done = graphs::vprop(main_vk.schedule.graph, false);
  for(auto root_vert:graphs::vertex_ids(main_vk.schedule.graph)){
    if(graphs::get(job_done, root_vert))
      continue;
    for(auto job_vert:graphs::ipostorder(std::ref(main_vk.schedule.graph), root_vert)){
      if(graphs::get(job_done, job_vert))
        continue;
      graphs::put(job_done, job_vert, true);
      auto &submit_vk = main_vk.submits.at(job_vert);
      auto &ctrl_vk = this->ctrl(submit_vk.dev);
      ctrl_vk.impl->queue.submit2(submit_vk.impl->submit_info, vk::Fence(nullptr));
    }
  }

  for(auto &submit_vk:main_vk.submits){
    auto &ctrl_vk = this->ctrl(submit_vk.dev);
    std::uint64_t wait_value_vk = 1;
    auto wait_semaphore_vk = *submit_vk.impl->signal_semaphore.value();
    vk::SemaphoreWaitInfo wait_vk(vk::SemaphoreWaitFlags{},
                                  wait_semaphore_vk,
                                  wait_value_vk);
    if(ctrl_vk.impl->device.waitSemaphores(wait_vk, std::numeric_limits<std::uint64_t>::max())
       != vk::Result::eSuccess)
      throw std::runtime_error("fail");
  }
}

namespace _{
  auto make_env(){
    static constexpr vk::ApplicationInfo app_info{
      "hysj.devices.vulkan",
      1,
      nullptr,
      0,
      VK_API_VERSION_1_4
    };
    static constexpr std::array<const char *, 0> layers{
      //FIXME: can't load - jeh
      // "VK_LAYER_KHRONOS_validation"
    };
    env env_vk{
      .context{},
      .instance{env_vk.context, vk::InstanceCreateInfo({}, &app_info, layers, {})}
    };
    return env_vk;
  }
  spvenv make_spvenv(const vk::raii::PhysicalDevice &physical_device){
    VkPhysicalDeviceVulkan13Properties p13{
      .sType = VkStructureType::VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_3_PROPERTIES
    };
    VkPhysicalDeviceProperties2 p{
      .sType = VkStructureType::VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2,
      .pNext = &p13,
    };
    vkGetPhysicalDeviceProperties2(*physical_device,&p);
    return {
      .max_compute_workgroup_count{
        {p.properties.limits.maxComputeWorkGroupCount[0],
         p.properties.limits.maxComputeWorkGroupCount[1],
         p.properties.limits.maxComputeWorkGroupCount[2]}},
      .max_compute_workgroup_size{
        {p.properties.limits.maxComputeWorkGroupSize[0],
         p.properties.limits.maxComputeWorkGroupSize[1],
         p.properties.limits.maxComputeWorkGroupSize[2]}},
      .min_subgroup_size = p13.minSubgroupSize,
      .max_subgroup_size = p13.maxSubgroupSize,
      .max_compute_workgroup_shared_memory_size = p.properties.limits.maxComputeSharedMemorySize,
      .min_storage_buffer_offset_alignment = p.properties.limits.minStorageBufferOffsetAlignment,
      .max_storage_buffer_range = p.properties.limits.maxStorageBufferRange,
      .min_memory_map_alignment = p.properties.limits.minMemoryMapAlignment,
      .max_buffer_size = p13.maxBufferSize
    };
  }
}

std::optional<std::size_t> env::min_alignment()const{
  std::optional<std::size_t> a{};
  for(auto &dev:devs){
    if(!dev.sym)
      continue;
    VkPhysicalDeviceExternalMemoryHostPropertiesEXT pmem{
      .sType = VkStructureType::VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_MEMORY_HOST_PROPERTIES_EXT
    };
    VkPhysicalDeviceProperties2 p{
      .sType = VkStructureType::VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2,
      .pNext = &pmem,
    };
    vkGetPhysicalDeviceProperties2(*dev.impl->physical_device,&p);
    a = std::max(a, just(static_cast<std::size_t>(pmem.minImportedHostPointerAlignment)));
  }
  return a;
}
env env::make(){
  env env_vk{};
  env_vk.impl = std::make_unique<impl_>(_::make_env());
  env_vk.devs = ranges::vec(
    env_vk.impl->instance.enumeratePhysicalDevices(),
    [&](vk::raii::PhysicalDevice &d){
      vulkan::dev dev_vk{};

      auto p = d.getProperties();
      dev_vk.vendor_id = p.vendorID;
      dev_vk.device_id = p.deviceID;
      dev_vk.type = [&]{
        switch(p.deviceType){
        case vk::PhysicalDeviceType::eIntegratedGpu: return devtype::igpu;
        case vk::PhysicalDeviceType::eDiscreteGpu: return devtype::dgpu;
        case vk::PhysicalDeviceType::eVirtualGpu: return devtype::vgpu;
        case vk::PhysicalDeviceType::eCpu: return devtype::cpu;
        default: return devtype::other;
        }
      }();
      dev_vk.name = p.deviceName.data();

      dev_vk.impl = std::make_unique<dev::impl_>(_::dev(std::move(d)));
      dev_vk.spirv = _::make_spvenv(dev_vk.impl->physical_device);
      return dev_vk;
    });
  return env_vk;
}

namespace _{

  auto make_device_types(const vk::raii::PhysicalDevice &d){
    const auto &p = d.getProperties();

    vk::PhysicalDeviceVulkan11Features pdv11f{};
    vk::PhysicalDeviceVulkan12Features pdv12f{};
    pdv12f.pNext = &pdv11f;
    vk::PhysicalDeviceVulkan13Features pdv13f{};
    pdv13f.pNext = &pdv12f;

    vk::PhysicalDeviceFeatures2 pdf2{
      {},
      &pdv13f
    };
    auto &pdf = pdf2.features;

    (*d).getFeatures2(&pdf2);

    std::vector<codes::type> types;

    auto add_types = [&](auto &&t){
      std::ranges::copy(t, std::back_inserter(types));
    };
    auto add_integer_types = [&](natural w){
      add_types(
        views::map(
          views::keep(enumerator_values<codes::set>, [](auto s){ return s != codes::set::reals; }),
          [&](auto s){ return codes::type{s, w}; }));
    };
    auto add_floating_point_types = [&](natural w){
      add_types(
        views::map(
          views::keep(enumerator_values<codes::set>, [](auto s){ return s == codes::set::reals; }),
          [&](auto s){ return codes::type{s, w}; }));
    };

    add_integer_types(32);
    add_floating_point_types(32);
    if(pdv12f.shaderInt8 && pdv12f.storageBuffer8BitAccess)
      add_integer_types(8);
    if(pdv12f.shaderFloat16 && pdv11f.storageBuffer16BitAccess)
      add_floating_point_types(16);
    if(pdf.shaderInt16)
      add_integer_types(16);
    if(pdf.shaderInt64)
      add_integer_types(64);
    if(pdf.shaderFloat64)
      add_floating_point_types(64);
    std::ranges::sort(types);

    return types;

  }
  vk::raii::Device make_device(const vk::raii::PhysicalDevice &physical_device, std::uint32_t queue_family_index){
    float priority = 1.0;
    vk::DeviceQueueCreateInfo dqci({}, queue_family_index, 1, &priority);

    vk::PhysicalDeviceFeatures pdf{};

    vk::PhysicalDeviceVulkan11Features pdv11f{};

    vk::PhysicalDeviceVulkan12Features pdv12f{};
    pdv12f.pNext = &pdv11f;
    pdv12f.vulkanMemoryModel = 1;
    pdv12f.bufferDeviceAddress = 1;
    pdv12f.timelineSemaphore = 1;

    vk::PhysicalDeviceVulkan13Features pdv13f{};
    pdv13f.pNext = &pdv12f;
    pdv13f.synchronization2 = 1;
    pdv13f.subgroupSizeControl = 1;

    auto types = _::make_device_types(physical_device);

    auto needs_integer_types = [&](natural w){
      return std::ranges::any_of(
        views::map(
          views::keep(enumerator_values<codes::set>, &codes::is_integral),
          [&](auto s){ return codes::type{s, w}; }),
        bind<>(ranges::contains, std::views::all(types)));
    };
    auto needs_floating_point_types = [&](natural w){
      return ranges::contains(types, codes::type{set::reals, w});
    };

    pdv12f.storageBuffer8BitAccess = pdv12f.shaderInt8 = needs_integer_types(8);
    pdf.shaderInt16 = needs_integer_types(16);
    pdv12f.shaderFloat16 = needs_floating_point_types(16);
    pdv11f.storageBuffer16BitAccess = pdf.shaderInt16 | pdv12f.shaderFloat16;
    pdf.shaderInt64 = needs_integer_types(64);
    pdf.shaderFloat64 = needs_floating_point_types(64);

    static constexpr std::array extensions{
      VK_EXT_EXTERNAL_MEMORY_HOST_EXTENSION_NAME
    };
    vk::DeviceCreateInfo dci({}, 1, &dqci, 0, nullptr, 1, extensions.data(), &pdf, &pdv13f);

    return physical_device.createDevice(dci);
  }
  std::uint32_t find_queue_family_index(const vk::raii::PhysicalDevice &D){
    auto properties = D.getQueueFamilyProperties();
    auto indices = views::indices(properties);
    const vk::QueueFlags queue_flags = vk::QueueFlagBits::eCompute | vk::QueueFlagBits::eTransfer;
    auto it = std::ranges::find_if(
      indices,
      [&](auto index){
        return (properties[index].queueFlags & queue_flags) != vk::QueueFlags{};
      });
    HYSJ_ASSERT(it != std::ranges::end(indices));
    return static_cast<std::uint32_t>(*it);
  }
  std::uint32_t find_memory_type_index(const vk::raii::PhysicalDevice &D,
                                       vk::MemoryPropertyFlags f){
    auto properties = D.getMemoryProperties();
    auto type_indices = views::indices(properties.memoryTypeCount);
    auto it = std::ranges::find_if(type_indices, [&](auto type_index) -> bool {
      return (f == vk::MemoryPropertyFlags{}) ||
        ((properties.memoryTypes[type_index].propertyFlags & f) == f);
    });
    HYSJ_ASSERT(it != std::ranges::end(type_indices));
    return *it;
  }

}

namespace _{
  void load_impl(cells &mem,exe &exe_vk,codes::apisym api){
    auto &code = exe_vk.code;
    {
      auto pre0 = [&](id o){
        return true;
      };
      auto pre1 = [&](id o,id r,id a){
        if(codes::reltag_of(r) != codes::reltag::lnk)
          return false;
        return pre0(a);
      };
      auto post = [&](auto... x){
        auto m = codes::taskcast(pick<-1>(x...));
        if(!m)
          return none{};

        if(ranges::contains_if(exe_vk.mains, [&](const auto &m_vk){ return m_vk.sym == *m; }))
          return none{};

        auto &main_vk = exe_vk.mains.emplace_back();
        main_vk.sym = *m;
        main_vk.schedule = codes::sched::make(code, main_vk.sym);
        main_vk.submits = ranges::vec(
          graphs::vertex_ids(main_vk.schedule.graph),
          [&](auto submit_vert){
            auto [dev, job] = with_famobj(
              [&](auto submit){
                if constexpr(submit.tag() == codes::optag::on){
                  auto [dev, job] = args(code, submit);
                  return just(kumi::make_tuple(dev, job));
                }
                if constexpr(submit.tag() == codes::optag::to){
                  auto [source, dest, buffer] = args(code, submit);
                  if(source == code.builtins.host && dest != code.builtins.host)
                    return just(kumi::make_tuple(dest, codes::task(submit)));
                  else if(dest == code.builtins.host)
                    return just(kumi::make_tuple(source, codes::task(submit)));
                }
                return no<kumi::tuple<codes::devsym, codes::taskfam>>;
              },
              main_vk.schedule.graph[submit_vert]).value();

            vulkan::submit submit_vk{};
            submit_vk.sym = job;
            submit_vk.dev = dev;
            submit_vk.schedule = codes::sched::make(code, job);

            auto &ctrl_vk = exe_vk.ctrl(submit_vk.dev);
            auto &dev_vk = *ctrl_vk.dev;

            for(auto task_vert:graphs::vertex_ids(submit_vk.schedule.graph)){
              auto task = submit_vk.schedule.graph[task_vert];
              if(codes::tocast(task.id()) //FIXME: task.is<codes::optag::to>() - jeh
                 || ranges::contains_if(
                   ctrl_vk.bins,
                   [&](const auto &bin_vk){
                     return bin_vk.spirv.kernel.decl == task;
                   }))
                continue;
              auto &bin_vk = ctrl_vk.bins.emplace_back();
              bin_vk.spirv = spirv::ccopt(dev_vk.spirv, code, task);
              err(bin_vk.spirv);
            }
            return submit_vk;
          });
        return none{};
      };
      grammars::walk<none>(
        code,
        overload(pre0, pre1),
        post,
        api);
    }

    const auto accesses = codes::accesses::make(code, api);

    for(auto &ctrl_vk:exe_vk.ctrls){
      auto &dev_vk = *ctrl_vk.dev;

      ctrl_vk.impl = [&]{
        _::ctrl ctrl_vk_impl{
          .queue_family_index = _::find_queue_family_index(dev_vk.impl->physical_device),
          .buf_type_index = _::find_memory_type_index(dev_vk.impl->physical_device, {}),
          .bus_type_index = _::find_memory_type_index(
            dev_vk.impl->physical_device,
            vk::MemoryPropertyFlagBits::eHostVisible
            | vk::MemoryPropertyFlagBits::eHostCoherent
            | vk::MemoryPropertyFlagBits::eHostCached),
          .device = _::make_device(dev_vk.impl->physical_device, ctrl_vk_impl.queue_family_index),
          .queue = ctrl_vk_impl.device.getQueue(ctrl_vk_impl.queue_family_index, 0),
          .pipeline_cache = ctrl_vk_impl.device.createPipelineCache({}),
          .command_pool = [&]{
            vk::CommandPoolCreateInfo info_vk({}, ctrl_vk_impl.queue_family_index);
            return ctrl_vk_impl.device.createCommandPool(info_vk);
          }(),
          .descriptor_pool = [&]{
            const auto buf_count = 
              ranges::sum(
                views::map(
                  ctrl_vk.bins,
                  [&](const auto &bin_vk){
                    return bin_vk.spirv.mem.buffers.size();
                  }));
            vk::DescriptorPoolSize dps{_::ctrl::buf_descriptor_type,std::max<std::uint32_t>(
                static_cast<std::uint32_t>(buf_count),
                1)};
            vk::DescriptorPoolCreateInfo dpci{vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet,
              std::max<std::uint32_t>(static_cast<std::uint32_t>(ctrl_vk.bins.size()), 1),
              1, &dps};
            return ctrl_vk_impl.device.createDescriptorPool(dpci);
          }(),
          .buses = [&]{
            auto make_bus = [&](auto type){
              auto buffer = mem.bytes(type);
              if(buffer.empty())
                return std::optional<_::slab>{};
              _::slab bus_vk{
                .buffer = [&]{
                  vk::ExternalMemoryBufferCreateInfo external_info_vk{
                    vk::ExternalMemoryHandleTypeFlagBits::eHostAllocationEXT
                  };
                  vk::BufferCreateInfo info_vk{
                    vk::BufferCreateFlags{},
                    buffer.size(),
                    vk::BufferUsageFlagBits::eTransferSrc
                    | vk::BufferUsageFlagBits::eTransferDst,
                    vk::SharingMode::eExclusive,
                    1,
                    &ctrl_vk_impl.queue_family_index,
                    &external_info_vk
                  };
                  return ctrl_vk_impl.device.createBuffer(info_vk);
                }(),
                .memory = [&]{
                  auto p_vk = ctrl_vk_impl.device.getMemoryHostPointerPropertiesEXT(
                    vk::ExternalMemoryHandleTypeFlagBits::eHostAllocationEXT,
                    (void *)buffer.data());
                  if(!((p_vk.memoryTypeBits >> ctrl_vk_impl.bus_type_index) & 0b1))
                    throw std::runtime_error("unsupported memory");
                  vk::MemoryAllocateFlagsInfo memory_flags_vk{
                    vk::MemoryAllocateFlagBits::eDeviceAddress
                  };
                  vk::ImportMemoryHostPointerInfoEXT import_memory_info_vk{
                    vk::ExternalMemoryHandleTypeFlagBits::eHostAllocationEXT,
                    (void *)buffer.data()
                  };
                  vk::MemoryAllocateInfo memory_info_vk(
                    buffer.size(),
                    ctrl_vk_impl.bus_type_index,
                    (void *)&import_memory_info_vk);
                  return _::make_memory(ctrl_vk_impl, memory_info_vk);
                }()
              };
              return just(std::move(bus_vk));
            };
            return apply(
              [&](auto... t){
                return std::array<std::optional<_::slab>, sizeof...(t)>{make_bus(t)...};
              },
              codes::typetab);
          }(),
          .buffers{}
        };
        for(auto buf:accesses.buffers(*ctrl_vk.dev->sym)){
          if(ranges::contains_if(ctrl_vk_impl.buffers,
                                 on<0>(bind<>(std::ranges::equal_to{}, buf), &_::buf::sym)))
            continue;
          const auto size_spv = spirv::obsize(code, buf);
          HYSJ_ASSERT(size_spv < dev_vk.spirv.max_buffer_size);
          auto &buf_vk = ctrl_vk_impl.buffers.emplace_back(
            [&]{
              _::buf buf_vk{
                {
                  _::make_buffer(ctrl_vk_impl, size_spv),
                  _::make_memory(ctrl_vk_impl, buf_vk.buffer)
                },
                buf
              };
              return buf_vk;
            }());
        }
        {
          auto bind_infos_vk = ranges::vec(
            views::catcast<const _::slab &>(
              views::deref(views::keep(ctrl_vk_impl.buses)),
              ctrl_vk_impl.buffers),
            [](const _::slab &s){
              return vk::BindBufferMemoryInfo{*s.buffer, *s.memory};
            });
          if(!bind_infos_vk.empty())
            ctrl_vk_impl.device.bindBufferMemory2(bind_infos_vk);
        }
        return std::make_unique<vulkan::ctrl::impl_>(std::move(ctrl_vk_impl));
      }();

      for(auto &ctrl_vk:exe_vk.ctrls)
        for(auto &bin_vk:ctrl_vk.bins)
          bin_vk.impl = [&]{
            _::bin bin_vk_impl{
              .descriptor_set_layout = [&]{
                const auto bindings_vk =
                  ranges::vec(
                    bin_vk.spirv.mem.buffers,
                    [&](const auto &buf_spv){
                      return vk::DescriptorSetLayoutBinding{
                        static_cast<std::uint32_t>(buf_spv.binding),
                        _::ctrl::buf_descriptor_type,
                        1,
                        _::bin::shader_stage_flag_bit
                      };
                    });
                vk::DescriptorSetLayoutCreateInfo info_vk{vk::DescriptorSetLayoutCreateFlags(),bindings_vk};
                return ctrl_vk.impl->device.createDescriptorSetLayout(info_vk);
              }(),
              .descriptor_set = [&]{
                vk::DescriptorSetAllocateInfo info_vk(
                  *ctrl_vk.impl->descriptor_pool, 1,
                  &*bin_vk_impl.descriptor_set_layout);
                return std::move(ctrl_vk.impl->device.allocateDescriptorSets(info_vk).front());
              }(),
              .shader_module = [&]{
                vk::ShaderModuleCreateInfo smci{
                  {}, bin_vk.spirv.tape.size() * sizeof(spirv::word), bin_vk.spirv.tape.data()
                };
                return ctrl_vk.impl->device.createShaderModule(smci);
              }(),
              .pipeline_layout = [&]{
                vk::PipelineLayoutCreateInfo info_vk({}, *bin_vk_impl.descriptor_set_layout);
                return ctrl_vk.impl->device.createPipelineLayout(info_vk);
              }(),
              .pipeline = [&]{
                auto bin_name_spv = bin_vk.spirv.kernel.name();
                vk::ComputePipelineCreateInfo info_vk{
                  {},
                  vk::PipelineShaderStageCreateInfo{
                    {}, _::bin::shader_stage_flag_bit, *bin_vk_impl.shader_module, bin_name_spv.c_str()},
                  *bin_vk_impl.pipeline_layout
                };
                return ctrl_vk.impl->device.createComputePipeline(ctrl_vk.impl->pipeline_cache, info_vk);
              }()
            };

            {
              auto descriptor_buffer_infos_vk =
                ranges::vec(
                  bin_vk.spirv.mem.buffers,
                  [&](const auto &buf_spv){
                    auto &buf_vk = ctrl_vk.impl->buf(buf_spv.op);
                    return vk::DescriptorBufferInfo(*buf_vk.buffer, 0, VK_WHOLE_SIZE);
                  });
              auto write_descriptor_sets_vk =
                ranges::vec(
                  std::views::zip(
                    bin_vk.spirv.mem.buffers,
                    descriptor_buffer_infos_vk),
                  bind<>(
                    apply,
                    [&](const auto &buf_spv,auto &info_vk){
                      return vk::WriteDescriptorSet(
                        *bin_vk_impl.descriptor_set, buf_spv.binding, 0, 1,
                        _::ctrl::buf_descriptor_type, nullptr, &info_vk);
                    }));
              if(!write_descriptor_sets_vk.empty())
                ctrl_vk.impl->device.updateDescriptorSets(write_descriptor_sets_vk, {});
            }

            return std::make_unique<vulkan::bin::impl_>(std::move(bin_vk_impl));
          }();


      vk::CommandBufferBeginInfo begin_info_vk(vk::CommandBufferUsageFlags{});
      vk::MemoryBarrier2 memory_barrier_vk{
        vk::PipelineStageFlagBits2::eTransfer | vk::PipelineStageFlagBits2::eComputeShader,
        vk::AccessFlagBits2::eTransferWrite | vk::AccessFlagBits2::eShaderStorageWrite,
        vk::PipelineStageFlagBits2::eTransfer | vk::PipelineStageFlagBits2::eComputeShader,
        vk::AccessFlagBits2::eTransferRead | vk::AccessFlagBits2::eShaderStorageRead
      };
      vk::DependencyInfo dependency_info_vk{
        {},
        1,
        &memory_barrier_vk,
        0,
        nullptr,
        0,
        nullptr,
        nullptr
      };
      for(auto &main_vk:exe_vk.mains)
        for(auto submit_vert:graphs::vertex_ids(main_vk.schedule.graph)){
          auto &submit_vk = main_vk.submits.at(submit_vert);
          submit_vk.impl = [&]{
            auto &ctrl_vk = exe_vk.ctrl(submit_vk.dev);
            _::submit submit_vk_impl{
              .cmds = ranges::vec(graphs::vertex_ids(submit_vk.schedule.graph),
                                  [&](ignore_t){
                                    return _::cmd{
                                      .command_buffer = _::make_command_buffer(*ctrl_vk.impl),
                                      .event = ctrl_vk.impl->device.createEvent(
                                        vk::EventCreateInfo{
                                          vk::EventCreateFlags{vk::EventCreateFlagBits::eDeviceOnly}})
                                    };
                                  }),
              .final_command_buffer = _::make_command_buffer(*ctrl_vk.impl),
              .signal_semaphore{}
            };

            auto job_done = graphs::vprop(submit_vk.schedule.graph, false);
            for(auto root_vert:graphs::vertex_ids(submit_vk.schedule.graph)){
              if(graphs::get(job_done, root_vert))
                continue;
              for(auto job_vert:graphs::ipostorder(std::ref(submit_vk.schedule.graph), root_vert)){
                if(graphs::get(job_done, job_vert))
                  continue;
                graphs::put(job_done, job_vert, true);
                auto job = submit_vk.schedule.graph[job_vert];
                auto &cmd_vk = submit_vk_impl.cmds.at(job_vert);
                cmd_vk.command_buffer.begin(begin_info_vk);
                if(auto await_verts = graphs::viadjacent(submit_vk.schedule.graph, job_vert);
                   !std::ranges::empty(await_verts)){
                  auto await_events_vk = ranges::vec(
                    await_verts,
                    [&](auto await_wait_vk){
                      return *submit_vk_impl.cmds.at(await_wait_vk).event;
                    });
                  auto await_infos_vk =
                    ranges::vec(
                      views::repeat_n(dependency_info_vk,std::ranges::size(await_events_vk)));
                  cmd_vk.command_buffer.waitEvents2(await_events_vk,await_infos_vk);
                }
                else{
                  cmd_vk.command_buffer.pipelineBarrier2(dependency_info_vk);
                }
                with_famobj(
                  [&](auto job){
                    if constexpr(job.tag() == codes::optag::to){

                      auto [src, dst, buf] = args(code, job);

                      const auto &bus_info = mem.info(buf);

                      auto &bus_vk = ctrl_vk.impl->bus(bus_info).value();
                      auto &buf_vk = ctrl_vk.impl->buf(buf);

                      if(src != code.builtins.host && dst != code.builtins.host)
                        throw std::runtime_error("unexpected to");

                      const auto upload = (src == code.builtins.host);

                      vk::BufferCopy2 copy_region(
                        upload ? bus_info.offset * (bus_info.width / 8): 0,
                        upload ? 0 : bus_info.offset * (bus_info.width / 8),
                        mem.bytes(buf).size());
                      vk::CopyBufferInfo2 copy_info{
                        upload ? *bus_vk.buffer : *buf_vk.buffer,
                        upload ? *buf_vk.buffer : *bus_vk.buffer,
                        1,
                        &copy_region
                      };
                      cmd_vk.command_buffer.copyBuffer2(copy_info);

                    }
                    else{
                      auto &bin_vk = ctrl_vk.bin({job});

                      cmd_vk.command_buffer.bindPipeline(
                        vk::PipelineBindPoint::eCompute,*bin_vk.impl->pipeline);
                      cmd_vk.command_buffer.bindDescriptorSets(
                        vk::PipelineBindPoint::eCompute,*bin_vk.impl->pipeline_layout,
                        0,
                        {*bin_vk.impl->descriptor_set},
                        {});
                      cmd_vk.command_buffer.dispatch(1,1,1);

                    }
                  },
                  job);
                cmd_vk.command_buffer.setEvent2(*cmd_vk.event,dependency_info_vk);
                cmd_vk.command_buffer.end();
                submit_vk_impl.command_buffer_info.push_back(vk::CommandBufferSubmitInfo{ *cmd_vk.command_buffer, 0});
              }
            }
            submit_vk_impl.final_command_buffer.begin(begin_info_vk);
            submit_vk_impl.final_command_buffer.pipelineBarrier2(dependency_info_vk);
            for(auto &cmd_vk:submit_vk_impl.cmds){
              submit_vk_impl.final_command_buffer
                .resetEvent2(*(cmd_vk.event),
                             {vk::PipelineStageFlagBits2::eComputeShader});
            }
            submit_vk_impl.final_command_buffer.end();
            submit_vk_impl.command_buffer_info.push_back(
              vk::CommandBufferSubmitInfo{*submit_vk_impl.final_command_buffer,0});
            submit_vk_impl.wait_semaphore_info.resize(graphs::idegree(main_vk.schedule.graph, submit_vert));
            return std::make_unique<vulkan::submit::impl_>(std::move(submit_vk_impl));
          }();
          submit_vk.impl->submit_info = 
            vk::SubmitInfo2(
              {},
              submit_vk.impl->wait_semaphore_info.size(),
              submit_vk.impl->wait_semaphore_info.data(),
              submit_vk.impl->command_buffer_info.size(),
              submit_vk.impl->command_buffer_info.data(),
              1,
              &submit_vk.impl->signal_semaphore_info);
        }
    }

  }
}
exe env::load(cells &mem, code code, codes::apisym api){
  compile(code, api);
  exe exe_vk{};
  exe_vk.code = std::move(code);
  exe_vk.ctrls = ranges::vec(
    views::keep(devs, compose<>(has_value, &vulkan::dev::sym)),
    [&](auto &dev_vk){
      ctrl ctrl_vk{};
      ctrl_vk.dev = &dev_vk;
      return ctrl_vk;
    });
  _::load_impl(mem, exe_vk, api);
  return exe_vk;
}

} //hysj::devices::vulkan
#include <hysj/tools/epilogue.hpp>
