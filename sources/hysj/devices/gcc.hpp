#pragma once

#include<array>
#include<functional>
#include<span>
#include<thread>
#include<vector>

#include<kumi/tuple.hpp>

#include<hysj/codes/grammar.hpp>
#include<hysj/codes/code.hpp>
#include<hysj/codes/cells.hpp>
#include<hysj/binaries/gcc.hpp>
#include<hysj/devices/device.hpp>
#include<hysj/tools/type_traits.hpp>
#include<hysj/tools/types.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::devices::gcc{

constexpr gccmem to_gccmem(cells &m){
  return with_integer_sequence(
    constant_c<std::size(gcctypes)>,
    [&](auto... I){
      return kumi::make_tuple(m.get(constant_c<gcctypes[I].set>,
                                    constant_c<gcctypes[I].width>)...);
    });
}

struct HYSJ_EXPORT exe{

  gcccfg config;
  codes::devsym dev;
  gccmem mem;
  codes::code code;
  std::vector<gccbin> bins;

  exe() = default;
  exe(exe &&) = default;
  exe &operator=(exe &&) = default;
  exe(const exe &) = delete;
  exe &operator=(const exe &) = delete;

  friend HYSJ_MIRROR_STRUCT(exe, (config, dev, mem, code, bins));

  auto &bin(this auto &c,codes::taskfam b){
    auto it = std::ranges::find_if(c.bins, b, &gccbin::decl);
    if(it == std::ranges::end(c.bins))
      throw std::out_of_range(fmt::format("{}", b));
    return *it;
  }

  void run(codes::taskfam);
};
static_assert(devices::concepts::exe<exe &>);

struct HYSJ_EXPORT env : gcccfg{
  std::optional<codes::devsym> dev;
  
  friend HYSJ_MIRROR_STRUCT(env, (dev)(gcccfg));

  exe load(cells &, code, codes::apisym);

  std::optional<std::size_t> min_alignment()const{
    return alignment.transform([](auto a){ return static_cast<std::size_t>(a); });
  }
};
static_assert(devices::concepts::env<env &>);

} //hysj::devices::gcc
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

namespace gccdevs = devs::gcc;

using gccenv = gccdevs::env;
using gccexe = gccdevs::exe;

} //hysj
#include<hysj/tools/epilogue.hpp>
