#pragma once
#include<cstdint>
#include<memory>
#include<optional>
#include<span>
#include<string>
#include<stdexcept>
#include<vector>

#include<fmt/core.h>

#include<hysj/codes/code.hpp>
#include<hysj/codes/cells.hpp>
#include<hysj/codes/schedules.hpp>
#include<hysj/devices/device.hpp>
#include<hysj/tools/preprocessor.hpp>
#include<hysj/tools/types.hpp>
#include<hysj/tools/ranges/join.hpp>
#include<hysj/tools/ranges/keep.hpp>
#include<hysj/tools/ranges/map.hpp>
#include<hysj/binaries/spirv.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::devices::vulkan{

namespace spirv = binaries::spirv;

struct HYSJ_EXPORT bin{
  struct impl_;

  spvbin spirv;
  std::unique_ptr<impl_> impl;

  friend HYSJ_MIRROR_STRUCT(bin, (spirv));

  bin();
  bin(bin &&);
  bin &operator=(bin &&);
  ~bin();
};

#define HYSJ_DEVICES_VULKAN_DEVS \
  (other)                        \
  (igpu)                         \
  (dgpu)                         \
  (vgpu)                         \
  (cpu)
enum class devtype : natural {
  HYSJ_SPLAT(0,HYSJ_DEVICES_VULKAN_DEVS)
};
HYSJ_MIRROR_ENUM(devtype,HYSJ_DEVICES_VULKAN_DEVS)

struct HYSJ_EXPORT dev{
  struct impl_;

  std::uint32_t vendor_id, device_id;
  devtype type;
  std::string name;

  std::unique_ptr<impl_> impl;
  spvenv spirv;
  std::optional<codes::devsym> sym;
  
  friend HYSJ_MIRROR_STRUCT(dev, (vendor_id, device_id, type, name, spirv, sym));

  dev();
  dev(dev &&);
  dev &operator=(dev &&);
  ~dev();
};


struct HYSJ_EXPORT ctrl{
  struct impl_;

  vulkan::dev *dev;
  std::unique_ptr<impl_> impl;
  std::vector<vulkan::bin> bins;

  friend HYSJ_MIRROR_STRUCT(ctrl, (dev, bins));

  ctrl();
  ctrl(ctrl &&);
  ctrl &operator=(ctrl &&);
  ~ctrl();

  auto &bin(this auto &c,codes::taskfam b){
    auto it = std::ranges::find_if(c.bins,
                                   [&](const auto &bin){
                                     return bin.spirv.kernel.decl == b;
                                   });
    if(it == std::ranges::end(c.bins))
      throw std::out_of_range(fmt::format("{}", b));
    return *it;
  }
};

struct HYSJ_EXPORT submit{
  struct impl_;

  codes::taskfam sym;
  codes::devsym dev;

  codes::sched schedule;

  std::unique_ptr<impl_> impl;

  friend HYSJ_MIRROR_STRUCT(submit, (sym, dev, schedule));

  submit();
  submit(submit &&);
  submit &operator=(submit &&);
  ~submit();
};

struct HYSJ_EXPORT main{
  codes::taskfam sym;

  codes::sched schedule;
  std::vector<submit> submits;

  friend HYSJ_MIRROR_STRUCT(main, (sym, schedule, submits));

  main();
  main(main &&);
  main &operator=(main &&);
  ~main();
};

struct HYSJ_EXPORT exe{
  codes::code code;
  std::vector<vulkan::ctrl> ctrls;
  std::vector<vulkan::main> mains;

  friend HYSJ_MIRROR_STRUCT(exe, (code, ctrls, mains));

  exe();
  exe(exe &&);
  exe &operator=(exe &&);
  ~exe();

  auto &ctrl(this auto &c,codes::devsym d){
    auto it = std::ranges::find(c.ctrls, d, [](const auto &c){ return c.dev->sym; });
    if(it == std::ranges::end(c.ctrls))
      throw std::runtime_error("unexpected dev");
    return *it;
  }
  auto &main(this auto &c,codes::taskfam m){
    auto it = std::ranges::find(c.mains, m, &vulkan::main::sym);
    if(it == std::ranges::end(c.mains))
      throw std::runtime_error("unexpected main");
    return *it;
  }
  auto submits(this auto &c){
    return views::join(views::map(c.mains, &vulkan::main::submits));
  }
  auto submits(this auto &c,codes::devsym d){
    return views::keep(c.submits(), [d](const auto &s){ return s.dev == d; });
  }

  void run(codes::taskfam);
};
static_assert(devices::concepts::exe<exe &>);

struct HYSJ_EXPORT env{

  struct impl_;

  std::unique_ptr<impl_> impl;
  std::vector<vulkan::dev> devs;

  friend HYSJ_MIRROR_STRUCT(env,(devs));

  env();
  env(env &&);
  env& operator=(env &&);
  ~env();

  std::optional<std::size_t> min_alignment()const;

  auto &dev(this auto &c,codes::devsym d){
    auto it = std::ranges::find(c.devs, d, &dev::sym);
    if(it == std::ranges::end(c.devs))
      throw std::out_of_range(fmt::format("{}", d));
    return *it;
  }

  exe load(cells &,code,codes::apisym);

  static env make();
};

static_assert(devices::concepts::env<env &>);

} //hysj::devices::vulkan
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

namespace vkdevs = devs::vulkan;

using vkenv = vkdevs::env;
using vkexe = vkdevs::exe;


} //hysj
HYSJ_ENUM_FORMATTER(hysj::_HYSJ_VERSION_NAMESPACE::devices::vulkan::devtype);
template<>
struct fmt::formatter<hysj::_HYSJ_VERSION_NAMESPACE::devices::vulkan::dev>{
  using T = hysj::_HYSJ_VERSION_NAMESPACE::devices::vulkan::dev;
  constexpr auto parse(format_parse_context& context)const{
    auto it  = context.begin(),
         end = context.end();
    if(it != end && *it != '}')
      throw fmt::format_error("invalid format");
    return it;
  }
  auto format(const T &d,
              format_context &context)const{
    return fmt::format_to(context.out(),"{0}:{1}",d.type,d.name);
  }
};

#include<hysj/tools/epilogue.hpp>
