#include<hysj/devices/gcc.hpp>

#include <exec/any_sender_of.hpp>
#include <exec/static_thread_pool.hpp>
#include <stdexec/execution.hpp>

#include<hysj/codes/code.hpp>
#include<hysj/codes/grammar.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::devices::gcc{

using main_receiver = exec::any_receiver_ref<
  stdexec::completion_signatures<stdexec::set_value_t(),
                                 stdexec::set_error_t(std::exception_ptr),
                                 stdexec::set_stopped_t()>>;
using main_any_sender = main_receiver::any_sender<>;

using main_sender = decltype(stdexec::split(std::declval<main_any_sender>()));

struct main{
  std::map<codes::taskfam, main_sender> senders;
  main_sender *entry;
};

main make_main(exe &exe,codes::taskfam task, auto scheduler){

  auto sender = [](main_any_sender sender) -> main_sender {
    return stdexec::split(std::move(sender));
  };

  main m{
    .senders{},
    .entry{}
  };

  auto pre0 = [&](id o){
    auto t = codes::task(o);
    if(auto it = m.senders.find(t);it != m.senders.end())
      return false;
    if(auto s = grammars::with_famobj(
      [&](auto t) -> std::optional<main_sender> {
        if constexpr(t.tag == codes::optag::noop)
          return sender(stdexec::just());
        else if constexpr(t.tag == codes::optag::to){
          auto [from, to, buf] = args(exe.code, t);
          return sender(stdexec::just());
        }
        else if constexpr(t.tag == codes::optag::on){
          auto [tgt, algo] = args(exe.code, t);
          auto &bin = *std::ranges::find(exe.bins, algo, &gccbin::decl);
          return sender(
            stdexec::just()
            |stdexec::then(
              [&]{
                auto run = [&](auto i){
                  apply(
                    [&](auto... mem){
                      bin.kernel(i, mem.data()...);
                    },
                    exe.mem);
                };
                const auto N = bin.local_thread_count(bin.impl);
                std::vector<std::jthread> threads(N - 1);
                for(auto i:views::indices(N - 1))
                  threads[i] = std::jthread(run, i);
                run(N - 1);
              }));
        }
        return std::nullopt;
      },
      t))
    {
      m.senders.emplace(t, std::move(*s));
      return false;
    }
    return true;
  };
  auto pre1 = [&](id o,id r,id a){
    if(codes::reltag_of(r) != codes::reltag::exe)
      return false;
    return pre0(a);
  };
  auto post = [&](auto... x){
    auto t = codes::task(pick<-1>(x...));
    m.senders.emplace(
      t,
      grammars::with_famobj(
        [&](auto t) -> main_sender {
          if constexpr(t.tag == codes::optag::when){
            auto [T] = args(exe.code, t);
            return sender(
              std::ranges::fold_left(
                T,
                main_any_sender{stdexec::just()},
                [&](main_any_sender s,codes::taskfam t) -> main_any_sender{
                  return stdexec::when_all(std::move(s), m.senders.at(t));
                }));
          }
          else if constexpr(t.tag == codes::optag::then){
            auto [T] = args(exe.code, t);
            return sender(
              std::ranges::fold_left(
                T,
                main_any_sender{stdexec::just()},
                [&](main_any_sender s,codes::taskfam t) -> main_any_sender{
                  return stdexec::let_value(std::move(s), [u=m.senders.at(t)]{ return u; });
                }));
          }
          else
            throw std::runtime_error(fmt::format("unexpected task: {}", t));
        },
        t));
    return none{};
  };
  grammars::walk<none>(exe.code, overload(pre0, pre1), post, task);
  auto it = m.senders.find(task);
  HYSJ_ASSERT(it != m.senders.end());
  m.entry = &it->second;
  return m;
}

void exe::run(codes::taskfam task){
  exec::static_thread_pool thread_pool{static_cast<std::size_t>(config.thread_count)};
  auto scheduler = thread_pool.get_scheduler();
  auto main = make_main(*this, task, scheduler);
  stdexec::sync_wait(stdexec::starts_on(scheduler, *main.entry));
}

exe env::load(cells &mem,code c,codes::apisym api){
  compile(c, api);

  exe exe{};

  exe.config = *this;
  exe.dev = dev.value();
  exe.mem = to_gccmem(mem);
  exe.code = std::move(c);

  auto pre0 = [&](id o){
    auto t = codes::taskcast(o);
    if(!t)
      return true;
    return grammars::with_famobj(
      [&](auto t) -> bool{
        if constexpr(t.tag == codes::optag::noop)
          return false;
        else if constexpr(t.tag == any_of(codes::optag::loop, codes::optag::exit, codes::optag::put))
          throw std::runtime_error(fmt::format("unexpected task: {}", t));
        else if constexpr(t.tag == codes::optag::to){
          auto [from, to, buf] = args(exe.code, t);
          if(from == none_of(exe.code.builtins.host, exe.dev) &&
             to == none_of(exe.code.builtins.host, exe.dev))
            throw std::runtime_error(fmt::format("unexpected to: {} from {} to {} for {}", t, from, to, buf));
          return false;
        }
        else if constexpr(t.tag == codes::optag::on){
          auto [tgt, algo] = args(exe.code, t);
          if(tgt != exe.dev)
            throw std::runtime_error(fmt::format("unexpected on: {} on {} for {}", t, tgt, algo));
          
          if(auto it = std::ranges::find(exe.bins, algo, &gccbin::decl);
             it == std::ranges::end(exe.bins)){
            auto config = exe.config;

            config.dot_path = config.dot_path.transform([&](const auto &s){ return fmt::format("{}-{}", s, t); });
            config.c_path = config.c_path.transform([&](const auto &s){ return fmt::format("{}-{}", s, t); });
            config.as_path = config.as_path.transform([&](const auto &s){ return fmt::format("{}-{}", s, t); });
            exe.bins.emplace_back(gcccc(config, exe.code, mem.infos, algo));
          }
          return false;
        }
        return true;
      },
      *t);
  };
  auto pre1 = [&](id o,id r,id a){
    if(codes::reltag_of(r) != any_of(codes::reltag::lnk, codes::reltag::exe))
      return false;
    return pre0(a);
  };
  grammars::walk<none>(exe.code, overload(pre0, pre1), never, api);

  return exe;
}


} //hysj::devices::gcc
#include<hysj/tools/epilogue.hpp>
