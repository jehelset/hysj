#pragma once

#include<array>
#include<functional>
#include<ranges>
#include<tuple>
#include<utility>
#include<initializer_list>
#include<vector>

#include<fmt/core.h>
#include<fmt/std.h>
#include<fmt/ranges.h>
#include<kumi/tuple.hpp>

#include<hysj/tools/coroutines.hpp>
#include<hysj/tools/coroutines/trees.hpp>
#include<hysj/tools/functional/apply.hpp>
#include<hysj/tools/functional/always.hpp>
#include<hysj/tools/functional/any_of.hpp>
#include<hysj/tools/functional/bind.hpp>
#include<hysj/tools/functional/optional.hpp>
#include<hysj/tools/functional/overload.hpp>
#include<hysj/tools/functional/invoke.hpp>
#include<hysj/tools/functional/unwrap.hpp>
#include<hysj/tools/functional/variant.hpp>
#include<hysj/tools/graphs/adjacency.hpp>
#include<hysj/tools/graphs/algorithms/deepwalks.hpp>
#include<hysj/tools/graphs/degree.hpp>
#include<hysj/tools/graphs/directions.hpp>
#include<hysj/tools/graphs/elements.hpp>
#include<hysj/tools/graphs/incidence.hpp>
#include<hysj/tools/graphs/incidence_graphs.hpp>
#include<hysj/tools/functional/pick.hpp>
#include<hysj/tools/preprocessor.hpp>
#include<hysj/tools/ranges/cat.hpp>
#include<hysj/tools/ranges/map.hpp>
#include<hysj/tools/ranges/generate.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/reflection.hpp>
#include<hysj/tools/trees.hpp>
#include<hysj/tools/types.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::grammars{

using id = std::array<natural,2>;

inline constexpr auto tag_of = []<typename tag>(type_t<tag>, const id &s)noexcept{
  return *enum_cast<tag>(s[0]);
};
constexpr natural idx_of(const id &i)noexcept{
  return i[1];
}

template<typename T>
inline constexpr bool is_obj = false;
template<typename T>
inline constexpr bool is_rel = false;
template<typename T>
inline constexpr bool is_sym = is_obj<T> || is_rel<T>;

namespace concepts{
  template<typename T>
  concept objtag = is_obj<T>;
  template<typename T>
  concept reltag = is_rel<T>;
  template<typename T>
  concept symtag = is_sym<T>;
}

template<typename Tree,typename Rels, typename Args>
struct objgram{
  Tree tree;
  Rels rels;
  Args args;
};

template<auto tag>
inline constexpr auto objgram_of = objgram{
  .tree = trees::tup(),
  .rels = kumi::tuple{},
  .args = kumi::tuple{}
};


template<auto O,std::size_t I>
inline constexpr auto objreltag = kumi::get<I>(objgram_of<O>.rels);
template<auto O,std::size_t I>
inline constexpr auto objargtag = kumi::get<I>(objgram_of<O>.args);

template<auto T>
struct sym{
  static_assert(concepts::symtag<decltype(T)>);
  
  static constexpr constant_t<T> tag{};

  natural idx;
  friend HYSJ_MIRROR_STRUCT(sym,(idx))

  auto operator<=>(const sym &)const = default;
  
  constexpr grammars::id id()const noexcept{
    return {etoi(T),idx};
  }

  constexpr operator grammars::id()const noexcept{
    return id();
  }
};

inline constexpr auto with_symtag = overload(
  []<typename tag>(type_t<tag>,auto &&f,std::underlying_type_t<tag> n){
    static_assert(concepts::symtag<tag>);
    return with_enum<tag>(
      [f = fwd(f)](auto t)mutable{
        return f(t);
      },
      enum_cast<tag>(n).value());
  },
  []<typename tag>(type_t<tag>,auto &&f,tag n){
    static_assert(concepts::symtag<tag>);
    return with_enum<tag>(
      [f = fwd(f)](auto t)mutable{
        return f(t);
      },
      n);
  });

inline constexpr auto with_sym = []<typename tag>(type_t<tag>,auto &&f,id id){
  static_assert(concepts::symtag<tag>);
  return with_enum<tag>(
    [=,f = fwd(f)](auto t)mutable{
      return f(sym<t()>{idx_of(id)});
    },
    tag_of(type_c<tag>,id));
};

template<typename T>
inline constexpr bool is_fam = false;

namespace concepts{
  template<typename T>
  concept famtag = is_fam<T>;
}

template<typename Objs>
struct famgram{
  Objs objs;
};
template<auto F>
inline constexpr auto famgram_of = famgram{
  .objs = kumi::tuple{}
};

template<auto F>
inline constexpr auto famnum = kumi::size_v<decltype(famgram_of<F>.objs)>;
template<auto F>
using famseq = std::make_index_sequence<famnum<F>>;

template<auto F,std::size_t I>
inline constexpr auto famobj = get<I>(famgram_of<F>.objs);

//FIXME: proper - jeh
template<auto F>
using famobjtag = std::remove_cvref_t<decltype(famobj<F, 0>)>;

template<auto F>
inline constexpr auto objdepth = []{
  if constexpr(is_obj<autotype(F)>)
    return 0_n;
  else
    return 1_n + fold(
      famseq<F>{},
      0_n,
      [](auto D,auto I){
        return std::max(D, objdepth<famobj<F, I>>);
      });
}();

namespace _{

  template<auto O,typename S,typename X>
  constexpr std::uint8_t famcastseq_impl(S &s,X x,std::uint8_t i){
    if constexpr(is_obj<autotype(O)>)
      return O == x ? i : 0;
    else{
      auto F = []<std::size_t... J>(std::index_sequence<J...>){
        return std::array{&famcastseq_impl<famobj<O, J>, S, X>...};
      }(grammars::famseq<O>{});
      for(std::uint8_t j = 0;j != static_cast<std::uint8_t>(F.size()); ++j)
        if(auto n = F[j](s, x, i + 1)){
          s[i] = j;
          return n;
        }
      return 0;
    }
  }

}
template<auto F>
constexpr auto famcastseq(auto x){
  constexpr auto D = objdepth<F>;
  std::array<std::uint8_t, D + 1> S{};
  S[D] = _::famcastseq_impl<F>(S, x, 0);
  return S;
}

template<auto F>
constexpr auto can_famcast(auto x){
  return famcastseq<F>(x).back() != 0;
}

inline constexpr auto is_famobj = overload(
  []<auto F, auto O>(constant_t<F>,constant_t<O>){
    static_assert(concepts::famtag<decltype(F)> && concepts::symtag<decltype(O)>
                  && std::is_same_v<famobjtag<F>,decltype(O)>);
    return can_famcast<F>(O);
  },
  []<auto F,typename O>(constant_t<F>,O o){
    static_assert(concepts::famtag<decltype(F)> && concepts::symtag<O>
                  && std::is_same_v<famobjtag<F>,O>);
    return can_famcast<F>(o);
  });

template<auto F>
struct fam;

template<auto T>
using famval = reify(
  []{
    if constexpr(is_fam<autotype(T)>)
      return type_c<fam<T>>;
    else
      return type_c<sym<T>>;
  }());

template<auto F>
using any_famobj = typename decltype(
  []<std::size_t... I>(std::index_sequence<I...>){
    return type_c<std::variant<famval<famobj<F,I>>...>>;
  }(famseq<F>{}))::type;

template<auto F>
struct fam{
  static_assert(concepts::famtag<decltype(F)>);

  static constexpr constant_t<F> tag{};

  any_famobj<F> obj;

  friend HYSJ_MIRROR_STRUCT(fam,(obj));

  auto operator<=>(const fam &)const = default;

  constexpr auto id()const noexcept{
    return visitor([](const auto &obj){ return obj.id(); })(obj);
  }
  constexpr operator grammars::id()const noexcept{
    return id();
  }
};

template<auto... F>
requires (concepts::famtag<decltype(F)> && ...)
constexpr auto with_famobj(auto &&g,fam<F>... f)
  arrow((std::visit(fwd(g),f.obj...)))
;

namespace _{

  template<auto F,auto U,auto I>
  constexpr auto famcast_impl(auto s){
    if constexpr(I == U.back())
      return s;
    else
      return fam<F>{
        famcast_impl<famobj<F, U[I]>, U, I + 1>(s)
      };
  }

}

template<auto F>
inline constexpr auto famcast = []<auto S>(sym<S> s){
  if constexpr(!can_famcast<F>(S))
    return no<fam<F>>;
  else
    return just(_::famcast_impl<F, famcastseq<F>(S), 0>(s));
};

inline constexpr auto refcast = []<auto T>(constant_t<T> tag,id ref){
  if constexpr(is_sym<decltype(T)>)
    return with_sym(
      type_c<decltype(T)>,
      [](auto ref){
        if constexpr(ref.tag == T)
          return just(ref);
        return no<sym<T>>;
      },
      ref);
  if constexpr(is_fam<decltype(T)>)
    return with_sym(
      type_c<famobjtag<T>>,
      [=](auto ref){
        return famcast<T>(ref);
      },
      ref);
};

template<auto T,std::size_t I>
inline constexpr auto objrelcast = bind<>(refcast, constant_c<objreltag<T, I>>);
template<auto T,std::size_t I>
inline constexpr auto objargcast = bind<>(refcast, constant_c<objargtag<T, I>>);

namespace concepts{

  template<typename T>
  concept input_id = ranges::concepts::convertible_input_range<T,id>;
  template<typename T>
  concept sized_input_id = input_id<T> && std::ranges::sized_range<T>;

  template<typename T>
  concept forward_id = ranges::concepts::convertible_forward_range<T,id>;
  template<typename T>
  concept random_access_id = ranges::concepts::convertible_random_access_range<T,id>;

  template<typename T,auto S>
  concept forward_sym = ranges::concepts::convertible_forward_range<T,sym<S>>;
  template<typename T,auto F>
  concept forward_fam = ranges::concepts::convertible_forward_range<T,fam<F>>;

  template<typename T,auto S>
  concept random_access_sym = ranges::concepts::convertible_random_access_range<T,sym<S>>;
  template<typename T,auto F>
  concept random_access_fam = ranges::concepts::convertible_random_access_range<T,fam<F>>;

}

using idgraph = graphs::incidence_graph<id, id>;
using vert = graphs::vertex_id;
using edge = graphs::edge_id;

template<typename tag,typename value>
using symtabs = std::array<std::vector<value>, enumerator_count<tag>>;

template<auto S>
inline constexpr auto symdat_of = type_c<void>; 

template<auto S>
using symdat_of_t = reify(symdat_of<S>);

template<auto S>
constexpr bool empty_symdat = std::is_same_v<symdat_of_t<S>, void>;

template<auto S>
using symdat_t = std::conditional_t<empty_symdat<S>, none, symdat_of_t<S>>; 

template<typename tag>
using symdats = reify(
  with_enumerators<tag>(
    [](auto... D){
      return type_c<
        kumi::tuple<std::conditional_t<empty_symdat<D()>,
                                       symdat_t<D()>,
                                       std::vector<symdat_t<D()>>>...>>;
    }));

template<typename objtag, typename reltag>
struct syntax{
  static_assert(concepts::objtag<objtag> && concepts::reltag<reltag>);

  using objtag_type = objtag;
  using reltag_type = reltag;

  idgraph graph;

  symtabs<objtag, vert> objverts;
  symtabs<reltag, edge> reledges;

  symdats<objtag> objdats;
  symdats<reltag> reldats;
  
  friend HYSJ_MIRROR_STRUCT(syntax, (graph, objverts, reledges, objdats, reldats));

  using am_syntax = void;
};

namespace concepts{

  template<typename T>
  concept syntax = requires { typename std::remove_cvref_t<unwrap_t<T>>::am_syntax; };

}

template<typename syntax>
using objtag_t = typename std::remove_cvref_t<unwrap_t<syntax>>::objtag_type;
template<typename syntax>
using reltag_t = typename std::remove_cvref_t<unwrap_t<syntax>>::reltag_type;

namespace concepts{

  template<typename T,typename syntax>
  concept objtag_for = std::same_as<objtag_t<syntax>, T>;
  template<typename T,typename syntax>
  concept reltag_for = std::same_as<reltag_t<syntax>, T>;
  template<typename T,typename syntax>
  concept symtag_for = objtag_for<T, syntax> || reltag_for<T, syntax>;

}

template<concepts::syntax S>
constexpr auto
objcount(S &&s)
  arrow((graphs::order(unwrap(s).graph)));
template<concepts::syntax S>
constexpr auto
relcount(S &&s)
  arrow((graphs::size(unwrap(s).graph)));

template<concepts::syntax S>
constexpr auto
symcount(S &&s,objtag_t<S> t)
  arrow((unwrap(s).objverts[etoi(t)].size()));
template<concepts::syntax S>
constexpr auto
symcount(S &&s,reltag_t<S> t)
  arrow((unwrap(s).reledges[etoi(t)].size()));

template<concepts::syntax S,typename T>
constexpr auto
make_symtabs(S &&s,type_t<T>,auto &&f){
  static_assert(concepts::symtag_for<T, S>);
  return with_enumerators<T>(
    [&](auto... t){
      return std::array<decltype(fwd(f)(constant_c<T{}>, std::size_t{})), sizeof...(t)>{
        f(t, symcount(s, t()))...
      };
    });
}
template<concepts::syntax S>
constexpr auto
make_objtabs(S &&s,auto &&f){
  return make_symtabs(s, type_c<objtag_t<S>>, fwd(f));
}
template<concepts::syntax S>
constexpr auto
make_reltabs(S &&s,auto &&f){
  return make_symtabs(s, type_c<reltag_t<S>>, fwd(f));
}

template<concepts::syntax S,typename T>
constexpr auto
resize_symtabs(S &&s,type_t<T>,auto &t){
  static_assert(concepts::symtag_for<T, S>);
  for(std::size_t i = 0;i != enumerator_count<T>;++i)
    t[i].resize(symcount(s, T{i}));
}
template<concepts::syntax S>
constexpr auto
resize_objtabs(S &&s,auto &t){
  resize_symtabs(s, type_c<objtag_t<S>>, t);
}
template<concepts::syntax S>
constexpr auto
resize_reltabs(S &&s,auto &t){
  resize_symtabs(s, type_c<reltag_t<S>>, t);
}

template<concepts::syntax S,objtag_t<S> T>
constexpr auto
syms(S &&s,constant_t<T> t)
  arrow((views::map(views::indices(symcount(s, t)), init<sym<T>>)));
template<concepts::syntax S,reltag_t<S> T>
constexpr auto
syms(S &&s,constant_t<T> t)
  arrow((views::map(views::indices(symcount(s, t)), init<sym<T>>)));

template<concepts::syntax S, concepts::symtag_for<S> T>
constexpr auto
symids(S &&s, type_t<T>){
  return with_enumerators<T>(
    [&](auto... t){
      return views::cat(
        views::map(
          views::indices(symcount(s, t)),
          bind<>(init<id>, etoi(t())))...);
    });
}
template<concepts::syntax S>
constexpr auto objids(S &&s)
  arrow((symids(s, type_c<objtag_t<S>>)))
  ;
template<concepts::syntax S>
constexpr auto relids(S &&s)
  arrow((symids(s, type_c<reltag_t<S>>)))
  ;

constexpr auto
objvert(concepts::syntax auto &&s, id id)
  arrow((unwrap(s).objverts.at(id[0]).at(id[1])));
constexpr auto
reledge(concepts::syntax auto &&s, id id)
  arrow((unwrap(s).reledges.at(id[0]).at(id[1])));

constexpr auto
argdeg(concepts::syntax auto &&s, id id)
  arrow((graphs::odegree(unwrap(s).graph, objvert(s, id))))
  ;
constexpr auto
objdeg(concepts::syntax auto &&s, id id)
  arrow((graphs::idegree(unwrap(s).graph, objvert(s, id))))
  ;

constexpr auto objid(concepts::syntax auto &&s, vert o)
  arrow((unwrap(s).graph.vertices[o]()));
constexpr auto relid(concepts::syntax auto &&s, edge r)
  arrow((unwrap(s).graph.edges[r]()));

constexpr auto
argverts(concepts::syntax auto &&s, vert o)
  arrow((graphs::voadjacent(unwrap(s).graph, o)));
constexpr auto
argids(concepts::syntax auto &&s, id o)
  arrow((views::map(argverts(s, objvert(s, o)), bind<>(lift((objid)),std::cref(s)))));

constexpr auto
reledges(concepts::syntax auto &&s, vert o)
  arrow((graphs::voincident(unwrap(s).graph, o)));
constexpr auto relids(concepts::syntax auto &&s, id o)
  arrow((views::map(reledges(s, objvert(s, o)),bind<>(lift((relid)),std::cref(s)))));

constexpr auto relobjvert(concepts::syntax auto &&s, edge r)
  arrow((graphs::tail(unwrap(s).graph, r)));
constexpr auto relobjid(concepts::syntax auto &&s, id i)
  arrow((objid(s, relobjvert(s, reledge(s, i)))));
constexpr auto relargvert(concepts::syntax auto &&s, edge r)
  arrow((graphs::head(unwrap(s).graph, r)));
constexpr auto relargid(concepts::syntax auto &&s, id i)
  arrow((objid(s, relargvert(s, reledge(s, i)))));

constexpr auto argobjverts(concepts::syntax auto &&s, vert o)
  arrow((graphs::viadjacent(unwrap(s).graph, o)));
constexpr auto argobjids(concepts::syntax auto &&s, id i)
  arrow((views::map(argobjverts(s, objvert(s, i)),bind<>(lift((objid)),std::cref(s)))));

constexpr auto argreledges(concepts::syntax auto &&s, vert o)
  arrow((graphs::viincident(unwrap(s).graph, o)));
constexpr auto argrelids(concepts::syntax auto &&s, id i)
  arrow((views::map(argreledges(s, objvert(s, i)),bind<>(lift((relid)),std::cref(s)))));

template<concepts::syntax S,auto T>
constexpr decltype(auto)
symdat(S &&s, sym<T> y){
  static_assert(concepts::symtag_for<decltype(T), S>);
  auto &D = [&] -> auto &{
    if constexpr(concepts::objtag_for<decltype(T),S>)
      return kumi::get<etoi(T)>(unwrap(s).objdats);
    else
      return kumi::get<etoi(T)>(unwrap(s).reldats);
  }();
  if constexpr(empty_symdat<T>)
    return (D);
  else
    return D.at(y.idx);
}

template<auto T>
inline constexpr auto parse(constant_t<T>,trees::concepts::input auto &&input)
  arrow((trees::parse<objgram_of<T>.tree>(fwd(input))));

template<auto O>
inline constexpr auto objnum = objgram_of<O>.tree.subrule_count(0);
template<auto O>
using objseq = std::make_index_sequence<objnum<O>>;
template<auto O>
using objseq_empty = std::make_index_sequence<std::max(std::size_t{1},objnum<O>) - 1>;

template<auto O>
constexpr bool objvar = objgram_of<O>.tree.max_size() == trees::none;
template<auto O,std::size_t I>
constexpr bool objvararg = 
  (objvar<O> && objnum<O> == I + 1);

namespace _{
  template<auto R>
  struct ref_impl;
  template<auto R>
  requires concepts::symtag<decltype(R)>
  struct ref_impl<R>:type_t<sym<R>>{};
  template<auto R>
  requires concepts::famtag<decltype(R)>
  struct ref_impl<R>:type_t<fam<R>>{};
}
template<auto R>
using ref = typename _::ref_impl<R>::type;

template<auto O,std::size_t I>
using objrel = ref<objreltag<O, I>>;
template<auto O,std::size_t I>
using objarg = ref<objargtag<O, I>>;
template<auto O,std::size_t I>
using objreldat = symdat_t<objreltag<O, I>>;

template<typename O,typename R,O T>
constexpr auto args(const syntax<O, R> &, sym<T> o, auto &&input){
  return trees::map<objgram_of<T>.tree>(
    parse(o.tag, fwd(input)),
    []<std::size_t I,std::size_t... J>(std::index_sequence<0, I, J...>, auto it){
      return objargcast<T, I>(*it).value();
    });
}
template<typename O,typename R,O T>
constexpr auto args(const syntax<O, R> &s, sym<T> o){
  return args(s, o, argids(s, (id)o));
}

template<typename O,typename R,O T>
constexpr auto rels(const syntax<O, R> &, sym<T> o, auto &&input){
  return trees::map<objgram_of<T>.tree>(
    parse(o.tag, fwd(input)),
    []<std::size_t I,std::size_t... J>(std::index_sequence< 0, I, J...>, auto it){
      return objrelcast<T, I>(*it).value();
    });
}
template<typename O,typename R,O T>
constexpr auto relargs(const syntax<O, R> &s, sym<T> o, auto &&input){
  return trees::map<objgram_of<T>.tree>(
    parse(o.tag, fwd(input)),
    [&]<std::size_t I,std::size_t... J>(std::index_sequence< 0, I, J...>, auto it){
      return objargcast<T, I>(relargid(s, *it)).value();
    });
}

template<typename O,typename R,O T>
constexpr auto rels(const syntax<O, R> &s, sym<T> o){
  return rels(s, o, relids(s, (id)o));
}
template<typename O,typename R,O T>
constexpr auto sink_rels(const syntax<O, R> &s, auto &&f, sym<T> o){
  trees::with<objgram_of<T>.tree>(
    rels(s, o),
    [f = fwd(f)]<std::size_t... I>(std::index_sequence< 0, I...>, auto tag,auto &&... syms){
      if constexpr(tag == trees::rule::var)
        for([[maybe_unused]] auto &&_:pick<0>(fwd(syms)...))
          ;
      else if constexpr(tag == trees::rule::one)
        invoke(f, std::integral_constant<std::size_t, pick<0>(I...)>{}, pick<0>(fwd(syms)...));
      return none{};
    });
}

template<typename O,typename R,O T,std::size_t I>
constexpr auto arg(const syntax<O, R> &s, sym<T> o, constant_t<I>)
  arrow((auto(kumi::get<I>(args(s, o)))))
  ;
template<typename O,typename R,O T,std::size_t I, std::size_t... J>
requires(sizeof...(J) > 0)
constexpr auto arg(const syntax<O, R> &s, sym<T> o, constant_t<I> i, constant_t<J>... j)
  arrow((auto(arg(s, arg(s, o, i), j...))))
  ;

template<typename O,typename R,O T,std::size_t I>
constexpr auto rel(const syntax<O, R> &s, sym<T> o, constant_t<I>)
  arrow((auto(kumi::get<I>(rels(s, o)))))
  ;
template<typename O,typename R,O T,std::size_t I, std::size_t... J>
requires(sizeof...(J) > 0)
constexpr auto rel(const syntax<O, R> &s, sym<T> o, constant_t<I> i, constant_t<J>... j)
  arrow((rel(s, rel(s, o, i), j...)))
  ;


namespace _{

  template<auto O, std::size_t I>
  inline constexpr auto objarg_cast = [](auto &&a){
    if constexpr(requires { objarg<O,I>{fwd(a)}; })
      return objarg<O, I>{fwd(a)};
    if constexpr(std::is_same_v<autotype(a), id>)
      return refcast(constant_c<objargtag<O, I>>, a).value();
  };
  template<typename T,auto O, std::size_t I>
  concept can_objarg_cast = requires(T &&t){
    { objarg_cast<O, I>(fwd(t)) } -> std::convertible_to<objarg<O,I>>;
  };

  template<typename T, auto O,std::size_t I>
  concept objarg_init =
    (!objvararg<O,I> && can_objarg_cast<T, O, I>)
    ||(objvararg<O,I> && std::ranges::forward_range<T>
       && can_objarg_cast<std::ranges::range_value_t<T>, O, I>);

  template<auto O, std::size_t I>
  inline constexpr auto objreldat_cast = [](auto &&a){
    if constexpr(requires { objreldat<O,I>{fwd(a)}; })
      return objreldat<O, I>{fwd(a)};
  };
  template<typename T,auto O, std::size_t I>
  concept can_objreldat_cast = requires(T &&t){
    { objreldat_cast<O, I>(fwd(t)) } -> std::convertible_to<objreldat<O,I>>;
  };

  template<typename T, auto O,std::size_t I>
  concept objreldat_init =
    (!objvararg<O,I> && can_objreldat_cast<T, O, I>)
    ||(objvararg<O,I> && std::ranges::forward_range<T> 
       && can_objreldat_cast<std::ranges::range_reference_t<T>, O, I>);

  template<auto O,std::size_t I>
  inline constexpr auto objreldat_default_init = []{
    if constexpr(objvararg<O,I>)
      return views::repeat(objreldat<O,I>{});
    else
      return objreldat<O,I>{};
  }();

}

template<auto O,std::size_t I>
using objarg_init_t = std::conditional_t<objvararg<O,I>,std::initializer_list<id>,id>;

template<auto O,std::size_t I>
using objarg_single_init_t = id;

namespace _{
  
  template<typename objtag, typename reltag, objtag O, std::size_t J>
  struct relfact{

    static constexpr std::size_t N = objnum<O>;

    static constexpr auto reladd(syntax<objtag, reltag> &syn,
                                 sym<O> obj,objarg<O,J> arg,objreldat<O, J> &&rel_d = objreldat<O, J>{}){
      constexpr auto R = objreltag<O, J>;
      auto obj_v = objvert(syn, obj);
      auto arg_v = objvert(syn, arg);
      auto rel_i = symcount(syn, constant_c<R>);
      auto rel_e = syn.graph.edge(obj_v, arg_v, id{etoi(R), rel_i});
      syn.reledges[etoi(R)].push_back(rel_e);
      if constexpr(!empty_symdat<R>)
        kumi::get<etoi(R)>(syn.reldats).push_back(std::move(rel_d));
      return sym<R>{rel_i};
    }

    static constexpr auto relsadd(syntax<objtag, reltag> &syn,
                                  sym<O> obj,
                                  objarg_init<O, J> auto &&args,
                                  objreldat_init<O, J> auto &&reldats){

      if constexpr(objvararg<O, J>)
        for(auto &&[arg,reldat]:std::views::zip(args, reldats))
          reladd(syn, obj, objarg_cast<O, J>(arg), objreldat_cast<O, J>(fwd(reldat)));
      else
        reladd(syn, obj, objarg_cast<O, J>(args), objreldat_cast<O, J>(fwd(reldats)));

    }

    static constexpr auto operator()(syntax<objtag,reltag> &syn,
                                     sym<O> obj,
                                     objarg_init<O, J> auto &&arg_init)
    requires (N >= 1 && empty_symdat<objreltag<O, J>>){
      relsadd(syn, obj, fwd(arg_init), objreldat_default_init<O,J>);
    }
    static constexpr auto operator()(syntax<objtag,reltag> &syn,
                                     sym<O> obj,
                                     objarg_init_t<O, J> arg_init)
    requires (N >= 1 && empty_symdat<objreltag<O, J>>){
      relsadd(syn, obj, std::move(arg_init), objreldat_default_init<O,J>);
    }

  };

}

template<typename objtag,typename reltag, objtag O,std::size_t J>
constexpr _::relfact<objtag, reltag, O, J> reladd{};

namespace _{
  template<
    typename objtag, typename reltag, objtag O,
    typename I = objseq<O>,
    typename I_empty = objseq_empty<O>>
  struct objfact;
  
  template<typename objtag, typename reltag, objtag O,std::size_t... I, std::size_t... I_empty>
  struct objfact<objtag, reltag, O, std::index_sequence<I...>, std::index_sequence<I_empty...>>{

    static constexpr auto I_last = sizeof...(I_empty);

    static constexpr auto grammar = objgram_of<O>;

    static constexpr auto objadd(syntax<objtag, reltag> &syn,symdat_t<O> &&obj_d = symdat_t<O>{}){
      auto obj_i = symcount(syn, constant_c<O>);
      auto obj_v = syn.graph.vertex(static_cast<natural>(etoi(O)), obj_i);
      syn.objverts[etoi(O)].push_back(obj_v);
      if constexpr(!empty_symdat<O>)
        kumi::get<etoi(O)>(syn.objdats).push_back(std::move(obj_d));
      return sym<O>{obj_i};
    }

    static constexpr auto operator()(syntax<objtag,reltag> &syn,
                                     symdat_t<O> obj_d = {})
      requires ((sizeof...(I) == 0 || (sizeof...(I) == 1 && objvar<O>)))
    { return objadd(syn, std::move(obj_d)); }

    //FIXME: add support for non-empty reldats?
    static constexpr auto operator()(syntax<objtag,reltag> &syn,
                                     objarg_init<O,I> auto &&...arg_init)
      requires (empty_symdat<O> && (sizeof...(I) >= 1)
                && (empty_symdat<objreltag<O, I>> && ...))
    {
      auto obj = objadd(syn);
      (reladd<objtag, reltag, O, I>(syn, obj, fwd(arg_init)), ...);
      return obj;
    }
    static constexpr auto operator()(syntax<objtag,reltag> &syn,
                                     symdat_t<O> obj_d,
                                     objarg_init<O, I> auto &&...arg_init)
      requires ((sizeof...(I) >= 1)
                && (empty_symdat<objreltag<O, I>> && ...))
    {
      auto obj = objadd(syn,std::move(obj_d));
      (reladd<objtag, reltag, O, I>(syn, obj, fwd(arg_init)),...);
      return obj;
    }

    //FIXME: add support for non-empty reldats?
    static constexpr auto operator()(syntax<objtag,reltag> &syn,
                                     objarg_init_t<O,I> ...arg_init)
      requires (empty_symdat<O> && (sizeof...(I) >= 1)
                && (empty_symdat<objreltag<O, I>> && ...))
    {
      auto obj = objadd(syn);
      (reladd<objtag, reltag, O, I>(syn, obj, std::move(arg_init)), ...);
      return obj;
    }
    static constexpr auto operator()(syntax<objtag,reltag> &syn,
                                     symdat_t<O> obj_d,
                                     objarg_init_t<O, I> ...arg_init)
      requires ((sizeof...(I) >= 1)
                && (empty_symdat<objreltag<O, I>> && ...))
    {
      auto obj = objadd(syn,std::move(obj_d));
      (reladd<objtag, reltag, O, I>(syn, obj, std::move(arg_init)),...);
      return obj;
    }

    static constexpr auto operator()(syntax<objtag,reltag> &syn,
                                     objarg_init_t<O,I_empty> ... arg_init)
      requires (objvar<O>
                && (sizeof...(I_empty) >= 1)
                && empty_symdat<O>
                && (empty_symdat<objreltag<O, I>> && ...))
    {
      auto obj = objadd(syn);
      (reladd<objtag, reltag, O, I_empty>(syn, obj, std::move(arg_init)), ...);
      return obj;
    }
    static constexpr auto operator()(syntax<objtag,reltag> &syn,
                                     symdat_t<O> obj_d,
                                     objarg_init_t<O,I_empty> ... arg_init)
      requires (objvar<O> 
                && (sizeof...(I_empty) >= 1)
                && (empty_symdat<objreltag<O, I>> && ...))
    {
      auto obj = objadd(syn,std::move(obj_d));
      (reladd<objtag, reltag, O, I_empty>(syn, obj, std::move(arg_init)), ...);
      return obj;
    }

    static constexpr auto operator()(syntax<objtag,reltag> &syn,
                                     objarg_single_init_t<O,I_empty> ... arg_init,
                                     objarg_single_init_t<O,I_last> arg_init_last)
      requires (objvar<O>
                && empty_symdat<O> && (sizeof...(I) >= 1)
                && (empty_symdat<objreltag<O, I>> && ...))
    {
      auto obj = objadd(syn);
      (reladd<objtag, reltag, O, I_empty>(syn, obj, std::move(arg_init)), ...);
      reladd<objtag, reltag, O, I_last>(syn, obj, std::views::single(std::move(arg_init_last)));
      return obj;
    }
    static constexpr auto operator()(syntax<objtag,reltag> &syn,
                                     symdat_t<O> obj_d,
                                     objarg_single_init_t<O,I_empty> ... arg_init,
                                     objarg_single_init_t<O,I_last> arg_init_last)
      requires (objvar<O>
                && (sizeof...(I) >= 1)
                && (empty_symdat<objreltag<O, I>> && ...))
    {
      auto obj = objadd(syn,std::move(obj_d));
      (reladd<objtag, reltag, O, I_empty>(syn, obj, std::move(arg_init)), ...);
      reladd<objtag, reltag, O, I_last>(syn, obj, std::views::single(std::move(arg_init_last)));
      return obj;
    }
  };
}

template<typename objtag,typename reltag,objtag O>
constexpr _::objfact<objtag, reltag, O> objadd{};

template<typename T_root>
std::optional<T_root> walk(concepts::syntax auto &&syntax,auto pre,auto post,id root){
  auto &graph = unwrap(syntax).graph;
  auto root_vertex = objvert(syntax, root);
  //FIXME: crap - jeh
  if(!pre(root))
    return std::nullopt;
  if(graphs::is_oskin(graph,root_vertex))
    return post(root);
  return graphs::dwalk(
    co_get<T_root>{},
    graphs::out_tag,
    std::cref(graph),
    root_vertex,
    [&](auto dw_get, auto dw_event, auto dw_ref){

      auto &dw = unwrap(dw_ref);

      constexpr auto is_root = constant_c<dw_event == any_of(graphs::dw::start,graphs::dw::stop)>;
      constexpr auto is_pre = constant_c<dw_event == any_of(graphs::dw::start,graphs::dw::down)>;

      auto &func = [&]-> auto &{
        if constexpr(is_pre)
          return pre;
        else
          return post;
      }();

      const auto o = objid(syntax,is_root ? dw.root : dw.tail());
      const auto o_head = objid(syntax,is_root ? dw.root : dw.head());

      if constexpr(!is_root){
        auto a = relid(syntax,dw.edge());
        if constexpr(is_pre){
          const bool o_run = invoke(func,o,a,o_head);
          return co::ret<1>(dw_get, !o_run ? graphs::dw::right : graphs::dw::down);
        }
        else{
          invoke(func,o,a,o_head);
          return co::nil<1>(dw_get);
        }
      }
      else{
        if constexpr(is_pre)
          return co::nil<1>(dw_get);
        else
          return co::ret<0>(dw_get,invoke(func,o));
      }
  })();
}

}
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

using id = grammars::id;
using grammars::sym;
using grammars::fam;

template<typename objtag, typename reltag>
using syntax = grammars::syntax<objtag, reltag>;

}
template<auto S>
struct std::hash<hysj::_HYSJ_VERSION_NAMESPACE::grammars::sym<S>>{
  constexpr std::size_t operator()(
    const hysj::_HYSJ_VERSION_NAMESPACE::grammars::sym<S> &i)const{
    auto [a,b] = i.id();
    return (a + b) * (a + b + 1) / 2 + a;
  }
};
template<auto F>
struct std::hash<hysj::_HYSJ_VERSION_NAMESPACE::grammars::fam<F>>{
  constexpr std::size_t operator()(
    const hysj::_HYSJ_VERSION_NAMESPACE::grammars::fam<F> &i)const{
    auto [a,b] = i.id();
    return (a + b) * (a + b + 1) / 2 + a;
  }
};
template<auto S>
struct fmt::formatter<hysj::_HYSJ_VERSION_NAMESPACE::grammars::sym<S>>{
  using T = hysj::_HYSJ_VERSION_NAMESPACE::grammars::sym<S>;
  constexpr auto parse(format_parse_context& context)const{
    auto it  = context.begin(),
         end = context.end();
    if(it != end && *it != '}')
      throw fmt::format_error("invalid format");
    return it;
  }
  auto format(const T &e,format_context &context)const{
    return fmt::format_to(context.out(),"{0},{1}",S,e.idx);
  }
};
template<auto F>
struct fmt::formatter<hysj::_HYSJ_VERSION_NAMESPACE::grammars::fam<F>>{
  using T = hysj::_HYSJ_VERSION_NAMESPACE::grammars::fam<F>;
  constexpr auto parse(format_parse_context& context)const{
    auto it  = context.begin(),
         end = context.end();
    if(it != end && *it != '}')
      throw fmt::format_error("invalid format");
    return it;
  }
  auto format(const T &e,format_context &context)const{
    return fmt::format_to(context.out(),"{0},{1}",F,e.obj);
  }
};
#include<hysj/tools/epilogue.hpp>
