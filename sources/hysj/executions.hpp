#pragma once
#include<hysj/executions/continuous.hpp>
#include<hysj/executions/continuous/fe.hpp>
#include<hysj/executions/continuous/be.hpp>
#include<hysj/executions/discrete.hpp>
#include<hysj/executions/hybrid.hpp>
