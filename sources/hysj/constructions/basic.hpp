#pragma once
#include<cstdint>
#include<ranges>
#include<span>

#include<hysj/codes/sets.hpp>
#include<hysj/devices/device.hpp>
#include<hysj/executions/buffers.hpp>
#include<hysj/tools/ranges/fold.hpp>
#include<hysj/tools/ranges/map.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/reflection.hpp>
#include<hysj/tools/types.hpp>
#include<hysj/tools/utility.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::constructions{

constexpr auto make_ring(std::int64_t begin,std::int64_t size,std::int64_t capacity){
  return views::map(
    views::indices(size),
    [=](auto index){
      return (begin + index) % capacity;
    });
}
using ring = decltype(make_ring({}, {}, {}));

//FIXME: contraint Exec, Exe, automaton.parameters -> 
template<typename Execution, typename Process>
struct basic_run{

  Execution *exec;
  cells *mem;
  Process *exe;

  std::int64_t *ring_begin = mem->i64(exec->buffer.begin).data(),
    *ring_size = mem->i64(exec->buffer.size).data(),
    ring_capacity = exec->buffer.capacity;

  friend HYSJ_MIRROR_STRUCT(basic_run,(exec, mem, exe, ring_begin, ring_size, ring_capacity));

  template<set D, natural W>
  auto param(constant_t<D> d,constant_t<W> w,std::size_t i){
    return mem->get(d, w, exec->automaton.parameters[i]);
  }
  #define HYSJ_LAMBDA(ids,ch,...) \
    template<natural W>           \
    auto ch##param(constant_t<W> w,std::size_t i){ return param(codes::ids##_c, w, i); }
  HYSJ_MAP_LAMBDA(HYSJ_CODES_SETS)
  #undef HYSJ_LAMBDA

  auto frame_size(id i)const{
    const auto &shape = mem->shape(i);
    const auto e = ranges::prod(std::views::drop(shape, 1));
    return e;
  }
  template<typename C>
  auto frames(id i,std::span<C> c)const{
    const auto n = exec->buffer.capacity;
    const auto e = frame_size(i);
    return views::map(
      views::indices(n),
      [=](auto f){
        return c.subspan(e*f, e);
      });
  }

  auto ring(){
    HYSJ_ASSERT(*ring_size >= 0);
    HYSJ_ASSERT(*ring_size <= ring_capacity);
    return make_ring(*ring_begin ,*ring_size, ring_capacity);
  }

};

} //hysj::constructions
#include<hysj/tools/epilogue.hpp>

