#pragma once
#include<cstdint>
#include<concepts>
#include<type_traits>
#include<utility>

#include<hysj/automata/discrete.hpp>
#include<hysj/codes/code.hpp>
#include<hysj/constructions/basic.hpp>
#include<hysj/devices/device.hpp>
#include<hysj/executions/discrete.hpp>
#include<hysj/tools/ranges/map.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/reflection.hpp>
#include<hysj/tools/type_traits.hpp>
#include<hysj/tools/types.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::constructions::discrete{

template<typename Exe>
struct run : basic_run<dexec, Exe>{

  using Base = basic_run<dexec, Exe>;
  
  std::span<icell64> events = this->mem->i64(this->exec->status);
  devent last_event = devent::stop;

  friend HYSJ_MIRROR_STRUCT(run,(events, last_event)(Base));

  void init(){
    this->exe->run(this->exec->put_state);
    this->exe->run(this->exec->put_parameters);
  }
  void put(){
    this->exe->run(this->exec->put_state);
  }
  void get(){
    this->exe->run(this->exec->get_state);
    this->exe->run(this->exec->transitions.get);
  }

  template<natural W>
  auto time(constant_t<W> w){
    auto t = this->exec->state.buffer.independent;
    return this->frames(t, this->mem->i(w, t));
  }
  template<set D, natural W>
  auto state(constant_t<D> d,constant_t<W> w,std::size_t i,std::size_t j){
    auto x = this->exec->state.buffer.dependent[i][j];
    return this->frames(x, this->mem->get(d, w, x));
  }
  template<set D, natural W>
  auto state(constant_t<D> d,constant_t<W> w,std::size_t i){
    return views::map(
      views::indices(this->exec->state.buffer.dependent[i]),
      [&,d,w,i](auto j){
        return state(d,w,i,j);
      });
  }

  template<set D, natural W>
  auto state(constant_t<D> d,constant_t<W> w){
    return views::map(
      views::indices(this->exec->state.buffer.dependent),
      [&, d, w](auto i){
        return state(d,w,i);
      });
  }
  #define HYSJ_LAMBDA(ids,ch,...)                                                                         \
    template<natural W>                                                                                   \
    auto ch##state(constant_t<W> w,std::size_t i,std::size_t j){ return state(codes::ids##_c, w, i, j); } \
    template<natural W>                                                                                   \
    auto ch##state(constant_t<W> w,std::size_t i){ return state(codes::ids##_c, w, i); }                  \
    template<natural W>                                                                                   \
    auto ch##state(constant_t<W> w){ return state(w, codes::ids##_c); }
  HYSJ_MAP_LAMBDA(HYSJ_CODES_SETS)
  #undef HYSJ_LAMBDA

  template<natural W>
  auto transition_guard(constant_t<W> w,std::size_t i){
    auto o = this->exec->transitions.container[i].guard;
    return this->mem->b(w, o);
  }
  template<set D,natural W>
  auto transition_function(constant_t<D> d,constant_t<W> w,std::size_t i){
    auto o = this->exec->transitions.container[i].function;
    return this->mem->get(d, w, o);
  }
  #define HYSJ_LAMBDA(ids,ch,...)                                                                                    \
    template<natural W>                                                                                              \
    auto ch##transition_function(constant_t<W> w,std::size_t i){ return transition_function(codes::ids##_c, w, i); }
  HYSJ_MAP_LAMBDA(HYSJ_CODES_SETS)
  #undef HYSJ_LAMBDA

  auto event(std::int64_t i)const{
    HYSJ_ASSERT(0 <= i);
    HYSJ_ASSERT(i < this->ring_capacity);
    return devent{static_cast<etoi_t<devent>>(events[i])};
  }

  operator bool()const noexcept{
    return last_event != devent::stop;
  }
  auto operator()(){
    static constexpr auto tasks = std::array{
      &dexec::resume,
      &dexec::start,
    };
    HYSJ_ASSERT(etoi(last_event) < tasks.size());
    this->exe->run(std::invoke(tasks[etoi(last_event)], this->exec));
    auto r = this->ring();
    last_event = event(ranges::last(r));
    return r;
  }

};

template<typename Exe>
auto cc(dexec &exec,cells &mem,Exe &exe){
  return run<Exe>{
    {
      &exec,
      &mem,
      &exe
    }
  };
}

} //hysj::constructions::discrete
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

namespace druns = constructions::discrete;

template<typename Exe>
using drun = druns::run<Exe>;

} //hysj
#include<hysj/tools/epilogue.hpp>
