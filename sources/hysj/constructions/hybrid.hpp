#pragma once

#include<algorithm>
#include<functional>
#include<optional>
#include<utility>

#include<hysj/automata.hpp>
#include<hysj/codes.hpp>
#include<hysj/constructions/continuous.hpp>
#include<hysj/constructions/discrete.hpp>
#include<hysj/devices/device.hpp>
#include<hysj/executions/hybrid.hpp>
#include<hysj/tools/functional/compose.hpp>
#include<hysj/tools/ranges/cat.hpp>
#include<hysj/tools/ranges/map.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/reflection.hpp>
#include<hysj/tools/type_traits.hpp>
#include<hysj/tools/types.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::constructions::hybrid{

template<typename HExe, typename CEnv>
struct run {

  using CExe = devs::exe_t<CEnv &>;
  using DRun = druns::run<HExe>;
  using CRun = cruns::run<CExe>;

  hexec *exec;
  cells *mem;
  HExe *exe;
  CEnv *cenv;
  
  std::optional<DRun> discrete_construction = std::nullopt;

  std::optional<cexecs::execution> continuous_execution = std::nullopt;
  std::optional<codes::taskfam> continuous_root_check = std::nullopt;
  std::optional<CExe> continuous_exe = std::nullopt;
  std::optional<CRun> continuous_construction = std::nullopt;

  friend HYSJ_MIRROR_STRUCT(
    run,
    (exec, mem, exe, cenv, discrete_construction, continuous_execution,
     continuous_root_check, continuous_exe, continuous_construction)
    ());

  constexpr auto &crun(){ return *continuous_construction; }
  constexpr auto &cexe(){ return *continuous_exe; }
  constexpr auto &cexec(){ return *continuous_execution; }
  constexpr auto &drun(){ return *discrete_construction; }

  template<natural W>
  auto discrete_time(constant_t<W> w){
    return mem->i(w, exec->discrete_execution.state.buffer.independent);
  }
  template<set D,natural W>
  auto discrete_state(constant_t<D> d,constant_t<W> w,std::size_t i,std::size_t j){
    return mem->get(d, w, exec->discrete_execution.state.buffer.dependent[i][j]);
  }
  template<set D,natural W>
  auto discrete_state(constant_t<D> d,constant_t<W> w,std::size_t i){
    return views::map(
      views::indices(exec->discrete_execution.state.buffer.dependent[i]),
      [&,i,d,w](auto j){
        return discrete_state(d,w,i,j);
      });
  }
  template<set D,natural W>
  auto discrete_state(constant_t<D> d,constant_t<W> w){
    return views::map(
      views::indices(exec->discrete_execution.state.buffer.dependent),
      [&,d,w](auto i){
        return discrete_state(d,w,i);
      });
  }
  #define HYSJ_LAMBDA(ids,ch,...)                                           \
    template<natural W>                                                     \
    auto discrete_##ch##state(constant_t<W> w,std::size_t i,std::size_t j){ \
      return discrete_state(codes::ids##_c, w, i, j); }                     \
    template<natural W>                                                     \
    auto discrete_##ch##state(constant_t<W> w,std::size_t i){               \
    return discrete_state(codes::ids##_c, w, i); }                          \
    template<natural W>                                                     \
    auto discrete_##ch##state(constant_t<W> w){                             \
      return discrete_state(codes::ids##_c, w);                             \
    }
  HYSJ_MAP_LAMBDA(HYSJ_CODES_SETS)
  #undef HYSJ_LAMBDA

  template<natural W>
  auto continuous_time(constant_t<W> w){
    return mem->r(w, exec->continuous_execution.state.buffer.independent);
  }
  template<natural W>
  auto continuous_state(constant_t<W> w,std::size_t i,std::size_t j){
    return mem->r(w, exec->continuous_execution.state.buffer.dependent[i][j]);
  }
  template<natural W>
  auto continuous_state(constant_t<W> w,std::size_t i){
    return views::map(
      views::indices(exec->continuous_execution.state.buffer.dependent[i]),
      [&,w,i](auto j){
        return continuous_state(w,i,j);
      });
  }
  template<natural W>
  auto continuous_state(constant_t<W> w){
    return views::map(
      views::indices(exec->continuous_execution.state.buffer.dependent),
      [&,w](auto i){
        return continuous_state(w,i);
      });
  }

  template<set D,natural W>
  auto param(constant_t<D> d,constant_t<W> w,std::size_t i){
    return mem->get(d, w, exec->parameters[i]);
  }
  #define HYSJ_LAMBDA(ids,ch,...)                                     \
    auto ch##param(std::size_t i){ return param(codes::ids##_c, i); }
  HYSJ_MAP_LAMBDA(HYSJ_CODES_SETS)
  #undef HYSJ_LAMBDA

  template<set D,natural W>
  auto continuous_param(constant_t<D> d,constant_t<W> w,std::size_t i){
    return mem->get(d, w, exec->continuous_execution.parameters[i]);
  }
  #define HYSJ_LAMBDA(ids,ch,...)                                       \
    template<natural W>                                                 \
    auto continuous_##ch##param(constant_t<W> w,std::size_t i){         \
      return continuous_param(codes::ids##_c, w,i);                     \
    }
  HYSJ_MAP_LAMBDA(HYSJ_CODES_SETS)
  #undef HYSJ_LAMBDA

  template<natural W>
  auto activity_guard(constant_t<W> w,std::size_t i){
    return mem->b(w, exec->activities.container[i].guard);
  }
  template<natural W>
  auto activity_guard(constant_t<W> w){
    return views::map(
      views::indices(exec->activities.container),
      [&,w](auto i){
        return activity_guard(w,i);
      });
  }
  template<natural W>
  auto root_guard(constant_t<W> w,std::size_t i){
    return mem->b(w, exec->roots.container[i].guard);
  }
  template<natural W>
  auto root_guard(constant_t<W> w){
    return views::map(
      views::indices(exec->roots.container),
      [&,w](auto i){
        return root_guard(w,i);
      });
  }
  template<natural W>
  auto action_guard(constant_t<W> w,std::size_t i){
    return mem->b(w, exec->actions.container[i].guard);
  }
  template<natural W>
  auto action_guard(constant_t<W> w){
    return views::map(
      views::indices(exec->actions.container),
      [&,w](auto i){
        return action_guard(w,i);
      });
  }

  template<natural W>
  auto active_activities(constant_t<W> w){
    return
      views::keep(views::indices(exec->activities.container), //FIXME: consider scalar - jeh
                  [this,w](auto i){
                    return std::ranges::any_of(
                      activity_guard(w, i),
                      [&](auto g){
                        return g == 1;
                      });
                  });
  }
  template<natural W>
  auto active_roots(constant_t<W> w){
    return views::keep(views::indices(exec->roots.container), //FIXME: consider scalar - jeh
                       [&,w](auto i){
                         return std::ranges::any_of(
                           root_guard(w,i),
                           [&](auto g){
                             return g == 1;
                           });
                       });
  }

  void init(){
    exe->run(exec->init);
  }
  void put(){
    exe->run(exec->put_state);
  }
  void get(){
    exe->run(exec->get_state);
  }

  void make_crun(){

    exe->run(exec->check);
    exe->run(exec->get_guards);

    {
      auto &hcexec = exec->continuous_execution;

      auto ccode = exe->code;

      auto cequations =
        compose<>(ranges::vec,
                  bind<-1>(views::map, &hexecs::activity::equation),
                  views::at)
        (exec->activities.container, active_activities(64_N));

      auto croots =
        compose<>(ranges::vec,
                  bind<-1>(views::map, &hexecs::root::continuous),
                  views::at)
        (exec->roots.container, active_roots(64_N));

      continuous_execution.emplace(cexecs::fe(ccode,
                                              hcexec.config,
                                              hcexec.buffer,
                                              hcexec.events,
                                              hcexec.status,
                                              hcexec.state,
                                              std::move(cequations),
                                              std::move(croots),
                                              hcexec.parameters,
                                              exec->roots.event));
      continuous_root_check = codes::task(
        codes::then(ccode,
                    {
                      codes::on(ccode, hcexec.config.dev, cexec().solve),
                      cexec().get_events
                    }));
      auto capi = codes::api(ccode, 
                 {
                   cexec().api,
                   *continuous_root_check,
                   exec->continuous_execution.put_discrete_state
                 });
      continuous_exe.emplace(cenv->load(*mem, std::move(ccode), capi));
    }
    continuous_construction.emplace(
      CRun{
        &*continuous_execution,
        mem,
        &*continuous_exe
      });
    cexe().run(cexec().init);
  }

  void make_drun(){
    discrete_construction.emplace(
      DRun{
        &exec->discrete_execution,
        mem,
        exe
      });
    exe->run(exec->discrete_execution.put_parameters);
  }
  void cput(){
    cexe().run(cexec().init);
    cexe().run(exec->continuous_execution.put_discrete_state);
  }
  void cget(){
    cexe().run(cexec().get_state);
    cexe().run(cexec().roots.get);
    put();
  }

  void put_hroot_variables(){
    exe->run(exec->roots.put_state);
  }
 
  bool check_actions(){
    cexe().run(cexec().roots.get);
    exe->run(exec->roots.put_state);
    exe->run(exec->actions.check);
    exe->run(exec->actions.get_event);
    return mem->b64(exec->actions.event)[0]; //FIXME: brittle - jeh
  }
  void apply_actions(){
    exe->run(exec->actions.apply);
  }

  cevent croot_check(){
    crun().exe->run(*continuous_root_check);
    return crun().last_event;
  } 

  template<typename CoGet>
  co::api_t<CoGet> operator()(CoGet,auto control)&{

    static constexpr integer co_hdepth = co::depth_v<CoGet>;

    auto &co_hthis = HYSJ_CO(CoGet);

    auto htransfer =
      [&](auto event){
        return control(co::get<none>(co_hthis),event);
      };

    make_drun();
    make_crun();
    
  step:
    co_await htransfer(hexecs::step_tag);
    { //cconstruction
      cput();
      cevent event = cevent::step;
      while(event == cevent::step)
        for(auto index:crun()()){
          event = crun().event(index);
          co_await with_enum(bind<0, -1>(control, co::get<none>(co_hthis), index), event);
        }
      cget();
      if(event == cevent::root)
        put_hroot_variables();
    }
    while(1){ //hconstruction
      bool dcontinue = false;
      {
        devent event = devent::step;
        while(event == devent::step)
          for(auto index:drun()()){
            event = drun().event(index);
            dcontinue |= (event == devent::step);
            co_await with_enum(bind<0, -1>(control, co::get<none>(co_hthis), index),event);
          }
      }
      {  //aconstruction
        cevent event = cevent::root;
        do{
          if(!check_actions())
            break;
          apply_actions();
          get();
          cput(); //should this happen with updated crun? 
          event = croot_check();
          cget();
          co_await htransfer(hexecs::action_tag);
        }while(event == cevent::root);
      }
      if(!dcontinue)
        break;
    }
    make_crun();
    goto step;
  }

};

auto cc(hexec &exec,cells &mem,auto &exe,auto &cenv){
  return run{
    &exec, &mem, &exe, &cenv
  };
}

} //hysj::constructions::hybrid
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

namespace hruns = constructions::hybrid;

template<typename HExe, typename CEnv>
using hrun = hruns::run<HExe, CEnv>;

}
#include<hysj/tools/epilogue.hpp>
