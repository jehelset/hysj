#pragma once
#include<utility>

#include<hysj/codes/code.hpp>
#include<hysj/codes/cells.hpp>
#include<hysj/automata/continuous.hpp>
#include<hysj/executions/continuous.hpp>
#include<hysj/constructions/basic.hpp>
#include<hysj/constructions/continuous.hpp>
#include<hysj/devices/device.hpp>
#include<hysj/tools/functional/none_of.hpp>
#include<hysj/tools/ranges/cat.hpp>
#include<hysj/tools/ranges/map.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/reflection.hpp>
#include<hysj/tools/type_traits.hpp>
#include<hysj/tools/types.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::constructions::continuous{

template<typename Exe>
struct run : basic_run<cexec, Exe> {

  using Base = basic_run<cexec, Exe>;

  std::span<icell64> events = this->mem->i64(this->exec->status);
  cevent last_event = cevent::stop;

  friend HYSJ_MIRROR_STRUCT(run,(events, last_event)(Base));

  void init(){
    this->exe->run(this->exec->init);
  }
  void put(){
    this->exe->run(this->exec->put_state);
  }
  void get(){
    this->exe->run(this->exec->get_state);
  }

  template<natural W>
  auto time(constant_t<W> w){
    auto t = this->exec->state.buffer.independent;
    return this->frames(t, this->mem->r(w, t));
  }
  template<natural W>
  auto state(constant_t<W> w,std::size_t i,std::size_t j){
    auto x = this->exec->state.buffer.dependent[i][j];
    return this->frames(x, this->mem->r(w, x));
  }
  template<natural W>
  auto state(constant_t<W> w,std::size_t i){
    return views::map(
      views::indices(this->exec->state.buffer.dependent[i]),
      [&,w,i](auto j){
        return state(w,i,j);
      });
  }

  template<natural W>
  auto state(constant_t<W> w){
    return views::map(
      views::indices(this->exec->state.buffer.dependent),
      [&, w](auto i){
        return state(w,i);
      });
  }

  void get_roots(){
    this->exe->run(this->exec->roots.get);
  }
  void put_roots(){
    this->exe->run(this->exec->roots.put);
  }
  template<natural W>
  auto root_residual(constant_t<W> w,std::size_t i){
    //FIXME: handle - jeh
    return this->mem->r(w, codes::accessed(this->exe.code, this->exec->automaton.roots[i]).value());
  }
  auto root_sign(std::size_t i){
    //FIXME: handle - jeh
    return this->mem->i64(codes::accessed(this->exe.code, this->exec->roots.container[i].sign).value());
  }
  auto root_sign_flip(std::size_t i){
    return this->mem->b64(this->exec->roots.container[i].sign_flip);
  }
  auto root_event(){
    return this->mem->b64(this->exec->roots.event);
  }

  auto event(std::int64_t i)const{
    HYSJ_ASSERT(0 <= i);
    HYSJ_ASSERT(i < this->ring_capacity);
    return cevent{static_cast<etoi_t<cevent>>(events[i])};
  }

  operator bool()const noexcept{
    return last_event == none_of(cevent::stop, cevent::fail);
  }
  auto operator()(){
    static constexpr auto tasks = std::array{
      &cexec::resume,
      &cexec::resume,
      &cexec::start,
      &cexec::start
    };
    HYSJ_ASSERT(etoi(last_event) < tasks.size());
    this->exe->run(std::invoke(tasks[etoi(last_event)], this->exec));
    auto r = this->ring();
    last_event = event(ranges::last(r));
    return r;
  }
};

template<typename Exe>
auto cc(cexec &exec, cells &mem, Exe &exe){
  return run<Exe>{
    &exec,
    &mem,
    &exe
  };
}

} //hysj::constructions::continuous
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

namespace cruns = constructions::continuous;

template<typename Exe>
using crun = cruns::run<Exe>;

} //hysj
#include<hysj/tools/epilogue.hpp>
