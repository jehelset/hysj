#pragma once

#include<hysj/codes/code.hpp>
#include<hysj/tools/reflection.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::numerics{

struct fixed_point_iteration_function{
  codes::exprfam variable, function, maxiter;

  codes::varsym status, last_function, residual;
  codes::exprfam fail;

  friend HYSJ_MIRROR_STRUCT(fixed_point_iteration_function,(variable, function, maxiter, status, last_function, residual, fail));

  auto operator<=>(const fixed_point_iteration_function &) const = default;
};

HYSJ_EXPORT
fixed_point_iteration_function fp_func(code &code,
                                       codes::exprfam function,
                                       codes::exprfam variable,
                                       codes::exprfam maxiter);

struct fixed_point_iteration : fixed_point_iteration_function{
  codes::taskfam init, eval, test_brk, step, fail_brk, loop, run;

  friend HYSJ_MIRROR_STRUCT(fixed_point_iteration,(init, eval, test_brk, step, fail_brk, loop, run)(fixed_point_iteration_function));

  auto operator<=>(const fixed_point_iteration &) const = default;
};

HYSJ_EXPORT
fixed_point_iteration fp(code &code,
                         fixed_point_iteration_function,
                         codes::exprfam test);

}
#include<hysj/tools/epilogue.hpp>
