#include<hysj/numerics/newtons_method.hpp>

#include<hysj/codes/code.hpp>
#include<hysj/linear_algebra/gaussian_elimination.hpp>
#include<hysj/codes/grammar.hpp>
#include<hysj/codes/factories.hpp>
#include<hysj/codes/compile.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::numerics{

newtons_method_function nm_func(code &code,
                                codes::exprfam function,
                                codes::exprfam derivative,
                                codes::exprfam variable,
                                codes::exprfam step_size,
                                codes::exprfam maxiter){
  using namespace codes::factories;

  codes::compile(code, std::array{function, variable, derivative});

  //TODO: proper shape and type check - jeh
  const auto function_rank = orank(code, function);
  const auto variable_rank = orank(code, variable);
  const auto derivative_rank = orank(code, derivative);
  HYSJ_ASSERT(variable_rank < 2);
  HYSJ_ASSERT(function_rank < 2);
  HYSJ_ASSERT(derivative_rank < 3);

  //FIXME: bad - jeh
  const auto i = variable_rank == 1
    ? codes::itrcast(oitrs(code, variable)[0]).value()
    : codes::itr(code, codes::oone(code, variable))
    ;
  const auto j = [&]{
    if(derivative_rank != 2)
      return itr_dup(code, i);
    auto itrs = oitrs(code, derivative);
    if(itrs[0] == i.id())
      return codes::itrcast(itrs[1]).value();
    return codes::itrcast(itrs[0]).value();
  }();
  HYSJ_ASSERT(i != j);

  //FIXME: support arbitrary dimensions - jeh
  newtons_method_function nm{
    .variable           = variable,
    .function           = function,
    .maxiter            = maxiter,
    .status             = ivar32(code),
    .residual           = tmp_for(code, function, set::reals, {i}),
    .fail               = geq(code, nm.status, maxiter),
    .derivative         = derivative,
    .newton_direction   = tmp_for(code, variable, {i}),
    .current_derivative = tmp_for(code, derivative, set::reals, {j, i}),
    .step_size          = step_size,
    .newton_step        = codes::neg(code,
                                     codes::mul(code, {
                                         nm.step_size,
                                         nm.newton_direction,
                                       }))
  };
  return nm;
}

newtons_method nm(code &code,
                  newtons_method_function nm_func,
                  codes::exprfam test){

  using namespace codes::factories;

  newtons_method nm{{std::move(nm_func)}};

  nm.init = {put(code, nm.status, code.builtins.lits.zero32)};
  nm.eval = {
    put(code, nm.residual, nm.function)
  };
  nm.test_brk = {
    if_(code, test, {code.builtins.brk})
  };
  nm.step = {
    when(code,
         {
           then(code,
                {
                  put(code, nm.current_derivative, nm.derivative),
                  linear_algebra::linear_solve(code, {nm.current_derivative}, {nm.newton_direction}, {nm.residual}),
                  put(code, nm.variable, codes::add(code, {nm.variable, nm.newton_step}))
                }),
           put(code, nm.status, inc(code, nm.status))
         })
  };
  nm.fail_brk = {
    if_(code, nm.fail, {code.builtins.brk})
  };
  nm.loop = {
    loop(
      code,
      then(
        code,{
          nm.eval,
          nm.test_brk,
          nm.step,
          nm.fail_brk
        }))
  };
  nm.run = {
    codes::then(code, {nm.init, nm.loop})
  };
  compile(code, nm.run);
  return nm;
}

}
#include<hysj/tools/epilogue.hpp>
