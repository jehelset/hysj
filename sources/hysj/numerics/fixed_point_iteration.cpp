#include<hysj/numerics/fixed_point_iteration.hpp>

#include<hysj/codes/code.hpp>
#include<hysj/codes/grammar.hpp>
#include<hysj/codes/factories.hpp>
#include<hysj/codes/compile.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::numerics{

fixed_point_iteration_function fp_func(code &code,
                                       codes::exprfam function,
                                       codes::exprfam variable,
                                       codes::exprfam maxiter){
  using namespace codes::factories;
  fixed_point_iteration_function fp{
    .variable      = variable,
    .function      = function,
    .maxiter       = maxiter,
    .status        = ivar32(code),
    .last_function = tmp_dup(code, function),
    .residual      = tmp_dup(code, function),
    .fail          = geq(code, fp.status, maxiter)
  };
  return fp;
}

fixed_point_iteration fp(code &code,
                         fixed_point_iteration_function fp_func,
                         codes::exprfam test){

  using namespace codes::factories;

  fixed_point_iteration fp{{std::move(fp_func)}};

  fp.init = {put(code, fp.status, code.builtins.lits.zero32)};
  fp.eval = {
    then(
      code,
      {
        put(code, fp.last_function, fp.function),
        put(code, fp.residual, sub(code, codes::expr(fp.last_function), codes::expr(fp.variable)))
      })
  };
  fp.test_brk = {
    if_(code, test, {code.builtins.brk})
  };
  fp.step = {
    when(code,
         {
           put(code, fp.variable, fp.last_function),
           put(code, fp.status, inc(code, fp.status))
         })
  };
  fp.fail_brk = {
    if_(code, fp.fail, {code.builtins.brk})
  };
  fp.loop = {
    loop(
      code,
      then(
        code,{
          fp.eval,
          fp.test_brk,
          fp.step,
          fp.fail_brk
        }))
  };
  fp.run = {
    codes::then(code, {fp.init, fp.loop})
  };
  compile(code, fp.run);
  return fp;
}


}
#include<hysj/tools/epilogue.hpp>
