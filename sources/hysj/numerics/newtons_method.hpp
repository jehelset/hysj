#pragma once

#include<hysj/codes/code.hpp>
#include<hysj/tools/reflection.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::numerics{

struct newtons_method_function{
  codes::exprfam variable, function, maxiter;

  codes::varsym status, residual;
  codes::exprfam fail;

  codes::exprfam derivative;

  codes::varsym newton_direction, current_derivative;

  codes::exprfam step_size, newton_step;


  friend HYSJ_MIRROR_STRUCT(newtons_method_function,(variable, function, maxiter,
                                                     status, residual, fail,
                                                     derivative,
                                                     newton_direction, current_derivative, 
                                                     step_size, newton_step));

  auto operator<=>(const newtons_method_function &) const = default;
};

HYSJ_EXPORT
newtons_method_function nm_func(code &code,
                                codes::exprfam function,
                                codes::exprfam derivative,
                                codes::exprfam variable,
                                codes::exprfam step_size,
                                codes::exprfam maxiter);

struct newtons_method : newtons_method_function{
  codes::taskfam init, eval, test_brk, step, fail_brk, loop, run;

  friend HYSJ_MIRROR_STRUCT(newtons_method,(init, eval, test_brk, step, fail_brk, loop, run)(newtons_method_function));

  auto operator<=>(const newtons_method &) const = default;
};

HYSJ_EXPORT
newtons_method nm(code &code,
                  newtons_method_function,
                  codes::exprfam test);

}
#include<hysj/tools/epilogue.hpp>
