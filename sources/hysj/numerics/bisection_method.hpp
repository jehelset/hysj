#pragma once

#include<hysj/codes/code.hpp>
#include<hysj/tools/reflection.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::numerics{

struct bisection_method_function{
  codes::exprfam variable, variable_min, variable_max, function, maxiter;

  codes::varsym status, residual;
  codes::exprfam fail;

  codes::exprfam bracket_which, bracket_left, bracket_right;

  friend HYSJ_MIRROR_STRUCT(bisection_method_function,(variable, variable_min, variable_max, function, maxiter,
                                                       status, residual,
                                                       fail,
                                                       bracket_which, bracket_left, bracket_right));

  auto operator<=>(const bisection_method_function &) const = default;
};

HYSJ_EXPORT
bisection_method_function bsm_func(code &code,
                                   codes::exprfam function,
                                   codes::exprfam variable,
                                   codes::exprfam variable_min,
                                   codes::exprfam variable_max,
                                   codes::exprfam maxiter);

struct bisection_method : bisection_method_function{
  codes::taskfam init, eval, test_brk, step, fail_brk, loop, run;

  friend HYSJ_MIRROR_STRUCT(bisection_method,(init, eval, test_brk, step, fail_brk, loop, run)(bisection_method_function));

  auto operator<=>(const bisection_method &) const = default;
};

HYSJ_EXPORT
bisection_method bsm(code &code,
                     bisection_method_function,
                     codes::exprfam test);

}
#include<hysj/tools/epilogue.hpp>
