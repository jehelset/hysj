#include<hysj/numerics/bisection_method.hpp>

#include<hysj/codes/code.hpp>
#include<hysj/codes/grammar.hpp>
#include<hysj/codes/factories.hpp>
#include<hysj/codes/compile.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::numerics{

bisection_method_function bsm_func(code &code,
                                  codes::exprfam function,
                                  codes::exprfam variable,
                                  codes::exprfam variable_min,
                                  codes::exprfam variable_max,
                                  codes::exprfam maxiter){
  using namespace codes::factories;
  bisection_method_function bsm{
    .variable      = variable,
    .variable_min  = variable_min,
    .variable_max  = variable_max,
    .function      = function,
    .maxiter       = maxiter,
    .status        = ivar32(code, {rfl(code, function)}),
    .residual      = tmp_dup(code, function),
    .fail          = geq(code, bsm.status, maxiter),
    .bracket_which = tmp_dup(code, variable),
    .bracket_left  = tmp_dup(code, variable),
    .bracket_right = tmp_dup(code, variable)
  };
  return bsm;
}

bisection_method bsm(code &code,
                    bisection_method_function bsm_func,
                    codes::exprfam test){

  using namespace codes::factories;

  //FIXME:
  //  add a status mask & only update non-converged / active cells
  //  - jeh

  bisection_method bsm{{std::move(bsm_func)}};

  bsm.init = {
    when(
      code,
      {
        put(code, bsm.status, code.builtins.lits.zero32),
        put(code, bsm.bracket_left, bsm.variable_min),
        put(code, bsm.bracket_right, bsm.variable_max)
      })
  };
  bsm.eval = {
    then(
      code,
      {
        put(code, bsm.residual, bsm.function)
      })
  };
  bsm.test_brk = {
    if_(code, test, {code.builtins.brk})
  };
  bsm.step = {
    then(code,
         {
           put(code, bsm.variable, div(code, add(code, {bsm.bracket_left, bsm.bracket_right}),
                                       codes::otwo(code, bsm.variable))),
           put(code,
               bsm.bracket_which,
               eq(code,
                  sign(code, bsm.variable),
                  sign(code, bsm.bracket_left))),
           when(
             code,
             {
               put(code,
                   bsm.bracket_left,
                   sel(code,
                       bsm.bracket_which,
                       {
                         bsm.bracket_left,
                         bsm.variable
                       })),
               put(code,
                   bsm.bracket_right,
                   sel(code,
                       bsm.bracket_which,
                       {
                         bsm.variable,
                         bsm.bracket_right
                       }))
             }),
           put(code, bsm.status, inc(code, bsm.status))
         })
  };
  bsm.fail_brk = {
    if_(code, bsm.fail, {code.builtins.brk})
  };

  bsm.loop = {
    loop(
      code,
      then(
        code,
        {
          bsm.eval,
          bsm.test_brk,
          put(code, bsm.variable, bsm.bracket_left),
          bsm.eval,
          bsm.test_brk,
          put(code, bsm.variable, bsm.bracket_right),
          bsm.eval,
          bsm.test_brk,
          loop(
            code,
            then(
              code,{
                bsm.eval,
                bsm.test_brk,
                bsm.step,
                bsm.fail_brk
              })),
          code.builtins.brk
        }))
  };
  bsm.run = {
    then(code, {bsm.init, bsm.loop})
  };
  compile(code, bsm.run);
  return bsm;
}

}
#include<hysj/tools/epilogue.hpp>
