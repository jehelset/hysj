#pragma once
#include<cstddef>
#include<coroutine>
#include<concepts>
#include<exception>
#include<memory>
#include<optional>
#include<utility>
#include<type_traits>

#include<hysj/tools/coroutines.hpp>
#include<hysj/tools/functional/always.hpp>
#include<hysj/tools/functional/any_of.hpp>
#include<hysj/tools/functional/ignore.hpp>
#include<hysj/tools/functional/optional.hpp>
#include<hysj/tools/functional/pick.hpp>
#include<hysj/tools/reflection.hpp>
#include<hysj/tools/type_traits.hpp>
#include<hysj/tools/types.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::coroutines::trees{

template<typename T>
struct splicer{};

template<typename Fruit>
inline constexpr bool spliced_v = false;
template<typename Fruit>
inline constexpr bool spliced_v<splicer<Fruit>> = true;

template<typename T>
using splicer_t = std::conditional_t<spliced_v<T>,T,splicer<T>>;

namespace _{
  template<typename Fruit>
  struct unspliced_t_impl{
    using type = Fruit;
  };
  template<typename Fruit>
  struct unspliced_t_impl<splicer<Fruit>>{
    using type = Fruit;
  };
}
template<typename Fruit>
using unspliced_t = typename _::unspliced_t_impl<Fruit>::type;

template<typename... Fruit>
constexpr int depth_v = (int)sizeof...(Fruit);

template<typename... Fruit>
constexpr bool root_v = depth_v<Fruit...> == 1;

namespace _{
  template<int ancestor,int depth>
  consteval int ancestor_v_impl(){
    static_assert(depth > 0);
    if constexpr(ancestor < 0){
      static_assert(depth + ancestor > 0);
      return depth + ancestor;
    }
    else{
      static_assert(depth - ancestor > 0);
      return ancestor;
    }
  }
}

template<int I,int D>
constexpr int ancestor_v = _::ancestor_v_impl<I,D>();

template<int I,int D>
constexpr int put_v =
  []{
    if constexpr(I == 0)
      return D - 1;
    else if constexpr(I > 0)
      return ancestor_v<I - 1,D>;
    else{
      static_assert(D > -I);
      return ancestor_v<D + I,D> - 1;
    };
  }();

template<typename... Fruit>
struct TreeTraits{
  static constexpr bool am_spliced = spliced_v<pick_t<0,Fruit...>>;
  static constexpr int  depth      = depth_v<Fruit...>;

  static_assert(depth > 0);
  static constexpr bool am_root    = depth == 1;
  static constexpr bool am_toproot = am_root && !am_spliced;
};

template<typename... Fruit>
struct TreePromise;

template<typename LFruit,typename... HFruit>
struct Trunk{

  static constexpr TreeTraits<LFruit,HFruit...> traits{};

  using Parent = std::coroutine_handle<
    std::conditional_t<traits.am_root,void,TreePromise<HFruit...>>>;
  using Current = std::conditional_t<traits.am_root,std::coroutine_handle<>,none>;

  Parent parent = []{
    if constexpr(traits.am_root)
      return std::noop_coroutine();
    else
      return Parent{};
  }();
  std::optional<LFruit> stem = std::nullopt;
  bool done = false;
  [[no_unique_address]] Current current{};
};

template<typename LFruit,typename... HFruit>
constexpr auto &root(Trunk<LFruit,HFruit...> &t){
  if constexpr(t.traits.am_root)
    return t;
  else
    return t.parent.promise();
}

template<typename LFruit,typename... HFruit>
struct Tree;

template<bool Spliced,typename T>
using splice_ref_t = std::conditional_t<!Spliced,T,std::reference_wrapper<T>>;

template<bool Spliced,typename T>
auto splice_ref(T &&t){
  if constexpr(Spliced)
    return std::ref(t);
  else
    return fwd(t);
}

namespace _{
  template<typename... Fruit>
  struct trunk_t_impl;
  template<typename LFruit,typename... HFruit>
  struct trunk_t_impl<LFruit,HFruit...>{
    using type = splice_ref_t<spliced_v<LFruit>,Trunk<unspliced_t<LFruit>,HFruit...>>;
  };
}
template<typename... Fruit>
using trunk_t = typename _::trunk_t_impl<Fruit...>::type;

template<typename LFruit,typename... HFruit>
struct Seed{
  
  static constexpr TreeTraits<LFruit,HFruit...> traits{};

  static constexpr int depth = traits.depth;

  using promise_type = TreePromise<LFruit,HFruit...>;

  trunk_t<LFruit,HFruit...> trunk;

  friend 
  constexpr auto &promise(Seed<LFruit,HFruit...> &s)noexcept
    requires (!traits.am_root)
  {
    return unwrap(s.trunk).parent.promise();
  }

};

template<typename... Fruit>
struct TreePromise{
  
  static constexpr TreeTraits<Fruit...> traits{};

  using Handle = std::coroutine_handle<TreePromise<Fruit...>>;

  trunk_t<Fruit...> trunk;

  TreePromise(Seed<Fruit...> &seed,const auto &...):
    trunk{std::move(seed.trunk)}
  {}
  TreePromise(const auto &,Seed<Fruit...> &seed,const auto &...):
    trunk{std::move(seed.trunk)}
  {}

  TreePromise(const auto &...):
    trunk{trunk_t<Fruit...>{}}
  {
    static_assert(traits.am_root);
  }

  struct IAwaitable{
    static constexpr
    bool await_ready()noexcept{
      return false;
    }
    static constexpr
    void await_suspend(auto tree_handle)noexcept{
      if constexpr(traits.am_toproot){
        auto &trunk = unwrap(tree_handle.promise().trunk);
        trunk.current = tree_handle;
      }
    }
    static constexpr void await_resume()noexcept {}
  };
  static constexpr IAwaitable initial_suspend()noexcept{
    return {};
  }

  struct FAwaitable{
    static constexpr
    bool await_ready()noexcept{
      return false;
    }
    static constexpr
    void await_suspend(auto)noexcept{
      HYSJ_UNREACHABLE;
    }
    static constexpr none await_resume()noexcept{
      HYSJ_UNREACHABLE;
      return {};
    }
  };
  static constexpr FAwaitable final_suspend()noexcept{
    return {};
  }

  Handle handle(){ return Handle::from_promise(*this); }

  static void unhandled_exception(){ throw; }

  Tree<Fruit...> get_return_object(){
    return Tree<Fruit...>{handle()};
  }

  template<typename YFruit>
  friend constexpr Seed<YFruit,Fruit...> get(TreePromise<Fruit...> &tree){
    Seed<YFruit,Fruit...> seed;
    seed.trunk.parent = tree.handle();
    return seed;
  }

  template<int I>
  friend constexpr auto &ancestor(TreePromise<Fruit... >&promise)noexcept{
    if constexpr(I == 0)
      return promise;
    else if constexpr(I < 0){
      constexpr int N = promise.traits.depth;
      constexpr int J = ancestor_v<I,N>;
      return ancestor<J>(promise);
    }
    else
      return ancestor<I - 1>(unwrap(promise.trunk).parent.promise());
  }

  template<int P,bool F>
  struct PAwaitable{
    static constexpr bool await_ready()noexcept{ return false; }
    static constexpr std::coroutine_handle<>
    await_suspend(auto leaf_handle)noexcept{
      auto &leaf = leaf_handle.promise();
      auto &bough = unwrap(ancestor<P>(leaf).trunk);
      unwrap(bough).done = F;
      if constexpr(bough.traits.am_root){
        if(F)
          bough.current = unwrap(leaf.trunk).parent;
        else
          bough.current = leaf.handle();
      }
      return bough.parent;
    }
    static constexpr auto await_resume()noexcept{
      if constexpr(F){
        HYSJ_UNREACHABLE;
      }
      return none{};
    }
  };
  template<int I,bool F,typename... T>
  friend constexpr auto put(TreePromise<Fruit... >&leaf,T &&... nutrients)noexcept{
    static_assert(leaf.traits.depth > 0);
    constexpr int P = put_v<I,leaf.traits.depth>;
    auto &bough = unwrap(ancestor<P>(leaf).trunk);
    bough.stem = {fwd(nutrients)...};
    return PAwaitable<P,F>{};
  }
  static constexpr void return_value(std::optional<none>)noexcept{}
};

//FIXME: yuck! - jeh
template<typename LFruit,typename... HFruit>
constexpr auto nop(const Seed<LFruit,HFruit...> &){
  if constexpr(!spliced_v<LFruit>)
    return always(no<LFruit>);
  else{
    //FIXME: need to resume parent! 
    return always(no<none>);
  }
}

template<int I,bool F,typename LFruit,typename... HFruit,typename... T>
constexpr auto put(Seed<LFruit,HFruit...> &seed,T &&... nutrients){
  if constexpr(I == 1){
    static_assert(!seed.traits.am_spliced,"Immediate 1-rets can't be spliced");
    static_assert(F,"Immediate 1-rets must be final.");
    return always(std::optional<unspliced_t<LFruit>>(fwd(nutrients)...));
  }
  else if constexpr(I == 0)
    return put<I,F>(promise(seed),fwd(nutrients)...);
  else{
    static_assert(I != any_of(0,1),"Only immediate puts are supported");
    HYSJ_UNREACHABLE; 
  }
}

template<typename LFruit,typename... HFruit>
constexpr Seed<splicer_t<LFruit>,HFruit...> pipe(TreePromise<LFruit,HFruit...> &tree){
  return {std::ref(tree.trunk)};
}

template<typename LFruit,typename... HFruit>
struct Tree{
  using Promise = TreePromise<LFruit,HFruit...>;
  using Handle  = typename Promise::Handle;

  static constexpr TreeTraits<LFruit,HFruit...> traits{};

  using promise_type = Promise;
    
  Handle handle;

  constexpr explicit
  Tree(Handle handle):
    handle{handle}
  {}

  constexpr Tree() noexcept = default;
  constexpr Tree(Tree &&rhs) noexcept:
    handle{std::exchange(rhs.handle,nullptr)}
  {}
  constexpr
  Tree &operator=(Tree &&rhs){
    if(this == &rhs)
      return *this;
    if(handle)
      chop();
    handle = std::exchange(rhs.handle,nullptr);
    return *this;
  }
  ~Tree(){ 
    if(handle)
      chop();
  }

  auto &trunk(){
    HYSJ_ASSERT(!stump());
    return handle.promise().trunk;
  }
  auto &trunk()const{
    HYSJ_ASSERT(!stump());
    return handle.promise().trunk;
  }
  auto &stem(){
    HYSJ_ASSERT(!stump());
    return unwrap(trunk()).stem;
  }
  auto &stem()const{
    HYSJ_ASSERT(!stump());
    return unwrap(trunk()).stem;
  }

  bool stump()const noexcept{
    return !handle || handle.done();
  }
  bool husk()const noexcept{
    HYSJ_ASSERT(!stump());
    return unwrap(trunk()).done;
  }

  bool ripen(){
    if(husk())
      stem() = std::nullopt;
    else
      root(unwrap(trunk())).current.resume();
    return stem().has_value();
  }
  void chop(){
    handle.destroy();
    handle = Handle{};
  }

  template<typename TreeReference>
  struct XAwaitable{
    TreeReference tree;

    constexpr 
    bool await_ready()const noexcept{
      return tree.husk();
    }
    std::coroutine_handle<> await_suspend(const auto &)noexcept{
      return tree.handle;
    }
    constexpr const auto &await_resume()noexcept{
      if constexpr(traits.am_spliced){
        HYSJ_UNREACHABLE;
        return no<none>;
      }
      else
        return tree.stem();
    }
  };
  auto operator co_await()&{
    HYSJ_ASSERT(!stump());
    return XAwaitable<Tree &>{*this};
  }
  auto operator co_await()&&{
    HYSJ_ASSERT(!stump());
    return XAwaitable<Tree &&>{std::move(*this)};
  }

  explicit operator bool()const noexcept requires (!traits.am_spliced) {
    return !husk(); 
  }
  auto &operator()() requires (!traits.am_spliced) {
    ripen();
    return stem();
  }

  friend HYSJ_MIRROR_STRUCT_ID(Tree)
};

using Sentinel = std::default_sentinel_t;

template<typename LFruit,typename... HFruit>
struct TreeIterator{

  static_assert(!spliced_v<LFruit>);

  using iterator_category = std::input_iterator_tag;
  using difference_type   = std::ptrdiff_t;
  using value_type        = LFruit;
  using reference         = std::add_lvalue_reference_t<value_type>;
  using pointer           = std::add_pointer_t<value_type>;

  Tree<LFruit,HFruit...> *tree = nullptr;

  TreeIterator() noexcept = default;
  TreeIterator(TreeIterator &&rhs){
    std::swap(tree,rhs.tree);
  }
  explicit TreeIterator(Tree<LFruit,HFruit...> &tree)noexcept:
    tree{std::addressof(tree)}
  {}

  constexpr
  TreeIterator &operator=(TreeIterator &&rhs)noexcept{
    std::swap(tree,rhs.tree);
    return *this;
  }

  ~TreeIterator(){}

  inline
  friend bool operator==(const TreeIterator &it,Sentinel)noexcept{
    return !it.tree;
  }
  TreeIterator &operator++(){
    if(!tree->ripen())
      tree = nullptr;
    return *this;
  }
  void operator++(int){
    (void)operator++();
  }
  reference operator*()const noexcept{
    return *tree->stem();
  }
  pointer operator->()const noexcept{
    return std::addressof(operator*());
  }
};


template<typename LFruit,typename... HFruit>
TreeIterator<LFruit,HFruit...> begin(Tree<LFruit,HFruit...> &t)noexcept{
  if(!t.ripen())
    return {};
  else
    return TreeIterator{t};
}

template<typename LFruit,typename... HFruit>
constexpr Sentinel end(Tree<LFruit,HFruit...> &)noexcept{
  return {};
}

template<typename LFruit,typename... HFruit>
TreeIterator(Tree<LFruit,HFruit...> &)
  -> TreeIterator<LFruit,HFruit...>;

} //hysj::coroutines::trees
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

template<typename... Fruit>
using co_get = coroutines::trees::Seed<Fruit...>;

using co_none = co_get<none>;

template<typename... Fruit>
using co_api = coroutines::trees::Tree<Fruit...>;

} //hysj
#include<hysj/tools/epilogue.hpp>
