#pragma once

#include<array>
#include<cstdint>
#include<span>
#include<stdexcept>
#include<string>
#include<type_traits>

#include<fmt/core.h>
#include<kumi/tuple.hpp>
#include<kumi/algorithm/push_pop.hpp>
#include<kumi/algorithm/cat.hpp>

#include<hysj/tools/functional/fold.hpp>
#include<hysj/tools/functional/apply.hpp>
#include<hysj/tools/functional/with.hpp>
#include<hysj/tools/functional/invoke.hpp>
#include<hysj/tools/preprocessor.hpp>
#include<hysj/tools/type_traits.hpp>
#include<hysj/tools/utility.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

namespace reflection{

  constexpr std::size_t enum_mirror_max0 = 64;
  constexpr std::size_t enum_mirror_max1 = 32;

  template<typename T>
  struct enum_mirror{
    std::size_t count;
    std::array<std::array<char, enum_mirror_max1>,enum_mirror_max0 + 1> names;
  };

  namespace _{
    template<std::size_t N>
    using enum_mirror_name_src = const char (&)[N];

    template<std::size_t N>
    constexpr auto make_enum_mirror_name(enum_mirror_name_src<N> src){
      static_assert(N < enum_mirror_max1);
      std::array<char, enum_mirror_max1> dst{};
      for(std::size_t i = 0;i != N;++i)
        dst[i] = src[i];
      return dst;
    }

    template<typename T,std::size_t... N>
    constexpr enum_mirror<T> make_enum_mirror(enum_mirror_name_src<N>... n){
      static_assert(sizeof...(N) < enum_mirror_max0 + 1);
      static_assert(((N <= enum_mirror_max1) && ...));
      return {
        .count = sizeof...(N) - 1,
        .names{ make_enum_mirror_name(n)... }
      };
    }
  }

  #define HYSJ_MIRROR_ENUM_IMPL(_, e) , #e
  #define HYSJ_MIRROR_ENUM(E, B)                                      \
    constexpr ::hysj::reflection::enum_mirror<E>                      \
    make_enum_mirror_hook(E *){                                       \
      return ::hysj::reflection::_::make_enum_mirror<E           \
        >(#E HYSJ_MAP(HYSJ_MIRROR_ENUM_IMPL, _, HYSJ_PROJECT(0, B))); \
    }
  namespace _{
    void make_enum_mirror_hook(...) = delete;
    namespace xxx{
      template<typename T>
      requires requires{ make_enum_mirror_hook((T *)0); }
      constexpr enum_mirror<T> enum_mirror_of_impl(){
        return make_enum_mirror_hook((T *)0);
      }
    }
  }

  template<typename T>
  inline constexpr enum_mirror<T> enum_mirror_of = _::xxx::enum_mirror_of_impl<T>();

  template<typename T>
  concept reflected_enum = requires{ _::xxx::enum_mirror_of_impl<T>(); };

  template<typename T>
  inline constexpr std::size_t enumerator_count = enum_mirror_of<T>.count;
  template<typename T>
  using enumerator_index_sequence = std::make_index_sequence<enumerator_count<T>>;

  template<typename T>
  inline constexpr std::array<T, enumerator_count<T>> enumerator_values =
    []{
      constexpr auto N = enumerator_count<T>;
      using V = std::array<T, N>;
      using I = etoi_t<T>;
      if constexpr(std::is_same_v<I,bool>){
        if constexpr(N == 0)
          return V{};
        if constexpr(N == 1)
          return V{T{false}};
        if constexpr(N == 2)
          return V{T{false}, T{true}};
      }
      else{
        V values;
        for(etoi_t<T> i = 0; i < static_cast<I>(N); ++i)
          values[i] = T{i};
        return values;
      }
    }();

  template<typename T>
  inline constexpr std::string_view enum_name = std::string_view{enum_mirror_of<T>.names[0].data()};

  template<typename T>
  inline constexpr std::array<std::string_view, enumerator_count<T>> enumerator_names =
    []{
      std::array<std::string_view, enumerator_count<T>> names;
      for(std::size_t i = 0; i < names.size(); ++i)
        names[i] = std::string_view{enum_mirror_of<T>.names[i + 1].data()};
      return names;
    }();
  template<typename T>
  constexpr std::string_view enumerator_name(const T &t){
    return enumerator_names<T>[etoi(t)];
  }

  template<typename T>
  constexpr auto with_enumerators(auto &&f){
    return [&]<std::size_t... I>(std::index_sequence<I...>){
      return invoke(f, std::integral_constant<T,T{I}>{}...);
    }(enumerator_index_sequence<T>{});
  }
  template<typename T>
  constexpr auto each_enumerator(auto &&f){
    return with_enumerators<T>(
      [&](auto... E){
        (invoke(f,E), ...);
      });
  }

  template<typename T>
  struct enum_formatter{
    static_assert(reflected_enum<T>);
    constexpr auto parse(fmt::format_parse_context& context)const{
      auto it  = context.begin(),
        end = context.end();
      if(it != end && *it != '}')
        throw fmt::format_error("invalid format");
      return it;
    }
    auto format(const T &e,fmt::format_context &context)const{
      return fmt::format_to(context.out(),"{0}", enumerator_name(e));
    }
  };
    
  //NOTE: assume that all enums are unsigned, start at 0, with no gaps - jeh
  template<typename E> 
  constexpr std::optional<E> enum_cast(etoi_t<E> u)noexcept{
    constexpr etoi_t<E> N = enumerator_count<E>;
    if(u < N)
      return static_cast<E>(u);
    return std::nullopt;
  }

  template<typename... E>
  constexpr decltype(auto) with_enum(auto &&F,E... e){
    return with<enumerator_count<E>...>(
      [&](auto... I) -> decltype(auto){
        return fwd(F)(constant_c<E{I()}>...);
      },
      e...);
  }

  template<typename B>
  struct base_mirror{
    using type = B;
  };

  template<auto base_mirror>
  using base_type_t = typename decltype(base_mirror)::type;
  

  template<std::size_t N>
  constexpr auto make_member_name_span(const char (&member_name)[N]){
    return std::span<const char,N>{member_name};
  }

  template<auto M>
  struct member_mirror{
    static constexpr decltype(M().first)  pointer = M().first;
    static constexpr decltype(M().second) name    = M().second;
    using type = member_type<pointer>;
  };

  template<auto member_mirror>
  using member_type_t = typename decltype(member_mirror)::type;

  template<auto M> constexpr decltype(M().first)  member_mirror<M>::pointer;
  template<auto M> constexpr decltype(M().second) member_mirror<M>::name;

  template<auto S,auto B,auto M>
  struct struct_mirror{
    static constexpr decltype(S()) name = S();
  };

  template<auto S,auto B,auto M> constexpr decltype(S()) struct_mirror<S,B,M>::name;

  namespace _{
    template<auto S,auto B,auto M>
    constexpr struct_mirror<S,B,M> make_struct_mirror_impl()noexcept{
      return {};
    }
  }
  
  #define HYSJ_MIRROR_STRUCT_IMPL(S) []{ return #S; }
  #define HYSJ_MIRROR_BASE_IMPL(S, b, ...) , hysj::reflection::base_mirror<b>{}
  #define HYSJ_MIRROR_MEMBER_IMPL(S, m, ...) , hysj::reflection::member_mirror<[]{ return std::make_pair(&S::m,#m); }>{}

  #define HYSJ_MIRROR_STRUCT_MEMBERS(S, m)          \
    HYSJ_MIRROR_STRUCT_MEMBERS_2(S,HYSJ_UNPAREN m)
  #define HYSJ_MIRROR_STRUCT_MEMBERS_2(S, ...)                        \
    []{ return kumi::pop_front(kumi::tuple{0 HYSJ_MAP_VA(HYSJ_MIRROR_MEMBER_IMPL, S __VA_OPT__(,) __VA_ARGS__) }); }
  #define HYSJ_MIRROR_STRUCT_BASES(S, b)          \
    HYSJ_MIRROR_STRUCT_BASES_2(S,HYSJ_UNPAREN b)
  #define HYSJ_MIRROR_STRUCT_BASES_2(S, ...)                          \
    []{ return kumi::pop_front(kumi::tuple{0 HYSJ_MAP_VA(HYSJ_MIRROR_BASE_IMPL, S __VA_OPT__(,) __VA_ARGS__) }); }
  
  #define HYSJ_MIRROR_STRUCT_0(S, BM)                      \
    constexpr decltype(                                    \
      hysj::reflection::_::make_struct_mirror_impl< \
      HYSJ_MIRROR_STRUCT_IMPL(S),                          \
      []{ return kumi::tuple{}; },                          \
      []{ return kumi::tuple{}; },                          \
      >())                                                 \
    make_struct_mirror(S *)                                \
    { return {}; }
  #define HYSJ_MIRROR_STRUCT_1(S, BM)                      \
    constexpr decltype(                                    \
      hysj::reflection::_::make_struct_mirror_impl< \
      HYSJ_MIRROR_STRUCT_IMPL(S),                          \
      []{ return kumi::tuple{}; },                          \
      HYSJ_MIRROR_STRUCT_MEMBERS(S, HYSJ_AT(0, BM))        \
      >())                                                 \
    make_struct_mirror(S *)                                \
    { return {}; }
  #define HYSJ_MIRROR_STRUCT_2(S, BM)                      \
    constexpr decltype(                                    \
      hysj::reflection::_::make_struct_mirror_impl< \
      HYSJ_MIRROR_STRUCT_IMPL(S),                          \
      HYSJ_MIRROR_STRUCT_BASES(S, HYSJ_AT(1, BM)),         \
      HYSJ_MIRROR_STRUCT_MEMBERS(S, HYSJ_AT(0, BM))        \
      >())                                                 \
    make_struct_mirror(S *)                                \
    { return {}; }
  #define HYSJ_MIRROR_STRUCT(S, BM) \
    HYSJ_CAT(HYSJ_MIRROR_STRUCT_,HYSJ_SIZE(BM))(S,BM)
  #define HYSJ_MIRROR_STRUCT_ID(S) HYSJ_MIRROR_STRUCT(S, ())
  
  template<class T>
  requires std::is_class_v<T>
  constexpr auto reflect() -> decltype(make_struct_mirror(static_cast<T *>(0))){
    return {};
  }

  template<class T>
  requires std::is_class_v<T>
  constexpr auto reflect(const T &){
    return reflect<T>();
  }

  template<class T>
  constexpr bool is_mirror = false;
  template<auto S,auto B,auto M>
  constexpr bool is_mirror<struct_mirror<S,B,M>> = true;

  template<class T>
  concept reflected =
    (!is_mirror<std::remove_cvref_t<T>>
     && requires { reflect<std::remove_cvref_t<T>>(); });

  template<class T>
  concept reflected_struct =
    (reflected<T> && std::is_class_v<std::remove_cvref_t<T>>);

  template<auto S,auto B,auto M>
  constexpr auto bases(struct_mirror<S,B,M>)noexcept{
    return B();
  }
  template<auto S,auto B,auto M>
  constexpr auto members(struct_mirror<S,B,M>)noexcept{
    return M();
  }
  template<auto S,auto B,auto M>
  constexpr auto base_count(struct_mirror<S,B,M> m)noexcept{
    return kumi::size_v<decltype(bases(m))>;
  }
  template<auto S,auto B,auto M>
  constexpr auto member_count(struct_mirror<S,B,M> m)noexcept{
    return kumi::size_v<decltype(members(m))>;
  }

  template<auto S,auto B,auto M,std::size_t i>
  constexpr auto base_at(struct_mirror<S,B,M> m,std::integral_constant<std::size_t,i>){
    return kumi::get<i>(bases(m));
  }

  template<auto S,auto B,auto M>
  constexpr auto base_indices(struct_mirror<S,B,M> m)noexcept{
    return std::make_index_sequence<base_count(m)>{};
  }

  template<auto S,auto B,auto M>
  constexpr auto member_indices(struct_mirror<S,B,M> m)noexcept{
    return std::make_index_sequence<member_count(m)>{};
  }

  constexpr auto member_refs(reflected_struct auto &&s)noexcept{
    return kumi::apply(
      [&](auto... M){
        return kumi::forward_as_tuple(fwd(s).*M.pointer...);
      },
      members(reflect(s)));
  }

  constexpr auto members(const reflected_struct auto &s)noexcept{
    return kumi::apply(
      [&](auto... M){
        return kumi::make_tuple((s.*M.pointer)...);
      },
      members(reflect(s)));
  }
  template<auto S,auto B,auto M,std::size_t i>
  constexpr auto member_at(struct_mirror<S,B,M> m,std::integral_constant<std::size_t,i>){
    return kumi::get<i>(members(m));
  }

  constexpr auto annotate(const reflected_struct auto &s,auto f)noexcept{
    return kumi::apply(
      [&](auto... M){
        //FIXME: heterog vs homog - jeh
        return std::array{kumi::tuple{s.*M.pointer,f(constant_c<M.pointer>)}...};
      },
      members(reflect(s)));
  }

  template <reflected_struct T, typename Char>
  struct struct_formatter {

    constexpr auto parse(auto &ctx) {
      auto it = ctx.begin(), end = ctx.end();

      if (it != end && *it != '}')
        throw fmt::format_error("invalid format");

      return it;
    }

    auto format(const T &t, auto &ctx) const {
      auto out = ctx.out();

      *out++ = '{';

      bool first = true;

      constexpr auto m = reflect<T>();
      kumi::apply(
        [&](auto... B){
          [[maybe_unused]] auto p = [&](auto b){
            if (!first)
              *out++ = ',';
            first = false;
            out = fmt::format_to(out, " {}", (typename decltype(b)::type const &) t);
          };
          (p(B),...);
        },
        bases(m));
      kumi::apply(
        [&](auto... M){
          [[maybe_unused]] auto p = [&](auto m){
            if (!first)
              *out++ = ',';
            first = false;
            out = fmt::format_to(out, " .{}={}", m.name, t.*m.pointer);
          };
          (p(M),...);
        },
        members(m));

      if (!first)
        *out++ = ' ';

      *out++ = '}';

      return out;
    }
  };


}//reflection

using reflection::reflected_enum;
using reflection::with_enum;
using reflection::enum_cast;
using reflection::with_enumerators;
using reflection::each_enumerator;
using reflection::enum_name;
using reflection::enumerator_names;
using reflection::enumerator_values;
using reflection::enumerator_count;
using reflection::enumerator_name;

using reflection::reflected;
using reflection::reflect;
using reflection::bases;
using reflection::base_type_t;
using reflection::members;
using reflection::member_refs;
using reflection::reflected_struct;


}//hysj

#define HYSJ_ENUM_FORMATTER(type)\
  template<typename C> struct fmt::formatter<type, C>:hysj::_HYSJ_VERSION_NAMESPACE::reflection::enum_formatter<type>{};

#define HYSJ_STRUCT_FORMATTER(C,U)                               \
  struct fmt::formatter<HYSJ_UNWRAP(C),U> :                             \
    hysj::_HYSJ_VERSION_NAMESPACE::reflection::struct_formatter<HYSJ_UNWRAP(C),U>{};


#include<hysj/tools/epilogue.hpp>
