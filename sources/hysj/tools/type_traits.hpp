#pragma once
#include<array>
#include<cstdint>
#include<memory>
#include<span>
#include<tuple>
#include<type_traits>
#include<vector>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

template<typename T>
concept testable = std::convertible_to<T,bool>
  || (requires(T t){ t.operator bool(); });

template<class T,class U>
concept not_same_as = (!std::same_as<T,U>);

template<class T>
using uncvref_t = std::remove_cvref_t<std::unwrap_reference_t<std::remove_cvref_t<T>>>;

template<class T,bool C>
using add_const_if = std::conditional_t<C,const T,T>;

template<class T>
using type_t = std::type_identity<T>;
template<class T>
inline constexpr type_t<T> type_c{};

template<auto i>
using constant_t = std::integral_constant<uncvref_t<decltype(i)>,i>;
template<auto i>
inline constexpr constant_t<i> constant_c{};

namespace _{

  template<auto N>
  constexpr auto with_constant_impl(constant_t<N>,auto &&F,constant_t<N>,auto) ->
    decltype(fwd(F)(constant_c<N>))
  {
    HYSJ_UNREACHABLE;
  }
  template<auto N,auto M>
  constexpr auto with_constant_impl(constant_t<N> n,auto &&F,constant_t<M>,auto i) ->
    decltype(fwd(F)(constant_c<N>))
  {
    if(M == i)
      return fwd(F)(constant_c<M>);
    return with_constant_impl(n,fwd(F),constant_c<M + 1>,i);
  }
  
}
template<auto N>
constexpr auto with_constant(constant_t<N> n,auto &&F,auto i){
  return _::with_constant_impl(n,fwd(F),constant_c<N - N>,i);
}

namespace _{
  template<class T> struct is_constant_impl:std::false_type{};
  template<class T,T value> struct is_constant_impl<std::integral_constant<T,value>>:std::true_type{};
}
template<class T>
concept is_constant = _::is_constant_impl<uncvref_t<T>>::value;

namespace _{
  template<class T,class U> struct is_constant_of_impl:std::false_type{};
  template<class T,T value,class U> struct is_constant_of_impl<std::integral_constant<T,value>,U>:
    std::is_same<uncvref_t<T>,U>{};
}
template<class T,class U>
concept is_constant_of = _::is_constant_of_impl<uncvref_t<T>,U>::value;

template<auto x,auto y>
consteval auto is_same_constant(constant_t<x>,constant_t<y>){
  if constexpr(std::is_same_v<decltype(x),decltype(y)>)
    return constant_c<x == y>;
  else
    return constant_c<false>;
}

template<class>
struct is_scoped_enum : std::false_type {};

template<auto N>
constexpr auto with_integer_sequence(constant_t<N>,auto &&F){
  using T = decltype(N);
  return [&]<T... I>(std::integer_sequence<T,I...>){
    return fwd(F)(constant_c<I>...);
  }(std::make_integer_sequence<T,N>{});
}
 
template<class T>
requires std::is_enum_v<T>
struct is_scoped_enum<T> : std::bool_constant<
    !std::is_convertible_v<T, std::underlying_type_t<T>>
> {};

template<class T>
constexpr bool is_scoped_enum_v = is_scoped_enum<T>::value;

template<typename T>
inline constexpr auto is_std_reference_wrapper = 
  std::bool_constant<!std::is_same_v<std::decay_t<std::unwrap_reference_t<T &&>>, std::unwrap_ref_decay_t<T &&>>>{};

template<class T>
constexpr auto is_std_unique_ptr = std::false_type{};
template<class T>
constexpr auto is_std_unique_ptr<std::unique_ptr<T>> = std::true_type{};

template<class T>
constexpr auto is_std_array = std::false_type{};
template<class T,std::size_t N>
constexpr auto is_std_array<std::array<T,N>> = std::true_type{};

template<class T>
constexpr auto is_std_span = std::false_type{};
template<class T,std::size_t N>
constexpr auto is_std_span<std::span<T,N>> = std::true_type{};

namespace _{
  template<auto pointer>
  struct member_type_impl;
  template<class T,class U,T U::* pointer>
  struct member_type_impl<pointer>{
    using type = T;
  };
}
template<auto pointer>
using member_type = typename _::member_type_impl<pointer>::type;

template<typename U,typename... T>
constexpr bool is_same_v = (std::is_same_v<U,T> && ...);

} //hysj
#include<hysj/tools/epilogue.hpp>
