#pragma once
#include<hysj/tools/prologue.hpp>

#define HYSJ_STRINGIFY_IMPL(X) #X
#define HYSJ_STRINGIFY(X) HYSJ_STRINGIFY_IMPL(X)

#define HYSJ_EXPAND(X) X
#define HYSJ_UNPAREN(...) __VA_ARGS__
#define HYSJ_UNWRAP(X) HYSJ_EXPAND(HYSJ_UNPAREN X)

#define HYSJ_CAT(a,b) HYSJ_CAT_IMPL(a,b)
#define HYSJ_CAT_IMPL(a, b) a ## b

#define HYSJ_JOIN(a,b) a b

#define HYSJ_BOOL(x) HYSJ_CAT(HYSJ_BOOL_,x)

#define HYSJ_BOOL_0 0
#define HYSJ_BOOL_1 1
#define HYSJ_BOOL_2 1
#define HYSJ_BOOL_3 1
#define HYSJ_BOOL_4 1
#define HYSJ_BOOL_5 1
#define HYSJ_BOOL_6 1
#define HYSJ_BOOL_7 1
#define HYSJ_BOOL_8 1
#define HYSJ_BOOL_9 1
#define HYSJ_BOOL_10 1
#define HYSJ_BOOL_11 1
#define HYSJ_BOOL_12 1
#define HYSJ_BOOL_13 1
#define HYSJ_BOOL_14 1
#define HYSJ_BOOL_15 1
#define HYSJ_BOOL_16 1
#define HYSJ_BOOL_17 1
#define HYSJ_BOOL_18 1
#define HYSJ_BOOL_19 1
#define HYSJ_BOOL_20 1
#define HYSJ_BOOL_21 1
#define HYSJ_BOOL_22 1
#define HYSJ_BOOL_23 1
#define HYSJ_BOOL_24 1
#define HYSJ_BOOL_25 1
#define HYSJ_BOOL_26 1
#define HYSJ_BOOL_27 1
#define HYSJ_BOOL_28 1
#define HYSJ_BOOL_29 1
#define HYSJ_BOOL_30 1
#define HYSJ_BOOL_31 1
#define HYSJ_BOOL_32 1
#define HYSJ_BOOL_33 1
#define HYSJ_BOOL_34 1
#define HYSJ_BOOL_35 1
#define HYSJ_BOOL_36 1
#define HYSJ_BOOL_37 1
#define HYSJ_BOOL_38 1
#define HYSJ_BOOL_39 1
#define HYSJ_BOOL_40 1
#define HYSJ_BOOL_41 1
#define HYSJ_BOOL_42 1
#define HYSJ_BOOL_43 1
#define HYSJ_BOOL_44 1
#define HYSJ_BOOL_45 1
#define HYSJ_BOOL_46 1
#define HYSJ_BOOL_47 1
#define HYSJ_BOOL_48 1
#define HYSJ_BOOL_49 1
#define HYSJ_BOOL_50 1
#define HYSJ_BOOL_51 1
#define HYSJ_BOOL_52 1
#define HYSJ_BOOL_53 1
#define HYSJ_BOOL_54 1
#define HYSJ_BOOL_55 1
#define HYSJ_BOOL_56 1
#define HYSJ_BOOL_57 1
#define HYSJ_BOOL_58 1
#define HYSJ_BOOL_59 1
#define HYSJ_BOOL_60 1
#define HYSJ_BOOL_61 1
#define HYSJ_BOOL_62 1
#define HYSJ_BOOL_63 1
#define HYSJ_BOOL_64 1
#define HYSJ_BOOL_65 1
#define HYSJ_BOOL_66 1
#define HYSJ_BOOL_67 1
#define HYSJ_BOOL_68 1
#define HYSJ_BOOL_69 1
#define HYSJ_BOOL_70 1
#define HYSJ_BOOL_71 1
#define HYSJ_BOOL_72 1
#define HYSJ_BOOL_73 1
#define HYSJ_BOOL_74 1
#define HYSJ_BOOL_75 1
#define HYSJ_BOOL_76 1
#define HYSJ_BOOL_77 1
#define HYSJ_BOOL_78 1
#define HYSJ_BOOL_79 1
#define HYSJ_BOOL_80 1
#define HYSJ_BOOL_81 1
#define HYSJ_BOOL_82 1
#define HYSJ_BOOL_83 1
#define HYSJ_BOOL_84 1
#define HYSJ_BOOL_85 1
#define HYSJ_BOOL_86 1
#define HYSJ_BOOL_87 1
#define HYSJ_BOOL_88 1
#define HYSJ_BOOL_89 1
#define HYSJ_BOOL_90 1
#define HYSJ_BOOL_91 1
#define HYSJ_BOOL_92 1
#define HYSJ_BOOL_93 1
#define HYSJ_BOOL_94 1
#define HYSJ_BOOL_95 1
#define HYSJ_BOOL_96 1
#define HYSJ_BOOL_97 1
#define HYSJ_BOOL_98 1
#define HYSJ_BOOL_99 1
#define HYSJ_BOOL_100 1

#define HYSJ_FIRST(a,...) a

#define HYSJ_SIZE(sequence) HYSJ_CAT(HYSJ_SIZE_, HYSJ_SIZE_0 sequence)

#define HYSJ_SIZE_0(...) HYSJ_SIZE_1
#define HYSJ_SIZE_1(...) HYSJ_SIZE_2
#define HYSJ_SIZE_2(...) HYSJ_SIZE_3
#define HYSJ_SIZE_3(...) HYSJ_SIZE_4
#define HYSJ_SIZE_4(...) HYSJ_SIZE_5
#define HYSJ_SIZE_5(...) HYSJ_SIZE_6
#define HYSJ_SIZE_6(...) HYSJ_SIZE_7
#define HYSJ_SIZE_7(...) HYSJ_SIZE_8
#define HYSJ_SIZE_8(...) HYSJ_SIZE_9
#define HYSJ_SIZE_9(...) HYSJ_SIZE_10
#define HYSJ_SIZE_10(...) HYSJ_SIZE_11
#define HYSJ_SIZE_11(...) HYSJ_SIZE_12
#define HYSJ_SIZE_12(...) HYSJ_SIZE_13
#define HYSJ_SIZE_13(...) HYSJ_SIZE_14
#define HYSJ_SIZE_14(...) HYSJ_SIZE_15
#define HYSJ_SIZE_15(...) HYSJ_SIZE_16
#define HYSJ_SIZE_16(...) HYSJ_SIZE_17
#define HYSJ_SIZE_17(...) HYSJ_SIZE_18
#define HYSJ_SIZE_18(...) HYSJ_SIZE_19
#define HYSJ_SIZE_19(...) HYSJ_SIZE_20
#define HYSJ_SIZE_20(...) HYSJ_SIZE_21
#define HYSJ_SIZE_21(...) HYSJ_SIZE_22
#define HYSJ_SIZE_22(...) HYSJ_SIZE_23
#define HYSJ_SIZE_23(...) HYSJ_SIZE_24
#define HYSJ_SIZE_24(...) HYSJ_SIZE_25
#define HYSJ_SIZE_25(...) HYSJ_SIZE_26
#define HYSJ_SIZE_26(...) HYSJ_SIZE_27
#define HYSJ_SIZE_27(...) HYSJ_SIZE_28
#define HYSJ_SIZE_28(...) HYSJ_SIZE_29
#define HYSJ_SIZE_29(...) HYSJ_SIZE_30
#define HYSJ_SIZE_30(...) HYSJ_SIZE_31
#define HYSJ_SIZE_31(...) HYSJ_SIZE_32
#define HYSJ_SIZE_32(...) HYSJ_SIZE_33
#define HYSJ_SIZE_33(...) HYSJ_SIZE_34
#define HYSJ_SIZE_34(...) HYSJ_SIZE_35
#define HYSJ_SIZE_35(...) HYSJ_SIZE_36
#define HYSJ_SIZE_36(...) HYSJ_SIZE_37
#define HYSJ_SIZE_37(...) HYSJ_SIZE_38
#define HYSJ_SIZE_38(...) HYSJ_SIZE_39
#define HYSJ_SIZE_39(...) HYSJ_SIZE_40
#define HYSJ_SIZE_40(...) HYSJ_SIZE_41
#define HYSJ_SIZE_41(...) HYSJ_SIZE_42
#define HYSJ_SIZE_42(...) HYSJ_SIZE_43
#define HYSJ_SIZE_43(...) HYSJ_SIZE_44
#define HYSJ_SIZE_44(...) HYSJ_SIZE_45
#define HYSJ_SIZE_45(...) HYSJ_SIZE_46
#define HYSJ_SIZE_46(...) HYSJ_SIZE_47
#define HYSJ_SIZE_47(...) HYSJ_SIZE_48
#define HYSJ_SIZE_48(...) HYSJ_SIZE_49
#define HYSJ_SIZE_49(...) HYSJ_SIZE_50
#define HYSJ_SIZE_50(...) HYSJ_SIZE_51
#define HYSJ_SIZE_51(...) HYSJ_SIZE_52
#define HYSJ_SIZE_52(...) HYSJ_SIZE_53
#define HYSJ_SIZE_53(...) HYSJ_SIZE_54
#define HYSJ_SIZE_54(...) HYSJ_SIZE_55
#define HYSJ_SIZE_55(...) HYSJ_SIZE_56
#define HYSJ_SIZE_56(...) HYSJ_SIZE_57
#define HYSJ_SIZE_57(...) HYSJ_SIZE_58
#define HYSJ_SIZE_58(...) HYSJ_SIZE_59
#define HYSJ_SIZE_59(...) HYSJ_SIZE_60
#define HYSJ_SIZE_60(...) HYSJ_SIZE_61
#define HYSJ_SIZE_61(...) HYSJ_SIZE_62
#define HYSJ_SIZE_62(...) HYSJ_SIZE_63
#define HYSJ_SIZE_63(...) HYSJ_SIZE_64
#define HYSJ_SIZE_64(...) HYSJ_SIZE_65
#define HYSJ_SIZE_65(...) HYSJ_SIZE_66
#define HYSJ_SIZE_66(...) HYSJ_SIZE_67
#define HYSJ_SIZE_67(...) HYSJ_SIZE_68
#define HYSJ_SIZE_68(...) HYSJ_SIZE_69
#define HYSJ_SIZE_69(...) HYSJ_SIZE_70
#define HYSJ_SIZE_70(...) HYSJ_SIZE_71
#define HYSJ_SIZE_71(...) HYSJ_SIZE_72
#define HYSJ_SIZE_72(...) HYSJ_SIZE_73
#define HYSJ_SIZE_73(...) HYSJ_SIZE_74
#define HYSJ_SIZE_74(...) HYSJ_SIZE_75
#define HYSJ_SIZE_75(...) HYSJ_SIZE_76
#define HYSJ_SIZE_76(...) HYSJ_SIZE_77
#define HYSJ_SIZE_77(...) HYSJ_SIZE_78
#define HYSJ_SIZE_78(...) HYSJ_SIZE_79
#define HYSJ_SIZE_79(...) HYSJ_SIZE_80
#define HYSJ_SIZE_80(...) HYSJ_SIZE_81
#define HYSJ_SIZE_81(...) HYSJ_SIZE_82
#define HYSJ_SIZE_82(...) HYSJ_SIZE_83
#define HYSJ_SIZE_83(...) HYSJ_SIZE_84
#define HYSJ_SIZE_84(...) HYSJ_SIZE_85
#define HYSJ_SIZE_85(...) HYSJ_SIZE_86
#define HYSJ_SIZE_86(...) HYSJ_SIZE_87
#define HYSJ_SIZE_87(...) HYSJ_SIZE_88
#define HYSJ_SIZE_88(...) HYSJ_SIZE_89
#define HYSJ_SIZE_89(...) HYSJ_SIZE_90
#define HYSJ_SIZE_90(...) HYSJ_SIZE_91
#define HYSJ_SIZE_91(...) HYSJ_SIZE_92
#define HYSJ_SIZE_92(...) HYSJ_SIZE_93
#define HYSJ_SIZE_93(...) HYSJ_SIZE_94
#define HYSJ_SIZE_94(...) HYSJ_SIZE_95
#define HYSJ_SIZE_95(...) HYSJ_SIZE_96
#define HYSJ_SIZE_96(...) HYSJ_SIZE_97
#define HYSJ_SIZE_97(...) HYSJ_SIZE_98
#define HYSJ_SIZE_98(...) HYSJ_SIZE_99
#define HYSJ_SIZE_99(...) HYSJ_SIZE_100

#define HYSJ_SIZE_HYSJ_SIZE_0 0
#define HYSJ_SIZE_HYSJ_SIZE_1 1
#define HYSJ_SIZE_HYSJ_SIZE_2 2
#define HYSJ_SIZE_HYSJ_SIZE_3 3
#define HYSJ_SIZE_HYSJ_SIZE_4 4
#define HYSJ_SIZE_HYSJ_SIZE_5 5
#define HYSJ_SIZE_HYSJ_SIZE_6 6
#define HYSJ_SIZE_HYSJ_SIZE_7 7
#define HYSJ_SIZE_HYSJ_SIZE_8 8
#define HYSJ_SIZE_HYSJ_SIZE_9 9
#define HYSJ_SIZE_HYSJ_SIZE_10 10
#define HYSJ_SIZE_HYSJ_SIZE_11 11
#define HYSJ_SIZE_HYSJ_SIZE_12 12
#define HYSJ_SIZE_HYSJ_SIZE_13 13
#define HYSJ_SIZE_HYSJ_SIZE_14 14
#define HYSJ_SIZE_HYSJ_SIZE_15 15
#define HYSJ_SIZE_HYSJ_SIZE_16 16
#define HYSJ_SIZE_HYSJ_SIZE_17 17
#define HYSJ_SIZE_HYSJ_SIZE_18 18
#define HYSJ_SIZE_HYSJ_SIZE_19 19
#define HYSJ_SIZE_HYSJ_SIZE_20 20
#define HYSJ_SIZE_HYSJ_SIZE_21 21
#define HYSJ_SIZE_HYSJ_SIZE_22 22
#define HYSJ_SIZE_HYSJ_SIZE_23 23
#define HYSJ_SIZE_HYSJ_SIZE_24 24
#define HYSJ_SIZE_HYSJ_SIZE_25 25
#define HYSJ_SIZE_HYSJ_SIZE_26 26
#define HYSJ_SIZE_HYSJ_SIZE_27 27
#define HYSJ_SIZE_HYSJ_SIZE_28 28
#define HYSJ_SIZE_HYSJ_SIZE_29 29
#define HYSJ_SIZE_HYSJ_SIZE_30 30
#define HYSJ_SIZE_HYSJ_SIZE_31 31
#define HYSJ_SIZE_HYSJ_SIZE_32 32
#define HYSJ_SIZE_HYSJ_SIZE_33 33
#define HYSJ_SIZE_HYSJ_SIZE_34 34
#define HYSJ_SIZE_HYSJ_SIZE_35 35
#define HYSJ_SIZE_HYSJ_SIZE_36 36
#define HYSJ_SIZE_HYSJ_SIZE_37 37
#define HYSJ_SIZE_HYSJ_SIZE_38 38
#define HYSJ_SIZE_HYSJ_SIZE_39 39
#define HYSJ_SIZE_HYSJ_SIZE_40 40
#define HYSJ_SIZE_HYSJ_SIZE_41 41
#define HYSJ_SIZE_HYSJ_SIZE_42 42
#define HYSJ_SIZE_HYSJ_SIZE_43 43
#define HYSJ_SIZE_HYSJ_SIZE_44 44
#define HYSJ_SIZE_HYSJ_SIZE_45 45
#define HYSJ_SIZE_HYSJ_SIZE_46 46
#define HYSJ_SIZE_HYSJ_SIZE_47 47
#define HYSJ_SIZE_HYSJ_SIZE_48 48
#define HYSJ_SIZE_HYSJ_SIZE_49 49
#define HYSJ_SIZE_HYSJ_SIZE_50 50
#define HYSJ_SIZE_HYSJ_SIZE_51 51
#define HYSJ_SIZE_HYSJ_SIZE_52 52
#define HYSJ_SIZE_HYSJ_SIZE_53 53
#define HYSJ_SIZE_HYSJ_SIZE_54 54
#define HYSJ_SIZE_HYSJ_SIZE_55 55
#define HYSJ_SIZE_HYSJ_SIZE_56 56
#define HYSJ_SIZE_HYSJ_SIZE_57 57
#define HYSJ_SIZE_HYSJ_SIZE_58 58
#define HYSJ_SIZE_HYSJ_SIZE_59 59
#define HYSJ_SIZE_HYSJ_SIZE_60 60
#define HYSJ_SIZE_HYSJ_SIZE_61 61
#define HYSJ_SIZE_HYSJ_SIZE_62 62
#define HYSJ_SIZE_HYSJ_SIZE_63 63
#define HYSJ_SIZE_HYSJ_SIZE_64 64
#define HYSJ_SIZE_HYSJ_SIZE_65 65
#define HYSJ_SIZE_HYSJ_SIZE_66 66
#define HYSJ_SIZE_HYSJ_SIZE_67 67
#define HYSJ_SIZE_HYSJ_SIZE_68 68
#define HYSJ_SIZE_HYSJ_SIZE_69 69
#define HYSJ_SIZE_HYSJ_SIZE_70 70
#define HYSJ_SIZE_HYSJ_SIZE_71 71
#define HYSJ_SIZE_HYSJ_SIZE_72 72
#define HYSJ_SIZE_HYSJ_SIZE_73 73
#define HYSJ_SIZE_HYSJ_SIZE_74 74
#define HYSJ_SIZE_HYSJ_SIZE_75 75
#define HYSJ_SIZE_HYSJ_SIZE_76 76
#define HYSJ_SIZE_HYSJ_SIZE_77 77
#define HYSJ_SIZE_HYSJ_SIZE_78 78
#define HYSJ_SIZE_HYSJ_SIZE_79 79
#define HYSJ_SIZE_HYSJ_SIZE_80 80
#define HYSJ_SIZE_HYSJ_SIZE_81 81
#define HYSJ_SIZE_HYSJ_SIZE_82 82
#define HYSJ_SIZE_HYSJ_SIZE_83 83
#define HYSJ_SIZE_HYSJ_SIZE_84 84
#define HYSJ_SIZE_HYSJ_SIZE_85 85
#define HYSJ_SIZE_HYSJ_SIZE_86 86
#define HYSJ_SIZE_HYSJ_SIZE_87 87
#define HYSJ_SIZE_HYSJ_SIZE_88 88
#define HYSJ_SIZE_HYSJ_SIZE_89 89
#define HYSJ_SIZE_HYSJ_SIZE_90 90
#define HYSJ_SIZE_HYSJ_SIZE_91 91
#define HYSJ_SIZE_HYSJ_SIZE_92 92
#define HYSJ_SIZE_HYSJ_SIZE_93 93
#define HYSJ_SIZE_HYSJ_SIZE_94 94
#define HYSJ_SIZE_HYSJ_SIZE_95 95
#define HYSJ_SIZE_HYSJ_SIZE_96 96
#define HYSJ_SIZE_HYSJ_SIZE_97 97
#define HYSJ_SIZE_HYSJ_SIZE_98 98
#define HYSJ_SIZE_HYSJ_SIZE_99 99
#define HYSJ_SIZE_HYSJ_SIZE_100 100

#define HYSJ_IF(condition,primary,secondary) HYSJ_CAT(HYSJ_IF_,condition)(primary,secondary)
#define HYSJ_IF_0(primary,secondary) secondary
#define HYSJ_IF_1(primary,secondary) primary

#define HYSJ_DROP(count,sequence)                               \
  HYSJ_CAT(HYSJ_DROP_IMPL_,HYSJ_BOOL(count))(count,sequence)
#define HYSJ_DROP_IMPL_1(count,sequence) \
  HYSJ_CAT(HYSJ_DROP_,count) sequence
#define HYSJ_DROP_IMPL_0(count,sequence) sequence

#define HYSJ_DROP_1(...)
#define HYSJ_DROP_2(...) HYSJ_DROP_1
#define HYSJ_DROP_3(...) HYSJ_DROP_2
#define HYSJ_DROP_4(...) HYSJ_DROP_3
#define HYSJ_DROP_5(...) HYSJ_DROP_4
#define HYSJ_DROP_6(...) HYSJ_DROP_5
#define HYSJ_DROP_7(...) HYSJ_DROP_6
#define HYSJ_DROP_8(...) HYSJ_DROP_7
#define HYSJ_DROP_9(...) HYSJ_DROP_8
#define HYSJ_DROP_10(...) HYSJ_DROP_9
#define HYSJ_DROP_11(...) HYSJ_DROP_10
#define HYSJ_DROP_12(...) HYSJ_DROP_11
#define HYSJ_DROP_13(...) HYSJ_DROP_12
#define HYSJ_DROP_14(...) HYSJ_DROP_13
#define HYSJ_DROP_15(...) HYSJ_DROP_14
#define HYSJ_DROP_16(...) HYSJ_DROP_15
#define HYSJ_DROP_17(...) HYSJ_DROP_16
#define HYSJ_DROP_18(...) HYSJ_DROP_17
#define HYSJ_DROP_19(...) HYSJ_DROP_18
#define HYSJ_DROP_20(...) HYSJ_DROP_19
#define HYSJ_DROP_21(...) HYSJ_DROP_20
#define HYSJ_DROP_22(...) HYSJ_DROP_21
#define HYSJ_DROP_23(...) HYSJ_DROP_22
#define HYSJ_DROP_24(...) HYSJ_DROP_23
#define HYSJ_DROP_25(...) HYSJ_DROP_24
#define HYSJ_DROP_26(...) HYSJ_DROP_25
#define HYSJ_DROP_27(...) HYSJ_DROP_26
#define HYSJ_DROP_28(...) HYSJ_DROP_27
#define HYSJ_DROP_29(...) HYSJ_DROP_28
#define HYSJ_DROP_30(...) HYSJ_DROP_29
#define HYSJ_DROP_31(...) HYSJ_DROP_30
#define HYSJ_DROP_32(...) HYSJ_DROP_31
#define HYSJ_DROP_33(...) HYSJ_DROP_32
#define HYSJ_DROP_34(...) HYSJ_DROP_33
#define HYSJ_DROP_35(...) HYSJ_DROP_34
#define HYSJ_DROP_36(...) HYSJ_DROP_35
#define HYSJ_DROP_37(...) HYSJ_DROP_36
#define HYSJ_DROP_38(...) HYSJ_DROP_37
#define HYSJ_DROP_39(...) HYSJ_DROP_38
#define HYSJ_DROP_40(...) HYSJ_DROP_39
#define HYSJ_DROP_41(...) HYSJ_DROP_40
#define HYSJ_DROP_42(...) HYSJ_DROP_41
#define HYSJ_DROP_43(...) HYSJ_DROP_42
#define HYSJ_DROP_44(...) HYSJ_DROP_43
#define HYSJ_DROP_45(...) HYSJ_DROP_44
#define HYSJ_DROP_46(...) HYSJ_DROP_45
#define HYSJ_DROP_47(...) HYSJ_DROP_46
#define HYSJ_DROP_48(...) HYSJ_DROP_47
#define HYSJ_DROP_49(...) HYSJ_DROP_48
#define HYSJ_DROP_50(...) HYSJ_DROP_49
#define HYSJ_DROP_51(...) HYSJ_DROP_50
#define HYSJ_DROP_52(...) HYSJ_DROP_51
#define HYSJ_DROP_53(...) HYSJ_DROP_52
#define HYSJ_DROP_54(...) HYSJ_DROP_53
#define HYSJ_DROP_55(...) HYSJ_DROP_54
#define HYSJ_DROP_56(...) HYSJ_DROP_55
#define HYSJ_DROP_57(...) HYSJ_DROP_56
#define HYSJ_DROP_58(...) HYSJ_DROP_57
#define HYSJ_DROP_59(...) HYSJ_DROP_58
#define HYSJ_DROP_60(...) HYSJ_DROP_59
#define HYSJ_DROP_61(...) HYSJ_DROP_60
#define HYSJ_DROP_62(...) HYSJ_DROP_61
#define HYSJ_DROP_63(...) HYSJ_DROP_62
#define HYSJ_DROP_64(...) HYSJ_DROP_63
#define HYSJ_DROP_65(...) HYSJ_DROP_64
#define HYSJ_DROP_66(...) HYSJ_DROP_65
#define HYSJ_DROP_67(...) HYSJ_DROP_66
#define HYSJ_DROP_68(...) HYSJ_DROP_67
#define HYSJ_DROP_69(...) HYSJ_DROP_68
#define HYSJ_DROP_70(...) HYSJ_DROP_69
#define HYSJ_DROP_71(...) HYSJ_DROP_70
#define HYSJ_DROP_72(...) HYSJ_DROP_71
#define HYSJ_DROP_73(...) HYSJ_DROP_72
#define HYSJ_DROP_74(...) HYSJ_DROP_73
#define HYSJ_DROP_75(...) HYSJ_DROP_74
#define HYSJ_DROP_76(...) HYSJ_DROP_75
#define HYSJ_DROP_77(...) HYSJ_DROP_76
#define HYSJ_DROP_78(...) HYSJ_DROP_77
#define HYSJ_DROP_79(...) HYSJ_DROP_78
#define HYSJ_DROP_80(...) HYSJ_DROP_79
#define HYSJ_DROP_81(...) HYSJ_DROP_80
#define HYSJ_DROP_82(...) HYSJ_DROP_81
#define HYSJ_DROP_83(...) HYSJ_DROP_82
#define HYSJ_DROP_84(...) HYSJ_DROP_83
#define HYSJ_DROP_85(...) HYSJ_DROP_84
#define HYSJ_DROP_86(...) HYSJ_DROP_85
#define HYSJ_DROP_87(...) HYSJ_DROP_86
#define HYSJ_DROP_88(...) HYSJ_DROP_87
#define HYSJ_DROP_89(...) HYSJ_DROP_88
#define HYSJ_DROP_90(...) HYSJ_DROP_89
#define HYSJ_DROP_91(...) HYSJ_DROP_90
#define HYSJ_DROP_92(...) HYSJ_DROP_91
#define HYSJ_DROP_93(...) HYSJ_DROP_92
#define HYSJ_DROP_94(...) HYSJ_DROP_93
#define HYSJ_DROP_95(...) HYSJ_DROP_94
#define HYSJ_DROP_96(...) HYSJ_DROP_95
#define HYSJ_DROP_97(...) HYSJ_DROP_96
#define HYSJ_DROP_98(...) HYSJ_DROP_97
#define HYSJ_DROP_99(...) HYSJ_DROP_98
#define HYSJ_DROP_100(...) HYSJ_DROP_99

#define HYSJ_TAKE(count,sequence)                               \
  HYSJ_CAT(HYSJ_TAKE_IMPL_,HYSJ_BOOL(count))(count,sequence)
#define HYSJ_TAKE_IMPL_1(count,sequence) \
  HYSJ_CAT(HYSJ_TAKE_,count) sequence)
#define HYSJ_TAKE_IMPL_0(count,sequence)

#define HYSJ_TAKE_1(...) (__VA_ARGS__) HYSJ_SWALLOW(
#define HYSJ_TAKE_2(...) (__VA_ARGS__) HYSJ_TAKE_1
#define HYSJ_TAKE_3(...) (__VA_ARGS__) HYSJ_TAKE_2
#define HYSJ_TAKE_4(...) (__VA_ARGS__) HYSJ_TAKE_3
#define HYSJ_TAKE_5(...) (__VA_ARGS__) HYSJ_TAKE_4
#define HYSJ_TAKE_6(...) (__VA_ARGS__) HYSJ_TAKE_5
#define HYSJ_TAKE_7(...) (__VA_ARGS__) HYSJ_TAKE_6
#define HYSJ_TAKE_8(...) (__VA_ARGS__) HYSJ_TAKE_7
#define HYSJ_TAKE_9(...) (__VA_ARGS__) HYSJ_TAKE_8
#define HYSJ_TAKE_10(...) (__VA_ARGS__) HYSJ_TAKE_9
#define HYSJ_TAKE_11(...) (__VA_ARGS__) HYSJ_TAKE_10
#define HYSJ_TAKE_12(...) (__VA_ARGS__) HYSJ_TAKE_11
#define HYSJ_TAKE_13(...) (__VA_ARGS__) HYSJ_TAKE_12
#define HYSJ_TAKE_14(...) (__VA_ARGS__) HYSJ_TAKE_13
#define HYSJ_TAKE_15(...) (__VA_ARGS__) HYSJ_TAKE_14
#define HYSJ_TAKE_16(...) (__VA_ARGS__) HYSJ_TAKE_15
#define HYSJ_TAKE_17(...) (__VA_ARGS__) HYSJ_TAKE_16
#define HYSJ_TAKE_18(...) (__VA_ARGS__) HYSJ_TAKE_17
#define HYSJ_TAKE_19(...) (__VA_ARGS__) HYSJ_TAKE_18
#define HYSJ_TAKE_20(...) (__VA_ARGS__) HYSJ_TAKE_19
#define HYSJ_TAKE_21(...) (__VA_ARGS__) HYSJ_TAKE_20
#define HYSJ_TAKE_22(...) (__VA_ARGS__) HYSJ_TAKE_21
#define HYSJ_TAKE_23(...) (__VA_ARGS__) HYSJ_TAKE_22
#define HYSJ_TAKE_24(...) (__VA_ARGS__) HYSJ_TAKE_23
#define HYSJ_TAKE_25(...) (__VA_ARGS__) HYSJ_TAKE_24
#define HYSJ_TAKE_26(...) (__VA_ARGS__) HYSJ_TAKE_25
#define HYSJ_TAKE_27(...) (__VA_ARGS__) HYSJ_TAKE_26
#define HYSJ_TAKE_28(...) (__VA_ARGS__) HYSJ_TAKE_27
#define HYSJ_TAKE_29(...) (__VA_ARGS__) HYSJ_TAKE_28
#define HYSJ_TAKE_30(...) (__VA_ARGS__) HYSJ_TAKE_29
#define HYSJ_TAKE_31(...) (__VA_ARGS__) HYSJ_TAKE_30
#define HYSJ_TAKE_32(...) (__VA_ARGS__) HYSJ_TAKE_31
#define HYSJ_TAKE_33(...) (__VA_ARGS__) HYSJ_TAKE_32
#define HYSJ_TAKE_34(...) (__VA_ARGS__) HYSJ_TAKE_33
#define HYSJ_TAKE_35(...) (__VA_ARGS__) HYSJ_TAKE_34
#define HYSJ_TAKE_36(...) (__VA_ARGS__) HYSJ_TAKE_35
#define HYSJ_TAKE_37(...) (__VA_ARGS__) HYSJ_TAKE_36
#define HYSJ_TAKE_38(...) (__VA_ARGS__) HYSJ_TAKE_37
#define HYSJ_TAKE_39(...) (__VA_ARGS__) HYSJ_TAKE_38
#define HYSJ_TAKE_40(...) (__VA_ARGS__) HYSJ_TAKE_39
#define HYSJ_TAKE_41(...) (__VA_ARGS__) HYSJ_TAKE_40
#define HYSJ_TAKE_42(...) (__VA_ARGS__) HYSJ_TAKE_41
#define HYSJ_TAKE_43(...) (__VA_ARGS__) HYSJ_TAKE_42
#define HYSJ_TAKE_44(...) (__VA_ARGS__) HYSJ_TAKE_43
#define HYSJ_TAKE_45(...) (__VA_ARGS__) HYSJ_TAKE_44
#define HYSJ_TAKE_46(...) (__VA_ARGS__) HYSJ_TAKE_45
#define HYSJ_TAKE_47(...) (__VA_ARGS__) HYSJ_TAKE_46
#define HYSJ_TAKE_48(...) (__VA_ARGS__) HYSJ_TAKE_47
#define HYSJ_TAKE_49(...) (__VA_ARGS__) HYSJ_TAKE_48
#define HYSJ_TAKE_50(...) (__VA_ARGS__) HYSJ_TAKE_49
#define HYSJ_TAKE_51(...) (__VA_ARGS__) HYSJ_TAKE_50
#define HYSJ_TAKE_52(...) (__VA_ARGS__) HYSJ_TAKE_51
#define HYSJ_TAKE_53(...) (__VA_ARGS__) HYSJ_TAKE_52
#define HYSJ_TAKE_54(...) (__VA_ARGS__) HYSJ_TAKE_53
#define HYSJ_TAKE_55(...) (__VA_ARGS__) HYSJ_TAKE_54
#define HYSJ_TAKE_56(...) (__VA_ARGS__) HYSJ_TAKE_55
#define HYSJ_TAKE_57(...) (__VA_ARGS__) HYSJ_TAKE_56
#define HYSJ_TAKE_58(...) (__VA_ARGS__) HYSJ_TAKE_57
#define HYSJ_TAKE_59(...) (__VA_ARGS__) HYSJ_TAKE_58
#define HYSJ_TAKE_60(...) (__VA_ARGS__) HYSJ_TAKE_59
#define HYSJ_TAKE_61(...) (__VA_ARGS__) HYSJ_TAKE_60
#define HYSJ_TAKE_62(...) (__VA_ARGS__) HYSJ_TAKE_61
#define HYSJ_TAKE_63(...) (__VA_ARGS__) HYSJ_TAKE_62
#define HYSJ_TAKE_64(...) (__VA_ARGS__) HYSJ_TAKE_63
#define HYSJ_TAKE_65(...) (__VA_ARGS__) HYSJ_TAKE_64
#define HYSJ_TAKE_66(...) (__VA_ARGS__) HYSJ_TAKE_65
#define HYSJ_TAKE_67(...) (__VA_ARGS__) HYSJ_TAKE_66
#define HYSJ_TAKE_68(...) (__VA_ARGS__) HYSJ_TAKE_67
#define HYSJ_TAKE_69(...) (__VA_ARGS__) HYSJ_TAKE_68
#define HYSJ_TAKE_70(...) (__VA_ARGS__) HYSJ_TAKE_69
#define HYSJ_TAKE_71(...) (__VA_ARGS__) HYSJ_TAKE_70
#define HYSJ_TAKE_72(...) (__VA_ARGS__) HYSJ_TAKE_71
#define HYSJ_TAKE_73(...) (__VA_ARGS__) HYSJ_TAKE_72
#define HYSJ_TAKE_74(...) (__VA_ARGS__) HYSJ_TAKE_73
#define HYSJ_TAKE_75(...) (__VA_ARGS__) HYSJ_TAKE_74
#define HYSJ_TAKE_76(...) (__VA_ARGS__) HYSJ_TAKE_75
#define HYSJ_TAKE_77(...) (__VA_ARGS__) HYSJ_TAKE_76
#define HYSJ_TAKE_78(...) (__VA_ARGS__) HYSJ_TAKE_77
#define HYSJ_TAKE_79(...) (__VA_ARGS__) HYSJ_TAKE_78
#define HYSJ_TAKE_80(...) (__VA_ARGS__) HYSJ_TAKE_79
#define HYSJ_TAKE_81(...) (__VA_ARGS__) HYSJ_TAKE_80
#define HYSJ_TAKE_82(...) (__VA_ARGS__) HYSJ_TAKE_81
#define HYSJ_TAKE_83(...) (__VA_ARGS__) HYSJ_TAKE_82
#define HYSJ_TAKE_84(...) (__VA_ARGS__) HYSJ_TAKE_83
#define HYSJ_TAKE_85(...) (__VA_ARGS__) HYSJ_TAKE_84
#define HYSJ_TAKE_86(...) (__VA_ARGS__) HYSJ_TAKE_85
#define HYSJ_TAKE_87(...) (__VA_ARGS__) HYSJ_TAKE_86
#define HYSJ_TAKE_88(...) (__VA_ARGS__) HYSJ_TAKE_87
#define HYSJ_TAKE_89(...) (__VA_ARGS__) HYSJ_TAKE_88
#define HYSJ_TAKE_90(...) (__VA_ARGS__) HYSJ_TAKE_89
#define HYSJ_TAKE_91(...) (__VA_ARGS__) HYSJ_TAKE_90
#define HYSJ_TAKE_92(...) (__VA_ARGS__) HYSJ_TAKE_91
#define HYSJ_TAKE_93(...) (__VA_ARGS__) HYSJ_TAKE_92
#define HYSJ_TAKE_94(...) (__VA_ARGS__) HYSJ_TAKE_93
#define HYSJ_TAKE_95(...) (__VA_ARGS__) HYSJ_TAKE_94
#define HYSJ_TAKE_96(...) (__VA_ARGS__) HYSJ_TAKE_95
#define HYSJ_TAKE_97(...) (__VA_ARGS__) HYSJ_TAKE_96
#define HYSJ_TAKE_98(...) (__VA_ARGS__) HYSJ_TAKE_97
#define HYSJ_TAKE_99(...) (__VA_ARGS__) HYSJ_TAKE_98
#define HYSJ_TAKE_100(...) (__VA_ARGS__) HYSJ_TAKE_99

#define HYSJ_HEAD(sequence) HYSJ_TAKE(1,sequence)
#define HYSJ_TAIL(sequence) HYSJ_DROP(1,sequence)

#define HYSJ_AT(index,sequence) HYSJ_HEAD(HYSJ_DROP(index,sequence))

#define HYSJ_NOT(boolean) HYSJ_IF(boolean,0,1)

#define HYSJ_IS_NOT_EMPTY(sequence) HYSJ_BOOL(HYSJ_SIZE(sequence))
#define HYSJ_IS_EMPTY(sequence) HYSJ_NOT(HYSJ_BOOL(HYSJ_SIZE(sequence)))

#define HYSJ_SWALLOW(x)

#define HYSJ_MAP(macro,data,sequence) HYSJ_CAT(HYSJ_MAP_IMPL_,HYSJ_SIZE(sequence))(macro,data,sequence)
#define HYSJ_MAP_VA(macro,data,...) HYSJ_MAP(macro,data,HYSJ_SEQUENCE(__VA_ARGS__))


#define HYSJ_MAP(macro,data,sequence) HYSJ_CAT(HYSJ_MAP_IMPL_,HYSJ_SIZE(sequence))(macro,data,sequence)
#define HYSJ_MAP_IMPL_CALL(macro,data,head) HYSJ_MAP_IMPL_CALL_1(macro,data,HYSJ_UNPAREN head)

#define HYSJ_MAP_IMPL_CALL_1(macro,data,head) HYSJ_MAP_IMPL_CALL_2(macro,data,head)
#define HYSJ_MAP_IMPL_CALL_2(macro,data,...) macro(data,__VA_ARGS__)

#define HYSJ_MAP_IMPL_0(macro,data,sequence)
#define HYSJ_MAP_IMPL_1(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_0(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_2(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_1(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_3(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_2(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_4(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_3(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_5(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_4(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_6(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_5(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_7(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_6(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_8(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_7(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_9(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_8(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_10(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_9(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_11(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_10(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_12(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_11(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_13(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_12(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_14(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_13(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_15(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_14(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_16(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_15(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_17(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_16(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_18(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_17(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_19(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_18(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_20(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_19(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_21(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_20(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_22(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_21(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_23(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_22(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_24(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_23(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_25(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_24(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_26(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_25(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_27(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_26(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_28(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_27(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_29(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_28(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_30(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_29(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_31(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_30(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_32(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_31(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_33(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_32(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_34(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_33(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_35(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_34(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_36(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_35(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_37(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_36(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_38(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_37(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_39(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_38(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_40(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_39(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_41(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_40(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_42(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_41(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_43(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_42(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_44(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_43(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_45(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_44(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_46(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_45(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_47(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_46(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_48(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_47(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_49(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_48(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_50(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_49(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_51(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_50(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_52(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_51(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_53(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_52(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_54(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_53(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_55(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_54(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_56(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_55(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_57(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_56(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_58(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_57(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_59(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_58(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_60(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_59(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_61(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_60(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_62(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_61(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_63(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_62(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_64(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_63(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_65(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_64(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_66(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_65(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_67(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_66(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_68(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_67(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_69(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_68(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_70(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_69(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_71(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_70(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_72(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_71(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_73(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_72(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_74(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_73(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_75(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_74(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_76(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_75(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_77(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_76(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_78(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_77(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_79(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_78(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_80(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_79(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_81(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_80(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_82(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_81(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_83(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_82(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_84(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_83(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_85(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_84(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_86(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_85(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_87(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_86(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_88(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_87(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_89(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_88(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_90(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_89(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_91(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_90(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_92(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_91(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_93(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_92(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_94(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_93(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_95(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_94(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_96(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_95(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_97(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_96(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_98(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_97(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_99(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_98(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_IMPL_100(macro,data,sequence)             \
  HYSJ_MAP_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_IMPL_99(macro,data,HYSJ_TAIL(sequence))

#define HYSJ_MAP_1(macro,data,sequence) HYSJ_CAT(HYSJ_MAP_1_IMPL_,HYSJ_SIZE(sequence))(macro,data,sequence)
#define HYSJ_MAP_1_IMPL_CALL(macro,data,head) HYSJ_MAP_1_IMPL_CALL_1(macro,data,HYSJ_UNPAREN head)

#define HYSJ_MAP_1_IMPL_CALL_1(macro,data,head) HYSJ_MAP_1_IMPL_CALL_2(macro,data,head)
#define HYSJ_MAP_1_IMPL_CALL_2(macro,data,...) macro(data,__VA_ARGS__)

#define HYSJ_MAP_1_IMPL_0(macro,data,sequence)
#define HYSJ_MAP_1_IMPL_1(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_0(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_2(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_1(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_3(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_2(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_4(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_3(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_5(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_4(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_6(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_5(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_7(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_6(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_8(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_7(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_9(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_8(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_10(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_9(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_11(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_10(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_12(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_11(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_13(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_12(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_14(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_13(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_15(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_14(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_16(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_15(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_17(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_16(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_18(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_17(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_19(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_18(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_20(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_19(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_21(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_20(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_22(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_21(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_23(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_22(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_24(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_23(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_25(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_24(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_26(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_25(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_27(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_26(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_28(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_27(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_29(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_28(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_30(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_29(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_31(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_30(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_32(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_31(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_33(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_32(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_34(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_33(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_35(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_34(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_36(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_35(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_37(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_36(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_38(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_37(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_39(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_38(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_40(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_39(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_41(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_40(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_42(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_41(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_43(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_42(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_44(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_43(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_45(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_44(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_46(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_45(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_47(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_46(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_48(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_47(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_49(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_48(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_50(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_49(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_51(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_50(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_52(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_51(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_53(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_52(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_54(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_53(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_55(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_54(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_56(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_55(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_57(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_56(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_58(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_57(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_59(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_58(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_60(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_59(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_61(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_60(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_62(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_61(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_63(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_62(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_64(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_63(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_65(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_64(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_66(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_65(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_67(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_66(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_68(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_67(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_69(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_68(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_70(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_69(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_71(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_70(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_72(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_71(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_73(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_72(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_74(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_73(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_75(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_74(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_76(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_75(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_77(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_76(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_78(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_77(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_79(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_78(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_80(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_79(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_81(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_80(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_82(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_81(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_83(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_82(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_84(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_83(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_85(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_84(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_86(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_85(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_87(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_86(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_88(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_87(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_89(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_88(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_90(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_89(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_91(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_90(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_92(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_91(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_93(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_92(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_94(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_93(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_95(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_94(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_96(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_95(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_97(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_96(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_98(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_97(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_99(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_98(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_1_IMPL_100(macro,data,sequence)             \
  HYSJ_MAP_1_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_1_IMPL_99(macro,data,HYSJ_TAIL(sequence))

#define HYSJ_MAP_2(macro,data,sequence) HYSJ_CAT(HYSJ_MAP_2_IMPL_,HYSJ_SIZE(sequence))(macro,data,sequence)
#define HYSJ_MAP_2_IMPL_CALL(macro,data,head) HYSJ_MAP_2_IMPL_CALL_1(macro,data,HYSJ_UNPAREN head)

#define HYSJ_MAP_2_IMPL_CALL_1(macro,data,head) HYSJ_MAP_2_IMPL_CALL_2(macro,data,head)
#define HYSJ_MAP_2_IMPL_CALL_2(macro,data,...) macro(data,__VA_ARGS__)

#define HYSJ_MAP_2_IMPL_0(macro,data,sequence)
#define HYSJ_MAP_2_IMPL_1(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_0(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_2(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_1(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_3(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_2(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_4(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_3(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_5(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_4(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_6(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_5(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_7(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_6(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_8(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_7(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_9(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_8(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_10(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_9(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_11(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_10(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_12(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_11(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_13(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_12(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_14(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_13(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_15(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_14(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_16(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_15(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_17(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_16(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_18(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_17(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_19(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_18(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_20(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_19(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_21(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_20(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_22(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_21(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_23(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_22(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_24(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_23(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_25(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_24(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_26(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_25(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_27(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_26(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_28(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_27(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_29(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_28(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_30(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_29(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_31(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_30(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_32(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_31(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_33(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_32(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_34(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_33(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_35(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_34(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_36(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_35(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_37(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_36(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_38(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_37(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_39(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_38(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_40(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_39(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_41(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_40(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_42(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_41(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_43(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_42(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_44(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_43(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_45(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_44(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_46(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_45(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_47(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_46(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_48(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_47(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_49(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_48(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_50(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_49(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_51(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_50(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_52(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_51(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_53(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_52(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_54(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_53(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_55(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_54(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_56(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_55(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_57(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_56(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_58(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_57(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_59(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_58(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_60(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_59(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_61(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_60(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_62(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_61(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_63(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_62(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_64(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_63(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_65(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_64(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_66(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_65(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_67(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_66(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_68(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_67(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_69(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_68(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_70(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_69(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_71(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_70(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_72(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_71(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_73(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_72(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_74(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_73(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_75(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_74(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_76(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_75(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_77(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_76(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_78(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_77(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_79(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_78(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_80(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_79(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_81(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_80(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_82(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_81(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_83(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_82(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_84(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_83(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_85(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_84(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_86(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_85(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_87(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_86(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_88(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_87(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_89(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_88(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_90(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_89(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_91(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_90(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_92(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_91(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_93(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_92(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_94(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_93(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_95(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_94(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_96(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_95(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_97(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_96(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_98(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_97(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_99(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_98(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_2_IMPL_100(macro,data,sequence)             \
  HYSJ_MAP_2_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_2_IMPL_99(macro,data,HYSJ_TAIL(sequence))

#define HYSJ_MAP_3(macro,data,sequence) HYSJ_CAT(HYSJ_MAP_3_IMPL_,HYSJ_SIZE(sequence))(macro,data,sequence)
#define HYSJ_MAP_3_IMPL_CALL(macro,data,head) HYSJ_MAP_3_IMPL_CALL_1(macro,data,HYSJ_UNPAREN head)

#define HYSJ_MAP_3_IMPL_CALL_1(macro,data,head) HYSJ_MAP_3_IMPL_CALL_2(macro,data,head)
#define HYSJ_MAP_3_IMPL_CALL_2(macro,data,...) macro(data,__VA_ARGS__)

#define HYSJ_MAP_3_IMPL_0(macro,data,sequence)
#define HYSJ_MAP_3_IMPL_1(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_0(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_2(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_1(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_3(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_2(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_4(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_3(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_5(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_4(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_6(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_5(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_7(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_6(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_8(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_7(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_9(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_8(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_10(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_9(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_11(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_10(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_12(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_11(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_13(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_12(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_14(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_13(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_15(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_14(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_16(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_15(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_17(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_16(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_18(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_17(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_19(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_18(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_20(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_19(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_21(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_20(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_22(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_21(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_23(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_22(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_24(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_23(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_25(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_24(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_26(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_25(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_27(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_26(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_28(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_27(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_29(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_28(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_30(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_29(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_31(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_30(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_32(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_31(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_33(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_32(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_34(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_33(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_35(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_34(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_36(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_35(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_37(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_36(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_38(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_37(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_39(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_38(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_40(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_39(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_41(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_40(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_42(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_41(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_43(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_42(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_44(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_43(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_45(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_44(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_46(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_45(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_47(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_46(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_48(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_47(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_49(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_48(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_50(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_49(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_51(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_50(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_52(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_51(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_53(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_52(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_54(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_53(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_55(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_54(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_56(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_55(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_57(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_56(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_58(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_57(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_59(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_58(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_60(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_59(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_61(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_60(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_62(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_61(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_63(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_62(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_64(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_63(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_65(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_64(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_66(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_65(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_67(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_66(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_68(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_67(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_69(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_68(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_70(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_69(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_71(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_70(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_72(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_71(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_73(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_72(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_74(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_73(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_75(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_74(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_76(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_75(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_77(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_76(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_78(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_77(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_79(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_78(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_80(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_79(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_81(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_80(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_82(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_81(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_83(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_82(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_84(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_83(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_85(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_84(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_86(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_85(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_87(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_86(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_88(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_87(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_89(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_88(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_90(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_89(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_91(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_90(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_92(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_91(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_93(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_92(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_94(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_93(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_95(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_94(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_96(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_95(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_97(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_96(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_98(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_97(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_99(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_98(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_MAP_3_IMPL_100(macro,data,sequence)             \
  HYSJ_MAP_3_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \
  HYSJ_MAP_3_IMPL_99(macro,data,HYSJ_TAIL(sequence))
#define HYSJ_PRODUCT(sequences)                                 \
  HYSJ_CAT(HYSJ_PRODUCT_IMPL_,HYSJ_SIZE(sequences))(sequences)

#define HYSJ_PRODUCT_MAP_F(right_something,...) \
  (__VA_ARGS__,HYSJ_UNWRAP(right_something))
#define HYSJ_PRODUCT_MAP(left_sequence,...)                     \
  HYSJ_MAP(HYSJ_PRODUCT_MAP_F,(__VA_ARGS__),left_sequence)
#define HYSJ_PRODUCT_IMPL_0(sequences)

#define HYSJ_PRODUCT_IMPL_1(sequences) \
  HYSJ_UNWRAP(HYSJ_HEAD(sequences))

#define HYSJ_PRODUCT_IMPL_2(sequences)                         \
  HYSJ_MAP_1(HYSJ_PRODUCT_MAP,                                 \
             HYSJ_PRODUCT_IMPL_1(HYSJ_TAKE(1,sequences)),      \
             HYSJ_UNWRAP(HYSJ_DROP(1,sequences)))

#define HYSJ_PRODUCT_IMPL_3(sequences)                         \
  HYSJ_MAP_1(HYSJ_PRODUCT_MAP,                                 \
             HYSJ_PRODUCT_IMPL_2(HYSJ_TAKE(2,sequences)),      \
             HYSJ_UNWRAP(HYSJ_DROP(2,sequences)))

#define HYSJ_PRODUCT_IMPL_4(sequences)                         \
  HYSJ_MAP_1(HYSJ_PRODUCT_MAP,                                 \
             HYSJ_PRODUCT_IMPL_3(HYSJ_TAKE(3,sequences)),      \
             HYSJ_UNWRAP(HYSJ_DROP(3,sequences)))

#define HYSJ_EXTRACT(_0, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20, _21, _22, _23, _24, _25, _26, _27, _28, _29, _30, _31, _32, _33, _34, _35, _36, _37, _38, _39, _40, _41, _42, _43, _44, _45, _46, _47, _48, _49, _50, _51, _52, _53, _54, _55, _56, _57, _58, _59, _60, _61, _62, _63, _64, _65, _66, _67, _68, _69, _70, _71, _72, _73, _74, _75, _76, _77, _78, _79, _80, _81, _82, _83, _84, _85, _86, _87, _88, _89, _90, _91, _92, _93, _94, _95, _96, _97, _98, _99, V, ...) V

#define HYSJ_VA_COUNT(...) HYSJ_EXTRACT(__VA_ARGS__ __VA_OPT__(,) 100,99,98,97,96,95,94,93,92,91,90,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,65,64,63,62,61,60,59,58,57,56,55,54,53,52,51,50,49,48,47,46,45,44,43,42,41,40,39,38,37,36,35,34,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0)

#define HYSJ_SEQUENCE(...) HYSJ_CAT(HYSJ_SEQUENCE_IMPL_,HYSJ_VA_COUNT(__VA_ARGS__))(__VA_ARGS__)

#define HYSJ_SEQUENCE_IMPL_0() 
#define HYSJ_SEQUENCE_IMPL_1(x0) (x0)
#define HYSJ_SEQUENCE_IMPL_2(x0,x1) (x0)(x1)
#define HYSJ_SEQUENCE_IMPL_3(x0,x1,x2) (x0)(x1)(x2)
#define HYSJ_SEQUENCE_IMPL_4(x0,x1,x2,x3) (x0)(x1)(x2)(x3)
#define HYSJ_SEQUENCE_IMPL_5(x0,x1,x2,x3,x4) (x0)(x1)(x2)(x3)(x4)
#define HYSJ_SEQUENCE_IMPL_6(x0,x1,x2,x3,x4,x5) (x0)(x1)(x2)(x3)(x4)(x5)
#define HYSJ_SEQUENCE_IMPL_7(x0,x1,x2,x3,x4,x5,x6) (x0)(x1)(x2)(x3)(x4)(x5)(x6)
#define HYSJ_SEQUENCE_IMPL_8(x0,x1,x2,x3,x4,x5,x6,x7) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)
#define HYSJ_SEQUENCE_IMPL_9(x0,x1,x2,x3,x4,x5,x6,x7,x8) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)
#define HYSJ_SEQUENCE_IMPL_10(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)
#define HYSJ_SEQUENCE_IMPL_11(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)
#define HYSJ_SEQUENCE_IMPL_12(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)
#define HYSJ_SEQUENCE_IMPL_13(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)
#define HYSJ_SEQUENCE_IMPL_14(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)
#define HYSJ_SEQUENCE_IMPL_15(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)
#define HYSJ_SEQUENCE_IMPL_16(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)
#define HYSJ_SEQUENCE_IMPL_17(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)
#define HYSJ_SEQUENCE_IMPL_18(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)
#define HYSJ_SEQUENCE_IMPL_19(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)
#define HYSJ_SEQUENCE_IMPL_20(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)
#define HYSJ_SEQUENCE_IMPL_21(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)
#define HYSJ_SEQUENCE_IMPL_22(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)
#define HYSJ_SEQUENCE_IMPL_23(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)
#define HYSJ_SEQUENCE_IMPL_24(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)
#define HYSJ_SEQUENCE_IMPL_25(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)
#define HYSJ_SEQUENCE_IMPL_26(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)
#define HYSJ_SEQUENCE_IMPL_27(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)
#define HYSJ_SEQUENCE_IMPL_28(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)
#define HYSJ_SEQUENCE_IMPL_29(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)
#define HYSJ_SEQUENCE_IMPL_30(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)
#define HYSJ_SEQUENCE_IMPL_31(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)
#define HYSJ_SEQUENCE_IMPL_32(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)
#define HYSJ_SEQUENCE_IMPL_33(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)
#define HYSJ_SEQUENCE_IMPL_34(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)
#define HYSJ_SEQUENCE_IMPL_35(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)
#define HYSJ_SEQUENCE_IMPL_36(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)
#define HYSJ_SEQUENCE_IMPL_37(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)
#define HYSJ_SEQUENCE_IMPL_38(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)
#define HYSJ_SEQUENCE_IMPL_39(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)
#define HYSJ_SEQUENCE_IMPL_40(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)
#define HYSJ_SEQUENCE_IMPL_41(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)
#define HYSJ_SEQUENCE_IMPL_42(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)
#define HYSJ_SEQUENCE_IMPL_43(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)
#define HYSJ_SEQUENCE_IMPL_44(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)
#define HYSJ_SEQUENCE_IMPL_45(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)
#define HYSJ_SEQUENCE_IMPL_46(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)
#define HYSJ_SEQUENCE_IMPL_47(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)
#define HYSJ_SEQUENCE_IMPL_48(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)
#define HYSJ_SEQUENCE_IMPL_49(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)
#define HYSJ_SEQUENCE_IMPL_50(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)
#define HYSJ_SEQUENCE_IMPL_51(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)
#define HYSJ_SEQUENCE_IMPL_52(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)
#define HYSJ_SEQUENCE_IMPL_53(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)
#define HYSJ_SEQUENCE_IMPL_54(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)
#define HYSJ_SEQUENCE_IMPL_55(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53,x54) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)(x54)
#define HYSJ_SEQUENCE_IMPL_56(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53,x54,x55) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)(x54)(x55)
#define HYSJ_SEQUENCE_IMPL_57(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53,x54,x55,x56) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)(x54)(x55)(x56)
#define HYSJ_SEQUENCE_IMPL_58(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53,x54,x55,x56,x57) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)(x54)(x55)(x56)(x57)
#define HYSJ_SEQUENCE_IMPL_59(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53,x54,x55,x56,x57,x58) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)(x54)(x55)(x56)(x57)(x58)
#define HYSJ_SEQUENCE_IMPL_60(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53,x54,x55,x56,x57,x58,x59) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)(x54)(x55)(x56)(x57)(x58)(x59)
#define HYSJ_SEQUENCE_IMPL_61(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53,x54,x55,x56,x57,x58,x59,x60) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)(x54)(x55)(x56)(x57)(x58)(x59)(x60)
#define HYSJ_SEQUENCE_IMPL_62(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53,x54,x55,x56,x57,x58,x59,x60,x61) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)(x54)(x55)(x56)(x57)(x58)(x59)(x60)(x61)
#define HYSJ_SEQUENCE_IMPL_63(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53,x54,x55,x56,x57,x58,x59,x60,x61,x62) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)(x54)(x55)(x56)(x57)(x58)(x59)(x60)(x61)(x62)
#define HYSJ_SEQUENCE_IMPL_64(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53,x54,x55,x56,x57,x58,x59,x60,x61,x62,x63) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)(x54)(x55)(x56)(x57)(x58)(x59)(x60)(x61)(x62)(x63)
#define HYSJ_SEQUENCE_IMPL_65(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53,x54,x55,x56,x57,x58,x59,x60,x61,x62,x63,x64) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)(x54)(x55)(x56)(x57)(x58)(x59)(x60)(x61)(x62)(x63)(x64)
#define HYSJ_SEQUENCE_IMPL_66(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53,x54,x55,x56,x57,x58,x59,x60,x61,x62,x63,x64,x65) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)(x54)(x55)(x56)(x57)(x58)(x59)(x60)(x61)(x62)(x63)(x64)(x65)
#define HYSJ_SEQUENCE_IMPL_67(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53,x54,x55,x56,x57,x58,x59,x60,x61,x62,x63,x64,x65,x66) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)(x54)(x55)(x56)(x57)(x58)(x59)(x60)(x61)(x62)(x63)(x64)(x65)(x66)
#define HYSJ_SEQUENCE_IMPL_68(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53,x54,x55,x56,x57,x58,x59,x60,x61,x62,x63,x64,x65,x66,x67) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)(x54)(x55)(x56)(x57)(x58)(x59)(x60)(x61)(x62)(x63)(x64)(x65)(x66)(x67)
#define HYSJ_SEQUENCE_IMPL_69(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53,x54,x55,x56,x57,x58,x59,x60,x61,x62,x63,x64,x65,x66,x67,x68) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)(x54)(x55)(x56)(x57)(x58)(x59)(x60)(x61)(x62)(x63)(x64)(x65)(x66)(x67)(x68)
#define HYSJ_SEQUENCE_IMPL_70(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53,x54,x55,x56,x57,x58,x59,x60,x61,x62,x63,x64,x65,x66,x67,x68,x69) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)(x54)(x55)(x56)(x57)(x58)(x59)(x60)(x61)(x62)(x63)(x64)(x65)(x66)(x67)(x68)(x69)
#define HYSJ_SEQUENCE_IMPL_71(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53,x54,x55,x56,x57,x58,x59,x60,x61,x62,x63,x64,x65,x66,x67,x68,x69,x70) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)(x54)(x55)(x56)(x57)(x58)(x59)(x60)(x61)(x62)(x63)(x64)(x65)(x66)(x67)(x68)(x69)(x70)
#define HYSJ_SEQUENCE_IMPL_72(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53,x54,x55,x56,x57,x58,x59,x60,x61,x62,x63,x64,x65,x66,x67,x68,x69,x70,x71) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)(x54)(x55)(x56)(x57)(x58)(x59)(x60)(x61)(x62)(x63)(x64)(x65)(x66)(x67)(x68)(x69)(x70)(x71)
#define HYSJ_SEQUENCE_IMPL_73(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53,x54,x55,x56,x57,x58,x59,x60,x61,x62,x63,x64,x65,x66,x67,x68,x69,x70,x71,x72) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)(x54)(x55)(x56)(x57)(x58)(x59)(x60)(x61)(x62)(x63)(x64)(x65)(x66)(x67)(x68)(x69)(x70)(x71)(x72)
#define HYSJ_SEQUENCE_IMPL_74(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53,x54,x55,x56,x57,x58,x59,x60,x61,x62,x63,x64,x65,x66,x67,x68,x69,x70,x71,x72,x73) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)(x54)(x55)(x56)(x57)(x58)(x59)(x60)(x61)(x62)(x63)(x64)(x65)(x66)(x67)(x68)(x69)(x70)(x71)(x72)(x73)
#define HYSJ_SEQUENCE_IMPL_75(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53,x54,x55,x56,x57,x58,x59,x60,x61,x62,x63,x64,x65,x66,x67,x68,x69,x70,x71,x72,x73,x74) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)(x54)(x55)(x56)(x57)(x58)(x59)(x60)(x61)(x62)(x63)(x64)(x65)(x66)(x67)(x68)(x69)(x70)(x71)(x72)(x73)(x74)
#define HYSJ_SEQUENCE_IMPL_76(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53,x54,x55,x56,x57,x58,x59,x60,x61,x62,x63,x64,x65,x66,x67,x68,x69,x70,x71,x72,x73,x74,x75) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)(x54)(x55)(x56)(x57)(x58)(x59)(x60)(x61)(x62)(x63)(x64)(x65)(x66)(x67)(x68)(x69)(x70)(x71)(x72)(x73)(x74)(x75)
#define HYSJ_SEQUENCE_IMPL_77(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53,x54,x55,x56,x57,x58,x59,x60,x61,x62,x63,x64,x65,x66,x67,x68,x69,x70,x71,x72,x73,x74,x75,x76) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)(x54)(x55)(x56)(x57)(x58)(x59)(x60)(x61)(x62)(x63)(x64)(x65)(x66)(x67)(x68)(x69)(x70)(x71)(x72)(x73)(x74)(x75)(x76)
#define HYSJ_SEQUENCE_IMPL_78(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53,x54,x55,x56,x57,x58,x59,x60,x61,x62,x63,x64,x65,x66,x67,x68,x69,x70,x71,x72,x73,x74,x75,x76,x77) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)(x54)(x55)(x56)(x57)(x58)(x59)(x60)(x61)(x62)(x63)(x64)(x65)(x66)(x67)(x68)(x69)(x70)(x71)(x72)(x73)(x74)(x75)(x76)(x77)
#define HYSJ_SEQUENCE_IMPL_79(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53,x54,x55,x56,x57,x58,x59,x60,x61,x62,x63,x64,x65,x66,x67,x68,x69,x70,x71,x72,x73,x74,x75,x76,x77,x78) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)(x54)(x55)(x56)(x57)(x58)(x59)(x60)(x61)(x62)(x63)(x64)(x65)(x66)(x67)(x68)(x69)(x70)(x71)(x72)(x73)(x74)(x75)(x76)(x77)(x78)
#define HYSJ_SEQUENCE_IMPL_80(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53,x54,x55,x56,x57,x58,x59,x60,x61,x62,x63,x64,x65,x66,x67,x68,x69,x70,x71,x72,x73,x74,x75,x76,x77,x78,x79) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)(x54)(x55)(x56)(x57)(x58)(x59)(x60)(x61)(x62)(x63)(x64)(x65)(x66)(x67)(x68)(x69)(x70)(x71)(x72)(x73)(x74)(x75)(x76)(x77)(x78)(x79)
#define HYSJ_SEQUENCE_IMPL_81(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53,x54,x55,x56,x57,x58,x59,x60,x61,x62,x63,x64,x65,x66,x67,x68,x69,x70,x71,x72,x73,x74,x75,x76,x77,x78,x79,x80) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)(x54)(x55)(x56)(x57)(x58)(x59)(x60)(x61)(x62)(x63)(x64)(x65)(x66)(x67)(x68)(x69)(x70)(x71)(x72)(x73)(x74)(x75)(x76)(x77)(x78)(x79)(x80)
#define HYSJ_SEQUENCE_IMPL_82(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53,x54,x55,x56,x57,x58,x59,x60,x61,x62,x63,x64,x65,x66,x67,x68,x69,x70,x71,x72,x73,x74,x75,x76,x77,x78,x79,x80,x81) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)(x54)(x55)(x56)(x57)(x58)(x59)(x60)(x61)(x62)(x63)(x64)(x65)(x66)(x67)(x68)(x69)(x70)(x71)(x72)(x73)(x74)(x75)(x76)(x77)(x78)(x79)(x80)(x81)
#define HYSJ_SEQUENCE_IMPL_83(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53,x54,x55,x56,x57,x58,x59,x60,x61,x62,x63,x64,x65,x66,x67,x68,x69,x70,x71,x72,x73,x74,x75,x76,x77,x78,x79,x80,x81,x82) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)(x54)(x55)(x56)(x57)(x58)(x59)(x60)(x61)(x62)(x63)(x64)(x65)(x66)(x67)(x68)(x69)(x70)(x71)(x72)(x73)(x74)(x75)(x76)(x77)(x78)(x79)(x80)(x81)(x82)
#define HYSJ_SEQUENCE_IMPL_84(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53,x54,x55,x56,x57,x58,x59,x60,x61,x62,x63,x64,x65,x66,x67,x68,x69,x70,x71,x72,x73,x74,x75,x76,x77,x78,x79,x80,x81,x82,x83) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)(x54)(x55)(x56)(x57)(x58)(x59)(x60)(x61)(x62)(x63)(x64)(x65)(x66)(x67)(x68)(x69)(x70)(x71)(x72)(x73)(x74)(x75)(x76)(x77)(x78)(x79)(x80)(x81)(x82)(x83)
#define HYSJ_SEQUENCE_IMPL_85(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53,x54,x55,x56,x57,x58,x59,x60,x61,x62,x63,x64,x65,x66,x67,x68,x69,x70,x71,x72,x73,x74,x75,x76,x77,x78,x79,x80,x81,x82,x83,x84) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)(x54)(x55)(x56)(x57)(x58)(x59)(x60)(x61)(x62)(x63)(x64)(x65)(x66)(x67)(x68)(x69)(x70)(x71)(x72)(x73)(x74)(x75)(x76)(x77)(x78)(x79)(x80)(x81)(x82)(x83)(x84)
#define HYSJ_SEQUENCE_IMPL_86(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53,x54,x55,x56,x57,x58,x59,x60,x61,x62,x63,x64,x65,x66,x67,x68,x69,x70,x71,x72,x73,x74,x75,x76,x77,x78,x79,x80,x81,x82,x83,x84,x85) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)(x54)(x55)(x56)(x57)(x58)(x59)(x60)(x61)(x62)(x63)(x64)(x65)(x66)(x67)(x68)(x69)(x70)(x71)(x72)(x73)(x74)(x75)(x76)(x77)(x78)(x79)(x80)(x81)(x82)(x83)(x84)(x85)
#define HYSJ_SEQUENCE_IMPL_87(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53,x54,x55,x56,x57,x58,x59,x60,x61,x62,x63,x64,x65,x66,x67,x68,x69,x70,x71,x72,x73,x74,x75,x76,x77,x78,x79,x80,x81,x82,x83,x84,x85,x86) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)(x54)(x55)(x56)(x57)(x58)(x59)(x60)(x61)(x62)(x63)(x64)(x65)(x66)(x67)(x68)(x69)(x70)(x71)(x72)(x73)(x74)(x75)(x76)(x77)(x78)(x79)(x80)(x81)(x82)(x83)(x84)(x85)(x86)
#define HYSJ_SEQUENCE_IMPL_88(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53,x54,x55,x56,x57,x58,x59,x60,x61,x62,x63,x64,x65,x66,x67,x68,x69,x70,x71,x72,x73,x74,x75,x76,x77,x78,x79,x80,x81,x82,x83,x84,x85,x86,x87) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)(x54)(x55)(x56)(x57)(x58)(x59)(x60)(x61)(x62)(x63)(x64)(x65)(x66)(x67)(x68)(x69)(x70)(x71)(x72)(x73)(x74)(x75)(x76)(x77)(x78)(x79)(x80)(x81)(x82)(x83)(x84)(x85)(x86)(x87)
#define HYSJ_SEQUENCE_IMPL_89(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53,x54,x55,x56,x57,x58,x59,x60,x61,x62,x63,x64,x65,x66,x67,x68,x69,x70,x71,x72,x73,x74,x75,x76,x77,x78,x79,x80,x81,x82,x83,x84,x85,x86,x87,x88) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)(x54)(x55)(x56)(x57)(x58)(x59)(x60)(x61)(x62)(x63)(x64)(x65)(x66)(x67)(x68)(x69)(x70)(x71)(x72)(x73)(x74)(x75)(x76)(x77)(x78)(x79)(x80)(x81)(x82)(x83)(x84)(x85)(x86)(x87)(x88)
#define HYSJ_SEQUENCE_IMPL_90(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53,x54,x55,x56,x57,x58,x59,x60,x61,x62,x63,x64,x65,x66,x67,x68,x69,x70,x71,x72,x73,x74,x75,x76,x77,x78,x79,x80,x81,x82,x83,x84,x85,x86,x87,x88,x89) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)(x54)(x55)(x56)(x57)(x58)(x59)(x60)(x61)(x62)(x63)(x64)(x65)(x66)(x67)(x68)(x69)(x70)(x71)(x72)(x73)(x74)(x75)(x76)(x77)(x78)(x79)(x80)(x81)(x82)(x83)(x84)(x85)(x86)(x87)(x88)(x89)
#define HYSJ_SEQUENCE_IMPL_91(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53,x54,x55,x56,x57,x58,x59,x60,x61,x62,x63,x64,x65,x66,x67,x68,x69,x70,x71,x72,x73,x74,x75,x76,x77,x78,x79,x80,x81,x82,x83,x84,x85,x86,x87,x88,x89,x90) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)(x54)(x55)(x56)(x57)(x58)(x59)(x60)(x61)(x62)(x63)(x64)(x65)(x66)(x67)(x68)(x69)(x70)(x71)(x72)(x73)(x74)(x75)(x76)(x77)(x78)(x79)(x80)(x81)(x82)(x83)(x84)(x85)(x86)(x87)(x88)(x89)(x90)
#define HYSJ_SEQUENCE_IMPL_92(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53,x54,x55,x56,x57,x58,x59,x60,x61,x62,x63,x64,x65,x66,x67,x68,x69,x70,x71,x72,x73,x74,x75,x76,x77,x78,x79,x80,x81,x82,x83,x84,x85,x86,x87,x88,x89,x90,x91) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)(x54)(x55)(x56)(x57)(x58)(x59)(x60)(x61)(x62)(x63)(x64)(x65)(x66)(x67)(x68)(x69)(x70)(x71)(x72)(x73)(x74)(x75)(x76)(x77)(x78)(x79)(x80)(x81)(x82)(x83)(x84)(x85)(x86)(x87)(x88)(x89)(x90)(x91)
#define HYSJ_SEQUENCE_IMPL_93(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53,x54,x55,x56,x57,x58,x59,x60,x61,x62,x63,x64,x65,x66,x67,x68,x69,x70,x71,x72,x73,x74,x75,x76,x77,x78,x79,x80,x81,x82,x83,x84,x85,x86,x87,x88,x89,x90,x91,x92) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)(x54)(x55)(x56)(x57)(x58)(x59)(x60)(x61)(x62)(x63)(x64)(x65)(x66)(x67)(x68)(x69)(x70)(x71)(x72)(x73)(x74)(x75)(x76)(x77)(x78)(x79)(x80)(x81)(x82)(x83)(x84)(x85)(x86)(x87)(x88)(x89)(x90)(x91)(x92)
#define HYSJ_SEQUENCE_IMPL_94(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53,x54,x55,x56,x57,x58,x59,x60,x61,x62,x63,x64,x65,x66,x67,x68,x69,x70,x71,x72,x73,x74,x75,x76,x77,x78,x79,x80,x81,x82,x83,x84,x85,x86,x87,x88,x89,x90,x91,x92,x93) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)(x54)(x55)(x56)(x57)(x58)(x59)(x60)(x61)(x62)(x63)(x64)(x65)(x66)(x67)(x68)(x69)(x70)(x71)(x72)(x73)(x74)(x75)(x76)(x77)(x78)(x79)(x80)(x81)(x82)(x83)(x84)(x85)(x86)(x87)(x88)(x89)(x90)(x91)(x92)(x93)
#define HYSJ_SEQUENCE_IMPL_95(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53,x54,x55,x56,x57,x58,x59,x60,x61,x62,x63,x64,x65,x66,x67,x68,x69,x70,x71,x72,x73,x74,x75,x76,x77,x78,x79,x80,x81,x82,x83,x84,x85,x86,x87,x88,x89,x90,x91,x92,x93,x94) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)(x54)(x55)(x56)(x57)(x58)(x59)(x60)(x61)(x62)(x63)(x64)(x65)(x66)(x67)(x68)(x69)(x70)(x71)(x72)(x73)(x74)(x75)(x76)(x77)(x78)(x79)(x80)(x81)(x82)(x83)(x84)(x85)(x86)(x87)(x88)(x89)(x90)(x91)(x92)(x93)(x94)
#define HYSJ_SEQUENCE_IMPL_96(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53,x54,x55,x56,x57,x58,x59,x60,x61,x62,x63,x64,x65,x66,x67,x68,x69,x70,x71,x72,x73,x74,x75,x76,x77,x78,x79,x80,x81,x82,x83,x84,x85,x86,x87,x88,x89,x90,x91,x92,x93,x94,x95) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)(x54)(x55)(x56)(x57)(x58)(x59)(x60)(x61)(x62)(x63)(x64)(x65)(x66)(x67)(x68)(x69)(x70)(x71)(x72)(x73)(x74)(x75)(x76)(x77)(x78)(x79)(x80)(x81)(x82)(x83)(x84)(x85)(x86)(x87)(x88)(x89)(x90)(x91)(x92)(x93)(x94)(x95)
#define HYSJ_SEQUENCE_IMPL_97(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53,x54,x55,x56,x57,x58,x59,x60,x61,x62,x63,x64,x65,x66,x67,x68,x69,x70,x71,x72,x73,x74,x75,x76,x77,x78,x79,x80,x81,x82,x83,x84,x85,x86,x87,x88,x89,x90,x91,x92,x93,x94,x95,x96) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)(x54)(x55)(x56)(x57)(x58)(x59)(x60)(x61)(x62)(x63)(x64)(x65)(x66)(x67)(x68)(x69)(x70)(x71)(x72)(x73)(x74)(x75)(x76)(x77)(x78)(x79)(x80)(x81)(x82)(x83)(x84)(x85)(x86)(x87)(x88)(x89)(x90)(x91)(x92)(x93)(x94)(x95)(x96)
#define HYSJ_SEQUENCE_IMPL_98(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53,x54,x55,x56,x57,x58,x59,x60,x61,x62,x63,x64,x65,x66,x67,x68,x69,x70,x71,x72,x73,x74,x75,x76,x77,x78,x79,x80,x81,x82,x83,x84,x85,x86,x87,x88,x89,x90,x91,x92,x93,x94,x95,x96,x97) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)(x54)(x55)(x56)(x57)(x58)(x59)(x60)(x61)(x62)(x63)(x64)(x65)(x66)(x67)(x68)(x69)(x70)(x71)(x72)(x73)(x74)(x75)(x76)(x77)(x78)(x79)(x80)(x81)(x82)(x83)(x84)(x85)(x86)(x87)(x88)(x89)(x90)(x91)(x92)(x93)(x94)(x95)(x96)(x97)
#define HYSJ_SEQUENCE_IMPL_99(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53,x54,x55,x56,x57,x58,x59,x60,x61,x62,x63,x64,x65,x66,x67,x68,x69,x70,x71,x72,x73,x74,x75,x76,x77,x78,x79,x80,x81,x82,x83,x84,x85,x86,x87,x88,x89,x90,x91,x92,x93,x94,x95,x96,x97,x98) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)(x54)(x55)(x56)(x57)(x58)(x59)(x60)(x61)(x62)(x63)(x64)(x65)(x66)(x67)(x68)(x69)(x70)(x71)(x72)(x73)(x74)(x75)(x76)(x77)(x78)(x79)(x80)(x81)(x82)(x83)(x84)(x85)(x86)(x87)(x88)(x89)(x90)(x91)(x92)(x93)(x94)(x95)(x96)(x97)(x98)
#define HYSJ_SEQUENCE_IMPL_100(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50,x51,x52,x53,x54,x55,x56,x57,x58,x59,x60,x61,x62,x63,x64,x65,x66,x67,x68,x69,x70,x71,x72,x73,x74,x75,x76,x77,x78,x79,x80,x81,x82,x83,x84,x85,x86,x87,x88,x89,x90,x91,x92,x93,x94,x95,x96,x97,x98,x99) (x0)(x1)(x2)(x3)(x4)(x5)(x6)(x7)(x8)(x9)(x10)(x11)(x12)(x13)(x14)(x15)(x16)(x17)(x18)(x19)(x20)(x21)(x22)(x23)(x24)(x25)(x26)(x27)(x28)(x29)(x30)(x31)(x32)(x33)(x34)(x35)(x36)(x37)(x38)(x39)(x40)(x41)(x42)(x43)(x44)(x45)(x46)(x47)(x48)(x49)(x50)(x51)(x52)(x53)(x54)(x55)(x56)(x57)(x58)(x59)(x60)(x61)(x62)(x63)(x64)(x65)(x66)(x67)(x68)(x69)(x70)(x71)(x72)(x73)(x74)(x75)(x76)(x77)(x78)(x79)(x80)(x81)(x82)(x83)(x84)(x85)(x86)(x87)(x88)(x89)(x90)(x91)(x92)(x93)(x94)(x95)(x96)(x97)(x98)(x99)
#define HYSJ_FOR_EACH(F, a, ...)                \
  HYSJ_MAP(F,a,HYSJ_SEQUENCE(__VA_ARGS__))

#define HYSJ_LAMBDA_IMPL(_,...) HYSJ_LAMBDA(__VA_ARGS__)
#define HYSJ_MAP_LAMBDA(X) HYSJ_MAP_3(HYSJ_LAMBDA_IMPL,_,X)

#define HYSJ_SPLAT_F(i,...) ,HYSJ_UNWRAP(HYSJ_AT(i,HYSJ_SEQUENCE(__VA_ARGS__)))
#define HYSJ_SPLAT_G(_) HYSJ_SPLAT_H(_)
#define HYSJ_SPLAT_H(_,...) __VA_ARGS__
#define HYSJ_SPLAT(i,x)                         \
  HYSJ_SPLAT_G(_ HYSJ_MAP(HYSJ_SPLAT_F,i,x))

#define HYSJ_PROJECT_F(i,...) HYSJ_AT(i,HYSJ_SEQUENCE(__VA_ARGS__))
#define HYSJ_PROJECT(i,x)                       \
  HYSJ_MAP(HYSJ_PROJECT_F,i,x)

#define HYSJ_SELECT_F(I,...)\
  HYSJ_SELECT_H(HYSJ_MAP_1(HYSJ_SELECT_G,HYSJ_SEQUENCE(__VA_ARGS__),I))
#define HYSJ_SELECT_G(S,i) , HYSJ_UNWRAP(HYSJ_AT(i,S))
#define HYSJ_SELECT_H(_) HYSJ_SELECT_I(_)
#define HYSJ_SELECT_I(_,...)                    \
  (__VA_ARGS__)
#define HYSJ_SELECT(I,x) \
  HYSJ_MAP(HYSJ_SELECT_F,I,x)

#define HYSJ_ADD(lhs,rhs)                       \
  HYSJ_CAT(HYSJ_ADD_IMPL_,HYSJ_BOOL(HYSJ_SIZE(lhs)))(lhs,rhs)
#define HYSJ_ADD_IMPL_0(lhs,rhs) rhs
#define HYSJ_ADD_IMPL_1(lhs,rhs) \
  HYSJ_CAT(HYSJ_ADD_, HYSJ_SIZE(rhs))(lhs, rhs)

#define HYSJ_ADD_OP__(L, r) L(r)
#define HYSJ_ADD_OP_(L, r) HYSJ_ADD_OP__(L, HYSJ_UNPAREN r)
#define HYSJ_ADD_OP(L, R) HYSJ_ADD_OP_(L, HYSJ_HEAD(R))
#define HYSJ_ADD_0(L, R) L
#define HYSJ_ADD_1(L, R) HYSJ_ADD_OP(L, R)
#define HYSJ_ADD_2(L, R) HYSJ_ADD_1(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_3(L, R) HYSJ_ADD_2(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_4(L, R) HYSJ_ADD_3(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_5(L, R) HYSJ_ADD_4(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_6(L, R) HYSJ_ADD_5(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_7(L, R) HYSJ_ADD_6(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_8(L, R) HYSJ_ADD_7(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_9(L, R) HYSJ_ADD_8(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_10(L, R) HYSJ_ADD_9(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_11(L, R) HYSJ_ADD_10(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_12(L, R) HYSJ_ADD_11(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_13(L, R) HYSJ_ADD_12(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_14(L, R) HYSJ_ADD_13(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_15(L, R) HYSJ_ADD_14(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_16(L, R) HYSJ_ADD_15(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_17(L, R) HYSJ_ADD_16(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_18(L, R) HYSJ_ADD_17(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_19(L, R) HYSJ_ADD_18(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_20(L, R) HYSJ_ADD_19(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_21(L, R) HYSJ_ADD_20(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_22(L, R) HYSJ_ADD_21(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_23(L, R) HYSJ_ADD_22(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_24(L, R) HYSJ_ADD_23(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_25(L, R) HYSJ_ADD_24(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_26(L, R) HYSJ_ADD_25(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_27(L, R) HYSJ_ADD_26(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_28(L, R) HYSJ_ADD_27(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_29(L, R) HYSJ_ADD_28(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_30(L, R) HYSJ_ADD_29(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_31(L, R) HYSJ_ADD_30(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_32(L, R) HYSJ_ADD_31(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_33(L, R) HYSJ_ADD_32(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_34(L, R) HYSJ_ADD_33(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_35(L, R) HYSJ_ADD_34(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_36(L, R) HYSJ_ADD_35(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_37(L, R) HYSJ_ADD_36(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_38(L, R) HYSJ_ADD_37(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_39(L, R) HYSJ_ADD_38(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_40(L, R) HYSJ_ADD_39(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_41(L, R) HYSJ_ADD_40(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_42(L, R) HYSJ_ADD_41(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_43(L, R) HYSJ_ADD_42(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_44(L, R) HYSJ_ADD_43(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_45(L, R) HYSJ_ADD_44(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_46(L, R) HYSJ_ADD_45(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_47(L, R) HYSJ_ADD_46(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_48(L, R) HYSJ_ADD_47(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_49(L, R) HYSJ_ADD_48(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_50(L, R) HYSJ_ADD_49(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_51(L, R) HYSJ_ADD_50(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_52(L, R) HYSJ_ADD_51(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_53(L, R) HYSJ_ADD_52(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_54(L, R) HYSJ_ADD_53(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_55(L, R) HYSJ_ADD_54(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_56(L, R) HYSJ_ADD_55(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_57(L, R) HYSJ_ADD_56(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_58(L, R) HYSJ_ADD_57(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_59(L, R) HYSJ_ADD_58(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_60(L, R) HYSJ_ADD_59(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_61(L, R) HYSJ_ADD_60(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_62(L, R) HYSJ_ADD_61(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_63(L, R) HYSJ_ADD_62(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_64(L, R) HYSJ_ADD_63(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_65(L, R) HYSJ_ADD_64(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_66(L, R) HYSJ_ADD_65(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_67(L, R) HYSJ_ADD_66(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_68(L, R) HYSJ_ADD_67(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_69(L, R) HYSJ_ADD_68(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_70(L, R) HYSJ_ADD_69(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_71(L, R) HYSJ_ADD_70(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_72(L, R) HYSJ_ADD_71(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_73(L, R) HYSJ_ADD_72(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_74(L, R) HYSJ_ADD_73(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_75(L, R) HYSJ_ADD_74(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_76(L, R) HYSJ_ADD_75(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_77(L, R) HYSJ_ADD_76(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_78(L, R) HYSJ_ADD_77(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_79(L, R) HYSJ_ADD_78(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_80(L, R) HYSJ_ADD_79(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_81(L, R) HYSJ_ADD_80(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_82(L, R) HYSJ_ADD_81(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_83(L, R) HYSJ_ADD_82(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_84(L, R) HYSJ_ADD_83(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_85(L, R) HYSJ_ADD_84(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_86(L, R) HYSJ_ADD_85(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_87(L, R) HYSJ_ADD_86(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_88(L, R) HYSJ_ADD_87(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_89(L, R) HYSJ_ADD_88(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_90(L, R) HYSJ_ADD_89(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_91(L, R) HYSJ_ADD_90(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_92(L, R) HYSJ_ADD_91(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_93(L, R) HYSJ_ADD_92(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_94(L, R) HYSJ_ADD_93(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_95(L, R) HYSJ_ADD_94(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_96(L, R) HYSJ_ADD_95(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_97(L, R) HYSJ_ADD_96(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_98(L, R) HYSJ_ADD_97(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_99(L, R) HYSJ_ADD_98(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))
#define HYSJ_ADD_100(L, R) HYSJ_ADD_99(HYSJ_ADD_OP(L, R), HYSJ_TAIL(R))

#include<hysj/tools/epilogue.hpp>
