#pragma once
#include<cstdint>
#include<utility>
#include<type_traits>

#include<hysj/tools/functional/invoke.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

namespace _{

  template<typename F0,typename F1,int... I>
  struct combine_fn_impl{
    [[no_unique_address]] F0 f0;
    [[no_unique_address]] F1 f1;

    template<bool b1>
    requires b1
    static constexpr decltype(auto) f2(auto &&f1)noexcept{
      return fwd(f1);
    }
    template<bool b1>
    requires (!b1)
    static constexpr std::identity f2(auto &&)noexcept{
      return {};
    }

    template<bool... b1>
    constexpr auto impl(this auto &&F,std::integer_sequence<bool,b1...>,auto &&...a)
      arrow((invoke(fwd(F).f0,invoke(f2<b1>(fwd(F).f1),fwd(a))...)))
      ;

    constexpr auto impl(this auto &&F,std::integer_sequence<bool>)
      arrow((invoke(fwd(F).f0,invoke(fwd(F).f1))))
      ;

    template<int M,int i>
    static consteval int k(){
      return (i < 0) ? (M + i) : i;
    }
    template<int M,int j>
    static consteval bool b1(){
      return (!sizeof...(I)) || ((j == k<M,I>()) || ...);
    }
    template<int M>
    static consteval auto B1()noexcept{
      return []<int... j>(std::integer_sequence<int,j...>){
        return std::integer_sequence<bool,b1<M,j>()...>{};
      }(std::make_integer_sequence<int,M>{});
    }

    constexpr auto operator()(this auto &&f,auto &&... a)
      arrow((fwd(f).impl(B1<sizeof...(a)>(),fwd(a)...)))
      ;
  };

} //_

template<int... I>
struct combine_fn{
  static constexpr auto operator()(auto &&f0,auto &&f1)
    arrow((_::combine_fn_impl<autotype(f0),autotype(f1),I...>{fwd(f0),fwd(f1)}))
};

template<int... I>
constexpr combine_fn<I...> combine{};

} //hysj
#include<hysj/tools/epilogue.hpp>
