#pragma once
#include<type_traits>
#include<utility>

#include<hysj/tools/preprocessor.hpp>
#include<hysj/tools/functional/ignore.hpp>
#include<hysj/tools/functional/invoke.hpp>
#include<hysj/tools/functional/apply.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

inline constexpr
struct fold_fn{

  static constexpr auto C(ignore_t,auto &&x) noexcept(noexcept(fwd(x))){
    return fwd(x);
  }
  static constexpr auto C(auto &f,auto &&x,auto &&u,auto &&... v)
    noexcept(noexcept(C(f,invoke(f,fwd(x),fwd(u)),fwd(v)...))){
    return C(f,invoke(f,fwd(x),fwd(u)),fwd(v)...);
  }
    
  //arrow
  static constexpr decltype(auto) B(auto &&U,auto &&x,auto &&f){
    return apply([&](auto &&... u){ return C(f,fwd(x),fwd(u)...); },fwd(U));
  }
  template<typename T,T... I>
  static constexpr auto B(std::integer_sequence<T, I...>,auto &&x,auto &&f)
    arrow((C(f,fwd(x),std::integral_constant<T,I>{}...)))
    ;

  static constexpr auto operator()(auto &&... x)
    arrow((B(fwd(x)...)));
} 
  fold{};

namespace _{
  constexpr auto make_each_op(auto &&f) noexcept(noexcept(auto(fwd(f)))){
    return [f = fwd(f)](ignore_t ignore, auto &&u){
      invoke(f, fwd(u));
      return ignore;
    };
  }
}
inline constexpr auto each = [](auto &&s,auto &&f)
  arrow((fold(fwd(s), ignore, _::make_each_op(fwd(f)))))
  ;
    
} //hysj
#include<hysj/tools/epilogue.hpp>
