#pragma once
#include<cstdint>
#include<functional>
#include<utility>
#include<type_traits>
#include<variant>

#include<hysj/tools/type_traits.hpp>
#include<hysj/tools/functional/unwrap.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

inline constexpr auto at =
  [](auto &&f,auto &&x)
    arrow((unwrap(fwd(f)).at(fwd(x))));

inline constexpr auto deref =
  [](auto &&x)
    arrow((*x));

template<typename T>
inline constexpr auto cast =
  [](auto &&x)
    arrow(((static_cast<T>(fwd(x)))));

template<typename T>
inline constexpr auto init =
  [](auto &&... x)
    arrow((T{fwd(x)...}))
  ;

} //hysj
#include<hysj/tools/epilogue.hpp>
