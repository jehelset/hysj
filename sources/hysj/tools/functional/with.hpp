#pragma once
#include<cstdint>
#include<type_traits>

#include<hysj/tools/preprocessor.hpp>
#include<hysj/tools/functional/ignore.hpp>
#include<hysj/tools/functional/invoke.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

namespace _{
  template<typename T,auto... I0>
  constexpr T with_impl(auto &&F,ignore_t,
                        std::index_sequence<I0...>,
                        std::integral_constant<std::size_t,0z>,std::index_sequence<>) 
  {
    return invoke(fwd(F), std::integral_constant<std::size_t,I0>{}...);
  }
  template<typename T,std::size_t... I0,std::size_t N0,std::size_t... N1>
  constexpr T with_impl(ignore_t,ignore_t,
                        std::index_sequence<I0...>,
                        std::integral_constant<std::size_t,N0>,std::index_sequence<N0,N1...>) 
  {
    HYSJ_UNREACHABLE;
  }
  template<typename T,std::size_t... I0,std::size_t J,std::size_t N0,std::size_t... N1>
  requires (J != N0)
  constexpr T with_impl(auto &&F,const auto &I,
                        std::index_sequence<I0...>,
                        std::integral_constant<std::size_t,J>,std::index_sequence<N0,N1...>) 
  {
    #define HYSJ_UNROLL (0)(1)(2)(3)(4)(5)(6)(7)(8)(9)
    #define HYSJ_LAMBDA(K)                                            \
      if constexpr(J + K < N0)                                        \
        if(I[sizeof...(I0)] == J + K)                                 \
          return with_impl<T>(fwd(F),I,                               \
            std::index_sequence<I0...,J + K>{},                       \
            std::integral_constant<std::size_t,0z>{},                 \
            std::index_sequence<N1...>{});
    HYSJ_MAP_LAMBDA(HYSJ_UNROLL)
    return with_impl<T>(fwd(F),I,
                        std::index_sequence<I0...>{},
                        std::integral_constant<std::size_t,std::min(N0,J + HYSJ_SIZE(HYSJ_UNROLL))>{},
                        std::index_sequence<N0,N1...>{});
    #undef HYSJ_LAMBDA
    #undef HYSJ_UNROLL
  }
}
template<auto... N>
inline constexpr auto with = [](auto &&F,auto... i) -> decltype(auto) requires (sizeof...(i) == sizeof...(N)){
  using T = decltype(invoke(fwd(F), std::integral_constant<std::size_t,decltype(N){}>{}...));
  return _::with_impl<T>(
    fwd(F),
    std::array<std::size_t,sizeof...(i)>{static_cast<std::size_t>(i)...},
    std::index_sequence<>{},
    std::integral_constant<std::size_t,0z>{},
    std::index_sequence<static_cast<std::size_t>(N)...>{});
};

} //hysj
#include<hysj/tools/epilogue.hpp>
