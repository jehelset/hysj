#pragma once

#include<type_traits>
#include<hysj/tools/types.hpp>
#include<hysj/tools/functional/combine.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

template<integer... I>
struct on_fn{

  static constexpr auto impl(std::integer_sequence<integer>,auto &&f0)
    arrow((fwd(f0)))

  template<integer J0>
  static constexpr auto impl(std::integer_sequence<integer,J0>,auto &&f0,auto &&f1)
    arrow((combine<J0>(fwd(f0),fwd(f1))))

  template<integer J0,integer J1,integer... J2>
  static constexpr auto impl(std::integer_sequence<integer,J0,J1,J2...>,auto &&f0,auto &&f1,auto &&... f2)
    arrow((impl(std::integer_sequence<integer,J1,J2...>{},
                impl(std::integer_sequence<integer,J0>{},fwd(f0),fwd(f1)),
                fwd(f2)...)))

  static constexpr auto operator()(auto &&f0,auto &&... f1)
    arrow((impl(std::integer_sequence<integer,I...>{},fwd(f0),fwd(f1)...)))
  
};

template<integer... I>
constexpr on_fn<I...> on{};

} //hysj
#include<hysj/tools/epilogue.hpp>
