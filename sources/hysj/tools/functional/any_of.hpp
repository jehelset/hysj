#pragma once

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

//NOTE: adapted from from L. Szolnoki - jeh
template<typename F>
struct any_of_fn{
  [[no_unique_address]] F f;

  template<typename T>
  friend
  constexpr bool operator==(const T& lhs,const any_of_fn& rhs)
    noexcept(noexcept(rhs.f(lhs))){
    return rhs.f(lhs);
  }
  template<typename T>
  friend
  constexpr bool operator!=(const T& lhs,const any_of_fn& rhs)
    noexcept(noexcept(!(lhs == rhs))){
    return !(lhs == rhs);
  }
  template<typename T>
  constexpr bool operator()(const T &lhs)const noexcept(noexcept(lhs == *this)){
    return lhs == *this;
  }
};

constexpr auto any_of(auto &&... args)noexcept{
  return any_of_fn{[...args = fwd(args)](const auto& lhs)constexpr noexcept(noexcept(((lhs == args) || ...))){
    return ((lhs == args) || ...);
  }};
}

} //hysj
#include<hysj/tools/epilogue.hpp>
