#pragma once

#include<cstdint>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

//NOTE: stolen from boost - jeh
inline constexpr auto hash_combine =
  [](std::size_t h,std::size_t k){
    const std::size_t m = 0xc6a4a7935bd1e995;
    const int r = 47;

    k *= m;
    k ^= k >> r;
    k *= m;

    h ^= k;
    h *= m;

    h += 0xe6546b64;
    return h;
  };
  
} //hysj
#include<hysj/tools/epilogue.hpp>
