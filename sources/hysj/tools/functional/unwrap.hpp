#pragma once
#include<type_traits>

#include<hysj/tools/type_traits.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

inline constexpr auto unwrap =
  [](auto &&t) constexpr noexcept -> decltype(auto) {
    using T = std::remove_cvref_t<decltype(t)>;
    if constexpr (is_std_reference_wrapper<T>())
      return fwd(t).get();
    else if constexpr(is_std_unique_ptr<T>() || std::is_pointer_v<T>)
      return *t;
    else
      return fwd(t);
  };

template<typename T>
using unwrap_t = decltype(unwrap(std::declval<T>()));

} //hysj
#include<hysj/tools/epilogue.hpp>
