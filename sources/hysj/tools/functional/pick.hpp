#pragma once

#include<hysj/tools/functional/ignore.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

namespace _{

  template<int I>
  requires (I == 0)
  constexpr decltype(auto) pick_impl(auto &&x,auto &&...)noexcept{
    return fwd(x);
  }

  template<int I>
  constexpr decltype(auto) pick_impl(ignore_t,auto &&... y)noexcept
    requires (I > 0) && (I <= sizeof...(y))
  {
    return _::pick_impl<I - 1>(fwd(y)...);
  }

  template<int I>
  constexpr decltype(auto) pick_impl(auto &&... y)noexcept
  requires (I < 0) && (-I <= (sizeof...(y)))
  {
    constexpr int N = sizeof...(y);
    return _::pick_impl<N + I>(fwd(y)...);
  }

}

template<integer I>
inline constexpr auto pick =
  [](auto &&... x)
    arrow((_::pick_impl<I>(fwd(x)...)));

namespace _{
  template<int I,typename... T>
  struct pick_t_impl;
  template<typename U,typename...T>
  struct pick_t_impl<0,U,T...>{
    using type = U;
  };
  template<int I,typename U,typename...T>
  requires (I > 0)
  struct pick_t_impl<I,U,T...>:pick_t_impl<I - 1,T...>{};
  template<int I,typename... T>
  requires (I < 0) && (sizeof...(T) >= (-I))
  struct pick_t_impl<I,T...>:pick_t_impl<sizeof...(T)+I,T...>{};
}
template<int I,typename... T>
using pick_t = typename _::pick_t_impl<I,T...>::type;
 
} //hysj
#include<hysj/tools/epilogue.hpp>
