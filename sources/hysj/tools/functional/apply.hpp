#pragma once

#include<cstdint>
#include<tuple>
#include<type_traits>
#include<utility>

#include<kumi/tuple.hpp>

#include<hysj/tools/functional/invoke.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

inline constexpr struct {
  template<typename F, typename T,std::size_t... I>
  static constexpr auto impl(F &&f, T &&t,std::index_sequence<I...>)
    arrow((invoke(fwd(f), get<I>(fwd(t))...)));
  template<typename F, typename T>
  static constexpr auto operator()(F &&f, T &&t) 
    arrow((impl(fwd(f), fwd(t), std::make_index_sequence<kumi::size_v<T>>{})));
}
  apply{};

} //hysj
#include<hysj/tools/epilogue.hpp>
