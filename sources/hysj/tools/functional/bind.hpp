#pragma once

#include<array>
#include<cstdint>
#include<utility>
#include<type_traits>

#include<kumi/tuple.hpp>

#include<hysj/tools/preprocessor.hpp>
#include<hysj/tools/functional/apply.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

namespace _{
  
  template<typename F, auto I, typename... A>
  struct bind_impl{

    [[no_unique_address]] F f;
    kumi::tuple<A...> a;

    template<std::size_t M>
    static consteval auto J(){
      constexpr auto N = I.size();
      std::array<int,N + M> J{};
      J.fill(-1);
      for(int ii = 0;ii != N;++ii)
        J[I[ii] < 0 ? (M + N) + I[ii] : I[ii]] = ii;

      for(int kk = 0,jj = N;kk != N + M;++kk)
        if(J[kk] == -1)
          J[kk] = jj++;
      return J;
    }

    template<auto J,std::size_t... ii,std::size_t... jj>
    static constexpr auto C(std::index_sequence<ii...>,
                            std::index_sequence<jj...>,
                            auto &&a,auto &&... b)noexcept{
      return
        kumi::forward_as_tuple(
          kumi::get<(std::size_t)J[jj]>(
            kumi::forward_as_tuple(kumi::get<ii>(fwd(a))...,
                                  fwd(b)...))...);
    }
    
    constexpr auto operator()(this auto &&g, auto &&... b)
      arrow((apply(fwd(g).f,
                   C<J<sizeof...(b)>()>(
                     std::make_index_sequence<I.size()>{},
                     std::make_index_sequence<I.size() + sizeof...(b)>{},
                     fwd(g).a,
                     fwd(b)...))))

  };

}

template<int... I>
struct bind_fn{
  static constexpr auto operator()(auto&& f, auto&&... a)
    arrow((_::bind_impl<
             autotype(f),
             std::array<int,sizeof...(I)>{I...},
             autotype(a)...>{
               fwd(f),
               {fwd(a)...}
             }))
};

template<>
struct bind_fn<>{

  template<std::size_t... j>
  static constexpr auto C(std::index_sequence<j...>,auto &&f,auto &&... a)
    arrow((_::bind_impl<
             autotype(f),
             std::array<int,sizeof...(j)>{j...},
             autotype(a)...>{
               fwd(f),
               {fwd(a)...}
             }))

  static constexpr auto operator()(auto &&f, auto &&... a)
    arrow((C(std::make_index_sequence<sizeof...(a)>{},fwd(f),fwd(a)...)))

};

template<int... I>
inline constexpr bind_fn<I...> bind{};

inline constexpr auto unpack = bind<0>(bind<0>,apply);

} //hysj
#include<hysj/tools/epilogue.hpp>
