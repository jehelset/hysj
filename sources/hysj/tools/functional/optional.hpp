#pragma once
#include<optional>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

inline constexpr auto just =
  [](auto &&t)
    arrow((std::make_optional(fwd(t))));

template<typename T>
inline constexpr std::optional<T> no{};

inline constexpr auto has_value =
  [](const auto &x)
    arrow((x.has_value()));
inline constexpr auto value =
  [](auto &&x)
    arrow((fwd(x).value()));
inline constexpr auto autovalue =
  [](auto &&x)
    arrow((auto(fwd(x).value())));
inline constexpr auto map_value =
  [](auto &&x,auto &&f)
    arrow((x.has_value() ? just(fwd(f)(value(fwd(x)))) : no<decltype(fwd(f)(value(fwd(x))))>));
inline constexpr auto maybe =
  [](auto p,auto &&v)
    arrow((p ? just(fwd(v)) : no<decltype(auto(fwd(v)))>));
inline constexpr auto map_if =
  [](auto p,auto &&f)
    arrow((p ? just(fwd(f)()) : no<decltype(auto(fwd(f)()))>));

} //hysj
#include<hysj/tools/epilogue.hpp>
