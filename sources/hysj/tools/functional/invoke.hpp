#pragma once

#include<type_traits>
#include<utility>

#include<hysj/tools/type_traits.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

template<typename T>
struct invoke_unwrap{
  using type = T &&;
};
template<typename T>
requires (is_std_reference_wrapper<T>())
struct invoke_unwrap<T>{
  using type = typename std::remove_cvref_t<T>::type &;
};
template<typename T>
requires std::is_pointer_v<std::remove_cvref_t<T>>
struct invoke_unwrap<T>{
  using type = std::remove_pointer_t<std::remove_cvref_t<T>> &;
};
template<typename T>
using invoke_unwrap_t = typename invoke_unwrap<T>::type;

//FIXME: use concepts when gcc not bugged - jeh
template<typename F,typename X, typename... Y>
constexpr bool can_invoke_member_v = []{
  if constexpr(std::is_member_object_pointer_v<std::remove_cvref_t<F>>)
    return !sizeof...(Y) && requires(F &&f, invoke_unwrap_t<X> &&z){
      fwd(z).*f;
    };
  else
    return requires(F &&f,invoke_unwrap_t<X> &&z,Y &&... y){
      (fwd(z).*f)(fwd(y)...);
    };
}();
template<typename F,typename... X>
concept can_invoke = 
  (std::is_member_pointer_v<std::remove_cvref_t<F>>
   && can_invoke_member_v<F,X...>)
  || requires(F &&f,X &&... x){
       fwd(f)(fwd(x)...);
     };

template<typename F,typename X, typename... Y>
constexpr bool can_nothrow_invoke_member_v = []{
  if constexpr(std::is_member_object_pointer_v<std::remove_cvref_t<F>> && !sizeof...(Y))
    return true;
  else
    return noexcept((std::declval<invoke_unwrap_t<X>>().*(std::declval<F>()))(std::declval<Y>()...));
}();
template<typename F,typename... X>
constexpr bool can_nothrow_invoke_v = []{
  if constexpr(std::is_member_pointer_v<std::remove_cvref_t<F>>)
    return can_nothrow_invoke_member_v<F,X...>;
  else
    return noexcept(std::declval<F>()(std::declval<X>()...));
}();

inline constexpr struct {
  template<typename F,typename... X>
  requires can_invoke<F, X...>
  static constexpr auto operator()(F &&f,X &&... x) noexcept(can_nothrow_invoke_v<F, X...>) -> decltype(auto) {
    if constexpr(std::is_member_pointer_v<std::remove_cvref_t<decltype(f)>>)
      return [&](auto &&y, auto &&... z) -> decltype(auto){
        auto &&w = [&] -> decltype(auto){
          if constexpr(is_std_reference_wrapper<decltype(y)>())
            return fwd(y).get();
          else if constexpr(std::is_pointer_v<std::remove_cvref_t<decltype(y)>>)
            return *fwd(y);
          else
            return (fwd(y));
        }();
        if constexpr(std::is_member_object_pointer_v<std::remove_cvref_t<decltype(f)>>)
          return fwd(w).*f;
        else
          return (fwd(w).*f)(fwd(z)...);
      }(fwd(x)...);
    else
      return fwd(f)(fwd(x)...);
  }
}
  invoke{};

template<typename T,typename... X>
using invoke_result_t = decltype(invoke(std::declval<T>(), std::declval<X>()...));

} //hysj
#include<hysj/tools/epilogue.hpp>
