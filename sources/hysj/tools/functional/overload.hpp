#pragma once

#include<type_traits>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

namespace _{

  template<typename... T>
  struct overload_fn_impl:T...{
    using T::operator()...;
  };

  template<typename... T>
  overload_fn_impl(T &&...) -> overload_fn_impl<std::decay_t<T>...>;
  
}

inline constexpr
struct overload_fn{
  static constexpr auto operator()(auto &&... f)
    arrow((_::overload_fn_impl{fwd(f)...}))
}
  overload{};

} //hysj
#include<hysj/tools/epilogue.hpp>
