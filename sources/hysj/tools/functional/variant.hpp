#pragma once
#include<variant>
#include<utility>

#include<hysj/tools/functional/always.hpp>
#include<hysj/tools/functional/bind.hpp>
#include<hysj/tools/functional/compose.hpp>
#include<hysj/tools/functional/overload.hpp>
#include<hysj/tools/type_traits.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

namespace _{
  struct visitor_impl{
    static constexpr auto operator()(auto &&f,auto &&... v) 
      arrow((std::visit(fwd(f),fwd(v)...)))
    
    static constexpr auto operator()(auto &&f,auto &&... v)
      arrow((std::visit(fwd(f).get(),fwd(v)...))
            (; is_std_reference_wrapper<std::remove_cvref_t<decltype(f)>>()))
  };
}
inline constexpr auto visitor =
  compose<>(bind<0>(bind<>,lift((std::visit))), overload);

} //hysj
#include<hysj/tools/epilogue.hpp>
