#pragma once
#include<cstdint>
#include<type_traits>
#include<utility>

#include<kumi/tuple.hpp>
#include<kumi/algorithm/cat.hpp>
#include<kumi/algorithm/extract.hpp>

#include<hysj/tools/preprocessor.hpp>
#include<hysj/tools/type_traits.hpp>
#include<hysj/tools/functional/apply.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

namespace _{

  //FIXME: workaround while waiting for upstream fix - jeh
  template<std::size_t I0, std::size_t I1, kumi::product_type Tuple>
  requires( (I0 <= kumi::size_v<Tuple>) && (I1 <= kumi::size_v<Tuple>) )
  [[nodiscard]] constexpr
  auto extract2( Tuple && t
              , [[maybe_unused]] kumi::index_t<I0> i0
              , [[maybe_unused]] kumi::index_t<I1> i1
              ) noexcept
  {
    return [&]<std::size_t... N>(std::index_sequence<N...>)
    {
      return kumi::tuple<kumi::element_t<N + I0, Tuple &&>...> {kumi::get<N + I0>(fwd(t))...};
    }
    (std::make_index_sequence<I1 - I0>());
  }
  template<std::size_t I0, kumi::product_type Tuple>
  requires(I0<= kumi::size_v<Tuple>)
  [[nodiscard]] constexpr auto extract2(Tuple && t, kumi::index_t<I0> i0) noexcept
  {
    return extract2(fwd(t), i0, kumi::index<kumi::size_v<Tuple>>);
  }
  template<std::size_t I0, kumi::product_type Tuple>
  requires(I0 <= kumi::size_v<Tuple>)
  [[nodiscard]] constexpr auto split2( Tuple && t
                                    , [[maybe_unused]] kumi::index_t<I0> i0
                                    ) noexcept
  {
    return kumi::make_tuple(
      extract2(fwd(t), kumi::index<0>, kumi::index<I0>),
      extract2(fwd(t), kumi::index<I0>));
  }

  
  template<typename F0,int N,typename F1>
  struct compose_fn_impl{
    [[no_unique_address]] F0 f0;
    [[no_unique_address]] F1 f1;

    static constexpr auto D(auto &&F,auto &&A,auto &&B)
      arrow((
              kumi::apply(fwd(F).f0,
                          kumi::cat(kumi::forward_as_tuple(kumi::apply(fwd(F).f1,fwd(A))),
                                    fwd(B))))
            (; } && (N>= 0) && requires{ true))
      ;

    static constexpr auto D(auto &&F,auto &&A,auto &&B)
      arrow((
          kumi::apply(fwd(F).f0,
                      kumi::cat(fwd(A),
                                kumi::forward_as_tuple(kumi::apply(fwd(F).f1,fwd(B))))))
            (; } && (N < 0) && requires{ true))
      ;
    
    static constexpr auto D(auto &&F,auto &&A,auto &&B)
      arrow(
        (kumi::apply(fwd(F).f1,fwd(A)),kumi::apply(fwd(F).f0,fwd(B)))
        (; { kumi::apply(fwd(F).f1,fwd(A)) } -> std::same_as<void>))
      ;

    static constexpr auto C(auto &&F,auto &&AB)
      arrow((D(fwd(F),kumi::get<0>(fwd(AB)),kumi::get<1>(fwd(AB)))));

    constexpr auto operator()(this auto &&f,auto &&... a)
      arrow((C(fwd(f),
               split2(
                 kumi::forward_as_tuple(fwd(a)...),
                 kumi::index<(N >= 0 ? 
                              ((N == 0) ? (int)sizeof...(a) : N) :
                              ((int)sizeof...(a) + N))>))));

  };
  
} //_

template<int N>
struct compose_fn{

  static constexpr auto operator()(auto &&f0,auto &&f1)
    arrow((_::compose_fn_impl<
             autotype(f0),N,autotype(f1)>{fwd(f0),fwd(f1)})) 

  static constexpr auto operator()(auto &&f0,auto &&f1,auto &&... f2)
    arrow((operator()(_::compose_fn_impl<
                        autotype(f0),N,autotype(f1)>{fwd(f0),fwd(f1)},
                      fwd(f2)...)))
    
};

template<int N = 0>
inline constexpr compose_fn<N> compose{};

} //hysj
#include<hysj/tools/epilogue.hpp>
