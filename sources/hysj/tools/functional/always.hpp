#pragma once
#include<coroutine>
#include<type_traits>

#include<hysj/tools/types.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

namespace _{
  template<typename T>
  struct always_impl{
    static constexpr bool am_nothrow_copy_constructible =
      std::is_nothrow_copy_constructible_v<T>;
    T value;

    constexpr T operator()(const auto &...)const
      noexcept(am_nothrow_copy_constructible)
    { return value; }

    constexpr auto operator co_await()const noexcept(am_nothrow_copy_constructible){
      struct awaiter{
        T value;
        static constexpr bool await_ready()noexcept{ return true; }
        static constexpr void await_suspend(std::coroutine_handle<>)noexcept{}
        constexpr T await_resume()const noexcept(am_nothrow_copy_constructible){ return value; }
      };
      return awaiter{value};
    }
  };
  
  template<>
  struct always_impl<void>{
    static constexpr void operator()(const auto &...)
      noexcept
    {}
    constexpr std::suspend_never operator co_await()noexcept{
      return {};
    }
    static constexpr void await_resume()noexcept{ return; }
  };
  
} //_

inline constexpr
struct always_fn{
  static constexpr auto operator()(auto &&value)
    arrow((_::always_impl<autotype(value)>{fwd(value)}));
  static constexpr _::always_impl<void> operator()() {
    return {};
  }
}
  always{};

inline constexpr auto never = always(none{});

} //hysj
#include<hysj/tools/epilogue.hpp>
