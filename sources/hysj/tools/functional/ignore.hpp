#pragma once

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

inline constexpr struct ignore_t{
  constexpr ignore_t(const auto &...)noexcept{}
  constexpr auto &operator=(const auto &)noexcept{
    return *this;
  }
  static constexpr ignore_t operator()(const auto &...)noexcept{
    return {};
  }
} ignore{};

}
#include<hysj/tools/epilogue.hpp>
