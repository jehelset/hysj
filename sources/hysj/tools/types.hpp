#pragma once
#include<cstddef>
#include<cstdint>
#include<compare>
#include<type_traits>

#include<fmt/core.h>

#include<hysj/tools/reflection.hpp>
#include<hysj/tools/type_traits.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

template<bool N>
using bool_t = std::integral_constant<bool,N>;

template<bool N>
constexpr bool_t<N> bool_c = {};

inline constexpr auto true_c = constant_c<true>;
inline constexpr auto false_c = constant_c<false>;

template<typename T>
inline constexpr bool is_reference_wrapper_v = 
  !std::is_same_v<std::decay_t<std::unwrap_reference_t<T &&>>, std::unwrap_ref_decay_t<T &&>>;

template<typename T>
using ref_ = std::conditional_t<is_reference_wrapper_v<T>, T, std::reference_wrapper<std::remove_reference_t<T>>>;

template<std::size_t...>
struct empty{
  friend HYSJ_MIRROR_STRUCT(empty,())
  auto operator<=>(const empty &)const = default;
};

struct HYSJ_EXPORT none{
  friend HYSJ_MIRROR_STRUCT(none,())
  auto operator<=>(const none &)const = default;
};

using real    = double;
using natural = uint64_t;
using integer = int64_t;
using ptrdiff = std::ptrdiff_t;

constexpr real operator ""_r(long double r){ return r; }
constexpr real operator ""_r(unsigned long long int r){ return r; }
constexpr natural operator ""_n(unsigned long long int n){ return n; }
constexpr integer operator ""_i(unsigned long long int i){ return i; }
constexpr ptrdiff operator ""_p(unsigned long long int i){ return i; }

template<integer N>
using integer_t = std::integral_constant<integer,N>;

template<integer N>
constexpr integer_t<N> integer_c = {};

template<natural N>
using natural_t = std::integral_constant<natural,N>;
  
template<natural N>
constexpr natural_t<N> natural_c = {};

template<ptrdiff N>
using ptrdiff_t = std::integral_constant<ptrdiff,N>;
  
template<ptrdiff N>
constexpr ptrdiff_t<N> ptrdiff_c = {};

//NOTE:
//  stolen from
//  https://codereview.stackexchange.com/questions/50910/
//  user-defined-literal-for-stdintegral-constant
// - jeh
constexpr auto literal_combine(auto p){
  return p;
}
constexpr auto literal_combine(auto val, auto p0, auto... pp){
  return literal_combine(val*10 + static_cast<decltype(val)>(p0), pp...);
}
constexpr auto literal_parse(char C){
  return (C >= '0' && C <= '9')
    ? C - '0'
    : throw std::out_of_range("only decimal digits are allowed");
}
template<char... digits>
constexpr auto operator"" _N()
  -> natural_t<literal_combine(0_n, literal_parse(digits)...)>
{ return {}; }

template<char... digits>
constexpr auto operator"" _P()
  -> ptrdiff_t<literal_combine(0_p, literal_parse(digits)...)>
{ return {}; }
  
} //hysj

template<>
struct fmt::formatter<hysj::_HYSJ_VERSION_NAMESPACE::none>{
  using T = hysj::_HYSJ_VERSION_NAMESPACE::none;
  constexpr auto parse(format_parse_context& context)const{
    auto it  = context.begin(),
         end = context.end();
    if(it != end)
      throw fmt::format_error("invalid format");
    return it;
  }
  auto format(const T &,format_context &ctx)const{
    return detail::write<char>(ctx.out(), "none");
  }
};

template<>
struct std::hash<hysj::_HYSJ_VERSION_NAMESPACE::none>{
  using T = hysj::_HYSJ_VERSION_NAMESPACE::none;
  constexpr std::size_t operator()(const T &)noexcept{
    return {};
  }
};

#include<hysj/tools/epilogue.hpp>
