#pragma once
#include<type_traits>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

template<typename E>
constexpr auto to_underlying(E e)noexcept{
  return static_cast<std::underlying_type_t<E>>(e);
}

template<typename E,E e>
constexpr auto to_underlying(std::integral_constant<E,e>)noexcept{
  return std::integral_constant<std::underlying_type_t<E>,
                                to_underlying(e)>{};
}

}//hysj
#include<hysj/tools/epilogue.hpp>
