#include<utility>

#include <libassert/assert.hpp>

#include<hysj/tools/version.hpp>
#include<hysj/tools/export.hpp>

//NOTE: mostly stolen from libunifex - jeh
#ifdef HYSJ_PROLOGUE
#error "Ambiguous prologue"
#endif
#define HYSJ_PROLOGUE

#define HYSJ_PROLOGUE_EXPAND(x) x

#define HYSJ_PROLOGUE_UNPAREN(...) __VA_ARGS__
#define HYSJ_PROLOGUE_WITH_NIL(...) (__VA_ARGS__), HYSJ_PROLOGUE_NIL
#define HYSJ_PROLOGUE_DROP_NIL(a) HYSJ_PROLOGUE_DROP_NIL_0(a)
#define HYSJ_PROLOGUE_DROP_NIL_0(a,...) a

#define HYSJ_PROLOGUE_HEAD(a)\
  HYSJ_PROLOGUE_DROP_NIL(HYSJ_PROLOGUE_WITH_NIL a())
#define HYSJ_PROLOGUE_TAIL(a) HYSJ_PROLOGUE_TAIL_0 a
#define HYSJ_PROLOGUE_TAIL_0(...) 
#define HYSJ_PROLOGUE_ARROW_IMPL(a,b)                                       \
  noexcept(noexcept a) -> decltype(auto)                                    \
  requires requires { {HYSJ_PROLOGUE_UNPAREN a} HYSJ_PROLOGUE_UNPAREN b; }{ \
    return HYSJ_PROLOGUE_UNPAREN a;                                         \
  }

#if defined(arrow)
  #error "arrow was already defined"
#endif
#define arrow(a)                                  \
   HYSJ_PROLOGUE_ARROW_IMPL(                      \
     HYSJ_PROLOGUE_HEAD(a),                       \
     HYSJ_PROLOGUE_HEAD(HYSJ_PROLOGUE_TAIL(a())))

#if defined(fwd)
  #error "fwd was already defined"
#endif
#define fwd(expr) static_cast<decltype(expr) &&>(expr)

#if defined(lift)
  #error "lift was already defined"
#endif
#define HYSJ_PROLOGUE_LIFT_IMPL(a,b,c)                                      \
  [HYSJ_PROLOGUE_UNPAREN b](auto &&... x)                                   \
    HYSJ_PROLOGUE_UNPAREN c arrow(((HYSJ_PROLOGUE_UNPAREN a (fwd(x)...))))

#define lift(a)                                                         \
  HYSJ_PROLOGUE_LIFT_IMPL(                                              \
    HYSJ_PROLOGUE_HEAD(a),                                              \
    HYSJ_PROLOGUE_HEAD(HYSJ_PROLOGUE_TAIL(a())),                        \
    HYSJ_PROLOGUE_HEAD(HYSJ_PROLOGUE_TAIL(HYSJ_PROLOGUE_TAIL(a()()))))

#if defined(reify)
  #error "reify was already defined"
#endif
#define reify(expr) std::remove_cvref_t<decltype(expr)>::type

#if defined(autotype)
  #error "autotype was already defined"
#endif
#define autotype(expr) decltype(auto(fwd(expr)))

#if defined(co_final_await)
  #error "co_final_await was already defined"
#endif
#define co_final_await co_return co_await 

#if defined(etoi)
  #error "etoi was already defined"
#endif
#define etoi(...) std::to_underlying(__VA_ARGS__)
#define etoi_t std::underlying_type_t

#if defined(__GNUC__) || defined(__clang__)
  #define HYSJ_UNREACHABLE std::unreachable()
#else
  #error "not supported"
#endif

#define HYSJ_ASSERT LIBASSERT_ASSERT
