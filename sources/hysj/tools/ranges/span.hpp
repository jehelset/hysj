// MIT License

// Copyright (c) 2020 Barry Revzin

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
#pragma once

//NOTE:
// adapted from https://github.com/brevzin/span_ext
// - jeh

#include <compare>
#include <algorithm>
#include <span>
#include <concepts>
#include <ranges>

namespace span_ext {
    using std::same_as;
    using std::convertible_to;
    using std::invocable;
    using std::equality_comparable;
    using std::three_way_comparable;

    using std::ranges::begin;
    using std::ranges::end;
    using std::ranges::range_value_t;
    using std::contiguous_iterator;
    using std::ranges::contiguous_range;

  inline constexpr auto synth_three_way =
    []<class T>(const T& t, const T& u)
        requires requires {
            { t < u } -> convertible_to<bool>;
            { u < t } -> convertible_to<bool>;
        }
    {
        if constexpr (three_way_comparable<T>) {
            return t <=> u;
        } else {
            if (t < u) return std::weak_ordering::less;
            if (u < t) return std::weak_ordering::greater;
            return std::weak_ordering::equivalent;
        }
    };

    template <typename T>
    concept synth_comparable = invocable<decltype(synth_three_way), T, T>;

    template <typename T, typename U>
    concept sameish = same_as<std::remove_cvref_t<T>, std::remove_cvref_t<U>>;

    template <typename R, typename T>
    concept contiguous_range_of = contiguous_range<R const>
        && sameish<T, range_value_t<R const>>;

    template <typename InputIter1, typename InputIter2>
    auto lexicographical_compare_three_way(
                    InputIter1 first1, InputIter1 last1,
                    InputIter2 first2, InputIter2 last2)
        -> decltype(synth_three_way(*first1, *first2))
    {
        return std::lexicographical_compare_three_way(
            first1, last1, first2, last2, synth_three_way);
    }
}

namespace std {
    template <span_ext::equality_comparable T, size_t E,
              span_ext::contiguous_range_of<T> R>
    constexpr bool operator==(span<T, E> lhs, R const& rhs)
    {
        return ::std::equal(
            lhs.begin(), lhs.end(),
            span_ext::begin(rhs), span_ext::end(rhs));
    }

    template <span_ext::synth_comparable T, size_t E,
              span_ext::contiguous_range_of<T> R>
    constexpr auto operator<=>(span<T, E> lhs, R const& rhs)
    {
        return span_ext::lexicographical_compare_three_way(
            lhs.begin(), lhs.end(),
            span_ext::begin(rhs), span_ext::end(rhs));
    }
}
