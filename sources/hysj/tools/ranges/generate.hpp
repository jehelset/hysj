#pragma once

#include<concepts>
#include<iterator>
#include<type_traits>
#include<variant>

#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/functional/always.hpp>
#include<hysj/tools/functional/invoke.hpp>
#include<hysj/tools/functional/compose.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::views::generators{

template<typename F>
struct iterator{

  using difference_type = std::ptrdiff_t;
  using value_type = std::remove_cvref_t<invoke_result_t<F &>>;

  static constexpr bool is_nothrow_copy_constructible =
    std::is_nothrow_copy_constructible_v<F>;
  // static constexpr bool is_const_invocable =
  //   std::is_invocable_v<const F>;
  
  using iterator_concept = std::random_access_iterator_tag;
  using iterator_category = iterator_concept;
  
  [[no_unique_address]] ranges::box<F> function;

  constexpr auto
    operator*() 
      arrow((invoke(*function)))

  constexpr auto
    operator*()const
      arrow((invoke(const_cast<F &>(*function))))

  constexpr iterator &operator++(){
    return *this;
  }
  constexpr iterator operator++(int) noexcept(is_nothrow_copy_constructible){
    return *this;
  }

  friend constexpr bool operator==(const iterator &, const iterator &){
    return std::monostate{} == std::monostate{};
  }
  
  friend
  constexpr auto
  operator<=>(const iterator &, const iterator &)
  { return std::monostate{} <=> std::monostate{}; }

  constexpr iterator &operator--(){
    return *this;
  }
  constexpr iterator operator--(int) noexcept(is_nothrow_copy_constructible){
    return *this;
  }

  constexpr iterator &operator+=(difference_type){
    return *this;
  }
  constexpr iterator &operator-=(difference_type){
    return *this;
  }
  friend constexpr iterator operator+(const iterator &i,difference_type)
    noexcept(is_nothrow_copy_constructible){
    return i;
  }
  friend constexpr iterator operator+(difference_type,const iterator &i)
    noexcept(is_nothrow_copy_constructible){
    return i;
  }
  friend constexpr iterator operator-(difference_type,const iterator &i)
    noexcept(is_nothrow_copy_constructible){
    return i;
  }
  friend constexpr iterator operator-(const iterator &i,difference_type)
    noexcept(is_nothrow_copy_constructible){
    return i;
  }

  constexpr auto operator[](difference_type) const
    arrow((*(*this)))

  friend constexpr difference_type
  operator-(const iterator &, const iterator &)
  { return 0; }

};

template<typename F>
struct view:std::ranges::view_interface<view<F>>{
  
  [[no_unique_address]] ranges::box<F> function; 

  constexpr iterator<F> begin()const{
    return {*function};
  }
  static constexpr std::unreachable_sentinel_t end(){
    return {};
  }
};

struct factory{
  template<std::invocable F>
  constexpr view<std::decay_t<F>>
    operator()(F&& f)const{
    return {
      .function = ranges::box<std::decay_t<F>>{fwd(f)}
    };
  }
};

} //hysj::views::generators

namespace hysj::inline _HYSJ_VERSION_NAMESPACE::views{

inline constexpr generators::factory generate{};
inline constexpr auto generate_n =
  [](auto f,auto n)
    arrow((std::views::take(views::generate(std::move(f)),n)))
  ;

inline constexpr auto repeat = compose<>(generate,always);
inline constexpr auto repeat_n =
  [](auto x,auto n)
    arrow((std::views::take(views::repeat(std::move(x)),n)))
  ;

} //hysj::views

#include<hysj/tools/epilogue.hpp>
