#pragma once
#include<algorithm>
#include<concepts>
#include<iterator>
#include<map>
#include<memory>
#include<optional>
#include<ranges>
#include<span>
#include<type_traits>
#include<utility>
#include<vector>

#include<hysj/tools/priority_tag.hpp>
#include<hysj/tools/type_traits.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::views{

template<typename T>
inline constexpr auto no = std::views::empty<T>;

inline constexpr auto own =
  [](auto &&r)
    arrow((auto(std::ranges::owning_view{fwd(r)})))
  ;

inline constexpr auto ref =
  [](auto &r)
    arrow((auto(std::ranges::ref_view{r})))
  ;

inline constexpr auto span =
  [](auto &&r)
    arrow((std::span(r)))
  ;

inline constexpr struct{

  static constexpr auto impl(auto&& r,priority_tag<2>)
    arrow((auto(fwd(r)))(-> std::ranges::view))
  static constexpr auto impl(auto &&r,priority_tag<1>)
    arrow(((ref)(fwd(r))))
  static constexpr auto impl(auto &&r,priority_tag<0>)
    arrow(((own)(fwd(r))))

  constexpr auto operator()(auto &&r) const
    arrow((auto(impl(fwd(r),priority_tag<2>{}))))
}
  all{};

template<typename T>
using all_t = decltype(views::all(std::declval<T>()));

template<typename T>
concept viewable_range = std::ranges::range<T> && requires(T &&t){ views::all(fwd(t)); };

}
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::ranges{


inline constexpr auto tie =
  [](auto it,auto end)
    arrow((std::ranges::subrange{std::move(it),std::move(end)}))
  ;


template<typename... V>
consteval auto common_iterator_category(){
  if constexpr ((std::ranges::random_access_range<V> && ...))
    return std::random_access_iterator_tag{};
  else if constexpr ((std::ranges::bidirectional_range<V> && ...))
    return std::bidirectional_iterator_tag{};
  else if constexpr ((std::ranges::forward_range<V> && ...))
    return std::forward_iterator_tag{};
  else if constexpr ((std::ranges::input_range<V> && ...))
    return std::input_iterator_tag{};
  else
    return std::output_iterator_tag{};
}

template<typename A,typename B>
consteval auto max_iterator_category(A,B){
  if constexpr(std::is_same_v<A,B>)
    return A{};
  else if constexpr(std::derived_from<A,B>)
    return A{};
  else if constexpr(std::derived_from<B,A>)
    return B{};
}
template<typename A,typename B>
consteval auto min_iterator_category(A,B){
  if constexpr(std::is_same_v<A,B>)
    return A{};
  else if constexpr(std::derived_from<A,B>)
    return B{};
  else if constexpr(std::derived_from<B,A>)
    return A{};
}
template<typename A,typename B,typename C>
consteval auto clamp_iterator_category(A a,B b,C c){
  return max_iterator_category(
    min_iterator_category(a,b),
    c);
}

//NOTE:
//  stolen from https://github.com/cor3ntin/rangesnext/tree/master/include/cor3ntin/rangesnext
//  - jeh
template <class R>
concept simple_view = 
  (std::ranges::view<R>
   && std::ranges::range<const R>
   && std::same_as<std::ranges::iterator_t<R>, std::ranges::iterator_t<const R>>
   && std::same_as<std::ranges::sentinel_t<R>, std::ranges::sentinel_t<const R>>);

template<class I>
concept has_arrow = (std::input_iterator<I>
                     && (std::is_pointer_v<I> || requires(I i) { i.operator->(); }));

template<class T, class U>
concept different_from = !std::same_as<std::remove_cvref_t<T>, std::remove_cvref_t<U>>;

template<typename T>
concept boxable = std::move_constructible<T> && std::is_object_v<T>;

template<boxable T>
struct box:std::optional<T>{

  constexpr
  box()
    requires (!std::default_initializable<T>):
    std::optional<T>{}
  { }

  constexpr
  box()
    noexcept(std::is_nothrow_default_constructible_v<T>)
    requires (std::default_initializable<T>):
    std::optional<T>{std::in_place}
  { }

  constexpr
  box(T value)
    noexcept(noexcept(std::optional<T>{std::move(value)})):
    std::optional<T>{std::move(value)}
  { }

  box(const box&) = default;
  box(box&&) = default;

  using std::optional<T>::operator=;

  box& operator=(const box& that)
    noexcept(std::is_nothrow_copy_constructible_v<T>)
    requires (!std::copyable<T>){
    if((bool)that)
      this->emplace(*that);
    else
      this->reset();
    return *this;
  }

  box&
  operator=(box&& that)
    noexcept(std::is_nothrow_move_constructible_v<T>)
    requires (!std::movable<T>){
    if((bool)that)
      this->emplace(std::move(*that));
    else
      this->reset();
    return *this;
  }
  
};

template<boxable T> requires (std::semiregular<T>)
struct box<T>{
  [[no_unique_address]] T value;

  constexpr bool has_value()const noexcept{
    return true;
  }

  constexpr T& operator*() noexcept{
    return value;
  }

  constexpr const T& operator*() const noexcept{
    return value;
  }

  constexpr T* operator->() noexcept{
    return &value;
  }

  constexpr const T* operator->() const noexcept{
    return &value;
  }
};
    
template<typename T>
using with_ref = T&;

template<typename T>
concept can_reference = requires { typename with_ref<T>; };

inline constexpr struct {
  static constexpr auto impl(auto &&r,auto F){
    auto U = std::views::transform(fwd(r),F);
    using value_type =
      std::remove_cvref_t<
        std::ranges::range_value_t<std::remove_cvref_t<decltype(U)>>>;
    std::vector<value_type> V{};
    std::ranges::copy(U,std::back_inserter(V));
    return V;
  }
  constexpr auto operator()(auto &&r,auto F)const{
    return impl(views::all(fwd(r)),F);
  }

  constexpr auto operator()(auto &&r)const{
    return operator()(fwd(r),lift((auto)));
  }
}
  vec{};

inline constexpr struct {
  static auto impl(auto r,auto F){
    auto U = std::views::transform(r,F);
    using value_type =
      std::remove_cvref_t<std::ranges::range_value_t<std::remove_cvref_t<decltype(U)>>>;
    std::map<typename value_type::first_type,
             typename value_type::second_type> V{};
    std::ranges::copy(U,std::inserter(V,std::end(V)));
    return V;
  }
  auto operator()(auto &&r,auto F)const{
    return impl(std::views::all(r),F);
  }
  auto operator()(auto &&r)const{
    return impl(fwd(r),lift((auto)));
  }
}
  to_map{};

inline constexpr auto contains =
  [](std::ranges::input_range auto &&r,auto v)
    arrow((std::ranges::find(r,v) != std::ranges::end(r)));
inline constexpr auto contains_if =
  [](std::ranges::input_range auto &&r,auto p)
    arrow((std::ranges::find_if(r,p) != std::ranges::end(r)));

namespace concepts{

  template<typename I,typename S>
  concept fat_iterator = std::input_iterator<std::remove_cvref_t<I>> && requires(I i, S s){
    { compute_common_end(fwd(i), fwd(s)) } -> std::same_as<std::remove_cvref_t<I>>;
  };

}

inline constexpr struct common_end_fn{

  //TODO: noexecptness - jeh
  template<typename I>
  requires std::input_iterator<std::remove_cvref_t<I>>
  constexpr auto operator()(I &&,I &&end) const{
    return fwd(end);
  }
  template<typename I, typename S>
  requires (std::input_iterator<std::remove_cvref_t<I>>
            && std::sentinel_for<S, std::remove_cvref_t<I>>
            && concepts::fat_iterator<I &&, const S &>)
  constexpr auto operator()(I &&i,const S &s)const{
    return compute_common_end(fwd(i), s);
  }

  template<std::ranges::range R>
  constexpr auto operator()(R && r) const
    arrow((operator()(std::ranges::begin(r),std::ranges::end(r))))

  template<std::ranges::random_access_range R>
  requires (!std::ranges::common_range<R> && std::ranges::sized_range<R>)
  constexpr auto operator()(R && r) const
    arrow((std::ranges::next(std::ranges::begin(r),std::ranges::size(r))))

}
  common_end{};

namespace concepts{

  template<typename T>
  concept weakly_common_range = std::ranges::range<T> && requires(T &t){
    ranges::common_end(t);
  };

}

inline constexpr auto first =
  [](std::ranges::range auto &&r)
    arrow((*std::ranges::begin(r)))
  ;
//FIXME: constant-time reversible! - jeh
inline constexpr auto last =
  [](std::ranges::bidirectional_range auto &&r)
    arrow((*std::ranges::prev(common_end(r))))
  ;

template<typename I>
inline constexpr bool enable_monosentinel = false;
template<>
inline constexpr bool enable_monosentinel<std::default_sentinel_t> = true;
template<>
inline constexpr bool enable_monosentinel<std::unreachable_sentinel_t> = true;

namespace concepts{
  template<typename I>
  concept monosentinel = enable_monosentinel<std::remove_cvref_t<I>>;

  template<typename R>
  concept monosentinel_range = requires(R &&r){
    { std::ranges::end(fwd(r)) } -> monosentinel;
  };
}

namespace concepts{
  template<typename I>
  concept unreachable_sentinel = std::same_as<std::remove_cvref_t<I>,std::unreachable_sentinel_t>;

  template<typename R>
  concept infinite_range = requires(R &&r){
    { std::ranges::end(fwd(r)) } -> unreachable_sentinel;
  };
}


namespace concepts{

  template<typename T,typename V>
  concept constructible_forward_range = std::ranges::forward_range<T> &&
    std::constructible_from<V,std::ranges::range_value_t<T>>;

  template<typename T,typename V>
  concept convertible_input_range = std::ranges::input_range<T> &&
    std::convertible_to<std::ranges::range_value_t<T>,V>;
  template<typename T,typename V>
  concept convertible_forward_range = std::ranges::forward_range<T> &&
    std::convertible_to<std::ranges::range_value_t<T>,V>;
  template<typename T,typename V>
  concept convertible_bidirectional_range = std::ranges::bidirectional_range<T> &&
    std::convertible_to<std::ranges::range_value_t<T>,V>;
  template<typename T,typename V>
  concept convertible_random_access_range = std::ranges::random_access_range<T> &&
    std::convertible_to<std::ranges::range_value_t<T>,V>;
  template<typename T,typename V>
  concept convertible_contiguous_range = std::ranges::contiguous_range<T> &&
    std::convertible_to<std::ranges::range_value_t<T>,V>;
  
  template<typename T,typename V>
  concept same_input_range = std::ranges::input_range<T> &&
    std::same_as<std::ranges::range_value_t<T>,V>;
  template<typename T,typename V>
  concept same_forward_range = std::ranges::forward_range<T> &&
    std::same_as<std::ranges::range_value_t<T>,V>;
  template<typename T,typename V>
  concept same_bidirectional_range = std::ranges::bidirectional_range<T> &&
    std::same_as<std::ranges::range_value_t<T>,V>;
  template<typename T,typename V>
  concept same_random_access_range = std::ranges::random_access_range<T> &&
    std::same_as<std::ranges::range_value_t<T>,V>;
  template<typename T,typename V>
  concept same_contiguous_range = std::ranges::contiguous_range<T> &&
    std::same_as<std::ranges::range_value_t<T>,V>;

} //concepts

template<typename I,typename S>
struct sequence{
  I iterator;
  [[no_unique_address]] S sentinel;

  operator bool()const{ return iterator == sentinel; }
  std::optional<std::iter_value_t<I>> operator()(){
    if(iterator == sentinel)
      return std::nullopt;
    auto value = *iterator;
    ++iterator;
    return value;
  }
};

inline constexpr auto seq =
  [](std::ranges::borrowed_range auto &&r){
    return sequence{std::ranges::begin(r),std::ranges::end(r)};
  };

inline constexpr auto put =
  [](auto &&out,auto &&val)
    arrow((void((*out = fwd(val)),std::ranges::advance(out,1))))
  ;

inline constexpr auto vecset = []<typename P = std::ranges::less>(auto &r,P p = {}){
  std::ranges::sort(r, p);
  auto e = [&](const auto &e0,const auto &e1){
    return !p(e0, e1) && !p(e1, e0);
  };
  auto [it,end] = std::ranges::unique(r, e);
  r.erase(it, end);
};

} //hysj::ranges

namespace hysj::inline _HYSJ_VERSION_NAMESPACE::views{

inline constexpr struct {
  constexpr auto operator()(std::integral auto N)const
    arrow((std::views::iota(decltype(N){},N)))
  constexpr auto operator()(std::ranges::range auto &&R)const
    arrow((operator()(std::ranges::distance(R))))
  template<std::integral T>
  constexpr auto operator()(T i,T N)const
    arrow((std::views::iota(i,N)))
}
  indices{};

inline constexpr struct{
  template<typename T>
  constexpr auto operator()(T &v) const
    arrow((std::span<T,1>{std::addressof(v),std::addressof(v) + 1}))
  template<typename T>
  constexpr auto operator()(T &&v) const
    arrow((std::views::single(std::move(v))))
}
  one{};

inline constexpr auto slice =
  []<std::ranges::random_access_range T>(
    T &&v,std::ranges::range_difference_t<T> i,
    std::ranges::range_difference_t<T> n =
      std::numeric_limits<std::ranges::range_difference_t<T>>::max())
    requires std::ranges::sized_range<T> {
    std::ranges::range_difference_t<T> m = std::ranges::size(v);
    if(i < 0)
      i += m;
    if(n == std::numeric_limits<std::ranges::range_difference_t<T>>::max())
      n = (m - i);
    else if(n < 0)
      n = (m - i) + n;
    n = std::min(n,m - i);
    return std::views::take(std::views::drop(fwd(v),i),n);
  };

} //hysj::views
#include<hysj/tools/epilogue.hpp>
