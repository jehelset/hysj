#pragma once
#include<array>
#include<cstdint>
#include<functional>
#include<iterator>
#include<type_traits>
#include<tuple>

#include<hysj/tools/types.hpp>
#include<hysj/tools/type_traits.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::views::linear_spans{

template<ptrdiff value>
struct static_ptrdiff{
  constexpr operator ptrdiff()const noexcept{ return value; }
  
  constexpr
  static_ptrdiff<-value> operator-()const noexcept{
    return {};
  }
  template<ptrdiff v1>
  constexpr
  static_ptrdiff<value*v1> operator*(const static_ptrdiff<v1> &)noexcept{
    return {};
  }
  template<ptrdiff v1>
  constexpr
  static_ptrdiff<value+v1> operator+(const static_ptrdiff<v1> &)noexcept{
    return {};
  }
};

struct dynamic_ptrdiff{
  ptrdiff value;
  constexpr operator ptrdiff()const noexcept{ return value; }
  
  constexpr
  dynamic_ptrdiff operator-()const noexcept{
    return {-value};
  }
  constexpr
  dynamic_ptrdiff operator*(const ptrdiff &v1)noexcept{
    return {value * v1};
  }
  constexpr
  dynamic_ptrdiff operator+(const ptrdiff &v1)noexcept{
    return {value + v1};
  }
};

constexpr dynamic_ptrdiff ensure_ptrdiff(ptrdiff p)noexcept{
  return {.value = p};
}
constexpr dynamic_ptrdiff ensure_ptrdiff(dynamic_ptrdiff p)noexcept{
  return {.value = p};
}

template<ptrdiff p>
constexpr static_ptrdiff<p> ensure_ptrdiff(constant_t<p>)noexcept{
  return {};
}
template<ptrdiff p>
constexpr static_ptrdiff<p> ensure_ptrdiff(static_ptrdiff<p>)noexcept{
  return {};
}

constexpr auto ensure_data(auto *t)noexcept{
  return t;
}
constexpr auto ensure_data(auto &&t)
  arrow((t.data()))
constexpr auto ensure_data(std::contiguous_iterator auto &&i)noexcept{
  return std::to_address(fwd(i));
}


namespace concepts{
  
  namespace _{
    template<class T> struct ptrdiff_impl:std::false_type{};
    template<> struct ptrdiff_impl<ptrdiff>:std::true_type{};
    template<> struct ptrdiff_impl<dynamic_ptrdiff>:std::true_type{};
    template<ptrdiff D>
    struct ptrdiff_impl<static_ptrdiff<D>>:std::true_type{};
  }
  template<class T>
  concept ptrdiff = _::ptrdiff_impl<T>::value;

}

template<ptrdiff S,ptrdiff I>
constexpr static_ptrdiff<S * I>
linear_ptrdiff_transform(static_ptrdiff<S>,
                         static_ptrdiff<I>)noexcept
{ return {}; }

constexpr
dynamic_ptrdiff linear_ptrdiff_transform(concepts::ptrdiff auto stride,
                                         concepts::ptrdiff auto index)noexcept{
  return {stride * index};
}

template<typename T,typename S>
struct iterator{
  T *ptr;
  S stride;
  
  using iterator_category = std::random_access_iterator_tag;
  using value_type        = T;
  using difference_type   = ptrdiff;
  using reference         = T &;
  using pointer           = T *;

  constexpr
  reference operator*() const noexcept { return *ptr; }

  constexpr
  pointer operator->() const noexcept { return ptr; }

  constexpr iterator &operator++() noexcept{
    ptr = ptr + stride;
    return *this;
  }

  constexpr iterator operator++(int) noexcept{
    auto tmp = *this;
    *this++;
    return tmp;
  }

  // Bidirectional iterator requirements
  constexpr iterator &operator--() noexcept{
    ptr = ptr - stride;
    return *this;
  }

  constexpr iterator operator--(int) noexcept{
    auto tmp = *this;
    --*this;
    return tmp;
  }

  constexpr reference operator[](difference_type i) const noexcept{
    return ptr[i * stride];
  }

  constexpr iterator &operator+=(difference_type i) noexcept{
    ptr += i * stride;
    return *this;
  }

  constexpr iterator operator+(difference_type i) const noexcept{
    return { ptr + i * stride, stride };
  }
  friend constexpr iterator operator+(difference_type i,iterator r)noexcept{
    return { r.ptr + i * r.stride, r.stride };
  }

  constexpr iterator &operator-=(difference_type i) noexcept{
    ptr -= i * stride;
    return *this;
  }

  constexpr iterator operator-(difference_type i) const noexcept{
    return { ptr - i * stride, stride };
  }
  //NOTE: probably problematic - jeh
  constexpr difference_type
  operator-(const iterator &r)const noexcept{
    return (ptr - r.ptr) / stride;
  }

  friend constexpr auto operator<=>(const iterator &,
                                    const iterator &) = default;
};

template<typename T,typename S,typename E>
struct view{
  using element_type     = T;
  using value_type       = std::remove_cv_t<T>;
  using size_type        = std::size_t;
  using difference_type  = ptrdiff;
  using pointer          = std::add_pointer_t<T>;
  using const_pointer    = std::add_pointer_t<std::add_const_t<T>>;
  using reference        = std::add_lvalue_reference_t<element_type>;
  using const_reference  = std::add_lvalue_reference_t<std::add_const_t<T>>;
  using iterator         = linear_spans::iterator<T,S>;
  
  pointer ptr;
  [[no_unique_address]] S stride;
  [[no_unique_address]] E extent;
  
  [[nodiscard]] constexpr size_type
  size()const noexcept{
    return extent;
  }
  [[nodiscard]] constexpr bool
  empty() const noexcept{
    return size() == 0;
  }

  [[nodiscard]] constexpr reference
  operator[](ptrdiff index)const noexcept{
    HYSJ_ASSERT(0 <= index && static_cast<std::size_t>(index) < size());
    const ptrdiff linear_index = stride * index;
    return *(ptr + linear_index);
  }

  [[nodiscard]] constexpr reference
  front()const noexcept{
    HYSJ_ASSERT(!empty());
    return operator[](0);
  }

  [[nodiscard]]constexpr reference
  back() const noexcept{
    HYSJ_ASSERT(!empty());
    return operator[](size() - 1);
  }
  [[nodiscard]]constexpr pointer
  data() const noexcept{
    return ptr;
  }
  
  [[nodiscard]] auto
  begin()const noexcept{
    return iterator{ptr,stride};
  }
  [[nodiscard]] auto
  end()const noexcept{
    const ptrdiff end = (ptrdiff)stride * extent;
    return iterator{ptr + end,stride};
  }

};

template<typename T,typename S,typename E>
view(T *,S,E) -> view<T,S,E>;

namespace concepts{
  namespace _{
    template<typename T> struct linear_span_impl:std::false_type{};
    template<typename T,typename S,typename E>
    struct linear_span_impl<view<T,S,E>>:
      std::true_type{};
  }
  template<typename S>
  concept linear_span = _::linear_span_impl<uncvref_t<S>>::value;
}

using default_stride_t = static_ptrdiff<1>;
template<std::size_t N,typename S>
using default_extent_t = static_ptrdiff<(N == 0) ? 0 : (N + N % S{}) / S{}>;

struct factory{

  //FIXME: gets noexcept wrong - jeh
  constexpr auto
  operator()(auto &&r,auto s,auto e)const
    arrow((view{
            .ptr = ensure_data(fwd(r)),
            .stride = ensure_ptrdiff(s),
            .extent = ensure_ptrdiff(e)
        }))
  
  template<typename T,typename S = default_stride_t>
  constexpr auto
  operator()(T &&r,S s = {}) const noexcept
    requires (is_std_array<std::remove_cvref_t<T>>()){
    using E = default_extent_t<kumi::size_v<T>,S>;
    return operator()(fwd(r),s,E{});
  }
  
  template<typename T,std::size_t N,typename S = default_stride_t>
  constexpr auto
  operator()(std::span<T,N> r,S s = {})const noexcept{
    if constexpr(N == std::dynamic_extent)
      return operator()(r,s,dynamic_ptrdiff{r.size()});
    else
      return operator()(r,s,static_ptrdiff<N>{});
  }

  template<typename T,std::size_t N,typename S = default_stride_t>
	constexpr auto operator()(T (&A)[N],
                            S s = {}) const noexcept{
    return operator()(A,s,static_ptrdiff<N>{});
  }

  template<typename R,typename S = default_stride_t>
	requires (std::ranges::contiguous_range<R>
            && std::ranges::sized_range<R> //NOTE: const implies read only - jeh
            && (std::ranges::borrowed_range<R> 
                || std::is_const_v<std::ranges::range_value_t<R>>)
            && (!is_std_array<std::remove_cvref_t<R>>())
            && (!is_std_span<std::remove_cvref_t<R>>())
            && (!std::is_array_v<std::remove_cvref_t<R>>))
  constexpr auto operator()(R&& r,S s = {}) const 
    arrow((operator()(std::ranges::data(r),
                      s,
                      std::ranges::size(r))))
};

constexpr auto reverse(concepts::linear_span auto a)noexcept{
  return view{
    .ptr    = a.ptr + a.stride*(a.extent - 1),
    .stride = -a.stride,
    .extent = a.extent
  };
}

} //hysj::views::linear_spans

namespace hysj::inline _HYSJ_VERSION_NAMESPACE::views{

using linear_spans::dynamic_ptrdiff;
using linear_spans::static_ptrdiff;
inline constexpr linear_spans::factory linear_span{};
inline constexpr linear_spans::factory lspan{};

} //hysj::views

template <typename V, typename S, typename E>
inline constexpr bool std::ranges::enable_borrowed_range<
  hysj::_HYSJ_VERSION_NAMESPACE::views::linear_spans::view<V, S, E>> = true;

template <typename V, typename S, typename E>
inline constexpr bool std::ranges::enable_view<
  hysj::_HYSJ_VERSION_NAMESPACE::views::linear_spans::view<V, S, E>> = true;

#include <hysj/tools/epilogue.hpp>
