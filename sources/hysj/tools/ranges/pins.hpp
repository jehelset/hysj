#pragma once
#include<array>
#include<concepts>
#include<ranges>
#include<memory>
#include<type_traits>

#include<hysj/tools/functional/utility.hpp>
#include<hysj/tools/functional/combine.hpp>
#include<hysj/tools/ranges/utility.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::views::pins{

template<typename... X>
concept pinable =
  requires { typename std::common_type_t<X...>; }
  && (std::constructible_from<std::decay_t<std::common_type_t<X...>>,X&&> && ...)
  ;

template<typename X,std::size_t N>
using container = std::array<X,N>;

template<typename... X>
using container_t = container<std::decay_t<std::common_type_t<X...>>,sizeof...(X)>;
  
template<typename X,std::size_t N>
struct iterator{
  std::shared_ptr<const container<X,N>> base;
  typename container<X,N>::const_iterator base_iterator;
  
  using value_type = typename container<X,N>::value_type;
  using difference_type = typename container<X,N>::difference_type;

  constexpr auto
  operator*() const
    arrow((*base_iterator))

  constexpr auto &operator++(){
    ++base_iterator;
    return *this;
  }
  constexpr auto operator++(int){
    auto that = *this;
    ++*this;
    return that;
  }
  constexpr auto &operator--(){
    --base_iterator;
    return *this;
  }
  constexpr auto operator--(int){
    auto that = *this;
    --*this;
    return that;
  }
  constexpr auto &operator+=(difference_type n){
    base_iterator += n;
    return *this;
  }
  constexpr auto &operator-=(difference_type n){
    base_iterator -= n;
    return *this;
  }

  constexpr decltype(auto) operator[](difference_type n) const{
    return base_iterator[n];
  }

  friend constexpr bool operator==(const iterator &x, const iterator &y){
    return x.base_iterator == y.base_iterator;
  }
  
  friend
  constexpr auto
    operator<=>(const iterator &x, const iterator &y)
      arrow((x.base_iterator <=> y.base_iterator))

  friend
  constexpr iterator operator+(iterator i, difference_type n){
    return {
      .base = i.base,
      .base_iterator = i.base_iterator + n
    };
  }

  friend
  constexpr iterator operator+(difference_type n, iterator i){
    return {
      .base = i.base,
      .base_iterator = i.base_iterator + n
    };
  }

  friend
  constexpr iterator operator-(iterator i, difference_type n){
    return {
      .base = i.base,
      .base_iterator = i.base_iterator - n
    };
  }

  friend
  constexpr difference_type operator-(const iterator &x, const iterator &y){
    return x.base_iterator - y.base_iterator;
  }

  friend
  constexpr auto iter_move(const iterator &i)
    arrow((std::move(*i)))

  friend
  constexpr auto
  iter_swap(const iterator &x, const iterator &y)
    arrow((std::ranges::iter_swap(x.base_iterator, y.base_iterator)))

};

template<typename X,std::size_t N>
struct view:std::ranges::view_interface<view<X, N>>{
  std::shared_ptr<const container<X,N>> base;

  constexpr iterator<X,N> begin()const{
    return { base,std::ranges::begin(*base) }; 
  }
  constexpr iterator<X,N> end()const{
    return { base,std::ranges::end(*base) }; 
  }
  static constexpr auto size(){
    return N;
  }
};

struct factory{
  template<typename... X>
  requires pinable<X...>
  constexpr view<std::common_type_t<X...>,sizeof...(X)>
    operator()(X &&... x)const{
    return {
      .base = std::make_shared<const container_t<X...>>(container_t<X...>{{fwd(x)...}})
    };
  }
};

} //hysj::views::pins

namespace hysj::inline _HYSJ_VERSION_NAMESPACE::views{

inline constexpr pins::factory pin{};
template<typename T>
inline constexpr auto pincast = combine<>(pin,hysj::_HYSJ_VERSION_NAMESPACE::cast<T>);

} //hysj::views

template <typename X,std::size_t N>
inline constexpr bool std::ranges::enable_borrowed_range<
  hysj::_HYSJ_VERSION_NAMESPACE::views::pins::view<X,N>> =
    true;
template <typename X,std::size_t N>
inline constexpr bool std::ranges::enable_view<
  hysj::_HYSJ_VERSION_NAMESPACE::views::pins::view<X,N>> =
    true;

#include <hysj/tools/epilogue.hpp>
