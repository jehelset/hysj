#pragma once
#include<concepts>
#include<iterator>
#include<ranges>
#include<type_traits>
#include<utility>

#include<hysj/tools/functional/bind.hpp>
#include<hysj/tools/functional/compose.hpp>
#include<hysj/tools/functional/invoke.hpp>
#include<hysj/tools/functional/utility.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/type_traits.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::views::maps{

template<typename V,typename F>
concept mapable =
  (std::ranges::input_range<V>
   && std::copy_constructible<F>
   && std::ranges::view<V>
   && std::is_object_v<F>
   && std::regular_invocable<F&, std::ranges::range_reference_t<V>>
   && ranges::can_reference<invoke_result_t<F&,std::ranges::range_reference_t<V>>>);

template<typename D,typename F>
struct traits{
  static constexpr bool is_const = std::is_const_v<D>;
  
  using domain_type = D;
  using domain_iterator_type = std::ranges::iterator_t<domain_type>;
  using domain_sentinel_type = std::ranges::sentinel_t<domain_type>;
  using domain_reference_type = std::ranges::range_reference_t<domain_type>;

  using difference_type = std::ranges::range_difference_t<domain_type>;
  
  using value_type =
    std::remove_cvref_t<invoke_result_t<F &,domain_reference_type>>;
  
  using invoke_result_type =
    invoke_result_t<F&, domain_reference_type>;

  using iterator_concept =
    decltype(
      ranges::max_iterator_category(
        ranges::common_iterator_category<domain_type>(),
        std::input_iterator_tag{}));

  static constexpr bool is_invocable =
    std::is_invocable_v<F,domain_reference_type>;
  static constexpr bool is_nothrow_invocable =
    std::is_nothrow_invocable_v<F,domain_reference_type>;
  static constexpr bool is_nothrow_const_invocable =
    std::is_nothrow_invocable_v<const F,domain_reference_type>;
  
  static constexpr bool is_common =
    std::ranges::common_range<domain_type>;
  static constexpr bool is_forward =
    std::ranges::forward_range<domain_type>;
  static constexpr bool is_bidirectional =
    std::ranges::bidirectional_range<domain_type>;
  static constexpr bool is_random_access =
    std::ranges::random_access_range<domain_type>;
  static constexpr bool is_sized = std::ranges::sized_range<domain_type>;

  static constexpr bool is_iterator_equality_comparable =
    std::equality_comparable<domain_iterator_type>;
  static constexpr bool is_iterator_three_way_comparable =
    std::three_way_comparable<domain_iterator_type>;
  static constexpr bool is_indirectly_swappable =
    std::indirectly_swappable<domain_iterator_type>;
};

template<typename D,typename F,bool constness>
using traits_t = traits<add_const_if<D,constness>, F>;

template<typename D,typename F>
using mutable_traits_t = traits_t<D,F,false>;
template<typename D,typename F>
using const_traits_t = traits_t<D,F,true>;

template<typename D,typename F>
struct iterator_prologue : traits<D,F>{};
  
template<typename D,typename F>
requires traits<D,F>::is_forward
struct iterator_prologue<D,F> : traits<D,F>{
  using iterator_category =
    decltype(
      []{
        if constexpr (
          std::is_lvalue_reference_v<
            typename iterator_prologue::domain_reference_type>)
          return ranges::min_iterator_category(
            std::random_access_iterator_tag{},
            typename std::iterator_traits<
              typename iterator_prologue::domain_iterator_type
                >::iterator_category{});
        else
          return std::input_iterator_tag{};
      }());
};

template<typename D,typename F>
struct iterator:iterator_prologue<D,F>{
  
  typename iterator::domain_iterator_type domain_iterator;
  [[no_unique_address]] ranges::box<F> function;

  constexpr auto
  operator*()
    arrow((invoke(*function, *domain_iterator)))

  constexpr auto
  operator*() const
    arrow((invoke(*function, *domain_iterator)))

  constexpr iterator &operator++(){
    ++domain_iterator;
    return *this;
  }

  constexpr void operator++(int){
    ++domain_iterator;
  }

  constexpr iterator operator++(int) requires iterator::is_forward{
    auto that = *this;
    ++*this;
    return that;
  }

  constexpr iterator &operator--() requires iterator::is_bidirectional{
    --domain_iterator;
    return *this;
  }

  constexpr iterator
  operator--(int) requires iterator::is_bidirectional{
    auto that = *this;
    --*this;
    return that;
  }

  constexpr iterator &operator+=(typename iterator::difference_type n)
    requires iterator::is_random_access{
    domain_iterator += n;
    return *this;
  }

  constexpr iterator &operator-=(typename iterator::difference_type n)
    requires iterator::is_random_access{
    domain_iterator -= n;
    return *this;
  }

  constexpr decltype(auto) operator[](typename iterator::difference_type n) const
    requires iterator::is_random_access{
    return invoke(*function, domain_iterator[n]);
  }

  friend constexpr bool operator==(const iterator &x, const iterator &y)
    requires iterator::is_iterator_equality_comparable{
    return x.domain_iterator == y.domain_iterator;
  }
  
  friend
  constexpr auto
  operator<=>(const iterator &x, const iterator &y)
    requires (iterator::is_random_access
              && iterator::is_iterator_three_way_comparable)
  { return x.domain_iterator <=> y.domain_iterator; }

  friend
  constexpr iterator
  operator+(iterator i, typename iterator::difference_type n)
    requires iterator::is_random_access{
    return {
      .domain_iterator = i.domain_iterator + n,
      .function = i.function
    };
  }

  friend
  constexpr iterator
  operator+(typename iterator::difference_type n, iterator i)
    requires iterator::is_random_access{
    return {
      .domain_iterator = i.domain_iterator + n,
      .function = i.function
    };
  }

  friend
  constexpr iterator
  operator-(iterator i, typename iterator::difference_type n)
    requires iterator::is_random_access{
    return {
      .domain_iterator = i.domain_iterator - n,
      .function = i.function
    };
  }

  friend
  constexpr typename iterator::difference_type
  operator-(const iterator &x, const iterator &y)
    requires std::sized_sentinel_for<typename iterator::domain_iterator_type,
                                     typename iterator::domain_iterator_type>{
    return x.domain_iterator - y.domain_iterator;
  }

  friend
  constexpr decltype(auto)
  iter_move(const iterator &i) noexcept(noexcept(*i)){
    if constexpr(std::is_lvalue_reference_v<decltype(*i)>)
      return std::move(*i);
    else
      return *i;
  }

  friend
  constexpr void
  iter_swap(const iterator &x, const iterator &y)
    noexcept(noexcept(std::ranges::iter_swap(x.domain_iterator,
                                             y.domain_iterator)))
    requires iterator::is_indirectly_swappable{
    return std::ranges::iter_swap(x.domain_iterator, y.domain_iterator);
  }

};

template<typename D,typename F,bool constness>
using iterator_t = iterator<add_const_if<D,constness>,
                            add_const_if<F,constness>>;

template<typename D,typename F>
struct sentinel:traits<D,F>{

  typename sentinel::domain_sentinel_type domain_sentinel;
  
  friend
  constexpr bool
  operator==(const iterator<D,F>& x, const sentinel& y)
    requires std::sentinel_for<typename sentinel::domain_sentinel_type,
                               typename sentinel::domain_iterator_type>{
    return x.domain_iterator == y.domain_sentinel;
  }

  friend
  constexpr typename sentinel::difference_type
  operator-(const iterator<D,F>& x, const sentinel& y)
      requires std::sized_sentinel_for<typename sentinel::domain_sentinel_type,
                                       typename sentinel::domain_iterator_type>{
    return x.domain_iterator - y.domain_sentinel;
  }

  friend 
  constexpr typename sentinel::difference_type
  operator-(const sentinel& y, const iterator<D,F>& x)
    requires std::sized_sentinel_for<typename sentinel::domain_sentinel_type,
                                     typename sentinel::domain_iterator_type>{
    return y.domain_sentinel - x.domain_iterator;
  }
  


};

//FIXME: require copyable? - jeh
template<typename I, typename D, typename F>
requires std::same_as<std::remove_cvref_t<I>, iterator<D, F>>
constexpr auto compute_common_end(I &&x, const sentinel<D, F>& y)
  arrow((iterator<D, F>{
        .domain_iterator = ranges::common_end(fwd(x).domain_iterator, y.domain_sentinel),
        .function = x.function
      }))
  ;

template<typename D,typename F,bool constness>
using sentinel_t = sentinel<add_const_if<D,constness>,add_const_if<F,constness>>;

template<typename D,typename F>
constexpr auto make_begin(D &domain,F &function){
  constexpr bool constness = std::is_const_v<D>;
  return iterator_t<D,F,constness>{
    .domain_iterator = std::ranges::begin(domain),
    .function = ranges::box<F>{function}
  };
}

template<typename D,typename F>
constexpr auto make_end(D &domain,F &function){
  constexpr bool constness = std::is_const_v<D>;
  using traits = traits_t<D,F,constness>;
  if constexpr(traits::is_common)
    return iterator_t<D,F,constness>{
      .domain_iterator = std::ranges::end(domain),
      .function = ranges::box<F>{function}
    };
  else
    return sentinel_t<D,F,constness>{
      .domain_sentinel = std::ranges::end(domain)
    };
}

template<typename D,typename F>
struct view:std::ranges::view_interface<view<D, F>>{
  
  static constexpr bool domain_is_const_range =
    std::ranges::range<const D>;

  ranges::box<D> domain; //FIXME: hold iterators by value! ranges::base<D> / ranges::make_base<D> - jeh? 
  [[no_unique_address]] ranges::box<F> function; 

  constexpr auto begin(){
    return (make_begin)(*domain,*function);
  }
  constexpr auto begin()const requires
    (domain_is_const_range &&
     const_traits_t<D,F>::is_invocable){
    return (make_begin)(*domain,*function);
  }
  constexpr auto end(){
    return (make_end)(*domain,*function);
  }
  constexpr auto end()const requires(
    domain_is_const_range
    && const_traits_t<D,F>::is_invocable){
    return (make_end)(*domain,*function);
  }
  constexpr auto size() requires mutable_traits_t<D,F>::is_sized {
    return std::ranges::size(*domain);
  }
  constexpr auto size() const requires (
    domain_is_const_range
    && const_traits_t<D,F>::is_sized){
    return std::ranges::size(*domain);
  }
};

struct factory{
  template<std::ranges::viewable_range D, typename F>
  requires mapable<std::views::all_t<D>,std::decay_t<F>>
  constexpr view<std::views::all_t<D>,std::decay_t<F>>
    operator()(D&& d, F&& f)const{
    return {
      .domain   = ranges::box<std::views::all_t<D>>{fwd(d)},
      .function = ranges::box<std::decay_t<F>>{fwd(f)}
    };
  }
};

} //hysj::views::maps

namespace hysj::inline _HYSJ_VERSION_NAMESPACE::views{

inline constexpr maps::factory map{};

//FIXME: how to compose?
inline constexpr auto at = [](auto &&V,auto &&I){
  return views::map(fwd(I),[V = std::views::all(fwd(V))](auto &&i) -> decltype(auto){
    return V[fwd(i)];
  });
};

template<typename T>
inline constexpr auto cast = bind<-1>(map,hysj::cast<T>);

inline constexpr auto deref = bind<-1>(map,hysj::deref);

} //hysj::views

template <typename V,typename F>
inline constexpr bool std::ranges::enable_borrowed_range<
  hysj::_HYSJ_VERSION_NAMESPACE::views::maps::view<V,F>> =
    std::ranges::enable_borrowed_range<V>;
template <typename V,typename F>
inline constexpr bool std::ranges::enable_view<
  hysj::_HYSJ_VERSION_NAMESPACE::views::maps::view<V,F>> =
    true;

#include<hysj/tools/epilogue.hpp>
