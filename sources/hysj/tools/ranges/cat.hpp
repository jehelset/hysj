#pragma once

#include<array>
#include<cstdint>
#include<concepts>
#include<functional>
#include<numeric>
#include<ranges>
#include<type_traits>
#include<utility>
#include<variant>

#include<kumi/tuple.hpp>

#include<hysj/tools/types.hpp>
#include<hysj/tools/functional/with.hpp>
#include<hysj/tools/functional/pick.hpp>
#include<hysj/tools/functional/combine.hpp>
#include<hysj/tools/functional/on.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/ranges/map.hpp>

#include <hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::views::cats{

//NOTE:
//  stolen from Hui Xie at https://github.com/huixie90/cpp_papers/blob/main/impl/concat/concat.hpp
//  - jeh

template<class... T>
using reference_t = std::common_reference_t<std::ranges::range_reference_t<T>...>;

template<class... T>
using rvalue_reference_t = std::common_reference_t<std::ranges::range_rvalue_reference_t<T>...>;

template<class... T>
using value_t = std::common_type_t<std::ranges::range_value_t<T>...>;

template<class... T>
using difference_t = std::common_type_t<std::ranges::range_difference_t<T>...>;

template<class... T>
using size_t =
  std::make_unsigned_t<std::common_type_t<decltype(std::ranges::size(std::declval<T>()))...>>;

template <class Ref, class RRef, class It>
concept indirectly_readable_impl = requires (const It it){
  static_cast<Ref>(*it);
  static_cast<RRef>(std::ranges::iter_move(it));
};

template <class... T>
concept indirectly_readable =
  std::common_reference_with<reference_t<T...> &&, value_t<T...>&>&&
  std::common_reference_with<reference_t<T...>&&, rvalue_reference_t<T...>&&>&&
  std::common_reference_with<rvalue_reference_t<T...>&&, value_t<T...> const&> &&
  (indirectly_readable_impl<reference_t<T...>, rvalue_reference_t<T...>, std::ranges::iterator_t<T>> && ...);

template <class... T>
concept concatable = (requires {
    typename reference_t<T...>;
    typename value_t<T...>;
    typename rvalue_reference_t<T...>;
  }) && indirectly_readable<T...>;

template<typename... T>
struct traits{

  static constexpr bool is_homogeneuous = is_same_v<T...>;
  static constexpr bool is_iterator_homogeneuous = is_same_v<std::ranges::iterator_t<T>...>;
  static constexpr bool is_sentinel_homogeneuous = is_same_v<std::ranges::sentinel_t<T>...>;
  static constexpr bool is_monosentinel = (ranges::concepts::monosentinel_range<T> && ...);
  static constexpr bool is_infinite = (ranges::concepts::infinite_range<T> || ...);

  static constexpr bool is_sized = (std::ranges::sized_range<T> && ...);
  static constexpr bool is_referencing = std::is_lvalue_reference_v<reference_t<T...>>;

  static constexpr bool has_borrowed_base = (std::ranges::borrowed_range<T> || ...);
  static constexpr bool is_borrowed = (std::ranges::borrowed_range<T> && ...);
  static constexpr bool is_common = std::ranges::common_range<pick_t<-1,T...>> && !has_borrowed_base;

  static constexpr bool prev_base_is_weakly_common = []{
    constexpr std::array ctr{ranges::concepts::weakly_common_range<T>...};
    for(std::size_t i = 0;i + 1 < sizeof...(T);++i)
      if(!ctr[i])
        return false;
    return true;
  }();

  static constexpr bool is_forward = (std::ranges::forward_range<T> && ...);
  static constexpr bool is_bidirectional = prev_base_is_weakly_common && (std::ranges::bidirectional_range<T> && ...);
  static constexpr bool is_random_access = is_bidirectional && (std::ranges::random_access_range<T> && ...) && (std::ranges::sized_range<T> && ...) ;

  static constexpr bool is_equality_comparable =
    (std::equality_comparable<std::ranges::iterator_t<T>> &&...);
  static constexpr bool is_iterator_three_way_comparable =
    (std::three_way_comparable<std::ranges::iterator_t<T>> && ...);
  static constexpr bool is_indirectly_swappable = //FIXME: need to compare all types... - jeh
    (std::indirectly_swappable<std::ranges::iterator_t<T>> && ...);
  static constexpr bool is_nothrow_swappable = false; //FIXME: compute correctly... - jeh

  static constexpr bool is_nothrow_iterator_movable =
    ((std::is_nothrow_invocable_v<decltype(std::ranges::iter_move),const std::ranges::iterator_t<T>&> &&
      std::is_nothrow_convertible_v<std::ranges::range_rvalue_reference_t<T>,rvalue_reference_t<T...>>) && ...);

  using iterator_concept = decltype([]{
    if constexpr (is_random_access)
      return std::random_access_iterator_tag{};
    else if constexpr (is_bidirectional)
      return std::bidirectional_iterator_tag{};
    else if constexpr (is_forward)
      return std::forward_iterator_tag{};
    else
      return std::input_iterator_tag{};
  }());
};

template<typename... T>
struct iterator_prologue : traits<T...>{};

template<typename... T>
requires traits<T...>::is_forward
struct iterator_prologue<T...> : traits<T...>{
  using iterator_category =
    decltype(
      []{
        if constexpr(!traits<T...>::is_referencing)
          return std::input_iterator_tag{};
        else if constexpr((std::derived_from<std::random_access_iterator_tag, T> &&
                           ...) && traits<T...>::is_random_access)
          return std::random_access_iterator_tag{};
        else if constexpr((std::derived_from<std::bidirectional_iterator_tag, T> &&
                           ...) && traits<T...>::is_bidirectional)
          return std::bidirectional_iterator_tag{};
        else if constexpr((std::derived_from<std::forward_iterator_tag, T> &&
                           ...) && traits<T...>::is_forward)
          return std::forward_iterator_tag{};
        else
          return std::input_iterator_tag{};
      }());
};

template<typename... T>
constexpr auto view_cache(auto &v)noexcept{
  if constexpr(traits<T...>::is_borrowed)
    return empty<0>{};
  else
    return std::addressof(v);
}

template<typename V,typename... T>
using view_cache_t = decltype(view_cache<T...>(std::declval<V &>()));

template<std::size_t I,typename T>
constexpr auto base_cache0(T &t){
  if constexpr(std::ranges::borrowed_range<T>)
    return std::ranges::begin(t);
  else
    return empty<0,I>{};
}

template<typename... T>
constexpr auto base_caches0(T &... t){
  return [&]<std::size_t... I>(std::index_sequence<I...>){
    if constexpr(traits<T...>::is_homogeneuous && traits<T...>::is_borrowed)
      return std::array{base_cache0<I>(t)...};
    else
      return kumi::tuple{base_cache0<I>(t)...};
  }(std::make_index_sequence<sizeof...(T)>{});
}

template<typename... T>
using base_caches0_t = decltype(base_caches0(std::declval<T &>()...));

template<std::size_t I,typename T>
constexpr auto base_cache1(T &t){
  if constexpr(std::ranges::borrowed_range<T>){
    if constexpr(ranges::concepts::monosentinel_range<T>)
      return empty<1,I>{};
    else
      return std::ranges::end(t);
  }
  else
    return empty<1,I>{};
}

template<typename... T>
constexpr auto base_caches1(T &... t){
  return [&]<std::size_t... I>(std::index_sequence<I...>){
    if constexpr(traits<T...>::is_borrowed
                 && traits<T...>::is_sentinel_homogeneuous
                 && !traits<T...>::is_monosentinel)
      return std::array{base_cache1<I>(t)...};
    else
      return kumi::tuple{base_cache1<I>(t)...};
  }(std::make_index_sequence<sizeof...(T)>{});
}

template<typename... T>
using base_caches1_t = decltype(base_caches1(std::declval<T &>()...));

template<std::size_t I,typename...T>
constexpr decltype(auto) base_begin_fn(auto &view,auto &bases0){
  if constexpr(std::ranges::borrowed_range<pick_t<I,T...>>){
    if constexpr(traits<T...>::is_homogeneuous)
      return std::get<I>(bases0);
    else
      return kumi::get<I>(bases0);
  }
  else{
    if constexpr(traits<T...>::is_homogeneuous)
      return std::ranges::begin(std::get<I>(view->bases));
    else
      return std::ranges::begin(kumi::get<I>(view->bases));
  }
}
template<std::size_t I,typename...T>
constexpr decltype(auto) base_end_fn(auto &view,auto &bases1){
  using U = pick_t<I,T...>;
  if constexpr(std::ranges::borrowed_range<U>){
    if constexpr(ranges::concepts::monosentinel_range<U>)
      return std::ranges::sentinel_t<U>{};
    else if constexpr(traits<T...>::is_homogeneuous)
      return std::get<I>(bases1);
    else
      return kumi::get<I>(bases1);
  }
  else{
    if constexpr(traits<T...>::is_homogeneuous)
      return std::ranges::end(std::get<I>(view->bases));
    else
      return std::ranges::end(kumi::get<I>(view->bases));
  }
}

template<typename... T>
using base_iterator_t = std::variant<std::ranges::iterator_t<T>...>;

template<typename V,typename... T>
requires concatable<T...>
struct iterator:iterator_prologue<T...>{

  using value_type            = value_t<T...>;
  using reference_type        = reference_t<T...>;
  using rvalue_reference_type = rvalue_reference_t<T...>;
  using difference_type       = difference_t<T...>;

  static constexpr std::size_t N = sizeof...(T);

  [[no_unique_address]] V view;
  [[no_unique_address]] base_caches0_t<T...> bases0;
  [[no_unique_address]] base_caches1_t<T...> bases1;
  base_iterator_t<T...> base_iterator;

  constexpr auto &base()     { return base_iterator; }
  constexpr auto &base()const{ return base_iterator; }
  template<std::size_t I> constexpr auto &base()       {
    return std::get<I>(base());
  }
  template<std::size_t I> constexpr auto &base() const {
    return std::get<I>(base());
  }

  template<std::size_t I>
  constexpr decltype(auto) base_begin(){
    return (base_begin_fn<I,T...>)(view,bases0);
  }
  template<std::size_t I>
  constexpr decltype(auto) base_begin()const{
    return (base_begin_fn<I,T...>)(view,bases0);
  }
  template<std::size_t I>
  constexpr decltype(auto) base_end(){
    return (base_end_fn<I,T...>)(view,bases1);
  }
  template<std::size_t I>
  constexpr decltype(auto) base_end()const{
    return (base_end_fn<I,T...>)(view,bases1);
  }

  template<std::size_t I>
  constexpr auto base_size()const requires std::ranges::sized_range<pick_t<I,T...>>{
    if constexpr(std::ranges::borrowed_range<pick_t<I,T...>>)
      return std::ranges::distance(base_begin<I>(),base_end<I>());
    else if constexpr(traits<T...>::is_homogeneuous)
      return std::ranges::size(std::get<I>(view->bases));
    else
      return std::ranges::size(kumi::get<I>(view->bases));
  }

  constexpr auto with_base_index(auto &&f)const{
    return with<N>(f,base().index());
  }

  template<std::size_t I>
  constexpr void satisfy(){
    if constexpr(I + 1 != N)
      if(base<I>() == base_end<I>()){
        base().template emplace<I + 1>(base_begin<I + 1>());
        satisfy<I + 1>();
      }
  }

  template<std::size_t I>
  auto base_common_end(){
    using U = pick_t<I,T...>;
    if constexpr(std::ranges::borrowed_range<U>)
      return ranges::common_end(base_begin<I>(), base_end<I>());
    else if constexpr(traits<T...>::is_homogeneuous)
      return ranges::common_end(std::get<I>(view->bases));
    else
      return ranges::common_end(kumi::get<I>(view->bases));
  }

  template <std::size_t I>
  constexpr void prev(){
    if constexpr(I == 0)
      --std::get<0>(base());
    else{
      if(base<I>() != base_begin<I>())
        --base<I>();
      else{
        base().template emplace<I - 1>(base_common_end<I - 1>());
        prev<I - 1>();
      }
    }
  }

  template<std::size_t I>
  constexpr void advance_fwd(difference_type current_offset,
                             difference_type steps){
    if constexpr(I + 1 == N)
      base<I>() += steps;
    else{
      auto n_size = static_cast<difference_type>(base_size<I>());
      if(current_offset + steps < n_size)
        base<I>() += steps;
      else{
        base().template emplace<I + 1>(base_begin<I + 1>());
        advance_fwd<I + 1>(0, current_offset + steps - n_size);
      }
    }
  }

  template <std::size_t I>
  constexpr void advance_bwd(difference_type current_offset,
                             difference_type steps){
    if constexpr(I == 0)
      base<I>() -= steps;
    else{
      if(current_offset >= steps)
        base<I>() -= steps;
      else{
        base().template emplace<I - 1>(base_common_end<I - 1>());
        advance_bwd<I - 1>(base_size<I - 1>(),steps - current_offset);
      }
    }
  }

  auto base_sizes()const requires iterator::is_sized{
    return [&]<std::size_t... I>(std::index_sequence<I...>){
      return std::array{static_cast<size_t<T...>>(base_size<I>())...};
    }(std::make_index_sequence<N>{});
  }

  constexpr auto operator*() const
    arrow((std::visit([](auto&& x) -> reference_type { return *x; }, base())));

  constexpr iterator& operator++(){
    with_base_index(
      [&](auto I){
        ++base<I>();
        satisfy<I>();
      });
    return *this;
  }

  constexpr void operator++(int)requires (!iterator::is_forward){ ++*this; }
  constexpr iterator operator++(int)requires iterator::is_forward{
    auto tmp = *this;
    ++*this;
    return tmp;
  }

  constexpr iterator& operator--() requires iterator::is_bidirectional{
    with_base_index([&](auto I){ prev<I>(); });
    return *this;
  }

  constexpr iterator operator--(int) requires iterator::is_bidirectional{
    auto tmp = *this;
    --*this;
    return tmp;
  }

  constexpr iterator& operator+=(difference_type n) requires iterator::is_random_access{
    if(n != 0)
      with_base_index(
        [&](auto I){
          if(n > 0)
            advance_fwd<I>(base<I>() - base_begin<I>(), n);
          else
            advance_bwd<I>(base<I>() - base_begin<I>(),-n);
        });
    return *this;
  }

  constexpr iterator& operator-=(difference_type n) requires iterator::is_random_access{
    *this += -n;
    return *this;
  }
  constexpr decltype(auto) operator[](difference_type n) const requires iterator::is_random_access{
    return *((*this) + n);
  }

  friend constexpr bool operator==(const iterator& x, const iterator& y)
    requires iterator::is_equality_comparable{
    return x.base() == y.base();
  }

  friend constexpr bool operator!=(const iterator& x, const iterator& y)
    requires iterator::is_equality_comparable{
    return !(x == y);
  }

  friend constexpr bool operator==(const iterator& x, const std::default_sentinel_t &){
    return x.base().index() + 1 == N && x.base<N - 1>() == x.base_end<N - 1>();
  }

  friend constexpr bool operator!=(const iterator& x, const std::default_sentinel_t &y){
    return !(x == y);
  }

  friend constexpr bool operator<(const iterator& x, const iterator& y)
    requires iterator::is_random_access{
    return x.base() < y.base();
  }

  friend constexpr bool operator>(const iterator& x, const iterator& y)
    requires iterator::is_random_access{
    return y < x;
  }

  friend constexpr bool operator<=(const iterator& x, const iterator& y)
    requires iterator::is_random_access{
    return !(y < x);
  }

  friend constexpr bool operator>=(const iterator& x, const iterator& y)
    requires iterator::is_random_access{
    return !(x < y);
  }

  friend constexpr auto operator<=>(const iterator& x, const iterator& y)
    requires iterator::is_iterator_three_way_comparable{
    return x.base() <=> y.base();
  }

  friend constexpr iterator operator+(const iterator& it, difference_type n)
    requires iterator::is_random_access{
    return auto(it) += n;
  }

  friend constexpr iterator operator+(difference_type n, const iterator& x)
    requires iterator::is_random_access{
    return x + n;
  }

  friend constexpr iterator operator-(const iterator& x, difference_type n)
    requires iterator::is_random_access{
    return auto(x) -= n;
  }

  friend constexpr difference_type operator-(const iterator& x, const iterator& y)
    requires iterator::is_random_access{
    auto ix = x.base().index();
    auto iy = y.base().index();
    if(ix > iy){
      const auto x_sizes = x.base_sizes();
      auto in_between = std::accumulate(x_sizes.data() + iy + 1, x_sizes.data() + ix,
                                        difference_type(0));

      auto y_to_end = y.with_base_index(
        [&](auto I){
          return x_sizes[I] - (y.template base<I>() - y.template base_begin<I>());
        });

      auto begin_to_x = x.with_base_index(
        [&](auto I){
          return x.template base<I>() - x.template base_begin<I>();
        });

      return y_to_end + in_between + begin_to_x;
    }
    else if(ix < iy)
      return -(y - x);
    else
      return x.with_base_index(
        [&](auto I)->difference_type{
          return x.template base<I>() - y.template base<I>();
        });
  }

  friend constexpr difference_type operator-(const iterator& x, std::default_sentinel_t)
    requires iterator::is_random_access{

    const auto x_sizes = x.base_sizes();

    auto n0 = std::accumulate(x_sizes.begin() + x.base().index() + 1, x_sizes.end(), difference_type(0));
    auto n1 =
      x.with_base_index(
        [&](auto I){
          return static_cast<difference_type>(x_sizes[I] - (x.template base<I>() - x.template base_begin<I>()));
        });
    return -(n0 + n1);
  }

  friend constexpr difference_type operator-(std::default_sentinel_t x, const iterator& y)
    requires iterator::is_random_access{
    return -(y - x);
  }

  friend constexpr rvalue_reference_type iter_move(const iterator &x)
    noexcept(iterator::is_nothrow_iterator_movable){
    return std::visit(
      [](auto const& i) -> rvalue_reference_type{
        return std::ranges::iter_move(i);
      },
      x.base());
  }

  friend constexpr void iter_swap(const iterator& x, const iterator& y)
    noexcept(iterator::is_nothrow_swappable)
    requires(iterator::is_indirectly_swappable){
    std::visit(std::ranges::iter_swap, x.base(), y.base());
  }

};

template<typename V,typename... T>
constexpr auto compute_common_end(const iterator<V, T...> &i,
                                  std::default_sentinel_t){
  return iterator<V,T...>{
    .view   = i.view,
    .bases0 = i.bases0,
    .bases1 = i.bases1,
    .base_iterator = base_iterator_t<T...>{
      std::in_place_index<sizeof...(T) - 1>,
      i.template base_end<sizeof...(T) - 1>()
    }
  };
}



template<std::ranges::range... T,typename V>
constexpr auto begin_fn(V &v){
  using VC = view_cache_t<V,T...>;
  
  iterator<VC,T...> x{
    .view = view_cache<T...>(v),
    .bases0 = [&]{
      if constexpr(traits<T...>::is_homogeneuous)
        return std::apply(lift(((base_caches0))),v.bases);
      else
        return kumi::apply(lift(((base_caches0))),v.bases);
    }(),
    .bases1 = [&]{
      if constexpr(traits<T...>::is_homogeneuous)
        return std::apply(lift(((base_caches1))),v.bases);
      else
        return kumi::apply(lift(((base_caches1))),v.bases);
    }(),
    .base_iterator = base_iterator_t<T...>{
      std::in_place_index<0>,
      x.template base_begin<0>()
    }
  };
  x.template satisfy<0>();
  return x;
}
template<std::ranges::range... T,typename V>
constexpr auto end_fn(V &v){
  using VC = view_cache_t<V,T...>;
  if constexpr (traits<T...>::is_common && !traits<T...>::has_borrowed_base){
    iterator<VC,T...> x{
      .view  = view_cache<T...>(v),
      .bases0 = [&]{
        if constexpr(traits<T...>::is_homogeneuous)
          return std::apply(lift(((base_caches0))),v.bases);
        else
          return kumi::apply(lift(((base_caches0))),v.bases);
      }(),
      .bases1 = [&]{
        if constexpr(traits<T...>::is_homogeneuous)
          return std::apply(lift(((base_caches1))),v.bases);
        else
          return kumi::apply(lift(((base_caches1))),v.bases);
      }(),
      .base_iterator = base_iterator_t<T...>{
        std::in_place_index<sizeof...(T) - 1>,
        x.template base_end<sizeof...(T) - 1>()
      }
    };
    return x;
  }
  else if constexpr(traits<T...>::is_infinite)
    return std::unreachable_sentinel;
  else
    return std::default_sentinel;
}
template<typename... T>
constexpr auto size_fn(auto &b) requires traits<T...>::is_sized{
  using U = size_t<T...>;
  if constexpr(traits<T...>::is_homogeneuous)
    return std::apply(
      [](auto &&... b){
        return (U{} + ... + static_cast<U>(std::ranges::size(fwd(b))));
      },
      b);
  else
    return kumi::apply(
      [](auto &&... b){
        return (U{} + ... + static_cast<U>(std::ranges::size(fwd(b))));
      },
      b);
}



template<typename ...T>
requires concatable<T...>
struct view:std::ranges::view_interface<view<T...>>{

  using traits = cats::traits<T...>;

  using bases_t = std::conditional_t<traits::is_homogeneuous,
    std::array<pick_t<0,T...>,sizeof...(T)>,
    kumi::tuple<T...>>;

  bases_t bases;

  constexpr auto begin() arrow(((begin_fn<T...>)(*this)))
  constexpr auto end()   arrow(((end_fn<T...>)(*this)))
  constexpr auto size()  arrow(((size_fn<T...>)(bases)))

  constexpr auto begin()const arrow(((begin_fn<const T...>)(*this)))
  constexpr auto end()  const arrow(((end_fn<const T...>)(*this)))
  constexpr auto size() const arrow(((size_fn<const T...>)(bases)))

  using am_concat_view = void;
};

namespace concepts{
  template<typename T>
  concept view = requires{ typename std::remove_cvref_t<T>::am_concat_view; };
}


namespace _{

  template<typename T>
  decltype(auto) ensure_bases_tuple(T &&t){
    if constexpr(concepts::view<T>){
      if constexpr(autotype(t)::traits::is_homogeneuous)
        return kumi::apply(lift((kumi::make_tuple)), fwd(t).bases);
      else
        return (fwd(t).bases);
    }
    else
      return kumi::forward_as_tuple(fwd(t));
  }

  template<typename ...T>
  auto make_impl(T &&... t){
    return view<views::all_t<T &&>...>{
      .bases{views::all(fwd(t))...}
    };
  }

}
template<typename ...T>
requires concatable<T...>
auto make(T &&... t){
  return kumi::apply(lift((_::make_impl)),
                     kumi::cat(_::ensure_bases_tuple(fwd(t))...));
}

struct factory {
  constexpr auto operator()(auto &&t) const
    arrow((std::views::all(fwd(t))))
  constexpr auto operator()(auto &&... t) const
    arrow(((make)(fwd(t)...)))
};

} //hysj::views::cats

namespace hysj::inline _HYSJ_VERSION_NAMESPACE::views{

inline constexpr cats::factory cat{};

inline constexpr auto prepend = combine< 0   >(views::cat,views::one);
inline constexpr auto append  = combine<-1   >(views::cat,views::one);
inline constexpr auto bookend = on     < 0,-1>(views::cat,views::one,views::one);

template<typename T>
inline constexpr auto catcast = combine<>(cat,cast<T>);

} //hysj::views

template<typename... T>
inline constexpr bool std::ranges::enable_borrowed_range<
  hysj::_HYSJ_VERSION_NAMESPACE::views::cats::view<T...>> =
    hysj::_HYSJ_VERSION_NAMESPACE::views::cats::traits<T...>::is_borrowed;

template<typename... T>
inline constexpr bool std::ranges::enable_view<
  hysj::_HYSJ_VERSION_NAMESPACE::views::cats::view<T...>> =
    true;

#include <hysj/tools/epilogue.hpp>
