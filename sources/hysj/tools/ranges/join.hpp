#pragma once

#include<concepts>
#include<iterator>
#include<ranges>
#include<type_traits>

#include<hysj/tools/type_traits.hpp>
#include<hysj/tools/types.hpp>
#include<hysj/tools/ranges/utility.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::views::joins{

template<typename T>
concept joinable =
  std::ranges::input_range<T>
  && std::ranges::view<T>
  && std::ranges::input_range<std::ranges::range_reference_t<T>>
  && ((std::is_reference_v<std::ranges::range_reference_t<T>>
       || std::ranges::borrowed_range<std::ranges::range_reference_t<T>>)
      || std::ranges::view<std::ranges::range_value_t<T>>);

template<typename Outer>
struct traits{

  using outer_type = Outer;
  using outer_reference_type = std::ranges::range_reference_t<outer_type>;
  using outer_iterator_type = std::ranges::iterator_t<outer_type>;
  using outer_sentinel_type = std::ranges::sentinel_t<outer_type>;

  using inner_type = std::remove_reference_t<outer_reference_type>;
  using inner_iterator_type = std::ranges::iterator_t<inner_type>;
  using inner_sentinel_type = std::ranges::sentinel_t<inner_type>;

  static constexpr bool outer_is_borrowable =
    std::ranges::enable_borrowed_range<inner_type>;
  static constexpr bool outer_is_referencing =
    std::is_reference_v<outer_reference_type>;
  
  static constexpr bool outer_is_common = std::ranges::common_range<outer_type>;
  static constexpr bool outer_is_forward = std::ranges::forward_range<outer_type>;
  static constexpr bool outer_is_bidirectional =
    std::ranges::bidirectional_range<outer_type>;
  
  static constexpr bool outer_is_iterator_equality_comparable =
    std::equality_comparable<outer_iterator_type>;

  static constexpr bool inner_is_borrowable =
    std::ranges::enable_borrowed_range<inner_type>;
  static constexpr bool inner_is_common = std::ranges::common_range<inner_type>;
  static constexpr bool inner_is_forward = std::ranges::forward_range<inner_type>;
  static constexpr bool inner_is_bidirectional =
    std::ranges::bidirectional_range<inner_type>;
  static constexpr bool inner_is_iterator_equality_comparable =
    std::equality_comparable<inner_iterator_type>;

  
  static constexpr bool is_forward =
    (outer_is_forward
     && inner_is_forward
     && (inner_is_borrowable || outer_is_referencing));
  static constexpr bool is_bidirectional =
    (outer_is_bidirectional
     && inner_is_bidirectional
     && inner_is_common //FIXME: (inner_is_common || inner_is_random_access) - jeh
     && (inner_is_borrowable || outer_is_referencing));

  static constexpr bool is_iterator_equality_comparable =
    (outer_is_iterator_equality_comparable &&
     inner_is_iterator_equality_comparable);
  static constexpr bool is_indirectly_swappable =
    std::indirectly_swappable<outer_iterator_type>;

  static constexpr bool has_arrow =
    (ranges::has_arrow<outer_iterator_type>
     && std::copyable<outer_iterator_type>);

  using iterator_concept = decltype(
    []{
      if constexpr(is_bidirectional)
        return std::bidirectional_iterator_tag{};
      else if constexpr (is_forward)
        return std::forward_iterator_tag{};
      else
        return std::input_iterator_tag{};
    }());

  using value_type = std::ranges::range_value_t<inner_type>;
  
  using difference_type =
    std::common_type_t<std::ranges::range_difference_t<outer_type>,
                       std::ranges::range_difference_t<inner_type>>;

  using outer_cache_type =
    typename decltype(
      []{
        if constexpr(outer_is_borrowable)
          return type_c<outer_sentinel_type>;
        else
          return type_c<outer_type *>;
      }())::type;

  using inner_cache_type =
    typename decltype(
      []{
        if constexpr(inner_is_borrowable)
          return type_c<inner_sentinel_type>;
        else if constexpr(!outer_is_referencing)
          return type_c<std::views::all_t<inner_type>>;
        else
          return type_c<none>;
      }())::type;

};

template<typename Outer>
using traits_t = traits<Outer>;

template<typename Outer>
struct iterator_prologue : traits<Outer>{};

template<typename Outer>
requires ((traits<Outer>::outer_is_referencing
           || traits<Outer>::inner_is_borrowable)
          && traits<Outer>::is_forward)
struct iterator_prologue<Outer> : traits<Outer>{
  using iterator_category = decltype(
    []{
      return ranges::clamp_iterator_category(
        std::input_iterator_tag{},
        ranges::common_iterator_category<
          typename iterator_prologue::outer_type,
          typename iterator_prologue::inner_type>(),
        std::bidirectional_iterator_tag{});
    }());
};

template<typename Outer>
struct iterator : iterator_prologue<Outer>{
 
  typename iterator::outer_cache_type outer_cache;
  typename iterator::outer_iterator_type outer_iterator;
  std::optional<typename iterator::inner_iterator_type> inner_iterator;
  [[no_unique_address]] typename iterator::inner_cache_type inner_cache;
  
  constexpr void satisfy(){

    auto &&outer_end =
      [this]()->decltype(auto){
        if constexpr(iterator::outer_is_borrowable)
          return (outer_cache);
        else
          return std::ranges::end(*outer_cache);
      }();

    auto process_inner =
      [&](auto &&inner) -> decltype(auto){
        if constexpr(iterator::inner_is_borrowable){
          inner_iterator = std::ranges::begin(inner);
          inner_cache = std::ranges::end(inner);
          return inner_cache;
        }
        else if constexpr(iterator::outer_is_referencing){
          inner_iterator = std::ranges::begin(inner);
          return std::ranges::end(inner);
        }
        else{
          inner_cache = std::views::all(std::move(inner));
          inner_iterator = std::ranges::begin(inner_cache);
          return std::ranges::end(inner_cache);
        }
      };
    
    for(; outer_iterator != outer_end; ++outer_iterator){
      auto &&inner_end = process_inner(*outer_iterator);
      if(!(*inner_iterator == inner_end))
        return;
    }

    inner_iterator.reset();
  }

  constexpr decltype(auto) operator*() const{
    return **inner_iterator;
  }

  constexpr auto operator->() const requires iterator::has_arrow{
    return *inner_iterator;
  }

  constexpr auto& operator++(){
    
    auto &&inner_end =
      [&]()->decltype(auto){
        if constexpr(iterator::inner_is_borrowable)
          return (inner_cache);
        else if constexpr(!iterator::outer_is_referencing)
          return std::ranges::end(inner_cache);
        else
          return std::ranges::end(*outer_iterator);
      }();
    
    if(++*inner_iterator == inner_end){
      ++outer_iterator;
      satisfy();
    }
    
    return *this;
  }

  constexpr void operator++(int){
    ++*this;
  }

  constexpr iterator operator++(int)
    requires iterator::is_forward{
    auto that = *this;
    ++*this;
    return that;
  }

  constexpr iterator &operator--()
    requires iterator::is_bidirectional{

    auto &&outer_end =
      [this]()->decltype(auto){
        if constexpr(iterator::outer_is_borrowable)
          return (outer_cache);
        else
          return std::ranges::end(*outer_cache);
      }();

    auto process_inner =
      [&](auto &&inner){
        if constexpr(iterator::inner_is_borrowable)
          inner_cache = *inner_iterator = std::ranges::end(inner);
        else
          *inner_iterator = std::ranges::end(inner);
      };
    
    if(outer_iterator == outer_end)
      process_inner(*--outer_iterator);
    
    while(*inner_iterator == std::ranges::begin(*outer_iterator))
      process_inner(*--outer_iterator);

    --*inner_iterator;
    return *this;
  }

  constexpr iterator operator--(int) requires iterator::is_bidirectional{
    auto that = *this;
    --*this;
    return that;
  }

  friend constexpr bool
  operator==(const iterator &x, const iterator &y)
    requires iterator::is_iterator_equality_comparable{
    return (x.outer_iterator == y.outer_iterator
            && x.inner_iterator == y.inner_iterator);
  }
  friend constexpr bool
  operator==(const iterator &x, const std::default_sentinel_t &)
    requires iterator::outer_is_borrowable{
    return x.outer_iterator == x.outer_cache;
  }

  friend constexpr decltype(auto) iter_move(const iterator &i)
    noexcept(noexcept(std::ranges::iter_move(*i.inner_iterator))){
    return std::ranges::iter_move(*i.inner_iterator);
  }

  friend constexpr void iter_swap(const iterator &x, const iterator &y)
    noexcept(noexcept(std::ranges::iter_swap(*x.inner_iterator,
                                             *y.inner_iterator)))
    requires iterator::is_indirectly_swappable{
    return std::ranges::iter_swap(*x.inner_iterator, *y.inner_iterator);
  }

};

template<typename Output>
struct sentinel : traits<Output>{
  typename sentinel::outer_sentinel_type outer_sentinel{};

  friend
  constexpr bool operator==(const iterator<Output>& x,
                            const sentinel& y){
    return x.outer_iterator == y.outer_sentinel;
  }
};

template<typename Outer>
constexpr auto make_begin(Outer &outer){
  using traits = traits_t<Outer>;
  iterator<Outer> it{
    .outer_cache{
      [&]{
        if constexpr(traits::outer_is_borrowable)
          return std::ranges::end(outer);
        else
          return std::addressof(outer);
      }()
    },
    .outer_iterator = std::ranges::begin(outer),
    .inner_iterator = std::nullopt
  };
  it.satisfy();
  return it;
}

template<typename Outer>
auto make_end(Outer &outer){
  using traits = traits_t<Outer>;
  if constexpr(traits::outer_is_borrowable)
    return std::default_sentinel;
  else if constexpr(traits::outer_is_common)
    return iterator<Outer>{
      .outer_cache = std::addressof(outer),
      .outer_iterator = std::ranges::end(outer),
      .inner_iterator = std::nullopt
    };
  else
    return sentinel<Outer>{
      .outer_sentinel = std::ranges::end(outer)
    };
}

template<typename Outer>
struct view : std::ranges::view_interface<view<Outer>>{
 static constexpr joins::traits<Outer> traits{};

  ranges::box<Outer> outer;
  
  constexpr auto begin(){
    return (make_begin)(*outer);
  }

  constexpr auto begin()const{
    return (make_begin)(*outer);
  }

  constexpr auto end(){
    return (make_end)(*outer);
  }

  constexpr auto end()const{
    return (make_end)(*outer);
  }
  
};

struct factory{
  template<std::ranges::viewable_range Outer>
  requires joinable<std::views::all_t<Outer>>
  constexpr view<std::views::all_t<Outer>> operator()(Outer&& outer)const{
    return {
      .outer = ranges::box<std::views::all_t<Outer>>{fwd(outer)}
    };
  }
};

} //hysj::views::joins
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::views{

inline constexpr joins::factory join{};

} //hysj::views

template<typename O>
inline constexpr bool std::ranges::enable_borrowed_range<
  hysj::_HYSJ_VERSION_NAMESPACE::views::joins::view<O>> =
    std::ranges::enable_borrowed_range<O>;

template<typename O>
inline constexpr bool std::ranges::enable_view<
  hysj::_HYSJ_VERSION_NAMESPACE::views::joins::view<O>> =
    true;

#include<hysj/tools/epilogue.hpp>

