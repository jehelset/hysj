#pragma once
#include<concepts>
#include<ranges>
#include<iterator>
#include<utility>

#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/functional/invoke.hpp>
#include<hysj/tools/functional/utility.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::views::keeps{

template<typename D,typename P>
concept keepable =
  std::ranges::input_range<D>
  && std::indirect_unary_predicate<P,std::ranges::iterator_t<D>>
  && std::ranges::view<D>
  && std::is_object_v<P>;

template<typename D,typename P>
struct traits{

  using domain_iterator_type = std::ranges::iterator_t<D>;
  using domain_sentinel_type = std::ranges::sentinel_t<D>;
  using reference_type = std::ranges::range_reference_t<D>;
  using value_type = std::ranges::range_value_t<D>;
  using difference_type = std::ranges::range_difference_t<D>;
  using invoke_result_type = invoke_result_t<P &,reference_type>;
  using range_rvalue_reference_type = std::ranges::range_rvalue_reference_t<D>;
  static constexpr bool is_common = std::ranges::common_range<D>;
  static constexpr bool is_forward = std::ranges::forward_range<D>;
  static constexpr bool is_bidirectional = std::ranges::bidirectional_range<D>;

  static constexpr bool is_iterator_three_way_comparable =
    std::three_way_comparable<domain_iterator_type>;
  static constexpr bool is_iterator_equality_comparable =
    std::equality_comparable<domain_iterator_type>;
  static constexpr bool is_iterator_indirectly_swappable =
    std::indirectly_swappable<domain_iterator_type>;
  
  using iterator_concept =
    decltype(
      []{
        if constexpr(is_bidirectional)
          return std::bidirectional_iterator_tag{};
        else if constexpr(is_forward)
          return std::forward_iterator_tag{};
        else
          return std::input_iterator_tag{};
      }());

  static constexpr bool has_arrow =
    ranges::has_arrow<domain_iterator_type>
    && std::copyable<domain_iterator_type>;
};

template<typename D,typename P>
struct iterator_prologue : traits<D,P>{};

template<typename D,typename P>
requires traits<D,P>::is_forward
struct iterator_prologue<D,P> : traits<D,P>{
  using iterator_category =
    decltype(
      []{
        if constexpr(
          std::is_lvalue_reference_v<
            typename iterator_prologue::reference_type>)
          return ranges::min_iterator_category(
            std::random_access_iterator_tag{},
            typename std::iterator_traits<
              typename iterator_prologue::domain_iterator_type
                >::iterator_category{});
        else
          return std::input_iterator_tag{};
      }());
};

using sentinel = std::default_sentinel_t;

template<typename D,typename P>
struct iterator:iterator_prologue<D,P>{
        
  typename iterator::domain_iterator_type domain_iterator;
  typename iterator::domain_sentinel_type domain_sentinel;
      
  [[no_unique_address]] ranges::box<P> predicate;

  constexpr typename iterator::reference_type operator*()const{
    return *domain_iterator;
  }

  constexpr auto operator->()const requires iterator::has_arrow{
    return domain_iterator;
  }

  constexpr iterator &operator++(){
    domain_iterator =
      std::ranges::find_if(std::move(++domain_iterator),
                           domain_sentinel,
                           std::ref(*predicate));
    return *this;
  }

  constexpr void operator++(int){
    ++*this;
  }

  constexpr iterator operator++(int) requires iterator::is_forward{
    auto that = *this;
    ++*this;
    return that;
  }

  constexpr iterator &operator--() requires iterator::is_bidirectional{
    do
      --domain_iterator;
    while(!invoke(*predicate,*domain_iterator));
    return *this;
  }

  constexpr iterator operator--(int) requires iterator::is_bidirectional{
    auto that = *this;
    --*this;
    return that;
  }

  friend
  constexpr bool operator==(const iterator &x, const iterator &y)
    requires iterator::is_iterator_equality_comparable{
    return x.domain_iterator == y.domain_iterator;
  }

  friend
  constexpr typename iterator::range_rvalue_reference_type
  iter_move(const iterator &i)
    noexcept(noexcept(std::ranges::iter_move(i.domain_iterator))){
    return std::ranges::iter_move(i.domain_iterator);
  }

  friend
  constexpr void iter_swap(const iterator &x, const iterator &y)
    noexcept(noexcept(std::ranges::iter_swap(x.domain_iterator,
                                             y.domain_iterator)))
    requires iterator::is_iterator_indirectly_swappable{
    std::ranges::iter_swap(x.domain_iterator, y.domain_iterator);
  }

  friend
  constexpr bool operator==(const iterator &x, const sentinel &)
    noexcept(noexcept(x.domain_iterator == x.domain_sentinel)){
    return x.domain_iterator == x.domain_sentinel;
  }
  
};

template<typename D,typename P>
constexpr iterator<D,P> make_begin(D &domain,P predicate){
  auto domain_iterator = std::ranges::find_if(domain,std::ref(predicate));
  return iterator<D,P>{
    .domain_iterator = std::move(domain_iterator),
    .domain_sentinel = std::ranges::end(domain),
    .predicate = ranges::box<P>{std::move(predicate)}
  };
}

template<typename D,typename P>
struct view:std::ranges::view_interface<view<D, P>>{
  D domain;
  [[no_unique_address]] ranges::box<P> predicate;
  
  constexpr auto begin()const{
    return (make_begin)(domain,*predicate);
  }
  constexpr auto end()const{
    return sentinel{};
  }
};

struct factory{

  template<std::ranges::viewable_range D, typename P>
  requires keepable<std::views::all_t<D>,std::decay_t<P>>
  constexpr view<std::views::all_t<D>,std::decay_t<P>>
    operator()(D&& d, P p)const{
    return {
      .domain = std::views::all(fwd(d)),
      .predicate = ranges::box<std::decay_t<P>>{std::move(p)}
    };
  }

  constexpr auto operator()(auto &&d) const 
    arrow((operator()(fwd(d), hysj::cast<bool>)));

};

} //hysj::views::keeps
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::views{

inline constexpr keeps::factory keep{};

} //hysj::views
template <typename V,typename F>
inline constexpr bool std::ranges::enable_borrowed_range<
  hysj::_HYSJ_VERSION_NAMESPACE::views::keeps::view<V,F>> =
    std::ranges::enable_borrowed_range<V>;

template <typename V,typename F>
inline constexpr bool std::ranges::enable_view<
  hysj::_HYSJ_VERSION_NAMESPACE::views::keeps::view<V,F>> =
    true;

#include <hysj/tools/epilogue.hpp>
