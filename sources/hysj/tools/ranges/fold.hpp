#pragma once

#include<algorithm>
#include<functional>
#include<ranges>

#include<hysj/tools/functional/invoke.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::ranges{

inline constexpr auto sum =
  []<std::ranges::input_range R>(R &&r)
    arrow((std::ranges::fold_left(r,std::ranges::range_value_t<R>{0},std::plus{})))
  ;
inline constexpr auto prod  =
  []<std::ranges::input_range R>(R &&r)
    arrow((std::ranges::fold_left(r,std::ranges::range_value_t<R>{1},std::multiplies{})))
  ;

} //hysj::ranges
#include<hysj/tools/epilogue.hpp>
