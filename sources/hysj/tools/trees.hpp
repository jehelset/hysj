#pragma once

#include<array>
#include<algorithm>
#include<cstdlib>
#include<optional>
#include<ranges>
#include<type_traits>
#include<utility>
#include<vector>

#include<kumi/tuple.hpp>

#include<hysj/tools/ranges/map.hpp>
#include<hysj/tools/functional/pick.hpp>
#include<hysj/tools/functional/any_of.hpp>
#include<hysj/tools/functional/invoke.hpp>
#include<hysj/tools/functional/apply.hpp>
#include<hysj/tools/functional/ignore.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::trees{

namespace concepts{

  template <typename T>
  concept input = std::ranges::forward_range<T> && std::ranges::borrowed_range<T>;

}

enum class rule : std::size_t{ one, tup, var };

constexpr std::size_t none = -1;

template <std::size_t len_>
struct grammar{
  std::array<std::size_t, len_> buf{};

  static constexpr auto len(){ return len_; }

  constexpr std::size_t rule_len(std::size_t p) const noexcept{
    const rule r{buf[p++]};
    switch(r){
      case rule::tup:{
        auto n = buf[p++];
        std::size_t s = 2;
        for(std::size_t i = 0; i != n; ++i) s += rule_len(p);
        return s;
      }
      case rule::var:{
        return 1 + rule_len(p);
      }
      default:
        return 1;
    }
  }
  constexpr std::size_t subrule_count(std::size_t p) const noexcept{
    const rule r{buf[p]};
    switch(r){
      case rule::tup:
        return buf[p + 1];
      case rule::var:
        return 1;
      default:
        return 0;
    }
  }
  constexpr auto subrule(std::size_t p, std::size_t i) const noexcept{
    const rule r{buf[p]};
    switch(r){
      case rule::tup:{
        [[maybe_unused]] auto n = buf[p + 1];
        auto u = p + 2;
        while(i != 0){
          --i;
          u += rule_len(u);
        }
        return u;
      }
      case rule::var:{
        HYSJ_ASSERT(i == 0);
        return p + 1;
      }
      default:
        return none;
    }
  }
  constexpr auto subrule(std::size_t p, std::size_t i,auto ... j)const noexcept requires (sizeof...(j) > 0) {
    return subrule(subrule(p, i), j...);
  }
  constexpr std::size_t subrule_count(std::size_t p, auto... i)const noexcept requires(sizeof...(i) > 0) {
    return subrule_count(subrule(p, i...));
  }

  constexpr auto subrule_len(std::size_t p, auto... i) const noexcept{
    return rule_len(subrule(p, i...));
  }

  constexpr std::size_t rule_stride(std::size_t p) const noexcept{
    const rule r{buf[p]};
    switch(r){
      case rule::one:
        return 1;
      case rule::tup:{
        auto n = subrule_count(p);
        std::size_t s = 0;
        for(std::size_t i = 0; i != n; ++i)
          s += rule_stride(subrule(p, i));
        return s;
      }
      case rule::var:{
        return rule_stride(subrule(p, 0));
      }
      default:
        return 0;
    }
  }
  constexpr std::size_t rule_size(std::size_t p) const noexcept{
    const rule r{buf[p]};
    switch(r){
      case rule::var:
        return none;
      case rule::tup:{
        auto n = subrule_count(p);
        if(n == 0)
          return 0;
        auto s = rule_size(subrule(p, n - 1));
        if(s == none) return s;
        for(std::size_t i = 0; i != n - 1; ++i)
          s += rule_size(subrule(p, i));
        return s;
      }
      default:
        return rule_stride(p); //FIXME: doesn't really recurse corrrectly... - jeh
    }
  }
  constexpr std::size_t subrule_size(std::size_t p,auto... i)const noexcept{
    return rule_size(subrule(p, i...));
  }
  constexpr std::size_t size()const noexcept{
    return rule_size(0);
  }

  constexpr std::size_t rule_max_size(std::size_t p) const noexcept{
    const rule r{buf[p]};
    switch(r){
    case rule::var:
      return none;
    case rule::tup:{
      auto n = subrule_count(p);
      if(n == 0)
        return 0;
      auto s = rule_max_size(subrule(p, n - 1));
      if(s == none) return s;
      for(std::size_t i = 0; i != n - 1; ++i)
        s += rule_max_size(subrule(p, i));
      return s;
    }
    default:
      return rule_size(p);
    }
  }
  constexpr auto max_size() const noexcept{
    return rule_max_size(0);
  }

  constexpr std::size_t rule_depth(std::size_t p)const noexcept{
    const rule r{buf[p]};
    switch(r){
      case rule::var:
        return 1 + rule_depth(subrule(p, 0));
      case rule::tup:{
        auto n = subrule_count(p);
        auto s = 0;
        for(std::size_t i = 0; i != n - 1; ++i)
          s = std::max(s, rule_depth(subrule(p, i)));
        return 1 + s;
      }
      default:
        return 1;
    }
  }
  constexpr std::size_t depth()const noexcept{
    return rule_depth(0);
  }

  constexpr auto rule_tag(std::size_t p)const noexcept{
    return rule{buf[p]};
  }
  constexpr auto subrule_tag(std::size_t p,auto... i)const noexcept{
    return rule_tag(subrule(p, i...));
  }
  constexpr auto tag()const noexcept{
    return rule_tag(0);
  }
};

inline constexpr grammar<1> one{.buf{etoi(rule::one)}};
constexpr auto tup(auto... e){
  grammar<2 + (e.len() + ... + 0)> g{
      .buf{etoi(rule::tup), sizeof...(e)},
  };
  [&]<std::size_t... I>(std::index_sequence<I...>){
    HYSJ_ASSERT((((I + 1 == sizeof...(e)) || e.size() != none) && ...));
  }(std::make_index_sequence<sizeof...(e)>{});
  [[maybe_unused]] auto it = g.buf.data() + 2;
  ((it = std::ranges::copy(e.buf, it).out), ...); (void) it;
  return g;
}
constexpr auto var(auto e){
  grammar<1 + e.len()> g{.buf{etoi(rule::var)}};
  std::ranges::copy(e.buf, g.buf.data() + 1);
  return g;
}

template <auto grammar, std::size_t pos = 0>
constexpr auto parse_rule(concepts::input auto &&input){

  using I = decltype(input);
  using iterator = std::ranges::iterator_t<I>;
  using sentinel = std::ranges::sentinel_t<I>;

  using enum rule;
  constexpr rule tag{grammar.buf[pos]};

  auto iter = std::ranges::begin(input);
  auto sent = std::ranges::end(input);
  auto result = [&]{

    if constexpr(tag == one){
      if(iter == sent)
        return kumi::tuple{std::optional<iterator>{}, iter};
      return kumi::tuple{std::optional<iterator>{iter},
                       std::ranges::next(iter)};
    }
    if constexpr(tag == tup){
      constexpr auto count = grammar.subrule_count(pos);
      if constexpr(count == 0)
        return
          kumi::tuple{std::optional<kumi::tuple<>>{kumi::tuple{}}, iter};
      else{
        constexpr auto indices = std::make_index_sequence<count>{};
        constexpr bool varsize = grammar.subrule_size(pos, count - 1) == none;

        using subsentinel = std::conditional_t<varsize, sentinel, iterator>;

        using tree =
          typename decltype(
            []<std::size_t... index>(std::index_sequence<index...>){
              return std::type_identity<
                decltype(
                  kumi::make_tuple(
                    kumi::get<0>(
                      parse_rule<grammar, grammar.subrule(pos, index)>(
                        std::ranges::subrange(
                          std::declval<iterator>(),
                          std::declval<std::conditional_t<
                          index + 1 == count, subsentinel, iterator>>()))).value()...))>{};
            }(indices))::type;

        constexpr auto subrule_sizes =
          []<std::size_t... index>(
            std::index_sequence<index...>){
          return std::array{grammar.subrule_size(pos, index)...};
        }(indices);

        std::array<iterator, count> subiters{};
        subiters[0] = iter;
        for(std::size_t i = 1; i != count; ++i){
          subiters[i] =
            std::ranges::next(subiters[i - 1], subrule_sizes[i - 1], sent);
          if(subiters[i] == sent && !(varsize && i + 1 == count))
            return kumi::tuple{std::optional<tree>{}, subiters[i]};
        }

        std::array<iterator, count - 1> subsents{};
        for(std::size_t i = 0; i != count - 1; ++i)
          subsents[i] = subiters[i + 1];

        auto last_subsent = [&]{
          if constexpr(varsize)
            return sent;
          else
            return std::ranges::next(subiters[count - 1],
                                     subrule_sizes[count - 1], sent);
        }();

        auto subsent =
          [&]<std::size_t index>(std::integral_constant<std::size_t, index>){
          if constexpr(index + 1 == count)
            return last_subsent;
          else
            return subsents[index];
        };

        return [&]<std::size_t... index>(
          std::index_sequence<index...>){
          return kumi::tuple{
            std::optional{
              kumi::make_tuple(
                kumi::get<0>(parse_rule<grammar, grammar.subrule(pos, index)>(
                               std::ranges::subrange(
                                 subiters[index],
                                 subsent(std::integral_constant<std::size_t, index>{})))
                  ).value()...)},
            last_subsent};
        }(indices);
      }
    }

    if constexpr(tag == var)
      return kumi::make_tuple(
          std::optional{views::map(
              std::views::chunk(fwd(input), grammar.rule_stride(pos)),
              [](auto &&chunk){
                return kumi::get<0>(parse_rule<grammar, grammar.subrule(pos, 0)>(fwd(chunk))).value();
              })},
          sent);

  }();

  if constexpr(pos == 0)
    if(kumi::get<1>(result) != sent)
      kumi::get<0>(result).reset();

  return result;
}

template <auto grammar>
constexpr auto parse(concepts::input auto &&input){
  return kumi::get<0>(parse_rule<grammar>(std::views::all(fwd(input)))).value();
}

template <auto rules, std::size_t pos, std::size_t... step>
constexpr auto with_rule(auto &&tree, auto &&map){
  using enum rule;
  constexpr constant_t<rule{rules.buf[pos]}> tag{};
  constexpr std::index_sequence<step...> path{};
  if constexpr(tag() == one)
    return invoke(map, path, tag, fwd(tree));
  if constexpr(tag() == tup)
    return [&]<std::size_t... index>(std::index_sequence<index...>){
      return invoke(
        map, path, tag, 
        with_rule<rules, rules.subrule(pos, index), step..., index>(
          kumi::get<index>(fwd(tree)), map)...
        );
    }(std::make_index_sequence<rules.subrule_count(pos)>{});
  if constexpr(tag() == var)
    return invoke(
      map, path, tag,
      views::map(tree, [map = map](auto &&subtree){
        return with_rule<rules, rules.subrule(pos, 0), step..., 0>(fwd(subtree), map);
      }));
}

template <auto rules>
constexpr auto with(auto &&tree, auto &&map){
  return with_rule<rules, 0, 0>(fwd(tree), fwd(map));
}

template <auto rules>
constexpr auto map(auto &&tree, auto &&map){
  return with_rule<rules, 0, 0>(
    fwd(tree),
    [map = fwd(map)](auto path, auto tag, auto &&... subtree){

      using enum rule;
      if constexpr(tag == one)
        return invoke(map, path, pick<0>(fwd(subtree)...));
      if constexpr(tag == tup)
        return kumi::make_tuple(fwd(subtree)...);
      if constexpr(tag == var)
        return pick<0>(fwd(subtree)...);

    });
}

template <auto rules>
constexpr auto values(auto &&tree){
  return map<rules>(
    fwd(tree),
    []<typename I>(ignore_t, I &&i) -> std::iter_value_t<std::remove_cvref_t<I>>{
      return *fwd(i);
    });
}

}
#include<hysj/tools/epilogue.hpp>
