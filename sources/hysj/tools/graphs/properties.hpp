#pragma once
#include<concepts>
#include<hysj/tools/graphs/elements.hpp>
#include<hysj/tools/priority_tag.hpp>
#include<hysj/tools/tag_invoke.hpp>
#include<hysj/tools/type_traits.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::graphs{

namespace hooks{
  
  inline constexpr
  struct put_fn{

    friend constexpr auto
    ordered_tag_invoke(put_fn,priority_tag<2>,auto &p,auto i,auto &&v)
      arrow((fwd(p).put(i,fwd(v))))
    
    friend constexpr auto
    ordered_tag_invoke(put_fn,priority_tag<1>,auto &p,auto i,auto &&v)
      arrow((p.at(i) = fwd(v)))
    
    friend constexpr auto
    ordered_tag_invoke(put_fn,priority_tag<0>,auto &p,auto i,auto &&v)
      arrow((p[i] = fwd(v)))

    friend constexpr auto
    tag_invoke(put_fn fn,auto &p,auto i,auto &&v)
      arrow((ordered_tag_invoke(fn,priority_tag<2>{},p,i,fwd(v))))

    constexpr auto
    operator()(auto &p,auto i,auto &&v)const
      arrow((tag_invoke(*this,unwrap(p),i,fwd(v))))
    
  }
    put{};
  
  inline constexpr
  struct get_fn{

    friend constexpr auto
    ordered_tag_invoke(get_fn,priority_tag<2>,auto &&p,auto i)
      arrow((fwd(p).get(i)))

    friend constexpr auto
    ordered_tag_invoke(get_fn,priority_tag<1>,auto &&p,auto i)
      arrow((fwd(p).at(i)))

    friend constexpr auto
    ordered_tag_invoke(get_fn,priority_tag<0>,auto &&p,auto i)
      arrow((fwd(p)[i]))

    friend constexpr auto
    tag_invoke(get_fn fn,auto &&p,auto i)
      arrow((ordered_tag_invoke(fn,priority_tag<2>{},fwd(p),i)))

    constexpr auto
    operator()(auto &&p,auto i)const
      arrow((tag_invoke(*this,unwrap(fwd(p)),i)))
    
  }
    get{};
  
} //hooks

using hooks::put;
using hooks::get;

template<typename P,typename I>
using get_t = decltype(graphs::get(std::declval<const P &>(),std::declval<I>()));

namespace hooks{

  inline constexpr
  struct fill_fn{

    friend constexpr auto
    tag_invoke(fill_fn,auto &p,auto && v)
      arrow((std::fill(std::ranges::begin(p),std::ranges::end(p),fwd(v))))

    constexpr auto
    operator()(auto &p,auto &&v)const
      arrow((tag_invoke(*this,unwrap(p),fwd(v))))
    
  }
    fill{};
  
} //hooks

using hooks::fill;

namespace hooks{
  
  inline constexpr
  struct resize_fn{

    friend constexpr auto
    tag_invoke(resize_fn,auto &p,natural n,auto &&v)
      arrow((p.resize(n,fwd(v))))

    constexpr auto
    operator()(auto &p,natural n,auto &&v)const
      arrow((tag_invoke(*this,unwrap(p),n,fwd(v))))
    
  }
    resize{};
}

using hooks::resize;

namespace concepts{

  template<typename P,typename I>
  concept const_property =
    (requires(const P &p,I i){
      graphs::get(p,i);
    });

  template<typename P,typename I>
  concept property =
    const_property<P,I>
    &&(requires(P &p,I i,get_t<P,I> v){
        graphs::put(p,i,v); //NOTE: not exactly the same - jeh
      });

  template<typename P,typename G,element_tag T>
  concept const_element_property = const_property<P,element_id_t<G,T>>;

  template<typename P,typename G,element_tag T>
  concept element_property = property<P,element_id_t<G,T>>;

  template<typename P,typename G,element_tag T,typename V>
  concept const_element_property_of =
    const_element_property<P,G,T>
    && std::convertible_to<get_t<P,element_id_t<G,T>>,V> ;

  template<typename P,typename G,element_tag T,typename V>
  concept element_property_of =
    element_property<P,G,T>
    && std::convertible_to<get_t<P,element_id_t<G,T>>,V> ;

  #define HYSJ_LAMBDA(T,...)                                            \
    template<typename P,typename V,typename G>                          \
    concept const_##T##_property_of = const_element_property_of<P,G,T##_tag,V>;
  HYSJ_MAP_LAMBDA(HYSJ_GRAPH_ELEMENTS)
  #undef HYSJ_LAMBDA

  #define HYSJ_LAMBDA(T,...)                          \
    template<typename P,typename V,typename G>                    \
    concept T##_property_of = element_property_of<P,G,T##_tag,V>;
  HYSJ_MAP_LAMBDA(HYSJ_GRAPH_ELEMENTS)
  #undef HYSJ_LAMBDA

  template<typename P,typename G,element_tag T>
  concept const_integral_element_property =
    const_element_property<P,G,T>
    && std::integral<std::remove_cvref_t<get_t<P,element_id_t<G,T>>>>
    ;

  template<typename P,typename G,element_tag T>
  concept integral_element_property =
    element_property<P,G,T>
    && std::integral<std::remove_cvref_t<get_t<P,element_id_t<G,T>>>>
    ;

  #define HYSJ_LAMBDA(T,...)                                            \
    template<typename P,typename G>                                     \
    concept const_integral_##T##_property = const_integral_element_property<P,G,T##_tag>;
  HYSJ_MAP_LAMBDA(HYSJ_GRAPH_ELEMENTS)
  #undef HYSJ_LAMBDA

  #define HYSJ_LAMBDA(T,...)                                            \
    template<typename P,typename G>                                     \
    concept integral_##T##_property = integral_element_property<P,G,T##_tag>;
  HYSJ_MAP_LAMBDA(HYSJ_GRAPH_ELEMENTS)
  #undef HYSJ_LAMBDA

  template<typename P,typename G,element_tag T>
  concept element_order =
    const_element_property<P,G,T>
    && std::totally_ordered<std::remove_cvref_t<get_t<P,element_id_t<G,T>>>>
    ;

  #define HYSJ_LAMBDA(T,...)                        \
    template<typename P,typename G>                 \
    concept T##_order = element_order<P,G,T##_tag>;
  HYSJ_MAP_LAMBDA(HYSJ_GRAPH_ELEMENTS)
  #undef HYSJ_LAMBDA

} //concepts

template<auto T>
struct static_property{

  template<typename I,typename U>
  static constexpr void put(const I &,const U &)noexcept{
  
  }
  template<typename I>
  static constexpr decltype(auto) get(const I &)noexcept{
    return T;
  }

};

template<typename T>
struct constant_property{
  T value;

  template<typename I,typename U>
  constexpr void put(const I &,const U &)noexcept{
  
  }
  template<typename I>
  constexpr decltype(auto) get(const I &)noexcept{
    return (value);
  }
};

template<element_tag T,typename value>
auto make_vector_property(constant_t<T> e,
                          const graphs::concepts::elements<T> auto &g,
                          value v = value{}){
  return std::vector(graphs::element_count(e,g),std::move(v));
}

#define HYSJ_LAMBDA(T,...)\
  auto make_##T##_vector_property(auto &&... f){\
    return make_vector_property(T##_tag,fwd(f)...);\
  }
HYSJ_MAP_LAMBDA(HYSJ_GRAPH_ELEMENTS)
#undef HYSJ_LAMBDA

#define HYSJ_LAMBDA(T,t,...)\
  auto t##prop(auto &&... f){\
    return make_vector_property(T##_tag,fwd(f)...);\
  }
HYSJ_MAP_LAMBDA(HYSJ_GRAPH_ELEMENTS)
#undef HYSJ_LAMBDA

} //hysj::graphs
#include<hysj/tools/epilogue.hpp>
