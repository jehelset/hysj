#pragma once
#include<array>
#include<ranges>
#include<type_traits>

#include<hysj/tools/graphs/elements.hpp>
#include<hysj/tools/graphs/directions.hpp>
#include<hysj/tools/graphs/elements.hpp>
#include<hysj/tools/graphs/incidence.hpp>
#include<hysj/tools/graphs/degree.hpp>
#include<hysj/tools/ranges/map.hpp>
#include<hysj/tools/type_traits.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::graphs{

//FIXME: constrain this, need sized & random-access - jeh
template<typename G>
struct indirected_graph{
  G base;

  //FIXME:
  //  hard error without this, but should be a constraint failure
  //  - jeh
  template<element_tag T>
  using element_id = graphs::element_id_t<G,T>;
    
  friend HYSJ_MIRROR_STRUCT(indirected_graph,(base));

  using am_indirected_graph = void;

  template<element_tag T>
  auto element_ids(constant_t<T> t)const{
    return graphs::element_ids(t,base);
  }
  
  auto incident(vertex_tag_type t,auto d,auto i)const{
    return views::map(views::indices(graphs::degree(d,base,i)),
                             [&,d,i](auto j){ return graphs::incident(t,d,base,i)[j]; });
  }
  auto incident(edge_tag_type t,auto d,auto i)const{
    return graphs::incident(t,d,base,i); 
  }
};

inline constexpr auto indirect =
  [](auto &&G){
    return indirected_graph{fwd(G)};
  };

} //hysj::graphs
#include<hysj/tools/epilogue.hpp>
