#pragma once
#include<functional>

#include<hysj/tools/graphs/adjacency.hpp>
#include<hysj/tools/tag_invoke.hpp>
#include<hysj/tools/type_traits.hpp>
#include<hysj/tools/types.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::graphs::partitions{

#define HYSJ_GRAPHS_PARTITIONS_EVENTS \
  (start)                             \
  (refine)                            \
  (stop)

enum class event_tag : unsigned {
  HYSJ_SPLAT(0,HYSJ_GRAPHS_PARTITIONS_EVENTS)
};
HYSJ_MIRROR_ENUM(event_tag,HYSJ_GRAPHS_PARTITIONS_EVENTS);

#define HYSJ_LAMBDA(id)                                       \
  inline constexpr auto id##_tag = constant_c<event_tag::id>;
HYSJ_MAP_LAMBDA(HYSJ_GRAPHS_PARTITIONS_EVENTS);
#undef HYSJ_LAMBDA

#define HYSJ_LAMBDA(id)                            \
  using id##_tag_type = constant_t<event_tag::id>;
HYSJ_MAP_LAMBDA(HYSJ_GRAPHS_PARTITIONS_EVENTS);
#undef HYSJ_LAMBDA

namespace hooks{

  inline constexpr struct enumerate_fn{

    friend constexpr auto
    tag_invoke(enumerate_fn,const auto &p)
      arrow((p.enumerate()))
      
    constexpr auto operator()(auto &&p) const
      arrow((tag_invoke(*this,unwrap(p)))
            (-> std::ranges::forward_range))

  }
    enumerate{};

} //hooks

using hooks::enumerate;

template<typename P>
using enumerate_t =
  decltype(enumerate(std::declval<P>()));

inline constexpr auto count =
  [](const auto &P){
    return std::ranges::distance(partitions::enumerate(P));
  };

template<typename P>
using partition_t =
  std::ranges::range_value_t<enumerate_t<P>>;

namespace concepts{
  template<typename T,typename P>
  concept partition = std::convertible_to<partition_t<P>,T>;
}

namespace hooks{

  inline constexpr struct members_fn{

    friend constexpr auto
    tag_invoke(members_fn,auto &p,auto i)
      arrow((p.members(i)))
      
    constexpr auto operator()(auto &&p,auto i) const
      arrow((tag_invoke(*this,unwrap(p),i))
            (-> std::ranges::forward_range))

  }
    members{};

} //hooks

using hooks::members;

template<typename P>
using members_t =
  std::remove_reference_t<
    decltype(members(
               std::declval<P>(),
               std::declval<partition_t<P>>()))>;

template<typename P>
using member_t = std::ranges::range_value_t<members_t<P>>;

namespace hooks{

  inline constexpr struct refine_fn{

    friend constexpr auto
    tag_invoke(refine_fn,auto &p,auto i,auto j)
      arrow((p.refine(i,j)))

    constexpr auto operator()(auto &&p,auto i,auto j) const
      arrow((tag_invoke(*this,unwrap(p),i,j))
            (-> concepts::partition<decltype(p)>))

  }
    refine{};

} //hooks

using hooks::refine;

namespace concepts{

  template<typename P>
  concept partitioning = requires(P p){
    partitions::enumerate(p);
    requires requires(partition_t<P> i){
      partitions::members(p,i);
    };
    requires requires(partition_t<P> i,std::ranges::iterator_t<members_t<P>> j){
      partitions::refine(p,i,j);
    };
  };
  
}

struct partitioning{
  std::vector<std::vector<vertex_id>> partitions;

  using partition_iterator = std::vector<vertex_id>::const_iterator;
  
  friend HYSJ_MIRROR_STRUCT(partitioning,(partitions));
  
  auto enumerate()const{
    return views::indices((natural)partitions.size());
  }
  auto &members(natural i){
    return partitions[i];
  }
  auto &members(natural i)const{
    return partitions[i];
  }
  auto refine(natural i,partition_iterator j){
    partitions.push_back(
      [&]{
        auto &p_i = partitions[i];
        std::vector<vertex_id> p{j,p_i.cend()};
        p_i.erase(j,p_i.end());
        return p;
      }());
    return static_cast<natural>(partitions.size() - 1);
  }
};

auto construct(auto vertices_,auto compare){
  auto vertices = ranges::vec(vertices_);
  std::ranges::sort(vertices,compare);
  partitioning p{};
  for(auto it = std::ranges::begin(vertices);
      it != std::ranges::end(vertices);){
    auto it2 = std::ranges::adjacent_find(it,std::ranges::end(vertices),compare);
    if(it2 != std::ranges::end(vertices))
      ++it2;
    p.partitions.emplace_back(it,it2);
    it = it2;
  }
  return p;
}

} //hysj::graphs::partitions
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::graphs{

namespace prt = partitions;

} //hysj::graphs
#include <hysj/tools/epilogue.hpp>
