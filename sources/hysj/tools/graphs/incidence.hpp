#pragma once
#include<tuple>
#include<utility>

#include<kumi/tuple.hpp>

#include<hysj/tools/functional/bind.hpp>
#include<hysj/tools/graphs/directions.hpp>
#include<hysj/tools/graphs/elements.hpp>
#include<hysj/tools/priority_tag.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/tag_invoke.hpp>
#include<hysj/tools/types.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::graphs{

#define HYSJ_GRAPHS_INCIDENCE_MODES                           \
  HYSJ_PRODUCT((HYSJ_SELECT((0)(1),HYSJ_GRAPH_ELEMENTS))      \
               (HYSJ_GRAPHS_DIRECTIONAL_TAGS))

namespace hooks::incident_hook{
  
  inline constexpr
  struct incident_fn{

    template<element_tag T,auto D>
    friend constexpr auto
    tag_invoke(incident_fn,constant_t<T> t,constant_t<D> d,const auto &g,auto i)
      arrow((g.incident(t,d,i)))

    template<element_tag T>
    constexpr auto
    operator()(constant_t<T> t,auto d,const auto &g,auto i)const
      arrow((tag_invoke(*this,t,d,unwrap(g),i))
            (-> concepts::element_id_range<decltype(g),other(T)>))
      
  }
    incident{};
  
} //hooks::incident_hook

using hooks::incident_hook::incident;

#define HYSJ_LAMBDA(X,x,...)                        \
  inline constexpr auto x##incident =               \
    bind<>(graphs::incident,X##_tag);
HYSJ_MAP_LAMBDA(HYSJ_GRAPH_ELEMENTS)
#undef HYSJ_LAMBDA

#define HYSJ_LAMBDA(X,x,D,d)                           \
  inline constexpr auto x##d##incident =               \
    bind<>(graphs::incident,X##_tag,D##_tag);
HYSJ_MAP_LAMBDA(HYSJ_GRAPHS_INCIDENCE_MODES)
#undef HYSJ_LAMBDA

//FIXME:
//  these should have element tag?
//  - jeh

namespace hooks::port_hook{
  
  inline constexpr
  struct port_fn{

    friend constexpr auto
    tag_invoke(port_fn,auto d,const auto &g,auto i)
      arrow((ranges::first(graphs::eincident(d,g,i))))
    
    constexpr auto
    operator()(concepts::directional auto d,const auto &g,auto i)const
      arrow((tag_invoke(*this,d,unwrap(g),i)))

  }
    port{};
  
} //hooks::port_hook

using hooks::port_hook::port;

inline constexpr auto head = bind<>(port,out_tag);
inline constexpr auto tail = bind<>(port,in_tag);

inline constexpr struct{
  
  template<typename G,typename I>
  constexpr auto operator()(const G &g,I i)const{
    return with_enumerators<direction_tag>(
      [g = std::cref(g),i = i](auto... d){
        return kumi::make_tuple(graphs::port(d,g,i)...);
      });
  }
}
  ports{};

template<element_tag T,typename D,typename G,typename I = element_id_t<G,T>>
using incident_t =
  decltype(graphs::incident(constant_c<T>,
                            std::declval<D>(),
                            std::declval<const G &>(),
                            std::declval<I>()));

#define HYSJ_LAMBDA(X,x,...)                               \
  template<typename D,typename G,typename I = X##_id_t<G>> \
  using x##incident_t = incident_t<X##_tag,D,G,I>; 
HYSJ_MAP_LAMBDA(HYSJ_GRAPH_ELEMENTS)
#undef HYSJ_LAMBDA

#define HYSJ_LAMBDA(X,x,D,d)                                    \
  template<typename G,typename I = X##_id_t<G>>                   \
  using x##d##incident_t = incident_t<X##_tag,D##_tag_type,G,I>; 
HYSJ_MAP_LAMBDA(HYSJ_GRAPHS_INCIDENCE_MODES)
#undef HYSJ_LAMBDA

namespace _{
  template<element_tag T,typename D,typename G,typename I>
  struct incidence_id_t_impl{
    using type = std::ranges::range_value_t<incident_t<T,D,G,I>>;
  };
  template<direction_tag D,typename G,typename I>
  struct incidence_id_t_impl<edge_tag,constant_t<D>,G,I>{
    using type = incident_t<edge_tag,constant_t<D>,G,I>;
  };
}

template<element_tag T,typename D,typename G,typename I = element_id_t<G,T>>
using incident_id_t = std::ranges::range_value_t<incident_t<T,D,G,I>>;

#define HYSJ_LAMBDA(X,x,...)                               \
  template<typename D,typename G,typename I = X##_id_t<G>> \
  using x##incident_id_t = incident_id_t<X##_tag,D,G,I>; 
HYSJ_MAP_LAMBDA(HYSJ_GRAPH_ELEMENTS)
#undef HYSJ_LAMBDA

#define HYSJ_LAMBDA(X,x,D,d)                                            \
  template<typename G,typename I = X##_id_t<G>>                         \
  using x##d##incident_id_t = incident_id_t<X##_tag,D##_tag_type,G,I>; 
HYSJ_MAP_LAMBDA(HYSJ_GRAPHS_INCIDENCE_MODES)
#undef HYSJ_LAMBDA

namespace concepts{
  
  template<typename G,element_tag T,typename D,typename I = element_id_t<G,T>>
  concept incidence_graph =
    (requires(const G &g,I i){
      graphs::incident(constant_c<T>,std::declval<D>(),g,i);
    });

  #define HYSJ_LAMBDA(X,x,D,d)                                              \
    template<typename G>                                                    \
    concept x##d##incidence_graph = incidence_graph<G,X##_tag,D##_tag_type>;
  HYSJ_MAP_LAMBDA(HYSJ_GRAPHS_INCIDENCE_MODES)
  #undef HYSJ_LAMBDA

  #define HYSJ_LAMBDA(D,X,x,...) && incidence_graph<G,X##_tag,D>
  template<typename G,typename D>
  concept directional_incidence_graph = 
    true
    HYSJ_MAP(HYSJ_LAMBDA,D,HYSJ_GRAPH_ELEMENTS)
    ;
  #undef HYSJ_LAMBDA

  template<typename G,typename D>
  concept unidirectional_incidence_graph =
    directional_incidence_graph<G,D>
    &&(requires(D d,const G &g,vertex_id_t<G> i){
        { graphs::vincident(d,g,i) } ->
          concepts::static_edge_id_range<G>;
    });

  #define HYSJ_LAMBDA(D,d) && directional_incidence_graph<G,D##_tag_type>
  template<typename G>
  concept bidirectional_incidence_graph =
    true 
    HYSJ_MAP_LAMBDA(HYSJ_GRAPHS_DIRECTIONS)
    ;
  #undef HYSJ_LAMBDA

  template<typename G,typename D>
  concept bincidence =
    (directional_incidence_graph<G,D>)
    &&(undirected_constant<D>
       && bidirectional_incidence_graph<G>);
  template<typename G,typename D>
  concept uincidence =
    (directional_incidence_graph<G,D>)
    &&!(undirected_constant<D>
        && bidirectional_incidence_graph<G>);

} //concepts



} //hysj::graphs
#include<hysj/tools/epilogue.hpp>
