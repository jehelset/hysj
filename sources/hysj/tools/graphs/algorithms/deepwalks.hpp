#pragma once
#include<optional>
#include<ranges>
#include<tuple>
#include<vector>
#include<utility>

#include<fmt/format.h>

#include<hysj/tools/coroutines.hpp>
#include<hysj/tools/coroutines/trees.hpp>
#include<hysj/tools/preprocessor.hpp>
#include<hysj/tools/reflection.hpp>
#include<hysj/tools/type_traits.hpp>
#include<hysj/tools/functional/unwrap.hpp>

#include<hysj/tools/graphs/incidence.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::graphs::deepwalks{
  
#define HYSJ_GRAPHS_DEEPWALK_EVENTS \
  (start)                           \
  (down)                            \
  (right)                           \
  (up)                              \
  (left)                            \
  (stop)
  
enum event_tag : unsigned {
  HYSJ_SPLAT(0,HYSJ_GRAPHS_DEEPWALK_EVENTS)
};
HYSJ_MIRROR_ENUM(event_tag,HYSJ_GRAPHS_DEEPWALK_EVENTS);

#define HYSJ_LAMBDA(id)                                       \
  inline constexpr auto id##_tag = constant_c<event_tag::id>;
HYSJ_MAP_LAMBDA(HYSJ_GRAPHS_DEEPWALK_EVENTS)
#undef HYSJ_LAMBDA
#define HYSJ_LAMBDA(id)                             \
  using id##_tag_type = constant_t<event_tag::id>;
HYSJ_MAP_LAMBDA(HYSJ_GRAPHS_DEEPWALK_EVENTS)
#undef HYSJ_LAMBDA

namespace concepts{
  template<typename T>
  concept event = std::convertible_to<T,event_tag>;
}
template<typename T>
consteval bool is_event(const T &){
  return concepts::event<T>;
}

constexpr bool is_root_event(event_tag tag){
  return tag == any_of(event_tag::start,event_tag::stop);
}
constexpr bool is_branch_event(event_tag tag){
  return tag == any_of(event_tag::right,event_tag::down);
}
constexpr bool is_pre_event(event_tag tag){
  return tag == any_of(event_tag::start,event_tag::down);
}
constexpr bool is_post_event(event_tag tag){
  return tag == any_of(event_tag::right,event_tag::stop);
}

template<typename Incident>
struct frame{
  std::ranges::iterator_t<Incident> edge_iter;
  [[no_unique_address]] std::ranges::sentinel_t<Incident> edge_end;
    
  friend HYSJ_MIRROR_STRUCT(frame,(edge_iter,edge_end));
};

template<typename Direction,typename Graph>
using frame_t = frame<graphs::vincident_t<Direction,Graph>>;

template<typename State>
using direction_t = std::remove_cvref_t<decltype(unwrap(std::declval<State>()).direction)>;

namespace concepts{
      
  template<typename Stack,typename Direction,typename Graph>
  concept stack =
    (requires(Stack &stack,frame_t<Direction,Graph> frame){
      { stack.pop_back() };
      { stack.push_back(std::move(frame)) };
      { stack.back() } -> std::same_as<frame_t<Direction,Graph> &>;
      { stack.empty() } -> std::same_as<bool>;
      { stack.clear() };
    });
  
  //FIXME:
  //  need borrowable incidence relation
  //  - jeh
  template<typename State>
  concept state =
    (requires(State state){
      { unwrap(state).direction } -> graphs::concepts::directional;
      { unwrap(state).graph } -> graphs::concepts::directional_incidence_graph<decltype(unwrap(state).direction)>;
      { unwrap(state).root } -> graphs::concepts::vertex_id_convertible<decltype(unwrap(state).graph)>;
      { unwrap(unwrap(state).stack) } -> stack<decltype(unwrap(state).direction),decltype(unwrap(state).graph)>;
    });

  template<class State>
  concept bstate =
    state<State>
    && (requires(State state){
        { unwrap(state).graph } -> graphs::concepts::bincidence<decltype(unwrap(state).direction)>;
      });
  
  template<class State>
  concept ustate =
    state<State>
    && (requires(State state){
        { unwrap(state).graph } -> graphs::concepts::uincidence<decltype(unwrap(state).direction)>;
      });

} //concepts

template<typename State>
using vertex = vertex_id_t<decltype(unwrap(std::declval<State>()).graph)>;
template<typename State>
using edge = edge_id_t<decltype(unwrap(std::declval<State>()).graph)>;
template<typename State>
using step = vincident_id_t<decltype(unwrap(std::declval<State>()).direction),decltype(unwrap(std::declval<State>()).graph)>;

template<concepts::state S>
void reset(S & dw_ref){
  auto &dw = unwrap(dw_ref);
  dw.stack.clear();
}

template<typename CoGet>
co::api_t<CoGet> walk(CoGet,concepts::state auto state_,auto control){ //NOTE: can this be constrained? - jeh

  auto &co_this = HYSJ_CO(CoGet);

  auto &state = unwrap(state_);
  auto transfer = [&](auto event){
    return control(co::get<event_tag>(co_this), event, std::ref(state));
  };

  std::optional<event_tag> action = std::nullopt;
 start:

  action = co_await transfer(start_tag);
  switch(action.value_or(start)){
  case start:
    state.start();
    break;
  case stop:
    state.stop();
    break;
  default:
    goto start;
  }

  while(!state.done())
    if(!state.is_rightmost()){
      action = co_await transfer(down_tag);
      switch(action.value_or(down)){
      case start:
        state.stop();
        goto start;
      case stop:
        state.finish();
        break;
      case left:
        break;
      case right:
        state.right();
        break;
      default:
        state.down();
        break;
      }
    }
    else if(!state.upmost()){
      state.up();
      action = co_await transfer(right_tag);
      switch(action.value_or(right)){
      case start:
        state.stop();
        goto start;
      case stop:
        state.finish();
        break;
      case left:
        break;
      default:
        state.right();
        break;
      }
    }
    else
      state.stop();
  co_await transfer(stop_tag);
  goto start;
}

template<typename Direction,typename Graph>
using stack = std::vector<frame_t<Direction,Graph>>;

constexpr auto make_stack(const auto &direction,const auto &graph){
  return stack<decltype(direction),decltype(graph)>{};
}

template<typename Direction,typename Graph,typename Stack = stack<Direction,Graph>>
struct state{

  [[no_unique_address]] Direction direction;
  Graph graph;
  Stack stack;
  vertex_id_t<Graph> root;

  //FIXME: trash! - jeh
  bool done()const noexcept{
    return stack.empty();
  }
  auto &frame(){
    return stack.back();
  }
  auto &frame()const{
    return stack.back();
  }
  auto edges()const{
    return ranges::tie(frame().edge_iter,frame().edge_end());
  }
  auto &upframe(){
    return stack[stack.size() - 1];
  }
  auto &upframe()const{
    return stack[stack.size() - 2];
  }
  void up(){
    stack.pop_back();
  }
  void down(vertex_id_t<Graph> vertex){
    auto edges = graphs::vincident(direction,graph,vertex);
    stack.push_back({
      .edge_iter = std::ranges::begin(edges),
      .edge_end = std::ranges::end(edges)
    });
  }
  void down(){
    down(head());
  }
  void start(){
    down(root);
  }
  void stop(){
    stack.clear();
  }
  auto &here(){
    return frame().edge_iter;
  }
  auto &uphere()const{
    return upframe().edge_iter;
  }
  auto &here()const{
    return frame().edge_iter;
  }

  auto edge()const{
    return *here();
  }
  auto vertex()const{
    if(done())
      return root;
    else
      return tail();
  }
  auto head()const{
    return graphs::head(graph,*here());
  }
  auto tail()const{
    return graphs::tail(graph,*here());
  }
  auto &left(){
    return --here();
  }
  auto &right(){
    return ++here();
  }
  void finish(){
    //FIXME: compute_common_end - jeh
    while(!is_rightmost())
      right();
  }
  bool upmost()const{
    return stack.size() == 1;
  }
  auto rightmost()const{
    return frame().edge_end;
  }
  bool is_rightmost()const{
    return here() == rightmost();
  }
  auto leftmost()const{
    return std::ranges::begin(graphs::vincident(direction,graph,tail()));
  }
  bool is_leftmost()const{
    return leftmost() == here();
  }

  friend HYSJ_MIRROR_STRUCT(state,(direction,graph,stack,root));
};

template<typename D,typename G>
auto make_state(D d,G g,vertex_id_t<G> r = {}){
  return state<D,G>{
    .direction{d},
    .graph{std::move(g)},
    .stack{},
    .root{std::move(r)}
  };
}

constexpr auto with_starts(std::ranges::forward_range auto &&U,auto C){
  return 
    [R = ranges::seq(fwd(U)),C = std::move(C)]
      <typename DWGet>(DWGet,auto dw_event,auto dw_ref) mutable
        -> co::api_t<DWGet> {
      auto &dw_co = HYSJ_CO(DWGet);
      if constexpr(dw_event == start){
        auto &dw = unwrap(dw_ref);
        if(auto r = R()){
          dw.root = {*r};
          co_await C(co::pipe(dw_co),dw_event,dw_ref);
        }
        else
          co_final_await co::nil<2>(dw_co);
      }
      else
        co_await C(co::pipe(dw_co),dw_event,dw_ref);
    };
}

template<typename D,typename G>
struct event{
  using Element = std::variant<graphs::vertex_id_t<G>,graphs::vincident_id_t<D,G>>;

  event_tag tag;
  Element element;

  friend HYSJ_MIRROR_STRUCT(event,(tag,element));
  auto operator<=>(const event &) const = default;

  static event make(auto tag,const auto &state){
    if constexpr(tag == any_of(start_tag,stop_tag))
      return {tag(),state.root};
    else
      return {tag(),state.edge()};
  }
};

template<typename D,typename G>
auto trace(D d,G g,std::ranges::forward_range auto &&U){
  using E = event<D,G>;
  using S = state<D,G>;
  return 
    walk(co_get<E>{},
         S{d,g,{}},
         with_starts(
           fwd(U),
           []<class DWGet>(DWGet,auto dw_event_tag,auto dw_ref) -> co::api_t<DWGet>{
             auto &dw_co = HYSJ_CO(DWGet);
             auto &dw = unwrap(dw_ref);
             auto dw_event = E::make(dw_event_tag,dw);
             co_await co::put<0>(dw_co,dw_event);
             co_final_await co::nil<1>(dw_co);
           }));
}

} //hysj::graphs::deepwalks
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::graphs{

namespace dw = deepwalks;

auto dwalk(auto co,auto s,auto c){
  return dw::walk(co,std::move(s),c);
}
template<typename D,typename G>
auto dwalk(auto co,D d,G g,vertex_id_t<G> r,auto c){
  return dw::walk(co,dw::state<D,G>{d,g,{},r},c);
}

} //hysj::graphs
HYSJ_ENUM_FORMATTER(hysj::_HYSJ_VERSION_NAMESPACE::graphs::dw::event_tag);
template<typename D,typename G>
struct fmt::formatter<hysj::_HYSJ_VERSION_NAMESPACE::graphs::dw::event<D,G>>{
  using T = hysj::_HYSJ_VERSION_NAMESPACE::graphs::dw::event<D,G>;
  constexpr auto parse(format_parse_context& context)const{
    auto it  = context.begin(),
         end = context.end();
    if(it != end && *it != '}')
      throw fmt::format_error("invalid format");
    return it;
  }
  auto format(const T &e,format_context &context)const{
    return std::visit(
      [&](auto i){
        return fmt::format_to(context.out(),"{},{}",e.tag,i);
      },
      e.element);
  }
};
#include<hysj/tools/epilogue.hpp>
