#pragma once
#include<concepts>
#include<functional>
#include<optional>
#include<set>
#include<ranges>

#include<kumi/tuple.hpp>

#include<hysj/tools/coroutines.hpp>
#include<hysj/tools/coroutines/trees.hpp>
#include<hysj/tools/graphs/incidence.hpp>
#include<hysj/tools/graphs/partitions.hpp>
#include<hysj/tools/graphs/properties.hpp>
#include<hysj/tools/reflection.hpp>
#include<hysj/tools/type_traits.hpp>
#include<hysj/tools/types.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::graphs::hopcroft1971{

//Adapted from "An n·log(n) Algorithm for Minimizing States in a Finite Automaton" - [Hopcroft1971]:
//  1: construct δ⁻¹(s,a). 
//  2: B(1) = F, B(2) = S-F. ∀ i,a ∈ |2| x I construct
//     B'(B(i),a) = { s | s ∈ B(i) and δ⁻¹(s,a) ≠ ∅ }
//     B': B × I → P(S)
//  3: set k = 3
//  4: ∀ a ∈ I (symbols)
//      L(a) = { {1} if |B'(B(1),a)| ≤ |B'(B(2),a)|, 
//               {2} otherwise }
//      L(a) paritions symbols
//  5: select a,i ∈ I × L(a), terminate if L(a) = {} for all a in I
//  6: delete i from L(a)
//  7: ∀ j < k | ∃ t ∈ B(j) with δ(t,a) ∈ B'(B(i),a)
//     1: partition B(j) into
//          B'(j)  = { t | δ(t,a) ∈ B'(B(i),a) }
//          B''(j) = B(j) - B'(j)
//     2: replace B(j) by B'(j) and construct B(k) = B''(j)
//        construct the corresponding B'(B(j),a) and B'(B(k),a) ∀ a ∈ I
//     3: ∀ a ∈ I modify L(a)
//  8: goto 5
//
//δ and δ⁻¹ implemented in terms of graph incidence, and the symbol of an
//edge is computed via symbol function parameter.
//L(A) is initially set up based on initial partitioning provided by user,
//instead of state terminality. 

namespace concepts{
  template<typename Symbol>
  concept symbol = std::integral<std::remove_cvref_t<Symbol>>;
}

template<typename Partitioning,typename Symbol>
using partitioner_t = kumi::tuple<prt::partition_t<Partitioning>,Symbol>;

namespace concepts{
      
  template<typename Partitioners,typename Partitioning,typename Alphabet>
  concept partitioners = requires(Partitioners &partitioners,partitioner_t<Partitioning,Alphabet> partitioner){
    { partitioners.next() } -> std::convertible_to<std::optional<partitioner_t<Partitioning,Alphabet>>>;
    { partitioners.add(partitioner) } -> std::convertible_to<bool>;
  };

  template<typename State>
  concept state = requires(State state){
    { unwrap(state).graph        } -> graphs::concepts::bidirectional_incidence_graph;
    { unwrap(state).alphabet     } -> symbol;
    { unwrap(state).symbols      } ->
      graphs::concepts::const_edge_property_of<
        decltype(unwrap(state).alphabet),
        decltype(unwrap(state).graph)>;
    { unwrap(state).partitioning } -> prt::concepts::partitioning;
    { unwrap(state).partitioners } ->
      concepts::partitioners<decltype(unwrap(state).partitioning),
                             decltype(unwrap(state).alphabet)>;
  };
    
}

using partitioner = partitioner_t<prt::partitioning,natural>;

struct partitioners{
  std::set<partitioner> container;
  
  std::optional<partitioner> next(){
    if(container.empty())
      return std::nullopt;
    auto node = container.extract(container.begin());
    return std::move(node.value());
  }
  bool add(partitioner p){
    return std::get<1>(container.insert(p));
  }

  friend HYSJ_MIRROR_STRUCT(partitioners,(container))
};

template<typename Graph,typename Alphabet,typename Symbols,typename Partitioning = prt::partitioning,typename Partitioners = partitioners>
struct state{
  Graph graph;
  Alphabet alphabet;
  Symbols symbols;
  Partitioning partitioning;
  Partitioners partitioners;
  
  friend HYSJ_MIRROR_STRUCT(state,(alphabet,graph,symbols,partitioning,partitioners));
};

template<prt::event_tag e>
auto task(constant_t<e>,auto &co_this){
  if constexpr(e == any_of(prt::stop_tag))
    return co::pipe(co_this);
  else
    return co::get<none>(co_this);
}

template<typename RefineGet>
co::api_t<RefineGet> refine(RefineGet,concepts::state auto state_,auto control){

  auto &refine_co = HYSJ_CO(RefineGet);

  auto &state = unwrap(state_);
  using partitioner = partitioner_t<decltype(state.partitioning),decltype(state.alphabet)>;

  auto transfer = [&](auto event,auto... desc){
    return control(task(event,refine_co),event,std::ref(state),desc...);
  };

  auto targets_of = 
    [&](auto partitioner){
      auto [partition_id,symbol] = partitioner;
      return views::keep(
        prt::members(state.partitioning,partition_id),
        [&,symbol](auto member_id){
          auto edge_ids = graphs::viincident(state.graph,member_id);
          return ranges::contains_if(
            edge_ids,
            [&](auto edge){ return graphs::get(state.symbols,edge) == symbol; });
        });
    };

  auto add_partitioner =
    [&](auto partition_id,auto new_partition_id,auto symbol){
      const auto target_count =
        std::ranges::distance(targets_of(partitioner{partition_id,symbol}));
      const auto new_target_count =
        std::ranges::distance(targets_of(partitioner{new_partition_id,symbol}));
      if(target_count > new_target_count ||
         !state.partitioners.add(partitioner{partition_id,symbol})){
        state.partitioners.add(partitioner{new_partition_id,symbol});
        return new_partition_id;
      }
      return partition_id;
    };

  co_await transfer(prt::start_tag);

  //NOTE:
  //  set up initial partitioners. don't add any partitioners
  //  on the partition with the smallest target-set for any
  //  symbol.
  //  - jeh
  {
    auto partition_ids = prt::enumerate(state.partitioning);
    if(std::ranges::size(partition_ids) < 2)
      co_await transfer(prt::stop_tag);

    for(auto symbol:views::indices(state.alphabet)){
      auto partition_it = std::ranges::begin(partition_ids);
      auto min_partition_id = *partition_it++;
      for(auto partition_end = std::ranges::end(partition_ids);
          partition_it != partition_end;
          ++partition_it)
        if(auto added_partition_id = add_partitioner(min_partition_id,*partition_it,symbol);
           added_partition_id == min_partition_id)
            min_partition_id = *partition_it;
    }
  }
  
  while(auto partitioner = state.partitioners.next()){
    //FIXME: just search for an edge with a target whose partition is the target-partition.
    //       do partitioning-lookups instead of this. - jeh
    auto partition_targets = ranges::vec(targets_of(*partitioner));
    auto [target_partition,symbol] = *partitioner;

    auto is_not_partitioned =
      [&](auto source){
        auto edge_ids = graphs::voincident(state.graph,source);
        return std::ranges::find_if(
          edge_ids,
          [&](auto edge){
            auto target = graphs::head(state.graph,edge);
            return graphs::get(state.symbols,edge) == symbol
                   && ranges::contains(partition_targets,target);
          }) == std::ranges::end(edge_ids);
      };
    
    for(auto partition_id:prt::enumerate(state.partitioning)){
      auto &members = prt::members(state.partitioning,partition_id);
      auto [partitioned_member_it,partitioned_member_end] =
        std::ranges::partition(members,is_not_partitioned);
      if(partitioned_member_it != std::ranges::begin(members)
         && partitioned_member_it != partitioned_member_end){
        auto new_partition_id =
          state.partitioning.refine(partition_id,partitioned_member_it);
        for(auto symbol:views::indices(state.alphabet))
          add_partitioner(partition_id,new_partition_id,symbol);
        co_await transfer(prt::refine_tag,partition_id,new_partition_id);
      }
    }
  }
  co_final_await transfer(prt::stop_tag);
}

concepts::state auto
make_state(auto graph,auto alphabet,
           auto symbols,auto partitioning){
  return state{
    .graph        = std::move(graph),
    .alphabet     = alphabet,
    .symbols      = symbols,
    .partitioning = std::move(partitioning),
    .partitioners = partitioners{}
  };
}

auto construct(auto graph,auto alphabet,
               auto symbols,auto partitioning){
  using partitions = std::vector<std::vector<prt::member_t<decltype(partitioning)>>>;
  return refine(
    co_get<partitions>{},
    make_state(std::move(graph),
               alphabet,
               symbols,
               partitioning),
    []<typename Hop71Get>(Hop71Get,auto hop71_event,auto hop71_ref,auto...)
      -> co::api_t<Hop71Get>{
      auto &hop71_co = HYSJ_CO(Hop71Get);

      auto &hop71 = unwrap(hop71_ref);
      if constexpr(hop71_event == any_of(prt::stop_tag)){
        partitions P = ranges::vec(
          prt::enumerate(hop71.partitioning),
          [&](auto partition){
            return ranges::vec(prt::members(hop71.partitioning,partition));
          });
        co_await co::ret<0>(hop71_co,std::move(P));
      }
      else
        co_await co::nil<1>(hop71_co);
    })().value();
}

} //hysj::graphs::hopcroft1971
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::graphs{

namespace hop71 = hopcroft1971;

} //hysj::graphs
#include<hysj/tools/epilogue.hpp>
