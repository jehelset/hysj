#pragma once
#include<optional>
#include<tuple>
#include<utility>

#include<fmt/format.h>

#include<hysj/tools/coroutines.hpp>
#include<hysj/tools/preprocessor.hpp>
#include<hysj/tools/reflection.hpp>
#include<hysj/tools/type_traits.hpp>

#include<hysj/tools/graphs/incidence.hpp>
#include<hysj/tools/graphs/properties.hpp>
#include<hysj/tools/graphs/algorithms/deepwalks.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::graphs::deepsearch{

#define HYSJ_GRAPHS_DEEPSEARCH_COLORS \
  (white)                             \
  (grey)                              \
  (black)

enum color : unsigned {
  HYSJ_SPLAT(0,HYSJ_GRAPHS_DEEPSEARCH_COLORS)
};
HYSJ_MIRROR_ENUM(color,HYSJ_GRAPHS_DEEPSEARCH_COLORS)

#define HYSJ_LAMBDA(id)                                     \
  inline constexpr auto id##_tag = constant_c<color::id>;
HYSJ_MAP_LAMBDA(HYSJ_GRAPHS_DEEPSEARCH_COLORS)
#undef HYSJ_LAMBDA
  
#define HYSJ_GRAPHS_DEEPSEARCH_EVENTS \
  (branch)                            \
  (frond)                             \
  (vine)                              \
  (liana)                             \
  (weed)                              \
  (root)                              \
  (bough)                             \
  (palm)                              \
  (snag)
  
enum event_tag : unsigned {
  HYSJ_SPLAT(0,HYSJ_GRAPHS_DEEPSEARCH_EVENTS)
};
HYSJ_MIRROR_ENUM(event_tag,HYSJ_GRAPHS_DEEPSEARCH_EVENTS)

#define HYSJ_LAMBDA(id)                                       \
  inline constexpr auto id##_tag = constant_c<event_tag::id>;
HYSJ_MAP_LAMBDA(HYSJ_GRAPHS_DEEPSEARCH_EVENTS)
#undef HYSJ_LAMBDA
#define HYSJ_LAMBDA(id)                             \
  using id##_tag_type = constant_t<event_tag::id>;
HYSJ_MAP_LAMBDA(HYSJ_GRAPHS_DEEPSEARCH_EVENTS)
#undef HYSJ_LAMBDA

namespace concepts{

  template<typename T>
  concept event = is_constant_of<T,event_tag>;

}

template<typename T>
consteval bool is_event(const T &){
  return concepts::event<T>;
}
  
namespace concepts{
      
  template<typename State>
  concept state =
    (requires(State state){
      { unwrap(state).dw } -> dw::concepts::state;
      { unwrap(state).colors } ->
        graphs::concepts::vertex_property_of<color,decltype(unwrap(state).dw.graph)>;
      { unwrap(state).times } -> 
        graphs::concepts::integral_vertex_property<decltype(unwrap(state).dw.graph)>;
      requires (
        requires { { unwrap(state).dw } -> dw::concepts::ustate; }
        || requires { { unwrap(state).edge_masks } -> 
            graphs::concepts::edge_property_of<bool,decltype(unwrap(state).dw.graph)>; });
    });
  
  template<typename State>
  concept bstate =
    state<State>
    && (requires(State state){
        { unwrap(state).dw } -> dw::concepts::bstate;
      });

} //concepts

//FIXME: garbage again - jeh
template<typename DFSGet>
co::api_t<DFSGet> search(DFSGet,concepts::state auto dfs_ref,auto control){ //NOTE: can this be constrained? - jeh

  auto &dfs_co = HYSJ_CO(DFSGet);
  auto &dfs = unwrap(dfs_ref);

  co_await graphs::dwalk(
    co::pipe(dfs_co),
    std::ref(dfs.dw),
    [&]<typename DWGet>(DWGet,auto dw_event,auto dw_ref) -> co::api_t<DWGet> {
      auto &dw_co = HYSJ_CO(DWGet);
      auto &dw = unwrap(dw_ref);

      auto dfs_transfer = [&](auto dfs_event){
        return control(co::get<event_tag>(dw_co),dfs_event,std::ref(dfs));
      };
      auto dw_transfer = [&](auto dfs_event){
        return control(co::pipe(dw_co),dfs_event,std::ref(dfs));
      };

      if constexpr(dw_event == dw::start){
        auto action = co_await dfs_transfer(root_tag);
        switch(action.value_or(root)){
        case snag:
          break;
        case palm:
          if(dfs.color(dw.root) == white)
            dfs.discover(dw.root);
          dfs.finish(dw.root);
          co_await dfs_transfer(palm_tag);
          break;
        default:
          if(dfs.color(dw.root) == white){
            dfs.discover(dw.root);
            co_final_await co::ret<1>(dw_co,dw::start);
          }
          co_await dfs_transfer(snag_tag);
          break;
        }
        co_final_await co::ret<1>(dw_co,dw::right);
      }
      else if constexpr(dw_event == dw::down){
        static constexpr bool bdw = dw::concepts::bstate<decltype(dw)>;
        if constexpr(bdw){
          auto edge = dw.edge();
          if(graphs::get(dfs.edge_masks,edge))
            co_final_await co::ret<1>(dw_co,dw::right);
          else
            graphs::put(dfs.edge_masks,edge,true);
        }

        auto head = dw.head();
        switch(dfs.color(head)){
        case white:{
          const auto dfs_action = co_await dfs_transfer(branch_tag);
          if(dfs_action != snag)
            dfs.discover(head);
          const auto dw_action =
            [&](auto dfs_action){
              switch(dfs_action){
              case snag:   return dw::right;
              case bough:  return dw::right;
              case branch: return dw::down;
              case palm:   return dw::stop;
              default:     return dw::left;
              }
            }(dfs_action.value_or(branch));
          co_final_await co::ret<1>(dw_co,dw_action);
          break;
        }
        case grey:
          co_await dfs_transfer(frond_tag);
          break;
        case black:{
          auto tail = dw.tail();
          if(dfs.time(tail) < dfs.time(head))
            co_await dfs_transfer(weed_tag);
          else if(dfs.time(head) < dfs.time(dfs.dw.root))
            co_await dfs_transfer(liana_tag);
          else
            co_await dfs_transfer(vine_tag);
          break;
        }
        }
        co_await co::ret<1>(dw_co,dw::right);
      }
      else if constexpr(dw_event == dw::right){
        dfs.finish(dw.head());
        const auto dfs_action = co_await dfs_transfer(bough_tag);
        const auto dw_action =
          [](auto dfs_action){
            switch(dfs_action){
            case branch: return dw::left;
            default: return dw::right;
            }
          }(dfs_action.value_or(bough));
        co_final_await co::ret<1>(dw_co,dw_action);
      }
      else{
        static_assert(dw_event == dw::stop);
        dfs.finish(dw.root);
        co_await dw_transfer(palm_tag); //FIXME: pipe & final-await! - jeh
      }
    });
}

using colors = std::vector<color>;
using times  = std::vector<natural>;

//FIXME: garbage!
template<typename DWState>
using edge_masks_t =
  std::conditional_t<dw::concepts::bstate<DWState>,
                     std::vector<bool>,
                     none>;

template<typename DWState,typename DFSColors = colors,typename DFSTimes = times>
struct state{
  using DWVertex = dw::vertex<DWState>;
  using DFSTime = std::remove_cvref_t<get_t<DFSTimes,DWVertex>>;
  using DFSEdgeMasks = edge_masks_t<DWState>;

  DWState dw;
  DFSColors colors;
  DFSTimes times;
  DFSTime now;
  [[no_unique_address]] DFSEdgeMasks edge_masks;

  void resize(){
    graphs::resize(colors,graphs::order(dw.graph),white);
    graphs::resize(times,graphs::order(dw.graph),0z);
  }

  auto color(auto vertex)const{
    return graphs::get(colors,vertex);
  }
  auto time(auto vertex)const{
    return graphs::get(times,vertex);
  }

  void discover(auto vertex){
    graphs::put(times,vertex,now++);
    graphs::put(colors,vertex,grey);
  }
  void finish(auto vertex){
    graphs::put(colors,vertex,black);
  }
  HYSJ_MIRROR_STRUCT(state,(dw,colors,times,now));
};

auto make_colors(const auto &g){
  return vprop(g,white);
}
auto make_times(const auto &g){
  return vprop(g,natural{});
}
template<typename DW>
constexpr auto make_edge_masks(const DW &dw){
  if constexpr(dw::concepts::bstate<DW>)
    return eprop(dw.graph,false);
  else
    return none{};
}
template<typename D,typename G>
concepts::state auto make_state(D d,G g,vertex_id_t<G> r = {}){
  auto C = make_colors(g);
  auto T = make_times(g);
  auto W = dw::make_state(d,std::move(g),r);
  auto E = make_edge_masks(W);
  return state{
    .dw = std::move(W),
    .colors = std::move(C),
    .times = std::move(T),
    .now = natural{},
    .edge_masks = std::move(E)
  };
}

//FIXME: trash - jeh
auto with_roots(std::ranges::forward_range auto &&U,auto C){
  return 
    [R = ranges::seq(fwd(U)),C = std::move(C)]<typename DFSGet>
      (DFSGet,auto dfs_event_tag,auto dfs_ref) mutable
        -> co::api_t<DFSGet>{
      auto &dfs_co = HYSJ_CO(DFSGet);
      if constexpr(dfs_event_tag == root_tag){
        auto &dfs = unwrap(dfs_ref);
        if(auto r = R()){
          dfs.dw.root = {*r};
          co_await C(co::get<none>(dfs_co),dfs_event_tag,dfs_ref);
          co_await co::ret<1>(dfs_co,root);
        }
        else
          co_await co::nil<0>(dfs_co);
      }
      else
        co_await C(co::pipe(dfs_co),dfs_event_tag,dfs_ref);
    };
}

template<typename D,typename G>
struct event{
  using Element = std::variant<graphs::vertex_id_t<G>,graphs::vincident_id_t<D,G>>;
  event_tag tag;
  Element element;
  natural time;

  auto operator<=>(const event &) const = default;

  friend HYSJ_MIRROR_STRUCT(event,(tag,element,time))

  static event make(auto tag,const auto &dfs){
    if constexpr(tag == any_of(root_tag,palm_tag,snag_tag))
      return {tag(), dfs.dw.root  ,dfs.now};
    else
      return {tag(), dfs.dw.edge(),dfs.now};
  }
};

template<typename D,typename G>
auto trace(D d,G g,std::ranges::forward_range auto &&U){
  using E = event<D,G>;
  return 
    search(co_get<E>{},
           make_state(d,std::move(g)),
           with_roots(
             fwd(U),
             []<typename DFSGet>(DFSGet,auto dfs_event_tag,auto dfs_ref) mutable -> co::api_t<DFSGet>{
               auto &dfs_co = HYSJ_CO(DFSGet);
               auto &dfs = unwrap(dfs_ref);
               auto dfs_event = E::make(dfs_event_tag,dfs);
               co_await co::put<0>(dfs_co,dfs_event);
               co_final_await co::nil<1>(dfs_co);
             }));
}

} //hysj::graphs::deepsearch
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::graphs{
  
namespace dfs = deepsearch;

auto dsearch(auto co,dfs::concepts::state auto s,auto c){
  return dfs::search(co,std::move(s),c);
}

template<typename D,typename G>
auto dsearch(auto co,D d,G g,vertex_id_t<G> v,auto c){
  return dfs::search(co,make_state(d,std::move(g),v),c);
}

} //hysj::graphs
HYSJ_ENUM_FORMATTER(hysj::_HYSJ_VERSION_NAMESPACE::graphs::dfs::event_tag);
template<typename D,typename G>
struct fmt::formatter<hysj::_HYSJ_VERSION_NAMESPACE::graphs::dfs::event<D,G>>{
  using T = hysj::_HYSJ_VERSION_NAMESPACE::graphs::dfs::event<D,G>;
  constexpr auto parse(format_parse_context& context)const{
    auto it  = context.begin(),
         end = context.end();
    if(it != end && *it != '}')
      throw fmt::format_error("invalid format");
    return it;
  }
  auto format(const T &e,format_context &context)const{
    return std::visit(
      [&](auto i){
        return fmt::format_to(context.out(),"{},{},{}",e.tag,i,e.time);
      },
      e.element);
  }
};
#include<hysj/tools/epilogue.hpp>
