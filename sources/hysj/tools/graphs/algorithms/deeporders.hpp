#pragma once
#include<utility>
#include<functional>

#include<kumi/tuple.hpp>

#include<hysj/tools/coroutines.hpp>
#include<hysj/tools/graphs/algorithms/deepwalks.hpp>
#include<hysj/tools/functional/any_of.hpp>
#include<hysj/tools/functional/overload.hpp>
#include<hysj/tools/reflection.hpp>
#include<hysj/tools/functional/pick.hpp>
#include<hysj/tools/type_traits.hpp>
#include<hysj/tools/types.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::graphs::deeporders{

#define HYSJ_GRAPHS_DEEPORDERS \
  (pre)                        \
  (post)

enum class order_tag : unsigned {HYSJ_SPLAT(0,HYSJ_GRAPHS_DEEPORDERS)};
HYSJ_MIRROR_ENUM(order_tag,HYSJ_GRAPHS_DEEPORDERS)

#define HYSJ_LAMBDA(id)                                       \
  using id##_tag_type = constant_t<order_tag::id>;            \
  inline constexpr auto id##_tag = constant_c<order_tag::id>;
HYSJ_MAP_LAMBDA(HYSJ_GRAPHS_DEEPORDERS)
#undef HYSJ_LAMBDA

#define HYSJ_GRAPHS_DEEPORDERS_MODES                          \
  HYSJ_PRODUCT((HYSJ_GRAPHS_DEEPORDERS)                       \
               (HYSJ_SELECT((0)(1),HYSJ_GRAPHS_DIRECTIONS)))

template<dw::event_tag tag>
constexpr auto map_event = []{
  if constexpr(tag == any_of(dw::start_tag,dw::down_tag))
    return pre_tag;
  else
    return post_tag;
 }();

template<auto dfo_filter_>
auto order(dw::concepts::state auto s){

  static constexpr auto dfo_filter = std::optional<order_tag>{dfo_filter_};
  using V = dw::vertex<decltype(s)>;
  using result = std::conditional_t<!dfo_filter.has_value(),kumi::tuple<order_tag,V>,V>;
  return dw::walk(
    co_get<result>{},
    std::move(s),
    []<typename DFOGet>(DFOGet,auto dw_event,auto dw_ref)-> co::api_t<DFOGet>{

      auto &dfo_co = HYSJ_CO(DFOGet);
      constexpr auto dfo_event = map_event<dw_event>;
      constexpr bool dfo_accept = dfo_filter.value_or(dfo_event) == dfo_event;

      auto dw_result =
        [&](auto vertex){
          if constexpr(dfo_filter)
            return vertex;
          else
            return result{dfo_event,vertex};
        };
      
      auto &dw = unwrap(dw_ref);

      if constexpr(dw_event != dw::stop_tag){
        if constexpr(dfo_accept){
          if constexpr(dw_event == dw::start_tag)
            co_await co::put<0>(dfo_co, dw_result(dw.root));
          else
            co_await co::put<0>(dfo_co, dw_result(dw.head()));
        }
        co_await co::nil<1>(dfo_co);
      }
      else if constexpr(dfo_accept)
        co_final_await co::ret<0>(dfo_co,dw_result(dw.root));
      else
        co_final_await co::nil<0>(dfo_co);
    });
}

} //hysh::graph::deeporders
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::graphs{
  
namespace dfo = deeporders;

template<typename D,typename G>
auto dorder(D d,G g,vertex_id_t<G> v){
  return dfo::order<std::nullopt>(dw::make_state(d,std::move(g),v));
}
template<dfo::order_tag O,typename D,typename G>
auto dorder(constant_t<O>,D d,G g,vertex_id_t<G> v){
  return dfo::order<O>(dw::make_state(d,std::move(g),v));
}

#define HYSJ_LAMBDA(D,d)                                         \
  inline constexpr auto d##order =                               \
    bind<>(lift((dorder)),D##_tag);
HYSJ_MAP_LAMBDA(HYSJ_GRAPHS_DIRECTIONS)
#undef HYSJ_LAMBDA
#define HYSJ_LAMBDA(o,D,d)                                         \
  inline constexpr auto d##o##order =                              \
    bind<>(lift((dorder)),dfo::o##_tag,D##_tag);
HYSJ_MAP_LAMBDA(HYSJ_GRAPHS_DEEPORDERS_MODES)
#undef HYSJ_LAMBDA

} //hysj::graphs
HYSJ_ENUM_FORMATTER(hysj::_HYSJ_VERSION_NAMESPACE::graphs::dfo::order_tag);
#include<hysj/tools/epilogue.hpp>
