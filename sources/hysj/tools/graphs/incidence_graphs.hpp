#pragma once
#include<array>
#include<ranges>
#include<vector>

#include<hysj/tools/functional/always.hpp>
#include<hysj/tools/functional/apply.hpp>
#include<hysj/tools/graphs/directions.hpp>
#include<hysj/tools/graphs/elements.hpp>
#include<hysj/tools/graphs/incidence.hpp>
#include<hysj/tools/priority_tag.hpp>
#include<hysj/tools/ranges/cat.hpp>
#include<hysj/tools/ranges/map.hpp>
#include<hysj/tools/ranges/linear_span.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/tag_invoke.hpp>
#include<hysj/tools/types.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::graphs{

template<typename V = none,typename E = none>
struct incidence_graph{

  template<element_tag tag>
  using element_id = graphs::element_id<tag>;

  using vertex_value = V;
  using edge_value   = E;

  template<element_tag T>
  struct element{
    using value_type = std::conditional_t<T == vertex_tag(),V,E>;
    using port_type = std::conditional_t<T == vertex_tag(),std::vector<edge_id>,vertex_id>;

    std::array<port_type,2> ports;
    [[no_unique_address]] value_type value;

    friend HYSJ_MIRROR_STRUCT(element,(ports,value));

    constexpr auto &operator[](direction_tag d)      noexcept{ return ports[d]; }
    constexpr auto &operator[](direction_tag d)const noexcept{ return ports[d]; }

    constexpr auto &operator()()noexcept { return value; }
    constexpr auto &operator()()const noexcept { return value; }

    auto operator<=>(const element &) const = default;
  };

  template<element_tag C>
  struct elements{
    std::vector<element<C>> container;

    friend HYSJ_MIRROR_STRUCT(elements,(container));
    
    constexpr auto &operator[](element_id<C> i)     { return container.at(i); }
    constexpr auto &operator[](element_id<C> i)const{ return container.at(i); }

    constexpr auto operator()()const{
      return views::map(
        views::indices(std::ranges::size(container)),
        [](auto i){ return element_id<C>{i}; });
    }
    auto operator<=>(const elements &) const = default;
  };

  
  elements<vertex_tag> vertices;
  elements<edge_tag>   edges;
  friend HYSJ_MIRROR_STRUCT(incidence_graph,(vertices,edges));

  auto operator<=>(const incidence_graph &) const = default;

  using am_incidence_graph = void;

  constexpr auto &operator()(vertex_id i)     { return vertices[i]; }
  constexpr auto &operator()(vertex_id i)const{ return vertices[i]; }
  constexpr auto &operator()(edge_id i)     { return edges[i]; }
  constexpr auto &operator()(edge_id i)const{ return edges[i]; }

  constexpr auto &operator[](vertex_id i)     { return operator()(i)(); }
  constexpr auto &operator[](vertex_id i)const{ return operator()(i)(); }
  constexpr auto &operator[](edge_id i)     { return operator()(i)(); }
  constexpr auto &operator[](edge_id i)const{ return operator()(i)(); }

  static auto make(natural order = 0,natural size = 0){
    incidence_graph g{};
    g.vertices.container.resize(order);
    g.edges.container.resize(size);
    return g;
  }
  vertex_id vertex(auto &&... x){
    vertices.container.push_back({
        .ports = {},
        .value{fwd(x)...}
      });
    return {vertices.container.size()-1};
  }

  auto edge(vertex_id v_tail, vertex_id v_head,auto &&... arg){
    const edge_id e{edges.container.size()};
    edges.container.push_back({
      .ports{},
      .value{fwd(arg)...}
    });

    iset(e,v_tail);
    oset(e,v_head);
    return e;
  }

  void clear(){
    vertices.container.clear();
    edges.container.clear();
  }
  
  template<class D>
  auto set(D direction,edge_id edge,vertex_id vertex){
    auto old_vertex = std::exchange(edges[edge][direction],vertex);
    auto &vport = vertices[vertex][other(direction)];
    vport.push_back(edge);
    return old_vertex;
  }
  #define HYSJ_LAMBDA(D,d,...)                  \
    auto d##set(auto e,auto v){                 \
      return set(D##_tag,e,v);                  \
    }
  HYSJ_MAP_LAMBDA(HYSJ_GRAPHS_DIRECTIONS)
  #undef HYSJ_LAMBDA

  template<class D>
  auto unset(D direction,edge_id edge){
    auto vertex = std::exchange(edges[edge][direction],vertex_id::max());
    auto &vport = vertices[vertex][other(direction)];
    auto eit = std::ranges::find(vport,edge);
    HYSJ_ASSERT(eit != std::ranges::end(vport));
    vport.erase(eit);
    return vertex;
  }
  #define HYSJ_LAMBDA(D,d,...)                    \
    auto d##unset(auto e,auto v){                 \
      return unset(D##_tag,e,v);                  \
    }
  HYSJ_MAP_LAMBDA(HYSJ_GRAPHS_DIRECTIONS)
  #undef HYSJ_LAMBDA
  
  template<class D>
  auto reset(D direction,edge_id edge,vertex_id vertex){
    auto old_vertex = edges[edge][direction];
    if(old_vertex != vertex && old_vertex != vertex_id::max())
      unset(direction,edge);
    set(direction,edge,vertex);
    return old_vertex;
  }
  #define HYSJ_LAMBDA(D,d,...)                    \
    auto d##reset(auto e,auto v){                 \
      return reset(D##_tag,e,v);                  \
    }
  HYSJ_MAP_LAMBDA(HYSJ_GRAPHS_DIRECTIONS)
  #undef HYSJ_LAMBDA

  auto find_edge(vertex_id v_0,vertex_id v_1)const{
    return std::ranges::find_if(
      edges.container,
      [v = std::array{v_0,v_1}](const auto &e){
        return e.ports == v;
      });
  }
  bool has_edge(vertex_id v_0,vertex_id v_1)const{
    return find_edge(v_0,v_1) != std::ranges::end(edges.container);
  }

  edge_id try_edge(vertex_id v_0,vertex_id v_1,auto f = always(E{})){
    const auto it = find_edge(v_0,v_1);
    if(it == std::ranges::end(edges.container))
      return edge(v_0,v_1,f());
    return { //FIXME: bad - jeh
      (natural)std::ranges::distance(std::ranges::begin(edges.container),it)
    };
  }

  //FIXME:
  //  clean up / remove - jeh
  template<element_tag c>
  auto swap(constant_t<c> C,auto i0,auto i1){
    if(i0 == i1)
      return;

    std::array<element_id<C>,2> I{i0,i1};
    for(std::size_t i = 0;i != 2;++i)
      if constexpr(c == graphs::vertex_tag)
        std::ranges::for_each(
          enumerator_values<direction_tag>,
          [&](auto d){
            for(auto j:vertices[I[i]][d])
              edges[j][other(d)] = I[(i + 1)%2];
          });
      else{
        std::vector<element_id<other(C)>> vertex_visited;
        std::ranges::for_each(
          enumerator_values<direction_tag>,
          [&](auto d){
            auto j = edges[I[i]][d];
            if(ranges::contains(vertex_visited,j))
              return;
            vertex_visited.push_back(j);
            for(auto &k:vertices[j][other(d)])
              for(natural l = 0;l != 2;++l)
                if(k == I[l]){
                  k = I[(l+1)%2];
                  break;
                }
          });
      }
    std::swap(g(i0),g(i1));
  }

  auto incident(edge_tag_type,undirected_tag_type,edge_id i)const{
    auto &P = edges[i].ports;
    return std::span<const vertex_id,enumerator_count<direction_tag>>{
      std::ranges::begin(P),
      std::ranges::end(P)
    };
  }

  auto incident(edge_tag_type,undirected_tag_type d,auto i) const{
    auto s = views::linear_span(incident(edge_tag,d,undirect(i)),
                                views::dynamic_ptrdiff{1});
    if(i.direction == out_tag())
      return s;
    return reverse(s);
  }
  
  auto incident(edge_tag_type,auto d,auto i) const{
    return views::one(incident(edge_tag,undirected_tag,i)[index(d)]);
  }
  
  template<typename D>
  auto incident(vertex_tag_type,D d,auto i) const{
    if constexpr(graphs::concepts::direction_constant<D>)
      return views::map(std::span{vertices[i][d]},
                               [](auto e){
                                 return static_edge_id<D{}>{e};
                               });
    else if constexpr(graphs::concepts::direction<D>)
      return views::map(std::span{vertices[i][d]},
                               [d=d](auto e){
                                 return dynamic_edge_id{e,d};
                               });
    else
      return apply(
        [&](auto... direction){
          return views::cat(incident(vertex_tag,direction,i)...);
        },
        enumerator_values<direction_tag>);
  }

};

} //hysj::graphs
#include<hysj/tools/epilogue.hpp>
