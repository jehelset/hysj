#pragma once
#include<string_view>

#include<fmt/format.h>

#include<hysj/tools/reflection.hpp>
#include<hysj/tools/types.hpp>
#include<hysj/tools/utility.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::graphs{

#define HYSJ_GRAPHS_DIRECTIONS \
  (in,i)                       \
  (out,o)
#define HYSJ_GRAPHS_UNDIRECTED_TAG \
  (undirected,u)

#define HYSJ_GRAPHS_DIRECTIONAL_TAGS \
  HYSJ_JOIN(HYSJ_GRAPHS_DIRECTIONS,  \
            HYSJ_GRAPHS_UNDIRECTED_TAG)

enum direction_tag : bool { HYSJ_SPLAT(0,HYSJ_GRAPHS_DIRECTIONS) };
HYSJ_MIRROR_ENUM(direction_tag,HYSJ_GRAPHS_DIRECTIONS)

#define HYSJ_LAMBDA(D,...)                           \
  using D##_tag_type = constant_t<direction_tag::D>; \
  inline constexpr D##_tag_type D##_tag{};
HYSJ_MAP_LAMBDA(HYSJ_GRAPHS_DIRECTIONS)
#undef HYSJ_LAMBDA

constexpr auto index(direction_tag d)noexcept{
  return to_underlying(d);
}
template<direction_tag D>
constexpr auto index(constant_t<D> d)noexcept{
  return to_underlying(d);
}

constexpr auto other(direction_tag d)noexcept{
  if(d == in_tag)
    return out_tag();
  return in_tag();
}
template<direction_tag D>
constexpr auto other(constant_t<D>)noexcept{
  return constant_c<other(D)>;
}

enum class undirected_monotag : bool { HYSJ_SPLAT(0,HYSJ_GRAPHS_UNDIRECTED_TAG) };
HYSJ_MIRROR_ENUM(undirected_monotag,HYSJ_GRAPHS_UNDIRECTED_TAG)

#define HYSJ_LAMBDA(D,...)             \
  using D##_tag_type = constant_t<undirected_monotag::D>; \
  inline constexpr D##_tag_type D##_tag{};
HYSJ_MAP_LAMBDA(HYSJ_GRAPHS_UNDIRECTED_TAG)
#undef HYSJ_LAMBDA

namespace concepts{

  template<typename T>
  concept direction_constant = is_constant_of<uncvref_t<T>,direction_tag>;

  template<typename T>
  concept undirected_constant = is_constant_of<uncvref_t<T>,undirected_monotag>;

  template<typename T>
  concept directional_constant = direction_constant<T> || undirected_constant<T>;

  template<typename T>
  concept direction = std::same_as<uncvref_t<T>,direction_tag>;

  template<typename T>
  concept directed = direction_constant<T> || direction<T>;

  template<typename T>
  concept directional = directional_constant<T> || direction<T>;

} //concepts

} //hysj::graphs
HYSJ_ENUM_FORMATTER(hysj::_HYSJ_VERSION_NAMESPACE::graphs::direction_tag);
HYSJ_ENUM_FORMATTER(hysj::_HYSJ_VERSION_NAMESPACE::graphs::undirected_monotag);
#include<hysj/tools/epilogue.hpp>
