#pragma once
#include<concepts>
#include<functional>

#include<hysj/tools/graphs/directions.hpp>
#include<hysj/tools/graphs/elements.hpp>
#include<hysj/tools/graphs/incidence.hpp>
#include<hysj/tools/ranges/keep.hpp>
#include<hysj/tools/functional/bind.hpp>
#include<hysj/tools/functional/unwrap.hpp>
#include<hysj/tools/tag_invoke.hpp>
#include<hysj/tools/types.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::graphs{

namespace hooks{
  
  inline constexpr
  struct degree_fn{

    friend constexpr auto
    tag_invoke(degree_fn,auto d,const auto &g,auto i)
      arrow((std::ranges::distance(graphs::vincident(d,g,i))))

    constexpr auto
    operator()(auto d,const auto &g,auto i)const
      arrow((tag_invoke(*this,d,unwrap(g),i))
            (-> std::convertible_to<natural>))

  }
    degree{};
  
} //hooks

using hooks::degree;

#define HYSJ_LAMBDA(D,d)                         \
  inline constexpr auto d##degree =              \
    bind<>(graphs::degree,D##_tag);
HYSJ_MAP_LAMBDA(HYSJ_GRAPHS_DIRECTIONAL_TAGS)
#undef HYSJ_LAMBDA

inline constexpr struct is_skin_fn{
  
  template<typename D,typename G>
  requires concepts::directional_incidence_graph<G,D>
  auto operator()(D d,const G &g,auto v)const
    requires (requires{ graphs::degree(d,g,v); }){
    return graphs::degree(d,g,v) == 0;
  }

}
  is_skin{};

#define HYSJ_LAMBDA(D,d,...)                     \
  inline constexpr auto is_##d##skin =           \
    bind<>(graphs::is_skin,D##_tag);
HYSJ_MAP_LAMBDA(HYSJ_GRAPHS_DIRECTIONAL_TAGS)
#undef HYSJ_LAMBDA

inline constexpr struct skin_fn{

  static auto P(auto d,const auto &g){
    return bind<>(graphs::is_skin,d,std::cref(g));
  }
  
  template<typename D,typename G>
  requires concepts::directional_incidence_graph<G,D>
  auto operator()(D d,const G &g) const {
    return views::keep(
      graphs::vertex_ids(g),
      P(d,g));
  }
    
}
  skin{};

#define HYSJ_LAMBDA(D,d,...)                  \
  inline constexpr auto d##skin =             \
    bind<>(graphs::skin,D##_tag);
HYSJ_MAP_LAMBDA(HYSJ_GRAPHS_DIRECTIONAL_TAGS)
#undef HYSJ_LAMBDA


} //hysj::graphs
#include<hysj/tools/epilogue.hpp>
