#pragma once
#include<utility>
#include<variant>
#include<type_traits>

#include<hysj/tools/functional/bind.hpp>
#include<hysj/tools/functional/ignore.hpp>
#include<hysj/tools/functional/overload.hpp>
#include<hysj/tools/functional/unwrap.hpp>
#include<hysj/tools/graphs/directions.hpp>
#include<hysj/tools/priority_tag.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/reflection.hpp>
#include<hysj/tools/tag_invoke.hpp>
#include<hysj/tools/types.hpp>
#include<hysj/tools/utility.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::graphs{

#define HYSJ_GRAPH_ELEMENTS \
  (vertex,v,vertices)       \
  (edge,e,edges)

enum class element_tag : unsigned { HYSJ_SPLAT(0,HYSJ_GRAPH_ELEMENTS) };
HYSJ_MIRROR_ENUM(element_tag,HYSJ_GRAPH_ELEMENTS)

#define HYSJ_LAMBDA(T,...)                         \
  using T##_tag_type = constant_t<element_tag::T>; \
  inline constexpr auto T##_tag = constant_c<element_tag::T>;
HYSJ_MAP_LAMBDA(HYSJ_GRAPH_ELEMENTS)
#undef HYSJ_LAMBDA

#define HYSJ_LAMBDA(T,...)                                              \
  inline constexpr auto T##_tag_index = constant_c<to_underlying(element_tag::T)>;
HYSJ_MAP_LAMBDA(HYSJ_GRAPH_ELEMENTS)
#undef HYSJ_LAMBDA

constexpr auto other(element_tag e)noexcept{
  return e == vertex_tag ? edge_tag() : vertex_tag();
}

template<element_tag E>
constexpr auto other(constant_t<E>)noexcept{
  return constant_c<other(E)>;
}

namespace traits{

  template<typename G,element_tag T>
  struct element_id{
    using type = typename uncvref_t<G>::template element_id<T>;
  };

}
template<typename G,element_tag T>
using element_id_t = typename traits::element_id<G,T>::type;

#define HYSJ_LAMBDA(T,...) template<typename G> using T##_id_t = element_id_t<G,T##_tag>;
HYSJ_MAP_LAMBDA(HYSJ_GRAPH_ELEMENTS)
#undef HYSJ_LAMBDA

namespace concepts{

  template<typename I,typename G,element_tag T>
  concept element_id_convertible = std::convertible_to<I,element_id_t<G,T>>;

  #define HYSJ_LAMBDA(T,...)                                     \
    template<typename I,typename G>                                          \
    concept T##_id_convertible = element_id_convertible<I,G,element_tag::T>;
  HYSJ_MAP_LAMBDA(HYSJ_GRAPH_ELEMENTS)
  #undef HYSJ_LAMBDA
  
  template<typename R,typename G,element_tag T>
  concept element_id_range =
    (std::ranges::input_range<R>); //FIXME: - jeh
//     && element_id_convertible<std::ranges::range_value_t<R>,G,T>);

  #define HYSJ_LAMBDA(T,...)                         \
    template<typename R,typename G>                              \
    concept T##_id_range = element_id_range<R,G,element_tag::T>;
  HYSJ_MAP_LAMBDA(HYSJ_GRAPH_ELEMENTS)
  #undef HYSJ_LAMBDA
 
}

namespace hooks{

  inline constexpr
  struct direction_fn{
    
    friend constexpr auto
    tag_invoke(direction_fn,ignore_t,auto e)
      arrow((auto(e.direction)))
    
    constexpr auto
    operator()(const auto &g,auto e)const
      arrow((tag_invoke(*this,unwrap(g),e)))

  }
    direction{};
  
}
using hooks::direction;

namespace concepts{

  template<typename I,typename G>
  concept undirected = (requires(const G &g,I i){
      { graphs::direction(g,i) } -> concepts::undirected_constant;
    });
  
  template<typename I,typename G>
  concept static_edge = (requires(const G &g,I i){
      { graphs::direction(g,i) } -> concepts::direction_constant;
    });
  
  template<typename I,typename G>
  concept dynamic_edge = (requires(const G &g,I i){
      { graphs::direction(g,i) } -> concepts::direction;
    });
  
  template<typename I,typename G>
  concept directed_edge = static_edge<I,G> || dynamic_edge<I,G>;
  
  template<typename I,typename G>
  concept undirected_edge_id =
      edge_id_convertible<I,G> &&
      undirected<I,G>;

  template<typename R,typename G>
  concept undirected_edge_id_range = 
      (edge_id_range<R,G>
       && undirected_edge_id<std::ranges::range_value_t<R>,G>);

  template<typename I,typename G>
  concept dynamic_edge_id =
      edge_id_convertible<I,G> &&
      dynamic_edge<I,G>;

  template<typename R,typename G>
  concept dynamic_edge_id_range = 
      (edge_id_range<R,G>
       && dynamic_edge_id<std::ranges::range_value_t<R>,G>);

  template<typename I,typename G>
  concept static_edge_id =
      edge_id_convertible<I,G> &&
      static_edge<I,G>;

  template<typename R,typename G>
  concept static_edge_id_range = 
      (edge_id_range<R,G>
       && static_edge_id<std::ranges::range_value_t<R>,G>);

  
} //concepts

namespace hooks{

  inline constexpr
  struct element_ids_fn{

    friend constexpr auto
    ordered_tag_invoke(element_ids_fn,priority_tag<1>,auto t,const auto &g)
      arrow((g.element_ids(t))) 
    
    friend constexpr auto
    ordered_tag_invoke(element_ids_fn,
                       priority_tag<0>,
                       vertex_tag_type,
                       const auto &g)
      arrow((g.vertices()))

    friend constexpr auto
    ordered_tag_invoke(element_ids_fn,
                       priority_tag<0>,
                       edge_tag_type,
                       const auto &g)
      arrow((g.edges()))

    friend constexpr auto
    tag_invoke(element_ids_fn f,auto t,const auto &g)
      arrow((ordered_tag_invoke(f,priority_tag_v<1>,t,g)))

    template<element_tag T,typename G>
    constexpr concepts::element_id_range<G,T> auto
    operator()(constant_t<T> i,const G &g)const
      arrow((tag_invoke(*this,i,unwrap(g))))

  }
    element_ids{};
  
} //hooks

using hooks::element_ids;

#define HYSJ_LAMBDA(T,...)    \
  inline constexpr auto T##_ids =         \
    bind<>(element_ids,T##_tag);
HYSJ_MAP_LAMBDA(HYSJ_GRAPH_ELEMENTS)
#undef HYSJ_LAMBDA

namespace hooks{

  inline constexpr
  struct element_count_fn{

    friend constexpr auto
    ordered_tag_invoke(element_count_fn,priority_tag<1>,auto t,const auto &g)
      arrow((g.element_count(g)))

    //FIXME: weird bug with this, element_ids.size() != 0,
    //       but distance == 0.
    //       - jeh
    friend constexpr auto
    ordered_tag_invoke(element_count_fn,priority_tag<0>,auto t,const auto &g)
      arrow((std::ranges::distance(graphs::element_ids(t,g))))
    
    friend constexpr auto
    tag_invoke(element_count_fn fn,auto t,const auto &g)
      arrow((ordered_tag_invoke(fn,priority_tag<1>{},t,g)))

    template<element_tag T,typename G>
    constexpr auto
    operator()(constant_t<T> t,const G &g)const
      arrow((tag_invoke(*this,t,unwrap(g))))

  }
    element_count{};

} //hooks

using hooks::element_count;

#define HYSJ_LAMBDA(T,...)    \
  inline constexpr auto T##_count =       \
    bind<>(element_count,T##_tag);
HYSJ_MAP_LAMBDA(HYSJ_GRAPH_ELEMENTS)
#undef HYSJ_LAMBDA

inline constexpr auto order = vertex_count;
inline constexpr auto size  = edge_count;

namespace concepts{

  template<typename G,element_tag T>
  concept elements =
    (requires(const G &g){
      typename element_id_t<G,T>;
      graphs::element_ids(constant_t<T>{},g);
      graphs::element_count(constant_t<T>{},g);
    });

  #define HYSJ_LAMBDA(T,t,Ts,...) \
    template<typename G>                      \
    concept Ts = elements<G,T##_tag>;
  HYSJ_MAP_LAMBDA(HYSJ_GRAPH_ELEMENTS)
  #undef HYSJ_LAMBDA

} //concepts

namespace hooks{

  inline constexpr
  struct swap_fn{

    template<element_tag T,typename G,typename I>
    friend constexpr auto
    tag_invoke(swap_fn,constant_t<T> t,G &g,I i0,I i1)
      arrow((swap(t,g,i0,i1)))

    template<element_tag T,typename G,typename I>
    constexpr auto
    operator()(constant_t<T> t,G &g,I i0,I i1)const
      arrow((tag_invoke(*this,t,unwrap(g),i0,i1)))

  }
    swap{};

} //hooks

using hooks::swap;

#define HYSJ_LAMBDA(X,x,...)\
  inline constexpr auto x##swap = bind<>(swap,X##_tag);
HYSJ_MAP_LAMBDA(HYSJ_GRAPH_ELEMENTS)
#undef HYSJ_LAMBDA

template<element_tag T>
struct element_id{
  static constexpr auto tag = constant_c<T>;
  natural value;

  friend HYSJ_MIRROR_STRUCT(element_id,(value))

  constexpr operator natural()const noexcept{ return value; }

  auto operator<=>(const element_id &) const = default;

  static element_id max(){ return {std::numeric_limits<natural>::max()}; }
};

#define HYSJ_LAMBDA(element,...)      \
   using element##_id = element_id<element##_tag>;
HYSJ_MAP_LAMBDA(HYSJ_GRAPH_ELEMENTS)
#undef HYSJ_LAMBDA

template<char... digits>
constexpr auto operator"" _v()
  -> vertex_id
{ return {literal_combine(0_n, literal_parse(digits)...)}; }

template<char... digits>
constexpr auto operator"" _e()
  -> edge_id
{ return {literal_combine(0_n, literal_parse(digits)...)}; }

constexpr auto undirect(const edge_id &id)noexcept{
  return id;
}

struct dynamic_edge_id : edge_id{
  direction_tag direction;

  auto operator<=>(const dynamic_edge_id &) const = default;
  
  friend HYSJ_MIRROR_STRUCT(dynamic_edge_id,(direction)(edge_id))

  friend constexpr edge_id undirect(const dynamic_edge_id &did)noexcept{
    return did;
  }
};
using dedge_id = dynamic_edge_id;

template<direction_tag D>
struct static_edge_id:edge_id{
  static constexpr auto tag = edge_tag;
  static constexpr auto direction = constant_c<D>;

  auto operator<=>(const static_edge_id &) const = default;
  
  friend HYSJ_MIRROR_STRUCT(static_edge_id,()(edge_id))
  
  friend constexpr edge_id undirect(const static_edge_id &sid)noexcept{
    return sid;
  }
};

#define HYSJ_LAMBDA(D,d,...)                    \
  using D##_edge_id = static_edge_id<D##_tag>;
HYSJ_MAP_LAMBDA(HYSJ_GRAPHS_DIRECTIONS)
#undef HYSJ_LAMBDA

using any_element_id = std::variant<vertex_id,edge_id,dynamic_edge_id,out_edge_id,in_edge_id>;

} //hysj::graphs

template<hysj::_HYSJ_VERSION_NAMESPACE::graphs::element_tag c>
struct std::hash<hysj::_HYSJ_VERSION_NAMESPACE::graphs::element_id<c>>{
  std::size_t operator()(
    const hysj::_HYSJ_VERSION_NAMESPACE::graphs::element_id<c> &i)const{
    return std::hash<hysj::_HYSJ_VERSION_NAMESPACE::natural>{}(i.value);
  }
};
template<>
struct std::hash<hysj::_HYSJ_VERSION_NAMESPACE::graphs::dynamic_edge_id>{
  std::size_t operator()(
    const hysj::_HYSJ_VERSION_NAMESPACE::graphs::dynamic_edge_id &i)const{
    return std::hash<hysj::_HYSJ_VERSION_NAMESPACE::natural>{}(i.value);
  }
};
template<hysj::_HYSJ_VERSION_NAMESPACE::graphs::direction_tag d>
struct std::hash<hysj::_HYSJ_VERSION_NAMESPACE::graphs::static_edge_id<d>>{
  std::size_t operator()(
    const hysj::_HYSJ_VERSION_NAMESPACE::graphs::static_edge_id<d> &i)const{
    return std::hash<hysj::_HYSJ_VERSION_NAMESPACE::natural>{}(i.value);
  }
};

template<hysj::_HYSJ_VERSION_NAMESPACE::graphs::element_tag c>
struct fmt::formatter<hysj::_HYSJ_VERSION_NAMESPACE::graphs::element_id<c>>{
  constexpr auto parse(format_parse_context& context)const{
    auto it  = context.begin(),
         end = context.end();
     if(it != end && *it != '}')
       throw fmt::format_error("invalid format");
    return it;
  }
  auto format(const hysj::_HYSJ_VERSION_NAMESPACE::graphs::element_id<c> e,
              format_context &context)const{
    static constexpr char prefix =
      hysj::_HYSJ_VERSION_NAMESPACE::overload(
        [](hysj::_HYSJ_VERSION_NAMESPACE::graphs::vertex_tag_type){ return 'v'; },
        [](hysj::_HYSJ_VERSION_NAMESPACE::graphs::edge_tag_type  ){ return 'e'; })
      (e.tag);
      
    return fmt::format_to(context.out(),"{}{}",prefix,e.value);
  }
};

template<>
struct fmt::formatter<hysj::_HYSJ_VERSION_NAMESPACE::graphs::dynamic_edge_id>{
  using T = hysj::_HYSJ_VERSION_NAMESPACE::graphs::dynamic_edge_id;
  constexpr auto parse(format_parse_context& context)const{
    auto it  = context.begin(),
         end = context.end();
    if(it != end && *it != '}')
      throw fmt::format_error("invalid format");
    return it;
  }
  auto format(const T e,
              format_context &context)const{
    const char prefix =
      (e.direction == hysj::_HYSJ_VERSION_NAMESPACE::graphs::in_tag)
      ? 'i'
      : 'o'
      ;
    return fmt::format_to(context.out(),"{}{}",prefix,undirect(e));
  }
};
template<hysj::_HYSJ_VERSION_NAMESPACE::graphs::direction_tag D>
struct fmt::formatter<hysj::_HYSJ_VERSION_NAMESPACE::graphs::static_edge_id<D>>{
  using T = hysj::_HYSJ_VERSION_NAMESPACE::graphs::static_edge_id<D>;
  constexpr auto parse(format_parse_context& context)const{
    auto it  = context.begin(),
         end = context.end();
    if(it != end && *it != '}')
      throw fmt::format_error("invalid format");
    return it;
  }
  auto format(const T e,
              format_context &context)const{
    const char prefix =
      (e.direction == hysj::_HYSJ_VERSION_NAMESPACE::graphs::in_tag)
      ? 'i'
      : 'o'
      ;
    return fmt::format_to(context.out(),"{0}{1}",prefix,undirect(e));
  }
};

#include<hysj/tools/epilogue.hpp>
