#pragma once
#include<functional>
#include<utility>

#include<hysj/tools/functional/bind.hpp>
#include<hysj/tools/functional/none_of.hpp>
#include<hysj/tools/functional/unwrap.hpp>
#include<hysj/tools/functional/utility.hpp>
#include<hysj/tools/graphs/directions.hpp>
#include<hysj/tools/graphs/elements.hpp>
#include<hysj/tools/graphs/incidence.hpp>
#include<hysj/tools/priority_tag.hpp>
#include<hysj/tools/ranges/join.hpp>
#include<hysj/tools/ranges/keep.hpp>
#include<hysj/tools/ranges/map.hpp>
#include<hysj/tools/tag_invoke.hpp>
#include<hysj/tools/types.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::graphs{

#define HYSJ_GRAPHS_SEMIADJACENCY_MODES                           \
  HYSJ_PRODUCT((HYSJ_SELECT((0)(1),HYSJ_GRAPH_ELEMENTS))     \
               (HYSJ_GRAPHS_DIRECTIONAL_TAGS)                     \
               (HYSJ_GRAPHS_DIRECTIONAL_TAGS))

namespace hooks{
  
  inline constexpr
  struct semiadjacent_fn{

    friend constexpr auto
    ordered_tag_invoke(semiadjacent_fn,priority_tag<1>,auto t,auto d0,auto d1,const auto &g,auto v)
      arrow((semiadjacent(t,d0,d1,g,v)))

    //FIXME: no arrow because partial ordering bug in gcc - jeh
    template<element_tag T>
    friend constexpr auto
    ordered_tag_invoke(semiadjacent_fn,
                       priority_tag<0>,
                       constant_t<T>,auto d0,auto d1,const auto &g,auto v)
      noexcept(noexcept(
                 views::map(
                   graphs::vincident(d0,g,v),
                   bind<>(graphs::port,d1,std::cref(g)))))
      requires(
        T == vertex_tag
        && requires {
          views::map(
            graphs::vincident(d0,g,v),
            bind<>(graphs::port,d1,std::cref(g)));
        }){
      return views::map(
        graphs::vincident(d0,g,v),
        bind<>(graphs::port,d1,std::cref(g)));
    }
    
    //FIXME:
    //  make filter conditional based on directions
    //  - jeh
    //FIXME:
    //  gcc-segfault with constraints
    //  - jeh
    friend constexpr auto
    ordered_tag_invoke(semiadjacent_fn,
                       priority_tag<0>,
                       edge_tag_type,auto d0,auto d1,const auto &g,auto e)
      noexcept(noexcept(
                 views::join(
                   views::map(
                     graphs::eincident(d0,std::cref(g),e),
                     bind<>(graphs::vincident,d1,std::cref(g))))))
      -> decltype(auto)
    {
      return views::join(
          views::map(
            graphs::eincident(d0,std::cref(g),e),
            bind<>(graphs::vincident,d1,std::cref(g))));
    }
    
    friend constexpr auto
    tag_invoke(semiadjacent_fn fn,auto t,auto d0,auto d1,const auto &g,auto v)
      arrow((ordered_tag_invoke(fn,priority_tag<1>{},t,d0,d1,g,v)))

    template<element_tag T>
    constexpr auto
    operator()(constant_t<T> t,auto d0,auto d1,const auto &g,auto i)const
      arrow((tag_invoke(*this,t,d0,d1,unwrap(g),i)))
    
  }
    semiadjacent{};

} //hooks

using hooks::semiadjacent;

#define HYSJ_LAMBDA(X,x,D0,d0,D1,d1)                                 \
  inline constexpr auto x##d0##d1##semiadjacent =                    \
    bind<>(graphs::semiadjacent,X##_tag,D0##_tag,D1##_tag);
HYSJ_MAP_LAMBDA(HYSJ_GRAPHS_SEMIADJACENCY_MODES)
#undef HYSJ_LAMBDA

inline constexpr struct {
  constexpr auto
  operator()(auto t,auto d0,auto d1,const auto &g,auto i)const
    arrow((views::keep(semiadjacent(t,d0,d1,g,i),none_of(i))));
}
  exclusive_semiadjacent{};

#define HYSJ_LAMBDA(X,x,D0,d0,D1,d1)                                    \
  inline constexpr auto e##x##d0##d1##semiadjacent =                    \
    bind<>(graphs::exclusive_semiadjacent,X##_tag,D0##_tag,D1##_tag);
HYSJ_MAP_LAMBDA(HYSJ_GRAPHS_SEMIADJACENCY_MODES)
#undef HYSJ_LAMBDA

#define HYSJ_GRAPHS_ADJACENCY_MODES HYSJ_GRAPHS_INCIDENCE_MODES

namespace hooks{
  
  inline constexpr
  struct adjacent_fn{

    friend constexpr auto
    ordered_tag_invoke(adjacent_fn,priority_tag<1>,auto t,auto d,const auto &g,auto v)
      arrow((adjacent(t,d,g,v)))

    consteval static auto semiadjacency(vertex_tag_type,auto){
      return graphs::semiadjacent;
    }
    consteval static auto semiadjacency(edge_tag_type,auto d){
      if constexpr(concepts::undirected_constant<decltype(d)>)
        return graphs::exclusive_semiadjacent;
      else
        return graphs::semiadjacent;
    }
    consteval static auto semiadjacent_second_direction(vertex_tag_type,auto){
      return out_tag;
    }
    static auto semiadjacent_second_direction(edge_tag_type,auto d){
      return d;
    }

    template<element_tag T>
    friend constexpr auto
    ordered_tag_invoke(adjacent_fn,
                       priority_tag<0>,
                       constant_t<T> t,auto d,const auto &g,auto v)
      arrow((semiadjacency(t,d)
             (t,d,semiadjacent_second_direction(t,d),std::cref(g),v)))
    
    friend constexpr auto
    tag_invoke(adjacent_fn fn,auto t,auto d,const auto &g,auto v)
      arrow((ordered_tag_invoke(fn,priority_tag<1>{},t,d,g,v)))

    template<element_tag T>
    constexpr auto
    operator()(constant_t<T> t,auto d,const auto &g,auto i)const
      arrow((tag_invoke(*this,t,d,unwrap(g),i))
            (-> concepts::element_id_range<decltype(g),T>))
    
  }
    adjacent{};

} //hooks

using hooks::adjacent;

#define HYSJ_LAMBDA(X,x,...)                        \
  inline constexpr auto x##adjacent =               \
    bind<>(graphs::adjacent,X##_tag);
HYSJ_MAP_LAMBDA(HYSJ_GRAPH_ELEMENTS)
#undef HYSJ_LAMBDA

#define HYSJ_LAMBDA(X,x,D,d)                              \
  inline constexpr auto x##d##adjacent =                  \
    bind<>(graphs::adjacent,X##_tag,D##_tag);
HYSJ_MAP_LAMBDA(HYSJ_GRAPHS_ADJACENCY_MODES)
#undef HYSJ_LAMBDA

template<element_tag T,typename D,typename G,typename I>
using adjacent_t = decltype(graphs::adjacent(constant_c<T>,
                                             std::declval<D>(),
                                             std::declval<const G &>(),
                                             std::declval<I>()));

#define HYSJ_LAMBDA(X,x,D,d)                                    \
template<typename G,typename I = element_id_t<G,X##_tag>> \
using x##d##adjacent_t = adjacent_t<X##_tag,D##_tag_type,G,I>;
HYSJ_MAP_LAMBDA(HYSJ_GRAPHS_ADJACENCY_MODES)
#undef HYSJ_LAMBDA

namespace concepts{
  
  template<typename G,element_tag T,typename D,typename I = element_id_t<G,T>>
  concept adjacency_graph =
    (requires(constant_t<T> t,D d,const G &g,I i){
      graphs::adjacent(t,d,g,i);
    });

  #define HYSJ_LAMBDA(X,x,D,d)                                              \
    template<typename G>                                                    \
    concept x##d##adjacency_graph = adjacency_graph<G,X##_tag,D##_tag_type>;
  HYSJ_MAP_LAMBDA(HYSJ_GRAPHS_ADJACENCY_MODES)
  #undef HYSJ_LAMBDA

  #define HYSJ_LAMBDA(D,X,x,...) && adjacency_graph<G,X##_tag,D>
  template<typename G,typename D>
  concept directional_adjacency_graph = 
    true
    HYSJ_MAP(HYSJ_LAMBDA,D,HYSJ_GRAPH_ELEMENTS)
    ;
  #undef HYSJ_LAMBDA

} //concepts

} //hysj::graphs
#include<hysj/tools/epilogue.hpp>
