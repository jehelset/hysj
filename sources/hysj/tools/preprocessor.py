N = 100
M = 4

with open('sources/hysj/tools/preprocessor.hpp', 'w') as pp:

    pp.write('''#pragma once
#include<hysj/tools/prologue.hpp>

#define HYSJ_STRINGIFY_IMPL(X) #X
#define HYSJ_STRINGIFY(X) HYSJ_STRINGIFY_IMPL(X)

#define HYSJ_EXPAND(X) X
#define HYSJ_UNPAREN(...) __VA_ARGS__
#define HYSJ_UNWRAP(X) HYSJ_EXPAND(HYSJ_UNPAREN X)

#define HYSJ_CAT(a,b) HYSJ_CAT_IMPL(a,b)
#define HYSJ_CAT_IMPL(a, b) a ## b

#define HYSJ_JOIN(a,b) a b

#define HYSJ_BOOL(x) HYSJ_CAT(HYSJ_BOOL_,x)

''')

    for i in range(N + 1):
        pp.write(f'#define HYSJ_BOOL_{i} {0 if i == 0 else 1}\n')

    pp.write('''
#define HYSJ_FIRST(a,...) a

#define HYSJ_SIZE(sequence) HYSJ_CAT(HYSJ_SIZE_, HYSJ_SIZE_0 sequence)

''')

    for i in range(N):
        pp.write(f'#define HYSJ_SIZE_{i}(...) HYSJ_SIZE_{i + 1}\n')
    pp.write('\n')
    for i in range(N + 1):
        pp.write(f'#define HYSJ_SIZE_HYSJ_SIZE_{i} {i}\n')

    pp.write(
        '''
#define HYSJ_IF(condition,primary,secondary) HYSJ_CAT(HYSJ_IF_,condition)(primary,secondary)
#define HYSJ_IF_0(primary,secondary) secondary
#define HYSJ_IF_1(primary,secondary) primary

#define HYSJ_DROP(count,sequence)                               \\
  HYSJ_CAT(HYSJ_DROP_IMPL_,HYSJ_BOOL(count))(count,sequence) 
#define HYSJ_DROP_IMPL_1(count,sequence) \\
  HYSJ_CAT(HYSJ_DROP_,count) sequence
#define HYSJ_DROP_IMPL_0(count,sequence) sequence
    
''')

    pp.write('#define HYSJ_DROP_1(...)\n')
    for i in range(2, N + 1):
        pp.write(f'#define HYSJ_DROP_{i}(...) HYSJ_DROP_{i - 1}\n')

    pp.write('''
#define HYSJ_TAKE(count,sequence)                               \\
  HYSJ_CAT(HYSJ_TAKE_IMPL_,HYSJ_BOOL(count))(count,sequence) 
#define HYSJ_TAKE_IMPL_1(count,sequence) \\
  HYSJ_CAT(HYSJ_TAKE_,count) sequence)
#define HYSJ_TAKE_IMPL_0(count,sequence)

''')

    pp.write('#define HYSJ_TAKE_1(...) (__VA_ARGS__) HYSJ_SWALLOW(\n')
    for i in range(2, N + 1):
        pp.write(f'#define HYSJ_TAKE_{i}(...) (__VA_ARGS__) HYSJ_TAKE_{i - 1}\n')

    pp.write('''
#define HYSJ_HEAD(sequence) HYSJ_TAKE(1,sequence)
#define HYSJ_TAIL(sequence) HYSJ_DROP(1,sequence)

#define HYSJ_AT(index,sequence) HYSJ_HEAD(HYSJ_DROP(index,sequence))

#define HYSJ_NOT(boolean) HYSJ_IF(boolean,0,1)

#define HYSJ_IS_NOT_EMPTY(sequence) HYSJ_BOOL(HYSJ_SIZE(sequence))
#define HYSJ_IS_EMPTY(sequence) HYSJ_NOT(HYSJ_BOOL(HYSJ_SIZE(sequence)))

#define HYSJ_SWALLOW(x)

#define HYSJ_MAP(macro,data,sequence) HYSJ_CAT(HYSJ_MAP_IMPL_,HYSJ_SIZE(sequence))(macro,data,sequence)
#define HYSJ_MAP_VA(macro,data,...) HYSJ_MAP(macro,data,HYSJ_SEQUENCE(__VA_ARGS__))

''')

    for i in range(M):
        suffix = '' if i == 0 else f'_{i}'
        pp.write(f'''
#define HYSJ_MAP{suffix}(macro,data,sequence) HYSJ_CAT(HYSJ_MAP{suffix}_IMPL_,HYSJ_SIZE(sequence))(macro,data,sequence)
#define HYSJ_MAP{suffix}_IMPL_CALL(macro,data,head) HYSJ_MAP{suffix}_IMPL_CALL_1(macro,data,HYSJ_UNPAREN head)

#define HYSJ_MAP{suffix}_IMPL_CALL_1(macro,data,head) HYSJ_MAP{suffix}_IMPL_CALL_2(macro,data,head)
#define HYSJ_MAP{suffix}_IMPL_CALL_2(macro,data,...) macro(data,__VA_ARGS__)
        
''')

        pp.write(f'#define HYSJ_MAP{suffix}_IMPL_0(macro,data,sequence)\n')
        for j in range(1, N + 1):
            pp.write(
                f'''#define HYSJ_MAP{suffix}_IMPL_{j}(macro,data,sequence)             \\
  HYSJ_MAP{suffix}_IMPL_CALL(macro,data,HYSJ_HEAD(sequence))     \\
  HYSJ_MAP{suffix}_IMPL_{j - 1}(macro,data,HYSJ_TAIL(sequence))
''')

    pp.write(
        '''#define HYSJ_PRODUCT(sequences)                                 \\
  HYSJ_CAT(HYSJ_PRODUCT_IMPL_,HYSJ_SIZE(sequences))(sequences)

#define HYSJ_PRODUCT_MAP_F(right_something,...) \\
  (__VA_ARGS__,HYSJ_UNWRAP(right_something))
#define HYSJ_PRODUCT_MAP(left_sequence,...)                     \\
  HYSJ_MAP(HYSJ_PRODUCT_MAP_F,(__VA_ARGS__),left_sequence)
 ''')

    pp.write(f'''#define HYSJ_PRODUCT_IMPL_0(sequences)

#define HYSJ_PRODUCT_IMPL_1(sequences) \\
  HYSJ_UNWRAP(HYSJ_HEAD(sequences))

#define HYSJ_PRODUCT_IMPL_2(sequences)                         \\
  HYSJ_MAP_1(HYSJ_PRODUCT_MAP,                                 \\
             HYSJ_PRODUCT_IMPL_1(HYSJ_TAKE(1,sequences)),      \\
             HYSJ_UNWRAP(HYSJ_DROP(1,sequences)))

#define HYSJ_PRODUCT_IMPL_3(sequences)                         \\
  HYSJ_MAP_1(HYSJ_PRODUCT_MAP,                                 \\
             HYSJ_PRODUCT_IMPL_2(HYSJ_TAKE(2,sequences)),      \\
             HYSJ_UNWRAP(HYSJ_DROP(2,sequences)))
  
#define HYSJ_PRODUCT_IMPL_4(sequences)                         \\
  HYSJ_MAP_1(HYSJ_PRODUCT_MAP,                                 \\
             HYSJ_PRODUCT_IMPL_3(HYSJ_TAKE(3,sequences)),      \\
             HYSJ_UNWRAP(HYSJ_DROP(3,sequences)))

''')

    pp.write(f'#define HYSJ_EXTRACT({", ".join(f"_{i}" for i in range(N))}, V, ...) V\n\n')

    pp.write(f'#define HYSJ_VA_COUNT(...) HYSJ_EXTRACT(__VA_ARGS__ __VA_OPT__(,) {",".join(str(i) for i in reversed(range(N + 1)))})\n\n')

    pp.write('#define HYSJ_SEQUENCE(...) HYSJ_CAT(HYSJ_SEQUENCE_IMPL_,HYSJ_VA_COUNT(__VA_ARGS__))(__VA_ARGS__)\n\n')

    for i in range(N + 1):
        pp.write(f'#define HYSJ_SEQUENCE_IMPL_{i}(' +
                 ','.join(f'x{j}' for j in range(i)) + ') ' +
                 ''.join(f'(x{j})' for j in range(i)) + '\n')

    pp.write('''#define HYSJ_FOR_EACH(F, a, ...)                \\
  HYSJ_MAP(F,a,HYSJ_SEQUENCE(__VA_ARGS__))

#define HYSJ_LAMBDA_IMPL(_,...) HYSJ_LAMBDA(__VA_ARGS__)
#define HYSJ_MAP_LAMBDA(X) HYSJ_MAP_3(HYSJ_LAMBDA_IMPL,_,X)

#define HYSJ_SPLAT_F(i,...) ,HYSJ_UNWRAP(HYSJ_AT(i,HYSJ_SEQUENCE(__VA_ARGS__)))
#define HYSJ_SPLAT_G(_) HYSJ_SPLAT_H(_)
#define HYSJ_SPLAT_H(_,...) __VA_ARGS__
#define HYSJ_SPLAT(i,x)                         \\
  HYSJ_SPLAT_G(_ HYSJ_MAP(HYSJ_SPLAT_F,i,x))

#define HYSJ_PROJECT_F(i,...) HYSJ_AT(i,HYSJ_SEQUENCE(__VA_ARGS__))
#define HYSJ_PROJECT(i,x)                       \\
  HYSJ_MAP(HYSJ_PROJECT_F,i,x)

#define HYSJ_SELECT_F(I,...)\\
  HYSJ_SELECT_H(HYSJ_MAP_1(HYSJ_SELECT_G,HYSJ_SEQUENCE(__VA_ARGS__),I))
#define HYSJ_SELECT_G(S,i) , HYSJ_UNWRAP(HYSJ_AT(i,S))
#define HYSJ_SELECT_H(_) HYSJ_SELECT_I(_)
#define HYSJ_SELECT_I(_,...)                    \\
  (__VA_ARGS__)
#define HYSJ_SELECT(I,x) \\
  HYSJ_MAP(HYSJ_SELECT_F,I,x)

#include<hysj/tools/epilogue.hpp>
''')
