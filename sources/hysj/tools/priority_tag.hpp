#pragma once
#include<cstdint>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

template<std::size_t N> struct priority_tag:priority_tag<N - 1>{};
template<> struct priority_tag<0ull>{};

template<std::size_t N>
constexpr priority_tag<N> priority_tag_v{};

} //hysj
#include<hysj/tools/epilogue.hpp>
