#pragma once
#include<coroutine>
#include<type_traits>

#include<hysj/tools/functional/bind.hpp>
#include<hysj/tools/functional/unwrap.hpp>
#include<hysj/tools/preprocessor.hpp>
#include<hysj/tools/tag_invoke.hpp>
#include<hysj/tools/types.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::coroutines{

namespace traits{
  template<typename Get>
  struct promise{
    using type = typename uncvref_t<unwrap_t<Get>>::promise_type;
  };
}
template<typename Get>
using promise_t = typename traits::promise<Get>::type; 

template<typename Get>
using api_t = decltype(std::declval<promise_t<Get>>().get_return_object());

namespace traits{

  template<typename Get>
  struct depth{
    static constexpr integer value = uncvref_t<Get>::depth;
  };
  
}

template<typename Get>
constexpr integer depth_v = traits::depth<Get>::value;

namespace _{
  template<typename Promise>
  constexpr auto promise_awaiter()noexcept{
    struct Awaiter{
      Promise *promise;

      static constexpr bool await_ready()noexcept{ return false; }
      constexpr std::coroutine_handle<> await_suspend(
        std::coroutine_handle<Promise> handle)noexcept{
        promise = std::addressof(handle.promise());
        return handle;
      }
      constexpr auto &await_resume()noexcept{ return *promise; }
    };
    return Awaiter{};
  }
}

#define HYSJ_COROUTINES_PROMISE(...)\
  co_await ::hysj::coroutines::_::promise_awaiter<::hysj::coroutines::promise_t<__VA_ARGS__>>()
#define HYSJ_CO(...) HYSJ_COROUTINES_PROMISE(__VA_ARGS__)

namespace hooks{

  none get();
  
  template<typename T>
  struct get_fn{

    friend constexpr auto
    tag_invoke(get_fn,auto &promise,auto &&...arg)
      arrow((get<T>(promise,fwd(arg)...)))

    constexpr auto
    operator()(auto &&promise,auto &&... arg)const
      arrow((tag_invoke(*this,unwrap(promise),fwd(arg)...)))
    
  };

} //hooks

template<typename T = none>
inline constexpr hooks::get_fn<T> get{};

namespace hooks{

  none pipe();
  
  struct pipe_fn{

    friend constexpr auto
    tag_invoke(pipe_fn,auto &promise,auto &&...arg)
      arrow((pipe(promise,fwd(arg)...)))

    constexpr auto
    operator()(auto &&promise,auto &&... arg)const
      arrow((tag_invoke(*this,unwrap(promise),fwd(arg)...)))
     
  };

} //hooks

inline constexpr hooks::pipe_fn pipe{};

namespace hooks{

  none promise();
  
  struct promise_fn{

    friend constexpr auto
    tag_invoke(promise_fn,auto &get)
      arrow((promise(get)))

    constexpr auto
    operator()(auto &&get)const
      arrow((tag_invoke(*this,unwrap(get))))

  };

} //hooks

inline constexpr hooks::promise_fn promise{};

namespace hooks{

  none ancestor();
  
  template<int I>
  struct ancestor_fn{

    friend constexpr auto
    tag_invoke(ancestor_fn,auto &promise)
      arrow((ancestor<I>(promise)))

    constexpr auto
    operator()(auto &&promise)const
      arrow((tag_invoke(*this,unwrap(promise))))
    
  };
  
} //hooks

template<int I = 0>
inline constexpr hooks::ancestor_fn<I> ancestor{};

inline constexpr hooks::ancestor_fn< 0> parent{};
inline constexpr hooks::ancestor_fn<-1> root{};

namespace hooks{

  none put();
  
  template<int I,bool F>
  struct put_fn{

    friend constexpr auto
    tag_invoke(put_fn,auto &putable,auto &&... values)
      arrow((put<I,F>(putable,fwd(values)...)))

    constexpr auto
    operator()(auto &&putable,auto &&...values)const
      arrow((tag_invoke(*this,unwrap(putable),fwd(values)...)))

  };

} //hooks

template<int I = 1,bool F = false>
inline constexpr hooks::put_fn<I,F> put{};

template<int I = 1>
inline constexpr auto ret = put<I,true>;
template<int I = 1>
inline constexpr auto nil = bind<-1>(ret<I>,std::nullopt);

} //hysj::coroutines
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

namespace co = coroutines;

} // hysj
#include<hysj/tools/epilogue.hpp>
