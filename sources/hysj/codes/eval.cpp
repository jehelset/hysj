#include<hysj/codes/eval.hpp>

#include<optional>
#include<vector>

#include<hysj/codes/code.hpp>
#include<hysj/codes/grammar.hpp>
#include<hysj/codes/sets.hpp>
#include<hysj/grammars.hpp>
#include<hysj/tools/functional/any_of.hpp>
#include<hysj/tools/functional/bind.hpp>
#include<hysj/tools/functional/compose.hpp>
#include<hysj/tools/functional/optional.hpp>
#include<hysj/tools/functional/ignore.hpp>
#include<hysj/tools/functional/pick.hpp>
#include<hysj/tools/types.hpp>
 
#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

inline constexpr auto evalbad =
  any_of(optag::typ,  optag::itr,  optag::con,  optag::rfl,  optag::var,
         optag::cur,  optag::rot,  optag::der,  optag::put,
         optag::then, optag::noop, optag::loop, optag::exit, optag::fork,
         optag::when, optag::on,   optag::to,   optag::dev,  optag::api);

inline constexpr auto evalcst =
  any_of(optag::lit, optag::cst);

struct evalctx{
  const codes::code &code;
  std::vector<std::optional<anycell>> stack;

  template<optag O>
  requires (O == evalbad)
  auto pre_impl(sym<O>){
    stack.push_back(std::nullopt);
    return false;
  }
  template<optag O>
  requires (O == evalcst)
  auto pre_impl(sym<O> o){
    with_set(
      [&](auto s){
        stack.push_back(
          anycell{
            std::in_place_index<etoi(s())>,
            csteval(s, symdat(code, o)).value()});
      },
      oset(code, o));
    return false;
  }
  template<optag O>
  auto pre_impl(sym<O>){
    return true;
  }
  auto pre(id o){
    return codes::with_op(lift((pre_impl)(&)),o);
  }
  auto pre(id,id r,id a){
    if(reltag_of(r) == any_of(reltag::dom, reltag::dim)){
      stack.push_back(std::nullopt);
      return false;
    }
    return pre(a);
  }

  template<optag O>
  requires (O == evalbad || O == evalcst)
  auto post_impl(sym<O>){
    return none{};
  }
  template<optag O>
  auto post_impl(sym<O> o){

    const auto N = grammars::argdeg(code, o);
    if(stack.size() < (std::size_t)N)
      throw std::runtime_error("invalid stack");
    auto Ae = std::span{stack}.last(N);

    auto make_anycell = [&](auto s,auto v){
      return anycell{
        std::in_place_index<etoi(s())>,
        v
      };
    };

    auto cvt = [&](const anycell &s,set tt,set st) -> anycell{
      return codes::with_set(
        [&](auto tt, auto st) -> anycell{
          using c = cell<tt()>;
          const auto &sc = std::get<etoi(st())>(s);
          if constexpr(requires{ static_cast<c>(sc); }){
            auto t = static_cast<c>(sc);
            if constexpr(tt() == set::booleans)
              return make_anycell(tt, t == 0 ? c{0} : c{1});
            else
              return make_anycell(tt, t);
          }
          else
            throw std::bad_cast();
        },
        tt,
        st);
    };

    auto acell = [&](auto t, std::size_t i){
      if(!Ae[i])
        return no<cell<t()>>;
      const auto &ae = Ae[i];
      if(set_of(*ae) != t())
        return no<cell<t()>>;
      return just(std::get<etoi(t())>(*ae));
    };

    for(auto &&[ae, r]:std::views::zip(Ae, grammars::relids(code, o.id()))){
      if(!ae)
        continue;
      auto rt = codes::rset(code, r);
      auto at = codes::oset(code, grammars::relargid(code, r));
      *ae = cvt(*ae, rt, at);
    }

    if constexpr(O == optag::cvt)
      std::swap(Ae[0], Ae[1]);
    else if constexpr(O == any_of(optag::neg, optag::sin, optag::bnot, optag::lnot, optag::fct)){
      auto t = codes::oset(code, o);
      Ae[0] = codes::with_set(
        [&](auto at) -> std::optional<anycell>{
          auto ac = acell(at, 0).value();
          if constexpr(O == optag::neg && requires { -ac; })
            return make_anycell(at, -ac);
          else if constexpr(O == optag::sin && requires { std::sin(ac); })
            return make_anycell(at, std::sin(ac));
          else if constexpr(O == optag::bnot && requires { std::bit_not{}(ac); })
            return make_anycell(at, std::bit_not{}(ac));
          else if constexpr(O == optag::lnot && requires { std::logical_not{}(ac); })
            return make_anycell(at, std::logical_not{}(ac));
          else if constexpr(O == optag::fct && is_integral(at())){
            auto c = ac;
            if(c < 0)
              return std::nullopt;
            auto r = 1;
            while(c != 0)
              r *= c--;
            return make_anycell(at, r);
          }
          return std::nullopt;
        },
        t);
    }
    else if constexpr(O == any_of(optag::div, optag::log, optag::blsh, optag::brsh)){
      auto [lhs, rhs] = grammars::rels(code, o);

      Ae[0] = codes::with_set(
        [&](auto ot) -> std::optional<anycell> {
          auto lhsc = acell(ot, 0).value();
          auto rhsc = acell(ot, 1).value();

          if constexpr(O == optag::div && is_arithmetic(ot()))
            return make_anycell(ot, lhsc / rhsc);
          if constexpr(O == optag::pow && is_arithmetic(ot())){
            if constexpr(is_integral_arithmetic(ot())){
              auto c = rhsc;
              cell<ot()> r = 1;
              if(c < 0)
                return std::nullopt;
              while(c != 0)
                r *= lhsc;
              return make_anycell(ot, r);
            }
            else
              return make_anycell(ot, std::pow(lhsc, rhsc));
          }
          if constexpr(O == optag::log && ot() == set::reals)
            return make_anycell(ot, std::log(lhsc) / std::log(rhsc));
          if constexpr(O == optag::blsh && is_integral_arithmetic(ot()))
            return make_anycell(ot, lhsc << rhsc);
          if constexpr(O == optag::brsh && is_integral_arithmetic(ot()))
            return make_anycell(ot, lhsc >> rhsc);
          if constexpr(O == optag::eq)
            return make_anycell(ot, lhsc == rhsc);
          if constexpr(O == optag::lt)
            return make_anycell(ot, lhsc < rhsc);
          return std::nullopt;
        },
        codes::oset(code, o));
    }
    else if constexpr(O == any_of(optag::eq, optag::lt)){
      auto [lhs, rhs] = grammars::rels(code, o);

      Ae[0] = codes::with_set(
        [&](auto ot, auto rt) -> std::optional<anycell> {
          auto lhsc = acell(rt, 0).value();
          auto rhsc = acell(rt, 1).value();

          if constexpr(O == optag::eq)
            return make_anycell(ot, lhsc == rhsc);
          if constexpr(O == optag::lt)
            return make_anycell(ot, lhsc < rhsc);
          return std::nullopt;
        },
        codes::oset(code, o),
        codes::rset(code, rhs));

    }
    else if constexpr(O == any_of(optag::mod, optag::pow)){
      auto [lhs, rhs] = grammars::rels(code, o);

      Ae[0] = codes::with_set(
        [&](auto ot, auto rhst) -> std::optional<anycell> {
          auto lhsc = acell(ot, 0).value();
          auto rhsc = acell(rhst, 1).value();
          if constexpr(O == optag::mod && is_arithmetic(ot())){
            //FIXME: (l % r + r) % r - jeh
            if constexpr(is_integral_arithmetic(ot()) && is_integral_arithmetic(rhst()))
              return make_anycell(ot, static_cast<cell<ot()>>(lhsc) % static_cast<cell<ot()>>(rhsc));
            else if constexpr(ot() == set::reals)
              return make_anycell(ot, std::fmod(lhsc, static_cast<rcell<>>(rhsc)));
          }
          if(O == optag::pow && is_arithmetic(ot())){
            if constexpr(is_integral_arithmetic(rhst())){
              auto c = rhsc;
              cell<ot()> r = 1;
              if(c < 0)
                return std::nullopt;
              while(c-- != 0)
                r *= lhsc;
              return make_anycell(ot, r);
            }
            else if(rhst() == set::reals)
              return make_anycell(ot, std::pow(static_cast<rcell<>>(lhsc), rhsc));
          }
          return std::nullopt;
        },
        codes::oset(code, o),
        codes::rset(code, rhs));
    }
    else if constexpr(O == any_of(optag::add, optag::mul, optag::bor, optag::band, optag::bxor,
                                  optag::land, optag::lor)){
      Ae[0] = codes::with_set(
        [&](auto ot) -> std::optional<anycell> {

          if constexpr((O == any_of(optag::add, optag::mul) && !is_arithmetic(ot()))
                       ||(O == any_of(optag::bor, optag::band, optag::bxor) && !is_integral_arithmetic(ot()))
                       ||(O == any_of(optag::land, optag::lor) && ot() != set::booleans))
            return std::nullopt;
          else{
            if(N == 0){
              if constexpr(O == optag::bxor)
                return std::nullopt;
              else if constexpr(O == any_of(optag::add, optag::bor, optag::lor))
                return make_anycell(ot, 0);
              else if constexpr(O == any_of(optag::mul, optag::land))
                return make_anycell(ot, 1);
              else{
                static_assert(O == optag::band);
                return make_anycell(ot, ~cell<ot()>{});
              }
            }

            auto oe = acell(ot, 0).value();

            constexpr auto op = []{
              if constexpr(O == optag::add)
                return std::plus<>{};
              else if constexpr(O == optag::mul)
                return std::multiplies<>{};
              else if constexpr(O == optag::bor)
                return std::bit_or<>{};
              else if constexpr(O == optag::band)
                return std::bit_and<>{};
              else if constexpr(O == optag::bxor)
                return std::bit_xor<>{};
              else if constexpr(O == optag::lor)
                return std::logical_or<>{};
              else{
                static_assert(O == optag::land);
                return std::logical_and<>{};
              }
            }();
            
            if(N == 0)
              throw std::runtime_error("invalid op");
            for(auto ai:views::indices(N - 1))
              oe = op(oe, acell(ot,1 + ai).value());
            return make_anycell(ot, oe);
          }
          return std::nullopt;
        },
        codes::oset(code, o));
    }
    else if constexpr(O == any_of(optag::sub)){
      Ae[0] = codes::with_set(
        [&](auto ot) -> std::optional<anycell> {
          auto oe = acell(ot, 0).value();
          for(auto ai:views::indices(N - 1))
            oe -= acell(ot,1 + ai).value();
          return make_anycell(ot, oe);
        },
        codes::oset(code, o));
    }
    else{
      static_assert(O == optag::sel);
      auto w = grammars::rel(code, o, 0_N);
      Ae[0] = codes::with_set(
        [&](auto ot,auto wt){
          return make_anycell(ot, acell(ot, 1 + acell(wt, 0).value()).value());
        },
        codes::oset(code, o),
        codes::rset(code, w));
    }
    stack.resize(stack.size() - (N - 1));
      
    return none{};
  }
  auto post(id o){
    return codes::with_op(lift((post_impl)(&)),o);
  }
  auto post(id,id,id a){
    return post(a);
  }
};

std::optional<anycell> eval(const code &c,set s,id r){

  evalctx ctx{c};

  grammars::walk<none>(
    c,
    lift((ctx.pre)(&)),
    lift((ctx.post)(&)),
    r);

  if(ctx.stack.size() != 1)
    return std::nullopt;
  auto &r_cell = ctx.stack.back();
  return (r_cell && set_of(*r_cell) == s) ? r_cell : std::nullopt;
}

statics::domain cover(const code &c,const statics::domain &d0,const statics::domain &d1){
  return {
    .min = with_set(
      [&](auto s0,auto s1){
        using C = cell<std::max(s0(), s1())>;
        auto v0 = static_cast<C>(eval(c, s0, d0.min).value());
        auto v1 = static_cast<C>(eval(c, s1, d1.min).value());
        return v0 < v1 ? d0.min : d1.min;
      },
      oset(c, d0.min),
      oset(c, d1.min)),
    .max = with_set(
      [&](auto s0,auto s1){
        using C = cell<std::max(s0(), s1())>;
        auto v0 = static_cast<C>(eval(c, s0, d0.max).value());
        auto v1 = static_cast<C>(eval(c, s1, d1.max).value());
        return v0 < v1 ? d1.max : d0.max;
      },
      oset(c, d0.max),
      oset(c, d1.max))
  };
}

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
