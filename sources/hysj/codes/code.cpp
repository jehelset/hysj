#include<hysj/codes/code.hpp>

#include<algorithm>
#include<cstdint>
#include<functional>
#include<numbers>
#include<numeric>
#include<ranges>

#include<fmt/format.h>

#include<hysj/codes/grammar.hpp>
#include<hysj/codes/statics.hpp>
#include<hysj/grammars.hpp>
#include<hysj/tools/functional/apply.hpp>
#include<hysj/tools/functional/bind.hpp>
#include<hysj/tools/graphs/algorithms/deeporders.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/type_traits.hpp>
#include<hysj/tools/types.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

std::partial_ordering code::subcmp(optag sup,natural sub0,natural sub1)const{
  return with_optag(
    [&](auto O)->std::partial_ordering{
      return symdat(*this, sym<O()>{sub0}) <=> symdat(*this, sym<O()>{sub1});
    },
    sup);
}
std::partial_ordering code::cmp(id op0,id op1)const{
  return codes::subcmp(lift((subcmp)(&)),op0,op1);
}
namespace _{
  template<optag O>
  std::partial_ordering subargcmp_impl(const code &code,sym<O> o0,sym<O> o1){
    auto A0 = rels(code, o0),
      A1 = rels(code, o1);

    auto cmp = ([&]<natural I>(constant_t<I>)-> std::partial_ordering{
        auto &a0 = kumi::get<I>(A0),
          &a1 = kumi::get<I>(A1);
        if constexpr(grammars::objvararg<O,I>){
          auto d0 = views::map(a0,[&](const auto &a){ return symdat(code, a); });
          auto d1 = views::map(a1,[&](const auto &a){ return symdat(code, a); });
          return std::lexicographical_compare_three_way(std::ranges::begin(d0),std::ranges::end(d0),
                                                        std::ranges::begin(d1),std::ranges::end(d1));

        }
        else
          return symdat(code, a0) <=> symdat(code, a1);
      });

    std::partial_ordering p = std::partial_ordering::equivalent;
    [&]<std::size_t... I>(std::index_sequence<I...>){
      (void)(((p = cmp(constant_c<I>)) != 0) || ...);
    }(grammars::objseq<O>{});
    return p;
  }
}
std::partial_ordering code::subargcmp(optag sup,natural sub0,natural sub1)const{
  return with_optag(
    [&](auto O)->std::partial_ordering{
      return _::subargcmp_impl(*this, sym<O()>{sub0},sym<O()>{sub1});
    },
    sup);
}
std::partial_ordering code::argcmp(id op0,id op1)const{
  return codes::subcmp(
    [&](optag sup,natural sub0,natural sub1){
      return subargcmp(sup, sub0, sub1);
    },
    op0,op1);
}

bool is_arg(const code &f,
            id root,
            id parameter)
{
  auto oprns = graphs::opreorder(std::cref(f.graph),objvert(f,root));
  return ranges::contains(oprns,objvert(f, parameter));
}

bool equal(const code &f,id o0,id o1){ 
  if(o0 == o1)
    return true;

  auto pretour =
    [&](auto root_index){
      auto root_vertex = objvert(f,root_index);
      return
        ranges::vec(
          graphs::opreorder(std::cref(f.graph),root_vertex),
          bind<>(lift((grammars::objid)), std::cref(f)));
      };
  auto tour0 = pretour(o0);
  auto tour1 = pretour(o1);
  
  auto eq = [&](id o0,id o1){
    return f.cmp(o0,o1) == std::partial_ordering::equivalent;
  };
  return std::ranges::equal(tour0,tour1,eq);
}

id copy(code &f,id o){
  return with_op(
    [&](auto o) -> id{
      return apply(
        [&](auto &&... args){
          return objadd<o.tag()>(f, symdat(f,o), args...);
        },
        args(f, o));
    },o);
  return {};
}

} //hysj::codes
