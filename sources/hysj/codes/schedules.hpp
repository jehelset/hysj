#pragma once

#include<optional>

#include<hysj/codes/code.hpp>
#include<hysj/grammars.hpp>
#include<hysj/tools/graphs/elements.hpp>
#include<hysj/tools/graphs/incidence_graphs.hpp>
#include<hysj/tools/ranges/cat.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

struct HYSJ_EXPORT sched{
  graphs::incidence_graph<taskfam> graph;

  friend HYSJ_MIRROR_STRUCT(sched, (graph));

  auto operator<=>(const sched &) const = default;

  static sched make(const code &, taskfam);
};

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
