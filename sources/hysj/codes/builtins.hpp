#pragma once
#include<array>
#include<tuple>

#include<hysj/codes/grammar.hpp>
#include<hysj/codes/sets.hpp>
#include<hysj/tools/functional/apply.hpp>
#include<hysj/tools/preprocessor.hpp>
#include<hysj/tools/reflection.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes::builtins{

struct HYSJ_EXPORT sets{
  #define HYSJ_LAMBDA(id,...) litsym id;
  HYSJ_MAP_LAMBDA(HYSJ_CODES_SETS)
  #undef HYSJ_LAMBDA

  friend HYSJ_MIRROR_STRUCT(sets,(HYSJ_SPLAT(0,HYSJ_CODES_SETS)));

  constexpr litsym operator[](integer i)const{
    switch(i){
      #define HYSJ_LAMBDA(id,...)               \
        case set::id: return id;
      HYSJ_MAP_LAMBDA(HYSJ_CODES_SETS)
        #undef HYSJ_LAMBDA
    default: HYSJ_UNREACHABLE;
    }
  }
  constexpr litsym operator[](set i)const{
    return operator[](etoi(i));
  }

  static constexpr auto make(syntax &f){
    return with_enumerators<set>(
      [&](auto... d){
        return sets{lit(f,litdat{constant{d.value}})...};
      });
  }

};

struct HYSJ_EXPORT widths{
  #define HYSJ_LAMBDA_0(_, w_) (w##w_)
  #define HYSJ_CODES_WIDTHS_MEMBERS HYSJ_MAP_2(HYSJ_LAMBDA_0, _, HYSJ_CODES_WIDTHS)

  #define HYSJ_LAMBDA_1(_, w) litsym w;
  HYSJ_MAP_3(HYSJ_LAMBDA_1, _, HYSJ_CODES_WIDTHS_MEMBERS)
  #undef HYSJ_LAMBDA

  friend HYSJ_MIRROR_STRUCT(widths,(HYSJ_SPLAT(0, HYSJ_CODES_WIDTHS_MEMBERS)));

  #undef HYSJ_LAMBDA_0
  #undef HYSJ_CODES_WIDTHS_MEMBERS
  #undef HYSJ_LAMBDA_1

  static constexpr auto make(syntax &f){
    return apply(
      [&](auto... w){
        return widths{lit(f, nvalue{w})...};
      },
      widthtab);
  }

  constexpr auto operator[](ncell<> w)const{
    switch(w){
      #define HYSJ_LAMBDA(w_)                   \
        case w_: return w##w_;
      HYSJ_MAP_LAMBDA(HYSJ_CODES_WIDTHS)
        #undef HYSJ_LAMBDA
    default: HYSJ_UNREACHABLE;
    }
  }
};

struct HYSJ_EXPORT types{

  #define HYSJ_LAMBDA_0(_0, _1, s_, w_) (s_##w_)
  #define HYSJ_CODES_TYPES_MEMBERS HYSJ_MAP_2(HYSJ_LAMBDA_0, _, HYSJ_CODES_TYPES)

  #define HYSJ_LAMBDA_1(_, t) typsym t;
  HYSJ_MAP_3(HYSJ_LAMBDA_1, _, HYSJ_CODES_TYPES_MEMBERS)
  #undef HYSJ_LAMBDA

  friend HYSJ_MIRROR_STRUCT(types,(HYSJ_SPLAT(0, HYSJ_CODES_TYPES_MEMBERS)));

  #undef HYSJ_LAMBDA_0
  #undef HYSJ_CODES_TYPES_MEMBERS
  #undef HYSJ_LAMBDA_1

  static constexpr auto make(syntax &f,
                             const sets &s,
                             const widths &w){
    return apply(
      [&](auto... t){
        return types{typ(f, s[t.set], w[t.width])...};
      },
      typetab);
  }

  constexpr auto operator[](set s, ncell<> w)const{
    #define HYSJ_LAMBDA(S_,s_,w_)               \
      if(s == set::S_ && w == w_)               \
        return s_##w_;
    HYSJ_MAP_LAMBDA(HYSJ_CODES_TYPES)
      #undef HYSJ_LAMBDA
      HYSJ_UNREACHABLE;
  }
  constexpr auto operator[](type t)const{
    return operator[](t.set, t.width);
  }
};

#define HYSJ_CODES_LITS HYSJ_PRODUCT((HYSJ_CODES_LITERALS)(HYSJ_CODES_WIDTHS))

#define HYSJ_LAMBDA(l,w) ((std::tuple{literal::l, ncell<>{w}}))
inline constexpr auto littab = std::array{HYSJ_SPLAT(0, HYSJ_MAP_LAMBDA(HYSJ_CODES_LITS))};
#undef HYSJ_LAMBDA

struct HYSJ_EXPORT lits{

  #define HYSJ_LAMBDA_0(_, l_, w_) (l_##w_)
  #define HYSJ_CODES_LITS_MEMBERS HYSJ_MAP_2(HYSJ_LAMBDA_0, _, HYSJ_CODES_LITS)

  #define HYSJ_LAMBDA_1(_, t) cstsym t;
  HYSJ_MAP_3(HYSJ_LAMBDA_1, _, HYSJ_CODES_LITS_MEMBERS)

  friend HYSJ_MIRROR_STRUCT(lits,(HYSJ_SPLAT(0, HYSJ_CODES_LITS_MEMBERS)));

  #undef HYSJ_LAMBDA_0
  #undef HYSJ_CODES_LITS_MEMBERS
  #undef HYSJ_LAMBDA_1

  static constexpr auto make(syntax &f, const widths &w){
    return apply(
      [&](auto... t){
        return lits{
          cst(f, 
              cstdat{constant{get<0>(t)}},
              w[get<1>(t)])...
        };
      },
      littab);
  }

  constexpr auto operator[](literal l, ncell<> w)const{
    #define HYSJ_LAMBDA(l_,w_)                  \
      if(l == literal::l_ && w == w_)           \
        return l_##w_;
    HYSJ_MAP_LAMBDA(HYSJ_CODES_LITS)
      #undef HYSJ_LAMBDA
      HYSJ_UNREACHABLE;
  }

};

constexpr std::optional<cstsym> identity(const lits &l,id o,ncell<> w){
  const auto O = optag_of(o);
  if(O == any_of(optag::sub,optag::add))
    return l[literal::zero, w];
  else if(O == any_of(optag::mul,optag::div))
    return l[literal::one, w];
  else if(O == any_of(optag::lor))
    return l[literal::bot, w];
  else if(O == any_of(optag::land))
    return l[literal::top, w];
  return std::nullopt;
}

struct HYSJ_EXPORT all{
  builtins::sets sets;
  builtins::widths widths;
  builtins::types types;
  builtins::lits lits;

  codes::devsym host;
  codes::noopsym noop;
  codes::exitsym cnt, brk, ret;

  friend HYSJ_MIRROR_STRUCT(all,(sets, lits, widths, types, host, noop, cnt, brk, ret));

  static auto make(syntax &s){
    all b{
      .sets   = sets::make(s),
      .widths = widths::make(s),
      .types  = types::make(s, b.sets, b.widths),
      .lits   = lits::make(s, b.widths),
      .host   = codes::dev(s),
      .noop   = codes::noop(s),
      .cnt    = codes::exit(s, b.lits.zero32),
      .brk    = codes::exit(s, b.lits.one32),
      .ret    = codes::exit(s, b.lits.negone32)
    };
    return b;
  }
};

} //hysj::codes::builtins
#include<hysj/tools/epilogue.hpp>
