#include<hysj/codes/compile/rotations.hpp>

#include<algorithm>
#include<array>
#include<utility>
#include<functional>
#include<optional>
#include<ranges>
#include<vector>

#include<hysj/codes/code.hpp>
#include<hysj/codes/access.hpp>
#include<hysj/codes/factories.hpp>
#include<hysj/codes/grammar.hpp>
#include<hysj/codes/compile/implicit_iterators.hpp>
#include<hysj/codes/compile/shapes.hpp>
#include<hysj/grammars.hpp>
#include<hysj/tools/functional/any_of.hpp>
#include<hysj/tools/functional/bind.hpp>
#include<hysj/tools/functional/compose.hpp>
#include<hysj/tools/functional/utility.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::_HYSJ_VERSION_NAMESPACE::codes{

struct compile_rotations_{
  codes::code &code;

  std::vector<itrsym> itrs;
  std::vector<id> slots;
  std::vector<std::optional<id>> argstack;

  auto new_argids(id o)const{
    return std::span{argstack}.last(argdeg(code, o));
  }
  auto argids(id o)const{
    return
      views::map(
        std::views::iota(0_i, argdeg(code, o)),
        [&,o](auto i){
          return new_argids(o)[i].value_or(grammars::argids(code, o)[i]);
        });
  }
  template<codes::optag O>
  auto args(sym<O> o)const{
    return grammars::args(code, o, argids(o));
  }
  bool has_new_args(id o)const{
    return std::ranges::any_of(new_argids(o), has_value);
  }

  template<optag O>
  id new_op(sym<O> o){
    return apply(
      [&](auto... a){
        return objadd<o.tag>(code, symdat(code, o), a...);
      },
      args(o));
  }
  template<optag O>
  auto try_new_op(sym<O> o){
    if(has_new_args(o))
      return just(new_op(o));
    return no<id>;
  }

  id itrmap(itrsym itr){
    auto slot = std::ranges::distance(std::ranges::begin(itrs), std::ranges::find(itrs, itr));
    if(slot == std::ranges::ssize(itrs))
      itrs.push_back(itr);
    return slots.at(slot);
  }

  void pop_args(id o){
    argstack.resize(argstack.size() - argdeg(code, o));
  }

  static constexpr std::size_t outer = 0,
    inner = 1;

  template<std::size_t pass>
  bool pre(id o){
    return with_op(
      [&](auto o){
        if(is_access(code, o)){
          argstack.push_back(
            [&] -> std::optional<id> {
              if constexpr(pass == outer)
                return {};
              if constexpr(can_access(o.tag)){
                if(has_implicit_iterator(code, o))
                  throw std::runtime_error("implicit iterator");
                
                if constexpr(o.tag == optag::var){
                  auto S = ranges::vec(arg(code, o, 1_N),
                                       [&](auto i){
                                         return walk<inner>(i).value_or(i.id());
                                       });
                  if(S.empty())
                    return o;
                  return rot(code, o, S);
                }
                else 
                  throw std::runtime_error("unexpected op");
              }
              else if constexpr(o.tag == optag::rot){
                auto [a, S] = grammars::args(code, o);
                argstack.push_back(a.id());
                for(auto s:S)
                  argstack.push_back(walk<inner>(compile_rotations(code, s)));
                auto u = new_op(o);
                pop_args(o);
                return u;
              }
              return {};
            }());
          return false;
        }
        else if constexpr(o.tag == optag::itr && pass == inner){
          argstack.push_back(itrmap(o));
          return false;
        }
        else{
          if constexpr(o.tag == optag::con && pass == inner){
            auto [a, S] = grammars::args(code, o);
            std::ranges::copy(S, std::back_inserter(slots));
            std::ranges::copy(S, std::back_inserter(itrs));
          }
          return true;
        }
      },
      o);
  }
  template<std::size_t pass>
  bool pre(id o,id r,id a){
    auto ot = optag_of(o);
    auto rt = reltag_of(r);
    if(rt == any_of(reltag::dom, reltag::dim, reltag::idx) ||
       can_access(ot) || ot == optag::der){
      argstack.push_back(std::nullopt);
      return false;
    }
    return pre<pass>(a);
  }
  
  template<std::size_t pass>
  auto post(id o){
    auto u = with_op(
      [&](auto o) -> std::optional<id>{
        if constexpr(o.tag == optag::rot){
          if constexpr(pass != outer)
            throw std::runtime_error("unexpected rot");
          else{
            auto [a, S] = args(o);
            slots.resize(std::ranges::size(S));
            std::ranges::copy(views::map(S, bind<>(&compile_rotations, std::ref(code))),
                              slots.data());
            itrs.clear();
            return walk<inner>(a);
          }
        }
        else{
          if constexpr(pass == inner && o.tag == optag::con){
            auto [a, C] = args(o);
            itrs.resize(itrs.size() - C.size());
            slots.resize(slots.size() - C.size());
            std::erase_if(itrs, bind<>(ranges::contains, C));
          }
          return try_new_op(o);
        }
      },
      o);
    pop_args(o);
    argstack.push_back(u);
    return u;
  }
  template<std::size_t pass>
  auto post(id o,id r,id a){
    return post<pass>(a);
  }

  template<std::size_t pass>
  auto walk(id o){
    auto n = argstack.size();
    grammars::walk<id>(code,
                       lift((pre<pass>)(&)),
                       lift((post<pass>)(&)),
                       o);
    if(argstack.size() == n)
      return no<id>;
    else if(argstack.size() != n + 1)
      throw std::runtime_error("whoops");
    auto u = argstack.back();
    argstack.pop_back();
    return u;
  }

  auto operator()(id o){
    return walk<outer>(o);
  }
};

//NOTE:
//  a rotation, h(X) := rot(f(X), (g_i(X)...)), substitutes the iterator in the ith slot of
//  compile_rotations(f(X)) with g_i(X). rotations are compiled in post-order fashion; all
//  rotations in and below f will be fully subsituted before the iterator substitutes of h
//  are put in place, and the ith slot is the ith slot of the image of f(X).
//
//  this pass moves along {jmp, alt, src, dst, exe}
//  - jeh
id compile_rotations(code &c,id o){
  compile_rotations_ ctx{c};
  //  not a walk...
  return ctx(o).value_or(o);
}

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
