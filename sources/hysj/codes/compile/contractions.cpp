#include<hysj/codes/compile/contractions.hpp>

#include<algorithm>
#include<array>
#include<utility>
#include<functional>
#include<optional>
#include<ranges>
#include<vector>

#include<hysj/codes/code.hpp>
#include<hysj/codes/access.hpp>
#include<hysj/codes/factories.hpp>
#include<hysj/codes/grammar.hpp>
#include<hysj/grammars.hpp>
#include<hysj/tools/functional/any_of.hpp>
#include<hysj/tools/functional/bind.hpp>
#include<hysj/tools/functional/compose.hpp>
#include<hysj/tools/functional/utility.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::_HYSJ_VERSION_NAMESPACE::codes{

struct compile_contractions_{
  codes::code &code;

  struct frame{
    bool all;
    std::vector<std::array<itrsym, 2>> dups;
  };

  std::vector<frame> stack;

  auto itrdup(itrsym i){

    if(stack.empty())
      return no<itrsym>;
    auto &frame = stack.back();

    auto it = std::ranges::find_if(
      frame.dups, compose<>(bind<>(std::ranges::equal_to{}, i), lift((get<0>))));
    if(it == std::ranges::end(frame.dups)){
      if(!frame.all)
        return no<itrsym>;
      frame.dups.push_back({i, itr_dup(code, i)});
      it = std::ranges::end(frame.dups) - 1;
    }
    return just(get<1>(*it));
  }
  auto has_itrdup(itrsym i){
    return itrdup(i) != std::nullopt;
  }

  std::vector<std::optional<id>> argstack;

  auto new_argids(id o)const{
    return std::span{argstack}.last(argdeg(code, o));
  }
  auto argids(id o)const{
    return
      views::map(
        std::views::iota(0_i, argdeg(code, o)),
        [&,o](auto i){
          return new_argids(o)[i].value_or(grammars::argids(code, o)[i]);
        });
  }
  template<codes::optag O>
  auto args(sym<O> o)const{
    return grammars::args(code, o, argids(o));
  }
  bool has_new_args(id o)const{
    return std::ranges::any_of(new_argids(o), has_value);
  }

  void pop_args(id o){
    argstack.resize(argstack.size() - argdeg(code, o));
  }

  constexpr bool pre(id o){
    return with_op(
      [&](auto o){
        if constexpr(o.tag == optag::itr){
          argstack.push_back(itrdup(o).transform([&](auto i){ return i.id(); }));
          return false;
        }
        else if constexpr(o.tag == any_of(optag::var, optag::rfl, optag::der)){
          if constexpr(o.tag == optag::var){
            auto D = arg(code, o, 1_N);
            if(std::ranges::any_of(
                 D, [&](auto d) -> bool{
                   if(auto i = itrcast(d))
                     return has_itrdup(*i);
                   return false;
                 })){
              auto U = ranges::vec(
                D,
                [&](auto d){
                  if(auto i = itrcast(d))
                    return itrdup(*i).value_or(*i);
                  else
                    return itr(code, d);
                });
              argstack.push_back(rot(code, o, U));
              return false;
            }
          }
          argstack.push_back(std::nullopt);
          return false;
        }
        else if constexpr(o.tag == optag::con){
          auto I = arg(code, o, 1_N);
          stack.push_back({.all = std::ranges::empty(I)});
          for(auto i:arg(code, o, 1_N))
            stack.back().dups.push_back({i, itr_dup(code, i)});
        }
        return true;
      },
      o);
  }
  constexpr bool pre(id o,id r,id a){
    auto rt = codes::reltag_of(r);
    if(rt == any_of(codes::reltag::dom, codes::reltag::dim)){
      argstack.push_back(std::nullopt);
      return false;
    }
    return pre(a);
  }

  constexpr auto post(id o){
    return with_op(
      [&](auto o) -> id{
        if constexpr(o.tag == optag::con)
          stack.pop_back();
        auto u = [&] -> id {
          if(has_new_args(o))
            return apply(
              [&](auto... a) -> id{
                return codes::objadd<o.tag>(code, symdat(code, o), a...);
              },
              args(o));
          return o;
        }();
        pop_args(o);
        argstack.push_back(maybe(o.id() != u, u));
        return u;
      },
      o);
  }
  constexpr auto post(id o,id r,id a){
    return post(a);
  }
};

//FIXME: ensure contraction iterators are unique...
id compile_contractions(code &c,id o){
  compile_contractions_ ctx{c};
  return grammars::walk<id>(c,
                            lift((ctx.pre)(&)),
                            lift((ctx.post)(&)),
                            o).value_or(o);
}

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
