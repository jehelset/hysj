#include<hysj/codes/compile/implicit_iterators.hpp>

#include<algorithm>
#include<array>
#include<utility>
#include<functional>
#include<optional>
#include<ranges>
#include<vector>

#include<hysj/codes/access.hpp>
#include<hysj/codes/code.hpp>
#include<hysj/codes/factories.hpp>
#include<hysj/codes/compile/shapes.hpp>
#include<hysj/codes/grammar.hpp>
#include<hysj/grammars.hpp>
#include<hysj/tools/functional/any_of.hpp>
#include<hysj/tools/functional/bind.hpp>
#include<hysj/tools/functional/compose.hpp>
#include<hysj/tools/functional/optional.hpp>
#include<hysj/tools/functional/utility.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::_HYSJ_VERSION_NAMESPACE::codes{

struct compile_implicit_iterators_{
  codes::code &code;

  std::vector<std::tuple<id, itrsym>> itrs;

  auto expleted_itr(dimexprfam n)const{
    auto it = std::ranges::find_if(itrs, compose<>(bind<>(std::ranges::equal_to{}, n.id()), lift((get<0>))));
    if(it == std::ranges::end(itrs))
      return no<itrsym>;
    return just(get<1>(*it));
  }
  auto explete_itr(dimexprfam d){
    if(auto i = itrcast(d))
      return *i;
    if(auto ei = expleted_itr(d))
      return *ei;
    return get<1>(itrs.emplace_back(d.id(), itr(code, d.id())));
  }
  bool has_implicit_itr(varsym v)const{
    return std::ranges::any_of(arg(code, v, 1_N),
                               [&](auto d){
                                 return !itrcast(d);
                               });
  }

  std::vector<std::optional<id>> argstack;

  auto new_argids(id o)const{
    return std::span{argstack}.last(argdeg(code, o));
  }
  auto argids(id o)const{
    return
      views::map(
        std::views::iota(0_i, argdeg(code, o)),
        [&,o](auto i){
          return new_argids(o)[i].value_or(grammars::argids(code, o)[i]);
        });
  }
  template<codes::optag O>
  auto args(sym<O> o)const{
    return grammars::args(code, o, argids(o));
  }
  bool has_new_args(id o)const{
    return std::ranges::any_of(new_argids(o), has_value);
  }

  void pop_args(id o){
    argstack.resize(argstack.size() - argdeg(code, o));
  }

  constexpr bool pre(id o){
    return true;
  }
  constexpr bool pre(id o,id r,id a){
    auto ot = optag_of(o);
    auto rt = reltag_of(r);
    auto at = optag_of(a);
    if(ot == optag::rot && is_access(code, o) && rt == reltag::src){
      argstack.push_back(std::nullopt);
      return false;
    }
    if(rt == reltag::dom){
      argstack.push_back(std::nullopt);
      return false;
    }
    if(rt == reltag::dim){
      argstack.push_back(std::nullopt);
      if(at == optag::rfl)
        return false;
      explete_itr(dimexprcast(a).value());
      return false;
    }
    return pre(a);
  }

  constexpr auto post(id o){
    return with_op(
      [&](auto o) -> id{
        auto u = [&] -> std::optional<id> {
          if constexpr(o.tag == optag::var){
            if(has_implicit_itr(o) || has_new_args(o))
              return compose<>(just, apply)(
                [&](auto t,auto D) -> id{
                  if (std::ranges::size(D) == 1 && rflcast(ranges::first(D))){
                    auto r = rflcast(ranges::first(D)).value();
                    compile_shape(code, r);
                    auto I = oitrs(code, r);
                    return rot(code, o, I);
                  }
                  return rot(
                    code, o,
                    views::map(D, [&](auto d){
                      return explete_itr(dimexprcast(d).value());
                    }));
                },
                args(o));
          }
          else if constexpr(o.tag == optag::rfl){
            auto p = [&] -> id{
              if(has_new_args(o)){
                auto [a] = args(o);
                return a;
              }
              return o;
            }();
            compile_shape(code, p);
            if(auto I = oitrs(code, p); !std::ranges::empty(I))
              return just(rot(code, o, I).id());
          }
          else if constexpr(o.tag == optag::der){
            auto [x, y, J] = args(o);
            compile_shape(code, x);
            auto I = ranges::vec(oitrs(code, x));
            compile_shape(code, y);
            std::ranges::copy(oitrs(code, y), std::back_inserter(I));
            std::ranges::copy(J, std::back_inserter(I));
            ranges::vecset(I);
            if(!std::ranges::empty(I))
              return just(rot(code, o, I).id());
          }
          else{
            if constexpr(o.tag == optag::con){
              auto [a, I] = args(o);
              if(std::ranges::empty(I)){
                compile_shape(code, a);
                auto J = oitrs(code, a);
                if(!std::ranges::empty(J))
                  return just(objadd<o.tag>(code, symdat(code, o), a, J).id());
              }
            }
            if(has_new_args(o))
              return compose<>(just, apply)(
                [&](auto... a) -> id{
                  return objadd<o.tag>(code, symdat(code, o), a...);
                },
                args(o));
          }
          return no<id>;
        }();
        pop_args(o);
        argstack.push_back(u);
        return u.value_or(o);
      },
      o);
  }
  constexpr auto post(id o,id r,id a){
    return post(a);
  }
};

id compile_implicit_iterators(code &c,id o){
  compile_shape(c, o);
  compile_implicit_iterators_ ctx{c};
  return grammars::walk<id>(c,
                            lift((ctx.pre)(&)),
                            lift((ctx.post)(&)),
                            o).value_or(o);
}

} //hysj::codes
#include<hysj/tools/epilogue.hpp>

