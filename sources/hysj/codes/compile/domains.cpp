#include<hysj/codes/compile/domains.hpp>

#include<functional>
#include<optional>
#include<utility>

#include<hysj/codes/code.hpp>
#include<hysj/codes/constants.hpp>
#include<hysj/codes/eval.hpp>
#include<hysj/codes/grammar.hpp>
#include<hysj/codes/statics.hpp>
#include<hysj/codes/compile/shapes.hpp>
#include<hysj/codes/compile/types.hpp>
#include<hysj/grammars.hpp>
#include<hysj/tools/functional/bind.hpp>
#include<hysj/tools/functional/compose.hpp>
#include<hysj/tools/functional/optional.hpp>
#include<hysj/tools/graphs/algorithms/deeporders.hpp>
#include<hysj/tools/types.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

namespace _{

  struct compile_domain_impl{
    codes::code &code;

    auto domain(id min, id max){
      for(auto o:std::array{min, max}){
        compile_type(code, o);
        compile_shape(code, o);
      }
      return statics::domain{min,max};
    }

    template<optag O>
    constexpr std::optional<statics::domain> odomain_impl(sym<O> o){
      if constexpr(O == any_of(optag::lit, optag::cst))
        return statics::domain{o, o};
      else if constexpr(O == optag::typ){
        auto d = grammars::arg(code, o, 0_N);
        auto w = owidth(code, o);
        return domain(statics::tmin(code, code.builtins.lits, d, w),
                      statics::tmax(code, code.builtins.lits, d, w));
      }
      else if constexpr(O == optag::itr){
        const auto &[min, max] = auto(odomain(code.statics.domains, grammars::arg(code, o, 0_N)).value());
        return domain(code.builtins.lits[literal::zero, owidth(code, o)], max);
      }
      else if constexpr(O == any_of(optag::var, optag::rfl, optag::cur, optag::rot, optag::der))
        return odomain(code.statics.domains, grammars::arg(code, o, 0_N)).value();
      else if constexpr(O == optag::cvt)
        return odomain(code.statics.domains, grammars::arg(code, o, 1_N)).value();
      else if constexpr(O == any_of(optag::neg, optag::bnot, optag::lnot)){
        const auto &[min, max] = auto(odomain(code.statics.domains, grammars::arg(code, o, 0_N)).value());

        constexpr auto fn = []{
          if constexpr(O == optag::neg)
            return neg;
          else if constexpr(O == optag::bnot)
            return bnot;
          else{
            static_assert(O == optag::lnot);
            return lnot;
          }
        }();

        return domain(fn(code, max), fn(code, min));
      }
      else if constexpr(O == optag::sin){
        auto w = owidth(code, o);
        return domain(code.builtins.lits[literal::negone, w],
                      code.builtins.lits[literal::one,    w]);
      }
      else if constexpr(O == optag::mod){
        auto [a, m] = grammars::args(code, o);
        auto m_width = owidth(code, m);
        auto m_zero = code.builtins.lits[literal::zero, m_width];

        const auto a_domain = odomain(code.statics.domains, a).value();
        const auto &m_domain = statics::domain{m_zero, m};
        const auto &c_domain = cccover(code, m_domain, a_domain);

        return c_domain != m_domain ? m_domain : a_domain;
      }
      else if constexpr(O == optag::fct){
        const auto &[a] = grammars::args(code, o);
        const auto &[a_min, a_max] = auto(odomain(code.statics.domains, a).value());
        return domain(fct(code, a_min), fct(code, a_max));
      }
      else if constexpr(O == any_of(optag::add, optag::land, optag::lor)){

        auto D = ranges::vec(
          grammars::arg(code, o, 0_N),
          compose<>(
            hysj::value,
            bind<>(lift((odomain)), std::ref(code.statics.domains))));

        constexpr auto fn = []{
          if constexpr(O == optag::add)
            return add;
          else if constexpr(O == optag::land)
            return land;
          else{
            static_assert(O == optag::lor);
            return lor;
          }
        }();

        return domain(fn(code, views::map(D, &statics::domain::min)),
                      fn(code, views::map(D, &statics::domain::max)));
      }
      else if constexpr(O == optag::sub){
        auto [a0, A1] = grammars::args(code, o);
        auto d0 = odomain(code.statics.domains, a0).value();
        auto D1 = ranges::vec(
          A1,
          compose<>(
            hysj::value,
            bind<>(lift((odomain)), std::ref(code.statics.domains))));
        return domain(sub(code, d0.min, views::map(D1, &statics::domain::max)),
                      sub(code, d0.max, views::map(D1, &statics::domain::min)));
      }
      else if constexpr(O == any_of(optag::blsh, optag::brsh)){
        auto [a0, a1] = grammars::args(code, o);

        constexpr auto fn = []{
          if constexpr(O == optag::blsh)
            return blsh;
          else{
            static_assert(O == optag::brsh);
            return brsh;
          }
        }();

        const auto &[d0_min, d0_max] = auto(odomain(code.statics.domains, a0).value());
        auto [d1_min, d1_max] = auto(odomain(code.statics.domains, a1).value());

        if constexpr(O == optag::brsh)
          std::swap(d1_min, d1_max);

        return statics::domain{
          .min = fn(code, d0_min, d1_min),
          .max = fn(code, d0_max, d1_max),
        };
      }
      else if constexpr(O == any_of(optag::bor, optag::band, optag::bxor)){
        auto w = owidth(code, o);
        return domain(code.builtins.lits[literal::zero, w],
                      code.builtins.lits[literal::inf,  w]);
      }
      else if constexpr(O == any_of(optag::mul, optag::div, optag::pow, optag::log, optag::sel)){
        //FIXME: impl these
        auto [s, w] = otype(code, o);
        return domain(statics::smin(code.builtins.lits, s, w),
                      statics::smax(code.builtins.lits, s, w));
      }
      else{
        //FIXME: impl these
        static_assert(
          O == any_of(optag::con,
                      optag::mul,  optag::div,  optag::pow,  optag::log,   optag::sel,
                      optag::eq,   optag::lt,   optag::put,  optag::noop,
                      optag::loop, optag::exit, optag::fork, optag::then, optag::when,
                      optag::on,   optag::to,   optag::dev,  optag::api));
        auto [s, w] = otype(code, o);
        return domain(statics::smin(code.builtins.lits, s, w),
                      statics::smax(code.builtins.lits, s, w));
      }
    }
    template<optag O,natural I>
    constexpr auto rdomain_impl(sym<O> o,constant_t<I> i){
      return odomain(code.statics.domains, o);
    }

    template<optag O>
    void operator()(sym<O> o){
      odomain(code.statics.domains, o) = odomain_impl(o);
      grammars::sink_rels(
        code,
        [&](auto I,id r){
          rdomain(code.statics.domains,r) = rdomain_impl(o,I);
        },
        o);
    }

  };
}

void compile_domain(code &c,id o){
  resize(c, c.statics.domains);
  if(has_odomain(c.statics.domains, o))
    return;
  for(auto o_vert:graphs::opostorder(std::cref(c.graph),objvert(c,o)))
    if(auto o = objid(c, o_vert); !has_odomain(c.statics.domains,o))
      with_op(_::compile_domain_impl{c}, o);
}

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
