#include<hysj/codes/compile/shapes.hpp>

#include<algorithm>
#include<utility>
#include<functional>
#include<ranges>

#include<hysj/codes/grammar.hpp>
#include<hysj/codes/code.hpp>
#include<hysj/codes/factories.hpp>
#include<hysj/tools/functional/apply.hpp>
#include<hysj/tools/functional/bind.hpp>
#include<hysj/tools/ranges/keep.hpp>
#include<hysj/tools/ranges/map.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/graphs/algorithms/deeporders.hpp>
#include<hysj/tools/functional/bind.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::_HYSJ_VERSION_NAMESPACE::codes{

struct shape_ctx{
  codes::code &code;

  constexpr bool pre(id o)const{
    return !has_oshape(code.statics.shapes,o);
  }

  bool pre(id,id r,id a)const{
    if(reltag_of(r) == reltag::dom)
      return false;
    return pre(a);
  }

  //FIXME: assert scalars and that - jeh
  template<optag O>
  void post_impl(sym<O> o){

    auto &c = code;
    auto &s = c.statics.shapes;

    auto &o_shape = oshape(s,o);
    o_shape.emplace(std::array<natural,3>{s.slots.size(),0_n,0_n});

    auto add_iter = [&](id i){
      auto i_itr_begin = std::ranges::begin(s.iterators);
      auto i_itr_end   = std::ranges::end(s.iterators);

      auto i_itr_it = i_itr_begin;
      while(1){

        i_itr_it = std::ranges::find(ranges::tie(i_itr_it,i_itr_end),i);
        auto i_itr = static_cast<natural>(std::ranges::distance(i_itr_begin,i_itr_it));

        if(has_itrop(i) || !ranges::contains(oslots(s,o), i_itr)){
          if(i_itr == s.iterators.size())
            s.iterators.push_back(i);
          return i_itr;
        }

        i_itr_it++;
      }
    };
    auto add = [&]<natural I>(constant_t<I>,id i){
      s.slots.push_back(add_iter(i));
      ++((*oshape(s, o)).iterators[1 + I]);
    };
    auto add_all = [&](auto I,auto I_slot){
      std::ranges::for_each(I_slot,bind<>(add,I));
    };

    auto merge = [&]<natural I>(constant_t<I> i,natural i_slot){
      if(ranges::contains(oslots(s,o,i),i_slot))
        return;
      s.slots.push_back(i_slot);
      ++((*oshape(s, o)).iterators[1 + I]);
    };
    auto merge_all = [&](auto I,auto I_slot){
      std::ranges::for_each(I_slot,bind<>(merge,I));
    };
    auto merge_prepare = [&](id a){
      s.slots.reserve(s.slots.size() + 2*orank(s,a,0_N)); //NOTE: 2* to account 
    };

    if constexpr(O == any_of(optag::typ,  optag::cur,  optag::cst,
                             optag::noop, optag::loop,
                             optag::exit, optag::fork, optag::then,
                             optag::when))
      ;
    else if constexpr(O == optag::itr)
      add(0_N,o);
    else if constexpr(O == any_of(optag::var)){
      auto dims = arg(c,o,1_N);
      //FIXME: intermingled rfl and dims! - jeh
      if(std::ranges::size(dims) == 1 && rflcast(ranges::first(dims))){
        auto r = *rflcast(ranges::first(dims));
        merge_prepare(r);
        merge_all(0_N,oslots(s,r));
      }
      else{
        HYSJ_ASSERT(std::ranges::none_of(dims,compose<>(has_value,rflcast)));
        add_all(0_N,arg(c, o, 1_N));
      }
    }
    else if constexpr(O == optag::cvt){
      auto a = arg(c, o, 1_N);
      merge_prepare(a);
      merge_all(0_N,oslots(s,a));
    }
    else if constexpr(O == optag::rot){
      auto [a,B] = args(c, o);
      for(auto b:B){
        merge_prepare(b);
        merge_all(0_N,oslots(s,b));
      }
      merge_prepare(a);
      merge_all(1_N,oslots(s,a));
    }
    else if constexpr(O == optag::con){
      auto [a,B] = args(c, o);
      if(!std::ranges::empty(B)){
        merge_prepare(a);
        merge_all(
          0_N,
          views::keep(
            oslots(s,a),
            [&](auto i_slot){
              return !ranges::contains(views::cast<id>(B), s.iterators[i_slot]);
            }));
        for(auto b:B){
          merge_prepare(b);
          merge_all(1_N,oslots(s,b));
        }
      }
      else{
        merge_prepare(a);
        merge_all(1_N,oslots(s,a));
      }
    }
    else
      for(auto a:argids(c, o)){
        merge_prepare(a);
        merge_all(0_N,oslots(s,a));
      }
  }
  none post(id o){
    with_op([&](auto o){ return post_impl(o); }, o);
    return {};
  }
  none post(id,id,id o){
    post(o);
    return {};
  }

};

void compile_shape(code &c,id o){

  shape_ctx ctx{c};

  resize(c, c.statics.shapes);
  if(has_oshape(c.statics.shapes, o))
    return;

  grammars::walk<none>(c,
                       lift((ctx.pre)(&)),
                       lift((ctx.post)(&)),
                       o);

}

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
