#pragma once

#include<hysj/codes/grammar.hpp>
#include<hysj/codes/code.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::_HYSJ_VERSION_NAMESPACE::codes{

HYSJ_EXPORT
void compile_shape(code &,id);

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
