#pragma once

#include<algorithm>
#include<functional>
#include<vector>

#include<hysj/codes/access.hpp>
#include<hysj/codes/code.hpp>
#include<hysj/codes/grammar.hpp>
#include<hysj/tools/coroutines/trees.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/functional/any_of.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::_HYSJ_VERSION_NAMESPACE::codes{

constexpr bool is_implicit_iterator(const code &,dimexprfam i){
  return !itrcast(i).has_value();
}
constexpr bool has_implicit_iterator(const code &c,id x){
  auto varcheck = [&](varsym v){
    return std::ranges::any_of(arg(c, v, 1_N),
                               bind<>(&is_implicit_iterator, std::ref(c)));
  };
  return with_op(
    [&](auto x){
      if constexpr(x.tag == optag::rfl)
        return true;
      else if constexpr(x.tag == optag::var)
        return varcheck(x);
      else if constexpr(x.tag == optag::der){
        auto [f, dx, dI] = args(c, x);
        return has_implicit_iterator(c, f) || has_implicit_iterator(c, dx);
      }
      else
        return false;
    },
    x);
}

HYSJ_EXPORT
id compile_implicit_iterators(code &,id);

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
