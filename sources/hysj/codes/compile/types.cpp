#include<hysj/codes/compile/types.hpp>

#include<array>
#include<algorithm>
#include<functional>
#include<ranges>

#include<hysj/codes/code.hpp>
#include<hysj/codes/constants.hpp>
#include<hysj/codes/grammar.hpp>
#include<hysj/codes/sets.hpp>
#include<hysj/tools/functional/any_of.hpp>
#include<hysj/tools/functional/apply.hpp>
#include<hysj/tools/graphs/algorithms/deeporders.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

constexpr auto widtheval(const code &c, domexprfam o){
  return grammars::with_famobj(
    [&](auto o){
      if constexpr(o.tag == any_of(optag::lit, optag::cst))
        return csteval(naturals_c, symdat(c, o)).value();
      else{
        static_assert(o.tag == optag::rfl);
        return owidth(c, o);
      }
    },
    o);
}
constexpr auto maxargwidth(const code &c,
                           grammars::concepts::forward_id auto &&O){
  if (std::ranges::empty(O))
    return widthtab[0];
  return std::ranges::max(
    views::map(O, bind<>(lift((owidth)), std::ref(c))));
}

constexpr auto otype_tmin(optag O){
  using enum optag;
  if(O == any_of(neg,add,sub,mul,div,pow,log,fct,bnot,blsh,brsh,bor,band,bxor))
    return set::naturals;
  else
    return set::booleans;
}
constexpr auto otype_tmax(optag O){
  using enum optag;
  if(O == any_of(fct,bnot,blsh,brsh,bor,band,bxor))
    return set::integers;
  else
    return set::reals;
}
constexpr auto otype_clamp(const code &c,optag o,typsym t){
  auto [d, w] = grammars::args(c, t);
  set d_clamp =
    std::min(std::max(otype_tmin(o), oset(c, t)), otype_tmax(o));
  if(d.id() == c.builtins.sets[d_clamp].id())
    return t;
  return c.builtins.types[d_clamp, widtheval(c, arg(c, t, 1_N))];
}
template<optag O>
constexpr typsym otype_post(const code &c,sym<O> o){

  auto t = [&](id o){
    return otype(c.statics.types, o).value();
  };
  auto tcmp = [&](typsym t0, typsym t1){
    return otype(c, t0) < otype(c,t1);
  };

  if constexpr(O == any_of(optag::lit,  optag::put,
                           optag::noop, optag::loop, optag::exit,
                           optag::fork, optag::then, optag::when,
                           optag::on,   optag::to,   optag::dev))
    return c.builtins.types.n8;
  else if constexpr(O == optag::typ)
    return [&]<std::size_t... I>(std::index_sequence<I... >) -> typsym {
      auto A = args(c, o);
      if(!(rflcast(kumi::get<I>(A)) || ...))
        return o;
      auto [s, w] = std::make_tuple(
        rflcast(kumi::get<I>(A))
        .transform([&](auto s){ return arg(c, t(arg(c, s, 0_N)), constant_c<I>).id(); })
        .or_else([&]{ return just(kumi::get<I>(A).id()); }).value()...);
      return c.builtins.types[statics::tset(c, s), statics::twidth(c, w)];
    }(grammars::objseq<O>{});
  else if constexpr(O == optag::cvt)
    return t(arg(c, o, 0_N));
  else if constexpr(O == optag::itr)
    return c.builtins.types[set::integers, owidth(c, arg(c, o, 0_N))];
  else if constexpr(O == any_of(optag::cur, optag::con,optag::var,optag::rfl,optag::rot,optag::neg,optag::sin))
    return t(arg(c, o, 0_N));
  else if constexpr(O == optag::cst)
    return c.builtins.types[cstdom(symdat(c, o)), widtheval(c, arg(c, o, 0_N))];
  else if constexpr(O == any_of(optag::div, optag::mod, optag::pow, optag::log)){
    auto [t0,t1] = apply([&](auto &&... a){ return std::array{ t(fwd(a))... }; },
                         args(c, o));
    return tcmp(t0,t1) ? t1 : t0;
  }
  else if constexpr(O == optag::sub){
    auto [a0,A1] = args(c, o);
    return std::ranges::max(views::prepend(t(a0),views::map(A1,t)),tcmp);
  }
  else if constexpr(O == optag::der){
    auto [a0,a1,A2] = args(c, o);
    return std::ranges::max({t(a0), t(a1)});
  }
  else if constexpr(O == any_of(optag::lnot,optag::lor,optag::land,optag::eq,optag::lt))
    return c.builtins.types[set::booleans, maxargwidth(c, argids(c, o.id()))]; //FIXME: not all need same... - jeh
  else if constexpr(O == optag::sel)
    return std::ranges::max(views::map(arg(c, o, 1_N), t), tcmp);
  else{
    auto t_max = std::ranges::max(views::map(argids(c, o), t),tcmp);
    return otype_clamp(c,O,t_max);
  }
}

template<optag O,natural I>
constexpr typsym rtype_post(code &c,sym<O> o,constant_t<I> i){

  constexpr auto A = grammars::objreltag<O,I>;

  auto t = [&](id o){
    return otype(c.statics.types, o).value();
  };
  auto tcmp = [&](typsym t0, typsym t1){
    return otype(c,t0) < otype(c,t1);
  };

  if constexpr(A == reltag::exe)
    return c.builtins.types.b8;
  else if constexpr(O == optag::var || (O == optag::rot && I == 1) || (O == any_of(optag::sel, optag::fork, optag::exit) && I == 0))
    return c.builtins.types.n32; //FIXME: crap - jeh
  else if constexpr(O == optag::exit && I == 0)
    return c.builtins.types.i32; //FIXME: crap - jeh
  else if constexpr(O == any_of(optag::eq,optag::lt)){
    auto [t0,t1] = apply([&](auto &&... a){ return std::array{ t(a)... }; },
                         args(c, o));
    return tcmp(t0,t1) ? t1 : t0;
  }
  else if constexpr(O == optag::pow && I == 1)
    return t(arg(c, o, i));
  else if constexpr(O == optag::put)
    return t(arg(c, o, 0_N));
  else
    return t(o);
}

template<optag O>
void type_post(code &c,sym<O> o){
  auto &o_type = otype(c.statics.types,o);
  o_type.emplace(otype_post(c,o));

  if(!otype(c.statics.types,o_type.value()))
    otype(c.statics.types,o_type.value()) = o_type.value();
  grammars::sink_rels(
    c,
    [&](auto I,id r){
      auto &r_type = rtype(c.statics.types,r);
      r_type.emplace(rtype_post(c,o,I));
      if(!otype(c.statics.types, r_type.value()))
        otype(c.statics.types, r_type.value()) = r_type.value();
    },
    o);
}

void compile_type(code &c,id o){
  resize(c, c.statics.types);
  if(has_otype(c.statics.types, o))
    return;
  for(auto o_vert:graphs::opostorder(std::cref(c.graph),objvert(c,o)))
    if(auto o = objid(c, o_vert); !has_otype(c.statics.types,o))
      with_op(
        [&](auto o){
          type_post(c,o);
        },
        o);
}

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
