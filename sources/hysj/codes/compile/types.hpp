#pragma once

#include<hysj/codes/grammar.hpp>
#include<hysj/codes/code.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

HYSJ_EXPORT
void compile_type(code &,id);

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
