#pragma once
#include<limits>
#include<numeric>
#include<optional>
#include<string_view>

#include<fmt/format.h>

#include<hysj/codes/sets.hpp>
#include<hysj/tools/functional/any_of.hpp>
#include<hysj/tools/functional/optional.hpp>
#include<hysj/tools/functional/variant.hpp>
#include<hysj/tools/preprocessor.hpp>
#include<hysj/tools/reflection.hpp>
#include<hysj/tools/types.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

#define HYSJ_CODES_LITERALS \
  (bot)                     \
  (top)                     \
  (zero)                    \
  (one)                     \
  (negone)                  \
  (two)                     \
  (half)                    \
  (neghalf)                 \
  (inf)                     \
  (neginf)                  \
  (pi)                      \
  (e)

enum class literal : natural {
  HYSJ_SPLAT(0,HYSJ_CODES_LITERALS)
};
HYSJ_MIRROR_ENUM(literal,HYSJ_CODES_LITERALS)

template<set S>
constexpr std::optional<cell<S>> liteval(constant_t<S>,literal l){
  using T = cell<S>;

  switch(l){
  case literal::bot: return static_cast<T>(false);
  case literal::top: return static_cast<T>(true);
  default: break;
  }

  if constexpr(S == any_of(set::integers,set::naturals,set::reals))
    switch(l){
    case literal::zero:   return  T{ 0};
    case literal::one:    return  T{ 1};
    default: break;
    }

  if constexpr(S == any_of(set::integers,set::reals))
    switch(l){
    case literal::negone: return  T{-1};
    case literal::two:    return  T{ 2};
    default: break;
    }
    
  if constexpr(S == set::integers)
    switch(l){
    case literal::inf:       return std::numeric_limits<T>::max();
    case literal::neginf:    return std::numeric_limits<T>::lowest();
    default: break;
    }
  if constexpr(S == set::naturals)
    switch(l){
    case literal::inf:       return std::numeric_limits<T>::max();
    case literal::neginf:    return T{0};
    default: break;
    }

  if constexpr(S == set::reals)
    switch(l){
    case literal::half:      return 0.5;
    case literal::neghalf:   return -0.5;
    case literal::pi:        return std::numbers::pi_v<T>;
    case literal::e:         return std::numbers::e_v<T>;
    case literal::inf:       return std::numeric_limits<T>::infinity();
    case literal::neginf:    return -std::numeric_limits<T>::infinity();
    default: break;
    }
    
  return std::nullopt;
}

template<set F>
struct value{
  codes::cell<F> cell;
  
  friend HYSJ_MIRROR_STRUCT(value, (cell));

  auto operator<=>(const value &) const = default;

  constexpr operator codes::cell<F>()const noexcept{ return cell; }
};

#define HYSJ_LAMBDA(S,s) using s##value = value<set::S>;
HYSJ_MAP_LAMBDA(HYSJ_CODES_SETS)
#undef HYSJ_LAMBDA

using constant = reify(
  with_enumerators<set>(
    []<set... F>(constant_t<F>...){
      return type_c<std::variant<set,literal,value<F>...>>;
    }));

HYSJ_EXPORT std::optional<bool> is_zero(const constant &);
HYSJ_EXPORT std::optional<bool> is_one(const constant &);
HYSJ_EXPORT std::optional<bool> is_negone(const constant &);
HYSJ_EXPORT std::optional<bool> is_odd(const constant &);

HYSJ_EXPORT set cstdom(const constant &);

template<set S>
constexpr auto csteval(constant_t<S> s,const constant &c){
  using V = cell<S>;
  return visitor(
    [](set){
      return no<V>;
    },
    [=](literal l){ 
      return liteval(s,l);
    },
    [](auto v){
      if constexpr(requires{ static_cast<V>(v); })
        return just(static_cast<V>(v));
      else
        return no<V>;
    })(c);
}

} //hysj::codes
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

using codes::literal;
using codes::constant;

} //hysj
HYSJ_ENUM_FORMATTER(hysj::_HYSJ_VERSION_NAMESPACE::codes::literal);
template<hysj::_HYSJ_VERSION_NAMESPACE::codes::set S>
struct fmt::formatter<hysj::_HYSJ_VERSION_NAMESPACE::codes::value<S>>{
  using T = hysj::_HYSJ_VERSION_NAMESPACE::codes::value<S>;
  constexpr auto parse(format_parse_context& context)const{
    auto it  = context.begin(),
         end = context.end();
    if(it != end && *it != '}')
      throw fmt::format_error("invalid format");
    return it;
  }
  auto format(const T &e,format_context &context)const{
    if constexpr(S == hysj::_HYSJ_VERSION_NAMESPACE::codes::set::reals)
      return fmt::format_to(context.out(),"{0}", static_cast<double>(e.cell));
    else
      return fmt::format_to(context.out(),"{0}", e.cell);
  }
};

#include<hysj/tools/epilogue.hpp>
