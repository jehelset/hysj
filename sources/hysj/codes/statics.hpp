#pragma once

#include<array>
#include<concepts>
#include<optional>
#include<vector>
#include<type_traits>

#include<hysj/codes/builtins.hpp>
#include<hysj/codes/grammar.hpp>
#include<hysj/grammars.hpp>
#include<hysj/tools/ranges/fold.hpp>
#include<hysj/tools/reflection.hpp>
#include<hysj/tools/type_traits.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes::statics{

struct HYSJ_EXPORT shape{
  std::array<natural,3> iterators;

  friend HYSJ_MIRROR_STRUCT(shape,(iterators));
};

struct HYSJ_EXPORT shapes{
  std::vector<id> iterators;
  std::vector<natural> slots;

  optabs<std::optional<shape>> ops;

  friend HYSJ_MIRROR_STRUCT(shapes,(iterators,slots,ops));

  static shapes make(const syntax &c);
};
HYSJ_EXPORT void resize(const syntax &,shapes &);

namespace concepts{
  template<typename N>
  concept shapes = std::same_as<uncvref_t<unwrap_t<N>>,statics::shapes>;
}

constexpr auto oshape(concepts::shapes auto &&s,id o)
  arrow((unwrap(s).ops.at(o[0]).at(o[1])))
  ;
constexpr auto has_oshape(concepts::shapes auto &&s,id o)
  arrow((oshape(s,o) != std::nullopt))
  ;
template<natural I = 0>
constexpr auto orank(concepts::shapes auto &&s, id o,constant_t<I> = {})
  arrow(((oshape(s,o).value()).iterators[1 + I]))
  ;
template<natural I = 0>
constexpr auto oslot(concepts::shapes auto &&s, id o,constant_t<I> = {})
  arrow(((oshape(s,o).value()).iterators[0] + (I == 0 ? 0 : orank(s,o,0_N))))
  ;
template<natural I = 0>
constexpr auto oslots(concepts::shapes auto &&s, id o,constant_t<I> i = {})
  arrow((std::span{s.slots}.subspan(oslot(s,o,i),orank(s,o,i))))
  ;

template<natural I = 0>
constexpr auto oitrs(concepts::shapes auto&&s,id o,constant_t<I> i = {}){
  return views::map(oslots(s,o,i),
                           [r = std::ref(unwrap(s).iterators)](natural s){
                             return unwrap(r).at(s);
                           });
}

template<natural I = 0>
constexpr auto oitrops(concepts::shapes auto&&s,id o,constant_t<I> i = {})
  arrow((views::map(oitrs(s,o,i),lift((itrop)))))
  ;
template<natural I = 0>
constexpr auto odims(const syntax &c,concepts::shapes auto&&s,id o,constant_t<I> i = {}){
  return views::map(oitrs(s,o,i),[&](id o){ return itrdim(c,o); });
}
template<natural I = 0>
constexpr auto oexts(const syntax &c,concepts::shapes auto&&s,id o,constant_t<I> i = {}){
  return views::map(oitrs(s,o,i),[&](id o){ return itrext(c,o); });
}
template<natural I = 0>
constexpr auto oext(const syntax &c,concepts::shapes auto&&s,id o,constant_t<I> i = {})
  arrow((ranges::prod(oexts(c,s,o,i))))
  ;

constexpr auto islot(const shapes &s, itrsym i){
  return static_cast<natural>(
    std::ranges::distance(std::ranges::begin(s.iterators), 
                          std::ranges::find(s.iterators, i.id())));
}

struct HYSJ_EXPORT types{
  optabs<std::optional<typsym>> ops;
  reltabs<std::optional<typsym>> rels;

  friend HYSJ_MIRROR_STRUCT(types,(ops,rels));

  static types make(const syntax &c);
};
HYSJ_EXPORT void resize(const syntax &,types &);

namespace concepts{
  template<typename N>
  concept types = std::same_as<uncvref_t<unwrap_t<N>>,statics::types>;
}

constexpr set tset(const syntax &c,id d){
  return with_op(
    [&](auto d){
      if constexpr(d.tag() == any_of(optag::cst, optag::lit))
        return just(visitor(
          [](set s){ return s; },
          [](auto ){ return set::integers; })
                    (symdat(c,d)));
      return no<set>;
    },
    d).value();
}
constexpr natural twidth(const syntax &c,id d){
  return with_op(
    [&](auto d){
      if constexpr(d.tag() == any_of(optag::cst, optag::lit))
        return csteval(naturals_c, symdat(c,d));
      return no<natural>;
    },
    d).value();
}
constexpr auto otype(concepts::types auto &&t,id o)
  arrow((unwrap(t).ops[o[0]][o[1]]))
  ;
constexpr auto rtype(concepts::types auto &&t,id a)
  arrow((unwrap(t).rels[a[0]][a[1]]))
  ;

constexpr auto has_otype(concepts::types auto &&t,id o)
  arrow((otype(t,o) != std::nullopt))
  ;
constexpr auto has_rtype(concepts::types auto &&t,id a)
  arrow((rtype(t,a) != std::nullopt))
  ;

constexpr auto odom(const syntax &c,concepts::types auto &&t,id o)
  arrow((grammars::arg(c, otype(t,o).value(), 0_N)))
  ;
constexpr auto rdom(const syntax &c,concepts::types auto &&t,id a)
  arrow((grammars::arg(c, rtype(t,a).value(), 0_N)))
  ;
constexpr auto owidth(const syntax &c,concepts::types auto &&t,id o)
  arrow((twidth(c, grammars::arg(c, otype(t,o).value(), 1_N))))
  ;
constexpr auto rwidth(const syntax &c,concepts::types auto &&t,id a)
  arrow((twidth(c, grammars::arg(c, rtype(t,a).value(), 1_N))))
  ;

constexpr auto oset(const syntax &c,concepts::types auto &&t,id o)
  arrow((tset(c,odom(c,t,o))))
  ;

constexpr auto rset(const syntax &c,concepts::types auto &&t,id a)
  arrow((tset(c,rdom(c,t,a))))
  ;

#define HYSJ_CODES_LITS HYSJ_PRODUCT((HYSJ_CODES_LITERALS)(HYSJ_CODES_WIDTHS))

constexpr id smin(const builtins::lits &l,set s,natural w){
  if(s == set::booleans)
    return l[literal::bot, w];
  else if(s == set::naturals)
    return l[literal::zero, w];
  else
    return l[literal::neginf, w];
}
constexpr id smax(const builtins::lits &l,set s,natural w){
  return (s == set::booleans) ? l[literal::top, w] : l[literal::inf, w];
}
constexpr id tmin(const syntax &c,const builtins::lits &l,id d,natural w){
  return with_op(
    [&](auto o) -> id{
      if constexpr(o.tag() == any_of(optag::cst, optag::lit)){
        return 
          visitor(
            [&](set s){
              if(s == set::booleans)
                return l[literal::bot, w];
              else if(s == set::naturals)
                return l[literal::zero, w];
              else
                return l[literal::neginf, w];
            },
            always(l[literal::zero, w]))
          (symdat(c, o));
      }
      else
        return l[literal::neginf, w];
    },
    d);
}
constexpr id tmax(const syntax &c,const builtins::lits &l,id d,natural w){
  return with_op(
    [&](auto o) -> id{
      if constexpr(o.tag() == any_of(optag::cst, optag::lit))
        return
          visitor(
            [&](set s) -> id{
              return (s == set::booleans) ? l[literal::top, w] : l[literal::inf, w];
            },
            always(id(o)))
          (symdat(c, o));
      else
        return l[literal::inf, w];
    },
    d);
}
constexpr id tmin(const syntax &c,const builtins::all &l,const type &t){
  return tmin(c, l.lits, l.sets[t.set], t.width);
}
constexpr id tmax(const syntax &c,const builtins::all &l,const type &t){
  return tmax(c, l.lits, l.sets[t.set], t.width);
}

constexpr auto omin(const syntax &c,const builtins::lits &l,concepts::types auto &&t,id o)
  arrow((tmin(c,l,odom(c,t,o),owidth(c,t,o))))
  ;
constexpr auto omax(const syntax &c,const builtins::lits &l,concepts::types auto &&t,id o)
  arrow((tmax(c,l,odom(c,t,o),owidth(c,t,o))))
  ;
constexpr auto ospan(const syntax &c,const builtins::lits &l,concepts::types auto &&t,id o)
  arrow((icsteval(c,omax(c,l,t,o)).value() - icsteval(c,omin(c,l,t,o)).value()))
  ;
constexpr auto rmin(const syntax &c,const builtins::lits &l,concepts::types auto &&t,id a)
  arrow((tmin(c,l,rdom(c,t,a),rwidth(c,t,a))))
  ;
constexpr auto rmax(const syntax &c,const builtins::lits &l,concepts::types auto &&t,id a)
  arrow((tmax(c,l,rdom(c,t,a),rwidth(c,t,a))))
  ;
constexpr auto rspan(const syntax &c,const builtins::lits &l,concepts::types auto &&t,id a)
  arrow((icsteval(c,rmax(c,l,t,a)).value() - icsteval(c,rmin(c,l,t,a)).value()))
  ;

constexpr auto with_oset(auto &&f,const syntax &c,const types &t,id o){
  auto s = oset(c,t,o);
  return with_op(
    [&](auto o){
      return with_set(
        [&](auto d){
          return f(o,d);
        },
        s);
    },
    o);
}
constexpr auto with_rset(auto &&f,const syntax &c,const types &t,id a){
  auto s = rset(c,t,a);
  return with_op(
    [&](auto a){
      return with_set(
        [&](auto d){
          return f(a,d);
        },
        s);
    },
    a);
}

constexpr auto otype(const syntax &c,concepts::types auto &&t,id o)
  arrow((type{oset(c,t,o), owidth(c,t,o)}))
  ;
constexpr auto rtype(const syntax &c,concepts::types auto &&t,id r)
  arrow((type{rset(c,t,r), rwidth(c,t,r)}))
  ;

struct HYSJ_EXPORT domain{
  id min, max;

  friend HYSJ_MIRROR_STRUCT(domain,(min, max));

  auto operator<=>(const domain &) const = default;
};

struct HYSJ_EXPORT domains{
  optabs<std::optional<domain>> ops;
  reltabs<std::optional<domain>> rels;

  friend HYSJ_MIRROR_STRUCT(domains,(ops,rels));

  static domains make(const syntax &c);
};

HYSJ_EXPORT void resize(const syntax &,domains &);

namespace concepts{
  template<typename N>
  concept domains = std::same_as<uncvref_t<unwrap_t<N>>,statics::domains>;
}

constexpr auto odomain(concepts::domains auto &&d,id o)
  arrow((unwrap(d).ops[o[0]][o[1]]))
  ;
constexpr auto rdomain(concepts::domains auto &&d,id a)
  arrow((unwrap(d).rels[a[0]][a[1]]))
  ;

constexpr auto has_odomain(const domains &d,id o){
  return odomain(d,o) != std::nullopt;
}
constexpr auto has_rdomain(const domains &d,id a){
  return rdomain(d,a) != std::nullopt;
}

struct HYSJ_EXPORT all{
  statics::types types;
  statics::shapes shapes;
  statics::domains domains;
  
  friend HYSJ_MIRROR_STRUCT(all, (types, shapes, domains));

  static all make(const syntax &c){
    return {
      .types   = statics::types::make(c),
      .shapes  = statics::shapes::make(c),
      .domains = statics::domains::make(c)
    };
  }

  friend void resize(const syntax &s,all &a){
    resize(s, a.types);
    resize(s, a.shapes);
    resize(s, a.domains);
  }
};

} //hysj::codes::statics
template <typename C>
HYSJ_STRUCT_FORMATTER((hysj::_HYSJ_VERSION_NAMESPACE::codes::statics::shape), C);
template <typename C>
HYSJ_STRUCT_FORMATTER((hysj::_HYSJ_VERSION_NAMESPACE::codes::statics::domain), C);
#include<hysj/tools/epilogue.hpp>
