#pragma once
#include<algorithm>
#include<array>
#include<concepts>
#include<functional>
#include<iterator>
#include<optional>
#include<ranges>
#include<utility>
#include<vector>

#include<hysj/codes.hpp>
#include<hysj/codes/algorithms/deepmaps.hpp>
#include<hysj/tools/coroutines/trees.hpp>
#include<hysj/tools/functional/apply.hpp>
#include<hysj/tools/functional/none_of.hpp>
#include<hysj/tools/functional/bind.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/ranges/pins.hpp>
#include<hysj/tools/type_traits.hpp>
#include<hysj/tools/types.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes::lexpands{

//NOTE: iffy name - jeh
namespace concepts{

  template<typename Domain>
  concept domain = std::convertible_to<Domain,std::optional<integer>>;
  
  template<typename Map>
  concept map = requires(Map m,codes::varsym i){
    { m(i) } -> domain;
  };
}

template<typename Map>
struct rules{
  using result = std::optional<id>;
  Map map;
  codes::code &code;

  result impl(codes::eqtag_t,codes::eqsym o){
    auto [l,r] = args(code, o);

    auto sl = codes::varcast(l);
    if(!sl)
      return std::nullopt;
    auto sr = codes::varcast(r);
    if(!sr)
      return std::nullopt;

    std::optional<integer>
      zl = map(*sl),
      zr = map(*sr);
    if(!zl || !zr)
      return std::nullopt;

    const auto N = std::min(*zl,*zr);

    const auto W = owidth(code, o);
    auto w = code.builtins.widths[W];

    const auto A = 
      ranges::vec(
        views::indices(0_i,N),
        [&](integer zd){
          auto z = icst(code,zd,w);
          auto le = eq(code,*sl,z),
            re = eq(code,*sr,z);
          return id(land(code,{le,re}));
        });
    switch(A.size()){
    case 0: return code.builtins.lits[literal::bot, W];
    case 1: return A[0];
    default: return lor(code, A);
    }
  }

  result impl(codes::lnottag_t,codes::eqsym o){
    auto [l,r] = args(code, o);

    auto sl = codes::varcast(l),
      sr = codes::varcast(r);
    auto il = icsteval(code,l),
      ir = icsteval(code,r);

    auto W = owidth(code, o);
    auto w = code.builtins.widths[W];

    auto E =
      [&](codes::varsym s,integer n,auto p,auto make){
        return ranges::vec(
          views::keep(views::indices(0_i,n),p),
          [&](integer zp){
            return id(make(s,icst(code,zp,w)));
          });
      };

    auto A =
      [&] -> std::vector<id>{
        if(!sl && !sr)
          return {};
        else if(sl && ir){
          std::optional<integer> nl = map(*sl);
          if(!nl)
            return {};
          return E(*sl,*nl,none_of(*ir),[&](auto s,auto z){ return eq(code,s,z); });
        }
        else if(il && sr){
          std::optional<integer> nr = map(*sr);
          if(!nr)
            return {};
          return E(*sr,*nr,none_of(*il),[&](auto s,auto z){ return eq(code,z,s); });
        }
        else{
          std::optional<integer> nl = map(*sl),
            nr = map(*sr);
          if(!nl || !nr)
            return {};
          auto El = E(*sl,*nl,always(true),[&](auto s,auto z){ return eq(code,s,z); });
          auto Er = E(*sr,*nr,always(true),[&](auto s,auto z){ return eq(code,s,z); });

          std::vector<id> result;
          for(auto [iel, ier]:std::views::cartesian_product(views::indices(*nl),views::indices(*nr)))
            if(iel != ier)
              result.push_back(land(code,{El[iel],Er[ier]}).id());
          return result;
        }
    }();
    switch(A.size()){
    case 0: return code.builtins.lits[literal::bot, W];
    case 1: return A[0];
    default: return lor(code,A);
    }
  }
  result dispatch(codes::eqsym e){
    return impl(e.tag,e);
  }
  result dispatch(codes::lnotsym c){
    auto [o] = args(code, c);
    auto b = codes::eqcast(o);
    if(!b)
      return std::nullopt;
    return impl(c.tag,*b);
  }
  
  result operator()(id o){
    compile(code, o);
    if(auto c = codes::eqcast(o))
      return dispatch(*c);
    if(auto c = codes::lnotcast(o))
      return dispatch(*c);
    return std::nullopt;
  }
};

template<typename Map>
struct HYSJ_EXPORT subpass{

  using nonidempotent = void;
  
  Map map;
  
  auto run(auto &pass)
    requires (dmaps::is_op_part(pass.part) && pass.step == dmaps::pre_tag())
  {
    return map_value(
      with_op(rules{map,pass.code()},pass.op.index),
      [&](auto op){
        return pass.emit(op);
      });
  }

};

auto map(concepts::map auto m,code &P,auto roots){
 return dmaps::map_roots(
   dmaps::make_state(P,std::move(roots),kumi::tuple{subpass{std::move(m)}}));
}
inline auto map1(concepts::map auto m,code &P,id o){
  return ranges::first(map(std::move(m),P,views::pin(o)));
}

} //hysj::codes::lexpands
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

constexpr auto lexpand(auto &&... x)
  arrow((lexpands::map(fwd(x)...)))
constexpr auto lexpand1(auto &&... x)
  arrow((lexpands::map1(fwd(x)...)))

} //hysj::codes::algorithms
#include<hysj/tools/epilogue.hpp>
