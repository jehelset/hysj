#include<hysj/codes/algorithms/constfolds.hpp>

#include<array>
#include<cmath>
#include<ranges>
#include<optional>

#include<hysj/codes.hpp>
#include<hysj/codes/eval.hpp>
#include<hysj/tools/functional/any_of.hpp>
#include<hysj/tools/functional/optional.hpp>
#include<hysj/tools/functional/variant.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/type_traits.hpp>
#include<hysj/tools/types.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes::constfolds{

bool can_constfold(const code &code,id i){
  return codes::with_op(
    [&](auto i){
      if(i.tag == any_of(optag::var, optag::typ))
        return false;
      else if constexpr(i.tag == optag::cvt){
        return is_always_constant(arg(code, i, 1_N));
      }
      else
        return std::ranges::all_of(argids(code, i), &is_always_constant);

    },
    i);
}

//FIXME: garbage - jeh
std::optional<id> evaluate(code &P,deepmaps::part p,id o){
  return with_op(
    [&]<optag O>(sym<O> o){
      return with_otype(
        [&](auto o_dom, auto o_width) -> std::optional<id> {

          const auto &d = symdat(P, o);
          if constexpr(O == optag::cst)
            if(std::holds_alternative<set>(d) || std::holds_alternative<literal>(d))
              return std::nullopt;

          if constexpr(O == optag::pow){
            auto [base_op,exp_op] = args(P, o);
            if(oset(P,base_op) < set::reals || oset(P,exp_op) < set::reals){
              auto base = evaluate(P,p,base_op);
              auto exp = evaluate(P,p,exp_op);
              if(!base && !exp)
                return std::nullopt;
              return pow(P,base.value_or(base_op),exp.value_or(exp_op));
            }
          }

          if constexpr(!has_cell<o_dom(), o_width()>)
            return std::nullopt;
          else{
            return cceval(P, o_dom, o).transform(
              [&](auto c) -> codes::cstdat{
                if constexpr(o_dom == codes::reals_c){ 
                  if(!std::isfinite(c)) //FIXME: need to discriminate? - jeh
                    return {literal::inf};
                  if(c == std::round(c))
                    return {codes::value<set::integers>{static_cast<cell<set::integers, o_width()>>(std::round(c))}};
                }
                if constexpr(o_dom == any_of(codes::booleans_c))
                  if(p != any_of(deepmaps::part::idx)){
                    if(c)
                      return {literal::top};
                    else 
                      return {literal::bot};
                  }
                if constexpr(o_dom == any_of(codes::integers_c,codes::reals_c))
                  if(p != any_of(deepmaps::part::idx) && c == -1)
                    return {literal::negone};
                if constexpr(o_dom == any_of(codes::naturals_c,codes::integers_c,codes::reals_c))
                  if(p != any_of(deepmaps::part::idx)){
                    if(c == 0)
                      return {literal::zero};
                    else if(c == 1)
                      return {literal::one};
                    else if(c == 2)
                      return {literal::two};
                  }
                return {codes::value<o_dom()>{c}};
              }).and_then(
                [&](auto d_new){
                  if constexpr(O == optag::cst)
                    if(d_new.index() == d.index())
                      return no<id>;
                  return just(cst(P,d_new,P.builtins.widths[o_width()]).id());
                });
          }
        },
        P,o);
    },
    o);
}

} //hysj::codes::constfolds
#include<hysj/tools/epilogue.hpp>
