#pragma once
#include<algorithm>
#include<array>
#include<functional>
#include<optional>
#include<ranges>
#include<tuple>
#include<utility>
#include<variant>
#include<vector>

#include<hysj/codes/algorithms/deepmaps.hpp>
#include<hysj/codes.hpp>
#include<hysj/tools/functional/optional.hpp>
#include<hysj/tools/graphs/adjacency.hpp>
#include<hysj/tools/graphs/elements.hpp>
#include<hysj/tools/graphs/properties.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/ranges/pins.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes::mulfolds{

template<typename Suborder>
struct subpass{
  Suborder suborder;
  
  struct base_op_fn{
    codes::code &code;
    
    template<optag T>
    std::optional<id> operator()(sym<T>)const{
      return std::nullopt;
    }
    std::optional<id> operator()(codes::cstsym s)const{
      return s;
    }
    std::optional<id> operator()(codes::varsym s)const{
      return s;
    }
    std::optional<id> operator()(codes::powsym p)const{
      if(auto s = codes::varcast(arg(code, p, 0_N)))
        return *s;
      if(auto c = codes::cstcast(arg(code, p, 0_N)))
        return *c;
      return std::nullopt;
    }
    std::optional<id> operator()(id index)const{
      return with_op(*this,index);
    }
  };

  struct exponent_expression_fn{
    codes::code &code;

    template<optag T>
    std::optional<id> operator()(sym<T>)const{
      return std::nullopt;
    }
    std::optional<id> operator()(codes::varsym var)const{
      return code.builtins.lits[literal::one, owidth(code, var)];
    }
    std::optional<id> operator()(codes::cstsym var)const{
      return code.builtins.lits[literal::one, owidth(code, var)];
    }
    std::optional<id> operator()(codes::powsym op)const{
      return (id)arg(code, op, 1_N);
    }
    std::optional<id> operator()(id op)const{
      return with_op(*this,op);
    }
  };

  std::optional<dmaps::action> run(auto &pass)
    requires (dmaps::is_op_part(pass.part) && pass.step == dmaps::post_tag())
  {
    auto &code = pass.code();
    auto op = pass.op.index;
    if(codes::optag_of(op) != codes::multag)
      return std::nullopt;

    compile(code, op);

    auto factors = ranges::vec(argids(code, op));

    base_op_fn base_op{code};
    auto base_cmp =
      [&](optag sup,natural sub0,natural sub1){
        if(sup == codes::vartag)
          return suborder(sup,sub0,sub1);
        else
          return code.subcmp(sup,sub0,sub1);
      };
    
    //NOTE: nullopts ordered first - jeh
    std::ranges::stable_sort(
      factors,
      [&](id op0,id op1){
        auto ba0 = base_op(op0);
        auto ba1 = base_op(op1);
        if(ba0 && ba1)
          return codes::subcmp(base_cmp,*ba0,*ba1) == std::partial_ordering::less;
        else
          return ba0 < ba1;
      });

    auto end = std::ranges::end(factors);
    auto it = std::ranges::find_if(factors,[&](auto factor){ return base_op(factor) != std::nullopt; });
    
    //FIXME: use views::chunk - jeh
    std::vector<id> new_factors{
      std::ranges::begin(factors),it
    };

    exponent_expression_fn exponent_expression{code};
    while(it != end){
      auto current_base_op = *base_op(*it);
      auto it_next =
        std::ranges::find_if_not(
          it + 1,end,
          [&](auto factor){ return codes::subcmp(base_cmp,current_base_op,*base_op(factor)) == std::partial_ordering::equivalent; });
      if(std::ranges::distance(it,it_next) == 1)
        new_factors.push_back(*it);
      else{
        auto collected_exponents = ranges::vec(
          std::ranges::subrange(it,it_next),
          [&](auto op){ return *exponent_expression(op); });
        new_factors.push_back(
          pow(code,current_base_op,
              add(code,collected_exponents)));
      }
      it = it_next;
    }
    //FIXME: sketch - jeh!
    return map_if(factors.size() != new_factors.size(),
                  [&]{
                    return pass.emit(
                      [&]->id{
                        if(new_factors.size() == 1)
                          return new_factors[0];
                        return mul(code,new_factors);
                      }());
                  });
  }

};

auto map(code &P,codes::concepts::suborder auto suborder,auto roots){
 return dmaps::map_roots(
   dmaps::make_state(
     P,
     std::move(roots),
     kumi::tuple{subpass{std::move(suborder)}}));
}
inline auto map1(code &P,auto suborder,id root){
  return ranges::first(map(P,std::move(suborder),views::pin(root)));
}

} //hysj::codes::mulfolds
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

constexpr auto mulfold(auto &&... x)
  arrow((mulfolds::map(fwd(x)...)))
constexpr auto mulfold1(auto &&... x)
  arrow((mulfolds::map1(fwd(x)...)))

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
