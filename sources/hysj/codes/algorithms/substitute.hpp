#pragma once

#include<optional>
#include<vector>

#include<kumi/tuple.hpp>

#include<hysj/codes/code.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

struct HYSJ_EXPORT substitute{
  codes::code &code;
  std::vector<kumi::tuple<id, id>> subs;

  std::optional<id> operator()(id)const;

  friend HYSJ_MIRROR_STRUCT(substitute, (subs));
};

auto try_substitute(substitute &S, grammars::concepts::forward_id auto &&I){
  return views::map(I, [&](id i){ return S(i).value_or(i); });
}

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
