#pragma once
#include<array>
#include<algorithm>
#include<functional>
#include<iterator>
#include<memory>
#include<ranges>
#include<utility>
#include<vector>

#include<hysj/codes/code.hpp>
#include<hysj/codes/algorithms/deepmaps.hpp>
#include<hysj/codes/algorithms/deepmetrics.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/ranges/pins.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes::addfolds{

//FIXME:
// route through context.output - jeh
struct HYSJ_EXPORT subpass{

  const dmetrics::state &metrics;
  std::vector<id> domain;
  
  struct term_factors_fn{
    const codes::code &code;
     
    template<optag T>
    std::vector<id> operator()(sym<T> op)const{
      return {op};
    }
    std::vector<id> operator()(codes::mulsym op)const{
      return ranges::vec(argids(code, op));
    }
    std::vector<id> operator()(id op)const{
      return with_op(*this,op);
    }
  };

  std::optional<dmaps::action> run(auto &pass)
    requires (dmaps::is_op_part(pass.part) && pass.step == dmaps::post_tag())
  {
    auto &code = pass.code();

    auto op = pass.op.index;
    if(codes::optag_of(op) != optag::add || !metrics.is_function_of(code,op,domain))
      return std::nullopt;
    
    auto terms        = ranges::vec(argids(code, op));
    auto term_factors = ranges::vec(terms,term_factors_fn{code});
    auto indices      = ranges::vec(views::indices(terms));

    //NOTE: compute suffixes - jeh
    auto suffixes = 
      ranges::vec(
        indices,
        [&](auto index){
          return std::ranges::stable_partition(
            term_factors[index],
            [&](auto factor_index){
              return !metrics.is_function_of(code,factor_index,domain);
            });
        });
    auto prefixes =
      ranges::vec(
        indices,
        [&](auto index){
          return std::ranges::subrange(std::ranges::begin(term_factors[index]),
                                       std::ranges::begin(suffixes[index]));
        });
    
    auto to_suffix = [&](auto index){ return suffixes[index]; };
    //NOTE: stable sort suffixes - jeh
    std::ranges::stable_sort(indices,
                             std::ranges::lexicographical_compare,
                             to_suffix);


    auto width = owidth(code, op);
    
    auto to_prefix_term =
      [&](auto index)->id{
        switch(std::ranges::distance(prefixes[index])){
        case 0:
          return code.builtins.lits[literal::one, width];
        case 1:
          return prefixes[index][0];
        default:
          return mul(code,ranges::vec(prefixes[index]));
        }
      };
    
    auto to_prefix_sum =
      [&](auto it0,auto it1)->id{
        return add(code,ranges::vec(std::ranges::subrange(it0,it1),to_prefix_term));
      };
    auto to_suffix_operation =
      [&](auto suffix)->id{
        if(std::ranges::distance(suffix) == 1)
          return suffix[0];
        return mul(code,ranges::vec(suffix));
      };

    std::vector<id> new_terms;
    //NOTE: chunk by suffixes
    auto it  = std::ranges::begin(indices);
    auto end = std::ranges::end(indices);
    while(it != end){
      auto suffix = to_suffix(*it);
      auto it_next = std::ranges::adjacent_find(it,end,[&](auto s0,auto s1){ return !std::ranges::equal(s0,s1); },to_suffix);
      if(it_next == it || it_next != end)
        ++it_next;
      if(std::ranges::distance(it,it_next) == 1)
        new_terms.push_back(terms[*it]);
      else if(std::ranges::empty(suffix))
        std::ranges::copy(
          views::join(
            views::map(std::ranges::subrange(it, it_next),
                                  [&](auto index)-> auto &{
                                    return prefixes[index];
                                  })),
          std::back_inserter(new_terms));
      else{
        auto suffix_operation = to_suffix_operation(suffix);
        auto prefix_sum = to_prefix_sum(it,it_next);
        new_terms.push_back(mul(code,{ prefix_sum, suffix_operation }));
      }
      it = it_next;
    }
    if(terms.size() == new_terms.size())
      return std::nullopt;

    return pass.emit(
      [&]->id{
        if(new_terms.size() == 1)
          return new_terms[0];
        return add(code,new_terms);
      }());
  }

};

auto map(code &P,auto roots,std::vector<id> domain){
  auto dmet_state = std::make_unique<dmetrics::state>();
  auto *dmet_state_ptr = dmet_state.get();
  return dmaps::map_roots(
    dmaps::make_state(P,std::move(roots),
                      kumi::tuple{
                        subpass{
                          .metrics = *dmet_state_ptr,
                          .domain = ranges::vec(domain)
                        },
                        dmetrics::subpass{std::move(dmet_state)}
                      }));
}
inline auto map1(code &P,id o,auto domain){
  return ranges::first(map(P,views::pin(o),domain));
}

auto map(code &P,auto roots){
  return map(P,roots,std::vector<id>{});
}
inline auto map1(code &P,id o){
  return ranges::first(map(P,views::pin(o),std::vector<id>{}));
}

} //hysj::codes::addfolds
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

constexpr auto addfold(auto &&... x)
  arrow((addfolds::map(fwd(x)...)))
constexpr auto addfold1(auto &&... x)
  arrow((addfolds::map1(fwd(x)...)))

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
