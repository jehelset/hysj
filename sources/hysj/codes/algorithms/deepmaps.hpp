#pragma once
#include<functional>
#include<optional>
#include<tuple>
#include<type_traits>
#include<utility>
#include<vector>

#include<kumi/tuple.hpp>

#include<hysj/codes.hpp>
#include<hysj/tools/coroutines.hpp>
#include<hysj/tools/coroutines/trees.hpp>
#include<hysj/tools/functional/any_of.hpp>
#include<hysj/tools/functional/bind.hpp>
#include<hysj/tools/functional/optional.hpp>
#include<hysj/tools/functional/unwrap.hpp>
#include<hysj/tools/graphs/algorithms/deepsearch.hpp>
#include<hysj/tools/graphs/elements.hpp>
#include<hysj/tools/graphs/incidence.hpp>
#include<hysj/tools/graphs/indirected_graphs.hpp>
#include<hysj/tools/graphs/properties.hpp>
#include<hysj/tools/ranges/map.hpp>
#include<hysj/tools/reflection.hpp>
#include<hysj/tools/type_traits.hpp>
#include<hysj/tools/types.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes::deepmaps{

//FIXME:
//  cleanup & simplify!
//  structure the states (op, arg, form)...
//  - jeh
  
#define HYSJ_CODES_DEEPMAPS_PARTS \
  (prog)                          \
  (root)                          \
  (arg)                           \
  (idx)                           \
  (dim)                           \
  (dom)

enum class part : unsigned {
  HYSJ_SPLAT(0,HYSJ_CODES_DEEPMAPS_PARTS)
};
HYSJ_MIRROR_ENUM(part,HYSJ_CODES_DEEPMAPS_PARTS)

#define HYSJ_LAMBDA(id)                                   \
  inline constexpr auto id##_tag = constant_c<part::id>;
HYSJ_MAP_LAMBDA(HYSJ_CODES_DEEPMAPS_PARTS)
#undef HYSJ_LAMBDA
#define HYSJ_LAMBDA(id)                         \
  using id##_tag_type = constant_t<part::id>;
HYSJ_MAP_LAMBDA(HYSJ_CODES_DEEPMAPS_PARTS)
#undef HYSJ_LAMBDA

template<part C>
consteval bool is_alg_part(constant_t<C>){
  return C != prog_tag;
}

template<part C>
consteval bool is_op_part(constant_t<C>){
  return C == any_of(root_tag,arg_tag);
}

template<part C>
consteval bool is_arg_part(constant_t<C>){
  return C == any_of(arg_tag,idx_tag,dim_tag,dom_tag);
}

#define HYSJ_CODES_DEEPMAPS_STEPS   \
  (pre)                             \
  (inter)                           \
  (post)
  
enum class step : unsigned {
  HYSJ_SPLAT(0,HYSJ_CODES_DEEPMAPS_STEPS)
};
HYSJ_MIRROR_ENUM(step,HYSJ_CODES_DEEPMAPS_STEPS)

#define HYSJ_LAMBDA(id)                                  \
  inline constexpr auto id##_tag = constant_c<step::id>;
HYSJ_MAP_LAMBDA(HYSJ_CODES_DEEPMAPS_STEPS)
#undef HYSJ_LAMBDA
#define HYSJ_LAMBDA(id)                         \
  using id##_tag_type = constant_t<step::id>;
HYSJ_MAP_LAMBDA(HYSJ_CODES_DEEPMAPS_STEPS)
#undef HYSJ_LAMBDA

#define HYSJ_CODES_DEEPMAPS_ACTIONS \
  (noop)                              \
  (recv)                              \
  (emit)                              \
  (done)
  
enum class action : unsigned {
  HYSJ_SPLAT(0,HYSJ_CODES_DEEPMAPS_ACTIONS)
};
HYSJ_MIRROR_ENUM(action,HYSJ_CODES_DEEPMAPS_ACTIONS)

#define HYSJ_LAMBDA(id)                                       \
  inline constexpr auto id##_tag = constant_c<action::id>;
HYSJ_MAP_LAMBDA(HYSJ_CODES_DEEPMAPS_ACTIONS)
#undef HYSJ_LAMBDA
#define HYSJ_LAMBDA(id)                         \
  using id##_tag_type = constant_t<action::id>;
HYSJ_MAP_LAMBDA(HYSJ_CODES_DEEPMAPS_ACTIONS)
#undef HYSJ_LAMBDA

consteval auto to_part(auto dw_tag){
  if constexpr(dw_tag() == any_of(graphs::dw::start,graphs::dw::stop))
    return root_tag;
  else
    return arg_tag;
}

consteval auto to_step(auto dw_tag){
  if constexpr(dw_tag() == any_of(graphs::dw::start,graphs::dw::down))
    return pre_tag;
  else
    return post_tag;
}

constexpr auto to_arg_part(codes::reltag tag){
  switch(tag){
  case codes::reltag::dom: return part::dom;
  case codes::reltag::dim: return part::dim;
  case codes::reltag::idx: return part::idx;
  default: return part::arg;
  }
}

template<typename DW,typename Roots,typename Subpasses> 
struct pass_state{

  static constexpr integer N = kumi::size_v<unwrap_t<Subpasses>>;

  DW dw;

  codes::code &code;
  Roots roots;
  std::optional<grammars::vert> root;
  Subpasses subpasses;

  std::vector<deepmaps::step> steps;
  std::vector<deepmaps::action> actions;
  std::vector<deepmaps::action> transmits;
  std::vector<std::optional<grammars::vert>> targets;
  std::vector<std::optional<grammars::vert>> sources;
  std::vector<std::array<bool,N>> root_blockers;
  std::vector<std::array<bool,N>> arg_blockers;

  template<integer I>
  constexpr auto &subpass()noexcept{
    return kumi::get<I>(subpasses);
  }

  void resize(){
    const auto O = graphs::order(code.graph);
    const auto S = graphs::size(code.graph);
    graphs::resize(steps,O,step::inter);
    graphs::resize(actions,O,action::noop);
    graphs::resize(transmits,S,action::noop);
    graphs::resize(targets,O,std::nullopt);
    graphs::resize(sources,O,std::nullopt);
    graphs::resize(root_blockers,O,std::array<bool,N>{});
    graphs::resize(arg_blockers,S,std::array<bool,N>{});
  }

};

//FIXME: structure this crap - jeh
template<typename... U>
auto make_state(code &P,auto O,kumi::tuple<U...> subpasses){
  auto &G = P.graph;
  
  auto D = graphs::dw::make_state(graphs::out_tag,std::cref(G));
  auto S = graphs::vprop(G,step::pre);
  auto A = graphs::vprop(G,action::noop);
  auto R = graphs::eprop(G,action::noop);
  auto I = graphs::vprop(G,std::optional<grammars::vert>{});
  auto X = graphs::vprop(G,std::optional<grammars::vert>{});
  auto RB = graphs::vprop(G,std::array<bool,sizeof...(U)>{});
  auto AB = graphs::eprop(G,std::array<bool,sizeof...(U)>{});
  return pass_state{
    .dw            = std::move(D),
    .code          = P,
    .roots         = std::move(O),
    .root          = std::nullopt,
    .subpasses     = std::move(subpasses),
    .steps         = std::move(S),
    .actions       = std::move(A),
    .transmits     = std::move(R),
    .targets       = std::move(I),
    .sources       = std::move(X),
    .root_blockers = std::move(RB),
    .arg_blockers  = std::move(AB)
  };
}

template<part C>
struct pass_op{
  grammars::vert vertex;
  id index;
};

template<part C>
requires (is_arg_part(constant_c<C>))
struct pass_op<C>{
  grammars::edge edge;
  grammars::vert vertex;
  id index;
};

template<integer D,part C,step S,typename StateRef,typename Control>
struct pass{
  static constexpr auto co_ret = constant_c<D>;
  static constexpr auto part   = constant_c<C>;
  static constexpr auto step   = constant_c<S>;
  
  using State = uncvref_t<unwrap_t<StateRef>>;

  template<natural I>
  using Subpass = uncvref_t<unwrap_t<kumi::element_t<I,decltype(State::subpasses)>>>;

  static constexpr auto N = State::N;
  
  StateRef state_ref;
  Control control;

  pass_op<C> op;

  auto &state()const{ return unwrap(state_ref); }
  auto operator->()const{ return &state(); }

  constexpr auto &code()noexcept{
    return state().code;
  }
  constexpr auto &code()const noexcept{
    return state().code;
  }

  auto source(grammars::vert op)const{
    auto &sources = state().sources;
    return graphs::get(sources,op);
  }
  auto first_source(grammars::vert op)const{
    auto &sources = state().sources;
    std::optional<grammars::vert> first_source{};
    while(auto source = graphs::get(sources,op)){
      if(op == *source)
        return source;
      first_source = op = *source;
    }
    return first_source;
  }

  auto first_root()const{
    auto root = *state().root;
    return first_source(root).value_or(root);
  }

  auto target(grammars::vert op)const{
    auto &targets = state().targets;
    return graphs::get(targets,op);
  }
  auto last_target(grammars::vert op)const{
    auto &targets = state().targets;
    std::optional<grammars::vert> last_target;
    while(auto emit = graphs::get(targets,op)){
      if(op == *emit)
        return emit;
      last_target = op = *emit;
    }
    return last_target;
  }

  auto last_op(grammars::vert op)const{
    return last_target(op).value_or(op);
  }

  void resize(){
    state().resize();
  }


  auto noop(auto &pass_co)const{
    return co::nil<1>(pass_co);
  }
  auto emit(grammars::vert op_emit,std::optional<deepmaps::step> op_emit_step = std::nullopt){
    resize();
    graphs::put(state().steps,op.vertex,step::post);
    graphs::put(state().actions,op.vertex,action::emit);
    graphs::put(state().targets,op.vertex,op_emit);
    graphs::put(state().sources,op_emit,op.vertex);
    if(op_emit_step)
      graphs::put(state().steps,op_emit,*op_emit_step);
    return action::emit;
  }
  auto emit(id op_emit,std::optional<deepmaps::step> op_emit_step = std::nullopt){
    return emit(objvert(code(), op_emit),op_emit_step);
  }

  template<integer I>
  static consteval bool subpass_idempotent(){
    return !requires{ typename Subpass<I>::nonidempotent; };
  }

  template<integer I>
  bool subpass_blocked()const{
    if constexpr(part == part::prog)
      return false;
    else if constexpr(part == part::root){
      auto &root_blockers = state().root_blockers;
      return graphs::get(root_blockers,first_root())[I];
    }
    else{
      auto &arg_blockers = state().arg_blockers;
      return graphs::get(arg_blockers,op.edge)[I];
    }
  }
  template<integer I>
  void subpass_block()const{
    if constexpr(part == part::prog)
      return;
    else if constexpr(part == part::root){
      auto &root_blockers = state().root_blockers;
      graphs::get(root_blockers,first_root())[I] = true;
    }
    else{
      auto &arg_blockers = state().arg_blockers;
      graphs::get(arg_blockers,op.edge)[I] = true;
    }
  }
  void subpass_unblock()const{
    if constexpr(part == part::prog)
      return;
    else if constexpr(part == part::root){
      auto &root_blockers = state().root_blockers;
      graphs::get(root_blockers,first_root()).fill(false);
    }
    else{
      auto &arg_blockers = state().arg_blockers;
      graphs::get(arg_blockers,op.edge).fill(false);
    }
  }

  template<deepmaps::step U = step>
  static consteval integer I_first(){ return (U == step::post) ? N - 1_i : 0_i; }
  template<deepmaps::step U = step>
  static consteval integer I_step (){ return (U == step::post) ? -1_i : 1_i;    }
  template<deepmaps::step U = step>
  static consteval integer I_last (){ return (U == step::post) ? -1_i : N;      }

  template<integer I>
  std::optional<action> subpass_run() requires(part != part::prog) {
    auto &subpass = state().template subpass<I>();
    if constexpr(requires { subpass.run(*this); })
      if(!subpass_blocked<I>())
        return subpass.run(*this);
    return std::nullopt;
  }
  template<integer I>
  auto subpass_run(auto &run_co) requires(part == part::prog) {
    auto &subpass = state().template subpass<I>();
    auto subpass_co = co::get<action>(run_co);
    if constexpr(requires { subpass.run(subpass_co,*this); })
      return subpass.run(subpass_co,*this);
    else
      return co::nil<1>(subpass_co);
  }

  template<integer I = I_first()>
  void subpass_skip(){
    if constexpr(I == I_last())
      return;
    else{
      auto &subpass = state().template subpass<I>();
      if constexpr(requires { subpass.skip(*this); })
        if(!subpass_blocked<I>())
          subpass.skip(*this);
      subpass_skip<I + I_step()>();
    }
  }

  template<integer I,deepmaps::action A,deepmaps::step R>
  auto subpass_recv(){
    if constexpr(I == I_last<R>())
      return;
    else{
      auto &subpass = state().template subpass<I>();
      if constexpr(requires {subpass.recv(*this,constant_c<A>,constant_c<R>);})
        subpass.recv(*this,constant_c<A>,constant_c<R>);
      subpass_recv<I + I_step<R>(),A,R>();
    }
  }

  template<deepmaps::action A>
  auto ctrl_recv(auto &run_co,constant_t<A> a){
    auto &ctrl = unwrap(control);
    auto ctrl_co =
      [&]{
        if constexpr(part == part::prog && step == step::post && A == action::noop)
          return co::pipe(run_co);
        else
          return co::get<none>(run_co);
      }();
    if constexpr(requires { ctrl(ctrl_co,*this,a); })
      return ctrl(ctrl_co,*this,a);
    else
      return co::nil<1>(ctrl_co);
  }

  template<integer I = N,deepmaps::action A>
  auto send(auto &run_co,constant_t<A> a){
    if constexpr(I != I_last<post_tag>())
      subpass_recv<I + I_step<post_tag>(),A, step::post>();
    if constexpr(I != I_last<pre_tag>())
      subpass_recv<I + I_step<pre_tag>() ,A, pre_tag>();
    return ctrl_recv(run_co,a);
  }

  auto again(auto &run_co){
    if constexpr(part == part::root){
      state().root = last_op(op.vertex); //NOTE: set up next root - jeh
      return co::ret<co_ret>(run_co,graphs::dw::stop);
    }
    else{
      static_assert(is_arg_part(part));
      auto &targets = state().targets;
      auto &graph = code().graph;
      auto target = graphs::get(targets,op.vertex);
      HYSJ_ASSERT(target);
      if(*target != op.vertex){
        HYSJ_ASSERT(graphs::tail(graph,op.edge) != *target);
        graph.oreset(op.edge,*target);
      }
      return co::ret<co_ret>(run_co,graphs::dw::left);
    }
  }
  template<typename RunGet>
  auto skip(RunGet) & ->co::api_t<RunGet>{
    auto &skip_co = HYSJ_CO(RunGet);
    subpass_skip();
    subpass_unblock();
    if constexpr(part == part::root && step == step::pre){
      co_await ctrl_recv(skip_co,done_tag);
      state().root = std::nullopt;
    }
    auto n = (part == part::root) ? graphs::dw::stop : graphs::dw::right;
    co_final_await co::ret<co_ret>(skip_co, n);
  }
  auto stop(auto &run_co){
    return co::nil<co_ret>(run_co);
  }

  template<integer I = I_first(),typename DMapGet>
  auto run(DMapGet) & -> co::api_t<DMapGet> {
    auto &run_co = HYSJ_CO(DMapGet);

    auto &targets   = state().targets;
    auto &sources   = state().sources;
    auto &steps     = state().steps;
    auto &actions   = state().actions;
    auto &transmits = state().transmits;
    auto &graph     = code().graph;

    auto op_recv = [&]{
      if(graphs::get(actions,op.vertex) == action::noop)
        graphs::put(actions,op.vertex,action::recv);
      if(graphs::get(targets,op.vertex) == std::nullopt)
        graphs::put(targets,op.vertex,op.vertex);
      if(graphs::get(sources,op.vertex) == std::nullopt)
        graphs::put(sources,op.vertex,op.vertex);
    };
    auto op_emit = [&]{
     if constexpr(part != part::prog && I != I_last())
       if constexpr(!subpass_idempotent<I>())
         subpass_block<I>();
    };

    auto arg_send = [&](action A){
      for(auto op_edge:graphs::viincident(graph,op.vertex)){
        auto op_recv = graphs::head(graph,op_edge);
        if constexpr(part == part::prog){
          if(graphs::get(steps,op_recv) != step::pre)
            graphs::put(steps,op_recv,step::inter);
        }
        else if(graphs::get(steps,op_recv) == step::post)
          continue;
        auto reaction = graphs::get(transmits,op_edge);
        graphs::put(transmits,op_edge,std::max(reaction,A));
      }
    };
    auto arg_recv = [&](auto op_recv){
      auto op_action = graphs::get(actions,op_recv);
      for(auto edge:graphs::voincident(graph,op_recv)){
        const auto arg_reaction = graphs::get(transmits,edge); 
        if(arg_reaction == action::noop)
          continue;
        graphs::put(transmits,edge,action::noop);
        if(arg_reaction == action::emit){
          const auto arg = graphs::head(graph,edge);
          if(auto last_arg = last_target(arg);
             last_arg && arg != last_arg && op_recv != last_arg)
            graph.oreset(edge,*last_arg);
        }
        if(op_action < arg_reaction){
          graphs::put(actions,op_recv,action::recv);
          op_action = arg_reaction;
        }
      }
      return op_action != action::noop;
    };

    if constexpr(part == part::prog){
      if constexpr(I == I_last()){
        resize(); //FIXME: suss - jeh
        for(auto op_recv:graphs::vertex_ids(graph))
          if(graphs::get(steps,op_recv) == step::inter && arg_recv(op_recv)){
            op.vertex = op_recv;
            op.index  = objid(code(), op.vertex);
            co_await ctrl_recv(run_co,recv_tag);
          }
        co_await send(run_co,done_tag);
        co_final_await stop(run_co);
      }
      else{
        auto subpass_co = subpass_run<I>(run_co); //FIXME: how to do targets here! - jeh
        while(co_await subpass_co == action::emit){
          op_emit();
          co_await ctrl_recv(run_co,emit_tag);
          arg_send(action::emit);
          graphs::put(steps,op.vertex,step::post);
        }
        co_final_await run<I + I_step()>(co::pipe(run_co));
      }
    }
    else{
      if constexpr(I == I_first()){
        //NOTE:
        //  ensure that at the start of a pre-pass, the args
        //  are updated to their last target.
        //  - jeh
        if constexpr(step == step::pre){
          op.vertex = last_op(op.vertex);
          op.index  = objid(code(), op.vertex);
          const auto op_step = graphs::get(steps,op.vertex);
          if(op_step == step::post)
            co_final_await skip(co::pipe(run_co));
          if constexpr(part == part::root)
            state().dw.root = op.vertex;
          if(op_step == step::pre)
            co_await send(run_co,noop_tag);
          graphs::put(steps,op.vertex,step::inter);
        }

        if(arg_recv(op.vertex))
          op_recv();
      }
      if constexpr(I == I_last()){
        if(graphs::get(actions,op.vertex) == action::recv){
          co_await send(run_co,recv_tag);
          arg_send(recv_tag);
          graphs::put(actions,op.vertex,action::noop);
        }
        if constexpr(step == step::post){
          co_await send(run_co,done_tag);
          graphs::put(steps,op.vertex,step::post);
          subpass_unblock();
          if constexpr(part == part::root)
            state().root = std::nullopt;
        }
        co_final_await stop(run_co);
      }
      else{
                     
        if(subpass_run<I>() == action::emit){
          op_emit();
          co_await send<I>(run_co,emit_tag);
          arg_send(emit_tag);
          co_final_await again(run_co);
        }
        co_final_await run<I + I_step()>(co::pipe(run_co));
      }
    }
  }
};

template<integer D,part C,step S,typename State,typename Ctrl>
constexpr auto make_pass(constant_t<D>,constant_t<C>,constant_t<S>,State state_ref,Ctrl ctrl){
  auto op = [&]->pass_op<C>{
    auto &dmap = unwrap(state_ref);
    auto &code = dmap.code;
    auto &dw = dmap.dw;
    if constexpr(C == part::prog)
      return {};
    else if constexpr(C == part::root)
      return {*dmap.root,objid(code, *dmap.root)};
    else
      return {dw.edge(),dw.head(),objid(code, dw.head())};
  }();
  return pass<D,C,S,State,Ctrl>{.state_ref = state_ref, .control = ctrl, .op = op};
}

template<typename DMapGet>
auto map(DMapGet,auto dmap_ref,auto dmap_control) -> co::api_t<DMapGet> {

  static constexpr auto dmap_co_depth = -co::depth_v<DMapGet>;
  auto &dmap_co = HYSJ_CO(DMapGet);
  auto &dmap = unwrap(dmap_ref);

  auto &dw = dmap.dw;
  auto &code = dmap.code;

  auto make_pass = bind<-2,-1>(lift((deepmaps::make_pass)),std::ref(dmap),std::ref(dmap_control));

  {
    auto pass = make_pass(constant_c<dmap_co_depth>,prog_tag,pre_tag); 
    co_await pass.run(co::get<none>(dmap_co));
  }
  {
    auto dw_roots = views::map(dmap.roots,[&](auto o){ return objvert(code, o); });
    auto dw_root_it = std::ranges::begin(dw_roots);
    auto dw_root_end = std::ranges::end(dw_roots);
    co_await dwalk(
      co::get<none>(dmap_co),
      std::ref(dw),
      [&]<typename DWGet>(DWGet,auto dw_tag,auto dw_ref) -> co::api_t<DWGet> {
        
        auto &dw_co = HYSJ_CO(DWGet);
        auto &dw = unwrap(dw_ref);

        constexpr auto dw_ctrl_co_depth = -co::depth_v<DWGet>;
        constexpr auto part = to_part(dw_tag); auto part_ = part(); (void)part_;
        constexpr auto step = to_step(dw_tag); auto step_ = step(); (void)step_;

        //FIXME: oof - jeh
        if constexpr(part == part::root){
          if constexpr(step == step::pre)
            if(!dmap.root){
              if(dw_root_it == dw_root_end)
                co_await co::nil<dmap_co_depth>(dw_co);
              dmap.root = *dw_root_it++;
            }
          auto pass = make_pass(constant_c<dw_ctrl_co_depth + 1>,part,step);
          co_await pass.run(co::pipe(dw_co));
        }
        else{
          auto arg_part = to_arg_part(codes::reltag_of(relid(code, dw.edge())));
          auto make_arg_pass = bind<0,-1>(make_pass,constant_c<dw_ctrl_co_depth + 1>,step);
          if(arg_part == part::arg){ //FIXME: urgh... - jeh
            auto pass = make_arg_pass(arg_tag);
            co_await pass.run(co::pipe(dw_co));
          }
          else if(arg_part == part::idx){
            auto pass = make_arg_pass(idx_tag);
            co_await pass.run(co::pipe(dw_co));
          }
          else if(arg_part == part::dim){
            auto pass = make_arg_pass(dim_tag);
            co_await pass.run(co::pipe(dw_co));
          }
          else{
            HYSJ_ASSERT(arg_part == part::dom);
            auto pass = make_arg_pass(dom_tag);
            co_await pass.run(co::pipe(dw_co));
          }
        }
      });
  }
  {
    auto pass = make_pass(constant_c<dmap_co_depth + 1>,prog_tag,post_tag);
    co_await pass.run(co::pipe(dmap_co));
  }
  co_final_await co::nil<1>(dmap_co);
}

inline constexpr auto map_roots(auto dmap_ref){
  return map(
    co_get<std::optional<id>>{},
    std::move(dmap_ref),
    [](auto dmap_get,const auto &pass,done_tag_type) requires (pass.part == part::root){
      const auto root = pass.op.vertex;
      const bool has_source = pass.source(root) != std::nullopt;
      return co::put<0>(co::promise(dmap_get),maybe(has_source,pass.op.index));
    });
}
inline constexpr auto map_prog(auto dmap_ref){
  return map(
    co_get<kumi::tuple<id,id>>{},
    std::move(dmap_ref),
    [](auto dmap_get,const auto &pass,emit_tag_type) requires (pass.part == part::prog) {
      auto &code = pass.code();
      HYSJ_ASSERT(pass.target(pass.op.vertex));
      const auto target = objid(code, *pass.target(pass.op.vertex));
      return co::put<0>(co::promise(dmap_get),kumi::make_tuple(pass.op.index,target));
    });
}


template<typename Domain,typename Control>
struct with_last_target{
  Domain domain;
  Control control;
  bool terminal = true;

  auto operator()(auto pass_get,const auto &pass,emit_tag_type)
    requires (pass.part == part::prog && pass.step == step::post){
    terminal = false;
    return co::nil<1>(pass_get);
  }

  template<typename CtrlGet>
  auto operator()(CtrlGet,const auto &pass,done_tag_type) -> co::api_t<CtrlGet>
    requires (pass.part == part::prog && pass.step == step::post){
    auto &ctrl_co = HYSJ_CO(CtrlGet);
    auto &p = pass.code();
    for(id s_index:domain){
      auto o_index =
        map_value(
          pass.last_target(objvert(p, s_index)),
          [&](auto o){
            return objid(p, o);
          });
      co_await control(co::get<none>(ctrl_co),terminal,o_index);
    }
    co_final_await co::nil<1>(ctrl_co);
  }
};


} //hysj::codes::deepmaps
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

namespace dmaps = deepmaps;

constexpr auto dmap(auto &&... x)
  arrow((dmaps::map(fwd(x)...)))

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
