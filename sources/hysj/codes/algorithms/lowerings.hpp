#pragma once
#include<algorithm>
#include<array>
#include<functional>
#include<iterator>
#include<optional>
#include<ranges>
#include<utility>
#include<vector>

#include<hysj/codes/algorithms/deepmaps.hpp>
#include<hysj/codes.hpp>
#include<hysj/tools/ranges/cat.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/ranges/pins.hpp>
#include<hysj/tools/functional/ignore.hpp>
#include<hysj/tools/functional/bind.hpp>
#include<hysj/tools/types.hpp>
#include<hysj/tools/type_traits.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes::lowerings{

struct HYSJ_EXPORT rules{ 
  //TODO:
  //  memoize common constants used during. use a
  //  symbolic_literals type to group'em.
  //  - jeh
  codes::code &code;
  
  std::optional<id> impl(ignore_t,ignore_t,ignore_t,ignore_t){
    return std::nullopt;
  }

  std::optional<id> impl(auto op,natural width,ignore_t,auto operands)
    requires (op.tag == optag::neg)
  {
    auto &[expression] = operands;

    if(is_zero(code,expression).value_or(false))
      return code.builtins.lits[literal::zero, width];
    return mul(
      code,
      {
        code.builtins.lits[literal::negone, width],
        expression
      });
  }
  std::optional<id> impl(auto op,natural width,ignore_t,auto operands)
    requires (op.tag == optag::sub)
  {
    
    auto terms = ranges::vec(
      views::catcast<id>(
        views::one(kumi::get<0>(operands)),
        kumi::get<1>(operands)));
    
    for(auto i:views::indices(1uz,terms.size()))
      terms[i] = mul(
        code,
        {
          code.builtins.lits[literal::negone, width],
          terms[i]
        });
    return add(code,terms);
  }
  std::optional<id> impl(auto op,natural width,ignore_t,auto operands)
    requires (op.tag == any_of(optag::add,optag::mul))
  {
    auto operand = ranges::vec(kumi::get<0>(operands));
    if constexpr(op.tag == optag::mul)
      if(ranges::contains_if(operand,[&](auto o){ return is_zero(code,o).value_or(false); }))
        return code.builtins.lits[literal::zero, width];
    const auto old_arity = operand.size();
    operand.erase(
      std::ranges::remove_if(
        operand,
        [&](auto o){
          return is_identity(op.tag,code,o).value_or(false);
        }).begin(),
      std::end(operand));
    const auto new_arity = operand.size();
    switch(operand.size()){
      case 0: return identity(code.builtins.lits, op, width);
      case 1: return operand[0];
      default:
        if(old_arity != new_arity)
          return objadd<op.tag()>(code, operand);
    }
    return std::nullopt;
  }
  std::optional<id> impl(auto op,natural width,ignore_t,auto operands)
    requires (op.tag == optag::div)
  {
    auto &[dividend,divisor] = operands;
    if(is_zero(code,dividend).value_or(false))
      return code.builtins.lits[literal::zero, width];
    if(is_zero(code,divisor).value_or(false))
      return code.builtins.lits[literal::inf, width];
    if(is_one(code,divisor).value_or(false))
      return dividend;
    return mul(
      code,
      {
        dividend,
        pow(code,divisor,code.builtins.lits[literal::negone, width])
      });
  }
  std::optional<id> impl(auto op,natural width,ignore_t,auto operands)
    requires (op.tag == optag::pow)
  {
    auto &[base,exponent] = operands;

    if(is_zero(code,base).value_or(false))
      return code.builtins.lits[literal::zero, width];
    if(is_zero(code,exponent).value_or(false))
      return code.builtins.lits[literal::one, width];
    if(is_one(code,exponent).value_or(false) || is_one(code,base).value_or(false))
      return base;
    if(is_negone(code,base).value_or(false))
      if(auto odd = is_odd(code,exponent)){
        if(*odd)
          return code.builtins.lits[literal::negone, width];
        else
          return code.builtins.lits[literal::one, width];
      }
    return std::nullopt;
  }
  std::optional<id> operator()(auto id){
    compile(code, id);
    return impl(id,owidth(code, id), symdat(code, id),args(code, id));
  }
};

struct HYSJ_EXPORT subpass{
  auto run(auto &pass)
    requires (dmaps::is_op_part(pass.part) && pass.step == dmaps::post_tag())
  {
    return map_value(
      with_op(rules{pass.code()},pass.op.index),
      [&](auto op){
        return pass.emit(op);
      });
  }
};

auto map(code &P,auto roots){
 return dmaps::map_roots(
   dmaps::make_state(P,std::move(roots),kumi::tuple{subpass{}}));
}
inline auto map1(code &P,id root){
  return ranges::first(map(P,views::pin(root)));
}

} //hysj::codes::lowerings
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

constexpr auto lower(auto &&... x)
  arrow((lowerings::map(fwd(x)...)))
constexpr auto lower1(auto &&... x)
  arrow((lowerings::map1(fwd(x)...)))

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
