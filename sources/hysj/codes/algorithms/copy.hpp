#pragma once
#include<algorithm>
#include<optional>
#include<functional>
#include<ranges>
#include<utility>
#include<span>
#include<vector>

#include<kumi/tuple.hpp>

#include<hysj/codes/code.hpp>
#include<hysj/tools/coroutines/trees.hpp>
#include<hysj/tools/functional/always.hpp>
#include<hysj/tools/functional/apply.hpp>
#include<hysj/tools/functional/bind.hpp>
#include<hysj/tools/functional/compose.hpp>
#include<hysj/tools/functional/none_of.hpp>
#include<hysj/tools/functional/unwrap.hpp>
#include<hysj/tools/ranges/pins.hpp>
#include<hysj/tools/ranges/utility.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

namespace concepts{
  template<typename T>
  concept copy_src = grammars::concepts::input_id<T>;

  template<typename F>
  concept copy_pred = requires(F &&f,id o,id r,id a){
    { std::invoke(f, o) } -> testable;
    { std::invoke(f, o, r, a) } -> testable;
  };
}

namespace _{

  template<typename P>
  struct copy_impl{

    codes::code &code;

    P predicate;

    std::vector<kumi::tuple<id, std::optional<id>>> cache;
    std::vector<std::optional<id>> argstack;
  
    auto new_argids(id o)const{
      return std::span{argstack}.last(argdeg(code, o));
    }
    auto argids(id o)const{
      return
        views::map(
          std::views::iota(0_i, argdeg(code, o)),
          [&,o](auto i){
            return new_argids(o)[i].value_or(grammars::argids(code, o)[i]);
          });
    }
    template<codes::optag O>
    auto args(sym<O> o)const{
      return grammars::args(code, o, argids(o));
    }
    bool has_new_args(id o)const{
      return std::ranges::any_of(new_argids(o), has_value);
    }
    void pop_args(id o){
      argstack.resize(argstack.size() - argdeg(code, o));
    }
    auto args(id i)const{
      return std::span{argstack}.last(grammars::argdeg(code, i));
    }

    auto cached(id i)const{
      if(auto it = std::ranges::find(cache, i, lift((get<0>)));
         it != std::ranges::end(cache))
        return get<1>(*it);
      return no<id>;
    }

    auto push_arg(id o,std::optional<id> c){
      cache.push_back(kumi::make_tuple(o, c));
      argstack.push_back(c);
    }

    auto pre(id o){
      if(auto c = cached(o)){
        argstack.push_back(c);
        return false;
      }
      if(!std::invoke(predicate, o)){
        push_arg(o, {});
        return false;
      }
      return true;
    }
    auto pre(id o,id r,id a){
      if(!pre(a))
        return false;
      if(!std::invoke(predicate, o, r, a)){
        push_arg(o, {});
        return false;
      }
      return true;
    }

    auto post(id o){
      auto p = 
        with_op(
          [&](auto o) -> id{
            return apply(
              [&](auto... A){
                return objadd<o.tag>(code, symdat(code, o), A...);
              },
              args(o));
          },
          o);
      pop_args(o);
      argstack.push_back(p);
      return just(none{});
    }
    auto post(id o, id r, id a){
      return post(a);
    }

    auto operator()(id o){
      grammars::walk<none>(code, lift((pre)(&)), lift((post)(&)), o);
      if(argstack.size() != 1)
        return no<id>;
      return just(argstack.front().value_or(o));
    }
  };

}
template<typename CoGet>
auto co_copy_if(CoGet,code &c,concepts::copy_src auto s,concepts::copy_pred auto p) -> co::api_t<CoGet> {
  auto &co_this = HYSJ_CO(CoGet);

  static constexpr auto co_depth = -co::depth_v<CoGet>;

  _::copy_impl f{c, std::move(p)};
  for(auto o:s)
    co_await co::put<>(co_this, f(o).value());

  co_final_await co::nil<>(co_this);
}

auto copy_if(code &c,concepts::copy_src auto &&s,concepts::copy_pred auto p){
  return co_copy_if(co_get<id>{}, c, std::views::all(fwd(s)), std::move(p));
}
auto copy(code &c,concepts::copy_src auto &&s){
  return copy_if(c,std::views::all(fwd(s)), always(true));
}

auto copy1_if(code &c,id s,concepts::copy_pred auto p){
  return ranges::first(copy_if(c,views::pin(s),std::move(p)));
}
auto copy1(code &c,id s){
  return ranges::first(copy(c,views::pin(s)));
}

auto clone(code &c,concepts::copy_src auto &&s){
  return copy_if(
    c,fwd(s),
    overload(
      compose<0>(none_of(optag::rfl,optag::var,optag::lit,optag::typ,optag::cst,optag::der,optag::itr),
                 lift((codes::optag_of))),
      always(true)));
}
auto clone1(code &c,auto root){
  return ranges::first(clone(c,views::pin(root)));
}

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
