#pragma once

#include<optional>

#include<hysj/codes/code.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

HYSJ_EXPORT
std::optional<exprfam> symdiff(code &,dersym);

} //hysj::codes
#include<hysj/tools/epilogue.hpp>

