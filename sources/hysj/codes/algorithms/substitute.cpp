#include<hysj/codes/algorithms/substitute.hpp>

#include<algorithm>
#include<vector>
#include<optional>
#include<ranges>

#include<kumi/tuple.hpp>

#include<hysj/grammars.hpp>
#include<hysj/codes/grammar.hpp>
#include<hysj/codes/code.hpp>
#include<hysj/tools/functional/optional.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

struct substitute_{

  codes::code &code;
  const std::vector<kumi::tuple<id, id>> &subs;

  std::vector<std::optional<id>> argstack;

  auto new_argids(id o)const{
    return std::span{argstack}.last(argdeg(code, o));
  }
  auto argids(id o)const{
    return
      views::map(
        std::views::iota(0_i, argdeg(code, o)),
        [&,o](auto i){
          return new_argids(o)[i].value_or(grammars::argids(code, o)[i]);
        });
  }
  template<codes::optag O>
  auto args(sym<O> o)const{
    return grammars::args(code, o, argids(o));
  }
  bool has_new_args(id o)const{
    return std::ranges::any_of(new_argids(o), has_value);
  }

  template<optag O>
  id new_op(sym<O> o){
    return apply(
      [&](auto... a){
        return objadd<o.tag>(code, symdat(code, o), a...);
      },
      args(o));
  }
  template<optag O>
  auto try_new_op(sym<O> o){
    if(has_new_args(o))
      return just(new_op(o));
    return no<id>;
  }
  void pop_args(id o){
    argstack.resize(argstack.size() - argdeg(code, o));
  }
  auto pre(id o){
    if(auto it = std::ranges::find_if(subs, [&](const auto &s){ return get<0>(s) == o; });
       it != std::ranges::end(subs)){
      argstack.push_back(get<1>(*it));
      return false;
    }
    return true;
  }
  auto pre(id o,id r,id a){
    return pre(a);
  }
  auto post(id o){
    auto r = codes::with_op(lift((try_new_op)(&)), o);
    pop_args(o);
    argstack.push_back(r);
  }
  auto post(id o,id r,id a){
    post(a);
  }

  auto operator()(id o){
    auto n = argstack.size();
    grammars::walk<none>(code,
                         lift((pre)(&)),
                         compose<>(never, lift((post)(&))),
                         o);
    if(argstack.size() == n)
      return no<id>;
    else if(argstack.size() != n + 1)
      throw std::runtime_error("whoops");
    auto u = argstack.back();
    argstack.pop_back();
    return u;
  }
};

std::optional<id> substitute::operator()(id root)const{
  substitute_ s{code, subs};
  return s(root);
}

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
