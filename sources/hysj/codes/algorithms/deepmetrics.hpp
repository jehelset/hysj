#pragma once
#include<algorithm>
#include<optional>
#include<functional>
#include<ranges>
#include<set>
#include<utility>
#include<vector>

#include<kumi/tuple.hpp>

#include<hysj/tools/types.hpp>
#include<hysj/tools/reflection.hpp>
#include<hysj/tools/functional/any_of.hpp>
#include<hysj/tools/graphs/incidence.hpp>
#include<hysj/tools/graphs/properties.hpp>
#include<hysj/codes.hpp>
#include<hysj/codes/algorithms/deepmaps.hpp>
#include<hysj/tools/ranges/pins.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/ranges/cat.hpp>
#include<hysj/tools/ranges/map.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes::deepmetrics{

using tour = std::vector<grammars::edge>;
using subtour = std::ranges::subrange<std::ranges::iterator_t<const tour>>;

struct metrics{
  subtour tour;

  friend HYSJ_MIRROR_STRUCT(metrics,(tour));
};

struct state{
  natural max_arity = 0;
  deepmetrics::tour tour;
  natural tour_first = 0;
  std::vector<std::optional<kumi::tuple<natural,natural>>> tour_indices;

  friend HYSJ_MIRROR_STRUCT(state,(max_arity,tour,tour_first,tour_indices));

  subtour tour_of(grammars::vert o)const{
    auto [first,last] = *graphs::get(tour_indices,o);
    return {tour.begin() + first,tour.begin() + last};
  }
  subtour current_tour()const{
    return {tour.begin() + tour_first,tour.end()};
  }
  
  metrics operator[](grammars::vert operation)const{
    return {
      .tour = tour_of(operation)
    };
  }
  auto optour(const code &p,grammars::vert o)const{
    return views::bookend(
      objid(p, o),
      views::map(tour_of(o),[&](grammars::edge e){
        return relargid(p, relid(p, e));
      }),
      objid(p, o));
  }

  bool is_function_of(const code &p,id f,const std::vector<id> &X)const{
    auto tour = tour_of(objvert(p, f));
    if(X.empty())
      return codes::optag_of(f) == optag::var || ranges::contains_if(
        tour,
        [&](grammars::edge e){
          return codes::optag_of(relargid(p,relid(p, e))) == optag::var;
        });
    else
      return ranges::contains(X,f) || ranges::contains_if(
        tour,
        [&](grammars::edge e){
          return ranges::contains(X,relargid(p, relid(p, e)));
        });
  }

  void reset(code &S){
    max_arity = 0_n;
    tour.clear();
    tour.reserve(2 * graphs::order(S.graph));
    tour_first = 0_n;
    tour_indices =
      graphs::vprop(S.graph,
                    std::optional<kumi::tuple<natural,natural>>{});
  }
  void resize(code &S){
    const auto order = graphs::order(S.graph);
    graphs::resize(tour_indices,order,std::nullopt);
    codes::resize(S);
  }
  void open(grammars::vert o){
    graphs::put(tour_indices,o,kumi::tuple<natural,natural>{tour.size(),tour.size()});
  }
  void open(const code &P,grammars::edge a){
    auto o = relargvert(P, a);
    tour.push_back(a);
    open(o);
  }
  void close(code &P,grammars::vert o){
    auto o_op = objid(P, o);
    max_arity = std::max<natural>(max_arity,argdeg(P, o_op));
    compile(P,o_op);
    if(!graphs::get(tour_indices,o).has_value())
      return;
    kumi::get<1>(*graphs::get(tour_indices,o)) = tour.size();
  }
  void close(code &P,grammars::edge a){
    auto o = relargvert(P, a);
    close(P,o);
    tour.push_back(a);
  }
  void reopen(const code &P,grammars::edge a){
    auto o = relargvert(P, a);
    const auto [first,last] = graphs::get(tour_indices,o).value();
    tour.reserve(tour.size() + (last - first) + 2);
    tour.insert(tour.end(),a);
    tour.insert(tour.end(),tour.begin() + first,tour.begin() + last);
    tour.insert(tour.end(),a);
  }

  void orphan(const code &P,grammars::vert o,natural i_begin,natural i_end){

    HYSJ_ASSERT(i_begin <= i_end);
    HYSJ_ASSERT(i_end == tour.size());
    const auto M = i_end - i_begin,
      N = i_begin - tour_first;
    //NOTE: right shift current tour by M - jeh
    //FIXME: only need half - jeh
    std::set<grammars::vert> garbage{};
    for(natural j = tour_first;j != tour.size();++j){
      auto oa = relobjvert(P, tour[j]);
      HYSJ_ASSERT(graphs::get(tour_indices,oa).has_value());
      auto &[j_begin,j_end] = *graphs::get(tour_indices,oa);
      if(j_begin < tour_first)
        continue;
      if(!garbage.insert(oa).second)
        continue;
      if(j_begin < i_begin || (j_begin == i_begin && oa != o)){
        j_begin += M;
        j_end += M;
      }
      else{
        j_begin -= N;
        j_end -= N;
      }
    }
    std::ranges::rotate(tour.begin() + tour_first,
                        tour.begin() + tour_first + N,
                        tour.end());
    HYSJ_ASSERT(tour_first + M <= tour.size());
    tour_first += M;
  }

  void orphan(const code &P,grammars::vert o){
    if(!graphs::get(tour_indices,o).has_value())
      return;
    auto &[i_begin,i_end] = *graphs::get(tour_indices,o);
    orphan(P,o,i_begin,i_end);
  }
  void orphan(const code &P,grammars::edge a){
    auto o = relargvert(P, a);
    if(!graphs::get(tour_indices,o).has_value())
      return;
    auto &[i_begin,i_end] = *graphs::get(tour_indices,o);
    orphan(P,o,i_begin - 1,i_end + 1);
  }
  
};

template<typename State>
struct subpass{
  State state;
  
  auto run(auto pass_co,const auto &pass)
    requires (pass.part == dmaps::prog_tag() && pass.step == dmaps::pre_tag())
  {
    unwrap(state).reset(pass.code());
    return pass.noop(pass_co);
  }
  auto run(const auto &pass)
    requires (dmaps::is_alg_part(pass.part) && pass.step == dmaps::pre_tag())
  {
    auto &s = unwrap(state);
    if constexpr(pass.part == dmaps::root_tag()){
      s.tour_first = s.tour.size();
      s.open(pass.op.vertex);
    }
    else
      s.open(pass.code(),pass.op.edge);
    return std::nullopt;
  }
  auto run(const auto &pass)
    requires (dmaps::is_alg_part(pass.part) && pass.step == dmaps::post_tag())
  {
    auto &s = unwrap(state);
    if constexpr(pass.part == dmaps::root_tag())
      s.close(pass.code(),pass.op.vertex);
    else
      s.close(pass.code(),pass.op.edge);
    return std::nullopt;
  }
  auto recv(const auto &pass,auto action,auto step)
    requires (dmaps::is_alg_part(pass.part) && action == dmaps::emit_tag())
  {
    auto &s = unwrap(state);
    auto &P = pass.code();
    s.resize(P);
    auto o = pass.op.vertex;
    auto o_new = pass.last_op(o);

    compile(P,objid(P, o_new));
    if constexpr(step == dmaps::post_tag()){
      if constexpr(pass.part == dmaps::root_tag())
        s.close(P,o);
      else
        s.close(P,pass.op.edge);
    }
    if constexpr(pass.part == dmaps::root_tag())
      s.orphan(P,o);
    else
      s.orphan(P,pass.op.edge);
  }
  auto skip(const auto &pass)
    requires (dmaps::is_arg_part(pass.part))
  {
    unwrap(state).reopen(pass.code(),pass.op.edge);
  }
};

auto map(code &P,auto roots){
  deepmetrics::state state;
  dmap(co_none{},
       dmaps::make_state(
         P,
         roots,
         kumi::tuple{
           subpass{.state = std::ref(state)}
         }),
       [](const auto &dmap_co){ return co::nil<1>(dmap_co); })();
  return state;
}
inline auto map1(code &P,id o){
  return map(P,views::pin(o));
}

} //hysj::codes::deepmetrics
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

namespace dmetrics = deepmetrics;

constexpr auto dmetric(auto &&... x)
  arrow((dmetrics::map(fwd(x)...)))
constexpr auto dmetric1(auto &&... x)
  arrow((dmetrics::map1(fwd(x)...)))

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
