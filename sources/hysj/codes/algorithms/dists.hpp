#pragma once
#include<algorithm>
#include<functional>
#include<ranges>
#include<optional>
#include<utility>
#include<vector>

#include<hysj/codes/algorithms/deepmetrics.hpp>
#include<hysj/codes/algorithms/deepmaps.hpp>
#include<hysj/tools/functional/any_of.hpp>
#include<hysj/tools/functional/none_of.hpp>
#include<hysj/tools/ranges/fold.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/type_traits.hpp>
#include<hysj/tools/types.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes::dists{

#define HYSJ_CODES_DISTS \
  (mul,add)              \
  (pow,mul)              \
  (land,lor)             \
  (lor,land)             \
  (lnot,lnot)

inline constexpr auto distributor_tags = kumi::pop_front(kumi::tuple{0
    #define HYSJ_LAMBDA(distributor,...)        \
      , codes::distributor##tag
    HYSJ_MAP_LAMBDA(HYSJ_CODES_DISTS)
    #undef HYSJ_LAMBDA
  });
      
namespace traits{
  template<optag T>
  struct collector_tag;

  #define HYSJ_LAMBDA(distributor,collector,...)                \
    template<> struct collector_tag<codes::distributor##tag>{ \
      static constexpr codes::collector##tag_t value{};    \
    };
  HYSJ_MAP_LAMBDA(HYSJ_CODES_DISTS)
  #undef HYSJ_LAMBDA
  
}
template<optag T>
constexpr optag collector_tag = traits::collector_tag<T>::value;

template<optag O,optag o>
consteval bool can_potentially_distribute(constant_t<O>,constant_t<o>){
  if constexpr(O == optag::mul)
    return o == any_of(optag::mul,optag::pow);
  else
    return o == O;
}

using distributor_arg_sets = std::vector<std::vector<id>>;

struct context{
  const dmetrics::state &metrics;
  const std::vector<id> &domain;
  codes::code &code;
  
  bool is_constant(id o)const{
    return !metrics.is_function_of(code,o,domain);
  }
};

//FIXME: garbage - jeh
struct collect_distributor_arg_sets_fn:context{
  distributor_arg_sets &result;

  natural operator()(codes::powtag_t,codes::powsym o){
    auto [base,exponent] = args(code, o);
    if(codes::optag_of(base) != optag::mul)
      return 0;
    auto base_factors = ranges::vec(arg(code, *codes::mulcast(base), 0_N));
    auto variables = std::ranges::stable_partition(base_factors,
                                                   [&](auto o){ return context::is_constant(o); });
    const auto variable_count = std::ranges::size(variables);
    if(variable_count == 0)
      return 0;

    auto new_pow = [&](id base){
      return pow(code,base,exponent);
    };
    result.push_back({});
    if(auto constants = std::ranges::subrange(std::ranges::begin(base_factors),
                                              variables.begin());
       !std::ranges::empty(constants)){
      auto constant_pow =
        [&]->id{
          if(std::ranges::size(constants) == 1)
            return new_pow(ranges::first(constants));
          else
            return new_pow(mul(code,ranges::vec(views::cast<id>(constants))));
        }();
      result.back().push_back(constant_pow);
    }
    for(auto variable:variables){
      if(codes::optag_of(variable) == optag::mul)
        for(auto arg:ranges::vec(argids(code, variable)))
          result.back().push_back(new_pow(arg));
      else
        result.back().push_back(new_pow(variable));
    }
    return 2;
  }
  
  natural operator()(codes::multag_t,codes::powsym o){
    auto [base,exponent] = args(code, o);

    const bool constant_base = context::is_constant(base);
    if(codes::addcast(base) == std::nullopt
       || !(map_value(icsteval(code,exponent),[](integer i){ return i >= 0; }).value_or(false))){
      result.push_back({id(o)});
      return constant_base ? 0 : 1;
    }
    auto count = static_cast<natural>(*icsteval(code,exponent));
    result.resize(count,ranges::vec(argids(code, base)));
    return constant_base ? 0 : count;
  }
  template<optag D>
  natural operator()(constant_t<D> distributor,sym<D> o){
    static constexpr auto C = collector_tag<D>;
    
    natural non_constant = 0;
    for(auto oprn:ranges::vec(argids(code, o))){
      if(context::is_constant(oprn)){
        result.push_back(std::vector{oprn});
        continue;
      }
      const auto oprn_tag = codes::optag_of(oprn);
      if(oprn_tag == D)
        non_constant += operator()(distributor,*grammars::refcast(distributor,oprn));
      else if(oprn_tag == C){
        ++non_constant;
        result.push_back({});
        for(auto grandoprn:argids(code, oprn))
          if(codes::optag_of(grandoprn) == C)
            std::ranges::copy(argids(code, grandoprn),
                              std::back_inserter(result.back()));
          else
            result.back().push_back(grandoprn);
      }else{
        if constexpr(D == optag::mul)
          if(oprn_tag == optag::pow){
            non_constant += operator()(distributor,*codes::powcast(oprn));
            continue;
          }
        ++non_constant;
        result.push_back(std::vector{oprn});
      }
    }
    return non_constant;
  }

  natural operator()(codes::lnottag_t,codes::lnotsym o){
    auto [oprn] = args(code, o);
    if(codes::optag_of(oprn) == none_of(optag::land,optag::lor))
      return 0;

    result.push_back(std::vector{id(oprn)});
    auto grandoprns = ranges::vec(argids(code, oprn));
    result.push_back(
      ranges::vec(
        grandoprns,
        [&](auto le){
          return id(lnot(code,le));
        }));
    return 2;
  }
  
};

template<optag D>
std::optional<distributor_arg_sets>
collect_distributor_arg_sets(constant_t<D> d,context c,auto o){
  distributor_arg_sets U{};
  if(collect_distributor_arg_sets_fn{{c},U}(d,o) > 1)
    if(std::ranges::any_of(U,[](auto N){ return N > 1; },std::ranges::size))
      return U;
  return std::nullopt;
}

auto distribute_operation(codes::powtag_t D,context &c,const auto &U){
  static constexpr optag C = collector_tag<D>;
  return codes::objadd<C>(c.code,U.back());
}

id distribute_operation(codes::lnottag_t,context &c,const auto &U){
  if(codes::optag_of(U.front().front()) == codes::landtag)
    return lor(c.code,U.back());
  else
    return land(c.code,U.back());
}

template<optag D>
auto distribute_operation(constant_t<D>,context &c,const auto &U){
   static constexpr optag C = collector_tag<D>;
  
  const auto N = ranges::vec(U,std::ranges::ssize);
  std::vector<std::ptrdiff_t> I(N.size(),0);

  auto f =
    [&]->id{
      std::vector<id> u_index;
      for(auto j:views::indices(I)){
        auto u = U[j][I[j]];
        if(codes::optag_of(u) == D)
          std::ranges::copy(argids(c.code, u),std::back_inserter(u_index));
        else
          u_index.push_back(u);
      }
      return objadd<D>(c.code, u_index);
    };
  
  std::vector<id> C_arg;
  auto M = std::ranges::fold_left(N,std::size_t{1},std::multiplies<>{});
  C_arg.reserve(M);
  const auto j0 = std::ranges::ssize(N) - 1;
  while(M--){
    C_arg.push_back(f());
    auto j = j0;
    while(!(I[j] = ((I[j] + 1) % N[j])))
      if(j-- == 0)
        break;
  }
  return objadd<C>(c.code,C_arg);
}

template<optag D>
struct rules{
  dists::context context;

  template<optag d>
  std::optional<id> operator()(sym<d> o){
    if constexpr(can_potentially_distribute(constant_c<D>,o.tag))
      if(auto U = collect_distributor_arg_sets(constant_c<D>,context,o))
        return distribute_operation(constant_c<D>,context,*U);
    return std::nullopt;
  }
  std::optional<id> operator()(id o){
    return with_op(*this,o);
  }
};

template<optag O>
struct subpass{

  const dmetrics::state &metrics;
  std::vector<id> domain;

  auto run(auto &pass)
    requires (dmaps::is_op_part(pass.part) && pass.step == dmaps::post_tag())
  {
    return map_value(rules<O>{{metrics,domain,pass.code()}}(pass.op.index),
                     [&](auto op){
                       return pass.emit(op);
                     });
  }
};

#define HYSJ_LAMBDA(distributor,...)                                \
  using distributor##_subpass = subpass<codes::distributor##tag>;
HYSJ_MAP_LAMBDA(HYSJ_CODES_DISTS)
#undef HYSJ_LAMBDA

inline auto default_domain(){ return std::vector<id>{}; }

template<optag distributor>
auto map(constant_t<distributor>,code &P,auto roots,auto domain){
  auto dmet_state = std::make_unique<dmetrics::state>();
  auto *dmet_state_ptr = dmet_state.get();
  return dmaps::map_roots(
    dmaps::make_state(P,std::move(roots),
                      kumi::tuple{
                        subpass<distributor>{
                          *dmet_state_ptr,
                          ranges::vec(domain)
                        },
                        dmetrics::subpass{std::move(dmet_state)}
                      }));
}
template<optag distributor>
inline auto map1(constant_t<distributor> d,code &P,id o,auto domain){
  return ranges::first(map(d,P,views::pin(o),domain));
}

template<optag D>
inline constexpr auto map(constant_t<D> d,code &P,auto roots){
  return map(d,P,roots,default_domain());
}
template<optag D>
inline auto map1(constant_t<D> d,code &P,id o){
  return map1(d,P,o,default_domain());
}

#define HYSJ_LAMBDA(distributor,...)                                                    \
  inline constexpr auto distributor##map = bind<>(lift((map)),codes::distributor##tag); \
  inline constexpr auto distributor##map1 = bind<>(lift((map1)),codes::distributor##tag);
HYSJ_MAP_LAMBDA(HYSJ_CODES_DISTS)
#undef HYSJ_LAMBDA

} //hysj::codes::dists
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

inline constexpr auto dist  = lift((dists::map));
inline constexpr auto dist1 = lift((dists::map1));

#define HYSJ_LAMBDA(distributor,...) \
  inline constexpr auto distributor##dist  = dists::distributor##map;\
  inline constexpr auto distributor##dist1 = dists::distributor##map1;
HYSJ_MAP_LAMBDA(HYSJ_CODES_DISTS)
#undef HYSJ_LAMBDA

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
