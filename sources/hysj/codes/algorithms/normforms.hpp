#pragma once

#include<functional>
#include<memory>
#include<vector>
#include<tuple>
#include<utility>

#include<kumi/tuple.hpp>

#include<hysj/codes/algorithms/addfolds.hpp>
#include<hysj/codes/algorithms/argfolds.hpp>
#include<hysj/codes/algorithms/argsorts.hpp>
#include<hysj/codes/algorithms/constfolds.hpp>
#include<hysj/codes/algorithms/deepmaps.hpp>
#include<hysj/codes/algorithms/deepmetrics.hpp>
#include<hysj/codes/algorithms/dists.hpp>
#include<hysj/codes/algorithms/lowerings.hpp>
#include<hysj/codes/algorithms/merges.hpp>
#include<hysj/codes/algorithms/mulfolds.hpp>
#include<hysj/codes.hpp>
#include<hysj/tools/coroutines.hpp>
#include<hysj/tools/coroutines/trees.hpp>
#include<hysj/tools/ranges/utility.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes::normforms{

template<codes::concepts::suborder Suborder>
inline auto make_subpasses(auto domain_,Suborder suborder){
  std::vector<id> domain = ranges::vec(domain_);
  auto [dmetrics,dmetrics_state] =
    []{
      auto state = std::make_unique<dmetrics::state>(dmetrics::state{});
      auto state_ptr = state.get();
      return std::make_pair(dmetrics::subpass{.state = std::move(state)},state_ptr);
    }();
  return kumi::make_tuple(
    argfolds::subpass{},mulfolds::subpass{.suborder = suborder},constfolds::subpass{.metrics = *dmetrics_state},lowerings::subpass{},
    argsorts::subpass{.metrics = *dmetrics_state,.suborder = suborder},
    dists::pow_subpass{.metrics = *dmetrics_state, .domain = domain},
    dists::mul_subpass{.metrics = *dmetrics_state, .domain = domain},
    addfolds::subpass{.metrics = *dmetrics_state, .domain = domain },
    merges::subpass{.metrics = *dmetrics_state,.suborder = suborder},
    std::move(dmetrics));
}

template<typename Suborder,typename Roots,typename Domain>
struct HYSJ_EXPORT state{
  codes::code &code;
  Suborder suborder;
  Roots roots;
  Domain domain;
};

//NOTE: crap - jeh
template<typename NFormGet>
auto normalize(NFormGet,auto nform_ref,auto control) -> co::api_t<NFormGet> {

  auto &nform_co = HYSJ_CO(NFormGet);
  auto &nform = unwrap(nform_ref);

  std::vector<id> roots = ranges::vec(nform.roots);
  std::vector<bool> mapped(roots.size(),false);

  bool terminal = false;
  while(!terminal){
    natural root_index = 0;
    auto dmap_co =
      dmaps::map(
        co::get<none>(nform_co),
        dmaps::make_state(nform.code,
                          views::all(roots),
                          make_subpasses(nform.domain,nform.suborder)),
        dmaps::with_last_target{
          views::all(roots),
          [&]
            <typename CtrlGet>(CtrlGet,bool terminal_pass,std::optional<id> image) mutable
            -> co::api_t<CtrlGet> {
              auto &ctrl_co = HYSJ_CO(CtrlGet);
              if(image){
                roots[root_index] = *image;
                mapped[root_index] = true;
              }
              root_index++;
              if(terminal_pass)
                terminal = true;
              co_final_await co::nil<1>(ctrl_co);
            }});
    co_await dmap_co;
  }
  for(auto root_index:views::indices(roots))
    co_await control(co::get<none>(nform_co),root_index,maybe(mapped[root_index],roots[root_index]));
  co_final_await co::nil<1>(nform_co);
}

auto map(code &P,codes::concepts::suborder auto suborder,auto roots,auto domain){
  return normalize(
    co_get<kumi::tuple<natural,std::optional<id>>>{},
    state{
      .code     = P,
      .suborder = std::move(suborder),
      .roots    = std::move(roots),
      .domain   = std::move(domain)
    },
    [](auto ctrl_co,natural target_index,std::optional<id> target){
      return co::put<0>(co::promise(ctrl_co),kumi::make_tuple(target_index,target));
    });
}
auto map(code &P,auto f,auto roots){
  return map(P,std::move(f),std::move(roots),std::vector<id>{});
}
inline auto map1(code &P,auto f,id o,auto domain){
  return kumi::get<1>(ranges::first(map(P,std::move(f),views::pin(o),std::move(domain))));
}
inline auto map1(code &P,auto f,id o){
  return map1(P,std::move(f),o,std::vector<id>{});
}

} //hysj::codes
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

constexpr auto normform(auto &&... x)
  arrow((normforms::map(fwd(x)...)))
constexpr auto normform1(auto &&... x)
  arrow((normforms::map1(fwd(x)...)))

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
