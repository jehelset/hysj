#pragma once
#include<algorithm>
#include<memory>
#include<optional>
#include<ranges>
#include<tuple>
#include<utility>

#include<hysj/tools/coroutines/trees.hpp>
#include<hysj/codes/algorithms/deepmetrics.hpp>
#include<hysj/codes/algorithms/deepmaps.hpp>
#include<hysj/codes.hpp>
#include<hysj/tools/functional/apply.hpp>
#include<hysj/tools/functional/unwrap.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes::argsorts{

constexpr std::optional<natural> op_commutes_from(optag o){
  return codes::with_optag( //FIXME: wrong for div and sub... - jeh
    [](auto o){
      using enum optag;
      if constexpr(o() == any_of(lit, typ, itr, cst, var, rfl, rot, sel, put,
                                 noop, loop, exit, fork, then, dev, on, to, api))
        return no<natural>;
      else
        return (grammars::objvar<o()> ? just(grammars::objnum<o()> - 1) : no<natural>);
    },
    o);
}

template<typename Suborder>
struct subpass{

  const dmetrics::state &metrics;
  Suborder suborder;
   
  std::optional<dmaps::action> run(auto &pass)
    requires (dmaps::is_op_part(pass.part) && pass.step == dmaps::post_tag())
  {
    auto &code = pass.code();

    auto op = pass.op.index;

    auto maybe_commutes_from = op_commutes_from(codes::optag_of(op));
    if(!maybe_commutes_from)
      return std::nullopt;
    
    auto commutes_from = *maybe_commutes_from;
 
    auto rels = ranges::vec(relids(code, op));
    if(rels.size() <= commutes_from)
      return std::nullopt;

    auto order =
      [&](auto u0,auto u1){
        return std::ranges::lexicographical_compare(
          metrics.optour(code,objvert(code, relargid(code, u0))),
          metrics.optour(code,objvert(code, relargid(code, u1))),
          [&](auto v0,auto v1){
            return codes::subcmp(suborder,v0,v1) == std::partial_ordering::less;
          });
      };

    auto commutative_rels = std::views::drop(rels,commutes_from);
    if(std::ranges::is_sorted(commutative_rels,order))
      return std::nullopt;

    std::ranges::stable_sort(commutative_rels,order);
    return pass.emit(
        with_op(
          [&](auto op) -> id{
            return apply(
              [&](auto &&... rels){
                return objadd<op.tag()>(code, symdat(code, op), fwd(rels)...);
              },
              grammars::relargs(code, op, rels));
          },
          op));
  }
  
};

auto map(code &P,codes::concepts::suborder auto suborder,auto roots){
  auto dmet_state = std::make_unique<dmetrics::state>();
  auto *dmet_state_ptr = dmet_state.get();
  return dmaps::map_roots(
    dmaps::make_state(P,std::move(roots),
                      kumi::tuple{
                        subpass{
                          .metrics = *dmet_state_ptr,
                          .suborder = std::move(suborder)
                        },
                        dmetrics::subpass{std::move(dmet_state)}}));
}
inline auto map1(code &P,codes::concepts::suborder auto suborder,id o){
  return ranges::first(map(P,std::move(suborder),views::pin(o)));
}

} //hysj::codes::argsorts
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

constexpr auto argsort(auto &&... x)
  arrow((argsorts::map(fwd(x)...)))
constexpr auto argsort1(auto &&... x)
  arrow((argsorts::map1(fwd(x)...)))

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
