#pragma once
#include<algorithm>
#include<array>
#include<functional>
#include<memory>
#include<optional>
#include<ranges>
#include<utility>
#include<vector>

#include<hysj/tools/graphs/adjacency.hpp>
#include<hysj/tools/graphs/elements.hpp>
#include<hysj/tools/graphs/properties.hpp>
#include<hysj/codes/algorithms/deepmaps.hpp>
#include<hysj/codes/algorithms/deepmetrics.hpp>
#include<hysj/codes.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/ranges/pins.hpp>
#include<hysj/tools/functional/variant.hpp>
#include<hysj/tools/functional/none_of.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes::constfolds{

constexpr bool is_always_constant(id i){
  return codes::optag_of(i) == any_of(optag::lit, optag::cst);
}
HYSJ_EXPORT bool can_constfold(const code &,id);

HYSJ_EXPORT std::optional<id> evaluate(code &,deepmaps::part,id);

struct HYSJ_EXPORT subpass{

  const dmetrics::state &metrics;

  std::optional<dmaps::action> run(auto &pass)
  requires (pass.part == any_of(dmaps::part::root,
                                dmaps::part::arg,
                                dmaps::part::idx)
            && pass.step == dmaps::post_tag())
  {
    auto &code = pass.code();

    auto op = pass.op.index;

    if(!can_constfold(code, op))
      return std::nullopt;

    return map_value(
      evaluate(code,pass.part,op),
      [&](auto op){
        return pass.emit(op);
      });
  }

};

auto map(code &P,auto roots){
  auto dmet_state = std::make_unique<dmetrics::state>();
  auto *dmet_state_ptr = dmet_state.get();
  return dmaps::map_roots(
    dmaps::make_state(P,std::move(roots),
                      kumi::tuple{
                        subpass{
                          .metrics = *dmet_state_ptr
                        },
                        dmetrics::subpass{std::move(dmet_state)}}));

}
inline auto map1(code &P,id o){
  return ranges::first(map(P,views::pin(o)));
}

} //hysj::codes::constfolds
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

constexpr auto constfold(auto &&... x)
  arrow((constfolds::map(fwd(x)...)))
constexpr auto constfold1(auto &&... x)
  arrow((constfolds::map1(fwd(x)...)))

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
