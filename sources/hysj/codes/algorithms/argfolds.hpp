#pragma once
#include<algorithm>
#include<array>
#include<functional>
#include<iterator>
#include<optional>
#include<ranges>
#include<utility>
#include<vector>

#include<hysj/codes/algorithms/deepmaps.hpp>
#include<hysj/codes.hpp>
#include<hysj/tools/ranges/pins.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/functional/bind.hpp>
#include<hysj/tools/functional/any_of.hpp>
#include<hysj/tools/functional/none_of.hpp>
#include<hysj/tools/types.hpp>
#include<hysj/tools/type_traits.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes::argfolds{

template<optag o>
consteval bool can_fold(constant_t<o>){
  using enum optag;
  return o == any_of(neg, add,// sub, 
                     mul, div,pow,
                     lnot,land,lor);
}

struct HYSJ_EXPORT rules{
  using result = std::optional<id>;
  //TODO:
  //  use pre_op
  //  - jeh
  codes::code &code;

  //TODO:
  //  add more lowering. log rules etc...
  ///  - jeh
  result impl(auto op,auto oprns)
    requires (op.tag() == any_of(optag::neg,optag::lnot))
  {
    auto [oprn] = oprns;
    if(auto twin = grammars::refcast(op.tag, oprn)){
      auto [grandoprns] = args(code, *twin);
      return grandoprns;
    }
    return std::nullopt;
  }

  result impl(auto operation,auto oprns)
    requires (operation.tag == any_of(optag::add,optag::mul,optag::land,optag::lor))
  {
    auto oprn = ranges::vec(views::cast<id>(kumi::get<0>(oprns)));
    auto redundant_oprns =
      std::ranges::stable_partition(oprn,
                                    none_of(operation.tag()),
                                    lift((codes::optag_of)));
    if(std::ranges::empty(redundant_oprns))
      return std::nullopt;
    
    //FIXME: bad... - jeh
    std::vector<id> new_oprns{};
    for(auto redundant_oprn:redundant_oprns){
      auto [grandoprns] = args(code, *grammars::refcast(operation.tag,redundant_oprn));
      std::ranges::copy(grandoprns,std::back_inserter(new_oprns));
    }
    auto [it,end] = redundant_oprns;
    oprn.erase(it,end);
    std::ranges::copy(new_oprns,std::back_inserter(oprn));
    return objadd<operation.tag()>(code, oprn);
  }
  result impl(auto operation,auto oprns)
    requires (operation.tag() == any_of(optag::div))
  {
    auto &[dividend,divisor] = oprns;

    auto dividend_div = codes::divcast(dividend);
    auto divisor_div = codes::divcast(divisor);
    if(dividend_div){
      auto [dividend_dividend,dividend_divisor] = args(code, *dividend_div);
      if(divisor_div){
        auto [divisor_dividend,divisor_divisor] = args(code, *divisor_div);
        return div(code,
                   mul(code,{dividend_dividend,divisor_divisor}),
                   mul(code,{dividend_divisor,divisor_dividend}));
      }
      return div(code,dividend_dividend,mul(code,{dividend_divisor,divisor}));
    }
    else if(divisor_div){
      auto [divisor_dividend,divisor_divisor] = args(code, *divisor_div);
      return div(code,mul(code,{dividend,divisor_divisor}),divisor_dividend);
    }
    return std::nullopt;
  }
  result impl(auto operation,auto oprns)
    requires (operation.tag() == any_of(optag::pow))
  {
    auto &[base,exponent] = oprns;
    if(auto pow_base = codes::powcast(base)){
      auto [base_base,base_exponent] = args(code, *pow_base);
      return pow(code,base_base,mul(code,{base_exponent,exponent}));
    }
    return std::nullopt;
  }
  result operator()(auto o){
    if constexpr(can_fold(o.tag))
      return impl(o,args(code, o));
    return std::nullopt;
  }
};

struct HYSJ_EXPORT subpass{
  auto run(auto &pass)
    requires (dmaps::is_op_part(pass.part) && pass.step == dmaps::post_tag())
  {
    auto &code = pass.code();
    auto new_op = with_op(rules{code},pass.op.index);
    return map_value(new_op,[&](auto op){ return pass.emit(op); });
  }
};

auto map(code &P,auto roots){
  return dmaps::map_roots(dmaps::make_state(P,std::move(roots),kumi::tuple{subpass{}}));
}
inline auto map1(code &P,id o){
  return ranges::first(map(P,views::pin(o)));
}

} //hysj::codes::argfolds
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

constexpr auto argfold(auto &&... x)
  arrow((argfolds::map(fwd(x)...)))
constexpr auto argfold1(auto &&... x)
  arrow((argfolds::map1(fwd(x)...)))

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
