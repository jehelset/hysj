#include<hysj/codes/algorithms/opsubs.hpp>

#include<utility>

#include<hysj/tools/graphs/incidence_graphs.hpp>
#include<hysj/codes.hpp>
#include<hysj/tools/functional/optional.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

void total_opsub(code &p,id source,id target){
  if(source == target)
    return;
  const auto source_vertex = objvert(p,source),
    target_vertex = objvert(p, target);

  auto &graph = p.graph;
  for(auto edge:std::exchange(graph.vertices[source_vertex][graphs::in_tag],{})){
    graph.edges[edge][graphs::out_tag] = target_vertex;
    graph.vertices[target_vertex][graphs::in_tag].push_back(edge);
  }
  //FIXME: recompile ancestors! 
}

bool partial_opsub(code &p,id root,id source,id target){
  return opsubs::map1(p,root,
                      [&](id o){
                        return maybe(o == source,target);
                      }) != std::nullopt;
}

} // hysj::codes
#include<hysj/tools/epilogue.hpp>
