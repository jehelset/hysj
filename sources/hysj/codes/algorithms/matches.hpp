#pragma once
#include<algorithm>
#include<functional>
#include<ranges>
#include<utility>
#include<vector>

#include<hysj/codes.hpp>
#include<hysj/tools/coroutines.hpp>
#include<hysj/tools/coroutines/trees.hpp>
#include<hysj/tools/graphs/algorithms/deepwalks.hpp>
#include<hysj/tools/graphs/directions.hpp>
#include<hysj/tools/graphs/incidence.hpp>
#include<hysj/tools/graphs/properties.hpp>
#include<hysj/tools/ranges/pins.hpp>
#include<hysj/tools/ranges/utility.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes::matches{

template<optag O>
constexpr auto match_co(constant_t<O>){
  return co_get<sym<O>>{};
}
template<famtag F>
constexpr auto match_co(constant_t<F>){
  return co_get<fam<F>>{};
}

//FIXME: const - jeh
template<typename T>
requires (is_constant_of<T,optag> || is_constant_of<T,famtag>)
auto walk(const code &code,auto op_roots,T optag,auto op_pred){
  return graphs::dwalk(
    match_co(optag),
    graphs::dw::make_state(graphs::out_tag,std::cref(code.graph)),
    graphs::dw::with_starts(
      views::map(op_roots,[&](auto op){ return objvert(code, op); }),
      [&,optag,op_pred,op_flag = graphs::vprop(code.graph,false)]<class DWGet>
        (DWGet,auto dw_event,auto dw_ref) mutable -> co::api_t<DWGet>{
        auto &dw_co = HYSJ_CO(DWGet);
        auto &dw = unwrap(dw_ref);
        if constexpr(dw_event == any_of(graphs::dw::start,graphs::dw::down)){
          auto vertex =
            [&]{ //FIXME: oof! - jeh
              if constexpr(dw_event == graphs::dw::start)
                return dw.root;
              else
                return dw.head();
            }();
          if(graphs::get(op_flag,vertex))
            co_final_await co::nil<1>(dw_co);
          graphs::put(op_flag,vertex,true);
          auto op = objid(code, vertex);
          auto op_id = grammars::refcast(optag,op);
          if(!op_id || !op_pred(*op_id))
            co_final_await co::nil<1>(dw_co);
          co_await co::put<0>(dw_co,*op_id);
          co_final_await co::ret<1>(dw_co,graphs::dw::right);
        }
        else
          co_final_await co::nil<1>(dw_co);
      }));
}
auto walk(const code &p,auto o,auto t){
  return walk(p,o,t,always(true));
}
auto walk1(const code &p,auto o,auto t,auto f){
  return walk(p,views::pin(o),t,f);
}
auto walk1(const code &p,auto o,auto t){
  return walk1(p,o,t,always(true));
}

} //hysj::codes::matches
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

constexpr auto match(auto &&... x)
  arrow((matches::walk(fwd(x)...)))
constexpr auto match1(auto &&... x)
  arrow((matches::walk1(fwd(x)...)))

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
