#pragma once
#include<functional>
#include<ranges>
#include<utility>
#include<vector>

#include<hysj/codes/algorithms/deepmetrics.hpp>
#include<hysj/codes.hpp>
#include<hysj/tools/coroutines.hpp>
#include<hysj/tools/coroutines/trees.hpp>
#include<hysj/tools/functional/unwrap.hpp>
#include<hysj/tools/graphs/adjacency.hpp>
#include<hysj/tools/graphs/algorithms/deeporders.hpp>
#include<hysj/tools/graphs/algorithms/hopcroft1971.hpp>
#include<hysj/tools/graphs/degree.hpp>
#include<hysj/tools/graphs/incidence_graphs.hpp>
#include<hysj/tools/graphs/partitions.hpp>
#include<hysj/tools/graphs/properties.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/types.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes::merges{

template<typename Suborder>
struct subpass{

  const dmetrics::state &metrics;
  Suborder suborder;

  std::vector<natural> operand_edge_indices;
  std::vector<grammars::vert> reachable;
  
  auto run(auto pass_co,const auto &pass)
    requires (pass.part == dmaps::prog_tag() && pass.step == dmaps::pre_tag()){
    operand_edge_indices = graphs::eprop(pass.code().graph,0_n);
    reachable.clear();
    return co::nil<1>(pass_co);
  }

  auto run(auto &pass)
    requires (dmaps::is_alg_part(pass.part) && pass.step == dmaps::pre_tag())
  {
    auto op = pass.op.vertex;
    auto &code = pass.code();
    reachable.push_back(op);
    for(auto [operand_index,operand_edge]:
          std::views::enumerate(graphs::voincident(code.graph,op)))
      graphs::put(operand_edge_indices,operand_edge,operand_index);
    return std::nullopt;
  }
 
  template<dmaps::action A,dmaps::step S>
  auto recv(const auto &pass,constant_t<A>,constant_t<S>)
    requires (A == dmaps::emit_tag()){
    auto &code = pass.code();
    auto size = graphs::size(code.graph);
    graphs::resize(operand_edge_indices,size,0_n);
  }

  template<typename PassGet>
  auto run(PassGet,auto &pass) -> co::api_t<PassGet>
    requires (pass.part == dmaps::prog_tag() && pass.step == dmaps::post_tag())
  {
    auto &subpass_co = HYSJ_CO(PassGet);

    auto &code = pass.code();

    const auto partitions = [&]{
      reachable.erase(
        std::ranges::remove_if(
          reachable,
          [&](auto o){
            return graphs::get(metrics.tour_indices,o) == std::nullopt;
          }).begin(),
        reachable.end());
      {
        std::ranges::sort(reachable);
        auto [it,end] = std::ranges::unique(reachable);
        reachable.erase(it,end);
      }
 
      auto initial_partitioning =
        graphs::prt::construct(
          reachable,
          [&](auto v0,auto v1){
            auto i0 = objid(code, v0);
            auto i1 = objid(code, v1);
            if(auto n0 = argdeg(code, i0), n1 = argdeg(code, i1);
               n0 != n1)
              return n0 < n1;
            return codes::subcmp(
              [&](auto sup,auto sub0,auto sub1){
                if(auto r = code.subargcmp(sup,sub0,sub1);
                   r != std::partial_ordering::equivalent)
                  return r;
                return suborder(sup,sub0,sub1);
              },
              i0,i1) == std::partial_ordering::less;
          });
      return graphs::hop71::construct(std::cref(code.graph),
                                      metrics.max_arity,
                                      std::cref(operand_edge_indices),
                                      initial_partitioning);
    }();
    
    for(const auto &partition:views::keep(partitions,[](const auto &p){ return p.size() > 1; })){
      auto base = partition[0];
      pass.op.vertex = base;
      pass.op.index  = objid(code, base);
      for(auto derived:std::views::drop(partition,1))
        co_await co::put<1>(subpass_co,pass.emit(derived));
    }
    co_final_await co::nil<1>(subpass_co);
  }
};

auto map(code &P,codes::concepts::suborder auto suborder,auto roots){
  auto state = std::make_unique<dmetrics::state>();
  auto *state_ptr = state.get();
  return dmaps::map_prog(
    dmaps::make_state(P,std::move(roots),
                      kumi::tuple{
                        dmetrics::subpass{std::move(state)},
                        subpass{*state_ptr,std::move(suborder)}
                      }));
}
inline auto map1(code &P,auto suborder,id root){
  return map(P,std::move(suborder),views::pin(root));
}

} //hysj::codes::merges
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

constexpr auto merge(auto &&... x)
  arrow((merges::map(fwd(x)...)))
constexpr auto merge1(auto &&... x)
  arrow((merges::map1(fwd(x)...)))

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
