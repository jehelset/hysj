#include<hysj/codes/algorithms/symdiffs.hpp>

#include<algorithm>
#include<functional>
#include<optional>
#include<ranges>
#include<span>
#include<vector>

#include<hysj/codes/code.hpp>
#include<hysj/codes/compile.hpp>
#include<hysj/codes/factories.hpp>
#include<hysj/codes/grammar.hpp>
#include<hysj/codes/access.hpp>
#include<hysj/grammars.hpp>
#include<hysj/tools/functional/apply.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/functional/apply.hpp>
#include<hysj/tools/functional/bind.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

namespace _{

  struct symdiff_impl{

    codes::code &code;

    codes::exprfam dx;
    const std::vector<codes::exprfam> &dI;
        
    std::vector<std::optional<id>> argstack;

    auto new_argids(id o)const{
      return std::span{argstack}.last(argdeg(code, o));
    }
    auto argids(id o)const{
      return
        views::map(
          std::views::iota(0_i, argdeg(code, o)),
          [&,o](auto i){
            return new_argids(o)[i].value_or(grammars::argids(code, o)[i]);
          });
    }
    template<codes::optag O>
    auto args(sym<O> o)const{
      return grammars::args(code, o, argids(o));
    }
    bool has_new_args(id o)const{
      return std::ranges::any_of(new_argids(o), has_value);
    }
    void pop_args(id o){
      argstack.resize(argstack.size() - argdeg(code, o));
    }
    auto args(id i)const{
      return std::span{argstack}.last(grammars::argdeg(code, i));
    }

    std::optional<id> impl(auto o)const{
      if constexpr(has_accessed(o.tag)){
        if(!is_access(code, o))
          return no<id>;
        return with_access(
          [&](auto u,auto I) -> std::optional<id>{
            if(u != dx.id())
              return der(code, o, dx, dI);
            if(dI.empty())
              return oone(code, u);
            else
              return
                land(code, 
                     ranges::vec(
                       std::views::zip(I, dI),
                       unpack(bind<>(codes::eq, std::ref(code)))));
          },
          code,
          o);
      }
      else{

        //FIXME: branch on !is_function_of(exponent,symbol)
        //       - jeh
        if(!has_new_args(o))
          return no<id>;

        auto A = grammars::args(code, o);
        auto dA = args(o);

        if constexpr(o.tag == any_of(optag::neg,optag::add,optag::sub,
                                     optag::eq, optag::lt, optag::lnot,
                                     optag::land,optag::lor))
          return apply(bind<>(objadd<o.tag()>, std::ref(code)), dA);

        else if constexpr(o.tag == optag::mul){
          auto [factors] = A;
          auto [differentiated_factors] = dA;
          const auto arity = factors.size();
          return add(code,
                     ranges::vec(
                       views::indices(arity),
                       [&](auto i){
                         auto subfactors = ranges::vec(factors,lift((id)));
                         subfactors[i] = differentiated_factors[i];
                         return id(mul(code,subfactors));
                       }));
        }
        else if constexpr(o.tag == optag::div){
          auto [dividend,divisor] = A;
          auto [differentiated_dividend, differentiated_divisor ] = dA;
          return div(code,
                     add(code,{
                         mul(code,{differentiated_dividend,divisor}),
                         mul(code,{dividend,differentiated_divisor})
                       }),
                     mul(code,{divisor, divisor}));
        }
        else if constexpr(o.tag == optag::pow){
          auto [base,exponent] = A;
          auto [differentiated_base,differentiated_exponent ] = dA;
          return add(code,{
              mul(code,{
                  exponent,
                  differentiated_base,
                  pow(code,base,sub(code,exponent, oone(code, o)))
                }),
              mul(code,{
                  pow(code,base,exponent),
                  log(code,oe(code, o),base),
                  differentiated_exponent
                })
            });
        }
        else if constexpr(o.tag == optag::log){
          auto [base,exponent] = A;
          auto [differentiated_base,
                differentiated_exponent ] = dA;
          auto
            ln_base = log(code,oe(code, o),base),
            ln_exponent = log(code,oe(code, o),exponent);
          return div(code,
                     sub(code,
                         div(code,
                             mul(code,{
                                 ln_base,
                                 differentiated_exponent
                               }),
                             exponent),
                         div(code,
                             mul(code,{
                                 differentiated_base,
                                 ln_exponent
                               }),
                             base)),
                     pow(code,ln_base,otwo(code, o)));

        }
        else if constexpr(o.tag == optag::sin){
          auto [angle] = A;
          auto [differentiated_angle] = dA;
          return mul(code,{
              differentiated_angle,
              sin(code,
                  sub(code,
                      angle,
                      div(code,opi(code, o),otwo(code, o))))
            });
        }
      }
      return {};
    }

    auto pre(id o){
      auto e = exprcast(o);
      if(!e)
        return false;
      return grammars::with_famobj(
        [&](auto e){
          if constexpr(e.tag == any_of(optag::lit, optag::itr, optag::cst)){
            argstack.push_back(ozero(code, e).id());
            return false;
          }
          return true;
        },
        *e);
    }
    auto pre(id o,id r,id a){
      if(reltag_of(r) == any_of(reltag::dom, reltag::dim, reltag::exe, reltag::lnk)){
        argstack.push_back(std::nullopt);
        return false;
      }
      return pre(a);
    }

    auto post(id o){
      auto e = exprcast(o);
      if(!e)
        return no<none>;
      auto p =
        grammars::with_famobj(
          [&](auto o){
            return impl(o);
          },
          *e);
      pop_args(o);
      argstack.push_back(p);
      return just(none{});
    }
    auto post(id o, id r, id a){
      return post(a);
    }

    auto operator()(exprfam e){
      //FIXME: deal with aborts...
      grammars::walk<none>(code, lift((pre)(&)), lift((post)(&)), e);
      if(argstack.size() != 1)
        return no<exprfam>;
      return exprcast(argstack.front().value_or(e.id()));
    }
  };
}

std::optional<exprfam> symdiff(code &c,dersym d){
  auto [f, dx, dI] = args(c, d);
  compile(c, d);
  return _::symdiff_impl{c, dx, ranges::vec(dI)}(f);
}


} //hysj::codes
#include<hysj/tools/epilogue.hpp>

