#pragma once
#include<concepts>
#include<algorithm>
#include<initializer_list>
#include<functional>
#include<ranges>
#include<utility>
#include<vector>

#include<hysj/codes/algorithms/deepmaps.hpp>
#include<hysj/codes.hpp>
#include<hysj/tools/functional/optional.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/ranges/pins.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes::opsubs{

namespace concepts{
  template<typename T>
  concept opmap =
    (requires(T t,id i){
      { t(i) } -> std::convertible_to<std::optional<id>>;
    });
}

template<typename Opmap>
struct subpass{
  using nonidempotent = void; 
  
  Opmap opmap;

  auto run(auto &pass)
    requires (dmaps::is_alg_part(pass.part) && pass.step == dmaps::post_tag())
  {
    return map_value(
      opmap(pass.op.index),
      [&](auto op){
        return pass.emit(op,dmaps::post_tag());
      });
  }
};

auto map(code &P,auto roots,concepts::opmap auto opmap){
  return dmaps::map_roots(
    dmaps::make_state(
      P,
      roots,
      kumi::tuple{subpass{std::move(opmap)}}));
}
auto map1(code &P,id root,auto opmap){
  return ranges::first((map)(P,views::pin(root),std::move(opmap)));
}

using pair = kumi::tuple<id,id>;

auto pairwise(ranges::concepts::same_forward_range<pair> auto &&pairs){
  return [pairs = views::all(fwd(pairs))](id source)mutable{
    auto pair_it = std::ranges::find_if(pairs,[&](auto pair){ return kumi::get<0>(pair) == source; });
    return maybe(pair_it != std::ranges::end(pairs),kumi::get<1>(*pair_it));
  };
}

} //hysj::codes::opsubs
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

constexpr auto opsub(auto &&... x)
  arrow((opsubs::map(fwd(x)...)))
constexpr auto opsub1(auto &&... x)
  arrow((opsubs::map1(fwd(x)...)))

HYSJ_EXPORT void total_opsub(code &,id source,id target);
HYSJ_EXPORT bool partial_opsub(code &,id root,id source,id target);

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
