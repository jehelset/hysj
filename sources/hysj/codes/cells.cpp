#include<hysj/codes/cells.hpp>

#include<concepts>
#include<ranges>
#include<tuple>
#include<vector>

#include<hysj/codes/code.hpp>
#include<hysj/codes/cells.hpp>
#include<hysj/codes/sets.hpp>
#include<hysj/codes/access.hpp>
#include<hysj/tools/ranges/map.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/type_traits.hpp>
#include<hysj/tools/types.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

const cellinfo &place(const code &c,cellinfos &l,grammars::id b){
  const auto &a = l.alignment;
  l.container.push_back(
    kumi::make_tuple(
      b,
      codes::with_type(
        [&](auto set,auto width){

          auto i = typeindex({set, width}).value();
          codes::cellinfo f_new{
            {
              set,
              width
            },
            l.sizes[i],
            ranges::vec(views::cast<integer>(oexts(c, b))),
          };
          if(f_new.shape.empty())
            f_new.shape = {1};
          auto size = [&]{ 
            auto n = f_new.size();
            if(a)
              n += (*a - n % *a);
            return n;
          }();
          l.sizes[i] += size;
          return f_new;
        },
        codes::otype(c, b))));
  return get<1>(l.container.back());
}
void place(const code &c,const accesses &A,cellinfos &l,devsym d){
  for(auto b:A.buffers(d))
    if(!l.contains(b))
      place(c, l, b);
}


cellinfos cellinfos::make(const code &c,const accesses &A,devsym d,std::optional<std::size_t> a){
  cellinfos F{
    .alignment = a
  };
  place(c, A, F, d);
  return F;
}

cells cells::make(cellinfos i){
  cells l{
    .infos = std::move(i),
    .data = [&]<std::size_t... I>(std::index_sequence<I...>){
      return std::make_tuple(
        cellvec<typetab[I].set, typetab[I].width>{
          .container{
            static_cast<std::size_t>(l.infos.sizes[I]),
            cellalloc<cell<typetab[I].set, typetab[I].width>>{
              l.infos.alignment
            }
          }
        }...);
    }(std::make_index_sequence<std::size(typetab)>{})
  };
  return l;
}

cells alloc(const code &c,codes::apisym b,std::optional<std::size_t> l){
  codes::cellinfos i{
    .alignment = l
  };
  const auto a = codes::accesses::make(c, b);
  place(c, a, i, c.builtins.host);
  return codes::cells::make(std::move(i));
}

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
