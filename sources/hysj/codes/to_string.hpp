#include<string>

#include<hysj/grammars.hpp>
#include<hysj/codes/code.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

HYSJ_EXPORT
std::string to_string(const code &,id);

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
