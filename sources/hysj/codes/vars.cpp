#include<vector>

#include<hysj/codes/code.hpp>
#include<hysj/codes/vars.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/types.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::_HYSJ_VERSION_NAMESPACE::codes{

std::vector<exprfam> depvar(code &code,
                            exprfam independent_variable,
                            natural order,
                            exprfam dependent_variable)
{
  std::vector<exprfam> X(order + 1);
  X[0] = dependent_variable;
  for(natural i = 0;i != order;++i)
    X[i + 1] = {
      der(code, X[i], independent_variable)
    };
  return X;
}

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
