#pragma once

#include<optional>
#include<span>
#include<vector>

#include<hysj/grammars.hpp>
#include<hysj/codes/code.hpp>
#include<hysj/codes/factories.hpp>
#include<hysj/tools/reflection.hpp>
#include<hysj/tools/types.hpp>
#include<hysj/tools/functional/ignore.hpp>
#include<hysj/tools/functional/optional.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

HYSJ_EXPORT
integer thread_partition(integer max_thread_count, std::span<const integer> extents);

HYSJ_EXPORT
integer thread_pivot(integer max_thread_count, std::span<const integer> extents);

struct thread_info{

  integer global_count;
  optabs<std::optional<integer>> local_counts;

  friend HYSJ_MIRROR_STRUCT(thread_info, (global_count, local_counts));

  auto operator<=>(const thread_info &) const = default;

  constexpr auto local_count(this auto &&s,id o)
    arrow((fwd(s).local_counts.at(o[0]).at(o[1])));

  static thread_info make(const code &c,integer global_count){
    thread_info T{
      .global_count = global_count,
      .local_counts = make_objtabs(c, [&](ignore_t, auto n){ return std::vector(n, no<integer>); })
    };
    return T;
  }
};

HYSJ_EXPORT
std::optional<integer> thread(code &,thread_info &,taskfam root);

HYSJ_EXPORT
taskfam thread_fork(code &,exprfam thread,taskfam task,exprfam thread_count);

HYSJ_EXPORT
std::optional<taskfam> thread_fork(code &,thread_info &,exprfam,taskfam);

inline exprfam cells_per_thread(code &code,exprfam num_threads,exprfam num_cells){
  using namespace codes::factories;
  return {inc(code, div(code, dec(code, num_cells), num_threads))};
}
inline exprfam first_cell_of_thread(code &code,exprfam thread, exprfam cells_per_thread){
  using namespace factories;
  return {mul(code, {thread, cells_per_thread})};
}

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
