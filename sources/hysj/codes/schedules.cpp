#include<hysj/codes/schedules.hpp>

#include<algorithm>
#include<array>
#include<optional>
#include<array>
#include<vector>
#include<ranges>
#include<stdexcept>

#include<fmt/core.h>
#include<kumi/tuple.hpp>

#include<hysj/grammars.hpp>
#include<hysj/codes/code.hpp>
#include<hysj/codes/access.hpp>
#include<hysj/tools/functional/any_of.hpp>
#include<hysj/tools/functional/pick.hpp>
#include<hysj/tools/functional/apply.hpp>
#include<hysj/tools/graphs/algorithms/deeporders.hpp>
#include<hysj/tools/graphs/elements.hpp>
#include<hysj/tools/ranges/cat.hpp>
#include<hysj/tools/ranges/join.hpp>
#include<hysj/tools/ranges/map.hpp>
#include<hysj/tools/ranges/utility.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

sched sched::make(const code &c, codes::taskfam t){

  sched s;

  using job = kumi::tuple<id, std::optional<graphs::vertex_id>>;
  std::vector<job> jobs;

  using synch = std::array<std::vector<std::size_t>, 2>;
  std::vector<synch> synchs;

  auto map = [&](std::size_t i){
    auto &[o, v] = jobs.at(i);
    if(v)
      return *v;
    return (v = s.graph.vertex(taskcast(o).value())).value();
  };
  auto pop = [&](id o){
    synchs.resize(synchs.size() - grammars::argdeg(c, o));
  };
  auto args = [&](id o){
    return std::span{synchs}.last(grammars::argdeg(c, o));
  };
  auto skip = [&](auto &U){
    return U[0].empty();
  };

  auto pre = [&](id o){
    auto ot = optag_of(o);
    if(!is_task(ot))
      throw std::runtime_error(fmt::format("invalid op: {}", o));
    if(ot == any_of(optag::then, optag::when))
      return true;
    jobs.push_back(job{o, std::nullopt});
    synchs.push_back(
      {
        std::vector{jobs.size() - 1},
        std::vector{jobs.size() - 1}
      });
    return false;
  };

  auto post = [&](id o){
    auto ot = optag_of(o);
    switch(ot){
    case optag::then:{
      auto U = args(o);
      {
        auto [it, end] = std::ranges::remove_if(U, skip);
        U = U.first(U.size() - std::ranges::distance(it, end));
      }
      if(U.empty())
        return synch{};
      for(auto i:views::indices(U.size() - 1)){
        auto &T_left  = U[i][1];
        auto &T_right = U[i + 1][0];
        for(auto &&[t_left, t_right]:std::views::cartesian_product(T_left, T_right)){
          auto u_left = map(t_left);
          auto u_right = map(t_right);
          s.graph.try_edge(u_left, u_right, never);
        }
      }
      return synch{std::move(ranges::first(U)[0]), std::move(ranges::last(U)[1])};
    }
    case optag::when:{
      auto U = args(o);
      synch S{
        ranges::vec(views::join(views::map(U, lift((get<0>))))),
        ranges::vec(views::join(views::map(U, lift((get<1>)))))
      };
      for(auto &J:S)
        ranges::vecset(J, [&](std::size_t j0,
                              std::size_t j1){
          return get<0>(jobs.at(j0)) < get<0>(jobs.at(j1));
        });
      return S; 
    }
    default: {
      throw std::runtime_error(fmt::format("unexpected op: {}", o));
    }
    }
  };

  grammars::walk<none>(
    c,
    compose<>(pre, pick<-1>),
    compose<>(
      never,
      [&](id o){
        auto J = post(o);
        pop(o);
        synchs.push_back(std::move(J));
      },
      pick<-1>),
    t);

  if(synchs.size() != 1)
    throw std::logic_error("unexpected stack size");

  std::ranges::for_each(apply(views::cat, synchs.front()), map);
  return s;
}

} //hysj::codes
#include<hysj/tools/epilogue.hpp>

