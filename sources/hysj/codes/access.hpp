#pragma once

#include<utility>
#include<vector>

#include<kumi/tuple.hpp>

#include<hysj/codes/code.hpp>
#include<hysj/codes/factories.hpp>
#include<hysj/codes/grammar.hpp>
#include<hysj/grammars.hpp>
#include<hysj/tools/functional/any_of.hpp>
#include<hysj/tools/functional/invoke.hpp>
#include<hysj/tools/functional/optional.hpp>
#include<hysj/tools/ranges/map.hpp>
#include<hysj/tools/ranges/utility.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::_HYSJ_VERSION_NAMESPACE::codes{

constexpr bool can_access(optag t){
  return t == any_of(optag::var, optag::rfl, optag::der);
}
constexpr bool can_access(id o){
  return can_access(optag_of(o));
}

constexpr auto accessed(const code &c,id o){
  return with_op(
    [&](auto o) -> std::optional<id>{
      using enum optag;
      if constexpr(can_access(o.tag))
        return o;
      else if constexpr(o.tag == rot){
        auto a = arg(c, o, 0_N);
        return maybe(can_access(a), a);
      }
      else
        return no<id>;
    },
    o);
}

constexpr bool has_accessed(optag t){
  return can_access(t) || t == any_of(optag::rot);
}

constexpr bool is_access(const code &c, id o){
  return accessed(c, o) != std::nullopt;
}

constexpr decltype(auto) with_access(auto &&f, const code &c, id o){
  return with_op(
    [&](auto o) -> decltype(invoke(fwd(f), std::declval<id>(), views::no<id>)){
      using enum optag;
      if constexpr(o.tag == any_of(var, rfl, der))
        return invoke(fwd(f), o.id(), oitrs(c, o));
      else if constexpr(o.tag == rot){
        auto [a, B] = args(c, o);
        return invoke(fwd(f), a.id(), views::cast<id>(B));
      }
      else if constexpr(o.tag == cur){
        return with_access(fwd(f), c, arg(c, o, 0_N));
      }
      else
        HYSJ_UNREACHABLE;
    },
    o);
}


HYSJ_EXPORT
exprfam access_for(code &C,const exprfam &e);

template<reflected_struct S>
auto access_for_struct(code &c, const S &s){
  return kumi::apply(
    [&](const auto &... m){
      return S{grammars::refcast(m.tag, access_for(c, expr(m))).value()...};
    },
    member_refs(s));
}
template<reflected_struct S>
auto buffer_for_struct(code &c, const S &s, codes::dimexprfam n){
  return kumi::apply(
    [&](const auto &... m){
      return S{grammars::refcast(m.tag, buf_for(c, access_for(c, expr(m)), {n})).value()...};
    },
    member_refs(s));
}

std::vector<id> strides_of(code &c,ncell<> w,grammars::concepts::random_access_id auto &&D){
  const auto r = std::ranges::size(D);
  std::vector<id> S(r);
  if(!r)
    return S;
  S[r - 1] = c.builtins.lits[literal::one, w];
  for(auto i:views::indices(r - 1))
    S[(r - 2) - i] = mul(c, {S[(r - 1) - i], D[(r - 1) - i]});
  return S;
}

HYSJ_EXPORT
std::vector<id> strides_of(code &c,id o);

HYSJ_EXPORT
bool needs_wrap(const code &,id itr,id slot);

HYSJ_EXPORT
id index_of(code &,id op,const std::vector<id> &strides);

inline id index_of(code &c,id o){
  return index_of(c, o, strides_of(c, o));
}

constexpr bool is_dst(const code &c,id r){
  id o = grammars::relobjid(c, r);
  const auto ot = optag_of(o);
  const auto rt = reltag_of(r);
  return !(ot == optag::put && rt == reltag::dst);
}

constexpr auto with_accesses_of(auto co_get,const code &c,id o,auto &&control){
  return graphs::dwalk(
    std::move(co_get),
    graphs::out_tag,
    std::ref(c.graph),
    grammars::objvert(c, o),
    [&,control = fwd(control)]<typename DWGet>(DWGet, auto dw_event, auto dw_ref) -> co::api_t<DWGet>{

      constexpr auto is_root = constant_c<dw_event == any_of(graphs::dw::start,graphs::dw::stop)>;
      constexpr auto is_pre = constant_c<dw_event == any_of(graphs::dw::start,graphs::dw::down)>;

      auto &dw_co = HYSJ_CO(DWGet);
      auto &dw = unwrap(dw_ref);

      const auto o_vert = is_root ? dw.root : dw.head();
      const auto o = grammars::objid(c, o_vert);
      if constexpr(is_pre){
        if constexpr(is_root)
          co_final_await co::nil<1>(dw_co);
        else{
          const auto u = grammars::objid(c, dw.tail());
          const auto ut = optag_of(u);
          const auto a = grammars::relid(c, dw.edge());
          const auto at = reltag_of(a);
          const bool ux = is_access(c, u);
          auto n = (ux && at == reltag::src)
            || (ut == optag::rfl)
            || (at == any_of(reltag::dom, reltag::dim))
            ? graphs::dw::right
              : graphs::dw::down;
          co_final_await co::ret<1>(
            dw_co,n);

        }
      }
      else{
        if(is_access(c, o)){
          const auto d = is_root || is_dst(c, grammars::relid(c, dw.edge()));
          co_await control(co::get<>(dw_co), o, d);
          if constexpr(is_root)
            co_final_await co::nil<2>(dw_co);
          else
            co_final_await co::ret<1>(dw_co, graphs::dw::right);
        }
        else{
          co_final_await co::nil<is_root ? 2 : 1>(dw_co);
        }
      }
    });
}
template<typename T>
constexpr auto with_accesses_of(const code &c,id o,auto &&control){
  return with_accesses_of(co_get<T>{}, c, o, fwd(control));
}
struct HYSJ_EXPORT accesses{
  using vertex_map_type = kumi::tuple<
    std::vector<kumi::tuple<devsym, graphs::vertex_id>>,
    std::vector<kumi::tuple<exprfam, graphs::vertex_id>>>;
  using graph_type = graphs::incidence_graph<id, exprfam>;

  vertex_map_type vertex_map;
  graph_type graph;

  constexpr auto vertex(devsym d)const{
    auto &D = get<0>(vertex_map);
    auto it = std::ranges::find(D, d, lift((get<0>)));
    if(it == std::ranges::end(D))
      return no<graphs::vertex_id>;
    return just(get<1>(*it));
  }

  friend HYSJ_MIRROR_STRUCT(accesses, (vertex_map, graph));

  auto operator<=>(const accesses &) const = default;

  auto inputs(devsym d)const{
    return views::map(
      graphs::viadjacent(graph, vertex(d).value()),
      [&](auto a){
        return expr(graph[a]);
      });
  }
  auto outputs(devsym d)const{
    return views::map(
      graphs::voadjacent(graph, vertex(d).value()),
      [&](auto a){
        return expr(graph[a]);
      });
  }
  auto buffers(devsym dev)const{
    return views::cat(inputs(dev), outputs(dev));
  }

  static accesses make(const code &c,id t);
};

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
