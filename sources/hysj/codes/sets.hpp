#pragma once
#include<numeric>
#include<variant>
#include<stdfloat>

#include<fmt/format.h>

#include<hysj/tools/functional/any_of.hpp>
#include<hysj/tools/functional/apply.hpp>
#include<hysj/tools/functional/invoke.hpp>
#include<hysj/tools/functional/optional.hpp>
#include<hysj/tools/preprocessor.hpp>
#include<hysj/tools/reflection.hpp>
#include<hysj/tools/types.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

#define HYSJ_CODES_SETS \
  (booleans,b)          \
  (naturals,n)          \
  (integers,i)          \
  (reals,r)

enum set : integer {
  HYSJ_SPLAT(0,HYSJ_CODES_SETS)
};
HYSJ_MIRROR_ENUM(set,HYSJ_CODES_SETS)

#define HYSJ_LAMBDA(id,...)\
  constexpr auto id##_c = constant_c<set::id>;
HYSJ_MAP_LAMBDA(HYSJ_CODES_SETS)
#undef HYSJ_LAMBDA

constexpr set set_cast(integer s)noexcept{
  return *enum_cast<set>(s);
}

template<std::same_as<set>... S>
constexpr decltype(auto) with_set(auto &&f,S... t){
  return with_enum(fwd(f),t...);
}

constexpr bool is_signed(set x){
  return x == any_of(set::integers,set::reals);
}
constexpr bool is_integral(set x){
  return x != set::reals;
}
constexpr bool is_arithmetic(set x){
  return x != set::booleans;
}
constexpr bool is_integral_arithmetic(set x){
  return is_integral(x) && is_arithmetic(x);
}

#define HYSJ_CODES_WIDTHS (1)(8)(16)(32)(64)(128)

#define HYSJ_LAMBDA(w) (natural{w})
inline constexpr auto widthtab = std::array{HYSJ_SPLAT(0, HYSJ_MAP_LAMBDA(HYSJ_CODES_WIDTHS))};
#undef HYSJ_LAMBDA

inline constexpr auto widthmin = widthtab.front();
inline constexpr auto widthmax = widthtab.back();
inline constexpr auto is_width = apply(lift((any_of)), widthtab);

namespace _{
  template<typename T,natural I>
  constexpr T with_width_impl(auto &&f,natural w,constant_t<I>){
    if constexpr(I == std::size(widthtab))
      throw std::runtime_error(fmt::format("invalid width: {}", w));
    else{
      if(widthtab[I] == w)
        return invoke(fwd(f), constant_c<auto(widthtab[I])>);
      return _::with_width_impl<T>(fwd(f), w, constant_c<I + 1>);
    }
  }
}
inline constexpr auto with_width = [](auto &&f,natural w)
  arrow((_::with_width_impl<decltype(invoke(fwd(f), constant_c<widthmin>))>(fwd(f), w, 0_N)));

#define HYSJ_CODES_TYPES HYSJ_PRODUCT((HYSJ_CODES_SETS)(HYSJ_CODES_WIDTHS))

namespace _{
  template<set S, natural W>
  struct cell_impl{
    using type = none;
  };
  #define HYSJ_LAMBDA(W)                                                             \
    template<> struct cell_impl<set::booleans, W>{ using type = std::uint##W##_t; }; \
    template<> struct cell_impl<set::naturals, W>{ using type = std::uint##W##_t; }; \
    template<> struct cell_impl<set::integers, W>{ using type = std::int##W##_t; };
  HYSJ_MAP_LAMBDA((8)(16)(32)(64))
  #undef HYSJ_LAMBDA
  #define HYSJ_LAMBDA(W)                                                             \
    template<> struct cell_impl<set::reals, W>{ using type = std::float##W##_t; };
  HYSJ_MAP_LAMBDA((16)(32)(64))
  #undef HYSJ_LAMBDA
}
template<set S,natural W = 64>
using cell = typename _::cell_impl<S, W>::type;
template<set S,natural W>
constexpr bool has_cell = !std::is_same_v<cell<S, W>, none>;

struct type{
  codes::set set;
  natural width;

  auto operator<=>(const type &) const = default;

  friend HYSJ_MIRROR_STRUCT(type, (set,width));
};

inline constexpr auto with_type = [](auto &&f,type t) -> decltype(auto) {
  return with_set(
    [&](auto s) -> decltype(auto) {
      return with_width(
        [&](auto w) -> decltype(auto) {
          return invoke(fwd(f), s, w);
        },
        t.width);
    },
    t.set);
};

namespace concepts{

  template<typename T>
  concept type = std::same_as<std::remove_cvref_t<T>, type>;

}

#define HYSJ_LAMBDA(S_enum, S, W) type{ codes::set::S_enum, W },
static constexpr auto typetab = std::array{
  HYSJ_MAP_LAMBDA(HYSJ_CODES_TYPES)
};
#undef HYSJ_LAMBDA

constexpr auto typeindex(type t){
  for(std::size_t i = 0; i != typetab.size(); ++i)
    if(typetab[i] == t)
      return just(i);
  return no<std::size_t>;
}

template<type t>
using cell_t = cell<t.set, t.width>;

using anycell = reify(
  with_enumerators<set>(
    [](auto... S){
      return type_c<std::variant<cell<S()>...>>;
    }));

constexpr auto set_of(const anycell &c){
  return set_cast(c.index());
}

//FIXME: libfmt "helpers" - jeh
#define HYSJ_LAMBDA(W)                                                                        \
  constexpr auto cell64(std::uint##W##_t v){ return static_cast<std::uint64_t>(v); } \
  constexpr auto cell64(std::int##W##_t v){ return static_cast<std::int64_t>(v); }
HYSJ_MAP_LAMBDA((8)(16)(32)(64))
#undef HYSJ_LAMBDA
#define HYSJ_LAMBDA(W)                                                  \
  constexpr auto cell64(std::float##W##_t v){ return static_cast<std::float64_t>(v); } 
HYSJ_MAP_LAMBDA((16)(32)(64))
#undef HYSJ_LAMBDA

} //hysj::codes
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

using codes::set;
using codes::with_set;
using codes::cell;
using codes::anycell;
using codes::set_of;

#define HYSJ_LAMBDA(S_enum,S,...)                                 \
  template<natural W = 64> using S##cell = cell<set::S_enum, W>;
HYSJ_MAP_LAMBDA(HYSJ_CODES_SETS)
#undef HYSJ_LAMBDA

#define HYSJ_LAMBDA(S_enum,S,W)                         \
  using S##cell##W = cell<set::S_enum, W>;
HYSJ_MAP_LAMBDA(HYSJ_CODES_TYPES)
#undef HYSJ_LAMBDA

} //hysj
template<>
struct fmt::formatter<hysj::_HYSJ_VERSION_NAMESPACE::codes::set>{
  using T = hysj::_HYSJ_VERSION_NAMESPACE::codes::set;
  constexpr auto parse(format_parse_context& context)const{
    auto it  = context.begin(),
         end = context.end();
    if(it != end && *it != '}')
      throw fmt::format_error("invalid format");
    return it;
  }
  auto format(const T &e,format_context &context)const{
    return fmt::format_to(
      context.out(),"{0}",
      [&]{
        switch(e){
        case T::booleans: return "𝐵";
        case T::integers: return "𝐼";
        case T::naturals: return "𝑁";
        case T::reals: return "𝑅";
        default: return "𝑉";
        }
      }());
  }
};
template<typename C>
HYSJ_STRUCT_FORMATTER((hysj::_HYSJ_VERSION_NAMESPACE::codes::type), C);
#include<hysj/tools/epilogue.hpp>
