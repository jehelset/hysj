#pragma once
#include<cstdint>
#include<compare>
#include<concepts>
#include<utility>
#include<ranges>

#include<hysj/codes/constants.hpp>
#include<hysj/codes/grammar.hpp>
#include<hysj/codes/sets.hpp>
#include<hysj/codes/statics.hpp>
#include<hysj/tools/preprocessor.hpp>
#include<hysj/tools/ranges/map.hpp>
#include<hysj/tools/reflection.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

struct HYSJ_EXPORT code : syntax {
  builtins::all builtins = builtins::all::make(*this);
  statics::all statics   = statics::all::make(*this);

  friend HYSJ_MIRROR_STRUCT(code,(builtins, statics)(syntax));

  std::partial_ordering subcmp(optag sup,natural sub0,natural sub1)const;
  std::partial_ordering cmp(id op0,id op1)const;
  std::partial_ordering subargcmp(optag sup,natural sub0,natural sub1)const;
  std::partial_ordering argcmp(id op0,id op1)const;
};
inline void resize(code &c){
  resize(c, c.statics);
}

namespace concepts{
  template<typename N>
  concept code = std::same_as<uncvref_t<unwrap_t<N>>,code>;
}

HYSJ_EXPORT
bool is_arg(const code &,id root,id pred);

HYSJ_EXPORT
bool equal(const code &,id,id);

HYSJ_EXPORT
id copy(code &,id);

template<optag O>
auto copy(code &c,sym<O> o){
  return grammars::refcast(o.tag,copy(c,(id)o)).value();
}
template<famtag F>
auto copy(code &c,fam<F> f){
  return grammars::refcast(f.tag,copy(c,(id)f)).value();
}

constexpr auto oset(const code &c,id o){
  return oset(c, c.statics.types, o);
}
constexpr auto owidth(const code &c,id o){
  return owidth(c, c.statics.types, o);
}
template<natural I = 0>
constexpr auto orank(const code &c,id o,constant_t<I> i = {}){
  return orank(c.statics.shapes, o, i);
}
template<natural I = 0>
constexpr auto oexts(const code &c,id o,constant_t<I> i = {}){
  return oexts(c, c.statics.shapes, o, i);
}
template<natural I = 0>
constexpr auto odims(const code &c,id o,constant_t<I> i = {}){
  return odims(c, c.statics.shapes, o, i);
}
template<natural I = 0>
constexpr auto oext(const code &c,id o,constant_t<I> i = {}){
  return oext(c, c.statics.shapes, o, i);
}
constexpr auto rset(const code &c,id o){
  return rset(c, c.statics.types, o);
}
constexpr auto rwidth(const code &c,id o){
  return rwidth(c, c.statics.types, o);
}

template<natural I = 0>
constexpr auto oitrs(const code &c,id o,constant_t<I> i = {}){
  return oitrs(c.statics.shapes, o, i);
}
constexpr auto iwidth(const code &c,id o){
  auto W = views::map(oitrs(c, o), [&](auto i){ return owidth(c, i); });
  auto it = std::ranges::max_element(W);
  if(it == std::ranges::end(W))
    return 8_n; //FIXME: omg... - jeh
  return *it;
}

constexpr auto odom(const code &c,id o){
  return odom(c, c.statics.types, o);
}
constexpr auto omin(const code &c,id o){
  return omin(c, c.builtins.lits, c.statics.types, o);
}
constexpr auto omax(const code &c,id o){
  return omax(c, c.builtins.lits, c.statics.types, o);
}
constexpr auto ospan(const code &c,id o){
  return ospan(c, c.builtins.lits, c.statics.types, o);
}
constexpr auto rdom(const code &c,id o){
  return rdom(c, c.statics.types, o);
}
constexpr auto rmin(const code &c,id o){
  return rmin(c, c.builtins.lits, c.statics.types, o);
}
constexpr auto rmax(const code &c,id o){
  return rmax(c, c.builtins.lits, c.statics.types, o);
}
constexpr auto rspan(const code &c,id o){
  return rspan(c, c.builtins.lits, c.statics.types, o);
}

constexpr auto otype(const code &c,id o){
  return type{oset(c,o), owidth(c,o)};
}
constexpr auto rtype(const code &c,id r){
  return type{rset(c,r), rwidth(c,r)};
}

namespace _{
  template<codes::optag O,std::size_t I>
  constexpr auto rtypes_impl(const code &c,sym<O> o,natural_t<I> i,auto r){
    if constexpr(grammars::objvararg<O, I>)
      return views::map(
        r, [&](auto r){
          return rtype(c, r);
        });
    else
      return rtype(c, r);
  }
}
template<codes::optag O>
constexpr auto rtypes(const code &c,sym<O> o){
  return with_integer_sequence(
    constant_c<grammars::objnum<O>>,
    [&](auto... i){
      auto R = grammars::rels(c, o);
      return std::make_tuple(_::rtypes_impl(c, o, i, std::get<i>(R))...);
    });
}
template<codes::optag O,std::size_t I>
constexpr auto rtype(const code &c,sym<O> o,constant_t<I> i){
  return _::rtypes_impl(c, o, i, rel(c, o, i));
}

namespace _{
  template<codes::optag O,std::size_t I>
  constexpr auto atypes_impl(const code &c,sym<O> o,natural_t<I> i,auto a){
    if constexpr(grammars::objvararg<O, I>)
      return views::map(
        a, [&](auto a){
          return otype(c,a);
        });
    else
      return otype(c,a);
  }
}
template<codes::optag O>
constexpr auto atypes(const code &c,sym<O> o){
  return with_integer_sequence(
    constant_c<grammars::objnum<O>>,
    [&](auto... i){
      auto R = grammars::args(c, o);
      return std::make_tuple(_::atypes_impl(c, o, i, std::get<i>(R))...);
    });
}
template<codes::optag O,std::size_t I>
constexpr auto atype(const code &c,sym<O> o,constant_t<I> i){
  return _::atypes_impl(c, o, i, arg(c, o, i));
}

constexpr auto itype(const code &,id){
  //FIXME: implement properly... - jeh
  return codes::type{set::naturals, 32};
}

constexpr auto with_otype(auto &&f,const code &c,id o) -> decltype(auto) {
  return with_type(
    [&](auto s, auto w) -> decltype(auto){
      return fwd(f)(s, w);
    },
    otype(c, o));
}

} //hysj::codes
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

using code = codes::code;

} //hysj
#include<hysj/tools/epilogue.hpp>
