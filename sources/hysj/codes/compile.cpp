#include<hysj/codes/compile.hpp>

#include<hysj/codes/code.hpp>
#include<hysj/codes/compile/domains.hpp>
#include<hysj/codes/compile/shapes.hpp>
#include<hysj/codes/compile/types.hpp>
#include<hysj/grammars.hpp>
#include<hysj/tools/graphs/algorithms/deeporders.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::_HYSJ_VERSION_NAMESPACE::codes{

void compile(code &c,id o){
  
  resize(c);
  if(has_odomain(c.statics.domains, o)
     && has_oshape(c.statics.shapes, o)
     && has_otype(c.statics.types, o))
    return;

  compile_shape(c,o);
  compile_type(c,o);
  compile_domain(c,o);

}

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
