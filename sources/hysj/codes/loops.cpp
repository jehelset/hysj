#include<hysj/codes/loops.hpp>

#include<array>
#include<algorithm>
#include<functional>
#include<numeric>
#include<ranges>
#include<utility>
#include<vector>

#include<hysj/codes/algorithms/constfolds.hpp>
#include<hysj/codes/code.hpp>
#include<hysj/codes/compile/implicit_iterators.hpp>
#include<hysj/codes/eval.hpp>
#include<hysj/codes/factories.hpp>
#include<hysj/codes/grammar.hpp>
#include<hysj/codes/threads.hpp>
#include<hysj/tools/functional/bind.hpp>
#include<hysj/tools/functional/optional.hpp>
#include<hysj/tools/ranges/cat.hpp>
#include<hysj/tools/ranges/fold.hpp>
#include<hysj/tools/ranges/map.hpp>
#include<hysj/tools/ranges/utility.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

loop_iterator make_basic_loop_iterator(code &c,itrsym i, exprfam b, exprfam e){
  using namespace factories;
  compile(c, i);
  loop_iterator l{
    .sym = i,
    .test = lt(c, cur(c, i), e),
    .init = put(c, cur(c, l.sym), b),
    .head = c.builtins.noop,
    .tail = put(c, cur(c, l.sym), inc(c, cur(c, l.sym)))
  };
  return l;
}
loop_iterator make_basic_loop_iterator(code &c,itrsym i){
  using namespace factories;
  return make_basic_loop_iterator(c, i, {ozero(c, i)}, {itrdim(c, i)});
}

std::vector<loop_iterator> make_basic_loop(code &c,grammars::id e){
  compile(c, e);
  return ranges::vec(
    views::map(oitrs(c, e), [&](id i){ return itrcast(i).value(); }),
    bind<>(lift((make_basic_loop_iterator)), std::ref(c)));
}
std::vector<loop_iterator> make_basic_loop(code &c,exprfam e){
  return make_basic_loop(c, e.id());
}
std::vector<loop_iterator> make_basic_loop(code &c,putsym t){
  return make_basic_loop(c, t.id());
}

std::vector<loop_iterator> make_inner_loop(code &c,consym e){
  return ranges::vec(
    arg(c, e, 1_N),
    bind<>(lift((make_basic_loop_iterator)), std::ref(c)));
}

std::vector<loop_iterator> bulk(code &code,putsym assign,exprfam thread,exprfam max_thread_count){
  using namespace factories;
  if(orank(code, assign) == 0)
    return {};

  const auto itrs = ranges::vec(oitrs(code, assign), [&](id i){ return itrcast(i).value(); });

  const auto dims = ranges::vec(itrs, bind<>(lift((itrdim)), std::ref(code)));
  const auto width_ = iwidth(code, assign);
  const auto width = code.builtins.widths[width_];
  const auto extents = ranges::vec(itrs, [&](const auto i){ return itrext(code, i); });

  const auto max_thread_count_ = icsteval(code, max_thread_count).value();     

  const auto thread_count_ = thread_partition(max_thread_count_, std::span{extents});
  const auto thread_pivot_ = thread_pivot(thread_count_, {extents});
  const auto blocks_per_thread_ = [&]{
    auto b = thread_count_;
    for(auto i:views::indices(thread_pivot_))
      b /= extents[i];
    return extents[thread_pivot_] / b;
  }();

  const auto blocks_per_thread = icst(code, blocks_per_thread_, width);

  const auto block_path_strides_ = [&]{
    std::vector<integer> strides(thread_pivot_);
    if(thread_pivot_ == 0)
      return strides;
    strides[thread_pivot_ - 1] = extents[thread_pivot_] / blocks_per_thread_;
    for(auto k:views::indices(thread_pivot_ - 1))
      strides[thread_pivot_ - 2 - k] = strides[thread_pivot_ - 1 - k] * extents[thread_pivot_ - 1 - k];
    return strides;
  }();

  auto block_path_strides = ranges::vec(
    block_path_strides_,
    [&](auto stride){
      return expr(icst(code, stride, width));
    });
  
  std::vector<exprfam> block_path;

  exprfam first_block{
    mul(code,
        {
          std::ranges::fold_left(
            block_path_strides,
            thread,
            [&](auto current, auto stride){
              block_path.push_back({div(code, current, stride)});
              return expr(mod(code, current, stride));
            }),
            blocks_per_thread
        })
  };

  const auto last_block = add(code, {first_block, blocks_per_thread});
  for(auto &block_step:block_path)
    block_step = expr(constfold1(code, block_step).value_or(block_step));
  first_block = expr(constfold1(code, first_block).value_or(first_block));


  std::vector<loop_iterator> loop;
  loop.push_back(make_basic_loop_iterator(code, itrs.at(thread_pivot_), {first_block}, {last_block}));
  {
    auto init_block_path =
      when(
        code,
        views::map(
          std::views::zip(itrs, block_path),
          unpack(
            [&](auto itr, auto block_step){
              return put(code, cur(code, itr), block_step);
            })));
    auto &block_iter = loop.back();
    block_iter.init = task(
      when(
        code,
        {
          init_block_path,
          block_iter.init
        }));
  }

  for(auto itr:std::views::drop(itrs, thread_pivot_ + 1))
    loop.push_back(make_basic_loop_iterator(code, itr));
  
  return loop;
}

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
