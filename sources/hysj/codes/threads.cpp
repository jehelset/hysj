#include<hysj/codes/threads.hpp>

#include<algorithm>
#include<vector>
#include<optional>
#include<span>
#include<stdexcept>
#include<ranges>

#include<hysj/codes/code.hpp>
#include<hysj/codes/factories.hpp>
#include<hysj/codes/compile/shapes.hpp>
#include<hysj/tools/functional/any_of.hpp>
#include<hysj/tools/functional/optional.hpp>
#include<hysj/grammars.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

integer thread_partition(integer max_thread_count, std::span<const integer> extents){
  const auto rank = std::ranges::ssize(extents);
  auto pivot = 0_i;
  auto rem_threads = max_thread_count;
  auto num_threads = 1_i;
  while(pivot < rank && extents[pivot] <= rem_threads){
    rem_threads /= extents[pivot];
    num_threads *= extents[pivot];
    pivot++;
  }
  if(pivot == rank || rem_threads == 1)
    return num_threads;
  num_threads *= //fixme bug
    std::ranges::max(
      views::map(
        views::indices(2_i, rem_threads + 1_i),
        [&](auto rem_threads){
          return std::gcd(extents[pivot], rem_threads);
        }));
  return num_threads;
}
 
integer thread_pivot(integer threads,std::span<const integer> extents){
  auto pivot = 0_i;
  const auto rank = std::ranges::ssize(extents);
  while(pivot < rank && extents[pivot] <= threads)
    threads /= extents[pivot++];
  return std::min<integer>(pivot, rank - 1);
}

struct thread_{
  const codes::code &code;
  thread_info &threads;

  std::vector<std::optional<integer>> argstack;

  auto exedeg(id o)const {
    return std::ranges::count_if(relids(code, o), [](id r){ return reltag_of(r) == reltag::exe; });
  }
  auto args(id o)const{
    return std::span{argstack}.last(exedeg(o));
  }
  auto max_arg(id o)const{
    auto R = args(o);
    if(std::ranges::empty(R))
      return no<integer>;
    return std::ranges::max(R);
  }

  void pop(id o){
    argstack.resize(argstack.size() - exedeg(o));
  }
  void push(id o,std::optional<integer> c){
    threads.local_count(o) = c;
    argstack.push_back(c);
  }

  constexpr bool pre(id o){
    return with_famobj(
      [&](auto o) -> bool{
        if constexpr(o.tag == any_of(optag::noop, optag::exit)){
          push(o, {});
          return false;
        }
        else if constexpr(o.tag == optag::put){
          auto extents = ranges::vec(oexts(code, o)); //FIXME: move to header. - jeh
          push(o, thread_partition(threads.global_count, {extents}));
          return false;
        }
        return true;
      },
      task(o));
  }
  constexpr bool pre(id o,id r,id a){
    if(reltag_of(r) == none_of(reltag::exe))
      return false;
    return pre(a);
  }

  constexpr auto post(id o){
    with_famobj(
      [&](auto o){
        auto n = max_arg(o);
        pop(o);
        push(o, n);
      },
      task(o));
    return none{};
  }
  constexpr auto post(id o,id r,id a){
    return post(a);
  }
};

std::optional<integer> thread(code &c,thread_info &T,taskfam root){
  grammars::resize_objtabs(c, T.local_counts);
  compile_shape(c, root);
  thread_ ctx{c, T};
  grammars::walk<none>(c,
                       lift((ctx.pre)(&)),
                       lift((ctx.post)(&)),
                       root);
  if(ctx.argstack.size() != 1)
    throw std::runtime_error("unexpected stack");
  return ctx.argstack.front();
}


taskfam thread_fork(code &c,exprfam i,taskfam t,exprfam N){
  return {
    fork(c,
         lt(c, i, cvt_for(c, i, N)),
         {c.builtins.noop, t})
  };
}

struct thread_fork_{

  codes::code &code;
  thread_info &threads;

  exprfam thread_id;

  std::vector<kumi::tuple<taskfam, integer>> thread_counts;
  std::vector<std::optional<id>> argstack;

  auto new_args(id o)const{
    return std::span{argstack}.last(argdeg(code, o));
  }
  auto argids(id o)const{
    return
      views::map(
        std::views::iota(0_i, argdeg(code, o)),
        [&,o](auto i){
          return new_args(o)[i]
            .value_or(grammars::argids(code, o)[i])
            ;
        });
  }
  template<optag O>
  auto args(sym<O> o)const{
    return grammars::args(code, o, argids(o));
  }
  bool has_new_args(id o)const{
    return std::ranges::any_of(new_args(o), has_value);
  }

  void pop(id o){
    argstack.resize(argstack.size() - argdeg(code, o));
  }
  void push(std::optional<id> o = std::nullopt){
    argstack.push_back(o);
  }

  auto push_local_count(taskfam t){
    const auto &c = threads.local_count(t);
    if(!c || (!thread_counts.empty() && c == get<1>(thread_counts.back())))
      return;
    thread_counts.push_back(kumi::make_tuple(t, c.value()));
  }
  auto pop_local_count(taskfam t){
    if(thread_counts.empty())
      return;
    const auto &[t_,c] = thread_counts.back();
    if(t == t_)
      thread_counts.pop_back();
  }
  auto current_local_count()const{
    if(thread_counts.empty())
      return no<integer>;
    return just(get<1>(thread_counts.back()));
  }

  auto try_fork(taskfam t){
    auto n = threads.local_count(t);
    auto c = current_local_count();
    if(!n || !c || n == c) 
      return no<taskfam>;
    auto u = thread_fork(code, thread_id, t, {icst32(code, *n)}); //FIXME: use syms in thread-info - jeh
    thread(code, threads, u); //FIXME: make sure no retraverse... 
    return just(u);
  }
  template<optag O>
  auto try_new_op(sym<O> o){
    if(!has_new_args(o))
      return no<sym<O>>;
    auto u = 
      apply(
        [&](auto... a){
          return objadd<o.tag>(code, symdat(code, o), a...);
        },
        args(o));
    thread(code, threads, {u});
    return just(u);
  }

  constexpr bool pre(id o){
    const auto t = task(o);
    auto r = with_famobj(
      [&](auto o) -> bool{
        if constexpr(o.tag == any_of(optag::noop)){
          push();
          return false;
        }
        else if constexpr(o.tag == optag::put){
          push(try_fork({o}));
          return false;
        }
        return true;
      },
      t);
    if(r)
      push_local_count(t);
    return r;
  }
  constexpr bool pre(id o,id r,id a){
    auto rt = reltag_of(r);
    if(rt != reltag::exe){
      push();
      return false;
    }
    return pre(a);
  }
  constexpr auto post(id o){
    const auto t = task(o);
    pop_local_count(t);
    const auto t_push = with_famobj(
      [&](auto o) -> std::optional<taskfam>{
        auto u = try_new_op(o).transform(task);
        if(!u)
          return try_fork(t);
        return try_fork(*u).value_or(*u);
      },
      t);
    pop(o);
    push(t_push.transform(&taskfam::id));
    return none{};
  }
  constexpr auto post(id o,id r,id a){
    if(reltag_of(r) != reltag::exe)
      return none{};
    return post(a);
  }
};

std::optional<taskfam> thread_fork(code &c,thread_info &t,exprfam i,taskfam r){
  thread_fork_ ctx{c, t, i};
  grammars::walk<none>(c, lift((ctx.pre)(&)), lift((ctx.post)(&)), r.id());
  if(ctx.argstack.size() != 1)
    throw std::runtime_error("unexpected stack");
  auto &u = ctx.argstack.front();
  return u.transform(lift((task)));
}

} //hysj::codes
#include<hysj/tools/epilogue.hpp>

