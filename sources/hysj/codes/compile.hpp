#pragma once

#include<hysj/grammars.hpp>
#include<hysj/codes/code.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

HYSJ_EXPORT
void compile(code &,id o);

auto compile(code &c,grammars::concepts::forward_id auto &&O){
  for(auto o:O)
    compile(c,o);
}

inline auto compile(code &c){
  return compile(c, objids(c));
}

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
