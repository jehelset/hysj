#include<hysj/codes/statics.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes::statics{

types types::make(const syntax &c){
  types t{};
  resize(c,t);
  return t;
}

void resize(const syntax &c,types &t){
  grammars::resize_objtabs(c, t.ops);
  grammars::resize_reltabs(c, t.rels);
}

shapes shapes::make(const syntax &c){
  shapes s{
    .iterators{},
    .slots{},
    .ops{}
  };
  resize(c, s);
  return s;
}
void resize(const syntax &c,shapes &s){
  grammars::resize_objtabs(c, s.ops);
}

domains domains::make(const syntax &c){
  domains d{};
  resize(c,d);
  return d;
}

void resize(const syntax &c,domains &d){
  grammars::resize_objtabs(c, d.ops);
  grammars::resize_reltabs(c, d.rels);
}

} //hysj::codes::statics
#include<hysj/tools/epilogue.hpp>
