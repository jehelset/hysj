#pragma once
#include<numeric>
#include<variant>
#include<vector>
#include<tuple>
#include<span>

#include<fmt/format.h>

#include<hysj/grammars.hpp>
#include<hysj/codes/sets.hpp>
#include<hysj/codes/code.hpp>
#include<hysj/codes/access.hpp>
#include<hysj/codes/statics.hpp>
#include<hysj/codes/grammar.hpp>
#include<hysj/tools/preprocessor.hpp>
#include<hysj/tools/reflection.hpp>
#include<hysj/tools/types.hpp>
#include<hysj/tools/functional/any_of.hpp>
#include<hysj/tools/functional/optional.hpp>
#include<hysj/tools/ranges/fold.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

template<typename C>
struct cellalloc {
  using value_type = C;

  std::optional<std::size_t> alignment;
  
  auto allocate(std::size_t size) {
    return new (std::align_val_t(alignment.value_or(alignof(value_type)))) value_type[size];
  }

  void deallocate(value_type *ptr, std::size_t size){
    ::operator delete[](ptr, std::align_val_t(alignment.value_or(alignof(value_type))));
  }

  auto operator<=>(const cellalloc &) const = default;
};

template<set S,natural W>
struct cellvec{
  using C = cell<S, W>;
  std::vector<C, cellalloc<C>> container;

  friend HYSJ_MIRROR_STRUCT(cellvec, (container));

  auto operator<=>(const cellvec &) const = default;
};

using celltup = typename decltype(
  []<std::size_t... I>(std::index_sequence<I...>){
    return type_c<std::tuple<cellvec<typetab[I].set, typetab[I].width>...>>;
  }(std::make_index_sequence<std::size(typetab)>{})
)::type;

struct HYSJ_EXPORT cellinfo : type{
  integer offset;
  std::vector<integer> shape;

  friend HYSJ_MIRROR_STRUCT(cellinfo,(offset, shape)(codes::type));

  constexpr auto size()const{
    return ranges::prod(shape);
  }
};

struct HYSJ_EXPORT cellinfos{

  std::optional<natural> alignment;
  std::array<integer, typetab.size()> sizes;
  std::vector<kumi::tuple<grammars::id, cellinfo>> container;

  auto &operator[](grammars::id id)const{
    auto it = std::ranges::find(container, id, lift((get<0>)));
    if(it == std::ranges::end(container))
      throw std::out_of_range("unknown id");
    return get<1>(*it);
  }

  bool contains(grammars::id id)const{
    return ranges::contains(views::map(container, lift((get<0>))), id);
  }
  std::size_t size(type t)const{
    return sizes[typeindex(t).value()];
  }
  std::size_t empty(type t)const{
    return size(t) == 0;
  }

  friend HYSJ_MIRROR_STRUCT(cellinfos,(alignment, sizes, container));

  HYSJ_EXPORT
  static cellinfos make(const code &,const accesses &,devsym,std::optional<std::size_t> = std::nullopt);
};

HYSJ_EXPORT const cellinfo &place(const code &,cellinfos &,grammars::id);
HYSJ_EXPORT void place(const code &,const accesses &,cellinfos &,devsym);

struct HYSJ_EXPORT cells{

  cellinfos infos;
  celltup data;

  friend HYSJ_MIRROR_STRUCT(cells,(infos, data));

  auto operator<=>(const cells &) const = default;

  constexpr auto &info(id o)const{
    return infos[o];
  }
  
  template<set D,natural W>
  constexpr auto get(constant_t<D>,constant_t<W>){
    return std::span{std::get<cellvec<D, W>>(data).container};
  }
  template<set D,natural W>
  constexpr auto get(constant_t<D>,constant_t<W>)const{
    return std::span{std::get<cellvec<D, W>>(data).container};
  }
  template<set D,natural W>
  constexpr auto get(constant_t<D> d,constant_t<W> w,id o){
    const auto &s = info(o);
    return get(d, w).subspan(s.offset,s.size());
  }
  template<set D,natural W>
  constexpr auto get(constant_t<D> d,constant_t<W> w,id o)const{
    const auto &s = info(o);
    return get(d, w).subspan(s.offset,s.size());
  }

  auto bytes(type t){
    return with_type(
      [&](auto s, auto w){
        return std::as_writable_bytes(get(s, w));
      },
      t);
  }
  auto bytes(type t)const{
    return with_type(
      [&](auto s, auto w){
        return std::as_bytes(get(s, w));
      },
      t);
  }

  #define HYSJ_LAMBDA(ids,ch)                                                                         \
    template<natural W> constexpr auto ch(this auto &&c,constant_t<W> w,id o){ return fwd(c).get(codes::ids##_c,w,o); }
  HYSJ_MAP_LAMBDA(HYSJ_CODES_SETS)
  #undef HYSJ_LAMBDA

  #define HYSJ_LAMBDA(ids,ch,w)                                       \
    constexpr auto ch##w(this auto &&c,id o){ return fwd(c).get(codes::ids##_c,natural_c<w>,o); }
  HYSJ_MAP_LAMBDA(HYSJ_CODES_TYPES)
  #undef HYSJ_LAMBDA

  auto bytes(id o){
    return with_type(
      [&](auto s, auto w){
        return std::as_writable_bytes(get(s, w, o));
      },
      info(o));
  }
  auto bytes(id o)const{
    return with_type(
      [&](auto s, auto w){
        return std::as_bytes(get(s, w, o));
      },
      info(o));
  }

  constexpr codes::type type(id o)const{ return this->info(o); }
  constexpr auto size(id o)const{ return this->info(o).size(); }
  constexpr auto shape(id o)const{ return std::span{this->info(o).shape}; }

  HYSJ_EXPORT
  static cells make(cellinfos);
};

HYSJ_EXPORT
cells alloc(const code &,codes::apisym,std::optional<std::size_t> alignment = {});

} //hysj::codes
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

using codes::cells;
using codes::alloc;

} //hysj
template<typename C>
HYSJ_STRUCT_FORMATTER((hysj::_HYSJ_VERSION_NAMESPACE::codes::cellinfo), C);
#include<hysj/tools/epilogue.hpp>
