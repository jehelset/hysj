#include<hysj/codes/to_string.hpp>

#include<functional>
#include<string>
#include<type_traits>
#include<variant>

#include<fmt/format.h>

#include<hysj/codes/grammar.hpp>
#include<hysj/grammars.hpp>
#include<hysj/tools/functional/any_of.hpp>
#include<hysj/tools/graphs/algorithms/deeporders.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/types.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

std::string to_string(const code &f,id r){
  std::string s{};

  auto put = [&](auto id){
    using enum optag;
    if constexpr(id.tag() == any_of(cst, lit))
      std::visit([&](auto v){
        if constexpr(std::is_same_v<decltype(v),bvalue>)
          s.append(v.cell ? "⟙" : "⟘");
        else if constexpr(std::is_same_v<decltype(v),literal>){
          using enum literal;
          switch(v){
          case bot: s.append("'⟘"); break;
          case top: s.append("'⟙"); break;
          case zero: s.append("'0"); break;
          case half: s.append("'½"); break;
          case neghalf: s.append("'-½"); break;
          case one: s.append("'1"); break;
          case negone: s.append("'-1"); break;
          case two: s.append("'2"); break;
          case inf: s.append("∞"); break;
          case neginf: s.append("-∞"); break;
          case pi: s.append("π"); break;
          case e: s.append("e"); break;
          }
        }
        else
          s.append(fmt::format("{}",v));
      },
      symdat(f, id));

    if constexpr(typ  == id.tag()) s.append(fmt::format("τ",id.idx));
    if constexpr(itr  == id.tag()) s.append(fmt::format("i{}",id.idx));
    if constexpr(var  == id.tag()) s.append(fmt::format("x{}",id.idx));
    if constexpr(rfl  == id.tag()) s.append("ϑ");
    if constexpr(con  == id.tag()) s.append("ψ");
    if constexpr(rot  == id.tag()) s.append("@");
    if constexpr(cur  == id.tag()) s.append("▶");
    if constexpr(cvt  == id.tag()) s.append("⊩");
    if constexpr(neg  == id.tag()) s.push_back('-');
    if constexpr(sin  == id.tag()) s.push_back('~');
    if constexpr(mod  == id.tag()) s.push_back('%');
    if constexpr(fct  == id.tag()) s.push_back('!');
    if constexpr(add  == id.tag()) s.push_back('+');
    if constexpr(sub  == id.tag()) s.push_back('-');
    if constexpr(mul  == id.tag()) s.push_back('*');
    if constexpr(div  == id.tag()) s.push_back('/');
    if constexpr(pow  == id.tag()) s.push_back('^');
    if constexpr(log  == id.tag()) s.append("log");
    if constexpr(der  == id.tag()) s.append("δ");
    if constexpr(bnot == id.tag()) s.push_back('~');
    if constexpr(blsh == id.tag()) s.append("<<");
    if constexpr(brsh == id.tag()) s.append(">>");
    if constexpr(bor  == id.tag()) s.push_back('|');
    if constexpr(band == id.tag()) s.push_back('&');
    if constexpr(bxor == id.tag()) s.append("⊻");
    if constexpr(eq   == id.tag()) s.push_back('=');
    if constexpr(lt   == id.tag()) s.push_back('<');
    if constexpr(lnot == id.tag()) s.append("¬");
    if constexpr(land == id.tag()) s.append("∧");
    if constexpr(lor  == id.tag()) s.append("∨");
    if constexpr(sel  == id.tag()) s.push_back('?');
    if constexpr(put  == id.tag()) s.append("⃪");
    if constexpr(then == id.tag()) s.append("➙");
    if constexpr(when == id.tag()) s.append("↩");
    if constexpr(fork == id.tag()) s.append("⋔");
    if constexpr(to == id.tag()) s.append("⇄");
    if constexpr(on == id.tag()) s.append("↓");
    if constexpr(noop == id.tag()) s.append("∅");
    if constexpr(exit == id.tag()) s.append("↑");
  };

  for(auto [order,vertex]:graphs::oorder(std::cref(f.graph),objvert(f,r)))
    with_op(
      [&](auto op){
        if(order == graphs::dfo::pre_tag()){
          if constexpr(op.tag() == any_of(optag::typ))
            s.push_back('{');
          else if constexpr(op.tag() == any_of(optag::var, optag::rfl, optag::cst))
            s.push_back('[');
          else if constexpr(grammars::objvar<op.tag()> || grammars::objnum<op.tag()> > 1)
            s.push_back('(');
          else if constexpr(op.tag() == any_of(optag::itr))
            s.push_back('<');
          put(op);
          s.push_back(' ');
        }
        else{
          if constexpr(op.tag() == any_of(optag::typ)){
            s.pop_back();
            s.push_back('}');
            s.push_back(' ');
          }
          else if constexpr(op.tag() == any_of(optag::var, optag::rfl, optag::cst)){
            s.pop_back();
            s.push_back(']');
            s.push_back(' ');
          }
          else if constexpr(grammars::objvar<op.tag()> || grammars::objnum<op.tag()> > 1){
            s.pop_back();
            s.push_back(')');
            s.push_back(' ');
          }
          else if constexpr(op.tag() == any_of(optag::itr)){
            s.pop_back();
            s.push_back('>');
            s.push_back(' ');
          }
        }
      },
      objid(f, vertex));
  s.pop_back();
  return s;

}

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
