#include<hysj/codes/constants.hpp>

#include<optional>

#include<hysj/codes/sets.hpp>
#include<hysj/tools/functional/ignore.hpp>
#include<hysj/tools/functional/overload.hpp>
#include<hysj/tools/functional/variant.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

std::optional<bool> is_zero(const constant &c){
  return visitor([](ivalue v)->std::optional<bool>{ return v == 0; },
                 [](nvalue v)->std::optional<bool>{ return v == 0; },
                 [](bvalue v)->std::optional<bool>{ return v == false; },
                 [](rvalue v)->std::optional<bool>{ return v == 0.0; },
                 [](literal v)->std::optional<bool>{ return v == literal::zero; },
                 [](auto)->std::optional<bool>{ return std::nullopt; })
  (c);
}

std::optional<bool> is_one(const constant &c){
  return visitor([](ivalue v){ return v == 1; },
                 [](nvalue v){ return v == 1; },
                 [](rvalue v){ return v == 1.0; },
                 [](bvalue v)->bool{ return v; },
                 [](literal v){ return v == literal::one; },
                 [](auto){ return false; })
         (c);
}
std::optional<bool> is_negone(const constant &c){
  return visitor([](ivalue v){ return v == -1; },
                 [](rvalue v){ return v == -1.0; },
                 [](literal v){ return v == literal::negone; },
                 [](auto){ return false; })
         (c);
}
  
std::optional<bool> is_odd(const constant &c){
  return visitor([](ivalue v)-> std::optional<bool> { return v % 2; },
                 [](nvalue v)-> std::optional<bool> { return v % 2; },
                 [](bvalue v) -> std::optional<bool>{ return v; },
                 [](literal v) -> std::optional<bool>{
                   switch(v){
                     case literal::zero: return false;
                     case literal::one: return true;
                     case literal::negone: return true;
                     case literal::two: return false;
                     default: return std::nullopt;
                   }
                 },
                 [](auto) -> std::optional<bool>{
                   return std::nullopt;
                 })(c);
}

set cstdom(const constant &c){
  return visitor(
    [](const set &){ return set::integers; },
    []<set F>(const value<F> &){ return F; },
    [](const literal &s){
      if(s == any_of(literal::bot,literal::top))
        return set::booleans;
      else if(s == any_of(literal::zero,literal::one,literal::negone,literal::two))
        return set::integers;
      else
        return set::reals;
    })(c);
}

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
