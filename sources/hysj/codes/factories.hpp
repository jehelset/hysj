#pragma once
#include<vector>

#include<hysj/codes/builtins.hpp>
#include<hysj/codes/code.hpp>
#include<hysj/codes/compile.hpp>
#include<hysj/codes/constants.hpp>
#include<hysj/codes/sets.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

constexpr auto neq(code &c,id l,id r){
  return lnot(c, eq(c, r, l));
}
constexpr auto leq(code &c,id l,id r){
  return lnot(c, lt(c, r, l));
}
constexpr auto geq(code &c,id l,id r){
  return lnot(c, lt(c, l, r));
}
constexpr auto obs(code &c,id e){
  auto b = rfl(c, e);
  return std::make_tuple(put(c, b, e), b);
}
constexpr auto obs(code &c,id e,id w){
  auto b = var(c, typ(c, rfl(c, e), w), rfl(c, e));
  return std::make_tuple(put(c, b, e), b);
}
constexpr auto cvt_for(code &c,id d,id e){
  return cvt(c, rfl(c, d), e);
}
constexpr auto inc(code &c,id d){ //FIXME: olit or rfl!
  return add(c, {d, cvt_for(c, d, c.builtins.lits.one32)});
}
constexpr auto dec(code &c,id d){
  return sub(c, d, cvt_for(c, d, c.builtins.lits.one32));
}

#define HYSJ_LAMBDA(S,c)                                               \
  inline auto c##lit(code &s,c##cell<> v){ return lit(s, constant{c##value{v}}); }
HYSJ_MAP_LAMBDA(HYSJ_CODES_SETS)
#undef HYSJ_LAMBDA

constexpr auto itr_dup(code &s,itrsym i){
  return itr(s, rfl(s, i));
}

constexpr auto tmp_dup(code &c,id o,id w){
  return var(c, typ(c, rfl(c, o), w), rfl(c, o));
}
constexpr auto tmp_dup(code &s,id o){
  return var(s, rfl(s, o), rfl(s, o));
}
constexpr auto tmp_for(code &s,id o,std::vector<id> e = {}){
  return var(s, rfl(s, o), std::move(e));
}
constexpr auto tmp_for(code &c,id o,set t,std::vector<id> e = {}){
  return var(c, typ(c, c.builtins.sets[t], rfl(c, o)), std::move(e));
}
constexpr auto buf_for(code &c,codes::exprfam o,std::vector<codes::dimexprfam> e){
  compile(c, o);
  return var(c, rfl(c, o), views::cat(e, views::map(oitrs(c, o), codes::dimexpr)));
}

#define HYSJ_LAMBDA(w_)                                         \
  inline auto cst##w_(code &s,constant c){ return cst(s, std::move(c), s.builtins.widths.w##w_); }
HYSJ_MAP_LAMBDA(HYSJ_CODES_WIDTHS)
#undef HYSJ_LAMBDA
#define HYSJ_LAMBDA(S,c,w_)                                              \
  inline auto c##cst##w_(code &s,c##cell<> v){ return cst(s, constant{c##value{v}}, s.builtins.widths.w##w_); }
HYSJ_MAP_LAMBDA(HYSJ_CODES_TYPES)
#undef HYSJ_LAMBDA
#define HYSJ_LAMBDA(S,c)                                               \
  inline auto c##cst(code &s,c##cell<> v,id w){ return cst(s, constant{c##value{v}}, w); } \
  inline auto c##cst(code &s,c##cell<> v,natural w){ return cst(s, constant{c##value{v}}, s.builtins.widths[w]); }
HYSJ_MAP_LAMBDA(HYSJ_CODES_SETS)
#undef HYSJ_LAMBDA
#define HYSJ_LAMBDA(l,w)                                             \
  constexpr auto l##w(code &c){                                      \
    return cst(c,cstdat{constant{literal::l}},c.builtins.widths[w]); \
  }
HYSJ_MAP_LAMBDA(HYSJ_CODES_LITS)
#undef HYSJ_LAMBDA

#define HYSJ_LAMBDA(S,c)                                               \
  inline auto c##typ(code &s,id w){ return typ(s, s.builtins.sets.S, w); }               \
  inline auto c##typ(code &s,natural w){ return typ(s, s.builtins.sets.S, s.builtins.widths[w]); }
HYSJ_MAP_LAMBDA(HYSJ_CODES_SETS)
#undef HYSJ_LAMBDA
#define HYSJ_LAMBDA(w_)                                         \
  inline auto typ##w_(code &s,id d){ return typ(s, d, s.builtins.widths[w_]); }
HYSJ_MAP_LAMBDA(HYSJ_CODES_WIDTHS)
#undef HYSJ_LAMBDA
#define HYSJ_LAMBDA(S,c,w)                                            \
  inline auto c##typ##w(const code &s){ return s.builtins.types[set::S, w]; }
HYSJ_MAP_LAMBDA(HYSJ_CODES_TYPES)
#undef HYSJ_LAMBDA

#define HYSJ_LAMBDA(S,c)                                                \
  inline auto c##var(code &s,id w,std::vector<id> op1 = {}){ return var(s,typ(s, s.builtins.sets.S, w),op1); }                \
  inline auto c##var(code &s,natural w,std::vector<id> op1 = {}){ return var(s,typ(s, s.builtins.sets.S, s.builtins.widths[w]),op1); }
HYSJ_MAP_LAMBDA(HYSJ_CODES_SETS)
#undef HYSJ_LAMBDA
#define HYSJ_LAMBDA(w_)                                         \
  inline auto var##w_(code &s,id d,std::vector<id> op1 = {}){ return var(s, typ##w_(s, d), op1); }
HYSJ_MAP_LAMBDA(HYSJ_CODES_WIDTHS)
#undef HYSJ_LAMBDA
#define HYSJ_LAMBDA(S,c,w)                                              \
  inline auto c##var##w(code &s,std::vector<id> op1 = {}){ return var(s,s.builtins.types[set::S,w],op1); }
HYSJ_MAP_LAMBDA(HYSJ_CODES_TYPES)
#undef HYSJ_LAMBDA

#define HYSJ_LAMBDA(S,c,w)                                            \
  inline auto c##cvt##w(code &c, id o){ return cvt(c, c.builtins.types[set::S, w], o); }
HYSJ_MAP_LAMBDA(HYSJ_CODES_TYPES)
#undef HYSJ_LAMBDA

constexpr auto min(code &c, id l, id r){
  return sel(c, lt(c, l, r), {r, l});
}
constexpr auto max(code &c, id l, id r){
  return sel(c, lt(c, l, r), {l, r});
}
constexpr auto clamp(code &c, id l, id x, id r){
  return max(c, l, min(c, x, r));
}

constexpr auto abs(code &c,id x){
  return max(c, x, neg(c, x));
}

constexpr auto ln(code &s,natural w,id op){ return log(s,id(s.builtins.lits[literal::e, w]),op); }

constexpr auto pin(code &c,id d,id e){
  return to(c, d, d, e);
}
constexpr auto pin(code &c,id e){
  return to(c, c.builtins.host, c.builtins.host, e);
}

constexpr auto send(code &c,id d,id b){
  return to(c, c.builtins.host, d, b);
}
constexpr auto recv(code &c,id d,id b){
  return to(c, d, c.builtins.host, b);
}

inline constexpr auto olit_for = [](code &c,id o,literal l){
  compile(c, o);
  return c.builtins.lits[l, owidth(c, o)];
};
inline constexpr auto rlit_for = [](code &c,id r,literal l){
  compile(c, relobjid(c, r));
  return c.builtins.lits[l, rwidth(c, r)];
};

#define HYSJ_LAMBDA(l)                                          \
  inline constexpr auto o##l = bind<-1>(olit_for, literal::l);  \
  inline constexpr auto r##l = bind<-1>(rlit_for, literal::l);
HYSJ_MAP_LAMBDA(HYSJ_CODES_LITERALS)
#undef HYSJ_LAMBDA

constexpr auto cos(code &c, id a){
  return sin(c, add(c, {div(c, c.builtins.lits.pi32, c.builtins.lits.two32), a})); //FIXME: lit_for... - jeh
}
constexpr auto tan(code &c, id a){
  return div(c, sin(c, a), cos(c, a));
}

constexpr auto if_(code &c, exprfam p, taskfam t){
  return fork(c, p, {c.builtins.noop, t});
}
constexpr auto ifel(code &c, exprfam p, taskfam t, taskfam f){
  return fork(c, p, {f, t});
}

constexpr auto sum(code &c, exprfam f){
  return con(c, add(c, f));
}
constexpr auto prod(code &c, exprfam f){
  return con(c, mul(c, f));
}
constexpr auto lany(code &c, exprfam f){
  return con(c, lor(c, f));
}
constexpr auto lall(code &c, exprfam f){
  return con(c, land(c, f));
}
constexpr auto lnone(code &c, exprfam f){
  return lnot(c, lany(c, f));
}

constexpr auto sqrt(code &c, exprfam f){
  return pow(c, f, ohalf(c, f));
}
constexpr auto pow2(code &c, exprfam f){
  return pow(c, f, otwo(c, f));
}

constexpr auto rms(code &c, exprfam f){
  compile(c, f); //FIXME: exts(f) op or similar - jeh
  auto N = icst32(c, std::max(1_i, ranges::prod(oexts(c, f))));
  return sqrt(c, {div(c, sum(c, {pow2(c, f)}), cvt_for(c, f, N))});
}

constexpr auto sign(code &c, exprfam f){
  return sel(
    c,
    lt(c,f,ozero(c, f)),
    {
      oone(c, f),
      onegone(c, f)
    });
}


} //hysj::codes
#include<hysj/tools/epilogue.hpp>
