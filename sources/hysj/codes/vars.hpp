#pragma once
#include<array>
#include<concepts>
#include<ranges>
#include<optional>
#include<utility>
#include<vector>

#include<hysj/codes/code.hpp>
#include<hysj/codes/compile.hpp>
#include<hysj/tools/functional/unwrap.hpp>
#include<hysj/tools/functional/bind.hpp>
#include<hysj/tools/functional/compose.hpp>
#include<hysj/tools/preprocessor.hpp>
#include<hysj/tools/ranges/join.hpp>
#include<hysj/tools/ranges/map.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/reflection.hpp>
#include<hysj/tools/tag_invoke.hpp>
#include<hysj/tools/type_traits.hpp>
#include<hysj/tools/types.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

#define HYSJ_CODE_VARS \
  (algebraic)          \
  (differential)

enum class depvartag : unsigned {
  HYSJ_SPLAT(0,HYSJ_CODE_VARS)
};
HYSJ_MIRROR_ENUM(depvartag,HYSJ_CODE_VARS)

constexpr auto varcount(const auto &X)
  arrow((static_cast<natural>(std::ranges::distance(X))))
  ;

constexpr auto varorder(const auto &x)
  arrow((static_cast<natural>(std::ranges::distance(x)) - 1))
  ;
  
constexpr auto varclassify(const auto &x)
  arrow((varorder(x) == 0_n ? depvartag::algebraic : depvartag::differential))
  ;

#define HYSJ_LAMBDA(x) inline constexpr auto is_##x = [](const auto &x){ return varclassify(x) == depvartag::x; };
HYSJ_MAP_LAMBDA(HYSJ_CODE_VARS)
#undef HYSJ_LAMBDA

namespace concepts{
    
  template<typename T>
  concept independent = std::convertible_to<T,codes::exprfam>;

  template<typename T>
  concept dependent = std::ranges::forward_range<T> &&
    ranges::concepts::convertible_forward_range<std::ranges::range_value_t<T>,codes::exprfam>;

}

namespace concepts{
  template<typename V>
  concept vars = requires(V v){
    { v.independent } -> concepts::independent;
    { v.dependent   } -> concepts::dependent;
  };
}

struct vars{
  codes::exprfam independent;
  std::vector<std::vector<codes::exprfam>> dependent;

  friend HYSJ_MIRROR_STRUCT(vars,(independent,dependent))

  auto operator<=>(const vars &)const = default;

  constexpr auto ids()const noexcept{
    return views::catcast<id>(views::one(independent),
                              views::join(dependent));
  }

  std::optional<kumi::tuple<std::size_t, std::size_t>> find_dependent(codes::exprfam expr)const{
    for(auto i:views::indices(dependent))
      for(auto j:views::indices(dependent[i]))
        if(dependent[i][j] == expr)
          return just(kumi::make_tuple(static_cast<std::size_t>(i),
                                       static_cast<std::size_t>(j)));
    return no<kumi::tuple<std::size_t, std::size_t>>;
  }
};
static_assert(concepts::vars<vars>);

HYSJ_EXPORT
std::vector<exprfam> depvar(code &, exprfam independent_variable, natural dep_order, exprfam dependent);

} //hysj::codes
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

using vars = codes::vars;

using codes::varcount;
using codes::varorder;
using codes::varclassify;
using codes::depvar;
using codes::depvartag;

} //hysj

template<typename C>
HYSJ_STRUCT_FORMATTER((hysj::codes::vars), C);

#include<hysj/tools/epilogue.hpp>
