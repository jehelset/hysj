#include<hysj/codes/access.hpp>

#include<algorithm>
#include<utility>
#include<ranges>
#include<vector>

#include<hysj/codes/algorithms/constfolds.hpp>
#include<hysj/codes/code.hpp>
#include<hysj/codes/eval.hpp>
#include<hysj/codes/factories.hpp>
#include<hysj/codes/grammar.hpp>
#include<hysj/codes/statics.hpp>
#include<hysj/tools/functional/apply.hpp>
#include<hysj/tools/functional/bind.hpp>
#include<hysj/tools/graphs/algorithms/deeporders.hpp>
#include<hysj/tools/ranges/keep.hpp>
#include<hysj/tools/ranges/map.hpp>
#include<hysj/tools/ranges/utility.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::_HYSJ_VERSION_NAMESPACE::codes{

exprfam access_for(code &C,const exprfam &e){
  if(can_access(e.id()))
    return e;
  return {rfl(C, e.id())};
}

std::vector<id> strides_of(code &c, id o){
  auto u = accessed(c, o).value();
  auto D = odims(c, u);
  const auto w = iwidth(c, u);
  return strides_of(c, w, D);
}

bool needs_wrap(const code &c,id i,id s){
  if(i == s)
    return false;
  const auto &i_domain = odomain(c.statics.domains, i).value(),
    &s_domain = odomain(c.statics.domains, s).value();

  auto c_domain = cover(c, s_domain, i_domain);
  return c_domain != i_domain;
}

id index_of(code &c,id o,const std::vector<id> &N){

  auto u = accessed(c, o).value();
  const auto r = orank(c, u);
  const auto w = iwidth(c, u);
  if(r == 0)
    return c.builtins.lits[literal::zero, w];

  //FIXME: reconsider rot(a, {i, j})... - jeh
  auto i = with_access(
    [&](auto b, auto S) -> id{
      auto I = oitrs(c, b);
      auto tmp = ranges::vec(
        std::views::zip(N, I, S),
        bind<>(
          apply,
          [&](id n,id i,id s) -> id {
            if(needs_wrap(c, i, s))
              s = mod(c, s, itrdim(c, i));
            return mul(c, {n, s});
          }));
      if(std::ranges::empty(tmp))
        return c.builtins.lits[literal::zero, w];
      return add(c, tmp);
    },
    c, o);
  return i;
}

accesses accesses::make(const code &c,id b){

  accesses A;

  auto ensure_dev = [&](devsym d){
    auto &D = get<0>(A.vertex_map);
    if(auto it = std::ranges::find_if(D, [&](const auto &m){ return get<0>(m) == d; });
       it != std::ranges::end(D))
      return get<1>(*it);
    return get<1>(D.emplace_back(kumi::make_tuple(d, A.graph.vertex(d.id()))));
  };
  auto ensure_buf = [&](exprfam e){
    auto &E = get<1>(A.vertex_map);
    if(auto it = std::ranges::find_if(E, [&](const auto &m){ return get<0>(m) == e; });
       it != std::ranges::end(E))
      return get<1>(*it);
    return get<1>(E.emplace_back(kumi::make_tuple(e, A.graph.vertex(e.id()))));
  };

  auto ensure_access = [&](graphs::vertex_id v_0,graphs::vertex_id v_1,exprfam a){
    auto it = std::ranges::find_if(
      A.graph.edges.container,
      [&,v = std::array{v_0, v_1}](const auto &e){
        return e.ports == v && e.value == a;
      });
    if(it == std::ranges::end(A.graph.edges.container))
      A.graph.edge(v_0, v_1, a);
  };
  auto ensure_input = [&](devsym d, exprfam e){
    auto d_v = ensure_dev(d);
    auto b_v = ensure_buf(expr(accessed(c, e.id()).value()));
    ensure_access(b_v, d_v, e);
  };
  auto ensure_output = [&](devsym d, exprfam e){
    auto d_v = ensure_dev(d);
    auto b_v = ensure_buf(expr(accessed(c, e.id()).value()));
    ensure_access(d_v, b_v, e);
  };

  std::vector<devsym> on{
    c.builtins.host
  };
  ensure_dev(c.builtins.host);

  auto pre0 = [&](id o){
    return with_op(
      [&](auto o){
        if constexpr(o.tag == optag::on){
          auto [d, t] = args(c, o);
          ensure_dev(d);
          on.push_back(d);
        }
        else if constexpr(o.tag == optag::to){
          auto [s,d,b] = args(c, o);
          ensure_dev(d);
          ensure_output(s,b);
          ensure_input(d,b);
        }
        return true;
      },
      o);
  };
  auto pre1 = [&](id o,id r,id a){
    if(optag_of(o) == optag::rfl
       || reltag_of(r) == any_of(reltag::dom, reltag::dim) //NOTE: consider dim - jeh
       || (is_access(c, o) && reltag_of(r) == reltag::src))
      return false;

    if(!is_access(c, a))
      return pre0(a);

    if(reltag_of(r) == reltag::dst)
      ensure_output(on.back(), expr(a));
    else
      ensure_input(on.back(), expr(a));
    return true;
  };
  auto post0 = [&](id o){
    with_op(
      [&](auto o){
        if constexpr(o.tag == optag::on){
          on.pop_back();
        }
      },
      o);
    return none{};
  };
  auto post1 = [&](id o,id r,id a){
    return post0(a);
  };
  grammars::walk<none>(
    c,
    overload(pre0, pre1),
    overload(post0, post1),
    b);

  return A;
}

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
