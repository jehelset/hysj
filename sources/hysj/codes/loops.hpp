#pragma once

#include<vector>

#include<hysj/codes/code.hpp>
#include<hysj/codes/grammar.hpp>
#include<hysj/grammars.hpp>
#include<hysj/tools/reflection.hpp>
#include<hysj/tools/ranges/utility.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

struct loop_iterator{
  itrsym sym;
  exprfam test;
  taskfam init, head, tail;

  friend HYSJ_MIRROR_STRUCT(loop_iterator,(sym, test, init, head, tail));

  auto operator<=>(const loop_iterator &) const = default;
};

namespace concepts{
  template<typename T>
  concept forward_loop_iterators = ranges::concepts::convertible_forward_range<T, loop_iterator>;
}

HYSJ_EXPORT loop_iterator make_basic_loop_iterator(code &,itrsym,exprfam begin,exprfam end);
HYSJ_EXPORT loop_iterator make_basic_loop_iterator(code &,itrsym);

HYSJ_EXPORT std::vector<loop_iterator> make_basic_loop(code &,grammars::id);
HYSJ_EXPORT std::vector<loop_iterator> make_basic_loop(code &,exprfam);
HYSJ_EXPORT std::vector<loop_iterator> make_basic_loop(code &,putsym);

HYSJ_EXPORT std::vector<loop_iterator> make_inner_loop(code &,consym);
HYSJ_EXPORT std::vector<loop_iterator> bulk(code &,putsym,exprfam thread,exprfam thread_count);

} //hysj::codes

template <typename C>
HYSJ_STRUCT_FORMATTER((hysj::codes::loop_iterator), C);

#include<hysj/tools/epilogue.hpp>
