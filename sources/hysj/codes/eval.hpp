#pragma once
#include<optional>
#include<utility>

#include<hysj/codes/code.hpp>
#include<hysj/codes/compile.hpp>
#include<hysj/codes/sets.hpp>
#include<hysj/codes/statics.hpp>
#include<hysj/tools/functional/bind.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

HYSJ_EXPORT
std::optional<anycell> eval(const code& c,set,id);

template<set S>
auto eval(const code& c,constant_t<S>,id o){
  return eval(c, S, o).transform([](auto &&v){ return std::get<etoi(S)>(fwd(v)); });
}

#define HYSJ_LAMBDA(S,s) inline constexpr auto s##eval = bind<1>(lift((eval)), S##_c);
HYSJ_MAP_LAMBDA(HYSJ_CODES_SETS)
#undef HYSJ_LAMBDA

inline auto cceval(code& c,set s,id o){
  compile(c, o);
  return eval(c, s, o);
}
template<set S>
auto cceval(code& c,constant_t<S>,id o){
  return cceval(c, S, o).transform([](auto &&v){ return std::get<etoi(S)>(fwd(v)); });
}

#define HYSJ_LAMBDA(S,s) inline constexpr auto s##cceval = bind<1>(lift((cceval)), S##_c);
HYSJ_MAP_LAMBDA(HYSJ_CODES_SETS)
#undef HYSJ_LAMBDA

HYSJ_EXPORT
statics::domain cover(const code &,const statics::domain &,const statics::domain &);
inline auto cccover(code &c,const statics::domain &d0,const statics::domain &d1){
  compile(c, std::initializer_list<id>{d0.min, d0.max, d1.min, d1.max});
  return cover(c, d0, d1);
}

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
