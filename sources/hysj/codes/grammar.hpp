#pragma once

#include<compare>
#include<concepts>
#include<functional>
#include<tuple>
#include<utility>

#include<hysj/codes/constants.hpp>
#include<hysj/codes/sets.hpp>
#include<hysj/grammars.hpp>
#include<hysj/tools/functional/bind.hpp>
#include<hysj/tools/functional/compose.hpp>
#include<hysj/tools/functional/optional.hpp>
#include<hysj/tools/preprocessor.hpp>
#include<hysj/tools/ranges/keep.hpp>
#include<hysj/tools/ranges/map.hpp>
#include<hysj/tools/reflection.hpp>
#include<hysj/tools/trees.hpp>
#include<hysj/tools/types.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

namespace codes{

  using grammars::idx_of;

  #define HYSJ_CODES_OPS \
    (lit )               \
    (typ )               \
    (itr )               \
    (cst )               \
    (cur )               \
    (var )               \
    (rfl )               \
    (cvt )               \
    (con )               \
    (rot )               \
    (neg )               \
    (sin )               \
    (mod )               \
    (fct )               \
    (add )               \
    (sub )               \
    (mul )               \
    (div )               \
    (pow )               \
    (log )               \
    (der )               \
    (bnot)               \
    (blsh)               \
    (brsh)               \
    (bor)                \
    (band)               \
    (bxor)               \
    (eq  )               \
    (lt  )               \
    (lnot)               \
    (land)               \
    (lor )               \
    (sel )               \
    (put )               \
    (noop)               \
    (loop)               \
    (exit)               \
    (fork)               \
    (then)               \
    (when)               \
    (dev)                \
    (on)                 \
    (to)                 \
    (api)

  enum class optag : natural {
    HYSJ_SPLAT(0,HYSJ_CODES_OPS)
  };
  HYSJ_MIRROR_ENUM(optag,HYSJ_CODES_OPS)

  inline constexpr auto optag_of   = bind<>(grammars::tag_of,type_c<optag>);
  inline constexpr auto with_optag = bind<>(grammars::with_symtag,type_c<optag>);
  inline constexpr auto with_op    = bind<>(grammars::with_sym,type_c<optag>);

  #define HYSJ_CODES_RELS \
    (dom)                 \
    (dim)                 \
    (idx)                 \
    (jmp)                 \
    (alt)                 \
    (src)                 \
    (dst)                 \
    (exe)                 \
    (lnk)

  enum class reltag : natural {
    HYSJ_SPLAT(0,HYSJ_CODES_RELS)
  };
  HYSJ_MIRROR_ENUM(reltag,HYSJ_CODES_RELS)

  inline constexpr auto reltag_of   = bind<>(grammars::tag_of,type_c<reltag>);
  inline constexpr auto with_reltag = bind<>(grammars::with_symtag,type_c<reltag>);
  inline constexpr auto with_rel    = bind<>(grammars::with_sym,type_c<reltag>);

  #define HYSJ_CODES_FAMS \
    (domexpr)             \
    (dimexpr)             \
    (typexpr)             \
    (expr)                \
    (task)                \
    (decl)

  enum class famtag : natural {
    HYSJ_SPLAT(0,HYSJ_CODES_FAMS)
  };
  HYSJ_MIRROR_ENUM(famtag,HYSJ_CODES_FAMS)

  #define HYSJ_LAMBDA(id)                       \
    using id##tag_t = constant_t<optag::id>;
  HYSJ_MAP_LAMBDA(HYSJ_CODES_OPS)
  #undef HYSJ_LAMBDA
  #define HYSJ_LAMBDA(id)                       \
    using id##tag_t = constant_t<reltag::id>;
  HYSJ_MAP_LAMBDA(HYSJ_CODES_RELS)
  #undef HYSJ_LAMBDA
  #define HYSJ_LAMBDA(id,...)                   \
    using id##tag_t = constant_t<famtag::id>;
  HYSJ_MAP_LAMBDA(HYSJ_CODES_FAMS)
  #undef HYSJ_LAMBDA

  #define HYSJ_LAMBDA(id,...)                   \
    constexpr id##tag_t id##tag{};
  HYSJ_MAP_LAMBDA(HYSJ_CODES_OPS)
  HYSJ_MAP_LAMBDA(HYSJ_CODES_RELS)
  HYSJ_MAP_LAMBDA(HYSJ_CODES_FAMS)
  #undef HYSJ_LAMBDA

  template<typename value>
  using optabs = grammars::symtabs<optag,value>;
  template<typename value>
  using reltabs = grammars::symtabs<reltag,value>;

  auto with_arg(auto &&f,id rel, id arg){
    return with_rel(
      [&](auto rel){
        return with_op(
          [&](auto arg){
            return fwd(f)(rel, arg);
          },
          arg);
      },
      rel);
  }

} //codes

namespace grammars{

  template<> inline constexpr bool is_obj<codes::optag> = true;
  template<> inline constexpr bool is_rel<codes::reltag> = true;
  template<> inline constexpr bool is_fam<codes::famtag> = true;

  template<codes::optag O>
  inline constexpr auto objgram_of<O> = []{

    using enum codes::optag;
    using enum codes::famtag;
    using enum codes::reltag;

    if constexpr(O == lit)
      return objgram{
        .tree = trees::tup(),
        .rels = kumi::tuple{},
        .args = kumi::tuple{}
      };
    if constexpr(O == typ)
      return objgram{
        .tree = trees::tup(trees::one,  trees::one),
        .rels = kumi::tuple{dom,        dom},
        .args = kumi::tuple{domexpr,    domexpr}
      };
    if constexpr(O == itr)
      return objgram{
        .tree = trees::tup(trees::one),
        .rels = kumi::tuple{dom},
        .args = kumi::tuple{domexpr}
      };
    if constexpr(O == any_of(con))
      return objgram{
        .tree = trees::tup(trees::one, trees::var(trees::one)),
        .rels = kumi::tuple{src , idx},
        .args = kumi::tuple{expr, itr}
      };
    if constexpr(O == cst)
      return objgram{
        .tree = trees::tup(trees::one),
        .rels = kumi::tuple{dom},
        .args = kumi::tuple{domexpr}
      };
    if constexpr(O == rfl)
      return objgram{
        .tree = trees::tup(trees::one),
        .rels = kumi::tuple{src },
        .args = kumi::tuple{expr}
      };
    if constexpr(O == cvt)
      return objgram{
        .tree = trees::tup(trees::one, trees::one),
        .rels = kumi::tuple{dom,     src },
        .args = kumi::tuple{typexpr, expr}
      };
    if constexpr(O == var)
      return objgram{
        .tree = trees::tup(trees::one,  trees::var(trees::one)),
        .rels = kumi::tuple{dom,        dim },
        .args = kumi::tuple{typexpr,    dimexpr}
      };
    if constexpr(O == rot)
      return objgram{
        .tree = trees::tup(trees::one, trees::var(trees::one)),
        .rels = kumi::tuple{src,  idx },
        .args = kumi::tuple{expr, expr}
      };
    if constexpr(O == any_of(neg, sin, fct, bnot, lnot, cur))
      return objgram{
        .tree = trees::tup(trees::one),
        .rels = kumi::tuple{src},
        .args = kumi::tuple{expr}
      };
    if constexpr(O == any_of(div, mod, pow, log, blsh, brsh, eq, lt))
      return objgram{
        .tree = trees::tup(trees::one, trees::one),
        .rels = kumi::tuple{src, src},
        .args = kumi::tuple{expr, expr}
      };
    if constexpr(O == any_of(add, mul, bor, band, bor, bxor, lor, land))
      return objgram{
        .tree = trees::tup(trees::var(trees::one)),
        .rels = kumi::tuple{src},
        .args = kumi::tuple{expr}
      };
    if constexpr(O == sub)
      return objgram{
        .tree = trees::tup(trees::one, trees::var(trees::one)),
        .rels = kumi::tuple{src,  src},
        .args = kumi::tuple{expr, expr}
      };
    if constexpr(O == der)
      return objgram{
        .tree = trees::tup(trees::one, trees::one, trees::var(trees::one)),
        .rels = kumi::tuple{src , src, src},
        .args = kumi::tuple{expr, expr, expr}
      };
    if constexpr(O == sel)
      return objgram{
        .tree = trees::tup(trees::one, trees::var(trees::one)),
        .rels = kumi::tuple{jmp,  alt},
        .args = kumi::tuple{expr, expr}
      };
    if constexpr(O == put)
      return objgram{
        .tree = trees::tup(trees::one, trees::one),
        .rels = kumi::tuple{dst , src},
        .args = kumi::tuple{expr, expr}
      };
    if constexpr(O == noop)
      return objgram{
        .tree = trees::tup(),
        .rels = kumi::tuple{},
        .args = kumi::tuple{}
      };
    if constexpr(O == loop)
      return objgram{
        .tree = trees::tup(trees::one),
        .rels = kumi::tuple{exe},
        .args = kumi::tuple{task}
      };
    if constexpr(O == exit)
      return objgram{
        .tree = trees::tup(trees::one),
        .rels = kumi::tuple{src},
        .args = kumi::tuple{expr}
      };
    if constexpr(O == fork)
      return objgram{
        .tree = trees::tup(trees::one, trees::var(trees::one)),
        .rels = kumi::tuple{src, exe},
        .args = kumi::tuple{expr, task}
      };
    if constexpr(O == any_of(then, when))
      return objgram{
        .tree = trees::tup(trees::var(trees::one)),
        .rels = kumi::tuple{exe},
        .args = kumi::tuple{task}
      };
    if constexpr(O == dev)
      return objgram{
        .tree = trees::tup(),
        .rels = kumi::tuple{},
        .args = kumi::tuple{}
      };
    if constexpr(O == on)
      return objgram{
        .tree = trees::tup(trees::one, trees::one),
        .rels = kumi::tuple{dst, exe},
        .args = kumi::tuple{dev, task}
      };
    if constexpr(O == to)
      return objgram{
        .tree = trees::tup(trees::one, trees::one, trees::one),
        .rels = kumi::tuple{src, dst,  src},
        .args = kumi::tuple{dev, dev, expr}
      };
    if constexpr(O == api)
      return objgram{
        .tree = trees::tup(trees::var(trees::one)),
        .rels = kumi::tuple{lnk},
        .args = kumi::tuple{decl}
      };

  }();

  template<codes::famtag F>
  inline constexpr auto famgram_of<F> = []{
    using enum codes::optag;
    using enum codes::famtag;

    if constexpr(F == domexpr)
      return famgram{
        .objs = std::array{
          lit, rfl, cst
        }
      };
    if constexpr(F == dimexpr)
      return famgram{
        .objs = std::array{
          itr, rfl
        }
      };
    if constexpr(F == typexpr)
      return famgram{
        .objs = std::array{
          typ, rfl
        }
      };
    if constexpr(F == expr)
      return famgram{
        .objs = std::array{
          cst ,itr ,con ,cvt ,var ,rot ,neg ,sin ,mod , fct,
          add ,sub ,mul ,pow ,log ,der ,div ,bnot,blsh, brsh,
          bor ,band,bxor,lnot,eq  ,lt  ,land,lor ,sel , rfl,
          cur
        }
      };
    if constexpr(F == task)
      return famgram{
        .objs = std::array{
          put,  noop, loop, exit,
          fork, then, when, on, to
        }
      };
    if constexpr(F == decl)
      return famgram{
        .objs = std::tuple{
          api, task
        }
      };
  }();

  template<codes::optag O>
  inline constexpr auto symdat_of<O> = []{
    using enum codes::optag;
    if constexpr(O == any_of(lit, cst))
      return type_c<constant>;
    else
      return type_c<void>;
  }();

  static_assert(!objvar<codes::optag::typ>);

} //grammars

namespace codes{

  #define HYSJ_LAMBDA(t,...)                    \
    using t##sym = grammars::sym<t##tag()>;
  HYSJ_MAP_LAMBDA(HYSJ_CODES_OPS)
  HYSJ_MAP_LAMBDA(HYSJ_CODES_RELS)
  #undef HYSJ_LAMBDA

  #define HYSJ_LAMBDA(t,...)                    \
    using t##fam = grammars::fam<famtag::t>;
  HYSJ_MAP_LAMBDA(HYSJ_CODES_FAMS)
  #undef HYSJ_LAMBDA

  #define HYSJ_LAMBDA(t,...)                                           \
    inline constexpr auto t##cast = bind<>(grammars::refcast,t##tag);
  HYSJ_MAP_LAMBDA(HYSJ_CODES_OPS)
  HYSJ_MAP_LAMBDA(HYSJ_CODES_RELS)
  HYSJ_MAP_LAMBDA(HYSJ_CODES_FAMS)
  #undef HYSJ_LAMBDA

  #define HYSJ_LAMBDA(t,...)                                \
    inline constexpr auto t = compose<>(autovalue, t##cast);
  HYSJ_MAP_LAMBDA(HYSJ_CODES_FAMS)
  #undef HYSJ_LAMBDA

  #define HYSJ_LAMBDA(t,...)                    \
    using t##dat = grammars::symdat_t<t##tag()>;
  HYSJ_MAP_LAMBDA(HYSJ_CODES_OPS)
  HYSJ_MAP_LAMBDA(HYSJ_CODES_RELS)
  #undef HYSJ_LAMBDA

  #define HYSJ_LAMBDA(t,...)                                       \
    inline constexpr auto is_##t = bind<>(grammars::is_famobj,t##tag);
  HYSJ_MAP_LAMBDA(HYSJ_CODES_FAMS)
  #undef HYSJ_LAMBDA

  namespace concepts{

    #define HYSJ_LAMBDA(fam,...)                                                     \
      template<typename T>                                                           \
      concept forward_##fam##_range = grammars::concepts::forward_fam<T,fam##tag()>; \
      template<typename T>                                                           \
      concept random_access_##fam##_range = grammars::concepts::random_access_fam<T,fam##tag()>;
    HYSJ_MAP_LAMBDA(HYSJ_CODES_FAMS)
    #undef HYSJ_LAMBDA

    #define HYSJ_LAMBDA(sym,...)                                                     \
      template<typename T>                                                           \
      concept forward_##sym##_range = grammars::concepts::forward_sym<T,sym##tag()>; \
      template<typename T>                                                           \
      concept random_access_##sym##_range = grammars::concepts::random_access_sym<T,sym##tag()>;
    HYSJ_MAP_LAMBDA(HYSJ_CODES_OPS)
    HYSJ_MAP_LAMBDA(HYSJ_CODES_RELS)
    #undef HYSJ_LAMBDA

  }

  using syntax = grammars::syntax<optag, reltag>;

  template<optag O, std::size_t J>
  inline constexpr auto reladd = grammars::reladd<optag, reltag, O, J>;

  template<optag O>
  inline constexpr auto objadd = grammars::objadd<optag, reltag, O>;

  inline namespace factories{
    #define HYSJ_LAMBDA(t, ...)                                         \
      inline constexpr auto t = grammars::objadd<optag, reltag, optag::t>;
    HYSJ_MAP_LAMBDA(HYSJ_CODES_OPS)
    #undef HYSJ_LAMBDA
  }

  #define HYSJ_LAMBDA(t, ...)                 \
    constexpr auto t##count(const syntax &p){ \
      return grammars::symcount(p, t##tag);   \
    }
  HYSJ_MAP_LAMBDA(HYSJ_CODES_OPS)
  HYSJ_MAP_LAMBDA(HYSJ_CODES_RELS)
  #undef HYSJ_LAMBDA

  #define HYSJ_LAMBDA(t, ...)                \
    constexpr auto t##syms(const syntax &p){ \
      return grammars::syms(p, t##tag);      \
    }
  HYSJ_MAP_LAMBDA(HYSJ_CODES_OPS)
  HYSJ_MAP_LAMBDA(HYSJ_CODES_RELS)
  #undef HYSJ_LAMBDA

  namespace concepts{

    template<typename T>
    concept suborder = requires(T &&t,optag sup,natural sub0,natural sub1){
      { fwd(t)(sup,sub0,sub1) } -> std::convertible_to<std::partial_ordering>;
    };
    template<typename T>
    concept order = requires(T &&t,id op0,id op1){
      { fwd(t)(op0,op1) } -> std::convertible_to<std::partial_ordering>;
    };

  }

  inline constexpr auto subcmp =
    [](concepts::suborder auto &&f,id op0,id op1) -> std::partial_ordering{
      if(op0[0] != op1[0])
        return op0[0] <=> op1[0];
      return fwd(f)(optag_of(op0),idx_of(op0),idx_of(op1));
    };
  inline constexpr auto cmp =
    [](concepts::order auto &&f,id op0,id op1) -> std::partial_ordering{
      return fwd(f)(op0,op1);
    };

  template<set D>
  constexpr auto csteval(constant_t<D> d,const syntax &s,id o){
    return with_op(
      [&](auto o){
        if constexpr(o.tag() == any_of(littag(), csttag()))
          return csteval(d,symdat(s,o));
        return no<cell<D>>;
      },
      o);
  }

  #define HYSJ_LAMBDA(i,p,...) constexpr auto p##csteval(const syntax &s,id o){ return csteval(i##_c,s,o); }
  HYSJ_MAP_LAMBDA(HYSJ_CODES_SETS)
  #undef HYSJ_LAMBDA

  constexpr std::optional<bool> is_zero(const syntax &c,id o){
    if(auto u = codes::cstcast(o))
      return is_zero(symdat(c, *u));
    return std::nullopt;
  }
  constexpr std::optional<bool> is_one(const syntax &c,id o){
    if(auto u = codes::cstcast(o))
      return is_one(symdat(c, *u));
    return std::nullopt;
  }
  constexpr std::optional<bool> is_negone(const syntax &c,id o){
    if(auto u = codes::cstcast(o))
      return is_negone(symdat(c, *u));
    return std::nullopt;
  }
  constexpr std::optional<bool> is_odd(const syntax &c,id o){
    if(auto u = codes::cstcast(o))
      return is_odd(symdat(c, *u));
    return std::nullopt;
  }

  template<optag O>
  constexpr std::optional<bool> is_identity(constant_t<O>,const syntax &c,id o){
    using enum optag;
    if constexpr(O == any_of(add,sub))
      return is_zero(c,o);
    else{
      static_assert(O == mul);
      return is_one(c,o);
    }
  }

  constexpr bool has_itrop(id i){
    return optag_of(i) == optag::itr;
  }
  constexpr std::optional<id> itrop(id i){
    if(has_itrop(i))
      return i;
    return std::nullopt;
  }

  constexpr auto itrdim(const syntax &c,id i){
    return with_op(
      [&](auto i) -> cstsym {
        if constexpr(i.tag() == optag::cst)
          return i;
        else if constexpr(i.tag() == any_of(optag::itr, optag::rfl))
          return itrdim(c, arg(c, i, 0_N));
        else{
          HYSJ_UNREACHABLE;
          return {};
        }
      },
      i);
  }
  constexpr auto itrext(const syntax &c,id i){
    return *icsteval(c,itrdim(c,i));
  }

} //codes

} //hysj
HYSJ_ENUM_FORMATTER(hysj::_HYSJ_VERSION_NAMESPACE::codes::optag);
HYSJ_ENUM_FORMATTER(hysj::_HYSJ_VERSION_NAMESPACE::codes::reltag);
HYSJ_ENUM_FORMATTER(hysj::_HYSJ_VERSION_NAMESPACE::codes::famtag);
#include<hysj/tools/epilogue.hpp>
