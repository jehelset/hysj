#include<hysj/binaries/gcc.hpp>

#include<algorithm>
#include<deque>
#include<memory>
#include<optional>
#include<ranges>
#include<stdexcept>
#include<vector>

#include<fmt/core.h>
#include<libgccjit++.h>

#include<hysj/codes/access.hpp>
#include<hysj/codes/cells.hpp>
#include<hysj/codes/eval.hpp>
#include<hysj/codes/code.hpp>
#include<hysj/codes/to_string.hpp>
#include<hysj/codes/compile.hpp>
#include<hysj/codes/compile/contractions.hpp>
#include<hysj/codes/compile/implicit_iterators.hpp>
#include<hysj/codes/compile/rotations.hpp>
#include<hysj/codes/factories.hpp>
#include<hysj/codes/grammar.hpp>
#include<hysj/codes/loops.hpp>
#include<hysj/codes/sets.hpp>
#include<hysj/codes/threads.hpp>
#include<hysj/tools/ranges/keep.hpp>
#include<hysj/tools/ranges/map.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/functional/apply.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::binaries::gcc{

constexpr natural bwidthmin = 8, swidthmin = 8;

constexpr auto btype(codes::type t){
  if(t.set == set::booleans)
    return codes::type{
      set::naturals,
      std::max<natural>(t.width, bwidthmin)
    };
  return t;
}
constexpr auto etype(codes::type t){
  if(t.set == set::booleans)
    return codes::type{t.set, 1};
  return codes::type{
    t.set,
    std::max<natural>(t.width, swidthmin)
  };
}
constexpr auto obtype(const code &c,id o){
  return btype(otype(c, o));
}
constexpr auto obsize(const code &c,id o){
  return oext(c, o) * obtype(c, o).width / 8;
}
constexpr auto oetype(const code &c,id o){
  return etype(otype(c, o));
}
constexpr auto retype(const code &c,id o){
  return etype(rtype(c, o));
}

template<codes::optag O>
std::string to_gcc_name(sym<O> o){
  return fmt::format("{}_{}", O, o.idx);
}
std::string to_gcc_name(grammars::id o){
  return codes::with_op(lift((gcc::to_gcc_name)), o);
}

constexpr std::optional<gcc_jit_types> to_gcc_type(codes::type t){
  switch(t.set){
  case set::booleans:
    switch(t.width){
    case 1: return GCC_JIT_TYPE_BOOL;
    }
    [[fallthrough]];
  case set::naturals:
    switch(t.width){
    case 8: return GCC_JIT_TYPE_UINT8_T;
    case 16: return GCC_JIT_TYPE_UINT16_T;
    case 32: return GCC_JIT_TYPE_UINT32_T;
    case 64: return GCC_JIT_TYPE_UINT64_T;
    case 128: return GCC_JIT_TYPE_UINT128_T;
    }
    break;
  case set::integers:
    switch(t.width){
    case 8: return GCC_JIT_TYPE_INT8_T;
    case 16: return GCC_JIT_TYPE_INT16_T;
    case 32: return GCC_JIT_TYPE_INT32_T;
    case 64: return GCC_JIT_TYPE_INT64_T;
    case 128: return GCC_JIT_TYPE_INT128_T;
    }
    break;
  case set::reals:
    switch(t.width){
    case 32: return GCC_JIT_TYPE_FLOAT;
    case 64: return GCC_JIT_TYPE_DOUBLE;
    case 128: return GCC_JIT_TYPE_LONG_DOUBLE;
    }
    break;
  }
  return std::nullopt;
}
constexpr auto to_gcc_param_name(codes::type t){
  return fmt::format(
    "{}{}",
    [&] -> std::optional<char> {
      switch(t.set){
      case set::booleans: return 'b';
      case set::naturals: return 'n';
      case set::integers: return 'i';
      case set::reals: return 'r';
      default: return std::nullopt;
      }
    }().value(),
    t.width);
}


struct bin::image_{
  gcc_jit_result *result;
  ~image_(){
    gcc_jit_result_release(result);
  }
};


bin::bin() = default;
bin::bin(bin &&) = default;
bin &bin::operator=(bin &&) = default;
bin::~bin() = default;

auto ccctx(const config &cfg, codes::taskfam task){
  using context = std::unique_ptr<gccjit::context, decltype([](gccjit::context *c){  delete c; })>;
  context c(new gccjit::context(gccjit::context::acquire()));
  c->set_int_option(GCC_JIT_INT_OPTION_OPTIMIZATION_LEVEL, static_cast<int>(cfg.optimization_level));
  c->set_str_option(gcc_jit_str_option::GCC_JIT_STR_OPTION_PROGNAME, to_gcc_name(task).c_str());
  c->add_command_line_option("-fno-exceptions");
  c->add_command_line_option("-fno-asynchronous-unwind-tables");
  if(cfg.arch)
    c->add_command_line_option(fmt::format("-march={}", *cfg.arch).c_str());
  if(cfg.c_path)
    c->set_bool_option(GCC_JIT_BOOL_OPTION_DEBUGINFO, true);
  return c;
}

auto ccthreads(const config &cfg,code &code,codes::exprfam thread_id,codes::taskfam task){
  auto threads = codes::thread_info::make(code, cfg.thread_count);
  thread(code, threads, task);
  task = thread_fork(code, threads, thread_id, task).value_or(task);
  return kumi::make_tuple(std::move(threads), task);
}

struct ccbin{
  const gcc::config &cfg;
  gcc::bin &bin;
  gccjit::context &context;
};

const codes::type thread_type{.set = set::integers, .width = 32};

struct cctypes{
  ccbin &bin;

  std::map<codes::type, gccjit::type> scalars;
  std::map<kumi::tuple<codes::type, integer>, gccjit::type> arrays;

  gccjit::type scalar(codes::type t){
    auto it = scalars.find(t);
    if(it != scalars.end())
      return it->second;
    auto t_gcc = bin.context.get_type(to_gcc_type(t).value());
    return scalars.emplace(t, t_gcc).first->second;
  }

  gccjit::type void_ = bin.context.get_type(GCC_JIT_TYPE_VOID),
    bool_ = bin.context.get_type(GCC_JIT_TYPE_BOOL),
    index = bin.context.get_type(GCC_JIT_TYPE_SIZE_T),
    thread = this->scalar(gcc::thread_type);

  auto pointer(codes::type t){
    return scalar(t).get_pointer();
  }
  auto array(codes::type t, integer n, std::optional<std::size_t> a = {}){
    HYSJ_ASSERT(n >= 1);
    auto it = arrays.find(kumi::make_tuple(t, n));
    if(it != arrays.end())
      return it->second;
    auto t_gcc = scalar(t);
    auto T_gcc = bin.context.new_array_type(t_gcc, n);
    if(a)
      T_gcc = T_gcc.get_aligned(*a);
    return arrays.emplace(kumi::make_tuple(t, n), T_gcc).first->second;
  }

  gccjit::rvalue cast(codes::type target_type, codes::type source_type, gccjit::rvalue source_gcc){
    if(target_type == source_type)
      return source_gcc;
    else if(source_type.set == set::booleans && source_type.width == 1
            && target_type.set == set::reals){
      codes::type temp_type{
        .set = set::naturals,
        .width = 32
      };
      return cast(target_type, temp_type, cast(temp_type, source_type, source_gcc));
    }
    auto target_type_gcc = scalar(target_type);
    return bin.context.new_cast(source_gcc, target_type_gcc);
  }

  gccjit::rvalue rvalue(codes::type t, auto v){
    return bin.context.new_rvalue(scalar(t), v);
  }
};

template<codes::optag O>
requires (O == any_of(codes::optag::lit, codes::optag::cst))
auto cclit(cctypes &types,sym<O> o){
  auto t = oetype(types.bin.bin.code, o);
  return codes::with_type(
    [&](auto s,auto w){
      auto v = codes::csteval(s, types.bin.bin.code, o).value();
      if constexpr(s == set::booleans && w == 1)
        return just(types.rvalue(t, v ? 1 : 0));
      else if constexpr(s == any_of(set::booleans, set::naturals) && codes::has_cell<set::integers, w()>)
        return just(types.rvalue(t, static_cast<cell<set::integers, w()>>(v)));
      else if constexpr(s == set::integers && codes::has_cell<s(), w()>)
        return just(types.rvalue(t, static_cast<cell<s(), w()>>(v)));
      else if constexpr(s == set::reals && codes::has_cell<s(), w()>)
        return just(types.rvalue(t, static_cast<cell<s(), 64>>(v)));
      else
        return no<gccjit::rvalue>;
    },
    t);
}

struct ccmem{
  ccbin &bin;
  cctypes &types;

  std::array<gccjit::param, gcc::types.size()> buffers = apply(
    [&](auto... type){
      auto buffer = [&](auto t){
        auto s = types.scalar(t);
        if(bin.cfg.alignment)
          s = s.get_aligned(*bin.cfg.alignment);
        auto p = s.get_pointer();
        return bin.context.new_param(p, to_gcc_param_name(t));
      };
      return std::to_array({buffer(type)...});
    },
    gcc::types);
  std::map<codes::exprfam, gccjit::lvalue> statics;

  auto buffer(codes::type t)const{
    return buffers.at(gcc::typeindex(t).value());
  }
  auto offset(codes::exprfam e)const{
    return bin.context.new_rvalue(types.index, bin.bin.layout[e].offset);
  }
  auto pointer(codes::exprfam access)const{
    auto buf = codes::expr(accessed(bin.bin.code, access).value_or(access));
    if(!bin.bin.layout.contains(buf))
      return no<gccjit::rvalue>;
    auto ptr = buffer(otype(bin.bin.code, buf));
    auto off = offset(buf);
    auto ref = bin.context.new_array_access(ptr, off);
    return just(ref.get_address());
  }
  gccjit::rvalue local_pointer(codes::exprfam access){
    auto buf = codes::expr(accessed(bin.bin.code, access).value_or(access));
    if(auto it = statics.find(buf); it != statics.end())
      return it->second;
    auto type = types.array(obtype(bin.bin.code, buf),
                            oext(bin.bin.code, buf),
                            bin.cfg.alignment);
    auto global = bin.context.new_global(GCC_JIT_GLOBAL_INTERNAL, type,
                                         fmt::format("{}_buffer", to_gcc_name(buf)));
    statics.emplace(buf, global);
    return global;
  }
};

struct ccscope{
  gccjit::block header, merge;
  std::map<codes::exprfam, gccjit::rvalue> exprs;
};

struct ccmain{
  ccbin &bin;
  cctypes &types;
  ccmem &mem;

  static constexpr auto name = "main";

  gccjit::param thread = bin.context.new_param(types.thread, "thread");

  gccjit::function main = [&]{
    auto params = ranges::vec(views::prepend(thread, mem.buffers));
    return bin.context.new_function(
      GCC_JIT_FUNCTION_EXPORTED,
      types.void_,
      name,
      params,
      0); //not_variadic
  }();

  std::deque<gccjit::block> blocks;
  std::deque<ccscope> scopes;
  
  std::map<codes::exprfam, kumi::tuple<bool, gccjit::lvalue>> vars;
  std::map<id, std::size_t> name_counts;

  auto &scope(integer index = 0){
    if(index < 0) //-x -> x - 1
      index = -(index + 1);
    else //x -> n - max(x, 1)
      index = scopes.size() - std::max(index, 1_i);
    return scopes.at(index);
  }
  void push_scope(gccjit::block header = {}, gccjit::block merge = {}){
    scopes.push_back(ccscope{header, merge});
  }
  void pop_scope(){
    scopes.pop_back();
  }

  void unlet(codes::exprfam o){
    for(auto &scope:std::views::reverse(scopes))
      if(scope.exprs.erase(o))
        return;
  }
  void let(codes::exprfam o,gccjit::rvalue e){
    scope().exprs[o] = std::move(e);
  }
  auto let(codes::exprfam o){
    for(auto &scope:std::views::reverse(scopes))
      if(auto it = scope.exprs.find(o); it != scope.exprs.end())
        return just(it->second);
    return no<gccjit::rvalue>;
  }

  auto unique_name(grammars::id i){
    return fmt::format("{}_{}", to_gcc_name(i), name_counts[i]++);
  }

  auto unique_local(gccjit::type type, std::string name){
    return main.new_local(type, name);
  }
  auto unique_local(codes::type type, std::string name){
    return unique_local(types.scalar(type), std::move(name));
  }
  auto ensure_local_array(codes::exprfam v){
    if(auto it = vars.find(codes::expr(v));it != vars.end())
      return get<1>(it->second);
    return get<1>(
      vars.emplace(codes::expr(v),
                   kumi::make_tuple(true,
                                    main.new_local(types.array(oetype(bin.bin.code, v), oext(bin.bin.code, v)),
                                                   unique_name(v)))).first->second);
  }
  auto ensure_local(codes::exprfam v){
    if(auto it = vars.find(codes::expr(v));it != vars.end())
      return get<1>(it->second);
    return get<1>(
      vars.emplace(codes::expr(v),
                   kumi::make_tuple(false,
                                    main.new_local(types.scalar(oetype(bin.bin.code, v)),
                                                   unique_name(v)))).first->second);
  }

  auto pointer(codes::exprfam o) -> std::optional<gccjit::rvalue>{
    auto it = vars.find(codes::expr(accessed(bin.bin.code, o).value_or(o.id())));
    if(it == vars.end())
      return no<gccjit::lvalue>;
    auto [array, lvalue] = it->second;
    if(array)
      return just(lvalue);
    return just(lvalue.get_address());
  }

  template<std::size_t N>
  auto top_blocks(){
    HYSJ_ASSERT(blocks.size() >= N);
    std::array<gccjit::block, N> top;
    std::ranges::copy(std::views::drop(blocks, blocks.size() - N), top.data());
    return top;
  }
  template<std::size_t N>
  auto push_blocks(std::array<std::string, N> B){
    for(auto b:B)
      blocks.push_back(main.new_block(b));
    return top_blocks<N>();
  }
  template<std::size_t N>
  auto pop_blocks(){
    HYSJ_ASSERT(blocks.size() >= N);
    auto pop = top_blocks<N>();
    blocks.resize(blocks.size() - N);
    return pop;
  }

  auto top_block(){
    return top_blocks<1>()[0];
  }
  auto push_block(std::string n){
    return push_blocks(std::array{std::move(n)})[0];
  }
  auto pop_block(){
    return pop_blocks<1>()[0];
  }

  static auto get_code(gcc_jit_result *result){
    return (gcc::kernel)gcc_jit_result_get_code(result, name);
  }
};

std::optional<gccjit::rvalue> ccexpr(ccmain &main,codes::exprfam expr);
void ccstmt(ccmain &main,codes::taskfam expr);

auto ccrelcast(ccmain &main,grammars::id rel){
  auto &code = main.bin.bin.code;
  auto arg = codes::expr(grammars::relargid(code, rel));
  return main.types.cast(retype(code, rel), oetype(code, arg), main.let(arg).value());
}
template<codes::optag O,std::size_t I>
auto ccrel(ccmain &main,sym<O> o, natural_t<I> relidx,auto rel){
  if constexpr(grammars::objvararg<O, I>)
    return views::map(
      rel, [&](auto rel){
        return ccrelcast(main, rel);
      });
  else
    return ccrelcast(main, rel);
}
template<codes::optag O,std::size_t I>
auto ccrel(ccmain &main,sym<O> o, constant_t<I> relidx){
  return ccrel(main, o, relidx, grammars::rel(main.bin.bin.code, o, relidx));
}
template<codes::optag O>
auto ccrels(ccmain &main,sym<O> o){
  return with_integer_sequence(
    constant_c<grammars::objnum<O>>,
    [&](auto... relidx){
      auto rels = grammars::rels(main.bin.bin.code, o);
      return kumi::make_tuple(ccrel(main, o, relidx, kumi::get<relidx>(rels))...);
    });
}

auto ccident(ccmain &main,codes::type target_type,codes::exprfam source){
  auto &code = main.bin.bin.code;
  auto init = codes::builtins::identity(code.builtins.lits, source, target_type.width).value();
  compile(code, init);
  auto init_type = otype(code, init);
  return main.types.cast(target_type, init_type, ccexpr(main, {init}).value());
}

std::optional<gccjit::lvalue> ccref(ccmain &main,codes::exprfam lhs){
  return with_famobj(
    [&](auto lhs){
      auto &code = main.bin.bin.code;
      if constexpr(codes::has_accessed(lhs.tag())){
        auto ptr = main.mem.pointer({lhs}).or_else(
          [&]{
            return main.pointer({lhs});
          }).or_else(
          [&]{
            return just(main.mem.local_pointer({lhs}));
          }).value();
        auto index = ccexpr(main, codes::expr(index_of(code, lhs.id()))).value();
        return just(main.bin.context.new_array_access(ptr, index));
      }
      else if constexpr(lhs.tag() == codes::optag::cur)
        return ccref(main, arg(code, lhs, 0_N));
      else if constexpr(lhs.tag() == codes::optag::itr)
        return just(main.ensure_local({lhs}));
      else
        return no<gccjit::lvalue>;
    },
    lhs);
}
std::optional<gccjit::rvalue> ccload(ccmain &main,codes::exprfam lhs){
  return ccref(main, lhs).transform(
    [&](auto lval){
      auto &code = main.bin.bin.code;
      auto ltype = obtype(code, lhs);
      auto rtype = oetype(code, lhs);
      return main.types.cast(rtype, ltype, lval);
    });
}
void ccstore(ccmain &main,codes::exprfam lhs,gccjit::rvalue rhs){
  auto &code = main.bin.bin.code;
  auto lval = ccref(main, lhs).value();
  auto rval = main.types.cast(obtype(code, lhs), oetype(code, lhs), rhs);
  main.top_block().add_assignment(lval, rval);
}

namespace concepts{
  template<typename T>
  concept sized_forward_blocks = std::ranges::forward_range<T> &&
    std::ranges::sized_range<T> &&
    std::convertible_to<std::ranges::range_value_t<T>, gccjit::block>;
}

auto ccswitch_header(ccmain &main, std::string_view name, codes::type which_type, codes::exprfam which, integer num_blocks){

  HYSJ_ASSERT(num_blocks > 0);
  auto &context = main.bin.context;
  auto &code = main.bin.bin.code;
  {
    auto current = main.pop_block();
    auto [merge, header] = main.push_blocks(std::array{
        fmt::format("{}_merge", name),
        fmt::format("{}_header", name)
      });
    current.end_with_jump(header);
  }

  auto which_source_type = oetype(code, which);
  auto which_gcc = main.types.cast(which_type, which_source_type, ccexpr(main, which).value());

  auto blocks_gcc = ranges::vec(views::indices(num_blocks), [&](auto i){
    return main.main.new_block(fmt::format("{}_block_{}", name, i));
  });
  auto cases_gcc = ranges::vec(
    views::indices(0_i, num_blocks - 1_i),
    [&](auto index){
      auto index_gcc = main.types.rvalue(which_type, index);
      return context.new_case(index_gcc, index_gcc, blocks_gcc[index]);
    });
  {
    auto header = main.pop_block();
    header.end_with_switch(which_gcc, blocks_gcc.back(), cases_gcc);
  }
  return blocks_gcc;
}
void ccswitch_merge(ccmain &main, concepts::sized_forward_blocks auto &&blocks_gcc){
  auto merge = main.top_block();
  for(auto &block_gcc:blocks_gcc)
    block_gcc.end_with_jump(merge);
}

namespace concepts{
  template<typename T>
  concept random_access_rvalues = std::ranges::random_access_range<T> &&
    std::convertible_to<std::ranges::range_value_t<T>, gccjit::rvalue>;
}


constexpr auto can_fold = any_of(codes::optag::add,  codes::optag::mul,
                                 codes::optag::band, codes::optag::bor,  codes::optag::bxor,
                                 codes::optag::land, codes::optag::lor);
template<codes::optag E>
requires (can_fold(E))
std::optional<gccjit::rvalue> ccfold(ccmain &main,sym<E> e,concepts::random_access_rvalues auto &&oprns){

  auto &code = main.bin.bin.code;
  auto &context = main.bin.context;

  compile(code, e);

  auto t = oetype(code, e);
  auto t_gcc = main.types.scalar(t);

  if(std::ranges::empty(oprns)){
    if constexpr(E == any_of(codes::optag::add, codes::optag::bor, codes::optag::bxor, codes::optag::lor))
      return t_gcc.zero();
    else if constexpr(E == any_of(codes::optag::mul, codes::optag::land))
      return t_gcc.one();
    else{
      static_assert(E == codes::optag::band);
      return ~t_gcc.zero();
    }
  }
  return just(
    std::ranges::fold_left(
      std::views::drop(oprns, 1),
      ranges::first(oprns),
      [&](auto cur,auto rhs){
        if constexpr(E == codes::optag::add)
          return cur + rhs;
        else if constexpr(E == codes::optag::mul)
          return cur * rhs;
        else if constexpr(E == codes::optag::band)
          return cur & rhs;
        else if constexpr(E == codes::optag::bor)
          return cur | rhs;
        else if constexpr(E == codes::optag::bxor)
          return cur ^ rhs;
        else if constexpr(E == codes::optag::land)
          return cur && rhs;
        else{
          static_assert(E == codes::optag::lor);
          return cur || rhs;
        }
      }));

}
std::optional<gccjit::rvalue> ccfold(ccmain &main,codes::exprfam e,concepts::random_access_rvalues auto &&oprns){
  return with_famobj(
    [&](auto e){
      if constexpr(can_fold(e.tag))
        return ccfold(main, e, fwd(oprns));
      return no<gccjit::rvalue>;
    },
    e);
}


std::optional<gccjit::rvalue> ccexpr(ccmain &main,codes::exprfam expr){
  auto &code = main.bin.bin.code;
  auto &context = main.bin.context;
  compile(code, expr);

  auto pre0 = [&](grammars::id o) -> bool{
    auto e = codes::expr(o);
    if(main.let(e))
      return false;
    if(is_access(code, e)){
      main.let(e, ccload(main, {e}).value());
      return false;
    }
    auto v = with_famobj(
      [&](auto e) -> std::optional<gccjit::rvalue> {
        static constexpr auto E = e.tag;
        if constexpr(E == any_of(codes::optag::lit, codes::optag::cst))
          return just(cclit(main.types, e).value());
        if constexpr(E == codes::optag::itr)
          return just(ccload(main, {e}).value());
        if constexpr(E == codes::optag::con){
          auto [u, C] = args(code, e);
          auto u_var = main.ensure_local({e});
          auto t = otype(code, e);
          main.top_block().add_assignment(u_var, ccident(main, t, u));
          ccloop_header(main, main.unique_name(e), codes::make_inner_loop(code, e));
        }
        if constexpr(E == codes::optag::sel){
          auto [w, C] = args(code, e);
          auto e_var = main.ensure_local({e});
          auto w_type = rtype(code, e, 0_N);
          auto name = main.unique_name(e);
          auto B_gcc = ccswitch_header(main, name, w_type, w, std::ranges::ssize(C));
          auto R = rel(code, e, 1_N);
          for(auto &&[b_gcc, c, r]:std::views::zip(B_gcc, C, R)){
            main.blocks.push_back(b_gcc);
            auto c_gcc = ccexpr(main, c).value();
            auto r_gcc = ccrelcast(main, r);
            main.top_block().add_assignment(e_var, r_gcc);
            b_gcc = main.pop_block();
          }
          ccswitch_merge(main, B_gcc);
          return e_var;
        }
        return no<gccjit::rvalue>;
      },
      e);
    if(v){
      main.let(e, *v);
      return false;
    }
    return true;
  };
  auto pre1 = [&](grammars::id,grammars::id rel,grammars::id arg){
    if(codes::reltag_of(rel) == any_of(codes::reltag::dom, codes::reltag::dim))
      return false;
    return pre0(arg);
  };
  auto post = [&](auto... ev){
    auto e = codes::expr(pick<-1>(ev...));
    auto t = oetype(code, e);
    auto t_gcc = main.types.scalar(t);
    auto v = grammars::with_famobj(
      [&](auto e) -> std::optional<gccjit::rvalue> {
        static constexpr auto E = e.tag;
        if constexpr(E == any_of(codes::optag::cur,
                                 codes::optag::neg,  codes::optag::sin, codes::optag::fct,
                                 codes::optag::lnot, codes::optag::bnot))
          return just(
            [&]{
              auto oprn = ccrel(main, e, 0_N);
              if constexpr(E == codes::optag::cur)
                return oprn;
              else if constexpr(E == codes::optag::neg)
                return -oprn;
              else if constexpr(E == codes::optag::sin){
                HYSJ_ASSERT(t.set == set::reals);
                auto builtin_suffix = [&]{
                  if(t.width == 32)
                    return "f";
                  else{
                    HYSJ_ASSERT(t.width == 64);
                    return "";
                  }
                }();
                auto builtin = context.get_builtin_function(fmt::format("__builtin_sin{}", builtin_suffix));
                return builtin(oprn);
              }
              else if constexpr(E == codes::optag::fct){
                HYSJ_ASSERT(codes::is_integral(t.set));

                auto accumulator = main.ensure_local({e});
                auto accumulator_type = main.types.scalar(t);
                auto name = main.unique_name(e);
                auto counter = main.unique_local(t, fmt::format("{}_counter", name));
                auto counter_type = accumulator_type;
                auto current = main.pop_block();
                current.add_assignment(accumulator, accumulator_type.one());
                current.add_assignment(counter, oprn);
                auto [merge, header, body] = main.push_blocks(std::array{
                    fmt::format("{}_merge", name),
                    fmt::format("{}_header", name),
                    fmt::format("{}_body", name)
                  });
                current.end_with_jump(header);
                header.end_with_conditional(counter != counter_type.zero(), body, merge);
                body.add_assignment(accumulator, accumulator * counter);
                body.add_assignment(counter, counter - counter_type.one());
                body.end_with_jump(header);
                main.pop_blocks<2>();
                return accumulator;
              }
              else if constexpr(E == codes::optag::lnot)
                return !oprn;
              else{
                static_assert(E == codes::optag::bnot);
                return ~oprn;
              }
            }());
        if constexpr(E == codes::optag::cvt)
          return just(ccrel(main, e, 1_N));
        if constexpr(E == any_of(codes::optag::sub, codes::optag::div,  codes::optag::mod, codes::optag::log,
                                 codes::optag::pow, codes::optag::blsh, codes::optag::brsh, codes::optag::lt,
                                 codes::optag::eq)){
          auto [lhs, rhs] = ccrels(main, e);
          if constexpr(E == codes::optag::sub)
            return just(std::ranges::fold_left(rhs, lhs, [&](auto cur, auto rhs){
              return cur - rhs;
            }));
          else if constexpr(E == codes::optag::div)
            return just(lhs / rhs);
          else if constexpr(E == codes::optag::mod){
            if(codes::is_integral(t.set))
              return just(((lhs % rhs) + rhs) % rhs);
            else{
              HYSJ_ASSERT(t.set == set::reals);
              auto fn_suffix = [&]{
                if(t.width == 32)
                  return "f";
                else{
                  HYSJ_ASSERT(t.width == 64);
                  return "";
                }
              }();
              auto fn = context.get_builtin_function(fmt::format("__builtin_fmod{}",fn_suffix));
              return just(fn(fn(lhs, rhs) + rhs, rhs));
            }
          }
          else if constexpr(E == codes::optag::log){
            HYSJ_ASSERT(t.set == set::reals);
            auto fn_suffix = [&]{
              if(t.width == 32)
                return "f";
              else{
                HYSJ_ASSERT(t.width == 64);
                return "";
              }
            }();
            auto fn = context.get_builtin_function(fmt::format("__builtin_log{}",fn_suffix));
            return just(fn(rhs) / fn(lhs));
          }
          else if constexpr(E == codes::optag::pow){
            auto t_rhs = rtype(code, rel(code, e, 1_N));
            if(codes::is_integral(t_rhs.set)){
              auto counter_type = main.types.scalar(t_rhs);
              auto name = main.unique_name(e);
              auto counter = main.unique_local(t_rhs, fmt::format("{}_counter", name));
              auto accumulator = main.ensure_local({e});
              auto accumulator_type = main.types.scalar(t);
              auto current = main.pop_block();
              current.add_assignment(accumulator, accumulator_type.one());
              current.add_assignment(counter, counter_type.zero());
              auto [merge, header, body] = main.push_blocks(std::array{
                  fmt::format("{}_merge", name),
                  fmt::format("{}_header", name),
                  fmt::format("{}_body", name)
                });
              current.end_with_jump(header);
              header.end_with_conditional(counter < rhs, body, merge);
              body.add_assignment(accumulator, accumulator * lhs);
              body.add_assignment(counter, counter + counter_type.one());
              body.end_with_jump(header);
              main.pop_blocks<2>();
              return just(accumulator);
            }
            else{
              HYSJ_ASSERT(t_rhs.set == set::reals);
              auto builtin_suffix = [&]{
                if(t.width == 32)
                  return "f";
                else{
                  HYSJ_ASSERT(t.width == 64);
                  return "";
                }
              }();
              auto builtin = context.get_builtin_function(fmt::format("__builtin_pow{}",builtin_suffix));
              return just(builtin(lhs, rhs));
            }
          }
          else if constexpr(E == codes::optag::blsh)
            return just(context.new_binary_op(gcc_jit_binary_op::GCC_JIT_BINARY_OP_LSHIFT, t_gcc, lhs, rhs));
          else if constexpr(E == codes::optag::brsh)
            return just(context.new_binary_op(gcc_jit_binary_op::GCC_JIT_BINARY_OP_RSHIFT, t_gcc, lhs, rhs));
          else if constexpr(E == codes::optag::lt)
            return just(lhs < rhs);
          else{
            static_assert(E == codes::optag::eq);
            return just(lhs == rhs);
          }
        }
        if constexpr(can_fold(E))
          return ccfold(main, e, ccrel(main, e, 0_N));
        else if constexpr(E == codes::optag::con){
          auto u = arg(code, e, 0_N);
          auto u_var = *(main.pointer({e}).value());
          auto oprns = std::to_array<gccjit::rvalue>({
              u_var, main.let(u).value()
            });
          main.top_block().add_assignment(u_var, ccfold(main, u, oprns).value());
          ccloop_merge(main, codes::make_inner_loop(code, e));
          return just(u_var);
        }
        return no<gccjit::rvalue>;
      },
      e).value();
    main.let(e, v);
    return none{};
  };
  grammars::walk<none>(code, overload(pre0, pre1), post, expr);
  return main.let(expr);
}

void ccloop_header(ccmain &main,std::string name,codes::concepts::forward_loop_iterators auto &&iterators){
  auto &code = main.bin.bin.code;
  for(const auto &i:iterators)
    main.ensure_local({i.sym});
  for(const auto &iterator:iterators){
    ccstmt(main, iterator.init);
    auto itr_name = to_gcc_name(iterator.sym);
    {
      auto current = main.pop_block();
      auto [merge, header] = main.push_blocks(std::array{
          fmt::format("{}_{}_merge", name, itr_name),
          fmt::format("{}_{}_header", name, itr_name)
        });
      current.end_with_jump(header);
    }
    auto test = ccexpr(main, iterator.test).value();
    {
      auto [merge, header] = main.top_blocks<2>();
      auto [body] = main.push_blocks(std::array{fmt::format("{}_{}_body", name, itr_name)});
      header.end_with_conditional(test, body, merge);
    }
    ccstmt(main, iterator.head);
  }
}
void ccloop_merge(ccmain &main,codes::concepts::forward_loop_iterators auto &&iterators){
  for(const auto &iterator:std::views::reverse(iterators)){
    ccstmt(main, iterator.tail);
    auto [merge, header, body] = main.top_blocks<3>();
    body.end_with_jump(header);
    main.pop_blocks<2>();
  }
}
void ccput(ccmain &main,codes::putsym put, codes::concepts::forward_loop_iterators auto &&iters){
  auto &code = main.bin.bin.code;

  auto body = [&]{
    auto [lhs, rhs] = args(code, put);
    ccexpr(main, rhs);
    ccstore(main, lhs, ccrel(main, put, 1_N));
  };

  compile(code, put);
  if(std::ranges::empty(iters))
    body();
  else{
    ccloop_header(main, main.unique_name(put), iters);
    body();
    ccloop_merge(main, iters);
  }
}
void ccput(ccmain &main,codes::putsym put){
  return ccput(main, put, views::no<codes::loop_iterator>);;
}

void ccstmt(ccmain &main,codes::taskfam stmt){
  auto &code = main.bin.bin.code;
  compile(code, stmt);

  auto pre0 = [&](grammars::id o){
    return with_famobj(
      [&](auto o){
        if constexpr(o.tag == codes::optag::noop)
          return false;
        if constexpr(o.tag == codes::optag::put){
          auto L = make_basic_loop(code, o);
          ccput(main, o, L);
          return false;
        }
        return true;
      },
      codes::task(o));
  };
  auto pre1 = [&](grammars::id op,grammars::id rel,grammars::id arg){
    if(codes::reltag_of(rel) != codes::reltag::exe)
      return false;
    return pre0(arg);
  };
  grammars::walk<none>(code, overload(pre0, pre1), never, stmt);
}

void cclatch(ccmain &main,integer thread_count,std::string_view prefix){

  auto &context = main.bin.context;
  if(thread_count < 2)
    return;

  auto latch_type = main.types.thread;
  auto latch_global = context.new_global(GCC_JIT_GLOBAL_INTERNAL, latch_type, fmt::format("{}_latch_global", prefix));
  latch_global.set_initializer_rvalue(latch_type.zero());

  auto latch_global_ptr = latch_global.get_address();
  auto void_ = main.types.void_;
  auto latch_global_mutable_ptr = gccjit::rvalue(
    gcc_jit_context_new_bitcast(
      context.get_inner_context(),
      nullptr,
      latch_global_ptr.get_inner_rvalue(),
      void_.get_volatile().get_pointer().get_inner_type()));

  auto latch_local = main.unique_local(thread_type, fmt::format("{}_latch_local", prefix));

  auto latch_count = context.new_rvalue(latch_type, thread_count);

  auto latch_width = thread_type.width / 8;

  auto memorder = context.new_rvalue(latch_type, __ATOMIC_SEQ_CST);
  auto add_fetch = context.get_builtin_function(fmt::format("__atomic_add_fetch_{}", latch_width));
  auto compare_exchange = context.get_builtin_function(fmt::format("__atomic_compare_exchange_{}", latch_width));

  auto current = main.pop_block();
  auto [merge, header] = main.push_blocks<2>({
      fmt::format("{}_latch_merge", prefix),
      fmt::format("{}_latch_header", prefix)
    });

  current.add_assignment(latch_local, add_fetch(latch_global_mutable_ptr, latch_type.one(), memorder));
  current.end_with_conditional(latch_local == latch_count, merge, header);
  header.add_assignment(latch_local, latch_count);
  
  auto set_zero = context.new_call(compare_exchange,
                                   latch_global_mutable_ptr, latch_local.get_address(), latch_type.zero(),
                                   main.types.bool_.one(),
                                   memorder, memorder);
  auto was_zero = latch_local == latch_type.zero();
  header.end_with_conditional(set_zero || was_zero, merge, header);
  main.pop_block();
}

void cctask(ccmain &main,codes::taskfam task){
  auto &bin = main.bin.bin;
  auto &code = bin.code;
  compile(code, task);

  auto pre0 = [&](grammars::id o){
    auto t = codes::task(o);
    auto c = with_famobj(
      [&](auto o){
        static constexpr auto O = o.tag;
        if constexpr(O == codes::optag::noop)
          return false;
        if constexpr(O == codes::optag::put){
          auto thread = main.bin.bin.thread;
          auto thread_count_ = main.bin.bin.local_thread_count({o});
          auto thread_count = icst32(code, thread_count_);
          auto L = bulk(code, o, thread, {thread_count});
          main.push_scope();
          ccput(main, o, L);
          main.pop_scope();
        }
        if constexpr(O == codes::optag::exit){
          auto scope_index = codes::ieval(code, arg(code, o, 0_N)).value();
          auto scope = main.scope(scope_index);
          auto current = main.pop_block();
          auto dead = main.push_block(fmt::format("{}_dead", main.unique_name(o)));
          current.end_with_conditional(main.types.bool_.one(),
                                       scope_index == 0 ? scope.header : scope.merge,
                                       dead);
          return false;
        }
        if constexpr(O == codes::optag::loop){
          auto current = main.pop_block();
          auto name = main.unique_name(o);
          auto [merge, header, body] = main.push_blocks(std::array{
              fmt::format("{}_merge", name),
              fmt::format("{}_header", name),
              fmt::format("{}_body", name)
            });
          current.end_with_jump(header);
          header.end_with_jump(body);
          main.push_scope(header, merge);
        }
        if constexpr(O == codes::optag::fork){
          auto [w, T] = args(code, o);
          auto w_type = rtype(code, o, 0_N);
          auto name = main.unique_name(o);
          main.push_scope();
          auto B_gcc = ccswitch_header(main, name, w_type, w, std::ranges::ssize(T));
          main.pop_scope();
          for(auto &&[t, b_gcc]:std::views::zip(T, B_gcc)){
            main.blocks.push_back(b_gcc);
            cctask(main, t);
            b_gcc = main.pop_block();
          }
          ccswitch_merge(main, B_gcc);
          return false;
        }
        if constexpr(O == any_of(codes::optag::then, codes::optag::when)){
          auto [U] = args(code, o);
          if(std::ranges::empty(U))
            return false;
          const auto N = std::ranges::size(U);
          const auto T = main.bin.bin.local_thread_count({o});

          auto name = main.unique_name(o);
          for(auto i:views::indices(N - 1)){
            cctask(main, U[i]);
            if constexpr(O == codes::optag::then)
              cclatch(main, T,  fmt::format("{}_{}", name, to_gcc_name(U[i])));
          }
          cctask(main, U[N - 1]);
          if constexpr(O == codes::optag::when)
            cclatch(main, T, fmt::format("{}_{}", name, main.unique_name(U[N - 1])));
          return false;
        }
        return true;
      },
      t);
    return c;
  };
  auto pre1 = [&](grammars::id op,grammars::id rel,grammars::id arg){
    if(codes::reltag_of(rel) != codes::reltag::exe)
      return false;
    return pre0(arg);
  };
  auto post0 = [&](grammars::id op){
    auto t = codes::task(op);
    with_famobj(
      [&](auto o){
        static constexpr auto O = o.tag;
        if constexpr(O == codes::optag::loop){
          auto name = main.unique_name(o);
          const auto T = main.bin.bin.local_thread_count({o});
          cclatch(main, T,  name);
          auto [merge, header, body] = main.top_blocks<3>();
          body.end_with_conditional(main.types.bool_.one(), header, merge);
          main.pop_blocks<2>();
          main.pop_scope();
        }
      },
      t);
    return none{};
  };
  auto post1 = [&](id op, id rel, id arg){
    return post0(arg);
  };
  grammars::walk<none>(code, overload(pre0, pre1), overload(post0, post1), task);
}

bin cc(const config &cfg,code code,codes::cellinfos layout,codes::taskfam task){

  gcc::bin bin{};

  bin.code = std::move(code);
  bin.layout = std::move(layout);
  bin.decl = task;
  bin.thread_count = cfg.thread_count;
  bin.thread = {codes::var(bin.code, bin.code.builtins.types[thread_type])};
  compile(bin.code, bin.thread);
  bin.impl = [&]{
    auto impl = codes::task(codes::compile_implicit_iterators(bin.code, bin.decl.id()));
    impl = codes::task(codes::compile_contractions(bin.code, impl));
    return codes::task(codes::compile_rotations(bin.code, impl));
  }();
  kumi::tie(bin.threads, bin.impl) = ccthreads(cfg, bin.code, bin.thread, bin.impl);
  compile(bin.code, bin.impl);

  bin.image = [&]{
    auto ctx = ccctx(cfg, task);
    gcc::ccbin ccbin{
      .cfg = cfg,
      .bin = bin,
      .context = *ctx
    };
    gcc::cctypes cctypes{
      .bin = ccbin
    };
    gcc::ccmem ccmem{
      .bin = ccbin,
      .types = cctypes
    };
    gcc::ccmain ccmain{
      .bin = ccbin,
      .types = cctypes,
      .mem = ccmem
    };

    auto header = ccmain.push_block("main_header");
    auto merge = ccmain.main.new_block("main_merge");
    {
      ccmain.push_scope(header, merge);
      ccmain.let(bin.thread, ccmain.thread);
      cctask(ccmain, bin.impl);
      HYSJ_ASSERT(ccmain.blocks.size() == 1);
      auto body = ccmain.pop_block();
      ccmain.pop_scope();
      body.end_with_jump(merge);
      merge.end_with_return();
    }
    if(cfg.dot_path)
      ccmain.main.dump_to_dot(fmt::format("{}-{}.dot", *cfg.dot_path, to_gcc_name(bin.impl)));
    if(cfg.c_path)
      ccbin.context.dump_to_file(fmt::format("{}-{}.c", *cfg.c_path, to_gcc_name(bin.impl)), true);
    if(cfg.as_path)
      ccbin.context.compile_to_file(gcc_jit_output_kind::GCC_JIT_OUTPUT_KIND_ASSEMBLER,
                                    fmt::format("{}-{}.s", *cfg.as_path, to_gcc_name(bin.impl)).c_str());
    return std::make_unique<bin::image_>(ctx->compile());
  }();
  bin.kernel = gcc::ccmain::get_code(bin.image->result);
  return bin;
}
bin cc(const config &cfg,code code,codes::taskfam task){
  auto accesses = codes::accesses::make(code, task);
  auto layout = codes::cellinfos::make(code, accesses, code.builtins.host);
  return cc(cfg, std::move(code), std::move(layout), task);
}

} //hysj
#include<hysj/tools/epilogue.hpp>
