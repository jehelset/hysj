#pragma once

#include<algorithm>
#include<array>
#include<cstdint>
#include<memory>
#include<ranges>
#include<thread>

#include<kumi/tuple.hpp>

#include<hysj/grammars.hpp>
#include<hysj/codes/grammar.hpp>
#include<hysj/codes/sets.hpp>
#include<hysj/codes/cells.hpp>
#include<hysj/codes/threads.hpp>
#include<hysj/codes/code.hpp>
#include<hysj/tools/functional/any_of.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::binaries::gcc{

struct HYSJ_EXPORT config{
  natural optimization_level = 2, thread_count = std::thread::hardware_concurrency();
  std::optional<natural> alignment = std::nullopt;

  std::optional<std::string> dot_path, c_path, as_path, arch;

  friend HYSJ_MIRROR_STRUCT(config, (optimization_level, thread_count, alignment, dot_path, c_path, as_path, arch));

  auto operator<=>(const config &) const = default;
};

constexpr bool is_supported_type(codes::type t){
  return t.width >= 8_n && t.width <= 64_n && (t.set != set::reals || t.width == any_of(32_n, 64_n));
}
inline constexpr auto types = []{
  constexpr auto size = []{
    return std::ranges::count_if(codes::typetab, &is_supported_type);
  }();
  std::array<codes::type, size> types;
  std::ranges::copy_if(codes::typetab, types.data(), &is_supported_type);
  return types;
}();

constexpr auto typeindex(codes::type t){
  for(std::size_t i = 0; i != types.size(); ++i)
    if(types[i] == t)
      return just(i);
  return no<std::size_t>;
}

template<codes::type t>
using param = codes::cell_t<t> *;

using kernel =
  reify(
    with_integer_sequence(
      constant_c<std::size(types)>,
      [](auto... I){
        return type_c<void(*)(std::size_t, param<types[I()]>...)>;
      }));

using mem =
  reify(
    with_integer_sequence(
      constant_c<std::size(types)>,
      [](auto... I){
        return type_c<kumi::tuple<std::span<codes::cell_t<types[I()]>>...>>;
      }));

struct HYSJ_EXPORT bin{
  struct image_;

  codes::code code;
  codes::cellinfos layout;
  codes::taskfam decl;
  natural thread_count;
  codes::exprfam thread;
  codes::thread_info threads;
  codes::taskfam impl;

  std::unique_ptr<image_> image;
  gcc::kernel kernel;

  bin();
  bin(bin &&);
  bin &operator=(bin &&);
  ~bin();

  auto local_thread_count(codes::taskfam task)const{
    return std::max(threads.local_count(task).value_or(1_i), 1_i);
  }

  friend HYSJ_MIRROR_STRUCT(bin,(code, layout, decl, thread_count, thread, threads, impl));

  auto operator()(std::size_t thread, mem mem)const{
    return with_integer_sequence(
      constant_c<std::size(types)>,
      [&](auto... I){
        return (*kernel)(thread, get<I()>(mem).data()...);
      });
  }
};

HYSJ_EXPORT bin cc(const config &,code,codes::cellinfos,codes::taskfam);
HYSJ_EXPORT bin cc(const config &,code,codes::taskfam);

} //hysj::binaries::gcc
namespace hysj{

using gcccfg = binaries::gcc::config;
using gccbin = binaries::gcc::bin;
using gccmem = binaries::gcc::mem;
inline constexpr auto gcccc = lift((binaries::gcc::cc));
inline constexpr auto gcctypes = binaries::gcc::types;

} //hysj
#include<hysj/tools/epilogue.hpp>

template<typename C>
HYSJ_STRUCT_FORMATTER((hysj::binaries::gcc::config), C);
