#pragma once

#include<vector>
#include<string>
#include<string_view>

#include<fmt/core.h>
#include<kumi/tuple.hpp>

#include<hysj/codes/code.hpp>
#include<hysj/codes/access.hpp>
#include<hysj/codes/threads.hpp>
#include<hysj/tools/functional/compose.hpp>
#include<hysj/tools/functional/optional.hpp>
#include<hysj/tools/functional/utility.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::binaries::spirv{

using halfword = std::uint16_t;
using word = std::uint32_t;

constexpr natural bwidthmin = 8, swidthmin = 8;

inline std::string debug_name(id op){
  return codes::with_op([](auto op){ return fmt::format("{}", op); }, op);
}

constexpr auto btype(codes::type t){
  if(t.set == set::booleans)
    return codes::type{
      set::naturals,
      std::max<natural>(t.width, bwidthmin)
    };
  return t;
}
constexpr auto etype(codes::type t){
  if(t.set == set::booleans)
    return codes::type{t.set, 1};
  return codes::type{
    t.set,
    std::max<natural>(t.width, swidthmin)
  };
}
constexpr auto obtype(const code &c,id o){
  return btype(otype(c, o));
}
constexpr auto obsize(const code &c,id o){
  return oext(c, o) * obtype(c, o).width / 8;
}
constexpr auto oetype(const code &c,id o){
  return etype(otype(c, o));
}
constexpr auto retype(const code &c,id o){
  return etype(rtype(c, o));
}
constexpr auto with_etype(auto &&f,const code &c,id o) -> decltype(auto) {
  return codes::with_type(
    [&](auto s, auto w) -> decltype(auto){
      return fwd(f)(s, w);
    },
    oetype(c, o));
}
constexpr auto with_btype(auto &&f,const code &c,id o) -> decltype(auto) {
  return codes::with_type(
    [&](auto s, auto w) -> decltype(auto){
      return fwd(f)(s, w);
    },
    obtype(c, o));
}

namespace _{
  template<codes::optag O,std::size_t I>
  constexpr auto retypes_impl(const code &c,sym<O> o,natural_t<I> i,auto r){
    if constexpr(grammars::objvararg<O, I>)
      return views::map(
        r, [&](auto r){
          return retype(c, r);
        });
    else
      return retype(c, r);
  }
}
template<codes::optag O>
constexpr auto retypes(const code &c,sym<O> o){
  return with_integer_sequence(
    constant_c<grammars::objnum<O>>,
    [&](auto... i){
      auto R = grammars::rels(c, o);
      return kumi::make_tuple(_::retypes_impl(c, o, i, kumi::get<i>(R))...);
    });
}
template<codes::optag O,std::size_t I>
constexpr auto retype(const code &c,sym<O> o,constant_t<I> i){
  return _::retypes_impl(c, o, i, rel(c, o, i));
}


#define HYSJ_SPIRV_ACCESSES                   \
  (none)                                      \
  (input)                                     \
  (output)

enum class access : integer {
  HYSJ_SPLAT(0,HYSJ_SPIRV_ACCESSES)
};
HYSJ_MIRROR_ENUM(access,HYSJ_SPIRV_ACCESSES)

constexpr auto operator|(access a0,access a1){
  return access{etoi(a0) | etoi(a1)};
}
constexpr auto &operator|=(access &a0,access a1){
  return (a0 = (a0 | a1));
}
constexpr auto operator&(access a0,access a1){
  return access{etoi(a0) & etoi(a1)};
}
constexpr auto &operator&=(access &a0,access a1){
  return (a0 = (a0 & a1));
}

constexpr auto accesstab = std::array{
  access::none,
  access::input,
  access::output,
  access::input | access::output
};
constexpr auto accessnum = accesstab.size();
constexpr bool is_input(access mask){
  return (mask & access::input) != access::none;
}
constexpr bool is_output(access mask){
  return (mask & access::output) != access::none;
}

constexpr std::string_view debug_name(access a){
  if(a == access::none)
    return "device";
  else if((a & access::input) == a)
    return "input";
  else if((a & access::output) == a)
    return "output";
  else
    return "channel";
}

struct HYSJ_EXPORT env{
  std::array<integer,3>
    max_compute_workgroup_count{1024,1024,1024},
    max_compute_workgroup_size{64,64,64};
  integer min_subgroup_size{32},
    max_subgroup_size{64},
    max_compute_workgroup_shared_memory_size{65536},
    max_compute_workgroup_device_memory_size{536870912};
  natural
    min_storage_buffer_offset_alignment{64},
    max_storage_buffer_range{4294967295},
    min_memory_map_alignment{4096},
    max_buffer_size{4294967292};

  natural optimization_level{2};
  integer max_local_size{max_compute_workgroup_size[0]};

  friend HYSJ_MIRROR_STRUCT(env,
                            (max_compute_workgroup_count,max_compute_workgroup_size,min_subgroup_size,max_subgroup_size,
                             max_compute_workgroup_shared_memory_size,max_compute_workgroup_device_memory_size,
                             min_storage_buffer_offset_alignment,max_storage_buffer_range,
                             min_memory_map_alignment,max_buffer_size,
                             optimization_level, max_local_size));

  auto operator<=>(const env &) const = default;

};

struct HYSJ_EXPORT buffer{
  codes::exprfam op;
  codes::type type;
  natural extent, binding;
  spirv::access access;

  friend HYSJ_MIRROR_STRUCT(buffer,(op,type,extent,binding,access));

  auto operator<=>(const buffer &) const = default;

  constexpr auto stride()const{
    return type.width / 8;
  }
  constexpr auto size()const{
    return extent * stride();
  }
};

struct HYSJ_EXPORT mem{
  static constexpr std::uint32_t descriptor_set = 0;

  codes::accesses accesses;
  std::vector<spirv::buffer> buffers;

  friend HYSJ_MIRROR_STRUCT(mem,(accesses,buffers));

  auto operator<=>(const mem &) const = default;

  constexpr auto buffer_index(codes::exprfam b)const{
    auto it = std::ranges::find(buffers, b, &spirv::buffer::op);
    if(it == std::ranges::end(buffers))
      return no<std::size_t>;
    return just((std::size_t)std::ranges::distance(std::ranges::begin(buffers), it));
  }

  const spirv::buffer *buffer(codes::exprfam op)const{
    if(auto i = buffer_index(op))
      return buffers.data() + *i;
    return nullptr;
  }
  spirv::buffer *buffer(codes::exprfam op){
    if(auto i = buffer_index(op))
      return buffers.data() + *i;
    return nullptr;
  }
};

struct HYSJ_EXPORT kernel{
  codes::taskfam decl;
  codes::exprfam thread;
  codes::thread_info threads;
  codes::taskfam impl;

  friend HYSJ_MIRROR_STRUCT(kernel, (decl, thread, threads, impl));

  auto operator<=>(const kernel &) const = default;

  constexpr auto name()const{
    return debug_name(decl.id());
  }
  constexpr auto thread_count(grammars::id op)const{
    return std::max(threads.local_count(op).value_or(1_i), 1_i);
  }
  constexpr auto thread_count()const{
    return thread_count(impl);
  }
};

struct HYSJ_EXPORT bin{
  codes::code code;
  spirv::mem mem;
  spirv::kernel kernel;
  std::vector<word> tape;

  friend HYSJ_MIRROR_STRUCT(bin, (code, mem, kernel, tape));
};

} //hysj::binaries::spirv
#include<hysj/tools/epilogue.hpp>

