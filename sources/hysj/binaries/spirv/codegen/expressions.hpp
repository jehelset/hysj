#pragma once

#include<array>
#include<cstdint>
#include<variant>
#include<vector>
#include<optional>

#include<kumi/tuple.hpp>

#include<hysj/binaries/spirv/binary.hpp>
#include<hysj/binaries/spirv/codegen/scope.hpp>
#include<hysj/binaries/spirv/codegen/sections.hpp>
#include<hysj/binaries/spirv/codegen/statics.hpp>
#include<hysj/codes/code.hpp>
#include<hysj/codes/grammar.hpp>
#include<hysj/codes/loops.hpp>
#include<hysj/tools/functional/any_of.hpp>
#include<hysj/tools/ranges/utility.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::binaries::spirv::codegen{

template<codes::optag, std::size_t...>
struct expression{};

template<codes::optag o>
requires (codes::has_accessed(o))
struct expression<o>{
  id index;
};

template<>
struct expression<codes::optag::pow, 0>{
  id lhs_f32, rhs_f32, result_f32, result;
};
using expression_pow_float = expression<codes::optag::pow, 0>;

template<>
struct expression<codes::optag::pow, 1>{
  id temporary, counter;

  id init_temporary, init_counter,
    not_done, next_temporary, next_counter;
};
using expression_pow_int = expression<codes::optag::pow, 1>;

using expression_pow_any =
  std::variant<expression<codes::optag::pow, 0>,
               expression<codes::optag::pow, 1>>;

template<>
struct expression<codes::optag::pow>{
  expression_pow_any impl;
};

template<>
struct expression<codes::optag::fct>{
  id temporary, counter;

  id init_temporary, init_counter,
    not_done, next_temporary, next_counter;
};

template<>
struct expression<codes::optag::con>{
  std::vector<codes::loop_iterator> contractors;

  id temporary, init;
};

struct expressions{
  using container_type = reify(
    with_enumerators<codes::optag>(
      [](auto... o){
        return type_c<kumi::tuple<std::vector<std::optional<expression<o()>>>...>>;
      }));

  container_type container;

  template<codes::optag O>
  auto &operator[](sym<O> s){
    auto &S = kumi::get<etoi(O)>(container);
    if(S.size() <= s.idx)
      S.resize(s.idx + 1);
    return S.at(s.idx);
  }
  template<codes::optag O>
  auto &operator[](sym<O> s)const{
    auto &S = kumi::get<etoi(O)>(container);
    return S.at(s.idx);
  }
};

template<>
struct expression<codes::optag::put>{
  std::vector<codes::loop_iterator> iters;
};

grammars::id build_walk(body &,expressions &,grammars::id);

inline void build(body &body, expressions &exprs,unwind &u){
  auto &code = body.emit.bin.code;
  u.var = body.scalar(build_walk(body, exprs, codes::ivar32(code)), {"unwind_var"}).op;
  u.on = build_walk(body, exprs, code.builtins.lits.zero32);
  u.off = build_walk(body, exprs, code.builtins.lits.one32);
  u.no_unwind = build_walk(body, exprs, codes::eq(code, u.var, u.off));
}

inline void build_walk(body &body, expressions &exprs, const codes::loop_iterator &i) {
  for(auto o : std::to_array<grammars::id>({i.sym, i.test, i.init, i.head, i.tail}))
    build_walk(body, exprs, o);
}

inline expression<codes::optag::put> build_assign(body &body,expressions &exprs,codes::putsym o,
                                                  codes::concepts::forward_loop_iterators auto &&iters){
  auto &code = body.emit.bin.code;
  for(const auto &i:iters)
    build_walk(body, exprs, body.iterator(i));
  auto [l, r] = args(code, o);
  build_walk(body, exprs, l);
  build_walk(body, exprs, r);
  return {
    ranges::vec(fwd(iters))
  };
}

template<codes::optag O>
expression<O> build(body &body,expressions &expr, sym<O> o){
  expression<O> a;

  auto &code = body.emit.bin.code;

  if constexpr(O == any_of(codes::optag::lit, codes::optag::cst)){
    body.let(o, body.statics.scalars.scalar(o), false);
    return a;
  }

  if constexpr(codes::has_accessed(O))
    build_walk(body, expr, a.index = index_of(code, o));
  if constexpr(O == codes::optag::itr)
    if(!body.has_variable(o))
      build_walk(body, expr, body.iterator(make_basic_loop_iterator(code, o)));

  if constexpr(O == codes::optag::fct){
    a.temporary = body.scalar(build_walk(body, expr, tmp_for(code, o))).op;
    a.counter = body.scalar(build_walk(body, expr, tmp_for(code, o, set::integers))).op;
    auto [u] = args(code, o);
    build_walk(body, expr, a.init_temporary =
         codes::cvt_for(code, a.counter, code.builtins.lits.one32));
    build_walk(body, expr, a.init_counter =
         codes::cvt_for(code, a.counter, u));
    build_walk(body, expr, a.not_done =
         codes::lnot(code,
                     codes::eq(code, a.counter, code.builtins.lits.zero32)));
    build_walk(body, expr, a.next_temporary =
         codes::mul(code, {a.temporary, a.counter}));
    build_walk(body, expr, a.next_counter = dec(code, a.counter));
  }

  if constexpr(O == codes::optag::pow){
    auto [lhs, rhs] = args(code, o);
    if(codes::is_integral(oset(code, rhs))){
      //FIXME: use iterator
      expression_pow_int impl{
        .temporary = body.scalar(build_walk(body, expr, tmp_for(code, o))).op,
        .counter = body.scalar(build_walk(body, expr, tmp_for(code, rhs))).op,
        .init_temporary = build_walk(body, expr, codes::cvt_for(code, o, code.builtins.lits.one32)),
        .init_counter = rhs,
        .not_done = build_walk(body, expr, 
          codes::lnot(code,
                      codes::eq(code, impl.counter, code.builtins.lits.zero32))),
        .next_temporary = build_walk(body, expr, 
          codes::mul(code, {impl.temporary, codes::cvt_for(code, o, lhs)})),
        .next_counter = build_walk(body, expr, dec(code, impl.counter))
      };
      a.impl = impl;
    }
    else{
      HYSJ_ASSERT(oset(code, o) == codes::set::reals);
      expression_pow_float impl{
        .lhs_f32 = build_walk(body, expr, codes::rcvt32(code, lhs)),
        .rhs_f32 = build_walk(body, expr, codes::rcvt32(code, rhs)),
        .result_f32 = build_walk(body, expr, codes::rvar32(code)),
        .result = build_walk(body, expr, codes::cvt_for(code, o, impl.result_f32))
      };
      a.impl = impl;
    }
  }

  if constexpr(O == codes::optag::con){
    a.contractors = ranges::vec(
      arg(code, o, 1_N),
      [&](auto i){
        return body.iterator(codes::make_basic_loop_iterator(code, i));
      });
    for(auto i:a.contractors)
      build_walk(body, expr, i);
    auto [u, I] = args(code, o);
    compile(code, u);
    build_walk(body, expr, 
      a.temporary = body.scalar(build_walk(body, expr, tmp_for(code, u))).op);
    build_walk(body, expr, 
      a.init = cvt_for(code, u,
                       codes::builtins::identity(code.builtins.lits, u, 32).value()));
  }

  if constexpr(O == codes::optag::put)
    a = build_assign(body, expr, o, make_basic_loop(code, o));

  return a;
}

inline grammars::id build_walk(body &body,expressions &exprs,grammars::id o){
  auto &code = body.emit.bin.code;
  
  compile(code, o);

  auto pre0 = [&](grammars::id o){
    return codes::with_op(
      [&](auto o){
        if(exprs[o])
          return false;
        else{
          auto a = build(body, exprs, o);
          exprs[o] = std::move(a);
          return !is_access(code, o);
        }
      },
      o);
  };
  auto pre1 = [&](grammars::id,grammars::id rel,grammars::id arg){
    if(codes::reltag_of(rel) == any_of(codes::reltag::dom, codes::reltag::dim))
      return false;
    return pre0(arg);
  };

  grammars::walk<none>(
    code,
    overload(pre0, pre1),
    never,
    o);
  return o;
}

inline std::optional<word> assemble(body &, const expressions &, grammars::id);

void assemble_loop_head(body &body, const expressions &exprs,
                        codes::concepts::forward_loop_iterators auto &&iterators){
  for(const auto &iterator:iterators){

    auto iterator_name = debug_name(iterator.sym);
    assemble(body, exprs, iterator.init);

    auto &emit = body.emit;
    auto &&[header, continue_, merge, let] = body.down(
      {
        emit.new_id(fmt::format("{}_header", iterator_name)),
        emit.new_id(fmt::format("{}_continue", iterator_name)),
        emit.new_id(fmt::format("{}_merge", iterator_name))
      });

    auto ramp = emit.new_id(fmt::format("{}_ramp", iterator_name));
    emit.loop_merge(*merge, continue_);
    emit.branch(ramp);
    emit.label(ramp);

    body.unlet(iterator.sym);
    auto now = assemble_let(body, iterator.sym);
    auto test = assemble(body, exprs, iterator.test).value();
    emit.debug_name(test, fmt::format("{}_test", debug_name(iterator.sym)));
    auto block = emit.new_id(fmt::format("{}_block", iterator_name));
    emit.branch_conditional(test, block, *merge);
    emit.label(block);
    assemble(body, exprs, iterator.head);
  }

}
void assemble_loop_tail(body &body, const expressions &exprs,
                        codes::concepts::forward_loop_iterators auto &&iterators){
  auto &emit = body.emit;
  for(const auto &iterator:std::views::reverse(iterators)){
    auto &&[header, continue_, merge, let] = body.up();
    assemble(body, exprs, iterator.tail);
    emit.branch(header);
    emit.label(*merge);
  }
}
auto assemble_assign_store(body &body, const expressions &exprs,codes::exprfam lhs,word rhs_asm){
  return codes::with_op(
    [&](auto lhs){
      const auto &code = body.emit.bin.code;
      if constexpr(codes::has_accessed(lhs.tag())){
        auto &expr = exprs[lhs].value();
        auto index_asm = assemble(body, exprs, expr.index).value();
        assemble_store(body, lhs, views::one(index_asm), rhs_asm);
      }
      else if constexpr(lhs.tag() == codes::optag::cur)
        assemble_assign_store(body, exprs, arg(code, lhs, 0_N), rhs_asm);
      else if constexpr(lhs.tag() == codes::optag::itr)
        assemble_store(body, lhs, rhs_asm);
      else
        throw std::runtime_error(fmt::format("invalid lhs: {}", lhs));
    },
    lhs);
}
void assemble_assign_body(body &body, const expressions &exprs,codes::putsym o){
  auto [l, r] = args(body.emit.bin.code, o);
  assemble(body, exprs, r);
  auto rhs_asm = body.rel(o, 1_N);
  assemble_assign_store(body, exprs, l, rhs_asm);
}
void assemble_assign(body &body, const expressions &exprs,codes::putsym o){
  const auto &a = exprs[o].value();
  if (std::ranges::empty(a.iters))
    assemble_assign_body(body, exprs, o);
  else{
    assemble_loop_head(body, exprs, a.iters);
    assemble_assign_body(body, exprs, o);
    assemble_loop_tail(body, exprs, a.iters);
  }
}

template<codes::optag O>
bool assemble_pre(body &body, const expressions &exprs, sym<O> o){
  auto &emit = body.emit;
  const auto &a = exprs[o].value();
  if constexpr(codes::has_accessed(O) || O == codes::optag::itr){
    auto i = [&]{
      if constexpr(O == codes::optag::itr)
        return views::no<word>;
      else
        return views::one(assemble(body, exprs, a.index).value());
    }();
    assemble_let(body, o, i);
    return false;
  }

  if constexpr(O == codes::optag::con){
    assemble_store(body, a.temporary, assemble(body, exprs, a.init).value());
    assemble_loop_head(body, exprs, a.contractors);
  }

  if constexpr(O == codes::optag::sel){
    auto [pre,post,aux,let] = body.down();
    emit.debug_name(pre, fmt::format("{}_head", debug_name(o)));
    emit.debug_name(post, fmt::format("{}_tail", debug_name(o)));
  }
  if constexpr(O == codes::optag::put){
    assemble_assign(body, exprs, o);
    return false;
  }
  if constexpr(O == codes::optag::noop)
    return false;
  return true;
}

template<codes::optag O>
std::optional<word> assemble_fold(
  body &body,
  const expressions &expr,
  sym<O> o,
  concepts::random_access_words auto &&relexprs){

  auto &emit = body.emit;

  const auto [F, W] = oetype(emit.bin.code, o);

  auto [insn,init] = [&] -> std::optional<kumi::tuple<word, word>> {
    if constexpr(O == codes::optag::add)
      if(F == any_of(set::integers,set::naturals,set::reals))
        return kumi::tuple<word,word>{
          F == set::reals ? instructions::FAdd : instructions::IAdd,
          body.statics.scalars.scalar(0, {F, W})
        };
    if constexpr(O == codes::optag::mul)
      if(F == any_of(set::integers,set::naturals,set::reals))
        return kumi::tuple<word,word>{
          F == set::reals ? instructions::FMul : instructions::IMul,
          body.statics.scalars.scalar(1, {F, W})
        };
    if constexpr(O == codes::optag::band)
      if(F == any_of(set::integers,set::naturals))
        return kumi::tuple<word,word>{
          instructions::BitwiseAnd,
          body.statics.scalars.scalar(~0_n, {F, W})
        };
    if constexpr(O == codes::optag::bor)
      if(F == any_of(set::integers,set::naturals))
        return kumi::tuple<word,word>{
          instructions::BitwiseOr,
          body.statics.scalars.scalar(0_n, {F, W})
        };
    if constexpr(O == codes::optag::land)
      if(F == set::booleans)
        return kumi::tuple<word,word>{instructions::LogicalAnd,body.statics.scalars.true_};
    if constexpr(O == codes::optag::lor)
      if(F == set::booleans)
        return kumi::tuple<word,word>{instructions::LogicalOr,body.statics.scalars.false_};
    return std::nullopt;
  }().value();

  auto type = body.statics.types.scalar(codes::type{F, W});
  return just(
    std::ranges::fold_left(
      relexprs,
      init,
      [&](auto result,auto rel){
        auto result_next = emit.new_id(debug_name(o));
        emit.insn(section::functions,
                  insn, 5,
                  type, result_next, result, rel);
        return result_next;
      }));
}

template<codes::optag O>
std::optional<word> assemble_post(body &body, const expressions &exprs, sym<O> o){
  auto &emit = body.emit;
  auto &code = body.emit.bin.code;

  const auto [F, W] = oetype(code, o);
  const auto type = body.statics.types.scalar(codes::type{F, W});

  const auto &a = exprs[o].value();
  if constexpr(O == codes::optag::cvt)
    return just(body.rel(o, 1_N));
  if constexpr(O == codes::optag::cur)
    return just(body.rel(o, 0_N));
  if constexpr(O == any_of(codes::optag::neg, codes::optag::lnot, codes::optag::bnot)){

    auto insn = [&] -> std::optional<word> {
      if constexpr(O == codes::optag::neg){
        if(F == set::integers)
          return instructions::SNegate;
        if(F == set::reals)
          return instructions::FNegate;
      }
      if constexpr(O == codes::optag::lnot){
        if(F == set::booleans)
          return instructions::LogicalNot;
      }
      if constexpr(O == codes::optag::bnot){
        if(F == any_of(set::integers,set::naturals))
          return instructions::Not;
      }
      return std::nullopt;
    }().value();

    auto oprn = body.rel(o, 0_N);
    auto result = emit.new_id();
    emit.insn(section::functions,
              insn, 4, type, result, oprn);
    return just(result);

  }
  if constexpr(O == any_of(codes::optag::mod, codes::optag::blsh, codes::optag::brsh,
                           codes::optag::lt, codes::optag::eq)){
    auto insn =
      [&] -> std::optional<word> {
      if constexpr(O == codes::optag::mod){
        if(F == set::integers)
          return instructions::SMod;
        if(F == set::naturals)
          return instructions::UMod;
        if(F == set::reals)
          return instructions::FMod;
      }
      if constexpr(O == any_of(codes::optag::lt, codes::optag::eq)){
        auto [lhs, rhs] = rels(code, o);
        auto [lhsfield, lhswidth] = retype(code, lhs);
        auto [rhsfield, rhswidth] = retype(code, rhs);
        HYSJ_ASSERT(lhsfield == rhsfield);
        const auto G = lhsfield;
        if constexpr(O == codes::optag::eq){
          if(G == any_of(set::integers,set::naturals))
            return instructions::IEqual;
          if(G == set::booleans)
            return instructions::LogicalEqual;
          if(G == set::reals)
            return instructions::FOrdEqual;
        }
        if constexpr(O == codes::optag::lt){
          if(G == set::integers)
            return instructions::SLessThan;
          if(G == set::naturals)
            return instructions::ULessThan;
          if(G == set::reals)
            return instructions::FOrdLessThan;
        }
      }
      if constexpr(O == codes::optag::blsh){
        if(F == any_of(set::integers, set::naturals))
          return instructions::ShiftLeftLogical;
      }
      if constexpr(O == codes::optag::brsh){
        if(F == any_of(set::integers, set::naturals))
          return instructions::ShiftRightLogical;
      }
      return std::nullopt;
    }().value();

    auto [lhs, rhs] = body.rels(o);
    if constexpr(O == codes::optag::mod){
      if(F == set::integers){

        auto lhs1 = emit.new_id(fmt::format("{}_tmp_0", debug_name(o)));
        emit.insn(section::functions,
                  insn, 5,
                  type, lhs1, lhs, rhs);
        auto lhs2 = emit.new_id(fmt::format("{}_tmp_1", debug_name(o)));
        emit.insn(section::functions,
                  instructions::IAdd, 5,
                  type, lhs2, lhs1, rhs);
        lhs = lhs2;

      }

    }

    auto result = emit.new_id();
    emit.insn(section::functions,
              insn, 5,
              type, result, lhs, rhs);
    return just(result);

  }
  if constexpr(O == any_of(codes::optag::sub,codes::optag::div,codes::optag::bxor)){
    auto insn = [&] -> std::optional<word>{
      if constexpr(O == codes::optag::sub){
        if(F == any_of(set::integers,set::naturals))
          return instructions::ISub;
        if(F == set::reals)
          return instructions::FSub;
      }
      if constexpr(O == codes::optag::div){
        if(F == set::integers)
          return instructions::SDiv;
        if(F == set::naturals)
          return instructions::UDiv;
        if(F == set::reals)
          return instructions::FDiv;
      }
      if constexpr(O == codes::optag::bxor){
        if(F == any_of(set::integers,set::naturals))
          return instructions::BitwiseXor;
      }
      return std::nullopt;
    }().value();

    auto [lhs, rhs] = [&]{
      if constexpr(O == codes::optag::sub)
        return body.rels(o);
      else if constexpr(O == codes::optag::div){
        auto [lhs, rhs] = body.rels(o);
        return kumi::make_tuple(lhs, std::views::single(rhs));
      }
      else{
        auto [rels] = body.rels(o);
        HYSJ_ASSERT(!std::ranges::empty(rels));
        return kumi::make_tuple(ranges::first(rels), std::views::drop(rels, 1));
      }
    }();

    return just(
      std::ranges::fold_left(
        rhs,
        lhs,
        [&](auto result,auto rhs){
          auto result_next = emit.new_id(debug_name(o));
          emit.insn(section::functions,
                    insn, 5,
                    type, result_next, result, rhs);
          return result_next;
        }));

  }
  if constexpr(O == any_of(codes::optag::add, codes::optag::mul, codes::optag::band,
                           codes::optag::bor, codes::optag::land, codes::optag::lor))
    return assemble_fold(body, exprs, o, body.rel(o, 0_N));

  if constexpr(O == codes::optag::sin){
    auto [angle] = body.rels(o);
    auto angle_f32 = body.statics.scalars.cast({set::reals, 32}, {F, W}, angle);
    auto result_f32 = emit.new_id(fmt::format("{}_f32", debug_name(o)));

    auto f32 = body.statics.types.scalar(codes::type{set::reals, 32});
    emit.insn(section::functions,
              instructions::ExtInst, 6,
              f32, result_f32, body.statics.imports.glsl_std_450.id,
              instructions::GLSL_std_450_Sin, angle_f32);

    return just(body.statics.scalars.cast({F, W}, {set::reals, 32}, result_f32));
  }

  if constexpr(O == codes::optag::log){
    if(F != set::reals)
      return no<word>;

    auto [lhs, rhs] = body.rels(o);
    auto [lhs_t, rhs_t] = retypes(code, o);

    codes::type f32_t{set::reals, 32};
    auto f32 = body.statics.types.scalar(f32_t);

    auto lhs_f32 = body.statics.scalars.cast(f32_t, lhs_t, lhs);
    auto rhs_f32 = body.statics.scalars.cast(f32_t, rhs_t, rhs);

    auto result_f32 = emit.new_id();

    auto tmp0_f32 = emit.new_id();
    auto tmp1_f32 = emit.new_id();

    emit.insn(section::functions,
              instructions::ExtInst, 6,
              f32, tmp0_f32,
              body.statics.imports.glsl_std_450.id,
              instructions::GLSL_std_450_Log, lhs_f32);
    emit.insn(section::functions,
              instructions::ExtInst, 6,
              f32, tmp1_f32,
              body.statics.imports.glsl_std_450.id,
              instructions::GLSL_std_450_Log, rhs_f32);
    emit.insn(section::functions,
              instructions::FDiv, 5,
              f32, result_f32, tmp1_f32, tmp0_f32);

    return just(body.statics.scalars.cast({F, W}, f32_t, result_f32));
  }

  if constexpr(O == codes::optag::fct){

    if(!codes::is_integral(oset(code, o)))
      return no<word>;

    auto [pre,post,aux,let] = body.down();

    emit.debug_name(pre,fmt::format("{}_head", debug_name(o)));
    emit.debug_name(post,fmt::format("{}_tail", debug_name(o)));

    auto init_temporary = assemble(body, exprs, a.init_temporary).value();
    assemble_store(body, a.temporary, init_temporary);
    auto init_counter = assemble(body, exprs, a.init_counter).value();
    assemble_store(body, a.counter, views::no<word>,init_counter);

    auto ctrl = emit.label(emit.branch());
    emit.debug_name(ctrl, fmt::format("{}_ctrl", debug_name(o)));

    assemble_let(body, a.counter);
    auto not_done = assemble(body, exprs, a.not_done).value();

    auto body_label = emit.new_id(fmt::format("{}_body", debug_name(o)));
    emit.loop_merge(post, body_label);
    emit.branch_conditional(not_done, body_label, post);
    emit.label(body_label);

    auto next_counter = assemble(body, exprs, a.next_counter).value();
    assemble_store(body, a.counter, next_counter);

    assemble_let(body, a.temporary);
    auto next_temporary = assemble(body, exprs, a.next_temporary).value();
    assemble_store(body, a.temporary, next_temporary);

    emit.branch(ctrl);
    body.up(false);

    return just(assemble_load(body, a.temporary, views::no<word>));
  }

  if constexpr(O == codes::optag::pow)
    return visitor(
      [&](const expression_pow_float &a){
        auto f32 = body.statics.types.scalar(codes::type{set::reals, 32});
        auto lhs_f32 = assemble(body, exprs, a.lhs_f32).value();
        auto rhs_f32 = assemble(body, exprs, a.rhs_f32).value();
        auto result_f32 = emit.new_id();
        //FIXME: undefined if x < 0
        emit.insn(section::functions,
                  instructions::ExtInst, 7,
                  f32, result_f32,
                  body.statics.imports.glsl_std_450.id,
                  instructions::GLSL_std_450_Pow, lhs_f32, rhs_f32);
        body.let(a.result_f32, result_f32);
        return assemble(body, exprs, a.result);
      },
      [&](const expression_pow_int &a){

        auto [pre,post,aux,let] = body.down(
          {
            emit.new_id(fmt::format("{}_head", debug_name(o))),
            emit.new_id(fmt::format("{}_tail", debug_name(o))),
            emit.new_id(fmt::format("{}_ctrl", debug_name(o)))
          });

        auto init_temporary = assemble(body, exprs, a.init_temporary).value();
        assemble_store(body, a.temporary, init_temporary);
        auto init_counter = assemble(body, exprs, a.init_counter).value();
        assemble_store(body, a.counter, views::no<word>,init_counter);

        emit.label(emit.branch(*aux));

        assemble_let(body, a.counter);
        auto not_done = assemble(body, exprs, a.not_done).value();

        auto body_label = emit.new_id(fmt::format("{}_body", debug_name(o)));
        emit.loop_merge(post, body_label);
        emit.branch_conditional(not_done, body_label, post);
        emit.label(body_label);

        auto next_counter = assemble(body, exprs, a.next_counter).value();
        assemble_store(body, a.counter, next_counter);

        assemble_let(body, a.temporary);
        auto next_temporary = assemble(body, exprs, a.next_temporary).value();
        assemble_store(body, a.temporary, next_temporary);

        emit.branch(*aux);
        body.up(false);

        return just(assemble_load(body, a.temporary, views::no<word>));

      })(a.impl);

  if constexpr(O == codes::optag::con){

    auto temp = assemble_load(body, a.temporary, views::no<word>);

    auto arg = grammars::arg(code, o, 0_N);

    std::array<word, 2> relexprs{ temp, body.let(arg).value() };

    auto next_temp = codes::with_op(
      [&](auto arg){
        if constexpr(requires{ assemble_fold(body, exprs, arg, relexprs); })
          return assemble_fold(body, exprs, arg, relexprs);
        else
          return no<word>;
      },
      arg).value();
    //FIXME: need to unbind...
    //       for(i e ...){
    //         for(i j ...){
    //           ...
    //         }
    //         unbind(i)
    //       }
    //       unbind(j)
    emit.debug_name(next_temp, fmt::format("{}_temp_next", o));
    assemble_store(body, a.temporary, next_temp);
    assemble_loop_tail(body, exprs, a.contractors);

    auto result = assemble_load(body, a.temporary, views::no<word>);
    return just(result);
  }

  if constexpr(O == codes::optag::sel){

    auto result_id = [&]{

      auto [which_type,case_types] = retypes(code, o);
      auto [which, cases_] = body.rels(o);
      auto cases = ranges::vec(cases_);
      auto case_num = std::ranges::size(cases);
      HYSJ_ASSERT(case_num > 0);
      if(case_num == 1)
        return ranges::first(cases);

      auto merge = emit.new_id(fmt::format("{}_merge", debug_name(o)));

      auto case_labels = ranges::vec(views::generate_n([&]{ return emit.new_id(); },case_num));
      for(auto [case_idx,case_label]:std::views::enumerate(case_labels))
        if(static_cast<std::size_t>(case_idx + 1) != case_num)
          emit.debug_name(case_label, fmt::format("{}_case_{}", debug_name(o), case_idx));
        else
          emit.debug_name(case_label, fmt::format("{}_default", debug_name(o)));

      auto ewhich_width = emit.lit_size<codes::cell_t<case_type>>();
      auto ewhich = body.statics.scalars.cast(case_type,
                                 which_type,
                                 which);
      emit.debug_name(ewhich, fmt::format("{}_which", debug_name(o)));

      emit.insn(section::functions,
                instructions::SelectionMerge, 3,
                merge, null);
      emit.insn(section::functions,
                instructions::Switch,
                3 + (1 + ewhich_width)*(case_num - 1),
                ewhich, ranges::last(case_labels));

      for(auto [case_idx, case_label]:
            std::views::take(
              std::views::enumerate(case_labels),
              case_num - 1)){
        emit.lit(section::functions, static_cast<codes::cell_t<case_type>>(case_idx));
        emit.words(section::functions, case_label);
      }

      for(auto case_label:case_labels){
        emit.label(case_label);
        emit.branch(merge);
      }
      emit.label(merge);

      auto result = emit.new_id();

      emit.insn(section::functions,
                instructions::Phi, 3 + 2*case_num,
                type, result);

      for(auto [case_,case_label]:std::views::zip(cases, case_labels))
        emit.words(section::functions, case_, case_label);

      return result;
    }();
    body.up();
    return just(result_id);

  }

  return std::nullopt;

}
inline std::optional<word> assemble(body &body, const expressions &exprs, grammars::id root){
  auto &code = body.emit.bin.code;
  auto pre0 = [&](grammars::id o){
    if(body.let(o))
      return false;
    return codes::with_op(
      [&](auto o){
        return assemble_pre(body, exprs, o);
      },
      o);
  };
  auto pre1 = [&](grammars::id op,grammars::id rel,grammars::id arg){
    if(codes::reltag_of(rel) == any_of(codes::reltag::dom, codes::reltag::dim, codes::reltag::idx,
                                       codes::reltag::dst))
      return false;
    return pre0(arg);
  };
  auto post0 = [&](auto... ev){
    codes::with_op(
      [&](auto o){
        if(auto w = assemble_post(body, exprs, o)){
          body.emit.debug_name(w.value(), debug_name(o));
          body.let(o, w.value());
        }
      },
      pick<-1>(ev...));
    return none{};
  };
  grammars::walk<none>(
    code,
    overload(pre0, pre1),
    post0,
    root);
  return body.let(root);
}


} //hysj::binaries::spirv::codegen
#include<hysj/tools/epilogue.hpp>
