#pragma once

#include<vector>
#include<string_view>

#include<hysj/binaries/spirv/binary.hpp>
#include<hysj/binaries/spirv/codegen/sections.hpp>
#include<hysj/codes/code.hpp>
#include<hysj/codes/grammar.hpp>
#include<hysj/codes/sets.hpp>
#include<hysj/codes/threads.hpp>
#include<hysj/tools/functional/any_of.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::binaries::spirv::codegen{

//NOTE: 32 because spirvbug! - jeh
constexpr codes::type bool_type{set::booleans, 1};
constexpr codes::type index_type{set::integers, 32};
constexpr auto thread_type = index_type;
constexpr codes::type case_type{set::naturals, 32};

struct scalar_type : codes::type{
  word id;
};
struct array_type {
  word value_type, extent, id;
};
struct pointer_type{
  word value_type, storage_class, id;
};

struct buffer : spirv::buffer{
  word id;
  word member;
  array_type member_type;
  std::vector<word> extents, strides;
};

inline constexpr auto is_extension_op = any_of(codes::optag::sin, codes::optag::log,
                                               codes::optag::pow, codes::optag::fct);

struct library{
  std::string_view name;
  word id;
};
struct imports{

  sections &emit;

  library glsl_std_450{"GLSL.std.450",null};

  friend HYSJ_MIRROR_STRUCT(imports,(glsl_std_450));
};

struct types{
  sections &emit;

  word void_,kernel,bool_;
  std::vector<scalar_type> scalars;
  std::vector<pointer_type> pointers;
  std::vector<array_type> arrays;

  word scalar(codes::type info){
    auto it = std::ranges::find(scalars, info);
    if(it != std::ranges::end(scalars))
      return it->id;

    auto type = scalars.emplace_back(codegen::scalar_type{
        {info},
        emit.new_id(fmt::format("{}{}", enumerator_name(info.set)[0], info.width))
      }).id;

    if(info.set == set::booleans)
      emit.insn(section::statics,
                instructions::TypeBool, 2,
                type);
    else if(info.set == any_of(set::naturals, set::integers))
      emit.insn(section::statics,
                instructions::TypeInt, 4,
                type, static_cast<word>(info.width),
                static_cast<word>(codes::is_signed(info.set)));
    else
      emit.insn(section::statics,
                instructions::TypeFloat, 3,
                type, static_cast<word>(info.width));
    return type;
  }
  auto scalar(id op){
    return scalar(otype(emit.bin.code, op));
  }

  auto pointer(word value_type_asm,word storage_class){
    auto it = std::ranges::find_if(
      pointers,
      [&](const auto &p){
        return std::tie(p.value_type,p.storage_class) ==
          std::tie(value_type_asm,storage_class);
      });
    if(it != std::ranges::end(pointers))
      return *it;

    auto type = pointers.emplace_back(codegen::pointer_type{
        value_type_asm,
        storage_class,
        emit.new_id()
      });
    emit.insn(section::statics,
              instructions::TypePointer, 4,
              type.id, storage_class, value_type_asm);
    return type;
  }
  auto pointer(codes::type info,word storage_class){
    return pointer(scalar(info), storage_class);
  }

  constexpr auto iterator(id o){
    return scalar(codes::itype(emit.bin.code, o));
  }

  word buffer(id buf){
    return scalar(emit.bin.mem.buffer(codes::expr(buf))->type);
  }
  auto oexpr(id o){
    return scalar(oetype(emit.bin.code, o));
  }
  auto rexpr(id r){
    return scalar(retype(emit.bin.code, r));
  }

  auto array(codes::type value_type,
             word extent_asm,
             std::string_view name = {}){
    auto value_type_asm = scalar(value_type);

    auto it = std::ranges::find_if(arrays, [&](const auto &t){
      return std::tie(t.value_type, t.extent)  == std::tie(value_type_asm, extent_asm);
    });
    if(it != std::ranges::end(arrays))
      return *it;

    auto type_asm = emit.new_id(name);
    emit.insn(section::statics,
              instructions::TypeArray, 4,
              type_asm, value_type_asm, extent_asm);
    emit.insn(section::annotations,
              instructions::Decorate, 4,
              type_asm, decoration::ArrayStride, static_cast<word>(value_type.width / 8));
    return arrays.emplace_back(value_type_asm, extent_asm, type_asm);
  }

  void variable(codegen::section section, pointer_type type, word decl){
    emit.insn(section,
              instructions::Variable, 4,
              type.id,
              decl,
              type.storage_class);
  }
  word block(codegen::buffer &b){
    auto block_type_asm = emit.new_id(fmt::format("block_{}_type", debug_name(b.op)));

    const auto member_count = 1;
    emit.insn(section::statics,
              instructions::TypeStruct,2 + member_count,
              block_type_asm);
    emit.words(section::statics, views::one(b.member_type.id));

    emit.insn(section::annotations,
              instructions::Decorate, 3,
              block_type_asm, decoration::Block);

    emit.insn(section::annotations,
              instructions::MemberDecorate, 5,
              block_type_asm, static_cast<word>(0), decoration::Offset,
              static_cast<word>(0));
    emit.debug_member_name(block_type_asm,
                           0,
                           fmt::format("block_{}_member", debug_name(b.op)));


    if(b.access == access::input)
      emit.insn(section::annotations,
                instructions::MemberDecorate, 4,
                block_type_asm, static_cast<word>(0), decoration::NonWritable);

    auto block_asm = emit.new_id(fmt::format("block_{}", debug_name(b.op)));
    emit.insn(section::annotations,
              instructions::Decorate, 4,
              block_asm, decoration::DescriptorSet, static_cast<word>(spirv::mem::descriptor_set));
    emit.insn(section::annotations,
              instructions::Decorate, 4,
              block_asm, decoration::Binding, static_cast<word>(b.binding));

    auto block_pointer_type_asm = emit.new_id(fmt::format("block_{}_ptr", debug_name(b.op)));
    emit.insn(section::statics,
              instructions::TypePointer, 4,
              block_pointer_type_asm,
              storage_class::StorageBuffer, block_type_asm);

    emit.insn(section::statics,
              instructions::Variable, 4,
              block_pointer_type_asm, block_asm, storage_class::StorageBuffer);

    return block_asm;
  }

  word load(codes::type type, word ptr){
    return emit.load(scalar(type), ptr);
  }

};

struct scalar : codes::type{
  anycell value;
  word id;
};

struct scalars{

  sections &emit;
  codegen::types &types;

  std::vector<codegen::scalar> container;
  word false_,true_;

  template<codes::type type>
  constexpr word new_scalar(auto value, word id){
    if constexpr(type.set == set::booleans)
      emit.insn(section::statics,value ? instructions::ConstantTrue : instructions::ConstantFalse, 3, types.bool_, id);
    else if constexpr(codes::has_cell<type.set, type.width>){
      auto type_id = types.scalar(type);
      auto coerced_value = static_cast<cell<type.set, type.width>>(value);
      constexpr auto size = emit.lit_size(coerced_value);
      emit.insn(section::statics,
                instructions::Constant, 3 + size,
                type_id, id);
      emit.lit(section::statics, coerced_value);
      emit.debug_name(
        id,
        fmt::format("{}{}_{}",
                    enumerator_name(type.set)[0],
                    type.width,
                    codes::value<type.set>(coerced_value)));
    }
    else
      throw std::runtime_error(fmt::format("invalid scalar type: {}", type));
    return id;
  }
  template<codes::type type>
  constexpr auto scalar(auto value){
    codegen::scalar scalar{
      {type},
      anycell(std::in_place_index<etoi(type.set)>, value),
      null
    };
    {
      auto it = std::ranges::find_if(container,
                                     [&](const auto &scalar_){
                                       return std::tie(scalar_.set, scalar_.width, scalar_.value) ==
                                         std::tie(scalar.set, scalar.width, scalar.value);
                                     });
      if(it != std::ranges::end(container))
        return it->id;
    }
    scalar.id = new_scalar<type>(value, emit.new_id());
    container.push_back(std::move(scalar));
    return container.back().id;
  }
  #define HYSJ_LAMBDA(S_,s_,w_)                   \
    constexpr auto s_##w_(auto v){                \
      return scalar<codes::type{set::S_,w_}>(v);  \
    }
  HYSJ_MAP_LAMBDA(HYSJ_CODES_TYPES)
  #undef HYSJ_LAMBDA

  constexpr auto new_scalar(auto value,codes::type type, word id){
    return codes::with_type(
      [&](auto s,auto w){
        return new_scalar<codes::type{s(), w()}>(value, id);
      },
      type);
  }
  constexpr auto scalar(auto value,codes::type type){
    return codes::with_type(
      [&](auto s,auto w){
        return scalar<codes::type{s(), w()}>(value);
      },
      type);
  }
  constexpr auto new_iterator(id op, auto v, word i){
    return new_scalar(v, codes::itype(emit.bin.code, op), i);
  }
  constexpr auto iterator(id op, auto v){
    return scalar(v, codes::itype(emit.bin.code, op));
  }

  word scalar(id op){
    return with_etype(
      [&](auto s, auto w) -> word{
        auto c = codes::csteval(s, emit.bin.code, op).value();
        if constexpr(s == set::booleans && w == 1)
          return c ? true_ : false_;
        else if constexpr(codes::has_cell<s(), w()>)
          return scalar<codes::type{s(), w()}>(static_cast<cell<s(), w()>>(c));
        else
          throw std::runtime_error("invalid type");
      },
      emit.bin.code,
      op);
  }

  word cast(codes::type target_type,
            codes::type source_type,
            word source){

    HYSJ_ASSERT(source != null);

    if(target_type == source_type)
      return source;

    auto target_type_id = types.scalar(target_type);
    auto source_type_id = types.scalar(source_type);

    word target = emit.new_id();

    auto [target_field, target_width] = target_type;
    auto [source_field, source_width] = source_type;

    if(target_field == set::booleans && target_width == 1){

      auto compare = [&]{
        if(codes::is_integral(source_field))
          return instructions::INotEqual;
        else{
          HYSJ_ASSERT(source_field == set::reals);
          return instructions::FOrdNotEqual;
        }
      }();

      auto zero = codes::with_type(
        [&](auto source_field, auto source_width){
          return scalar<codes::type{source_field,source_width}>(0);
        },
        source_type);

      emit.insn(section::functions,
                compare, 5,
                target_type_id, target, source, zero);
    }
    else if(source_field == set::booleans && source_width == 1){

      auto [zero,one] = codes::with_type(
        [&](auto target_field,auto target_width){
          return kumi::make_tuple(
            scalar<codes::type{target_field(), target_width()}>(0),
            scalar<codes::type{target_field(), target_width()}>(1));
        },
        target_type);

      emit.insn(section::functions,
                instructions::Select, 6,
                target_type_id, target, source, one, zero);

    }
    else{

      if(target_width != source_width){

        auto resize = [&]{
          if(codes::is_integral(source_field))
            return codes::is_signed(source_field) ? instructions::SConvert : instructions::UConvert;
          else{
            HYSJ_ASSERT(source_field == set::reals);
            return instructions::FConvert;
          }
        }();

        source_type_id = types.scalar(codes::type{source_field, target_width});

        auto resized_source = emit.new_id();
        emit.insn(section::functions,
                  resize, 4,
                  source_type_id, resized_source, source);
        source = resized_source;
      }

      if(target_field == source_field)
        return source;

      auto convert = [&] -> word {
        if(codes::is_integral(source_field)){
          if(target_field != set::reals)
            return instructions::Bitcast;
          else{
            HYSJ_ASSERT(target_field == set::reals);
            return codes::is_signed(source_field) ? instructions::ConvertSToF : instructions::ConvertUToF;
          }
        }
        else{
          HYSJ_ASSERT(source_field == set::reals);
          return codes::is_signed(target_field) ? instructions::ConvertFToS : instructions::ConvertFToU;
        }
      }();

      emit.insn(section::functions,
                convert, 4,
                target_type_id, target, source);

    }
    return target;
  }

};

struct value{
  codes::type type;
  word id;
};

struct builtins{

  sections &emit;
  codegen::types &types;

  word local_invocation_index;

  friend HYSJ_MIRROR_STRUCT(builtins,(local_invocation_index));

  auto ids()const{
    return kumi::apply(
      [](auto... member){
        return std::to_array<word>({member...});
      },
      members(*this));
  }

  auto pointer(grammars::id op,concepts::forward_words auto &&access_index_asm)const{
    if(op == emit.bin.kernel.thread.id())
      return just(codegen::value{thread_type, local_invocation_index});
    return no<codegen::value>;
  }

};

struct mem{

  sections &emit;

  codegen::types &types;
  codegen::scalars &scalars;

  std::vector<codegen::buffer> buffers;

  auto buffer(id op)const{
    auto buffer_it = std::ranges::find_if(buffers, [&](const auto &b){ return b.op == codes::expr(op); });
    if(buffer_it == std::ranges::end(buffers))
      return no<codegen::buffer>;
    return just(*buffer_it);
  }
  auto pointer(id op, concepts::forward_words auto &&access_index_asm)const{
    return
      accessed(emit.bin.code, op)
      .and_then(
        [&](auto op){
          return buffer(op);
        })
      .transform(
        [&](const auto &buffer){
          auto pointer_asm = emit.new_id(fmt::format("{}_ptr", debug_name(op)));
          emit.access(
            types.pointer(buffer.type, storage_class::StorageBuffer).id,
            pointer_asm,
            buffer.id,
            views::cat(views::one(buffer.member), fwd(access_index_asm)));
          return value{buffer.type, pointer_asm};
        });
  }

  auto accesses()const{
    auto A = ranges::vec(
      emit.bin.mem.accesses.buffers(emit.bin.code.builtins.host),
      [&](grammars::id a){
        return buffer(codes::expr(accessed(emit.bin.code, a).value())).value().id;
      });
    ranges::vecset(A);
    return A;
  }
};

struct statics{

  codegen::sections &emit;

  codegen::imports imports{emit};
  codegen::types types{emit};
  codegen::scalars scalars{emit, types};
  codegen::builtins builtins{emit, types};
  codegen::mem mem{emit, types, scalars};

  auto array_type(const spirv::buffer &buf){
    return types.array(
      buf.type,
      scalars.iterator(buf.op, buf.extent),
      fmt::format("{}{}[{}]", enumerator_name(buf.type.set)[0], buf.type.width,
                  fmt::join(oexts(emit.bin.code, buf.op),",")));
  }

};

inline void assemble(statics &statics){
  auto &emit = statics.emit;
  {
    kumi::for_each(
      [&](auto &m){
        m.id = emit.new_id();
        emit.insn(section::extensions,
                  instructions::ExtInstImport, 2 + emit.str_size(m.name),
                  m.id);
        emit.str(section::extensions,m.name);
      },
      member_refs(statics.imports));
  }
  {
    auto &types = statics.types;
    types.void_ = emit.new_id();
    types.kernel = emit.new_id("kernel");
    types.bool_ = types.scalar(bool_type);

    emit.insn(section::statics,
              instructions::TypeVoid, 2,
              types.void_);
    types.scalar(index_type);
    types.scalar(case_type);
    emit.insn(section::statics,
              instructions::TypeFunction, 3,
              types.kernel,
              types.void_);

  }
  {
    auto &scalars = statics.scalars;
    scalars.false_ = scalars.scalar(false, bool_type);
    scalars.true_ = scalars.scalar(true, bool_type);
  }
  {
    auto &types = statics.types;
    auto &builtins = statics.builtins;
    builtins.local_invocation_index = emit.new_id();
    emit.insn(section::annotations,
              instructions::Decorate, 4,
              builtins.local_invocation_index, decoration::BuiltIn,
              builtin::LocalInvocationIndex);
    types.variable(section::statics,
                   types.pointer(types.scalar(thread_type), storage_class::Input),
                   builtins.local_invocation_index);
  }
  {
    auto &bin = emit.bin;
    auto &mem  = statics.mem;
    auto &types = statics.types;
    auto &scalars = statics.scalars;
    for(const auto &buffer:bin.mem.buffers){
      auto &buffer_asm = mem.buffers.emplace_back(buffer);
      buffer_asm.member = scalars.new_iterator(buffer.op, 0, emit.new_id());
      auto extent_asm = scalars.new_iterator(buffer.op, buffer.extent, emit.new_id());
      auto value_type = obtype(bin.code, buffer.op);
      buffer_asm.member_type =
        types.array(value_type,
                    extent_asm,
                    fmt::format("{}_buffer", debug_name(buffer.op)));
      types.pointer(value_type, storage_class::StorageBuffer);
      buffer_asm.extents =
        ranges::vec(
          oexts(bin.code, buffer.op),
          [&](auto extent){
            return scalars.iterator(buffer.op, static_cast<integer>(extent));
          });

      std::ranges::fold_left(
        std::views::reverse(oexts(bin.code, buffer.op)),
        icell<>{1},
        [&](icell<> stride,auto extent){
          buffer_asm.strides.push_back(scalars.iterator(buffer.op, stride));
          return stride * extent;
        });
      std::ranges::reverse(buffer_asm.strides);
      buffer_asm.id = types.block(buffer_asm);
    }
  }
}

inline std::optional<value> assemble_pointer(statics &statics, codes::exprfam a, concepts::forward_words auto &&i){
  return statics.mem.pointer(a, i).or_else(
    [&]{
      return statics.builtins.pointer(a, i);
    });
}


} //hysj::binaries::spirv
#include<hysj/tools/epilogue.hpp>
