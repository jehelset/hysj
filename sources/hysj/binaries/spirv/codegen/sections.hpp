#pragma once

#include<array>
#include<algorithm>
#include<concepts>
#include<cstdint>
#include<ranges>
#include<string_view>
#include<utility>
#include<vector>

#include<hysj/binaries/spirv/binary.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::binaries::spirv::codegen{

constexpr word
  null = 0,
  magic_number = 0x07230203,
  version = 0x00010600,
  generator_magic_number = 0x19880202;

namespace concepts{
  template<typename T>
  concept input_words = ranges::concepts::convertible_input_range<T,word>;
  template<typename T>
  concept forward_words = ranges::concepts::convertible_forward_range<T,word>;
  template<typename T>
  concept random_access_words = ranges::concepts::convertible_random_access_range<T,word>;
}

#define HYSJ_SPIRV_EXECUTION_MODEL \
  (Vertex,0)                       \
  (TessellationControl,1)          \
  (TessellationEvaluation,2)       \
  (Geometry,3)                     \
  (Fragment,4)                     \
  (GLCompute,5)                    \
  (Kernel,6)                       \
  (Task,5267)                      \
  (Mesh,5268)                      \
  (RayGeneration,5313)             \
  (Interserction,5314)             \
  (AnyHit,5315)                    \
  (ClosestHit,5316)                \
  (Miss,5317)                      \
  (Callable,5318)

namespace execution_model{
  #define HYSJ_LAMBDA(id,value,...) constexpr word id = value;
  HYSJ_MAP_LAMBDA(HYSJ_SPIRV_EXECUTION_MODEL)
  #undef HYSJ_LAMBDA
}

#define HYSJ_SPIRV_ADDRESSING_MODEL \
  (Logical,0)                       \
  (Physical32,1)                    \
  (Physical64,2)                    \
  (PhysicalStorageBuffer64,5348)

namespace addressing_model{
  #define HYSJ_LAMBDA(id,value,...) constexpr word id = value;
  HYSJ_MAP_LAMBDA(HYSJ_SPIRV_ADDRESSING_MODEL)
  #undef HYSJ_LAMBDA
}

#define HYSJ_SPIRV_MEMORY_MODEL \
  (Simple,0)                    \
  (GLSL450,1)                   \
  (OpenCL,2)                    \
  (Vulkan,3)

enum class memory_model : word {
  HYSJ_SPLAT(0,HYSJ_SPIRV_MEMORY_MODEL)
};
HYSJ_MIRROR_ENUM(memory_model,HYSJ_SPIRV_MEMORY_MODEL)

#define HYSJ_SPIRV_EXECUTION_MODE \
  (Invocation,0)                  \
  (SpacingEqual,1)                \
  (SpacingFractionalEven,2)       \
  (SpacingFractionalOdd,3)        \
  (VertexOrderCw,4)               \
  (VertexOrderCcw,5)              \
  (PixelCenterInteger,6)          \
  (OriginUpperLeft,7)             \
  (OriginLowerLeft,8)             \
  (EarlyFragmentTest,9)           \
  (PointMode,10)                  \
  (Xfb,11)                        \
  (DepthReplacing,12)             \
  (DepthGreater,14)               \
  (DepthLess,15)                  \
  (DepthUnchanged,16)             \
  (LocalSize,17)                  \
  (LocalSizeHint,18)              \
  (InputPoints,19)                \
  (InputLines,20)                 \
  (InputLinesAdjacency,21)        \
  (Triangles,22)                  \
  (InputTrianglesAdjacency,23)    \
  (Quads,24)                      \
  (Isolines,25)                   \
  (OutputVertices,26)             \
  (OutputPoints,27)               \
  (OutputLineStrip,28)            \
  (OutputTriangleStrip,29)        \
  (VecTypeHint,30)                \
  (ContractionOff,31)             \
  (Initializer,33)                \
  (Finalizer,34)                  \
  (SubgroupSize,35)               \
  (SubgroupsPerWorkgroup,36)      \
  (SubgroupsPerWorkgroupId,37)    \
  (LocalSizeId,38)                \
  (LocalSizeHintId,39)

namespace execution_mode{
  #define HYSJ_LAMBDA(id,value,...) constexpr word id = value;
  HYSJ_MAP_LAMBDA(HYSJ_SPIRV_EXECUTION_MODE)
  #undef HYSJ_LAMBDA
}

#define HYSJ_SPIRV_STORAGE_CLASS \
  (UniformConstant,0)            \
  (Input,1)                      \
  (Uniform,2)                    \
  (Output,3)                     \
  (Workgroup,4)                  \
  (CrossWorkgroup,5)             \
  (Private,6)                    \
  (Function,7)                   \
  (Generic,8)                    \
  (PushConstant,9)               \
  (AtomicCounter,10)             \
  (Image,11)                     \
  (StorageBuffer,12)             \
  (CallableData,5328)            \
  (IncomingCallableData,5329)    \
  (RayPayload,5338)              \
  (HitAttribute,5339)            \
  (IncomingRayPayload,5342)      \
  (ShaderRecordBuffer,5343)      \
  (PhysicalStorageBuffer,5349)

namespace storage_class{
  #define HYSJ_LAMBDA(id,value,...) constexpr word id = value;
  HYSJ_MAP_LAMBDA(HYSJ_SPIRV_STORAGE_CLASS)
  #undef HYSJ_LAMBDA
}
constexpr auto storage_class_num = HYSJ_SIZE(HYSJ_SPIRV_STORAGE_CLASS);

#define HYSJ_SPIRV_LINKAGE_TYPE \
  (Export,0)                    \
  (Import,1)                    \
  (LinkOnceODR,2)

enum class linkage_type : word {
  HYSJ_SPLAT(0,HYSJ_SPIRV_LINKAGE_TYPE)
};
HYSJ_MIRROR_ENUM(linkage_type,HYSJ_SPIRV_LINKAGE_TYPE)

#define HYSJ_SPIRV_DECORATION \
  (RelaxedPrecision,0)        \
  (SpecId,1)                  \
  (Block,2)                   \
  (BufferBlock,3)             \
  (RowMajor,4)                \
  (ColMajor,5)                \
  (ArrayStride,6)             \
  (MatrixStride,7)            \
  (GLSLShared,8)              \
  (GLSLPacked,9)              \
  (CPacked,10)                \
  (BuiltIn,11)                \
  (NoPerspective,13)          \
  (Flat,14)                   \
  (Patch,15)                  \
  (Centroid,16)               \
  (Sample,17)                 \
  (Invar64iant,18)            \
  (Restrict,19)               \
  (Aliased,20)                \
  (Volatile,21)               \
  (Constant,22)               \
  (Coherent,23)               \
  (NonWritable,24)            \
  (NonReadable,25)            \
  (Uniform,26)                \
  (UniformId,27)              \
  (SaturatedConversion,28)    \
  (Stream,29)                 \
  (Location,30)               \
  (Component,31)              \
  (Index,32)                  \
  (Binding,33)                \
  (DescriptorSet,34)          \
  (Offset,35)                 \
  (XfbBuffer,36)              \
  (XfbStride,37)              \
  (FuncParamAttr,38)          \
  (FPRoundingMode,39)         \
  (FPFastMathMode,40)         \
  (LinkageAttributes,41)      \
  (NoContraction,42)          \
  (InputAttachmentIndex,43)   \
  (Alignment,44)              \
  (MaxByteOffset,45)          \
  (AlignmentId,46)            \
  (MaxByteOffsetId,47)

namespace decoration{
  #define HYSJ_LAMBDA(id,value,...) constexpr word id = value;
  HYSJ_MAP_LAMBDA(HYSJ_SPIRV_DECORATION)
  #undef HYSJ_LAMBDA
}

#define HYSJ_SPIRV_BUILTIN       \
  (Position,0)                   \
  (PointSize,1)                  \
  (ClipDistance,3)               \
  (CullDistance,4)               \
  (VertexId,5)                   \
  (InstanceId,6)                 \
  (PrimitiveId,7)                \
  (InvocationId,8)               \
  (Layer,9)                      \
  (ViewportIndex,10)             \
  (TessLevelOuter,11)            \
  (TessLevelInner,12)            \
  (TessCoord,13)                 \
  (PatchVertices,14)             \
  (FragCoord,15)                 \
  (PointCoord,16)                \
  (FrontFacing,17)               \
  (SampleId,18)                  \
  (SamplePosition,19)            \
  (SampleMask,20)                \
  (FragDepth,22)                 \
  (HelperInvocation,23)          \
  (NumWorkgroups,24)             \
  (WorkgroupSize,25)             \
  (WorkgroupId,26)               \
  (LocalInvocationId,27)         \
  (GlobalInvocationId,28)        \
  (LocalInvocationIndex,29)      \
  (WorkDim,30)                   \
  (GlobalSize,31)                \
  (EnqueuedWorkgroupSize,32)     \
  (GlobalOffset,33)              \
  (GlobalLinearId,34)            \
  (SubgroupSize,36)              \
  (SubgroupMaxSize,37)           \
  (NumSubgroups,38)              \
  (NumEnqueuedSubgroups,39)      \
  (SubgroupId,40)                \
  (SubgroupLocalInvocationId,41) \
  (VertexIndex,42)               \
  (InstanceIndex,43)

namespace builtin{
  #define HYSJ_LAMBDA(id,value,...) constexpr word id = value;
  HYSJ_MAP_LAMBDA(HYSJ_SPIRV_BUILTIN)
  #undef HYSJ_LAMBDA
}

#define HYSJ_SPIRV_FUNCTION_CONTROL \
  (None,0x0)                        \
  (Inline,0x1)                      \
  (DontInline,0x2)                  \
  (Pure,0x4)                        \
  (Const,0x8)

namespace function_control{
  #define HYSJ_LAMBDA(id,value,...) constexpr word id = value;
  HYSJ_MAP_LAMBDA(HYSJ_SPIRV_FUNCTION_CONTROL)
  #undef HYSJ_LAMBDA
}

#define HYSJ_SPIRV_CAPABILITY            \
  (Matrix,0)                             \
  (Shader,1)                             \
  (Geometry,2)                           \
  (Tesselation,3)                        \
  (Addresses,4)                          \
  (Linkage,5)                            \
  (Kernel,6)                             \
  (Vector16,7)                           \
  (Float16Buffer,8)                      \
  (Float16,9)                            \
  (Float64,10)                           \
  (Int64,11)                             \
  (Int64Atomics,12)                      \
  (ImageBasics,13)                       \
  (ImageReadWrite,14)                    \
  (ImageMipmap,15)                       \
  (Pipes,17)                             \
  (Groups,18)                            \
  (DeviceEnqueue,19)                     \
  (LiteralSampler,20)                    \
  (AtomicStorage,21)                     \
  (Int16,22)                             \
  (TessellationPointSize,23)             \
  (GeometryPointSize,24)                 \
  (ImageGatherExtended,25)               \
  (StorageImageMultisample,27)           \
  (UniformBufferArrayDynamicIndexing,28) \
  (SampledImageArrayDynamicIndexing,29)  \
  (StorageBufferArrayDynamicIndexing,30) \
  (StorageImageArrayDynamicIndexing,31)  \
  (ClipDistance,32)                      \
  (CullDistance,33)                      \
  (ImageCubeArray,34)                    \
  (SampleRateShading,35)                 \
  (ImageRect,36)                         \
  (SampledRect,37)                       \
  (GenericPointer,38)                    \
  (Int8,39)                              \
  (InputAttachment,40)                   \
  (SparseResidency,41)                   \
  (MinLod,42)                            \
  (Sampled1D,43)                         \
  (Image1D,44)                           \
  (SampledCubeArray,45)                  \
  (SampledBuffer,46)                     \
  (ImageBuffer,47)                       \
  (ImageMSArray,48)                      \
  (StorageImageExtendedFormats,49)       \
  (ImageQuery,50)                        \
  (DerivativeControl,51)                 \
  (InterpolationFunction,52)             \
  (TransformFeedback,53)                 \
  (GeometryStreams,54)                   \
  (StorageImageReadWithoutFormat,55)     \
  (StorageImageWriteWithoutFormat,56)    \
  (MultiViewpoint,57)                    \
  (SubgroupDispatch,58)                  \
  (NamedBarrier,59)                      \
  (PipeStorage,60)                       \
  (GroupNonUniform,61)                   \
  (GroupNonUniformVote,62)               \
  (GroupNonUniformArithmetic,63)         \
  (GroupNonUniformBallot,64)             \
  (GroupNonUniformShuffle,65)            \
  (GroupNonUniformShuffleRelative,66)    \
  (GroupNonUniformClustered,67)          \
  (GroupNonUniformQuad,68)               \
  (ShaderLayer,69)                       \
  (ShaderViewportIndex,70)               \
  (UniformDecoration,71)                 \
  (VulkanMemoryModel,5345)               \
  (VulkanMemoryModelDeviceScope,5346)    \
  (PhysicalStorageBufferAddresses,5347)

namespace capability{
  #define HYSJ_LAMBDA(id,value,...) constexpr word id = value;
  HYSJ_MAP_LAMBDA(HYSJ_SPIRV_CAPABILITY)
  #undef HYSJ_LAMBDA
}

namespace instructions{

 constexpr auto encode(halfword i,halfword n){
   return (n << 16) | i;
 }

  #define HYSJ_SPIRV_INSTRUCTIONS_MISCELLANEOUS \
    (Nop,0)                                     \
    (Undef,1)                                   \
    (SizeOf,321)

  #define HYSJ_LAMBDA(id,value,...) constexpr halfword id = value;
  HYSJ_MAP_LAMBDA(HYSJ_SPIRV_INSTRUCTIONS_MISCELLANEOUS)
  #undef HYSJ_LAMBDA

  #define HYSJ_SPIRV_INSTRUCTIONS_DEBUG         \
    (SourceContinued, 2)                        \
    (Source, 3)                                 \
    (SourceExtension, 4)                        \
    (Name, 5)                                   \
    (MemberName, 6)                             \
    (String, 7)                                 \
    (Line, 8)                                   \
    (NoLine, 317)                               \
    (ModuleProcessed, 330)

  #define HYSJ_LAMBDA(id,value,...) constexpr halfword id = value;
  HYSJ_MAP_LAMBDA(HYSJ_SPIRV_INSTRUCTIONS_DEBUG)
  #undef HYSJ_LAMBDA

  #define HYSJ_SPIRV_INSTRUCTIONS_ANNOTATION    \
    (Decorate,71)                               \
    (MemberDecorate,72)                         \
    (DecorationGroup,73)                        \
    (GroupDecorate,74)                          \
    (GroupMemberDecorate,75)                    \
    (DecorateId,332)

  #define HYSJ_LAMBDA(id,value,...) constexpr halfword id = value;
  HYSJ_MAP_LAMBDA(HYSJ_SPIRV_INSTRUCTIONS_ANNOTATION)
  #undef HYSJ_LAMBDA

  #define HYSJ_SPIRV_INSTRUCTIONS_EXTENSION     \
    (Extension,10)                              \
    (ExtInstImport,11)                          \
    (ExtInst,12)

  #define HYSJ_LAMBDA(id,value,...) constexpr halfword id = value;
  HYSJ_MAP_LAMBDA(HYSJ_SPIRV_INSTRUCTIONS_EXTENSION)
  #undef HYSJ_LAMBDA

  #define HYSJ_SPIRV_INSTRUCTIONS_MODE_SETTING  \
    (MemoryModel,14)                            \
    (EntryPoint,15)                             \
    (ExecutionMode,16)                          \
    (Capability,17)                             \
    (ExecutionModeId,331)

  #define HYSJ_LAMBDA(id,value,...) constexpr halfword id = value;
  HYSJ_MAP_LAMBDA(HYSJ_SPIRV_INSTRUCTIONS_MODE_SETTING)
  #undef HYSJ_LAMBDA

  #define HYSJ_SPIRV_INSTRUCTIONS_TYPE_DECLARATIONS \
    (TypeVoid, 19)                                  \
    (TypeBool, 20)                                  \
    (TypeInt, 21)                                   \
    (TypeFloat, 22)                                 \
    (TypeVector, 23)                                \
    (TypeMatrix, 24)                                \
    (TypeImage, 25)                                 \
    (TypeSampler, 26)                               \
    (TypeSampledImage, 27)                          \
    (TypeArray, 28)                                 \
    (TypeRuntimeArray, 29)                          \
    (TypeStruct, 30)                                \
    (TypeOpaque, 31)                                \
    (TypePointer, 32)                               \
    (TypeFunction, 33)                              \
    (TypeEvent, 34)                                 \
    (TypeDeviceEvent, 35)                           \
    (TypeReserveId, 36)                             \
    (TypeQueue, 37)                                 \
    (TypePipe, 38)                                  \
    (TypeForwardPointer, 39)                        \
    (TypePipeStorage, 40)                           \
    (TypeNamedBarrier, 41)

  #define HYSJ_LAMBDA(id,value,...) constexpr halfword id = value;
  HYSJ_MAP_LAMBDA(HYSJ_SPIRV_INSTRUCTIONS_TYPE_DECLARATIONS)
  #undef HYSJ_LAMBDA

  #define HYSJ_SPIRV_INSTRUCTIONS_CONSTANT_CREATION \
    (ConstantTrue, 41)                              \
    (ConstantFalse, 42)                             \
    (Constant, 43)                                  \
    (ConstantComposite, 44)                         \
    (ConstantSampler, 45)                           \
    (ConstantNull, 46)                              \
    (SpecConstantTrue, 48)                          \
    (SpecConstantFalse, 49)                         \
    (SpecConstant, 50)                              \
    (SpecConstantComposite, 51)                     \
    (SpecConstantOp, 51)

  #define HYSJ_LAMBDA(id,value,...) constexpr halfword id = value;
  HYSJ_MAP_LAMBDA(HYSJ_SPIRV_INSTRUCTIONS_CONSTANT_CREATION)
  #undef HYSJ_LAMBDA

  #define HYSJ_SPIRV_INSTRUCTIONS_MEMORY        \
    (Variable, 59)                              \
    (ImageTexelPointer, 60)                     \
    (Load, 61)                                  \
    (Store, 62)                                 \
    (CopyMemory, 63)                            \
    (CopyMemorySized, 64)                       \
    (AccessChain, 65)                           \
    (InBoundsAccessChain, 66)                   \
    (PtrAccessChain, 67)                        \
    (ArrayLength, 68)                           \
    (GenericPtrMemSemantics, 69)                \
    (InBoundsPtrAccessChain, 70)                \
    (OpPtrEqual, 401)                           \
    (OpPtrNotEqual, 402)                        \
    (OpPtrDiff, 403)

  #define HYSJ_LAMBDA(id,value,...) constexpr halfword id = value;
  HYSJ_MAP_LAMBDA(HYSJ_SPIRV_INSTRUCTIONS_MEMORY)
  #undef HYSJ_LAMBDA

  #define HYSJ_SPIRV_INSTRUCTIONS_FUNCTIONS     \
    (Function, 54)                              \
    (FunctionParameter, 55)                     \
    (FunctionEnd, 56)                           \
    (FunctionCall, 57)

  #define HYSJ_LAMBDA(id,value,...) constexpr halfword id = value;
  HYSJ_MAP_LAMBDA(HYSJ_SPIRV_INSTRUCTIONS_FUNCTIONS)
  #undef HYSJ_LAMBDA

  #define HYSJ_SPIRV_INSTRUCTIONS_CONVERSION    \
    (ConvertFToU,109)                           \
    (ConvertFToS,110)                           \
    (ConvertSToF,111)                           \
    (ConvertUToF,112)                           \
    (UConvert,113)                              \
    (SConvert,114)                              \
    (FConvert,115)                              \
    (QuantizeToF16,116)                         \
    (ConvertPtrToU,117)                         \
    (SatConvertSToU,118)                        \
    (SatConvertUToS,119)                        \
    (ConvertUToPtr,120)                         \
    (PtrCastToGeneric,121)                      \
    (GenericCastToPtr,122)                      \
    (GenericCastToPtrExplicit,123)              \
    (Bitcast,124)

  #define HYSJ_LAMBDA(id,value,...) constexpr halfword id = value;
  HYSJ_MAP_LAMBDA(HYSJ_SPIRV_INSTRUCTIONS_CONVERSION)
  #undef HYSJ_LAMBDA

  #define HYSJ_SPIRV_INSTRUCITONS_COMPOSITE     \
    (VectorExtractDynamic)                      \
    (VectorInsertDynamic)                       \
    (VectorShuffle)                             \
    (CompositeConstruct)                        \
    (CompositeExtract)                          \
    (CompositeInsert)                           \
    (CopyObject)                                \
    (Transpose)

  #define HYSJ_SPIRV_INSTRUCTIONS_ARITHMETIC    \
    (SNegate,126)                               \
    (FNegate,127)                               \
    (IAdd,128)                                  \
    (FAdd,129)                                  \
    (ISub,130)                                  \
    (FSub,131)                                  \
    (IMul,132)                                  \
    (FMul,133)                                  \
    (UDiv,134)                                  \
    (SDiv,135)                                  \
    (FDiv,136)                                  \
    (UMod,137)                                  \
    (SRem,138)                                  \
    (SMod,139)                                  \
    (FRem,140)                                  \
    (FMod,141)                                  \
    (VectorTimesScalar,142)                     \
    (MatrixTimesScalar,143)                     \
    (VectorTimesMatrix,144)                     \
    (MartixTimesVector,145)                     \
    (MatrixTimesMartix,146)                     \
    (OuterProduct,147)                          \
    (Dot,148)                                   \
    (IAddCarry,149)                             \
    (ISubBorrow,150)                            \
    (UMulExtended,151)                          \
    (SMulExtended,152)                          \
    (SDot,4450)                                 \
    (UDot,4451)                                 \
    (SUDot,4452)                                \
    (SDotAccSat,4453)                           \
    (UDotAccSat,4454)                           \
    (SUDotAccSat,4455)

  #define HYSJ_LAMBDA(id,value,...) constexpr halfword id = value;
  HYSJ_MAP_LAMBDA(HYSJ_SPIRV_INSTRUCTIONS_ARITHMETIC)
  #undef HYSJ_LAMBDA

  #define HYSJ_SPIRV_INSTRUCTIONS_BIT           \
    (ShiftRightLogical,194)                     \
    (ShiftRightArithmetic,195)                  \
    (ShiftLeftLogical,196)                      \
    (BitwiseOr,197)                             \
    (BitwiseXor,198)                            \
    (BitwiseAnd,199)                            \
    (Not,200)                                   \
    (BitFieldInsert,201)                        \
    (BitFieldSExtract,202)                      \
    (BitFieldUExtract,203)                      \
    (BitReverse,204)                            \
    (BitCount,205)

  #define HYSJ_LAMBDA(id,value,...) constexpr halfword id = value;
  HYSJ_MAP_LAMBDA(HYSJ_SPIRV_INSTRUCTIONS_BIT)
  #undef HYSJ_LAMBDA

  #define HYSJ_SPIRV_INSTRUCTIONS_RELATIONAL    \
    (Any,154)                                   \
    (All,155)                                   \
    (IsNan,156)                                 \
    (IsInf,157)                                 \
    (IsFinite,158)                              \
    (IsNormal,159)                              \
    (SignBitSet,160)                            \
    (LessOrGreater,161)                         \
    (Ordered,162)                               \
    (Unordered,163)                             \
    (LogicalEqual,164)                          \
    (LogicalNotEqual,165)                       \
    (LogicalOr,166)                             \
    (LogicalAnd,167)                            \
    (LogicalNot,168)                            \
    (Select,169)                                \
    (IEqual,170)                                \
    (INotEqual,171)                             \
    (UGreaterThan,172)                          \
    (SGreaterThan,173)                          \
    (UGreaterThanEqual,174)                     \
    (SGreaterThanEqual,175)                     \
    (ULessThan,176)                             \
    (SLessThan,177)                             \
    (ULessThanEqual,178)                        \
    (SLessThanEqual,179)                        \
    (FOrdEqual,180)                             \
    (FUnorEqual,181)                            \
    (FOrdNotEqual,182)                          \
    (FUnordNotEqual,183)                        \
    (FOrdLessThan,184)                          \
    (FUnordLessThan,185)                        \
    (FOrdGreaterThan,186)                       \
    (FUnordGreaterThan,187)                     \
    (FOrdLessThanEqual,188)                     \
    (FUnordLessThanEqual,189)                   \
    (FOrdGreaterThanEqual,190)                  \
    (FUnordGreaterThanEqual,191)

  #define HYSJ_LAMBDA(id,value,...) constexpr halfword id = value;
  HYSJ_MAP_LAMBDA(HYSJ_SPIRV_INSTRUCTIONS_RELATIONAL)
  #undef HYSJ_LAMBDA

  #define HYSJ_SPIRV_INSTRUCTIONS_CONTROL_FLOW  \
    (Phi, 245)                                  \
    (LoopMerge, 246)                            \
    (SelectionMerge, 247)                       \
    (Label, 248)                                \
    (Branch, 249)                               \
    (BranchConditional, 250)                    \
    (Switch, 251)                               \
    (Kill, 252)                                 \
    (Return, 253)                               \
    (ReturnValue, 254)                          \
    (Unreachable, 255)                          \
    (LifetimeStart, 256)                        \
    (LifetimeStop, 257)                         \
    (Terminate, 4416)

  #define HYSJ_LAMBDA(id,value,...) constexpr halfword id = value;
  HYSJ_MAP_LAMBDA(HYSJ_SPIRV_INSTRUCTIONS_CONTROL_FLOW)
  #undef HYSJ_LAMBDA

  #define HYSJ_SPIRV_INSTRUCTIONS_BARRIER  \
    (ControlBarrier, 224)                  \
    (MemoryBarrier, 225)                   \
    (NamedBarrierInitialize, 328)          \
    (MemoryNamedBarrier, 329)

  #define HYSJ_LAMBDA(id,value,...) constexpr halfword id = value;
  HYSJ_MAP_LAMBDA(HYSJ_SPIRV_INSTRUCTIONS_BARRIER)
  #undef HYSJ_LAMBDA

  #define HYSJ_SPIRV_INSTRUCTIONS_GLSL_STD_450  \
    (FAbs,4)                                    \
    (Sin,13)                                    \
    (Pow,26)                                    \
    (Exp,27)                                    \
    (Log,28)                                    \
    (FMax,41)

  #define HYSJ_LAMBDA(id,value,...) constexpr word GLSL_std_450_##id = value;
  HYSJ_MAP_LAMBDA(HYSJ_SPIRV_INSTRUCTIONS_GLSL_STD_450)
  #undef HYSJ_LAMBDA


  #define HYSJ_SPIRV_INSTRUCTIONS_SCOPES \
    (CrossDevice,0)                      \
    (Device,1)                           \
    (Workgroup,2)                        \
    (Subgroup,3)                         \
    (Invocation,4)                       \
    (QueueFamily,5)                      \
    (ShaderCallKHR,6)

  namespace scopes{
    #define HYSJ_LAMBDA(id,value,...) constexpr word id = value;
    HYSJ_MAP_LAMBDA(HYSJ_SPIRV_INSTRUCTIONS_SCOPES)
    #undef HYSJ_LAMBDA
  }

  #define HYSJ_SPIRV_INSTRUCTIONS_MEMORY_SEMANTICS \
    (None, 0x0)                                    \
    (Acquire, 0x2)                                 \
    (Release, 0x4)                                 \
    (AcquireRelease, 0x8)                          \
    (SequentiallyConsistent, 0x10)                 \
    (UniformMemory, 0x40)                          \
    (SubgroupMemory, 0x80)                         \
    (WorkgroupMemory, 0x100)                       \
    (CrossWorkgroupMemory, 0x200)                  \
    (AtomicCounterMemory, 0x400)                   \
    (ImageMemory, 0x800)                           \
    (OutputMemory, 0x1000)                         \
    (MakeAvailable, 0x2000)                        \
    (MakeVisible, 0x4000)                          \
    (Volatile, 0x8000)

  namespace memory_semantics{
    #define HYSJ_LAMBDA(id,value,...) constexpr word id = value;
    HYSJ_MAP_LAMBDA(HYSJ_SPIRV_INSTRUCTIONS_MEMORY_SEMANTICS)
    #undef HYSJ_LAMBDA
  }

}

#define HYSJ_BINARIES_SPIRV_SECTIONS \
  (prologue)                         \
  (capabilities)                     \
  (extensions)                       \
  (memory_model)                     \
  (entry_points)                     \
  (execution_modes)                  \
  (debug)                            \
  (annotations)                      \
  (statics)                          \
  (functions)

enum class section{
  HYSJ_SPLAT(0,HYSJ_BINARIES_SPIRV_SECTIONS)
};
HYSJ_MIRROR_ENUM(section,HYSJ_BINARIES_SPIRV_SECTIONS)

constexpr auto section_num  = enumerator_count<section>;

struct sections{

  const spirv::env &env;
  spirv::bin &bin;

  word id = 1;
  std::array<std::vector<word>,section_num> container = {};

  static constexpr auto str_size(std::string_view L){
    auto n = std::ranges::size(L) + 1;
    return (n + (4 - 1)) / 4;
  }
  constexpr void str(section s,std::string_view L){
    std::array<unsigned char,4> pack{};
    std::size_t i = 0;
    auto emit_pack = [&]{
      words(s, *reinterpret_cast<word *>(pack.data()));
    };
    auto emit_char = [&](char c){
      pack[i++] = c;
      if(i %= 4)
        return false;
      emit_pack();
      return true;
    };

    for(auto c:L)
      emit_char(c);
    if(!emit_char('\0'))
      while(!emit_char({}));
  }
  template<typename T>
  static constexpr auto lit_size(){
    return sizeof(T) <= sizeof(word) ? 1 : (sizeof(T) + sizeof(word) - 1) / sizeof(word);
  }
  template<typename T>
  static constexpr auto lit_size(const T &){
    return lit_size<T>();
  }
  template<typename T>
  constexpr void lit(section s,T l){
    if constexpr(sizeof(T) < sizeof(word)){

      //FIXME: not good - jeh
      union pun
      {
        std::byte b[sizeof(T)];
        T t;
      };
      pun p;
      p.t = std::move(l);

      std::array<std::byte, sizeof(word)> w{};

      for(auto i = 0;i != sizeof(T); ++i)
        w[i] = p.b[i];

      const std::byte f = [&]{
        if constexpr(std::signed_integral<T>)
          if(l < 0)
            return ~std::byte{};
        return std::byte{};
      }();
      for(auto i = sizeof(T);i != sizeof(word); ++i)
        w[i] = f;
      words(s,*reinterpret_cast<const word *>(w.data()));
    }
    else if constexpr(sizeof(T) == sizeof(word))
      words(s,l);
    else{
      static_assert((sizeof(T) % sizeof(word)) == 0);
      constexpr auto N = lit_size(l);
      auto l_ptr = reinterpret_cast<const word *>(&l);
      [&]<std::size_t... i>(std::index_sequence<i...>){
        words(s,l_ptr[i]...);
      }(std::make_index_sequence<N>{});
    }
  }

  constexpr word debug_name(word id,std::string_view name){
    HYSJ_ASSERT(id != null);

    auto name_size = str_size(name);
    insn(section::debug,
         instructions::Name,2 + name_size,id);
    str(section::debug,name);
    return id;
  }
  constexpr word debug_name(word id,grammars::id op){
    return debug_name(id, spirv::debug_name(op));
  }
  constexpr void debug_member_name(word struct_,word member,std::string_view name){
    HYSJ_ASSERT(struct_ != null);

    auto name_size = str_size(name);
    insn(section::debug,instructions::MemberName,3 + name_size,struct_);
    lit(section::debug,member);
    str(section::debug,name);
  }

  constexpr auto new_id(std::string_view name = {}){
    auto id_ = id++;
    if(!name.empty())
      debug_name(id_, name);
    return id_;
  }
  constexpr auto &operator[](section s,std::size_t i){
    return container[etoi(s)][i];
  }
  constexpr auto &operator[](section s){
    return container[etoi(s)];
  }

  constexpr void words(section s,auto... w){
    (container[etoi(s)].push_back(std::bit_cast<word>(w)),...);
  }
  constexpr void words(section s,concepts::input_words auto &&W){
    std::ranges::copy(
      fwd(W),
      std::back_inserter(container[etoi(s)]));
  }
  constexpr void insn(section s,halfword i,halfword n,auto... b){
    HYSJ_ASSERT(n != null);
    words(s,instructions::encode(i,n),b...);
  }

  constexpr void cap(word c){
    insn(section::capabilities,
         instructions::Capability, 2, c);
  }
  constexpr void caps(auto... w){
    (cap(w), ...);
  }

  constexpr word &ensure_id(word &id){
    if (id == null)
      id = new_id();
    return id;
  }

  constexpr auto load(word type, word var,word id = null){
    insn(section::functions,
         instructions::Load, 4,
         type, ensure_id(id), var);
    return id;
  }
  constexpr auto store(word access, word source){
    insn(section::functions,
         instructions::Store, 3,
         access, source);
    return source;
  }

  constexpr auto branch(word id = null){
    insn(section::functions,
         instructions::Branch, 2,
         ensure_id(id));
    return id;
  }
  constexpr auto label(word id = null){
    insn(section::functions,
         instructions::Label, 2,
         ensure_id(id));
    return id;
  }

  constexpr auto nop(){
    insn(section::functions, instructions::Nop, 1);
  }
  constexpr auto loop_merge(word merge_block,
                            word continue_target,
                            word loop_control = null,
                            auto... loop_control_parameters){
    insn(section::functions,
         instructions::LoopMerge, 4 + sizeof...(loop_control_parameters),
         merge_block, continue_target, loop_control,
         loop_control_parameters...);
  }
  constexpr auto selection_merge(word merge_block,
                                 word selection_control = null){
    insn(section::functions,
         instructions::SelectionMerge, 3,
         merge_block, selection_control);
  }
  constexpr auto branch_conditional(
    word condition,
    word true_label,
    word false_label,
    auto... branch_weights){
    insn(section::functions,
         instructions::BranchConditional, 4 + sizeof...(branch_weights),
         condition, true_label, false_label, branch_weights...);
  }
  constexpr auto control_barrier(
    word control_scope,
    word memory_scope,
    word memory_semantics){
    insn(section::functions,
         instructions::ControlBarrier, 4,
         control_scope,
         memory_scope,
         memory_semantics);
  }
  constexpr auto entry_point(word function,
                             std::string_view name,
                             concepts::forward_words auto &&uses){
    insn(section::entry_points,
         instructions::EntryPoint,
         3
         + str_size(name)
         + std::ranges::size(uses),
         execution_model::GLCompute,
         function);
    str(section::entry_points, name);
    words(section::entry_points, uses);
    return function;
  }
  constexpr auto local_size(word function,
                            word size){
    insn(section::execution_modes,
         instructions::ExecutionMode, 6,
         function,
         execution_mode::LocalSize,
         static_cast<word>(size), 1, 1);
    return function;
  }
  constexpr auto function(word return_type,
                          word function_type,
                          word function = null,
                          word function_control = function_control::None){
    insn(section::functions,
         instructions::Function, 5,
         return_type,
         ensure_id(function),
         function_control,
         function_type);
    return function;
  }
  constexpr auto return_(){
    insn(section::functions, instructions::Return, 1);
  }
  constexpr auto function_end(){
    insn(section::functions, instructions::FunctionEnd, 1);
  }
  constexpr auto access(word result_type_asm,
                        word result_asm,
                        word base_asm,
                        concepts::forward_words auto &&access_index_asm){
    insn(section::functions,
         instructions::AccessChain, 4 + std::ranges::distance(access_index_asm),
         result_type_asm,
         result_asm,
         base_asm);
    words(section::functions, access_index_asm);
    return result_asm;
  }

};

inline void assemble_pre(sections &emit){
  emit[section::prologue] = {
    magic_number,
    version,
    generator_magic_number,
    word{0},
    word{0}
  };
  emit.caps(capability::Shader,
            capability::GroupNonUniform,
            capability::VulkanMemoryModel,
            capability::PhysicalStorageBufferAddresses);

  //FIXME: types(code, dev) - jeh
  emit.caps(capability::Int8);
  emit.caps(capability::Int16, capability::Float16);
  emit.caps(capability::Int64);
  emit.caps(capability::Float64);

  emit.insn(section::memory_model,
            instructions::MemoryModel, 3,
            addressing_model::PhysicalStorageBuffer64,memory_model::Vulkan);
}
inline void assemble_post(sections &emit){
  emit[section::prologue,3] = emit.id;
  auto image = apply(views::cat, emit.container);
  emit.bin.tape.resize(std::ranges::size(image));
  std::ranges::copy(image, emit.bin.tape.data());
}

} //hysj::binaries::spirv
#include<hysj/tools/epilogue.hpp>
