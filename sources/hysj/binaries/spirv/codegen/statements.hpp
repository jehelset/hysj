#pragma once

#include<optional>
#include<vector>

#include<kumi/tuple.hpp>

#include<hysj/binaries/spirv/binary.hpp>
#include<hysj/binaries/spirv/codegen/expressions.hpp>
#include<hysj/binaries/spirv/codegen/sections.hpp>
#include<hysj/binaries/spirv/codegen/statics.hpp>
#include<hysj/codes/code.hpp>
#include<hysj/codes/eval.hpp>
#include<hysj/codes/grammar.hpp>
#include<hysj/tools/functional/any_of.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::binaries::spirv::codegen{

template<codes::optag, std::size_t...>
struct statement{};


template<codes::optag O>
requires (O == any_of(codes::optag::loop))
struct statement<O>{
  codegen::unwind unwind;
};

template<codes::optag O>
requires (O == any_of(codes::optag::then, codes::optag::when))
struct statement<O>{
  std::vector<codegen::unwind> unwinds;
};

template<>
struct statement<codes::optag::fork>{
  id control_scope, memory_scope, memory_semantics;
};


struct statements{
  using container_type = reify(
    with_enumerators<codes::optag>(
      [](auto... o){
        return type_c<kumi::tuple<std::vector<std::optional<statement<o()>>>...>>;
      }));

  container_type container;

  template<codes::optag O>
  auto &operator[](sym<O> s){
    auto &S = kumi::get<etoi(O)>(container);
    if(S.size() <= s.idx)
      S.resize(s.idx + 1);
    return S.at(s.idx);
  }
  template<codes::optag O>
  auto &operator[](sym<O> s)const{
    auto &S = kumi::get<etoi(O)>(container);
    return S.at(s.idx);
  }
};

inline grammars::id build_walk(body &,statements &,expressions &,grammars::id);

template<codes::optag O>
statement<O> build(body &body,statements &stms,expressions &exprs,sym<O> o){

  statement<O> a;

  auto &emit = body.emit;
  auto &code = emit.bin.code;

  if constexpr(O == codes::optag::put){
    auto &kernel = emit.bin.kernel;
    auto thread = kernel.thread;
    auto thread_count = icst32(code, std::max(kernel.threads.local_count(o).value(), 1_i));
    auto L = codes::bulk(code, o, thread, {thread_count});
    auto b = build_assign(body, exprs, o, L);
    exprs[o] = std::move(b);
  }
  if constexpr(O == any_of(codes::optag::loop)){
    build(body, exprs, a.unwind);
    a.unwind.visible = true;
  }
  if constexpr(O == any_of(codes::optag::then, codes::optag::when)){
    a.unwinds.resize(grammars::argdeg(code, o));
    for(auto &u:a.unwinds){
      build(body, exprs, u);
      u.visible = false;
    }
  }
  if constexpr(O == codes::optag::fork){
    build_walk(body, exprs, arg(code, o, 0_N));
    a.memory_scope = a.control_scope = build_walk(body, exprs, codes::icst32(code, instructions::scopes::Subgroup));
    a.memory_semantics = build_walk(body, exprs, codes::icst32(code, instructions::memory_semantics::None));
  }
  return a;
}

inline void build_walk(body &body,statements &stmt, expressions &exprs, codes::taskfam root){
  auto &code = body.emit.bin.code;
  auto pre0 = [&](grammars::id o){
    return codes::with_op(
      [&](auto o){
        if constexpr(!codes::is_task(o.tag))
          return false;
        else{
          if(stmt[o])
            return false;
          else{
            auto a = build(body, stmt, exprs, o);
            stmt[o] = std::move(a);
          }
          return true;
        }
      },
      o);
  };
  auto pre1 = [&](grammars::id op,grammars::id rel,grammars::id arg){
    if(codes::reltag_of(rel) != codes::reltag::exe)
      return false;
    return pre0(arg);
  };
  grammars::walk<none>(
    code,
    overload(pre0, pre1),
    never,
    root.id());
}

template<codes::optag O>
bool assemble_pre(body &body, const statements &stmts, const expressions &exprs, sym<O> o){
  auto &emit = body.emit;
  const auto &a = stmts[o].value();

  if constexpr(O == codes::optag::put){
    assemble_assign(body, exprs, o);
    return false;
  }

  if constexpr(O == codes::optag::noop){
    emit.nop();
    return false;
  }
  if constexpr(O == codes::optag::loop){
    assemble_store(body, a.unwind.var, assemble(body, exprs, a.unwind.off).value());

    body.push_unwind(a.unwind);
    auto &&[header,continue_,merge,let] =
      body.down({
          emit.new_id(fmt::format("{}_header", debug_name(o))),
          emit.new_id(fmt::format("{}_continue", debug_name(o))),
          emit.new_id(fmt::format("{}_merge", debug_name(o))),
        });

    //FIXME: garb - jeh
    assemble_let(body, a.unwind.var);
    auto no_unwind = assemble(body, exprs, a.unwind.no_unwind).value();
    emit.debug_name(no_unwind, fmt::format("{}_unwind_flag", debug_name(o)));
    auto body = emit.new_id(fmt::format("{}_body", o));
    emit.loop_merge(*merge, body);
    emit.branch_conditional(no_unwind, body, *merge);
    emit.label(body);
  }

  if constexpr(O == codes::optag::exit){

    auto h = arg(emit.bin.code, o, 0_N);
    auto h_index = codes::ieval(emit.bin.code, h.id()).value();

    const auto H_visible = std::ranges::count_if(body.unwinds, &unwind::visible);
    if(h_index < 0) //-x -> x - 1
      h_index = -(h_index + 1);
    else //x -> n - max(x, 1) 
      h_index = (H_visible + 1) - std::max(h_index, 1_i);

    if(h_index > H_visible + 1)
      throw std::runtime_error("invalid exit");

    //ignore root scope
    if(h_index != 0)
      --h_index;

    const auto H = std::ranges::size(body.unwinds);

    auto H_exit = H_visible - h_index;
    for(auto i = 0;H_exit != 0;++i){
      auto &u = body.unwinds.at(H - (i + 1));
      assemble_store(body, u.var, assemble(body, exprs, u.on).value());
      H_exit -= u.visible;
    }
    return false;
  }

  if constexpr(O == codes::optag::fork){

    auto [pre,post,aux,let] = body.down();

    emit.debug_name(pre, fmt::format("{}_header", debug_name(o)));
    emit.debug_name(post, fmt::format("{}_merge", debug_name(o)));

    auto which = rel(emit.bin.code, o, 0_N);
    auto which_type = retype(emit.bin.code, which);

    assemble(body, exprs, grammars::relargid(emit.bin.code, which)).value();
    auto which_asm = body.rel(o, 0_N, which);
                                      
    auto spv_which_width = emit.lit_size<codes::cell_t<case_type>>();
    auto spv_which_asm = body.statics.scalars.cast(case_type, which_type, which_asm);
    emit.debug_name(spv_which_asm, fmt::format("{}_which", debug_name(o)));

    //FIXME: emit scope at rel instead of recurse - jeh
    auto cases = arg(emit.bin.code, o, 1_N);
    const auto case_num = std::ranges::size(cases);
    const auto case_labels = ranges::vec(
      views::indices(case_num),
      [&](auto i){ return emit.new_id(fmt::format("{}_case_{}", debug_name(o), i)); });
    emit.selection_merge(post);
    emit.insn(section::functions,
              instructions::Switch,
              3 + (1 + spv_which_width)*(case_num - 1),
              spv_which_asm, ranges::last(case_labels));

    for(auto [case_idx, case_label]:
          std::views::take(
            std::views::enumerate(case_labels),
            case_num - 1)){
      emit.lit(section::functions, static_cast<codes::cell_t<case_type>>(case_idx));
      emit.words(section::functions, case_label);
    }

    for(auto [case_label,case_]:std::views::zip(case_labels, cases)){
      emit.label(case_label);
      assemble(body, stmts, exprs, case_);
      emit.branch(post);
    }
    body.up(false);
    emit.control_barrier(
      assemble(body, exprs, a.control_scope).value(),
      assemble(body, exprs, a.memory_scope).value(),
      assemble(body, exprs, a.memory_semantics).value());
    body.unlet();
    return false;
  }

  if constexpr(O == any_of(codes::optag::then, codes::optag::when)){
    auto [U] = args(emit.bin.code, o);
    if(std::ranges::empty(U))
      return false;

    for(auto [i, u, unwind]:std::views::zip(views::indices(U), U, a.unwinds)){

      body.push_unwind(unwind);
      auto &&[header,merge,aux,let] = body.down();

      emit.debug_name(header,
                      fmt::format("{}_header_{}", debug_name(o), i));
      emit.debug_name(merge,
                      fmt::format("{}_merge_{}", debug_name(o), i));

      assemble_store(body, unwind.var, assemble(body, exprs, unwind.off).value());

      assemble(body, stmts, exprs, u);

      emit.debug_name(
        emit.label(emit.branch()),
        fmt::format("{}_unwind_{}", debug_name(o), i));

      assemble_let(body, unwind.var);
      auto no_unwind = assemble(body, exprs, unwind.no_unwind).value();

      emit.selection_merge(merge);
      auto body = emit.new_id(fmt::format("{}_body_{}", debug_name(o), i));
      emit.branch_conditional(no_unwind, body, merge);
      emit.label(body);
    }

    for(auto u:std::views::reverse(U)){
      body.up();
      body.pop_unwind();
    }

    return false;
  }
  return true;
}
template<codes::optag O>
void assemble_post(body &body, const statements &stmts, const expressions &exprs, sym<O> o){
  if constexpr(O == codes::optag::loop){
    auto &emit = body.emit;
    auto &&[header,continue_,merge,let] = body.up();
    body.pop_unwind();
    emit.branch(header);
    emit.label(*merge);
  }
}
inline void assemble(body &body, const statements &stmts, const expressions &exprs, grammars::id root){
  auto &code = body.emit.bin.code;
  std::vector<grammars::id> done;
  auto pre0 = [&](grammars::id o){
    if(ranges::contains(done, o))
      return false;
    if(codes::optag_of(o) != any_of(codes::optag::exit))
      done.push_back(o);
    return codes::with_op(
      [&](auto o){
        return assemble_pre(body, stmts, exprs, o);
      },
      o);
  };
  auto pre1 = [&](grammars::id op,grammars::id rel,grammars::id arg){
    if(codes::reltag_of(rel) != codes::reltag::exe)
      return false;
    return pre0(arg);
  };
  auto post0 = [&](auto... ev){
    codes::with_op(
      [&](auto o){
        assemble_post(body, stmts, exprs, o);
      },
      pick<-1>(ev...));
    return none{};
  };
  grammars::walk<none>(
    code,
    overload(pre0, pre1),
    post0,
    root);
}

} //hysj::binaries::spirv::codegen
#include<hysj/tools/epilogue.hpp>

