#pragma once

#include<optional>
#include<vector>

#include<hysj/binaries/spirv/binary.hpp>
#include<hysj/binaries/spirv/codegen/expressions.hpp>
#include<hysj/binaries/spirv/codegen/sections.hpp>
#include<hysj/binaries/spirv/codegen/statements.hpp>
#include<hysj/binaries/spirv/codegen/statics.hpp>
#include<hysj/codes/code.hpp>
#include<hysj/codes/eval.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::binaries::spirv::codegen{

struct kernel{
  sections &emit;
  codegen::statics &statics;
  codegen::body body{emit, statics};
};

inline void assemble(kernel &kernel){
  auto &[emit, statics, body] = kernel;

  statements stmts{};
  expressions exprs{};

  build_walk(body, stmts, exprs, emit.bin.kernel.impl);
  const auto kernel_asm = emit.function(statics.types.void_, statics.types.kernel);
  {
    emit.debug_name(
      emit.label(),
      fmt::format("{}_body",
                  debug_name(emit.bin.kernel.impl)));
    for(const auto &v:body.vars)
      statics.types.variable(section::functions, v.pointer_type, v.id);
    
    assemble_let(body, emit.bin.kernel.thread, false);
    assemble(body, stmts, exprs, emit.bin.kernel.impl);
    emit.return_();
    emit.function_end();
  }
  emit.debug_name(kernel_asm,
                  fmt::format("{}_kernel",
                              debug_name(emit.bin.kernel.decl)));
  emit.local_size(kernel_asm, emit.bin.kernel.thread_count());
  emit.entry_point(kernel_asm, debug_name(emit.bin.kernel.decl),
                   views::cat(statics.builtins.ids(), statics.mem.accesses()));
}

} //hysj::binaries::spirv::codegen
#include<hysj/tools/epilogue.hpp>
