#pragma once

#include<optional>
#include<vector>
#include<optional>

#include<hysj/binaries/spirv/binary.hpp>
#include<hysj/binaries/spirv/codegen/sections.hpp>
#include<hysj/binaries/spirv/codegen/statics.hpp>
#include<hysj/codes/code.hpp>
#include<hysj/codes/grammar.hpp>
#include<hysj/codes/loops.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::binaries::spirv::codegen{

struct variable{
  grammars::id op;
  codes::type value_type;
  codegen::pointer_type pointer_type;
  word id;
};

struct let{
  grammars::id op;
  word id;
  bool local;
};

struct unwind{
  grammars::id var, on, off, no_unwind;
  bool visible;
};

struct scope{
  word pre,post;
  std::optional<word> aux;
  std::size_t let;
};

struct body {

  sections &emit;
  codegen::statics &statics;

  std::vector<codegen::variable> vars;
  std::vector<codegen::scope> scopes;
  std::vector<codegen::unwind> unwinds;

  std::vector<codegen::let> lets;

  auto has_variable(grammars::id op)const{
    return ranges::contains_if(vars, [&](const auto &v){ return v.op == op; });
  }
  auto find_variable(grammars::id op)const{
    if(auto it = std::ranges::find_if(vars, [&](const auto &v){ return v.op == op; });
       it != std::ranges::end(vars))
      return just(*it);
    return no<codegen::variable>;
  }

  auto variable(grammars::id op, codegen::pointer_type pointer_type,std::string_view name){
    if(auto it = std::ranges::find_if(vars, [&](const auto &v){ return v.op == op; });
       it != std::ranges::end(vars))
      return *it;
    compile(emit.bin.code, op);
    vars.push_back({
        .op = op,
        .value_type = obtype(emit.bin.code, op),
        .pointer_type = pointer_type,
        .id = emit.new_id(name)
      });
    return vars.back();
  }
  auto variable(grammars::id op, codegen::pointer_type pointer_type){
    return variable(op, pointer_type, debug_name(op));
  }
  auto array(grammars::id op, std::string_view name){
    auto t = obtype(emit.bin.code, op);
    auto n = statics.scalars.scalar<codes::type{set::integers, 32}>(std::max(oext(emit.bin.code, op), 1_i));
    return variable(op, statics.types.pointer(statics.types.array(t, n).id, storage_class::Function), name);
  }
  auto scalar(grammars::id op, std::string_view name){
    auto t = obtype(emit.bin.code, op);
    return variable(op, statics.types.pointer(t, storage_class::Function), name);
  }
  auto scalar(grammars::id op){
    return scalar(op, debug_name(op));
  }

  auto iterator(const codes::loop_iterator &i, std::string_view name){
    scalar(i.sym, name);
    return i;
  }
  auto iterator(const codes::loop_iterator &i){
    return iterator(i, debug_name(i.sym));
  }

  void let(grammars::id o, word v,bool local = true){
    if(v == null)
      throw std::runtime_error(fmt::format("let with invalid value {}", debug_name(o)));
    if(ranges::contains_if(lets, [&](const auto &l){ return l.op == o; }))
      throw std::runtime_error(fmt::format("let already bound {}", debug_name(o)));
    lets.push_back({o, v, local});
  }
  std::optional<word> let(grammars::id o){
    auto it = std::ranges::find_if(lets, [&](const auto &l){ return l.op == o; });
    if(it == std::ranges::end(lets))
      return no<word>;
    return it->id;
  }
  void unlet(grammars::id o){
    std::erase_if(lets, [&](const auto &l){ return l.op == o; });
  }
  void unlet(std::size_t tombstone){
    auto [l_begin, l_end] = std::ranges::remove_if(
      std::views::drop(lets, tombstone),
      &codegen::let::local);
    lets.erase(l_begin, l_end);
  }
  void unlet(){
    std::erase_if(lets, &codegen::let::local);
  }

  auto scope(integer s)const{
    if(s < 0)
      return scopes.at(scopes.size() + s);
    return scopes.at(s);
  }

  constexpr auto push_unwind(codegen::unwind u){
    unwinds.push_back(std::move(u));
  }
  constexpr auto pop_unwind(){
    unwinds.pop_back();
  }
  constexpr auto down(codegen::scope s){
    s.let = lets.size();
    auto pre = scopes.emplace_back(std::move(s)).pre;
    emit.branch(pre);
    emit.label(pre);
    return scopes.back();
  }
  constexpr auto down(){
    return down(codegen::scope{emit.new_id(), emit.new_id()});
  }
  constexpr auto up(bool b = true){
    auto s = scopes.back();
    scopes.pop_back();
    if(b)
      emit.branch(s.post);
    emit.label(s.post);
    unlet(s.let);
    return s;
  }

  word relcast(grammars::id rel){
    auto arg = grammars::relargid(emit.bin.code, rel);
    auto arg_asm = let(arg).value();
    return statics.scalars.cast(retype(emit.bin.code, rel), oetype(emit.bin.code, arg), arg_asm);
  }
  template<codes::optag O,std::size_t I>
  auto rel(sym<O> o, natural_t<I> relidx,auto rel){
    if constexpr(grammars::objvararg<O, I>)
      return views::map(
        rel, [&](auto rel){
          return relcast(rel);
        });
    else
      return relcast(rel);
  }
  template<codes::optag O,std::size_t I>
  auto rel(sym<O> o, constant_t<I> relidx){
    return rel(o, relidx, grammars::rel(emit.bin.code, o, relidx));
  }
  template<codes::optag O>
  auto rels(sym<O> o){
    return with_integer_sequence(
      constant_c<grammars::objnum<O>>,
      [&](auto... relidx){
        auto rels = grammars::rels(emit.bin.code, o);
        return kumi::make_tuple(rel(o, relidx, kumi::get<relidx>(rels))...);
      });
  }

};

auto assemble_pointer(body &body, codes::exprfam a,concepts::forward_words auto &&i){
  return assemble_pointer(body.statics, a, i).or_else(
    [&]{
      a = codes::expr(accessed(body.emit.bin.code, a).value_or(a)); //NOTE: var or itr... - jeh
      auto variable_it = std::ranges::find_if(body.vars, [&](const auto &v){
        return v.op == a.id();
      });
      if(variable_it == std::ranges::end(body.vars))
        return no<value>;
      auto ptr_asm = [&]{
        if(std::ranges::empty(i))
          return variable_it->id;
        auto pointer_asm = body.emit.new_id(fmt::format("{}_ptr", debug_name(a)));
        body.emit.access(
          body.statics.types.pointer(variable_it->value_type, storage_class::Function).id,
          pointer_asm,
          variable_it->id,
          fwd(i));
        return pointer_asm;
      }();
      return just(value{variable_it->value_type, ptr_asm});
    }).value();
}

word assemble_load(body &body,grammars::id o,concepts::forward_words auto &&i){
  const auto [type, ptr] = assemble_pointer(body, codes::expr(o), fwd(i));
  const auto load_asm = body.emit.debug_name(body.statics.types.load(type, ptr), o);
  const auto buffer_type = obtype(body.emit.bin.code, o),
    expr_type = oetype(body.emit.bin.code, o);
  auto expr_asm = body.statics.scalars.cast(expr_type, buffer_type, load_asm);
  if(expr_asm != load_asm)
    body.emit.debug_name(expr_asm, fmt::format("{}_expr", debug_name(o)));
  return expr_asm;
}
word assemble_load(body &body,grammars::id o){
  return assemble_load(body, o, views::no<word>);
}

word assemble_let(body &body,grammars::id o,concepts::forward_words auto &&i,bool local = true){
  auto v = assemble_load(body, o, fwd(i));
  body.let(o, v, local);
  return v;
}
word assemble_let(body &body,grammars::id o, bool local = true){
  return assemble_let(body, o, views::no<word>, local);
}

void assemble_store(body &body,grammars::id o,concepts::forward_words auto &&index_asm,word expr_asm){
  const auto [type, pointer_asm] = assemble_pointer(body, codes::expr(o), fwd(index_asm));
  const auto buffer_type = obtype(body.emit.bin.code, o),
    expr_type = oetype(body.emit.bin.code, o);
  auto buffer_asm = body.statics.scalars.cast(buffer_type, expr_type, expr_asm);
  body.emit.store(pointer_asm, buffer_asm);
}
void assemble_store(body &body,grammars::id o,word expr_asm){
  assemble_store(body, o, views::no<word>, expr_asm);
}

} //hysj::binaries::spirv::codegen
#include<hysj/tools/epilogue.hpp>
