#pragma once

#include<vector>

#include<hysj/codes/code.hpp>
#include<hysj/tools/functional/utility.hpp>
#include<hysj/binaries/spirv/binary.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::binaries::spirv{

HYSJ_EXPORT mem ccmem(const env &,const code &,codes::taskfam);
HYSJ_EXPORT bin ccbin(const env &,const code &,codes::taskfam);
HYSJ_EXPORT std::vector<kernel> cckrn(const env &,const code &,codes::taskfam);
HYSJ_EXPORT bin cc(const env &,const code &, codes::taskfam);

HYSJ_EXPORT bin opt(const env &, const bin &);
inline bin ccopt(const env &e,const code &c, codes::taskfam r){
  return opt(e, cc(e, c, r));
}

HYSJ_EXPORT std::optional<std::string> dis(const bin &);
HYSJ_EXPORT void err(const bin &);

} //hysj::binaries::spirv
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

using spvenv   = binaries::spirv::env;
using spvmem   = binaries::spirv::mem;
using spvbin   = binaries::spirv::bin;

constexpr auto spvcc = lift((binaries::spirv::cc));
constexpr auto spvdis = &binaries::spirv::dis;
constexpr auto spverr = &binaries::spirv::err;

} //hysj
#include<hysj/tools/epilogue.hpp>

