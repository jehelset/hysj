#include<hysj/binaries/spirv.hpp>

#include<cstdlib>
#include<functional>
#include<iostream>
#include<optional>
#include<vector>
#include<utility>

#include<kumi/tuple.hpp>
#include<spirv-tools/libspirv.h>
#include<spirv-tools/optimizer.hpp>

#include<hysj/codes/access.hpp>
#include<hysj/codes/code.hpp>
#include<hysj/codes/compile.hpp>
#include<hysj/codes/compile/contractions.hpp>
#include<hysj/codes/compile/implicit_iterators.hpp>
#include<hysj/codes/compile/rotations.hpp>
#include<hysj/codes/factories.hpp>
#include<hysj/codes/grammar.hpp>
#include<hysj/codes/statics.hpp>
#include<hysj/tools/coroutines/trees.hpp>
#include<hysj/tools/functional/utility.hpp>
#include<hysj/tools/functional/fold.hpp>
#include<hysj/tools/graphs/algorithms/deepwalks.hpp>
#include<hysj/tools/graphs/directions.hpp>
#include<hysj/tools/graphs/properties.hpp>
#include<hysj/tools/functional/optional.hpp>
#include<hysj/tools/functional/pick.hpp>
#include<hysj/tools/ranges/generate.hpp>
#include<hysj/tools/ranges/fold.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/types.hpp>
#include<hysj/binaries/spirv/codegen/kernel.hpp>
#include<hysj/binaries/spirv/codegen/expressions.hpp>
#include<hysj/binaries/spirv/codegen/sections.hpp>
#include<hysj/binaries/spirv/codegen/statics.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::binaries::spirv{

mem ccmem(const env &,const code &c,codes::taskfam k){

  mem m{
    .accesses = codes::accesses::make(c, k.id())
  };
  auto ensure_buffer = [&](codes::exprfam o){
    if(m.buffer(o))
      return;
    auto e = oext(c, o);
    m.buffers.push_back({
        .op = o,
        .type = obtype(c, o),
        .extent = (natural)e,
        .binding = m.buffers.size()
      });
  };
  auto ensure_write = [&](codes::exprfam o){
    auto u = codes::expr(accessed(c, o).value());
    ensure_buffer(u);
    m.buffer(u)->access |= spirv::access::output;
  };
  auto ensure_read = [&](codes::exprfam o){
    auto u = codes::expr(accessed(c, o).value());
    ensure_buffer(u);
    m.buffer(u)->access |= spirv::access::input;
  };

  //FIXME: weird to use host here... - jeh
  for(auto w:m.accesses.inputs(c.builtins.host))
    ensure_read(w);
  for(auto r:m.accesses.outputs(c.builtins.host))
    ensure_write(r);

  return m;
}

bin ccbin(const env &e,const code &c,codes::taskfam t){
  bin b{
    .code    = c,
    .mem     = ccmem(e, b.code, t),
    .kernel{
      .decl    = t,
      .thread  = codes::var(b.code, b.code.builtins.types[codegen::thread_type])
    }
  };
  t = codes::taskcast(codes::compile_implicit_iterators(b.code, t.id())).value();
  t = codes::taskcast(codes::compile_contractions(b.code, t)).value();
  t = codes::taskcast(codes::compile_rotations(b.code, t)).value();
  compile(b.code, b.kernel.thread);
  compile(b.code, t);
  b.kernel.threads = codes::thread_info::make(b.code, e.max_local_size);
  codes::thread(b.code, b.kernel.threads, t);
  b.kernel.impl = thread_fork(b.code, b.kernel.threads, b.kernel.thread, t).value_or(t);
  compile(b.code, b.kernel.impl);
  return b;
}

bin cc(const env &env,const code &code,codes::taskfam task){
  auto bin = ccbin(env, code, task);
  codegen::sections emit{env, bin};
  assemble_pre(emit);
  {
    codegen::statics statics{emit};
    assemble(statics);
    {
      codegen::kernel kernel{emit, statics};
      assemble(kernel);
    }
  }
  assemble_post(emit);
  return bin;
}

constexpr auto target_env = spv_target_env::SPV_ENV_UNIVERSAL_1_6;

namespace _{
  auto make_spv_optimizer_options(){
    return std::unique_ptr<spv_optimizer_options_t,decltype(&spvOptimizerOptionsDestroy)>(
      spvOptimizerOptionsCreate(),
      &spvOptimizerOptionsDestroy);
  }
}
bin opt(const env &env,const bin &unopt_bin){

  if(env.optimization_level == 0)
    return unopt_bin;

  bin opt_bin{
    .code    = unopt_bin.code,
    .mem     = unopt_bin.mem,
    .kernel = unopt_bin.kernel,
    .tape    = {}
  };

  auto opt_options = _::make_spv_optimizer_options();

  auto opt_options_ptr = opt_options.get();
  spvOptimizerOptionsSetRunValidator(opt_options_ptr,true);
  spvOptimizerOptionsSetPreserveBindings(opt_options_ptr,true);

  spvtools::Optimizer opt(target_env);
  opt.RegisterPerformancePasses();
  opt.RegisterSizePasses();
  opt.Run(unopt_bin.tape.data(),unopt_bin.tape.size(),&opt_bin.tape,opt_options_ptr);

  return opt_bin;
}
namespace _{
  auto make_spv_context(){
    return std::unique_ptr<spv_context_t,decltype(&spvContextDestroy)>(
      spvContextCreate(target_env),
      &spvContextDestroy);
  }
  auto make_spv_diagnostic(){
    return std::unique_ptr<spv_diagnostic_t,decltype(&spvDiagnosticDestroy)>(
      nullptr,
      &spvDiagnosticDestroy);
  }
  auto make_spv_text(){
    return std::unique_ptr<spv_text_t,decltype(&spvTextDestroy)>(nullptr,&spvTextDestroy);
  }
}
std::optional<std::string> dis(const bin &B){

  auto context = _::make_spv_context();
  auto text = _::make_spv_text();

  auto text_ptr = text.get();

  constexpr auto options =
    spv_binary_to_text_options_t::SPV_BINARY_TO_TEXT_OPTION_COLOR |
    spv_binary_to_text_options_t::SPV_BINARY_TO_TEXT_OPTION_INDENT |
    spv_binary_to_text_options_t::SPV_BINARY_TO_TEXT_OPTION_FRIENDLY_NAMES |
    spv_binary_to_text_options_t::SPV_BINARY_TO_TEXT_OPTION_COMMENT
    ;
  if(spvBinaryToText(context.get(),B.tape.data(),B.tape.size(),
                     options,&text_ptr,nullptr) != spv_result_t::SPV_SUCCESS)
    return std::nullopt;
  text.reset(text_ptr);
  return std::string(text->str,text->length);
}

void err(const bin &B){

  auto context = _::make_spv_context();
  spv_const_binary_t binary{
    .code = B.tape.data(),
    .wordCount = B.tape.size()
  };
  auto diagnostic = _::make_spv_diagnostic();
  auto diagnostic_ptr = diagnostic.get();
  if(auto result = spvValidate(context.get(),&binary,&diagnostic_ptr);
     result != spv_result_t::SPV_SUCCESS){
    diagnostic.reset(diagnostic_ptr);
    if(auto text = dis(B))
      std::cerr << *text << std::endl;
    throw std::runtime_error(diagnostic->error);
  }

}

} //hysj::binaries::spirv
#include<hysj/tools/epilogue.hpp>
