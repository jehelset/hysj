#pragma once
#include<concepts>
#include<optional>
#include<ranges>
#include<utility>
#include<vector>

#include<hysj/codes.hpp>
#include<hysj/tools/functional/unwrap.hpp>
#include<hysj/tools/preprocessor.hpp>
#include<hysj/tools/ranges/fold.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/reflection.hpp>
#include<hysj/tools/tag_invoke.hpp>
#include<hysj/tools/type_traits.hpp>
#include<hysj/tools/types.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::automata::continuous{

namespace concepts{
  
  template<typename E>
  concept equations = codes::concepts::forward_expr_range<E>;

  template<typename E>
  concept roots = codes::concepts::forward_expr_range<E>;

  template<typename E>
  concept parameters = codes::concepts::forward_expr_range<E>;
 
} //concepts

namespace concepts{
  template<typename A>
  concept automaton = requires(A a){
    { a.variables  } -> codes::concepts::vars;
    { a.equations  } -> concepts::equations;
    { a.roots      } -> concepts::roots;
    { a.parameters } -> concepts::parameters;
   };
} //concepts

constexpr auto order =
  overload(
    [](concepts::automaton auto &&a)
      arrow((static_cast<natural>(std::ranges::distance(a.variables.dependent)))),
    [](const code &s,const concepts::automaton auto &a){
      return ranges::sum(views::map(
                           a.variables.dependent,
                           [&](auto &X){
                             return oext(s,X[0]);
                           }));
    });
constexpr auto rank =
  overload(
    [](concepts::automaton auto &&a)
      arrow((static_cast<natural>(std::ranges::distance(a.equations)))),
    [](const code &s,const concepts::automaton auto &a){
      return ranges::sum(views::map(
                           a.equations,
                           [&](codes::exprfam f){
                             return oext(s,f);
                           }));
    });

constexpr auto size =
  overload(
    [](concepts::automaton auto &&a)
      arrow((static_cast<natural>(std::ranges::distance(a.roots)))),
    [](const code &s,const concepts::automaton auto &a){
      return ranges::sum(views::map(
                           a.roots,
                           [&](codes::exprfam r){
                             return oext(s,r);
                           }));
    });

inline constexpr auto regular =
  [](const code &s,const concepts::automaton auto &a)
    arrow((continuous::order(s,a) == continuous::rank(s,a)))
  ;

struct automaton{
  vars variables;
  std::vector<codes::exprfam> equations,roots,parameters;
  
  friend HYSJ_MIRROR_STRUCT(automaton,(variables,equations,roots,parameters))

  auto operator<=>(const automaton &)const = default;
};
static_assert(continuous::concepts::automaton<automaton>);

} //hysj::automata::continuous
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

namespace cautomata = automata::continuous;

using cautomaton = cautomata::automaton;

constexpr auto corder   = cautomata::order;
constexpr auto crank    = cautomata::rank;
constexpr auto csize    = cautomata::size;
constexpr auto cregular = cautomata::regular;

} //hysj
#include<hysj/tools/epilogue.hpp>
