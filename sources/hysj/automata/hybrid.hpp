#pragma once
#include<concepts>
#include<ranges>
#include<utility>

#include<hysj/tools/functional/unwrap.hpp>
#include<hysj/tools/tag_invoke.hpp>
#include<hysj/tools/type_traits.hpp>
#include<hysj/automata/continuous.hpp>
#include<hysj/automata/discrete.hpp>
#include<hysj/codes.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::automata::hybrid{

template<typename A>
using discrete_automaton_t =
  uncvref_t<decltype(std::declval<A>().discrete_automaton)>;

struct HYSJ_EXPORT activity{
  codes::exprfam equation,guard;

  friend HYSJ_MIRROR_STRUCT(activity,(equation,guard));

  auto operator<=>(const activity &)const = default;
};

namespace concepts{
  template<typename T>
  concept activities = ranges::concepts::convertible_forward_range<T,activity>;
}

struct HYSJ_EXPORT action{
  codes::exprfam variable,function,guard;

  friend HYSJ_MIRROR_STRUCT(action,(variable,function,guard));

  auto operator<=>(const action &)const = default;
};

namespace concepts{
  template<typename T>
  concept actions =
    ranges::concepts::convertible_forward_range<T,action>;
}

struct HYSJ_EXPORT root{
  codes::exprfam variable,function,guard;

  friend HYSJ_MIRROR_STRUCT(root,(variable,function,guard));

  auto operator<=>(const root &)const = default;
};

namespace concepts{
  template<typename T>
  concept roots =
    ranges::concepts::convertible_forward_range<T,root>;
}

namespace concepts{

  template<typename T>
  concept parameters = codes::concepts::forward_expr_range<T>;
}

namespace concepts{
  template<typename A>
  concept automaton =
    requires(A a){
      { a.discrete_automaton } -> dautomata::concepts::automaton;
      { a.continuous_variables } -> codes::concepts::vars;
      { a.continuous_parameters } -> cautomata::concepts::parameters;
      { a.activities } -> concepts::activities;
      { a.actions } -> concepts::actions;
      { a.roots } -> concepts::roots;
      { a.parameters } -> concepts::parameters;
  };
}

//FIXME: real rank!
inline constexpr auto rank =
  overload(
    [](concepts::automaton auto &&a)
      arrow((static_cast<natural>(std::ranges::distance(a.activities)))),
    [](const code &s,concepts::automaton auto &&a){
      return ranges::sum(views::map(
                         a.activities,
                         [&](const auto &f){
                           return oext(s,f.equation);
                         }));
    });
  ;

inline constexpr auto size =
  overload(
    [](concepts::automaton auto &&a)
      arrow((static_cast<natural>(std::ranges::distance(a.roots)))),
    [](const code &s,concepts::automaton auto &&a){
      return ranges::sum(views::map(
                         a.roots,
                         [&](const auto &r){
                           return oext(s,r.variable);
                         }));
    });
  ;

inline constexpr auto degree =
  overload(
    [](concepts::automaton auto &&a)
      arrow((static_cast<natural>(std::ranges::distance(a.actions)))),
    [](const code &s,concepts::automaton auto &&a){
      return ranges::sum(views::map(
                         a.actions,
                         [&](const auto &a){
                           return oext(s,a.variable);
                         }));
    });
  ;

struct automaton{
  dautomaton discrete_automaton;
  vars continuous_variables;
  std::vector<codes::exprfam> continuous_parameters;

  std::vector<activity> activities;
  std::vector<action> actions;
  std::vector<root> roots;
  std::vector<codes::exprfam> parameters;
    
  friend HYSJ_MIRROR_STRUCT(automaton,(discrete_automaton,continuous_variables,
                                       continuous_parameters,
                                       activities,actions,roots,parameters));

  auto operator<=>(const automaton &)const = default;
};
static_assert(concepts::automaton<automaton>);

} //hysj::automata::hybrid
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

namespace hautomata = automata::hybrid;

using hactivity  = hautomata::activity;
using haction    = hautomata::action;
using hroot      = hautomata::root;
using hautomaton = hautomata::automaton;

inline constexpr auto hrank   = hautomata::rank;
inline constexpr auto hsize   = hautomata::size;
inline constexpr auto hdegree = hautomata::degree;

}
#include<hysj/tools/epilogue.hpp>
