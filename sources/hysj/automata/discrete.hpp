#pragma once
#include<concepts>
#include<limits>
#include<ranges>
#include<utility>
#include<vector>

#include<hysj/codes.hpp>
#include<hysj/grammars.hpp>
#include<hysj/tools/functional/unwrap.hpp>
#include<hysj/tools/ranges/cat.hpp>
#include<hysj/tools/ranges/fold.hpp>
#include<hysj/tools/ranges/map.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/tag_invoke.hpp>
#include<hysj/tools/type_traits.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::automata::discrete{

struct transition{
  codes::exprfam variable, function, guard;
  friend HYSJ_MIRROR_STRUCT(transition,(variable,function,guard));

  auto operator<=>(const transition &)const = default;
};

namespace concepts{

  template<typename T>
  concept transitions = ranges::concepts::convertible_forward_range<T,transition>;

  template<typename T>
  concept parameters = codes::concepts::forward_expr_range<T>;

} //concepts

namespace concepts{
  template<typename A>
  concept automaton = requires(A a){
    { a.variables   } -> codes::concepts::vars;
    { a.transitions } -> concepts::transitions;
    { a.parameters  } -> concepts::parameters;
  };
} //concepts

inline constexpr auto order =
  overload(
    [](concepts::automaton auto &&a)
      arrow((static_cast<natural>(std::ranges::distance(a.variables.dependent)))),
    [](const code &s,const concepts::automaton auto &a) -> natural{
      return ranges::sum(views::map(
                           a.variables.dependent,
                           [&](const auto &Q){
                             return oext(s,Q.at(0));
                           }));
    });

inline constexpr auto rank =
  overload(
    [](concepts::automaton auto &&a)
      arrow((static_cast<natural>(std::ranges::distance(a.transitions)))),
    [](const code &s,const concepts::automaton auto &a) -> natural{
      return ranges::sum(views::map(
                           a.transitions,
                           [&](const auto &d){
                             return oext(s,d.variable);
                           }));
    });

inline constexpr auto cardinality =
  [](const code &s,const concepts::automaton auto &a){
    return ranges::prod(views::map(
                          a.variables.dependent,
                          [&](const auto &q){
                            return ospan(s,s.builtins.lits,s.statics.types,q[0]);
                          }));
  };

struct automaton{
  vars variables;
  std::vector<transition> transitions;
  std::vector<codes::exprfam> parameters;

  friend HYSJ_MIRROR_STRUCT(automaton,(variables,transitions,parameters));

  auto operator<=>(const automaton &) const = default;
};
static_assert(concepts::automaton<automaton>);

} //hysj::automata::discrete
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

namespace dautomata = automata::discrete;

using dtransition = dautomata::transition;
using dautomaton  = dautomata::automaton;

inline constexpr auto dorder       = dautomata::order;
inline constexpr auto drank        = dautomata::rank;
inline constexpr auto dcardinality = dautomata::cardinality;

} //hysj
#include<hysj/tools/epilogue.hpp>
