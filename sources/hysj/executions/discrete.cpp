#include<hysj/executions/discrete.hpp>

#include<hysj/codes.hpp>
#include<hysj/grammars.hpp>
#include<hysj/codes/compile/implicit_iterators.hpp>
#include<hysj/codes/algorithms/substitute.hpp>
#include<hysj/automata/discrete.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/executions/buffers.hpp>
#include<hysj/executions/state.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::executions::discrete{

events cc_events(code &C){
  events E;
  for(auto e:enumerator_values<event>)
    E[etoi(e)] = codes::expr(codes::icst64(C, etoi(e)));
  return E;
}

codes::exprfam cc_status(code &C,const buffer &R){
  return {ivar64(C, {R.itr})};
}

dtransition cc_substitute_transition(const codes::substitute &F,const dtransition &t){
  dtransition u{
    .variable = codes::expr(F(t.variable.id()).value_or(t.variable.id())),
    .function = codes::expr(F(t.function.id()).value_or(t.function.id())),
    .guard = codes::expr(F(t.guard.id()).value_or(t.guard.id()))
  };
  return u;
}

transition cc_transition(code &C,codes::devsym y,dtransition t){
  using namespace codes::factories;
  //FIXME: crap - jeh
  const auto wrap = [&](auto q, auto f) -> codes::exprfam {
    compile(C, q);
    auto q_max = omax(C, q);
    if(q_max == codes::oinf(C, q).id())
      return {f};
    return {mod(C, f, q_max)};
  };

  transition d{
    .function{codes::access_for(C, t.function)},
    .guard{codes::access_for(C, t.guard)},
    .active{bvar64(C)},
    .compute_guard{ is_access(C, t.guard) ? codes::task(C.builtins.noop) : codes::task(put(C, d.guard, t.guard)) },
    .compute_active{ put(C, d.active, con(C, lor(C, {d.guard}))) },
    .compute_function{ is_access(C, t.function) ? codes::task(C.builtins.noop) : codes::task(put(C, d.function, t.function)) },
    .compute_variable{ put(C, t.variable, sel(C, d.guard, {t.variable, wrap(t.variable, d.function)})) },
    .get{
      when(C, { recv(C, y, d.function), recv(C, y, d.guard), recv(C, y, d.active) })
    }
  };
  return d;
}

transitions cc_transitions(code &C,std::vector<transition> T){
  using namespace codes::factories;
  transitions D{
    .container = std::move(T),
    .event = bvar64(C),
    .compute_guards{ when(C, views::map(D.container, &transition::compute_guard)) },
    .compute_functions{ when(C, views::map(D.container, &transition::compute_function)) },
    //FIXME: parallell to different vars.
    .compute_variables{ 
      then(C,
           ranges::vec(D.container, &transition::compute_variable))
    },
    .compute_event{
      then(
        C,
        {
          when(C, views::map(D.container, &transition::compute_active)),
          put(C, D.event, lor(C, views::map(D.container, &transition::active)))
        })
    },
    .get = when(C, views::map(D.container, &transition::get))
  };
  return D;

}


namespace _{

  codes::taskfam cc_solve(code &C,
                          const events &E,
                          const buffer &R,
                          codes::exprfam s,
                          const transitions &D){
    using namespace codes::factories;

    //FIXME: non-determinism - jeh
    auto solve_status =
      then(C,
           {
             D.compute_event,
             put(C, cc_current(C, R, s), sel(C, D.event, {E[etoi(event::stop)], E[etoi(event::step)]}))
           });
    return {
      then(C,
           {
             D.compute_guards,
             when(C,{
                 then(C, {
                     D.compute_functions,
                     D.compute_variables,
                   }),
                 solve_status,
               })
           })
    };
  }

  codes::taskfam cc_step_independent_variable(code &C,const state &S){
    using namespace codes::factories;
    return {put(C, S.next.independent, inc(C, S.current.independent))};
  }

  //FIXME: causality, constraints... - jeh
  codes::taskfam cc_step_dependent_variable(code &C,const state &S,std::size_t i){

    using namespace codes::factories;
  
    const auto wrap = [&](auto q, auto f) -> codes::exprfam {
      auto q_max = omax(C, q);
      if(q_max == codes::oinf(C, q).id())
        return {f};
      return {mod(C, f, q_max)};
    };

    auto &Q_next = S.next.dependent.at(i);
    auto &Q_current = S.current.dependent.at(i);

    if(codes::varclassify(Q_current) == codes::depvartag::algebraic){
      auto q = Q_current[0];
      return {put(C, Q_next[0], wrap(q, q))};
    }

    auto compute_variables = ranges::vec(
      views::indices(Q_current),
      [&](natural j) -> codes::taskfam{
        auto q_next = Q_next[j];
        auto q_step = [&] -> codes::exprfam{
          auto q = Q_current[j];
          auto q_step = [&] -> codes::exprfam {
            if(j == varorder(Q_current))
              return {codes::ozero(C, q)};
            return {codes::add(C, {q, Q_current[j + 1]})};
          }();
          return wrap(q, q_step);
        }();
        return {put(C, q_next, q_step)};
      });
    return {
      then(C,compute_variables)
    };
  }
  codes::taskfam cc_step_dependent_variables(code &C,const state &S){
    using namespace codes::factories;
    return {
      then(C,
           ranges::vec(
             views::map(
               views::indices(S.buffer.dependent),
               [&](auto i){
                 return cc_step_dependent_variable(C, S, i);
               })))
    };
  }
  codes::taskfam cc_step(code &C,const state &S){
    using namespace codes::factories;
    auto t = cc_step_independent_variable(C, S);
    auto Q = cc_step_dependent_variables(C, S);
    auto step = then(C, {t, Q});
    return {step};
  }

}

execution cc(code &C,config S,buffer B,state X, transitions T, std::vector<codes::exprfam> P){
  execution E{
    .buffer      = std::move(B),
    .events      = cc_events(C),
    .status      = cc_status(C, E.buffer),
    .is_stop     = {codes::eq(C, cc_current(C, E.buffer, E.status), E.events[etoi(devent::stop)])},
    .is_done     = {codes::expr(codes::lor(C, {E.buffer.is_full, E.is_stop}))},
    .state       = std::move(X),
    .transitions = std::move(T),
    .parameters  = std::move(P),
    .solve       = _::cc_solve(C, E.events, E.buffer, E.status, E.transitions),
    .step        = _::cc_step(C, E.state),
    .loop = {
      codes::loop(C,
                  codes::then(
                    C,
                    {
                      codes::if_(C, E.is_done, codes::task(C.builtins.brk)),
                      E.step,
                      E.buffer.push,
                      E.solve
                    }))
    },
    .get_events = {
      codes::when(
        C,
        {
          codes::recv(C, S.dev, E.status),
          codes::recv(C, S.dev, E.buffer.begin),
          codes::recv(C, S.dev, E.buffer.size)
        })
    },
    .start = {
      codes::then(
        C,
        {
          codes::on(
            C,
            S.dev,
            codes::then(
              C,
              {
                E.buffer.clear,
                E.buffer.push,
                E.solve,
                E.loop
              })),
          E.get_events
        })
    },
    .resume = {
      codes::then(
        C,
        {
          codes::on(
            C,
            S.dev,
            codes::then(
              C,
              {
                E.buffer.cycle,
                E.loop
              })),
          E.get_events
        })
    },
    .put_state = compile_send(C, E.state.buffer, S.dev),
    .get_state = compile_recv(C, E.state.buffer, S.dev),
    .put_parameters = {
      codes::when(
        C,
        views::map(E.parameters, bind<>(lift((codes::send)), std::ref(C), S.dev)))
    },
    .api = codes::api(C, {
        E.start,
        E.resume,
        E.put_state,
        E.get_state,
        E.transitions.get,
        E.put_parameters
      })
  };
  return E;

}
execution cc(code &C,const dautomaton &A,config S){
  auto B = buffer::make(C, S.buffer_capacity);
  auto X = cc_state(C, A.variables, B);
  auto F = cc_substitute_current(C, X);
  auto T = cc_transitions(C,
                          ranges::vec(
                            A.transitions,
                            [&](const auto &t){
                              return cc_transition(C, S.dev, cc_substitute_transition(F, t));
                            }));
  return cc(C, std::move(S), std::move(B), std::move(X), std::move(T), A.parameters);
}

} //hysj::executions::discrete
#include<hysj/tools/epilogue.hpp>
