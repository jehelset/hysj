#pragma once
#include<cstdint>

#include<hysj/codes.hpp>
#include<hysj/tools/preprocessor.hpp>
#include<hysj/tools/reflection.hpp>
#include<hysj/tools/type_traits.hpp>
#include<hysj/tools/types.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::executions{

struct HYSJ_EXPORT buffer{
  std::int64_t capacity;

  codes::exprfam begin, size;
  codes::itrsym itr;
  codes::exprfam is_full;
  codes::taskfam push;
  codes::taskfam clear, cycle;

  codes::exprfam current, prev, next;

  friend HYSJ_MIRROR_STRUCT(buffer,(capacity, begin, size,
                                    itr, is_full, push, clear, cycle,
                                    current, prev, next));

  auto operator<=>(const buffer &)const = default;

  HYSJ_EXPORT
  static buffer make(code &, std::int64_t);
};

HYSJ_EXPORT
codes::exprfam cc_buffer(code &,const buffer &,codes::exprfam);

HYSJ_EXPORT
codes::exprfam cc_current(code &,const buffer &,codes::exprfam);
HYSJ_EXPORT
codes::exprfam cc_next(code &,const buffer &,codes::exprfam);
HYSJ_EXPORT
codes::exprfam cc_prev(code &,const buffer &,codes::exprfam);

HYSJ_EXPORT
codes::taskfam cc_buffer_put(code &,const buffer &,codes::exprfam expr,codes::exprfam storage);
HYSJ_EXPORT
codes::taskfam cc_buffer_get(code &,const buffer &,codes::exprfam expr,codes::exprfam storage);

} //hysj::executions
#include<hysj/tools/epilogue.hpp>
