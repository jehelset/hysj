#include<hysj/executions/buffers.hpp>

#include<hysj/codes.hpp>
#include<hysj/grammars.hpp>
#include<hysj/codes/compile/implicit_iterators.hpp>
#include<hysj/codes/algorithms/substitute.hpp>
#include<hysj/automata/discrete.hpp>
#include<hysj/tools/ranges/utility.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::executions{

buffer buffer::make(code &C,std::int64_t cap){

  const auto Z = codes::icst64(C, 0);
  const auto two = codes::icst64(C, 2);
  const auto N = codes::icst64(C, cap);

  buffer r{
    .capacity = cap,
    .begin = ivar64(C),
    .size = ivar64(C),
    .itr = codes::itr(C, N),
    .is_full{codes::eq(C, r.size, N)},
    .push{
      codes::put(C, r.size, inc(C, r.size))
    },
    .clear{
      codes::when(
        C,
        {
          codes::put(C, r.size, Z), codes::put(C, r.begin, Z)
        })
    },
    .cycle = codes::task(
      codes::then(
        C,
        {
          codes::put(C, r.begin, codes::mod(C, codes::add(C, {r.begin, r.size}), N)),
          codes::put(C, r.size, Z)
        })),
    .current{codes::mod(C, codes::add(C, {r.begin, codes::dec(C, r.size)}), N)},
    .prev{codes::mod(C, codes::add(C, {r.begin, codes::sub(C, r.size, two)}), N)},
    .next{codes::mod(C, codes::add(C, {r.begin, r.size}), N)}
  };
  return r;
}

codes::exprfam cc_buffer(code &C,const buffer &R,codes::exprfam e){
  auto u = codes::expr(compile_implicit_iterators(C, e));
  return {codes::buf_for(C, u, {codes::dimexpr(R.itr)})};
}
codes::exprfam cc_current(code &C,const buffer &R,codes::exprfam e){
  compile(C, e);
  const auto N = orank(C, e);
  return {codes::rot(C, e, views::prepend(R.current, std::views::drop(oitrs(C, e), 1)))};
}
codes::exprfam cc_next(code &C,const buffer &R,codes::exprfam e){
  compile(C, e);
  const auto N = orank(C, e);
  return {codes::rot(C, e, views::prepend(R.next, std::views::drop(oitrs(C, e), 1)))};
}
codes::exprfam cc_prev(code &C,const buffer &R,codes::exprfam e){
  compile(C, e);
  const auto N = orank(C, e);
  return {codes::rot(C, e, views::prepend(R.prev, std::views::drop(oitrs(C, e), 1)))};
}

codes::taskfam cc_buffer_put(code &c,const buffer &b,codes::exprfam expr,codes::exprfam storage){
  using namespace codes::factories;
  return { put(c, cc_current(c, b, storage), expr) };
}
codes::taskfam cc_buffer_get(code &c,const buffer &b,codes::exprfam expr,codes::exprfam storage){
  using namespace codes::factories;
  return { put(c, cc_current(c, b, storage), expr) };
}

} //hysj::executions
#include<hysj/tools/epilogue.hpp>
