#pragma once

#include<hysj/automata/continuous.hpp>
#include<hysj/codes/code.hpp>
#include<hysj/codes/grammar.hpp>
#include<hysj/executions/continuous.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::executions::continuous{

struct HYSJ_EXPORT be_config : config {
  codes::exprfam nm_step_size, nm_maxiter, nm_abstol;

  friend HYSJ_MIRROR_STRUCT(be_config, (nm_step_size, nm_maxiter, nm_abstol)(config));

  auto operator<=>(const be_config &) const = default;

  static be_config make(code &,codes::devsym);
};

HYSJ_EXPORT
execution be(code &,const cautomaton &,be_config);

} //hysj::executions::continuous
#include<hysj/tools/epilogue.hpp>
