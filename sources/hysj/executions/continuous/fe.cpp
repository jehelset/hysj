#include<hysj/executions/continuous/fe.hpp>

#include<algorithm>
#include<functional>
#include<iterator>
#include<ranges>
#include<vector>

#include<hysj/codes.hpp>
#include<hysj/executions/continuous.hpp>
#include<hysj/tools/functional/bind.hpp>
#include<hysj/tools/functional/optional.hpp>
#include<hysj/tools/ranges/map.hpp>
#include<hysj/tools/ranges/utility.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::executions::continuous{

execution fe(code &C,
             fe_config config,buffer B,events V,codes::exprfam s,
             state S,
             std::vector<codes::exprfam> F,
             std::vector<root> R,
             std::vector<codes::exprfam> P,
             codes::exprfam r_event){
  if(F.size() != S.variables.dependent.size())
    throw std::runtime_error("irregular automaton");

  auto E = cc(C, config, std::move(B), std::move(V), s, std::move(S),
              std::move(F), std::move(R), std::move(P), r_event);
  auto D =
      codes::then(
        C,
        ranges::vec(std::views::zip(E.state.current.dependent, E.equations),
                    unpack(
                      [&](const auto &X,auto f){
                        return codes::put(C, X.back(), f);
                      })));
  compile_compute_derivatives(C, E, {D});
  return E;
}


execution fe(code &C,const cautomaton &A,fe_config s){
  if(A.equations.size() != A.variables.dependent.size())
    throw std::runtime_error("irregular automaton");

  auto E = cc(C, A, std::move(s));
  auto D =
      codes::then(
        C,
        ranges::vec(std::views::zip(E.state.current.dependent, E.equations),
                    unpack(
                      [&](const auto &X,auto f){
                        return codes::put(C,X.back(), f);
                      })));
  compile_compute_derivatives(C, E, {D});
  return E;
}

} //hysj::executions::continuous
#include<hysj/tools/epilogue.hpp>
