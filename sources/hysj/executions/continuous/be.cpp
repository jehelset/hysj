#include<hysj/executions/continuous/be.hpp>

#include<algorithm>
#include<functional>
#include<iterator>
#include<ranges>
#include<vector>

#include<hysj/codes.hpp>
#include<hysj/codes/algorithms/symdiffs.hpp>
#include<hysj/codes/algorithms/substitute.hpp>

#include<hysj/numerics/newtons_method.hpp>

#include<hysj/executions/continuous.hpp>
#include<hysj/tools/functional/bind.hpp>
#include<hysj/tools/functional/optional.hpp>
#include<hysj/tools/ranges/map.hpp>
#include<hysj/tools/ranges/utility.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::executions::continuous{

be_config be_config::make(code &C,codes::devsym d){
  return {
    config::make(C, d),
    {codes::rcst64(C, 1.0e-2)},
    {codes::icst64(C, 128)},
    {codes::rcst64(C, 1.0e-3)}
  };
}

execution be(code &C,const cautomaton &A,be_config s){

  if(A.equations.size() != A.variables.dependent.size())
    throw std::runtime_error("irregular automaton");

  auto step_size = s.step_size;
  auto E = cc(C, A, std::move(s));

  //FIXME: write to next directly - jeh
  codes::vars Y{
    .independent = codes::add(C, {E.state.current.independent, step_size}),
    .dependent = ranges::vec(
      E.state.current.dependent,
      [&](const auto &X_current){
        const auto N = std::ranges::size(X_current);
        auto X_next = X_current;
        for(auto i:views::indices(0ul, N - 1ul)){
          const auto j = N - (i + 2);
          X_next[j] = codes::expr(
            codes::add(C, {X_current[j],  codes::mul(C, {step_size, X_next[j + 1]})}));
        }
        return X_next;
      })
  };

  codes::substitute t_subst{
    .code = C,
    .subs{
      kumi::make_tuple(A.variables.independent.id(), Y.independent.id())
    }
  };
  codes::substitute X_subst{
    .code = C,
    .subs = ranges::vec(
      std::views::zip(std::views::join(A.variables.dependent),
                      std::views::join(Y.dependent)),
      unpack([&](auto x, auto y){
        return kumi::make_tuple(x.id(), y.id());
      }))
  };

  auto D =
    codes::then(
      C,
      ranges::vec(
        std::views::zip(A.variables.dependent, Y.dependent, A.equations),
        unpack(
          [&](const auto &X,const auto &Y,auto f) -> codes::taskfam{

            compile(C, f);
            auto I = oitrs(C, f);
            auto J = ranges::vec(
              I,
              [&](auto i){ return itr_dup(C, codes::itrcast(i).value()); });
            auto dfdx = codes::symdiff(C, codes::der(C, f, X.back(), J)).value();

            f = codes::expr(t_subst(f.id()).value_or(f.id()));

            bool c = [&]{
              auto f_new = X_subst(f.id());
              if(!f_new)
                return false;
              f = codes::expr(f_new.value());
              return true;
            }();

            auto dx = Y.back();
            if(!c)
              return {codes::put(C, dx, f)};

            dfdx = codes::expr(t_subst(dfdx.id()).value_or(dfdx.id()));
            dfdx = codes::expr(X_subst(dfdx.id()).value_or(dfdx.id()));

            auto g = codes::sub(C, f, dx);
            auto dgdx = codes::sub(C, dfdx, codes::oone(C, f));
            auto nm_func = numerics::nm_func(C, {g}, {dgdx}, dx, s.nm_step_size, s.nm_maxiter);
            auto nm = numerics::nm(C, nm_func, {codes::lt(C, codes::rms(C, {nm_func.residual}), s.nm_abstol)});

            return nm.run;
          })));

  compile_compute_derivatives(C, E, {D});

  return E;
}

} //hysj::executions::continuous
#include<hysj/tools/epilogue.hpp>
