#pragma once

#include<hysj/automata/continuous.hpp>
#include<hysj/codes/code.hpp>
#include<hysj/codes/grammar.hpp>
#include<hysj/executions/continuous.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::executions::continuous{

using fe_config = config;

HYSJ_EXPORT
execution fe(code &,
             fe_config,
             buffer,
             events,
             codes::exprfam status,
             state,
             std::vector<codes::exprfam> equations,
             std::vector<root>,
             std::vector<codes::exprfam> parameters,
             codes::exprfam root_event);

HYSJ_EXPORT
execution fe(code &,const cautomaton &,fe_config);

} //hysj::executions::continuous
#include<hysj/tools/epilogue.hpp>
