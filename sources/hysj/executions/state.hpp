#pragma once
#include<vector>

#include<hysj/codes/algorithms/substitute.hpp>
#include<hysj/codes/code.hpp>
#include<hysj/codes/grammar.hpp>
#include<hysj/codes/vars.hpp>
#include<hysj/executions/buffers.hpp>
#include<hysj/tools/ranges/cat.hpp>
#include<hysj/tools/ranges/utility.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::executions{

HYSJ_EXPORT codes::taskfam compile_recv(code &,const vars &,codes::devsym);

HYSJ_EXPORT codes::taskfam compile_send(code &,const vars &,codes::devsym);

struct HYSJ_EXPORT state{
  codes::vars variables, buffer, current, next, prev;

  friend HYSJ_MIRROR_STRUCT(state, (variables, buffer, current, next, prev));
};

HYSJ_EXPORT state cc_state(code &,const vars &,const buffer &);

HYSJ_EXPORT codes::substitute cc_substitute_vars(code &,const codes::vars &,const codes::vars &);
HYSJ_EXPORT codes::substitute cc_substitute_current(code &,const state &);
HYSJ_EXPORT codes::substitute cc_substitute_buffer(code &,const state &);

codes::substitute cc_substitute_current(code &C,auto &&... S) requires (sizeof...(S) > 1){
  return codes::substitute{
    .code = C,
    .subs = ranges::vec(views::cat(executions::cc_substitute_current(C, S).subs...))
  };
}
codes::substitute cc_substitute_buffer(code &C,auto &&... S) requires (sizeof...(S) > 1){
  return codes::substitute{
    .code = C,
    .subs = ranges::vec(views::cat(executions::cc_substitute_buffer(C, S).subs...))
  };
}

} //hysj::executions
#include<hysj/tools/epilogue.hpp>
