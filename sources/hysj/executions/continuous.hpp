#pragma once
#include<cstdint>
#include<concepts>
#include<type_traits>
#include<utility>

#include<fmt/format.h>

#include<hysj/automata/continuous.hpp>
#include<hysj/codes.hpp>
#include<hysj/executions/state.hpp>
#include<hysj/executions/buffers.hpp>
#include<hysj/tools/preprocessor.hpp>
#include<hysj/tools/ranges/cat.hpp>
#include<hysj/tools/ranges/pins.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/reflection.hpp>
#include<hysj/tools/type_traits.hpp>
#include<hysj/tools/types.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::executions::continuous{

#define HYSJ_EXECUTIONS_CONTINUOUS_EVENT \
  (step)                                 \
  (root)                                 \
  (stop)                                 \
  (fail)

enum class event : integer { HYSJ_SPLAT(0,HYSJ_EXECUTIONS_CONTINUOUS_EVENT) };
HYSJ_MIRROR_ENUM(event,HYSJ_EXECUTIONS_CONTINUOUS_EVENT)

#define HYSJ_LAMBDA(id)                                   \
  inline constexpr auto id##_tag = constant_c<event::id>; \
  using id##_tag_type = constant_t<event::id>;
HYSJ_MAP_LAMBDA(HYSJ_EXECUTIONS_CONTINUOUS_EVENT)
#undef HYSJ_LAMBDA

#define HYSJ_EXECUTIONS_CONTINUOUS_SIGNS \
  (negative,-1)                          \
  (unknown,  0)                          \
  (positive, 1)

#define HYSJ_LAMBDA(id,value) \
  constexpr integer id = value;
HYSJ_MAP_LAMBDA(HYSJ_EXECUTIONS_CONTINUOUS_SIGNS)
#undef HYSJ_LAMBDA

using events = std::array<codes::exprfam, enumerator_count<event>>;

HYSJ_EXPORT
events cc_events(code &);

HYSJ_EXPORT
codes::exprfam cc_status(code &,const buffer &);

struct HYSJ_EXPORT config{
  codes::devsym dev;
  codes::exprfam step_size, stop_time;

  std::int64_t buffer_capacity = 1;

  friend HYSJ_MIRROR_STRUCT(config,(dev, step_size, stop_time, buffer_capacity));

  auto operator<=>(const config &)const = default;

  static config make(code &, codes::devsym);
};

HYSJ_EXPORT
codes::exprfam cc_root_sign(code &,codes::exprfam);
HYSJ_EXPORT
codes::exprfam cc_root_event(code &);

struct HYSJ_EXPORT root{
  codes::exprfam residual,sign_function, sign, sign_next, sign_flip;
  codes::taskfam evaluate_function, evaluate_sign_next, evaluate_sign_flip, evaluate, get, put;

  friend HYSJ_MIRROR_STRUCT(
    root,
    (residual, sign_function, sign, sign_next, sign_flip,
     evaluate_function, evaluate_sign_next, evaluate_sign_flip,
     evaluate, get, put));

  auto operator<=>(const root &)const = default;
};

HYSJ_EXPORT
root cc_root(code &, codes::devsym, codes::exprfam function, codes::exprfam sign);

struct HYSJ_EXPORT roots{
  std::vector<root> container;
  codes::exprfam event;
  codes::taskfam evaluate, get, put, clear;

  friend HYSJ_MIRROR_STRUCT(roots, (container, event, evaluate, get, put, clear)); 

  auto operator<=>(const roots &)const = default;
};

HYSJ_EXPORT
roots cc_roots(code &code, codes::devsym dev, std::vector<root>,codes::exprfam r_event);


struct HYSJ_EXPORT execution{

  executions::buffer buffer;
  continuous::events events;
  codes::exprfam status, is_stop, is_root, is_done;
  executions::state state;

  std::vector<codes::exprfam> equations;
  continuous::roots roots;
  std::vector<codes::exprfam> parameters;

  codes::taskfam compute_status, compute_derivatives, compute_root, solve;
  codes::taskfam step_independent, step_dependent, step;
  codes::taskfam loop, get_events, start, resume;
  codes::taskfam get_state, put_state, put_parameters, init;
  codes::apisym api;

  friend HYSJ_MIRROR_STRUCT(execution,
                            (buffer, events,
                             status, is_stop, is_root, is_done,
                             state, equations, roots, parameters,
                             get_events,
                             compute_status, compute_derivatives, compute_root, solve,
                             step_independent, step_dependent, step, 
                             loop, get_events, start, resume, 
                             get_state, put_state, put_parameters, init, 
                             api));

  auto operator<=>(const execution &)const = default;
};

HYSJ_EXPORT
void compile_compute_derivatives(code &,execution &,codes::taskfam);

HYSJ_EXPORT
std::vector<codes::exprfam> cc_equations(code &, const codes::vars &,
                                         const std::vector<codes::exprfam> &,
                                         const buffer &,const codes::vars &);

HYSJ_EXPORT
codes::taskfam cc_get_events(code &C,codes::devsym d,
                             codes::exprfam s,
                             const buffer &B);

HYSJ_EXPORT
execution cc(code &,
             config,
             buffer,
             events,
             codes::exprfam status,
             state,
             std::vector<codes::exprfam> equations,
             std::vector<root>,
             std::vector<codes::exprfam> parameters,
             codes::exprfam root_event);

HYSJ_EXPORT
execution cc(code &,const cautomaton &,config);

} //hysj::executions::continuous
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

namespace cexecs = executions::continuous;
using cexec = cexecs::execution;
using cevent  = cexecs::event;

} //hysj
HYSJ_ENUM_FORMATTER(hysj::_HYSJ_VERSION_NAMESPACE::cevent);
#include<hysj/tools/epilogue.hpp>
