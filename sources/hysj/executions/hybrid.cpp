#include<hysj/executions/hybrid.hpp>

#include<hysj/codes/vars.hpp>
#include<hysj/executions/state.hpp>
#include<hysj/executions/buffers.hpp>
#include<hysj/executions/discrete.hpp>
#include<hysj/executions/continuous.hpp>
#include<hysj/tools/functional/compose.hpp>
#include<hysj/tools/ranges/utility.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::executions::hybrid{

activity cc_activity(code &C,codes::devsym d,const codes::substitute &M,const hactivity &a){
  using namespace codes::factories;
  activity e{
    .guard = bvar64(C),
    .equation = codes::expr(M(a.equation).value_or(a.equation)),
    .check = codes::on(C, d, put(C, e.guard, codes::expr(M(a.guard).value_or(a.guard)))),
    .get_guard = recv(C, d, e.guard)
  };
  return e;
}

activities cc_activities(code &C,codes::devsym d,const codes::substitute &M,const std::vector<hactivity> &A){
  using namespace codes::factories;
  activities E{
    .container = ranges::vec(A, bind<>(&cc_activity, std::ref(C), d, std::ref(M))),
    .check = when(C, views::map(E.container, &activity::check)),
    .get_guards = when(C, views::map(E.container, &activity::get_guard))
  };
  return E;
}

action cc_action(code &C,codes::devsym d,const codes::substitute &M,const haction &a){
  using namespace codes::factories;
  auto g = codes::expr(M(a.guard).value_or(a.guard.id()));
  auto f = codes::expr(M(a.function).value_or(a.function.id()));
  auto v = codes::expr(M(a.variable).value_or(a.variable.id()));
  action e{
    .guard = codes::access_for(C, g),
    .event = bvar64(C),
    .check = [&] -> codes::taskfam {
      codes::taskfam u{codes::on(C, d, put(C, e.event, con(C, lor(C, e.guard))))};
      if(is_access(C, g))
        return u;
      return {
        codes::then(
          C,
          {
            codes::on(C, d, put(C, e.guard, g)),
            u
          })
      };
     }(),
    .apply = codes::on(C, d, put(C, v, sel(C, e.guard, {v, f}))),
    //FIXME:
    // need to recv both else bug (check sawtooth example) - jeh
    .get_event = when(C, {recv(C, d, e.guard), recv(C, d, e.event)}) 
  };
  return e;
}

actions cc_actions(code &C,codes::devsym d,const codes::substitute &M,const std::vector<haction> &A){
  using namespace codes::factories;
  actions E{
    .container = ranges::vec(A, bind<>(&cc_action, std::ref(C), d, std::ref(M))),
    .event = bvar64(C),
    .check = then(
      C,
      {
        when(C, views::map(E.container, &action::check)),
        codes::on(C, d, put(C, E.event, lor(C, views::map(E.container, &action::event))))
      }),
    .apply = when(C, views::map(E.container, &action::apply)),
    .get_event = when(C, views::prepend(codes::task(recv(C, d, E.event)),
                                        views::map(E.container, &action::get_event)))
  };
  return E;
}

root cc_root(code &C,const config &S,const codes::substitute &M,const hroot &a){
  using namespace codes::factories;

  auto f = codes::expr(M(a.function).value_or(a.function.id()));
  auto v = codes::expr(M(a.variable).value_or(a.variable.id()));
  auto g = codes::expr(M(a.guard).value_or(a.guard.id()));
  root e{
    .guard = bvar64(C),
    .continuous = cexecs::cc_root(C, S.continuous.dev, f, v),
    .check = codes::on(C, S.dev, put(C, e.guard, g)),
    .get_guard = recv(C, S.dev, e.guard),
    .put_state = send(C, S.dev, accessed(C, v).value())
  };
  return e;
}

roots cc_roots(code &C,config S,const codes::substitute &M,const std::vector<hroot> &A){
  using namespace codes::factories;
  roots E{
    .container = ranges::vec(A, bind<>(&cc_root, std::ref(C), std::ref(S), std::ref(M))),
    .event = continuous::cc_root_event(C),
    .check = when(C, views::map(E.container, &root::check)),
    .get_guards = when(C, views::map(E.container, &root::get_guard)),
    .get = when(C, views::map(E.container, compose<>(&cexecs::root::get, &root::continuous))),
    .get_event = recv(C, S.continuous.dev, E.event),
    .put_state = when(C, views::map(E.container, &root::put_state))
  };
  return E;
}


hcexec cc_hcexec(code &C,cexecs::config config,buffer B,
                 state X, std::vector<codes::exprfam> P){
  hcexec E{
    .config = config,
    .events = cexecs::cc_events(C),
    .buffer = std::move(B),
    .status = cexecs::cc_status(C, E.buffer),
    .state  = std::move(X),
    .parameters = std::move(P),
    .put_discrete_state = compile_send(C, E.state.buffer, config.dev),
    .get_events = continuous::cc_get_events(C, config.dev, E.status, E.buffer),
    .put_state = compile_send(C, E.state.buffer, config.dev),
    .get_state = compile_recv(C, E.state.buffer, config.dev),
    .put_parameters = codes::when(C, views::map(E.parameters, bind<>(lift((codes::send)), std::ref(C), config.dev)))
  };
  return E;
}

execution cc(code &C,const hautomaton &A,config config){

  //FIXME: need to transfer buffer state between cexec & dexec. - jeh
  
  auto B_discrete = buffer::make(C, config.discrete.buffer_capacity);
  auto X_discrete = cc_state(C, A.discrete_automaton.variables, B_discrete);

  auto B_continuous = buffer::make(C, config.continuous.buffer_capacity);
  auto X_continuous = cc_state(C, A.continuous_variables, B_continuous);

  auto S = cc_substitute_current(C, X_discrete, X_continuous);
  auto S_buffer = cc_substitute_buffer(C, X_discrete, X_continuous);

  auto T_discrete = cc_transitions(
    C,
    ranges::vec(
      A.discrete_automaton.transitions,
      [&](const auto &t){
        return dexecs::cc_transition(C, config.dev, dexecs::cc_substitute_transition(S, t));
      }));

  auto P_discrete = ranges::vec(try_substitute(S_buffer, A.discrete_automaton.parameters), codes::expr);
  auto P_continuous = ranges::vec(try_substitute(S_buffer, A.continuous_parameters), codes::expr);

  execution E{
    .discrete_execution = dexecs::cc(C, config.discrete, std::move(B_discrete), std::move(X_discrete),
                                     std::move(T_discrete), std::move(P_discrete)),
    .activities = cc_activities(C, config.dev, S, A.activities),
    .actions = cc_actions(C, config.dev, S, A.actions),
    .roots = cc_roots(C, config, S, A.roots),
    .continuous_execution = cc_hcexec(C, config.continuous, std::move(B_continuous), std::move(X_continuous),
                                      std::move(P_continuous)),
    .parameters = A.parameters,
    .check = codes::when(C, {E.activities.check, E.roots.check}),
    .get_guards = codes::when(C, {E.activities.get_guards, E.roots.get_guards}),
    .put_state{
      codes::when(
        C,
        {
          E.discrete_execution.put_state,
          E.continuous_execution.put_state
        })
    },
    .get_state{
      codes::when(
        C,
        {
          E.discrete_execution.get_state,
          E.continuous_execution.get_state
        })
    },
    .put_parameters{
      codes::when(
        C,
        {
          E.discrete_execution.put_parameters,
          E.continuous_execution.put_parameters,
          codes::when(
            C,
            views::map(A.parameters,
                       bind<>(lift((codes::send)), std::ref(C), config.dev)))
        })
    },
    .init = codes::when(
      C,
      {
        E.put_parameters,
        E.put_state
      }),
    .api = codes::api(C, {
        E.discrete_execution.api,
        E.check,
        E.roots.put_state,
        E.roots.get,
        E.roots.get_event,
        E.actions.check,
        E.actions.apply,
        E.actions.get_event,
        E.continuous_execution.get_events,
        E.continuous_execution.put_discrete_state,
        E.discrete_execution.put_state,
        E.continuous_execution.put_state,
        E.discrete_execution.get_state,
        E.continuous_execution.get_state,
        E.get_guards,
        E.put_state,
        E.get_state,
        E.put_parameters,
        E.init
      })
  };
  return E;
}

} //hysj::executions::hybrid
#include<hysj/tools/epilogue.hpp>
