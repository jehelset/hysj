#include<hysj/executions/state.hpp>

#include<functional>
#include<ranges>

#include<hysj/codes/vars.hpp>
#include<hysj/codes/code.hpp>
#include<hysj/codes/factories.hpp>
#include<hysj/tools/functional/bind.hpp>
#include<hysj/tools/ranges/utility.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::executions{

codes::taskfam compile_recv(code &C,const vars &V,codes::devsym d){
  auto G = ranges::vec(
    V.ids(),
    bind<>(lift((codes::recv)), std::ref(C), d));
  return codes::task(codes::when(C, G));
}

codes::taskfam compile_send(code &C,const vars &V,codes::devsym d){
  auto G = ranges::vec(
    V.ids(),
    bind<>(lift((codes::send)), std::ref(C), d));
  return codes::task(codes::when(C, G));
}

state cc_state(code &C,const codes::vars &V,const buffer &R){
  state S{
    .variables = V,
    .buffer{
      .independent = codes::varcast(cc_buffer(C, R, {V.independent})).value(),
      .dependent = ranges::vec(
        V.dependent,
        bind<-1>(
          ranges::vec,
          bind<>(lift((cc_buffer)), std::ref(C), std::ref(R))))
    },
    .current{
      .independent = cc_current(C, R, {S.buffer.independent}),
      .dependent = ranges::vec(
        S.buffer.dependent,
        bind<-1>(
          ranges::vec,
          bind<>(lift((cc_current)), std::ref(C), std::ref(R))))
    },
    .next{
      .independent = cc_next(C, R, {S.buffer.independent}),
      .dependent = ranges::vec(
        S.buffer.dependent,
        bind<-1>(
          ranges::vec,
          bind<>(lift((cc_next)), std::ref(C), std::ref(R))))
    },
    .prev{
      .independent = cc_prev(C, R, {S.buffer.independent}),
      .dependent = ranges::vec(
        S.buffer.dependent,
        bind<-1>(
          ranges::vec,
          bind<>(lift((cc_prev)), std::ref(C), std::ref(R))))
    }
  };
  
  each(member_refs(S),
       [&](const auto &V){
         compile(C, V.ids());
       });

  return S;
}

codes::substitute cc_substitute_vars(code &C,const vars &X,const vars &Y){
  return {
    .code = C,
    .subs = ranges::vec(std::views::zip(X.ids(), Y.ids()), unpack(lift((kumi::make_tuple))))
  };
}
codes::substitute cc_substitute_current(code &C,const state &S){
  return cc_substitute_vars(C, S.variables, S.current);
}
codes::substitute cc_substitute_buffer(code &C,const state &S){
  return cc_substitute_vars(C, S.variables, S.buffer);
}

} //hysj::executions
#include<hysj/tools/epilogue.hpp>
