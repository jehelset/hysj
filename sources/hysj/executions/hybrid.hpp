#pragma once

#include<variant>
#include<vector>

#include<fmt/format.h>

#include<hysj/automata.hpp>
#include<hysj/automata/hybrid.hpp>
#include<hysj/codes.hpp>
#include<hysj/codes/algorithms/substitute.hpp>
#include<hysj/executions/state.hpp>
#include<hysj/executions/continuous.hpp>
#include<hysj/executions/continuous/fe.hpp>
#include<hysj/executions/discrete.hpp>
#include<hysj/tools/functional/unwrap.hpp>
#include<hysj/tools/ranges/cat.hpp>
#include<hysj/tools/ranges/pins.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/reflection.hpp>
#include<hysj/tools/type_traits.hpp>
#include<hysj/tools/types.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::executions::hybrid{

#define HYSJ_EXECUTIONS_HYBRID_EVENTS \
  (action)                            \
  (step)                              \
  (stop)

enum class event : unsigned { HYSJ_SPLAT(0,HYSJ_EXECUTIONS_HYBRID_EVENTS) };
HYSJ_MIRROR_ENUM(event,HYSJ_EXECUTIONS_HYBRID_EVENTS)

#define HYSJ_LAMBDA(id)                                   \
  inline constexpr auto id##_tag = constant_c<event::id>; \
  using id##_tag_type = constant_t<event::id>;
 HYSJ_MAP_LAMBDA(HYSJ_EXECUTIONS_HYBRID_EVENTS)
#undef HYSJ_LAMBDA

struct HYSJ_EXPORT config{
  codes::devsym dev;
  cexecs::fe_config continuous;
  dexecs::config discrete;

  friend HYSJ_MIRROR_STRUCT(config, (dev, continuous, discrete));
};


struct HYSJ_EXPORT activity{
  codes::exprfam guard, equation;
  
  codes::taskfam check, get_guard;

  friend HYSJ_MIRROR_STRUCT(activity, (guard, equation, check, get_guard));
};

struct HYSJ_EXPORT activities{
  std::vector<activity> container;
  codes::taskfam check, get_guards;

  friend HYSJ_MIRROR_STRUCT(activities, (container, check, get_guards));
};

struct HYSJ_EXPORT action{
  codes::exprfam guard, event;

  codes::taskfam check, apply, get_event;

  friend HYSJ_MIRROR_STRUCT(
    action,
    (guard, event, check, apply, get_event));
};

struct HYSJ_EXPORT actions{
  std::vector<action> container;

  codes::exprfam event;
  codes::taskfam check, apply, get_event;

  friend HYSJ_MIRROR_STRUCT(actions, (container, event, check, apply, get_event));
};

struct HYSJ_EXPORT root{

  codes::exprfam guard;
  continuous::root continuous;
  
  codes::taskfam check, get_guard, put_state;

  friend HYSJ_MIRROR_STRUCT(root, (guard, continuous, check, get_guard, put_state));
};

struct HYSJ_EXPORT roots{
  std::vector<root> container;

  codes::exprfam event;

  codes::taskfam check, get_guards, get, get_event, put_state;

  friend HYSJ_MIRROR_STRUCT(roots, (container, event, check, get_guards, get, get_event, put_state));
};

struct HYSJ_EXPORT hcexec{
  cexecs::config config;
  continuous::events events;
  executions::buffer buffer;
  codes::exprfam status;
  executions::state state;

  std::vector<codes::exprfam> parameters;

  codes::taskfam put_discrete_state;
  codes::taskfam get_events;
  codes::taskfam put_state, get_state, put_parameters;

  friend HYSJ_MIRROR_STRUCT(
    hcexec,
    (config, events, buffer, status, state,
     parameters,
     get_events,
     put_discrete_state,
     put_state, get_state, put_parameters));
};

struct HYSJ_EXPORT execution{

  dexec discrete_execution;

  hybrid::activities activities;
  hybrid::actions actions;
  hybrid::roots roots;

  hcexec continuous_execution;

  std::vector<codes::exprfam> parameters;

  codes::taskfam check, get_guards;

  codes::taskfam put_state, get_state, put_parameters, init;
  codes::apisym api;

  friend HYSJ_MIRROR_STRUCT(
    execution,
    (discrete_execution, 
     activities, actions, roots, 
     continuous_execution, parameters,
     check, get_guards,
     put_state, get_state, put_parameters, init,
     api));
};

HYSJ_EXPORT
execution cc(code &,const hautomaton &,config = {});

} //hysj::executions::hybrid
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

namespace hexecs = executions::hybrid;
using hexec = hexecs::execution;

using hevent = hexecs::event;
using xevent = std::variant<devent, cevent, hevent>;

} //hysj
HYSJ_ENUM_FORMATTER(hysj::_HYSJ_VERSION_NAMESPACE::hevent);
#include<hysj/tools/epilogue.hpp>
