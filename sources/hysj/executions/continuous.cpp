#include<hysj/executions/continuous.hpp>

#include<algorithm>
#include<functional>
#include<iterator>
#include<ranges>
#include<vector>

#include<hysj/codes.hpp>
#include<hysj/codes/algorithms/opsubs.hpp>
#include<hysj/codes/algorithms/substitute.hpp>
#include<hysj/codes/code.hpp>
#include<hysj/codes/grammar.hpp>
#include<hysj/executions/continuous.hpp>
#include<hysj/executions/state.hpp>
#include<hysj/tools/functional/bind.hpp>
#include<hysj/tools/functional/optional.hpp>
#include<hysj/tools/ranges/map.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/reflection.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::executions::continuous{

events cc_events(code &C){
  events E;
  for(auto e:enumerator_values<event>)
    E[etoi(e)] = codes::expr(codes::icst64(C, etoi(e)));
  return E;
}

codes::substitute cc_buffer_substitute(code &C,
                                       const codes::vars &V,
                                       const buffer &U,
                                       const codes::vars &V_exec){
  return {
    .code = C,
    .subs = [&]{
      auto S = ranges::vec(std::views::zip(
                             std::views::join(V.dependent),
                             std::views::join(V_exec.dependent)),
                           unpack(
                             [&](const auto &x,const auto &x_exec){
                               return kumi::make_tuple(
                                 x.id(),
                                 cc_current(C, U, x_exec).id());
                             }));
      S.push_back(kumi::make_tuple(V.independent.id(),
                                   cc_current(C, U, {V_exec.independent}).id()));
      return S;
    }()
  };
}

codes::exprfam cc_status(code &C,const buffer &R){
  return {ivar64(C, {R.itr})}; //FIXME: native width - jeh
}

config config::make(code &C,codes::devsym d){
  return {
    .dev = d,
    .step_size{codes::rcst64(C, 1.0e-5)},
    .stop_time{C.builtins.lits.inf64}
  };
}

codes::exprfam cc_root_sign(code &C,codes::exprfam f){
  return {codes::ivar64(C, {codes::rfl(C, f)})};
}

codes::exprfam cc_root_event(code &C){
  return {bvar64(C)};
}

root cc_root(
  code &code,
  codes::devsym dev,
  codes::exprfam function,
  codes::exprfam sign){
  using namespace codes::factories;
  root r{
    .residual = codes::access_for(code, function),
    .sign_function = codes::sign(code, r.residual),
    .sign = sign,
    .sign_next = cc_root_sign(code, function),
    .sign_flip = cc_root_event(code),
          //FIXME: if function is access then don't need - jeh
    .evaluate_function = put(code,r.residual,function),
    .evaluate_sign_next = put(code,r.sign_next,r.sign_function),
    .evaluate_sign_flip = put(code,
                              r.sign_flip,
                              con(code,
                                  lor(code,{
                                      lnot(code,eq(code,r.sign,r.sign_next))
                                    }))),
    .evaluate = then(
      code,{
        r.evaluate_function,
        r.evaluate_sign_next,
        r.evaluate_sign_flip,
        put(code,r.sign,r.sign_next)
      }),
    .get{
      when(
        code,
        {
          recv(code, dev, accessed(code, r.residual).value()),
          recv(code, dev, accessed(code, r.sign).value()), //FIXME: handle in recv / send impl - jeh
          recv(code, dev, r.sign_flip)
        })
    },
    .put{
      send(code, dev, accessed(code, r.sign).value())
    }
  };
  return r;
}

roots cc_roots(code &C,
               codes::devsym d,
               std::vector<root> container,
               codes::exprfam r_event){

  using namespace codes::factories;
  roots exec_roots{
    .container = std::move(container),
    .event = r_event,
    .evaluate{
      then(
        C,
        {
          codes::when(C, views::map(exec_roots.container, &root::evaluate)),
          put(C,
              exec_roots.event,
              con(C,lor(C,views::map(exec_roots.container, &root::sign_flip))))
        })
    },
    .get{
      when(
        C,
        views::prepend(
          codes::task(codes::recv(C, d, exec_roots.event)),
          views::map(exec_roots.container, &root::get)))
    },
    .put{
      when(
        C,
        views::map(exec_roots.container, &root::put))
    },
    .clear{
      put(C, exec_roots.event, C.builtins.lits.bot64)
    }
  };
  return exec_roots;
}

void compile_compute_derivatives(code &code,execution &exec,codes::taskfam compute_derivatives){
  partial_opsub(code, exec.api, exec.compute_derivatives, compute_derivatives);
  exec.compute_derivatives = compute_derivatives;
}

namespace _{

  codes::taskfam cc_solve_status(code &C,
                                 codes::exprfam t_stop,
                                 const events &E,
                                 const buffer &R,
                                 codes::exprfam s,
                                 const state &S,
                                 codes::exprfam r_event){
    using namespace codes::factories;
    return {
        put(
          C,
          cc_current(C, R, s),
          sel(C,
              lt(C, S.current.independent, t_stop),
              {
                E[etoi(event::stop)],
                sel(C,
                    r_event,
                    {
                      E[etoi(event::step)],
                      E[etoi(event::root)]
                    })
              }))
    };
  }

  codes::taskfam cc_step_independent(code &C,
                                     codes::exprfam dt,
                                     const state &S){
    using namespace codes::factories;
    return {
      put(
        C,
        S.next.independent,
        add(
          C,
          {
            S.current.independent,
            dt
          }))
    };
  }

  codes::taskfam cc_step_dependent(code &C,
                                   codes::exprfam dt,
                                   const state &S){
    using namespace codes::factories;
    return {
      then(
        C,
        ranges::vec(
          views::join(
            views::map(
              std::views::zip(S.current.dependent, S.next.dependent),
              unpack(
                [&](const auto &X_current, const auto &X_next){
                return views::map(
                  views::indices(X_current.size() - 1),
                  [&, n = X_current.size()](auto i){
                    auto j = n - (i + 2);
                    return put(
                      C,
                      X_next[j],
                      add(
                        C,
                        {
                          X_current[j],
                          mul(C, {X_current[j + 1], dt})
                        }));
                  });
              })))))
    };
  }

}

codes::taskfam cc_get_events(code &C,codes::devsym d,
                             codes::exprfam s,
                             const buffer &B){
  return {
    codes::when(
        C,
        {
          codes::recv(C, d, s),
          codes::recv(C, d, B.begin),
          codes::recv(C, d, B.size)
        })
  };
}

execution cc(code &C,
             config config,
             buffer B,
             events V,
             codes::exprfam s,
             state S,
             std::vector<codes::exprfam> F,
             std::vector<root> R,
             std::vector<codes::exprfam> P,
             codes::exprfam r_event){
  execution E{
    .buffer = std::move(B),
    .events = std::move(V),
    .status = s,
    .is_stop = codes::eq(C, cc_current(C, E.buffer, E.status), E.events[etoi(cevent::stop)]),
    .is_root = codes::eq(C, cc_current(C, E.buffer, E.status), E.events[etoi(cevent::root)]),
    .is_done = codes::expr(codes::lor(C, {E.buffer.is_full, E.is_stop, E.is_root})),
    .state = std::move(S),
    .equations = std::move(F),
    .roots = cc_roots(C, config.dev, std::move(R), r_event),
    .parameters = std::move(P),
    .compute_status = _::cc_solve_status(C,
                                         config.stop_time,
                                         E.events,
                                         E.buffer,
                                         E.status,
                                         E.state,
                                         E.roots.event),
    .compute_derivatives = codes::noop(C),
    .compute_root = codes::noop(C),
    .solve = codes::then(
      C,
      {
        E.compute_derivatives,
        E.roots.evaluate,
        E.compute_status,
        E.roots.clear
      }),
    .step_independent = _::cc_step_independent(C, config.step_size, E.state),
    .step_dependent = _::cc_step_dependent(C, config.step_size, E.state),
    .step = codes::then(
      C,
      {
        E.step_dependent,
        E.step_independent,
      }),
    .loop{
      codes::loop(C,
                  codes::then(
                    C,
                    {
                      codes::if_(C, E.is_done, codes::task(C.builtins.brk)),
                      E.step,
                      E.buffer.push,
                      E.solve
                    }))
    },
    .get_events = {
      codes::when(
        C,
        {
          codes::recv(C, config.dev, E.status),
          codes::recv(C, config.dev, E.buffer.begin),
          codes::recv(C, config.dev, E.buffer.size)
        })
    },
    .start = {
      codes::then(
        C,
        {
          codes::on(C, config.dev,
                    codes::then(
                      C,
                      {
                        E.buffer.clear,
                        E.buffer.push,
                        E.solve,
                        E.loop
                      })),
          E.get_events
        })
    },
    .resume = {
      codes::then(
        C,
        {
          codes::on(
            C,
            config.dev,
            codes::then(
              C,
              {
                E.buffer.cycle,
                E.loop
              })),
          E.get_events
        })
    },
    .get_state = compile_recv(C, E.state.buffer, config.dev),
    .put_state = compile_send(C, E.state.buffer, config.dev),
    .put_parameters = [&]{
      auto G = ranges::vec(views::cast<id>(E.parameters),
                           compose<>(codes::task, bind<>(lift((codes::send)), std::ref(C), config.dev)));
      return codes::task(codes::when(C, G));
    }(),
    .init = codes::task(codes::when(C, {
          E.roots.put,
          E.put_parameters,
          E.put_state
    })),
    .api = codes::api(
      C,
      {
        E.get_events,
        E.get_state,
        E.put_state,
        E.put_parameters,
        E.roots.put,
        E.roots.get,
        E.start,
        E.resume,
        E.init
      })
  };
  return E;
}

execution cc(code &C,const cautomaton &A,config config){
  auto B = buffer::make(C, config.buffer_capacity);
  auto E = cc_events(C);
  auto s = cc_status(C, B);
  auto S = cc_state(C, A.variables, B);
  auto X = cc_substitute_current(C, S);
  auto F = ranges::vec(A.equations, [&](auto f){ return codes::expr(X(f.id()).value_or(f.id())); });
  auto R = ranges::vec(
    A.roots,
    [&](auto f){
      f = codes::expr(X(f.id()).value_or(f.id()));
      return cc_root(C, config.dev, f, cc_root_sign(C, f));
    });
  auto e_root = cc_root_event(C);
  return cc(C, config, std::move(B), std::move(E), s, std::move(S), std::move(F), std::move(R),
            A.parameters,
            e_root);
}

} //hysj::executions::continuous
#include<hysj/tools/epilogue.hpp>
