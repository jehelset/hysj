#pragma once
#include<concepts>
#include<cstdint>
#include<type_traits>
#include<utility>
#include<vector>

#include<fmt/format.h>

#include<hysj/automata/discrete.hpp>
#include<hysj/codes.hpp>
#include<hysj/executions/buffers.hpp>
#include<hysj/executions/state.hpp>
#include<hysj/executions/discrete.hpp>
#include<hysj/tools/preprocessor.hpp>
#include<hysj/tools/ranges/cat.hpp>
#include<hysj/tools/ranges/pins.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/reflection.hpp>
#include<hysj/tools/tag_invoke.hpp>
#include<hysj/tools/type_traits.hpp>
#include<hysj/tools/types.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::executions::discrete{

#define HYSJ_EXECUTIONS_DISCRETE_EVENT \
  (step)                               \
  (stop)
enum class event : unsigned { HYSJ_SPLAT(0,HYSJ_EXECUTIONS_DISCRETE_EVENT) };
HYSJ_MIRROR_ENUM(event,HYSJ_EXECUTIONS_DISCRETE_EVENT)

#define HYSJ_LAMBDA(id)                                   \
  inline constexpr auto id##_tag = constant_c<event::id>; \
  using id##_tag_type = constant_t<event::id>;
HYSJ_MAP_LAMBDA(HYSJ_EXECUTIONS_DISCRETE_EVENT)
#undef HYSJ_LAMBDA

struct HYSJ_EXPORT config{
  codes::devsym dev;
  std::int64_t buffer_capacity = 1;

  friend HYSJ_MIRROR_STRUCT(config, (dev, buffer_capacity));

  auto operator<=>(const config &)const = default;
};

using events = std::array<codes::exprfam, enumerator_count<event>>;

HYSJ_EXPORT
events cc_events(code &);

struct HYSJ_EXPORT transition{
  codes::exprfam function, guard, active;

  codes::taskfam compute_guard, compute_active, compute_function, compute_variable;
  codes::taskfam get;

  friend HYSJ_MIRROR_STRUCT(transition,
                            (function, guard, active,
                             compute_guard, compute_active, compute_function, compute_variable,
                             get));

  auto operator<=>(const transition &)const = default;
};

struct HYSJ_EXPORT transitions{
  std::vector<transition> container;

  codes::exprfam event;
  codes::taskfam compute_guards, compute_functions, compute_variables, compute_event;
  codes::taskfam get;

  friend HYSJ_MIRROR_STRUCT(transitions,
                            (container, event,
                             compute_guards, compute_functions, compute_variables, compute_event,
                             get));

  auto operator<=>(const transitions &)const = default;
};


HYSJ_EXPORT
dtransition cc_substitute_transition(const codes::substitute &,const dtransition &);

HYSJ_EXPORT
transition cc_transition(code &,codes::devsym,dtransition);

HYSJ_EXPORT
transitions cc_transitions(code &, std::vector<transition>);

//FIXME: add width - jeh
struct HYSJ_EXPORT execution{

  executions::buffer buffer;
  discrete::events events;
  codes::exprfam status, is_stop, is_done;

  executions::state state;
  discrete::transitions transitions;
  std::vector<codes::exprfam> parameters;

  codes::taskfam solve, step, loop, get_events, start, resume;

  codes::taskfam
    put_state, get_state,
    put_parameters;

  codes::apisym api;

  friend HYSJ_MIRROR_STRUCT(
    execution,(
      buffer, events, status, is_stop, is_done,
      state, transitions, parameters,
      solve, step, loop, get_events, start, resume,
      put_state, get_state, put_parameters,
      api));

  auto operator<=>(const execution &)const = default;
};

HYSJ_EXPORT
execution cc(code &,config,buffer,state,transitions,std::vector<codes::exprfam>);

HYSJ_EXPORT
execution cc(code &,const dautomaton &,config);

} //hysj::executions::discrete
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

namespace dexecs = executions::discrete;
using dexec = dexecs::execution;
using devent = dexecs::event;

} //hysj
HYSJ_ENUM_FORMATTER(hysj::_HYSJ_VERSION_NAMESPACE::devent);
#include<hysj/tools/epilogue.hpp>
