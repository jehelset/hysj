include(${CMAKE_CURRENT_LIST_DIR}/config.cmake)

ci_execute_process(
  COMMAND ${CMAKE_COMMAND} .
    -B build
    -DCMAKE_TOOLCHAIN_FILE=${HYSJ_TOOLCHAIN_FILE}
    -DCMAKE_BUILD_TYPE=Debug
    -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON
    -DHYSJ_WITH_TESTS=ON
    -DHYSJ_WITH_COVERAGE=ON
    -DHYSJ_WITH_PYTHON=ON
    -DHYSJ_WITH_DOCUMENTATION=ON)

ci_execute_process_continue(
  COMMAND ${CMAKE_COMMAND} --build build --verbose --target hysj-test-reports
  RESULT_VARIABLE test_result
  ERROR_VARIABLE test_error ERROR_QUIET)
ci_execute_process(
  COMMAND ${CMAKE_COMMAND} --build build --verbose --target hysj-coverage-cobertura)
ci_execute_process(
  COMMAND ${CMAKE_COMMAND} --install build --prefix ${SCRIPT_DIR} --component reports)

#NOTE: garb - jeh
if(NOT ${test_result} EQUAL 0)
  message(FATAL_ERROR "Hysj: Test failed with ${test_result} - ${test_error}.")
endif()
