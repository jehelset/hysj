include(${CMAKE_CURRENT_LIST_DIR}/config.cmake)

if(NOT CI_API_URL)
  message(FATAL_ERROR "Missing CI_API_URL")
endif()
if(NOT CI_PROJECT_ID)
  message(FATAL_ERROR "Missing CI_PROJECT_ID")
endif()
if(NOT CI_JOB_TOKEN)
  message(FATAL_ERROR "Missing CI_JOB_TOKEN")
endif()

set(PUBLISH_URL "${CI_API_URL}/projects/${CI_PROJECT_ID}/packages/generic")
set(PUBLISH_HEADER "JOB-TOKEN: ${CI_JOB_TOKEN}")

find_program(CURL_EXECUTABLE curl REQUIRED)

set(PACKAGE_NAME ${HYSJ_HOST}-${HYSJ_HOST_ID}-${HYSJ_HOST_VERSION_CODENAME})
string(TOLOWER ${PACKAGE_NAME} PACKAGE_NAME)
file(GLOB PACKAGE_FILES LIST_DIRECTORIES false "${PACKAGES_DIR}/*")

foreach(PACKAGE_FILE ${PACKAGE_FILES})
  get_filename_component(PACKAGE_BASE ${PACKAGE_FILE} NAME)
  ci_execute_process(
    COMMAND ${CURL_EXECUTABLE}
      --header ${PUBLISH_HEADER}
      --upload-file ${PACKAGE}
      ${PUBLISH_URL}/${PACKAGE_NAME}/${HYSJ_VERSION}/${PACKAGE_FILE})
endforeach()


