include(${CMAKE_CURRENT_LIST_DIR}/config.cmake)

ci_execute_process(
  COMMAND ${CMAKE_COMMAND} .
  -B build
  -G Ninja 
  -DCMAKE_BUILD_TYPE=Release
  -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON
  -DCMAKE_INTERPROCEDURAL_OPTIMIZATION=OFF
  -DCPACK_COMPONENTS_GROUPING="IGNORE"
  -DHYSJ_USER_PROLOGUE=${CMAKE_CURRENT_LIST_DIR}/pages-user-prologue.cmake
  -DHYSJ_WITH_PYTHON=ON
  -DHYSJ_WITH_DOCUMENTATION=ON
  -DHYSJ_WITH_STANDALONE_DOCUMENTATION=ON)

ci_execute_process(
  COMMAND ${CMAKE_COMMAND} -E make_directory ${PAGES_DIR})
ci_execute_process(
  COMMAND ${CMAKE_COMMAND} --build build --verbose --parallel ${N})
ci_execute_process(
  COMMAND ${CMAKE_COMMAND} --install build --prefix ${PAGES_DIR} --component documentation)
ci_execute_process(
  COMMAND ${CMAKE_COMMAND} --install build --prefix ${PAGES_DIR} --component development)
