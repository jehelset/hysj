cmake_minimum_required(VERSION 3.19 FATAL_ERROR)

include(ProcessorCount)
ProcessorCount(N)

math(EXPR N "(${N} + 1) / 2")

set(SCRIPT_DIR   ${CMAKE_CURRENT_LIST_DIR})
set(REPORTS_DIR  ${SCRIPT_DIR}/reports)
set(PACKAGES_DIR ${SCRIPT_DIR}/packages)

set(CI_API_URL $ENV{CI_API_V4_URL})

get_filename_component(TOOLS_DIR ${SCRIPT_DIR} DIRECTORY)
get_filename_component(PROJECT_DIR ${TOOLS_DIR} DIRECTORY)

set(PAGES_DIR ${PROJECT_DIR}/public)
set(BUILD_DIR ${PROJECT_DIR}/build)

set(CI_API_URL $ENV{CI_API_V4_URL})
set(CI_PROJECT_ID $ENV{CI_PROJECT_ID})
set(CI_JOB_TOKEN $ENV{CI_JOB_TOKEN})

include(${PROJECT_DIR}/tools/cmake/HysjPrologue.cmake)

if(NOT HYSJ_TOOLCHAIN_FILE)
  if(DEFINED ENV{HYSJ_TOOLCHAIN_FILE})
    set(HYSJ_TOOLCHAIN_FILE $ENV{HYSJ_TOOLCHAIN_FILE})
  else()
    if("${HYSJ_HOST}" STREQUAL "LINUX")
      if("${HYSJ_HOST_ID}" STREQUAL "ARCH")
	set(HYSJ_TOOLCHAIN_FILE "${PROJECT_DIR}/tools/containers/arch/toolchain.cmake")
      else()
	message(FATAL_ERROR "Hysj: unsupported platform")
      endif()
    else()
      message(FATAL_ERROR "Hysj: unsupported platform")
    endif()
  endif()
endif()
 
#NOTE:
#  run execute_process in project directory with fatal errors
#  - jeh
macro(ci_execute_process)
  execute_process(${ARGN} WORKING_DIRECTORY ${PROJECT_DIR} COMMAND_ERROR_IS_FATAL ANY)
endmacro()
macro(ci_execute_process_continue)
  execute_process(${ARGN} WORKING_DIRECTORY ${PROJECT_DIR})
endmacro()
