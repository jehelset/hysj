"""
hysj-sphinx-cpp_ref
~~~~~~~~~~~~~~~~~~~

Insert link to cppreference.
"""

from typing import Any, Dict, List, Tuple 

from docutils import nodes
from docutils.nodes import Node, system_message

from sphinx.application import Sphinx
from sphinx.util.docutils import SphinxRole

__version__ = '0.0.0'

class CppRefRole(SphinxRole):
  def run(self) -> Tuple[List[Node], List[system_message]]:
    split = self.text.rsplit(' ')
    def fail():
      msg = self.inliner.reporter.error(f'invalid cpp-ref, expected `name <some/cppref/path>`, got {self.text}',
                                        line=self.lineno)
      prb = self.inliner.problematic(self.rawtext,
                                     self.rawtext,
                                     msg)
      return [prb], [msg]
      
    if len(split) != 2:
      return fail()
    name,quoted_path = split
    if len(quoted_path) < 2 or (quoted_path[0],quoted_path[-1]) != ('<','>'):
      return fail()

    path = quoted_path[1:-1]
    url = f'https://en.cppreference.com/w/cpp/{path}'
    node = nodes.reference(self.rawtext, name, refuri = url, **self.options)
    return [node],[]

def setup(app: Sphinx) -> Dict[str, Any]:
  app.add_role('cppref', CppRefRole())
  return {'version': __version__}

