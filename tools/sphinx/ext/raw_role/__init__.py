"""
hysj-sphinx-raw_role
~~~~~~~~~~~~~~~~~~~~

Insert raw html directly.
"""

from typing import Any, Dict, List, Tuple 

from docutils import nodes
from docutils.nodes import Node, system_message

from sphinx.application import Sphinx
from sphinx.util.docutils import SphinxRole

__version__ = '0.0.0'

class RawRole(SphinxRole):
  def run(self) -> Tuple[List[Node], List[system_message]]:
    return [nodes.raw('', f'{self.text}', format='html')],[]

def setup(app: Sphinx) -> Dict[str, Any]:
  app.add_role('raw', RawRole())
  return {'version': __version__}

