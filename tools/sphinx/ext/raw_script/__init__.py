"""
hysj-sphinx-raw_script
~~~~~~~~~~~~~~~~~~~~~~

Run the script and insert raw-node with content.
"""

import os,sys
from subprocess import PIPE,Popen

from docutils import nodes
from docutils.parsers.rst import Directive,directives

from sphinx.util.docutils import SphinxDirective

__version__ = '0.0.0'

class RawScriptDirective(SphinxDirective):
  config: dict = {}

  has_content = True
  required_arguments = 1
  optional_arguments = 0

  option_spec = {
    'filename': directives.path
  }

  def run(self):
    config = RawScriptDirective.config

    input_encoding = config.get('input_encoding', 'ascii')
    output_encoding = config.get('output_encoding', 'ascii')
    prefix_chars = config.get('prefix_chars', 0)
    show_source = config.get('show_source', True)

    args = [sys.executable]
    stdin = None
    filename = self.arguments[0]
    if not filename:
      codelines = (line[prefix_chars:] for line in self.content)
      stdin = '\n'.join(codelines).encode(input_encoding)
    else:
      rel_filename, filename = self.env.relfn2path(filename)
      args = args + [filename]
      self.env.note_dependency(rel_filename)

    proc = Popen(args, cwd = os.getcwd(), stdin=PIPE, stdout=PIPE, stderr=PIPE)
    stdout, stderr = proc.communicate(stdin)

    out = None
    if stdout:
      out = stdout.decode(output_encoding)
    if proc.poll() != 0:
      raise Exception(stderr.decode(output_encoding))
    return [nodes.raw('', f'{out}', format='html')]

def setup(app):
  app.add_directive('raw-script', RawScriptDirective)
  return {'version': __version__}

