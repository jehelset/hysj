"""
hysj-sphinx-rest_script
~~~~~~~~~~~~~~~~~~~~~~~

Run the script and include its output into the document.
"""

import os,sys,tempfile
from subprocess import PIPE,Popen

from docutils import nodes
from docutils.parsers.rst import Directive,directives
from docutils.parsers.rst.directives.misc import Include

from sphinx.util.docutils import SphinxDirective

__version__ = '0.0.0'

class ReSTScriptDirective(Include,SphinxDirective):
  config: dict = {}

  has_content = True
  required_arguments = 0
  optional_arguments = 1

  option_spec = {
    'filename': directives.path
  }

  def run(self):
    config = ReSTScriptDirective.config

    input_encoding = config.get('input_encoding', 'ascii')
    output_encoding = config.get('output_encoding', 'ascii')
    prefix_chars = config.get('prefix_chars', 0)
    show_source = config.get('show_source', True)

    args = [sys.executable]
    stdin = None
    if not self.arguments:
      codelines = (line[prefix_chars:] for line in self.content)
      stdin = '\n'.join(codelines).encode(input_encoding)
    else:
      rel_filename, filename = self.env.relfn2path(self.arguments[0])
      args = args + [filename]
      self.env.note_dependency(rel_filename)
    proc = Popen(args, cwd = os.getcwd(), stdin=PIPE, stdout=PIPE, stderr=PIPE)
    stdout, stderr = proc.communicate(stdin)
    out = None
    if stdout:
      out = stdout.decode(output_encoding)
    location = self.state_machine.get_source_and_line(self.lineno)
    if proc.poll() != 0:
      raise Exception(f'in {location}: {stderr.decode(output_encoding)}')
    with tempfile.NamedTemporaryFile() as filename:
      self.arguments = [filename.name]
      filename.write(stdout)
      filename.flush()
      return super().run()

def setup(app):
  app.add_directive('rest-script', ReSTScriptDirective)
  return {'version': __version__}

