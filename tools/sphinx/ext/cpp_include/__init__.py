"""
hysj-sphinx-cpp_include
~~~~~~~~~~~~~~~~~~~~~~~

Insert link to include.
"""

from typing import Any, Dict, List, Tuple 

import os
from docutils import nodes
from docutils.nodes import Node, system_message

from sphinx.util import logging
from sphinx.application import Sphinx
from sphinx.util.docutils import SphinxRole

logger = logging.getLogger(__name__)

__version__ = '0.0.0'

class CppIncludeRole(SphinxRole):

  def run(self) -> Tuple[List[Node], List[system_message]]:
    root_include_dir = self.config.cpp_include_dir
    relative_root_dir = ''
    doc_dir = os.path.dirname(self.env.temp_data['docname'])
    if doc_dir:
      relative_root_dir = '/'.join(['..']*len(doc_dir.split('/'))) + '/'
    include_dir = f'{relative_root_dir}{root_include_dir}'

    def fail():
      msg = self.inliner.reporter.error(f'invalid cpp:include, expected `#include<some/header/path>`, got {self.text}',
                                        line=self.lineno)
      prb = self.inliner.problematic(self.rawtext,
                                     self.rawtext,
                                     msg)
      return [prb], [msg]

    prologue = '#include<'
    epilogue = '>'
    if len(self.text) < len(prologue) + len(epilogue):
      fail()
      
    #FIXME:
    #  add link to actual path, taken from cpp_include.include_path
    #  or similar - jeh
    path = self.text[len(prologue):-len(epilogue)]
    node = nodes.reference(self.rawtext, self.text, refuri = f'{include_dir}/{path}', **self.options)
    return [node],[]

def setup(app: Sphinx) -> Dict[str, Any]:
  app.add_role('cpp:include', CppIncludeRole())
  app.add_config_value('cpp_include_dir', '', False, [str])
  return {'version': __version__}

