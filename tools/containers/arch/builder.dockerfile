FROM archlinux/archlinux:base-devel

ENV LANG=en_US.UTF-8
ENV LC_ALL=C

RUN pacman -Syu --noconfirm ;

RUN echo "%wheel ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers ;
RUN useradd -m -g wheel -p ci ci

COPY builder-packages.sh /tmp
RUN su ci -c /tmp/builder-packages.sh

COPY builder-aur-packages.sh /tmp
RUN su ci -c /tmp/builder-aur-packages.sh

ENV PATH="$PATH:/usr/bin/vendor_perl:/usr/bin/core_perl"

USER "ci"
CMD ["/bin/bash"]
