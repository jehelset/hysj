#!/bin/sh
set -e 

declare -a aur_packages=(
    cpptrace libassert flamegraph kumi-git)

tmpdir=$(mktemp -d)
pushd ${tmpdir}

for package in ${aur_packages[@]}
do
    rm -rf ${package}
    curl -L https://aur.archlinux.org/cgit/aur.git/snapshot/${package}.tar.gz | tar -xz
    pushd ${package}
    makepkg --noconfirm -scfir
    popd
done
curl https://gitlab.com/jehelset/cpack-makepkg/-/package_files/22406673/download > cpack-makepkg.pkg.tar.zst
sudo pacman -U cpack-makepkg.pkg.tar.zst --noconfirm

popd
rm -rf ${tmpdir}
