#!/bin/sh
set -e 

declare -a packages=(
    curl git cmake ninja zstd ninja
    fmt vulkan-devel vulkan-swrast spirv-tools libpipeline libcgif
    benchmark gcovr doctest debugedit
    python python-numpy pybind11 python-pytest
    python-sphinx python-sphinx_rtd_theme graphviz librsvg cairo python-cairo)

sudo pacman -S --noconfirm --needed ${packages[@]}
sudo pacman -Scc --noconfirm
git config --global --add safe.directory "*"
