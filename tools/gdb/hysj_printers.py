import re

# Register the pretty printers
import gdb
import gdb.printing

class StdSpanPrinter(gdb.printing.PrettyPrinter):
  "Print a std::span"

  class _iterator(object):
    def __init__ (self, ptr, extent,):
      super().__init__()
      if len(extent.type.fields()):
        self.extent = extent['_M_extent_value']
      else:
        self.extent = extent.type.template_argument(0)
      self.ptr = ptr
      self.count = 0

    def __iter__(self):
      return self

    def __next__(self):
      if self.count == self.extent:
        raise StopIteration
      count = self.count
      elt = (self.ptr + count).dereference()
      self.count = count + 1
      return ('[%d]' % count, elt)

  def __init__(self, val):
    self.val = val

  def children(self):
    ptr = self.val['_M_ptr']
    extent = self.val['_M_extent']
    return self._iterator(ptr,extent)

  def to_string(self):
    return 'std::span'

  def display_hint(self):
    return 'array'

def strip_version_namespace(type_str):
  "Remove known inline namespaces from the canonical name of a type."
  type_str = type_str.replace('hysj::_0', 'hysj')
  return type_str

class GrammarsSym(gdb.printing.PrettyPrinter):

  def __init__(self,val):
    super().__init__('hysj::grammars::sym')
    self.val = val
    self.tag = f'{self.val.type.template_argument(0)}'.rsplit('::',1)[-1]
    self.idx = self.val['idx']

  def to_string(self):
    return f'{self.tag}{self.idx}'

  def display_hint(self):
    return None

class GrammarsSymType(object):

  def __init__(self):
    self.name = 'hysj::grammars::sym'
    self.enabled = True

  class _recognizer(object):

    def __init__(self,name):
      self.name = name
      
    def recognize(self, type_obj):
      if type_obj.tag is None:
        return None
      if not type_obj.tag.startswith('hysj::_0::grammars::sym<'):
        return None
      type = type_obj.strip_typedefs().unqualified()
      sym_namespace,sym_tag,sym = f'{type.template_argument(0)}'.rsplit('::',2)
      return strip_version_namespace(f'{sym_namespace}::{sym}')

  def instantiate(self):
    return self._recognizer(self.name)

class GrammarsFam(gdb.printing.PrettyPrinter):

  def __init__(self,val):
    super().__init__('hysj::grammars::fam')
    self.val = val
    self.sym_vis = gdb.default_visualizer(self.val['obj'])

  def to_string(self):
    fam = f'{self.val.type.template_argument(0)}'.rsplit('::',1)[-1]
    return f'{fam}'

  def children(self):
    return iter([('obj',self.val['obj'])])

  def display_hint(self):
    return None

class GrammarsFamType(object):

  def __init__(self):
    self.name = 'hysj::grammars::fam'
    self.enabled = True

  class _recognizer(object):

    def __init__(self,name):
      self.name = name
      
    def recognize(self, type):
      if type.tag is None:
        return None
      if not type.tag.startswith('hysj::_0::grammars::fam<'):
        return None
      type = type.strip_typedefs().unqualified()
      fam_namespace,fam_tag,fam = f'{type.template_argument(0)}'.rsplit('::',2)
      return strip_version_namespace(f'{fam_namespace}::{fam}')

  def instantiate(self):
    return self._recognizer(self.name)

class GraphsElementId(gdb.printing.PrettyPrinter):

  def __init__(self,val):
    super().__init__('hysj::graphs::element_id')
    self.val = val

  def to_string(self):
    tag = f'{self.val.type.template_argument(0)}'.rsplit('::',1)[-1]
    if tag =='vertex':
      return f'v{self.val["value"]}'
    elif tag == 'edge':
      return f'e{self.val["value"]}'
    else:
      return f'u{self.val["value"]}'

  def display_hint(self):
    return None

class GraphsElementIdType(object):

  def __init__(self):
    self.name = 'hysj::graphs::element_id'
    self.enabled = True

  class _recognizer(object):

    def __init__(self,name):
      self.name = name
      
    def recognize(self, type):
      if type.tag is None:
        return None
      if not type.tag.startswith('hysj::_0::graphs::element_id<'):
        return None
      type = type.strip_typedefs().unqualified()
      element = f'{type.template_argument(0)}'.rsplit('::',1)[-1]
      return f'hysj::graphs::{element}'

  def instantiate(self):
    return self._recognizer(self.name)

class GraphsDynamicEdgeId(gdb.printing.PrettyPrinter):

  def __init__(self,val):
    super().__init__('hysj::graphs::dynamic_edge_id')
    self.val = val

  def to_string(self):
    return f'({self.val["id"]},{self.val["direction"]})'

  def display_hint(self):
    return None

class GraphsDynamicEdgeIdType(object):

  def __init__(self):
    self.name = 'hysj::graphs::dynamic_edge_id'
    self.enabled = True

  class _recognizer(object):

    def __init__(self,name):
      self.name = name
      
    def recognize(self, type):
      if type.tag is None:
        return None
      if not type.tag.startswith('hysj::_0::graphs::dynamic_edge_id<'):
        return None
      type = type.strip_typedefs().unqualified()
      return str(type).split('<',1)[-1]

  def instantiate(self):
    return self._recognizer(self.name)

class GraphsStaticEdgeId(gdb.printing.PrettyPrinter):

  def __init__(self,val):
    super().__init__('hysj::graphs::static_edge_id')
    self.val = val

  def to_string(self):
    return f'({self.val["id"]},{self.val["direction"]})'

  def display_hint(self):
    return None

class GraphsStaticEdgeIdType(object):

  def __init__(self):
    self.name = 'hysj::graphs::static_edge_id'
    self.enabled = True

  class _recognizer(object):

    def __init__(self,name):
      self.name = name
      
    def recognize(self, type):
      if type.tag is None:
        return None
      if not type.tag.startswith('hysj::_0::graphs::static_edge_id<'):
        return None
      type = type.strip_typedefs().unqualified()
      return str(type).split('<',1)[-1]

  def instantiate(self):
    return self._recognizer(self.name)

class IntegralConstant(object):
  """
    Print a Integral constant
"""
  def __init__(self,val):
    super().__init__()
    self.val = val

  def to_string(self):
    tag = self.val.type.template_argument(1)
    return strip_version_namespace(f"{tag}_c")

  def display_hint(self):
    return None

class IntegralConstantType(object):
  """
    Print an IntegralConstantType.
  """

  def __init__(self):
    self.name = 'std::integral_constant'
    self.enabled = True

  class _recognizer(object):

    def __init__(self,name):
      self.name = name

    def recognize(self, type):
      if type.tag is None:
        return None
      if not type.tag.startswith('std::integral_constant'):
        return None
      if type.code == gdb.TYPE_CODE_REF:
        type = type.target()
      return strip_version_namespace(f'{type.template_argument(1)}_c')

  def instantiate(self):
    return self._recognizer(self.name)

pp1 = gdb.printing.RegexpCollectionPrettyPrinter('std_extras')
pp1.add_printer('std::span',
               '^std::span<',
                StdSpanPrinter)
pp1.add_printer('std::integral_constant',
               '^std::integral_constant<',
                IntegralConstant)
gdb.printing.register_pretty_printer(None,
                                     pp1,
                                     replace=True)
      

pp = gdb.printing.RegexpCollectionPrettyPrinter('hysj')
pp.add_printer('hysj::_0::grammars::sym',
               '^hysj::_0::grammars::sym<',
               GrammarsSym)
pp.add_printer('hysj::_0::grammars::fam',
               '^hysj::_0::grammars::fam<',
               GrammarsFam)
pp.add_printer('hysj::_0::tools::graphs::element_id',
               '^hysj::_0::tools::graphs::element_id<',
               GraphsElementId)
pp.add_printer('hysj::_0::tools::graphs::dynamic_edge_id',
               '^hysj::_0::tools::graphs::dynamic_edge_id',
               GraphsDynamicEdgeId)
pp.add_printer('hysj::_0::tools::graphs::static_edge_id',
               '^hysj::_0::tools::graphs::static_edge_id<',
               GraphsStaticEdgeId)

gdb.printing.register_pretty_printer(None,
                                     pp,
                                     replace=True)
gdb.types.register_type_printer(None,
                                GrammarsSymType())
gdb.types.register_type_printer(None,
                                GrammarsFamType())
gdb.types.register_type_printer(None,
                                IntegralConstantType())
gdb.types.register_type_printer(None,
                                GraphsElementIdType())
gdb.types.register_type_printer(None,
                                GraphsDynamicEdgeIdType())
gdb.types.register_type_printer(None,
                                GraphsStaticEdgeIdType())
