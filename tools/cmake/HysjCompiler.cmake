set(CMAKE_CXX_STANDARD 23)
set(CMAKE_CXX_VISIBILITY_PRESET hidden)
set(CMAKE_CXX_VISIBILITY_INLINES_HIDDEN ON)

add_library(hysj-private_flags INTERFACE)
target_compile_features(hysj-private_flags INTERFACE cxx_std_${CMAKE_CXX_STANDARD})
target_compile_definitions(hysj-private_flags INTERFACE $<$<CONFIG:Debug>:HYSJ_DEBUG>)

if(HYSJ_WITH_COVERAGE)
  #NOTE:
  #  add coverage flags
  #  - jeh
  target_compile_options(hysj-private_flags INTERFACE $<$<CONFIG:DEBUG>:-fprofile-arcs -ftest-coverage -fprofile-abs-path>)
  target_link_libraries(hysj-private_flags INTERFACE $<$<CONFIG:DEBUG>:gcov>)
endif()

add_library(hysj-public_flags INTERFACE)
target_compile_features(hysj-public_flags INTERFACE cxx_std_${CMAKE_CXX_STANDARD})
target_compile_definitions(hysj-public_flags INTERFACE $<$<CONFIG:Debug>:HYSJ_DEBUG>)

