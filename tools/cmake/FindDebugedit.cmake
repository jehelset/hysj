find_program(DEBUGEDIT_EXECUTABLE
             NAMES debugedit
             DOC "Path to debugedit executable")

include(FindPackageHandleStandardArgs)

find_package_handle_standard_args(Debugedit
                                  "Failed to find debugedit executable"
                                  DEBUGEDIT_EXECUTABLE)

