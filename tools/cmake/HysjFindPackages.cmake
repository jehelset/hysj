include_guard()

find_package(fmt             REQUIRED)
find_package(kumi            REQUIRED)
find_package(libassert       REQUIRED)
find_package(SPIRV-Tools     REQUIRED)
find_package(SPIRV-Tools-opt REQUIRED)
find_package(Vulkan          REQUIRED)
find_package(STDEXEC         REQUIRED)
find_package(GCCJit          REQUIRED)

if(HYSJ_WITH_BENCHMARKS)
  find_package(benchmark REQUIRED)
endif()
if(HYSJ_WITH_DOCUMENTATION)
  find_package(Sphinx REQUIRED)
endif()

if(PROJECT_IS_TOP_LEVEL)
  include(CTest)
endif()
if(BUILD_TESTING)
  find_package(doctest REQUIRED)
  include(doctest)
endif()

if(HYSJ_WITH_PYTHON)
  find_package(PkgConfig REQUIRED)
  pkg_search_module(cgif REQUIRED IMPORTED_TARGET cgif)
  pkg_search_module(rsvg REQUIRED IMPORTED_TARGET librsvg-2.0)
  pkg_search_module(cairo REQUIRED IMPORTED_TARGET cairo)
  pkg_search_module(pipeline REQUIRED IMPORTED_TARGET libpipeline)
  pkg_search_module(py3cairo REQUIRED IMPORTED_TARGET py3cairo)
  find_package(Python3 REQUIRED Interpreter Development)
  find_package(pybind11 REQUIRED)
endif()
