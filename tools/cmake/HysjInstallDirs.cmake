include_guard()

include(HysjPackageComponents)
include(GNUInstallDirs)

macro(hysj_install_generate name EXPRESSION)
  if(NOT COMMAND hysj_install_${name}dir)
    function(hysj_install_${name}dir COMPONENT RESULT)
      hysj_package_name(${COMPONENT} HYSJ_PACKAGE_NAME)
      hysj_package_component_absbase(${COMPONENT} HYSJ_PACKAGE_COMPONENT_ABSBASE)
      hysj_component_abspath(${COMPONENT} HYSJ_COMPONENT_ABSPATH)
      hysj_package_component_abspath(${COMPONENT} HYSJ_PACKAGE_COMPONENT_ABSPATH)
      set(${RESULT} ${EXPRESSION} PARENT_SCOPE)
    endfunction()
  endif()
endmacro()

hysj_install_generate(
  license
  ${CMAKE_INSTALL_DATADIR}/licenses/\${HYSJ_PACKAGE_NAME}-${HYSJ_VERSION})

hysj_install_generate(
  include
  ${CMAKE_INSTALL_INCLUDEDIR}/\${HYSJ_PACKAGE_NAME}-${HYSJ_VERSION})

hysj_install_generate(
  bin
  ${CMAKE_INSTALL_BINDIR})

hysj_install_generate(
  lib
  ${CMAKE_INSTALL_LIBDIR})

hysj_install_generate(
  symbol
  ${CMAKE_INSTALL_LIBDIR}/debug)

hysj_install_generate(
  source
  src/debug/\${HYSJ_PACKAGE_NAME}-${HYSJ_VERSION})

hysj_install_generate(
  doc
  ${CMAKE_INSTALL_DOCDIR}\${HYSJ_PACKAGE_COMPONENT_ABSBASE}-${HYSJ_VERSION}\${HYSJ_PACKAGE_COMPONENT_ABSPATH})

if(HYSJ_WITH_PYTHON)
  if(NOT COMMAND hysj_install_pythondir)
    if("${HYSJ_HOST}" STREQUAL "LINUX")
      if("${HYSJ_HOST_ID}" STREQUAL "ARCH")
	      set(HYSJ_INSTALL_PYDIR_PREFIX ${Python3_SITELIB})
	      string(REGEX REPLACE "^/usr/" "" HYSJ_INSTALL_PYDIR_PREFIX ${HYSJ_INSTALL_PYDIR_PREFIX})
      elseif("${HYSJ_HOST_ID}" STREQUAL "FEDORA")
        set(HYSJ_INSTALL_PYDIR_PREFIX ${Python3_SITELIB})
        string(REGEX REPLACE "^/usr/local/" "" HYSJ_INSTALL_PYDIR_PREFIX ${HYSJ_INSTALL_PYDIR_PREFIX})
      endif()
    endif()
  else()
    message(FATAL_ERROR "HysjHost: unsupported python platform")
  endif()
endif()

hysj_install_generate(
  python
  ${HYSJ_INSTALL_PYDIR_PREFIX}/hysj\${HYSJ_COMPONENT_ABSPATH})

hysj_install_generate(
  cmake
  ${CMAKE_INSTALL_LIBDIR}/cmake/\${HYSJ_PACKAGE_NAME}-${HYSJ_VERSION})

hysj_install_generate(
  report
  reports)

