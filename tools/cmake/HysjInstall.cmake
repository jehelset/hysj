include_guard()

include(HysjInstallDirs)
include(HysjTargets)

function(hysj_export COMPONENT RESULT)
  set(${RESULT} hysj-${COMPONENT}-targets PARENT_SCOPE)
endfunction()

hysj_export(runtime HYSJ_BUILTIN_EXPORT)
install(
  TARGETS hysj-public_flags hysj-private_flags
  EXPORT ${HYSJ_BUILTIN_EXPORT}
  LIBRARY DESTINATION ${HYSJ_INSTALL_LIBDIR}
  ARCHIVE DESTINATION ${HYSJ_INSTALL_LIBDIR})

function(hysj_install_library)
  set(_opts)
  set(_single_opts TARGET COMPONENT)
  cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})
  set(TARGET ${_args_TARGET})
  set(COMPONENT ${_args_COMPONENT})
  hysj_export(${COMPONENT} EXPORT)
  install(TARGETS ${TARGET}
    COMPONENT ${COMPONENT}
    EXPORT ${EXPORT}
    LIBRARY DESTINATION ${HYSJ_INSTALL_LIBDIR} NAMELINK_SKIP
    ARCHIVE DESTINATION ${HYSJ_INSTALL_LIBDIR}
    RUNTIME DESTINATION ${HYSJ_INSTALL_BINDIR})
endfunction()

function(hysj_install_headers)
  set(_opts)
  set(_single_opts COMPONENT)
  cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})
  set(COMPONENT ${_args_COMPONENT})

  hysj_install_includedir(${COMPONENT} DESTINATION)
  install(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/
    ${CMAKE_CURRENT_BINARY_DIR}/${HYSJ_GENERATED_SOURCEDIR}/
    COMPONENT ${COMPONENT}
    DESTINATION ${DESTINATION}
    FILES_MATCHING REGEX ".*[.]hpp$"
    REGEX "private" EXCLUDE)
endfunction()

if(HYSJ_WITH_PYTHON)

  function(hysj_install_python_module)
    set(_opts)
    set(_single_opts TARGET COMPONENT)
    cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})
    set(TARGET ${_args_TARGET})
    set(COMPONENT ${_args_COMPONENT})
    hysj_install_pythondir(${COMPONENT} DESTINATION)

    install(TARGETS ${TARGET}
      COMPONENT ${COMPONENT}
      LIBRARY DESTINATION ${DESTINATION})
  endfunction()

  function(hysj_install_python_source)
    set(_opts)
    set(_single_opts COMPONENT)
    cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})
    set(COMPONENT ${_args_COMPONENT})
    hysj_install_pythondir(${COMPONENT} DESTINATION)
    install(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/
      COMPONENT ${COMPONENT}
      DESTINATION ${DESTINATION}
      FILES_MATCHING
      PATTERN "*.py")
  endfunction()

endif()

function(hysj_install_license)
  set(_opts)
  set(_single_opts COMPONENT)
  cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})
  set(COMPONENT ${_args_COMPONENT})
  hysj_install_licensedir(${COMPONENT} DESTINATION)
  install(FILES ${PROJECT_SOURCE_DIR}/LICENSE
    COMPONENT ${COMPONENT}
    DESTINATION ${DESTINATION})
endfunction()

function(hysj_install_licenses)
  if(HYSJ_PACKAGE_PROJECT_LAYOUT)
    hysj_install_license(COMPONENT project)
  else()
    foreach(COMPONENT ${HYSJ_PACKAGE_COMPONENTS})
      hysj_install_license(COMPONENT ${COMPONENT})
    endforeach()
  endif()
endfunction()

if(HYSJ_WITH_CMAKE_PACKAGE_CONFIG)
  include(CMakePackageConfigHelpers)

  function(hysj_install_cmake_target_registry COMPONENT RESULT)
    if(HYSJ_PAKCAGE_PROJECT_LAYOUT)
      set(${RESULT} "HYSJ_CMAKE_PACKAGE_TARGET_REGISTRY")
    else()
      set(${RESULT} "HYSJ_CMAKE_${COMPONENT}_PACKAGE_TARGET_REGISTRY")
    endif()
    set(${RESULT} ${${RESULT}} PARENT_SCOPE)
  endfunction()

  foreach(HYSJ_COMPONENT ${HYSJ_COMPONENTS_ALL})
    hysj_install_cmake_target_registry(${HYSJ_COMPONENT} REGISTRY)
    set(${REGISTRY} ""  CACHE INTERNAL "" FORCE)
  endforeach()

  function(hysj_install_cmake_add_package_target COMPONENT TARGET_COMPONENT)
    hysj_install_cmake_target_registry(${COMPONENT} REGISTRY)
    set(${REGISTRY} "${${REGISTRY}};${TARGET_COMPONENT}" CACHE INTERNAL "" FORCE)
  endfunction()

  function(hysj_install_cmake_package_targets)
    set(_single_opts COMPONENT)
    set(_multi_opts TARGET_COMPONENTS)
    cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})

    set(COMPONENT ${_args_COMPONENT})
    set(TARGET_COMPONENTS ${_args_TARGET_COMPONENTS})
    hysj_install_cmakedir(${COMPONENT} DESTINATION)

    foreach(TARGET_COMPONENT ${TARGET_COMPONENTS})
      install(EXPORT hysj-${TARGET_COMPONENT}-targets
	COMPONENT ${TARGET_COMPONENT}
	NAMESPACE hysj::
	DESTINATION ${DESTINATION})
      hysj_install_cmake_add_package_target(${COMPONENT} ${TARGET_COMPONENT})
    endforeach()

    if(EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/depends.cmake.in)
      configure_file(${CMAKE_CURRENT_SOURCE_DIR}/depends.cmake.in
	${CMAKE_CURRENT_BINARY_DIR}/hysj-${COMPONENT}-depends.cmake
	@ONLY)
      install(FILES ${CMAKE_CURRENT_BINARY_DIR}/hysj-${COMPONENT}-depends.cmake
	COMPONENT ${COMPONENT}
	DESTINATION ${DESTINATION})
    endif()

  endfunction()

  function(hysj_install_cmake_package_name COMPONENT RESULT)
    set(PACKAGE_NAME hysj${COMPONENT_PREFIX_ID})
    if(HYSJ_INSTALL_LAYOUT STREQUAL "PROJECT")
      set(${RESULT} "hysj")
    else()
      hysj_component_name(${COMPONENT} COMPONENT_NAME)
      set(${RESULT} ${COMPONENT_NAME})
    endif()
    set(${RESULT} ${${RESULT}} PARENT_SCOPE)
  endfunction()

  function(hysj_install_cmake_package_config_should_skip COMPONENT RESULT)
    set(${RESULT} 1)
    if(HYSJ_INSTALL_LAYOUT STREQUAL "PROJECT")
      if(COMPONENT STREQUAL "project")
	set(${RESULT} 0)
      endif()
    else()
      if(NOT COMPONENT STREQUAL "project")
	set(${RESULT} 0)
      endif()
    endif()
    set(${RESULT} ${${RESULT}} PARENT_SCOPE)
  endfunction()

  function(hysj_install_cmake_package_config)
    set(_opts)
    set(_single_opts COMPONENT)
    cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})
    set(COMPONENT ${_args_COMPONENT})

    hysj_install_cmakedir(${COMPONENT} DESTINATION)
    hysj_install_cmake_package_name(${COMPONENT} PACKAGE_NAME)

    set(HYSJ_CMAKE_PACKAGE_NAME ${PACKAGE_NAME})
    hysj_install_cmake_target_registry(${COMPONENT} REGISTRY)
    set(HYSJ_CMAKE_PACKAGE_COMPONENTS ${${REGISTRY}})

    if(NOT HYSJ_CMAKE_PACKAGE_COMPONENTS)
      message(STATUS "HysjInstall: No components found for cmake-package '${COMPONENT}', skipping cmake-package-config for it.")
      return()
    endif()

    configure_package_config_file(
      ${CMAKE_CURRENT_FUNCTION_LIST_DIR}/templates/package-config.cmake.in
      ${PACKAGE_NAME}-config.cmake
      INSTALL_DESTINATION ${DESTINATION})
    write_basic_package_version_file(
      ${PACKAGE_NAME}-config-version.cmake
      VERSION ${HYSJ_VERSION}
      COMPATIBILITY SameMajorVersion)
    install(
      FILES
      ${CMAKE_CURRENT_BINARY_DIR}/${PACKAGE_NAME}-config.cmake
      ${CMAKE_CURRENT_BINARY_DIR}/${PACKAGE_NAME}-config-version.cmake
      COMPONENT ${COMPONENT}
      DESTINATION ${DESTINATION})
  endfunction()

  function(hysj_install_cmake_package_configs)
    if(HYSJ_PACKAGE_PROJECT_LAYOUT)
      hysj_install_cmake_package_config(COMPONENT project)
    else()
      foreach(COMPONENT ${HYSJ_PACKAGE_COMPONENTS})
	hysj_install_cmake_package_config(COMPONENT ${COMPONENT})
      endforeach()
    endif()
  endfunction()

endif()

function(hysj_install_reports)
  set(_single_opts COMPONENT)
  set(_multi_opts REPORTS)
  cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})
  set(REPORTS ${_args_REPORTS})
  set(COMPONENT ${_args_COMPONENT})
  hysj_install_reportdir(${COMPONENT} DESTINATION)
  install(FILES ${REPORTS}
    DESTINATION ${DESTINATION}
    COMPONENT ${COMPONENT}
    EXCLUDE_FROM_ALL)
endfunction()


if(BUILD_TESTING)
  function(hysj_install_test_report)
    set(_single_opts TARGET)
    cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})
    set(TARGET ${_args_TARGET})
    hysj_test_report_file(${TARGET} FILE)
    hysj_install_reports(REPORTS
      ${CMAKE_CURRENT_BINARY_DIR}/${FILE}
      COMPONENT reports)
  endfunction()
endif()

if(HYSJ_WITH_DEBUG)
  find_package(Debugedit REQUIRED)

  function(hysj_install_debug_sources)
    set(_opts)
    set(_single_opts COMPONENT)
    cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})
    set(COMPONENT ${_args_COMPONENT})

    hysj_install_sourcedir(${COMPONENT} SOURCEDIR)
    install(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/
      ${CMAKE_CURRENT_BINARY_DIR}/${HYSJ_GENERATED_SOURCEDIR}/
      COMPONENT ${COMPONENT}
      DESTINATION ${SOURCEDIR}
      FILES_MATCHING REGEX ".*[.][hc]pp$"
      REGEX "private" EXCLUDE)
  endfunction()

  function(hysj_install_debug_symbols)
    set(_opts)
    set(_single_opts TARGET COMPONENT)
    cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})
    set(TARGET ${_args_TARGET})
    set(COMPONENT ${_args_COMPONENT})

    hysj_buildid(${TARGET} BUILDID_HEAD BUILDID_TAIL)

    hysj_install_symboldir(${COMPONENT} SYMBOLDIR)
    set(SYMBOLS_PATH "${SYMBOLDIR}/.build-id/${BUILDID_HEAD}")
    set(SYMBOLS_FILE "${BUILDID_TAIL}.debug")

    #NOTE:
    #  hacky as hell, but the thought of grepping a 5GB of
    #  symbol-file ascii is too daunting.
    #  - jeh
    if(CMAKE_GENERATOR STREQUAL "Unix Makefiles")
      set(COMP_DIR ${CMAKE_CURRENT_BINARY_DIR})
    elseif(CMAKE_GENERATOR STREQUAL "Ninja")
      set(COMP_DIR ${PROJECT_BINARY_DIR})
    else()
      message(FATAL_ERROR "HysjTargets: Unsupported generator for symbol-stripping ${CMAKE_GENERATOR}")
    endif()
    
    install(CODE
      "\
execute_process(COMMAND ${DEBUGEDIT_EXECUTABLE}\
  --base-dir=${COMP_DIR}\
  --dest-dir=\${CMAKE_INSTALL_PREFIX}\
  ${SYMBOLS_FILE}\
  WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
  COMMAND_ERROR_IS_FATAL ANY)\
"
      COMPONENT ${COMPONENT})
    install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${SYMBOLS_FILE}
      COMPONENT ${COMPONENT}
      DESTINATION ${SYMBOLS_PATH})

  endfunction()

endif()

if(HYSJ_WITH_DOCUMENTATION)

  function(hysj_install_documentation)
    set(_opts)
    set(_single_opts COMPONENT PREFIX)
    cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})
    set(COMPONENT ${_args_COMPONENT})
    set(PREFIX ${_args_PREFIX})
    set(BUILD_DIR ${CMAKE_CURRENT_BINARY_DIR})
    hysj_install_docdir(${COMPONENT} DESTINATION)
    install(DIRECTORY ${BUILD_DIR}/html/
      COMPONENT ${COMPONENT}
      DESTINATION ${DESTINATION})
  endfunction()

endif()
