include_guard()

list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_LIST_DIR})

#NOTE:
#  set up about info
#  - jeh
set(HYSJ_AUTHOR             "John Eivind Helset")
set(HYSJ_EMAIL              "private@jehelset.no")
set(HYSJ_URL                "https://gitlab.com/jehelset/hysj")
set(HYSJ_DESCRIPTION        "Model and simulate with Hybrid Automata.")
set(HYSJ_LICENCE            "CC0")
set(HYSJ_CONTAINER_REGISTRY "registry.gitlab.com/jehelset/hysj")
set(HYSJ_COPYRIGHT          "No Rights Reserved")

if(NOT HYSJ_VERSION)
  find_package(Git REQUIRED)
  #NOTE:
  #  compute version, release and codename based on git tag
  #  - jeh
  execute_process(
    COMMAND "${GIT_EXECUTABLE}" "describe" "--always" "--tags" "HEAD"
    WORKING_DIRECTORY "${PROJECT_SOURCE_DIR}"
    OUTPUT_VARIABLE HYSJ_VERSION
    ERROR_QUIET OUTPUT_STRIP_TRAILING_WHITESPACE)
endif()

if(${HYSJ_VERSION} MATCHES "^([0-9]+)\\.([0-9]+)\\.([0-9]+)-([0-9]+).*$")
  set(HYSJ_VERSION_MAJOR   ${CMAKE_MATCH_1})
  set(HYSJ_VERSION_MINOR   ${CMAKE_MATCH_2})
  set(HYSJ_VERSION_PATCH   ${CMAKE_MATCH_3})
  set(HYSJ_RELEASE         ${CMAKE_MATCH_4})
  set(HYSJ_VERSION         "${CMAKE_MATCH_1}.${CMAKE_MATCH_2}.${CMAKE_MATCH_3}")
  set(HYSJ_TAG "${HYSJ_VERSION}-${HYSJ_RELEASE}")
else()
  message(FATAL_ERROR "HysjConfig: Invalid version string - ${HYSJ_VERSION}")
endif()

set(HYSJ_ID "hysj-${HYSJ_VERSION}")

if(NOT HYSJ_CODENAME)
  find_package(Git REQUIRED)
  execute_process(
    COMMAND "${GIT_EXECUTABLE}" "tag" "-l" "--format=%(subject)" ${HYSJ_TAG}
    WORKING_DIRECTORY "${PROJECT_SOURCE_DIR}"
    OUTPUT_VARIABLE HYSJ_CODENAME
    ERROR_QUIET OUTPUT_STRIP_TRAILING_WHITESPACE)
endif()

if("${HYSJ_CODENAME}" STREQUAL "")
  message(FATAL_ERROR "HysjConfig: Missing codename")
endif()

message(STATUS "HysjConfig: ${HYSJ_TAG} / ${HYSJ_CODENAME}")

#NOTE:
#  compute host info
#  - jeh
cmake_host_system_information(RESULT HYSJ_HOST QUERY OS_NAME)
string(TOUPPER ${HYSJ_HOST} HYSJ_HOST)

if("${HYSJ_HOST}" STREQUAL "LINUX")
  file(STRINGS /etc/os-release HYSJ_HOST_ID REGEX "^ID=.*$")
  string(REPLACE "ID=" "" HYSJ_HOST_ID ${HYSJ_HOST_ID})
  string(TOUPPER ${HYSJ_HOST_ID} HYSJ_HOST_ID)
  if(${HYSJ_HOST_ID} STREQUAL "ARCH")
    set(HYSJ_HOST_VERSION_CODENAME "LATEST")
  endif()
else()
  message(FATAL_ERROR "HysjConfig: unsupported host")
endif()

cmake_host_system_information(RESULT HYSJ_HOST_PLATFORM QUERY OS_PLATFORM)
string(TOUPPER ${HYSJ_HOST_PLATFORM} HYSJ_HOST_PLATFORM)

string(JOIN "-" HYSJ_HOST_SIGNATURE ${HYSJ_HOST} ${HYSJ_HOST_ID} ${HYSJ_HOST_VERSION_CODENAME} ${HYSJ_HOST_PLATFORM})
message(STATUS "HysjConfig: detected ${HYSJ_HOST_SIGNATURE}")

if(HYSJ_USER_PROLOGUE)
  include(${HYSJ_USER_PROLOGUE})
endif()
