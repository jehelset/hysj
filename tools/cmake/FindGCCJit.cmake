
find_path(GCCJit_INCLUDE_DIR NAMES libgccjit++.h)
find_library(GCCJit_LIBRARY NAMES gccjit)


include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(GCCJit
  FOUND_VAR GCCJit_FOUND
  REQUIRED_VARS
    GCCJit_LIBRARY
    GCCJit_INCLUDE_DIR
  VERSION_VAR GCCJit_VERSION
)

if(GCCJit_FOUND)
  set(GCCJit_LIBRARIES ${GCCJit_LIBRARY})
  set(GCCJit_INCLUDE_DIRS ${GCCJit_INCLUDE_DIR})
endif()

if(GCCJit_FOUND AND NOT TARGET GCCJit::libgccjit)
  add_library(GCCJit::libgccjit SHARED IMPORTED)
  set_target_properties(GCCJit::libgccjit PROPERTIES
    IMPORTED_LOCATION "${GCCJit_LIBRARY}"
    INTERFACE_INCLUDE_DIRECTORIES "${GCCJit_INCLUDE_DIR}"
  )
endif()

mark_as_advanced(
  GCCJit_INCLUDE_DIR
  GCCJit_LIBRARY
)

