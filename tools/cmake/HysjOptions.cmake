include(CMakeDependentOption)

option(HYSJ_WITH_COVERAGE	"Enable coverage"               OFF)
option(HYSJ_WITH_BENCHMARKS     "Enable benchmarking"           OFF)
option(HYSJ_WITH_CMAKE_CONFIG	"Generate cmake package config" OFF)
option(HYSJ_WITH_PYTHON		"Enable python bindings"        OFF)
option(HYSJ_WITH_CPACK		"Enable cpack"                  OFF)
option(HYSJ_WITH_CONTAINERS	"Enable container targets"      OFF)
option(HYSJ_WITH_PCH		"Enable precompiled header"     OFF)
option(HYSJ_WITH_DOCUMENTATION	"Enable documentation"          OFF)
option(HYSJ_WITH_DEBUG          "Enable debug symbols"          OFF)

cmake_dependent_option(HYSJ_WITH_STANDALONE_DOCUMENTATION "Enable standalone documentation" OFF "HYSJ_WITH_DOCUMENTATION" OFF)

