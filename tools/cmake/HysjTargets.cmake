include_guard()

include(GenerateExportHeader)

set(HYSJ_GENERATED_SOURCEDIR "_hysj_generated_sources")
function(hysj_generate_source IN OUT)
  configure_file(${IN} ${HYSJ_GENERATED_SOURCEDIR}/${OUT})
endfunction()

function(hysj_generate_export_header)
  set(_opts)
  set(_single_opts TARGET BASE_NAME OUT)
  set(_multi_opts)
  cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})

  if (NOT DEFINED _args_BASE_NAME)
    string(TOUPPER ${_args_TARGET} _args_BASE_NAME)
  endif()
  generate_export_header(${_args_TARGET}
    BASE_NAME ${_args_BASE_NAME}
    EXPORT_FILE_NAME ${CMAKE_CURRENT_BINARY_DIR}/${HYSJ_GENERATED_SOURCEDIR}/${_args_OUT})
endfunction()

function(hysj_stage)
  set(_opts)
  set(_single_opts TARGET PREFIX BINDIRS LIBDIRS PYDIRS)
  set(_multi_opts COMPONENTS DEPENDS)
  cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})

  if(NOT DEFINED _args_PREFIX)
    set(_args_PREFIX ${CMAKE_CURRENT_BINARY_DIR})
  endif()

  set(BINDIRS "")
  set(LIBDIRS "")
  set(PYDIRS "${_args_PREFIX}/${HYSJ_INSTALL_PYDIR_PREFIX}")
  foreach(COMPONENT ${_args_COMPONENTS})
    hysj_install_bindir(${COMPONENT} BINDIR)
    list(APPEND BINDIRS ${_args_PREFIX}/${BINDIR})
    hysj_install_libdir(${COMPONENT} LIBDIR)
    list(APPEND LIBDIRS ${_args_PREFIX}/${LIBDIR})
  endforeach()

  list(REMOVE_DUPLICATES BINDIRS)
  list(REMOVE_DUPLICATES LIBDIRS)
  list(REMOVE_DUPLICATES PYDIRS)

  if(DEFINED _args_BINDIRS)
    set(${_args_BINDIRS} ${BINDIRS} PARENT_SCOPE)
  endif()
  if(DEFINED _args_LIBDIRS)
    set(${_args_LIBDIRS} ${LIBDIRS} PARENT_SCOPE)
  endif()
  if(DEFINED _args_PYDIRS)
    set(${_args_PYDIRS} ${PYDIRS} PARENT_SCOPE)
  endif()

  set(COMPONENT_TARGETS "")
  foreach(COMPONENT ${_args_COMPONENTS})
    set(COMPONENT_TARGET ${_args_TARGET}-component-${COMPONENT})
    add_custom_target(${COMPONENT_TARGET} ALL
      COMMAND ${CMAKE_COMMAND} --install ${PROJECT_BINARY_DIR}
      --prefix ${_args_PREFIX} --component ${COMPONENT})
    if(DEFINED _args_DEPENDS)
      add_dependencies(${COMPONENT_TARGET} ${_args_DEPENDS})
    endif()
    list(APPEND COMPONENT_TARGETS ${COMPONENT_TARGET})
  endforeach()

  add_custom_target(${_args_TARGET})

  if(DEFINED _args_DEPENDS)
    add_dependencies(${_args_TARGET} ${_args_DEPENDS})
  endif()
  add_dependencies(${_args_TARGET} ${COMPONENT_TARGETS})
endfunction()

#TODO: compute a proper buildid - jeh
function(hysj_buildid TARGET BUILDID_HEAD_VARIABLE BUILDID_TAIL_VARIABLE)
  set(BUILDID_NAMESPACE "f6d60b66-b919-4f73-8668-4f9cfb970587")
  string(UUID BUILDID NAMESPACE ${BUILDID_NAMESPACE} NAME ${TARGET} TYPE SHA1)
  string(REGEX REPLACE "^(........)-(....)-(....)-(....)-(............)$" "\\1\\2\\3\\4\\5" BUILDID ${BUILDID})
  string(SUBSTRING ${BUILDID} 0 2 BUILDID_HEAD)
  string(SUBSTRING ${BUILDID} 2 -1 BUILDID_TAIL)

  set(${BUILDID_HEAD_VARIABLE} ${BUILDID_HEAD} PARENT_SCOPE)
  set(${BUILDID_TAIL_VARIABLE} ${BUILDID_TAIL} PARENT_SCOPE)
endfunction()

function(hysj_glob_sources)
  set(_single_opts TARGET)
  cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})
  set(TARGET ${_args_TARGET})
  file(GLOB_RECURSE sources CONFIGURE_DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/*.cpp)
  target_sources(${TARGET} PRIVATE ${sources})
endfunction()

function(hysj_configure_binary)
  set(_single_opts TARGET COMPONENT)
  cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})
  set(TARGET ${_args_TARGET})
  set(COMPONENT ${_args_COMPONENT})
  target_link_libraries(${TARGET}
    PRIVATE hysj-private_flags
    PUBLIC hysj-public_flags libassert::assert)
  target_compile_definitions(${TARGET}
    PRIVATE
    LIBASSERT_USE_FMT)
  if(HYSJ_WITH_DEBUG)
    #FIXME: assumes name of debug component! - jeh
    hysj_install_sourcedir(${COMPONENT}-debug SOURCEDIR)
    target_compile_options(${TARGET} PUBLIC
      "$<BUILD_INTERFACE:-ffile-prefix-map=${CMAKE_CURRENT_BINARY_DIR}/${HYSJ_GENERATED_SOURCEDIR}=${SOURCEDIR}>")
    target_compile_options(${TARGET} PUBLIC
      "$<BUILD_INTERFACE:-ffile-prefix-map=${CMAKE_CURRENT_SOURCE_DIR}=${SOURCEDIR}>")
    hysj_buildid(${TARGET} BUILDID_HEAD BUILDID_TAIL)
    target_link_options(${TARGET} PRIVATE
      "-Wl,--build-id=0x${BUILDID_HEAD}${BUILDID_TAIL}")
  endif()
  if(HYSJ_WITH_PCH)
    target_precompile_headers(${TARGET} REUSE_FROM hysj-pch)
  endif()
endfunction()

function(hysj_configure_executable)
  set(_single_opts TARGET COMPONENT)
  cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})
  set(TARGET ${_args_TARGET})
  set(COMPONENT ${_args_COMPONENT})

  set_target_properties(${TARGET} PROPERTIES VERSION ${HYSJ_VERSION})
  hysj_configure_binary(TARGET ${TARGET} COMPONENT ${COMPONENT})

  target_include_directories(${TARGET} PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}/${HYSJ_GENERATED_SOURCEDIR}>)
endfunction()

function(hysj_add_executable)
  set(_single_opts TARGET COMPONENT)
  cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})
  set(TARGET ${_args_TARGET})
  set(COMPONENT ${_args_COMPONENT})

  add_executable(${TARGET} ${_args_UNPARSED_ARGUMENTS})
  hysj_configure_executable(TARGET ${TARGET}
    COMPONENT ${COMPONENT})
endfunction()

function(hysj_configure_library)
  set(_single_opts TARGET COMPONENT INCLUDE_COMPONENT)
  cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})
  set(TARGET ${_args_TARGET})
  set(COMPONENT ${_args_COMPONENT})
  set(INCLUDE_COMPONENT ${_args_INCLUDE_COMPONENT})

  hysj_configure_binary(TARGET ${TARGET} COMPONENT ${COMPONENT}
    INCLUDE_COMPONENT ${INCLUDE_COMPONENT})
  set_target_properties(${TARGET} PROPERTIES
    VERSION ${HYSJ_VERSION}
    SOVERSION ${HYSJ_VERSION_MAJOR})

  if(INCLUDE_COMPONENT)
    hysj_install_includedir(${INCLUDE_COMPONENT} INCLUDEDIR)
    target_include_directories(${TARGET} PUBLIC
      $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
      $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}/${HYSJ_GENERATED_SOURCEDIR}>
      $<INSTALL_INTERFACE:${INCLUDEDIR}>)
  else()
    target_include_directories(${TARGET} PUBLIC
      $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
      $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}/${HYSJ_GENERATED_SOURCEDIR}>)
  endif()
endfunction()

function(hysj_add_library)
  set(_single_opts TARGET COMPONENT INCLUDE_COMPONENT)
  cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})
  set(TARGET ${_args_TARGET})
  set(COMPONENT ${_args_COMPONENT})
  set(INCLUDE_COMPONENT ${_args_INCLUDE_COMPONENT})
  add_library(${TARGET} ${_args_UNPARSED_ARGUMENTS})
  hysj_configure_library(TARGET ${TARGET} COMPONENT ${COMPONENT}
    INCLUDE_COMPONENT ${INCLUDE_COMPONENT})
endfunction()

if(HYSJ_WITH_PYTHON)

  function(hysj_add_python_module)
    set(_single_opts TARGET COMPONENT NAME)
    cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})
    set(TARGET ${_args_TARGET})
    set(COMPONENT ${_args_COMPONENT})
    set(NAME ${_args_NAME})
    pybind11_add_module(${TARGET})
    hysj_glob_sources(TARGET ${TARGET})
    set_target_properties(${TARGET}
      PROPERTIES OUTPUT_NAME ${NAME})
    hysj_configure_library(TARGET ${TARGET} COMPONENT ${COMPONENT})
  endfunction()

endif()

if(BUILD_TESTING)

  add_custom_target(hysj-test-reports)

  function(hysj_test_report_file TARGET RESULT)
    set(${RESULT} ${TARGET}-junit.xml PARENT_SCOPE)
  endfunction()

  function(hysj_add_test)
    set(_single_opts TARGET)
    cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})
    set(TARGET ${_args_TARGET})

    hysj_add_executable(TARGET ${TARGET} COMPONENT tests)
    #FIXME: exports - jeh

    target_link_libraries(${TARGET}
      PRIVATE doctest::doctest)
    doctest_discover_tests(${TARGET})

    hysj_test_report_file(${TARGET} FILE)
    add_custom_target(${TARGET}-report
      COMMAND $<TARGET_FILE:${TARGET}> -r=junit -o=${FILE}
      BYPRODUCTS ${FILE}
      DEPENDS ${TARGET})
    add_dependencies(hysj-test-reports ${TARGET}-report)
  endfunction()

  if(HYSJ_WITH_PYTHON)
    function(hysj_add_python_tests)
      set(_opts)
      set(_single_opts TARGET)
      set(_multi_opts DEPENDS COMPONENTS SCRIPTS DIRECTORY)
      cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})

      set(TARGET ${_args_TARGET})
      if(NOT _args_DIRECTORY)
	set(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})
      else()
	set(DIRECTORY ${_args_DIRECTORY})
      endif()

      cmake_path(IS_RELATIVE DIRECTORY DIRECTORY_RELATIVE)
      if(DIRECTORY_RELATIVE)
	set(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/${DIRECTORY})
      endif()
      set(DEPENDS ${_args_DEPENDS})
      set(COMPONENTS ${_args_COMPONENTS})

      file(GLOB_RECURSE scripts CONFIGURE_DEPENDS ${DIRECTORY}/test_*.py)

      hysj_stage(
	TARGET ${TARGET}-stage
	DEPENDS ${DEPENDS}
	COMPONENTS ${COMPONENTS}
	BINDIRS bindirs
	LIBDIRS libdirs
	PYDIRS pydirs)

      list(JOIN libdirs ":" libdirs)
      list(JOIN pydirs ":" pydirs)

      add_test(
	NAME ${TARGET}
	COMMAND cmake -E env LD_LIBRARY_PATH=${libdirs}:$ENV{PATH} PYTHONPATH=${pydirs}:$ENV{PYTHONPATH} ${Python3_EXECUTABLE} -m pytest -q ${scripts})

      hysj_test_report_file(${TARGET} FILE)
      add_custom_target(${TARGET}-report
	COMMAND cmake -E env LD_LIBRARY_PATH=${libdirs}:$ENV{PATH} PYTHONPATH=${pydirs}:$ENV{PYTHONPATH} ${Python3_EXECUTABLE} -m pytest -q --junitxml=${FILE} ${scripts}
	BYPRODUCTS ${FILE}
	DEPENDS ${TARGET}-stage)
      add_dependencies(hysj-test-reports ${TARGET}-report)
    endfunction()
  endif()

endif()

if(HYSJ_WITH_DEBUG)
  include(CMakeFindBinUtils)
  if(NOT CMAKE_OBJCOPY)
    message(FATAL_ERROR "'objcopy' not found")
  endif()
  if(NOT CMAKE_STRIP)
    message(FATAL_ERROR "'strip' not found")
  endif()
  function(hysj_add_debug_symbols)
    set(_opts)
    set(_single_opts TARGET)
    cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})

    set(TARGET ${_args_TARGET})
    set(TARGET_FILE "$<TARGET_FILE:${TARGET}>")
    hysj_buildid(${TARGET} BUILDID_HEAD BUILDID_TAIL)
    set(SYMBOLS_FILE "${BUILDID_TAIL}.debug")
    add_custom_command(TARGET ${TARGET} POST_BUILD
      COMMAND ${CMAKE_OBJCOPY} --only-keep-debug ${TARGET_FILE} ${SYMBOLS_FILE}
      COMMAND ${CMAKE_STRIP} ${TARGET_FILE}
      BYPRODUCTS ${SYMBOLS_FILE})
  endfunction()
endif()

if(HYSJ_WITH_DOCUMENTATION)

  set(HYSJ_HTML_STATIC_PATH ${PROJECT_SOURCE_DIR}/tools/sphinx/_static)

  include(HysjInstallDirs)
  function(hysj_add_documentation)
    set(_opts)
    set(_single_opts TARGET NAME COMPONENT)
    set(_multi_opts RUNTIME_COMPONENTS DEPENDS)
    cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})

    set(COMPONENT ${_args_COMPONENT})
    set(BUILD_DIR ${CMAKE_CURRENT_BINARY_DIR})

    set(CPP_INCLUDE_DIR "")

    hysj_install_includedir(${COMPONENT} INSTALL_INCLUDEDIR)
    hysj_install_docdir(${COMPONENT} INSTALL_DOCDIR)

    cmake_path(RELATIVE_PATH INSTALL_INCLUDEDIR
      BASE_DIRECTORY ${INSTALL_DOCDIR}
      OUTPUT_VARIABLE CPP_INCLUDE_DIR)

    set(HYSJ_DOCUMENTATION_PROJECT_NAME ${_args_NAME})

    set(RELATIVE_HYSJ_HTML_STATIC_PATH "")
    cmake_path(RELATIVE_PATH HYSJ_HTML_STATIC_PATH
      BASE_DIRECTORY ${BUILD_DIR}
      OUTPUT_VARIABLE RELATIVE_HYSJ_HTML_STATIC_PATH)
    set(HYSJ_DOCUMENTATION_HTML_STATIC_PATH "'${RELATIVE_HYSJ_HTML_STATIC_PATH}'")

    configure_file(${PROJECT_SOURCE_DIR}/tools/cmake/templates/sphinx-conf.py.in
      ${BUILD_DIR}/conf.py
      @ONLY)

    hysj_stage(TARGET ${_args_TARGET}-stage
      COMPONENTS ${_args_RUNTIME_COMPONENTS}
      DEPENDS ${_args_DEPENDS}
      BINDIRS RUNTIME_BINDIRS
      LIBDIRS RUNTIME_LIBDIRS
      PYDIRS RUNTIME_PYDIRS
      PREFIX ${BUILD_DIR})

    list(JOIN RUNTIME_BINDIRS ":" RUNTIME_BINDIRS)
    list(JOIN RUNTIME_LIBDIRS ":" RUNTIME_LIBDIRS)
    list(JOIN RUNTIME_PYDIRS ":" RUNTIME_PYDIRS)

    add_custom_target(
      ${_args_TARGET} ALL
      COMMAND ${CMAKE_COMMAND} -E env
      PATH="${RUNTIME_BINDIRS}:$ENV{PATH}"
      LD_LIBRARY_PATH="${RUNTIME_LIBDIRS}:$ENV{LD_LIBRARY_PATH}"
      PYTHONPATH="${RUNTIME_PYDIRS}:$ENV{PYTHONPATH}"
      ${SPHINX_EXECUTABLE} -b html -a
      -Dcpp_include_dir=${CPP_INCLUDE_DIR}
      -c ${BUILD_DIR}
      ${CMAKE_CURRENT_SOURCE_DIR}
      ${BUILD_DIR}/html
      WORKING_DIRECTORY ${BUILD_DIR})

    add_dependencies(${_args_TARGET} ${_args_DEPENDS})
    add_dependencies(${_args_TARGET} ${_args_TARGET}-stage)
  endfunction()
endif()
