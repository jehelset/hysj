include_guard()

if(NOT "${HYSJ_HOST}" STREQUAL "LINUX")
  message(FATAL_ERROR "HysjContainers: Unsupported host - ${HYSJ_HOST}")
endif()

find_program(PODMAN_EXECUTABLE podman REQUIRED)

set(HYSJ_CONTAINER_SOURCE_DIR "${PROJECT_SOURCE_DIR}/tools/containers")
set(HYSJ_CONTAINER_BINARY_DIR "${PROJECT_BINARY_DIR}")

if(NOT HYSJ_CONTAINER_TAG)
  set(HYSJ_CONTAINER_VERSION "latest")
endif()

function(hysj_add_builders DIRECTORY)

  string(REPLACE "/" "-" SIGNATURE ${DIRECTORY})
  set(SOURCE_DIR ${HYSJ_CONTAINER_SOURCE_DIR}/${DIRECTORY})
  set(BINARY_DIR "${HYSJ_CONTAINER_BINARY_DIR}/hysj-containers-builder-${SIGNATURE}")

  set(DOCKERFILE ${SOURCE_DIR}/builder.dockerfile)
  if(NOT EXISTS ${DOCKERFILE})
    message(FATAL_ERROR "HysjContainers: ${DOCKERFILE} not found.")
  endif()

  set(BUILDER_TAG ${HYSJ_CONTAINER_REGISTRY}/${SIGNATURE}/builder:${HYSJ_CONTAINER_VERSION})

  add_custom_target(hysj-containers-${SIGNATURE}-builder-build
    COMMAND ${CMAKE_COMMAND} -E make_directory ${BINARY_DIR}
    COMMAND ${CMAKE_COMMAND} -E copy_directory ${SOURCE_DIR} ${BINARY_DIR}
    COMMAND ${PODMAN_EXECUTABLE} build --pull -t ${BUILDER_TAG} -f ${DOCKERFILE} ${BINARY_DIR}
    BYPRODUCTS ${BINARY_DIR}
    VERBATIM)
  
  add_custom_target(hysj-containers-${SIGNATURE}-builder-push
    COMMAND ${PODMAN_EXECUTABLE} push ${BUILDER_TAG}
    VERBATIM)

endfunction()

hysj_add_builders(arch)
