include_guard()

find_program(GCOV_EXECUTABLE gcov REQUIRED)
find_program(GCOVR_EXECUTABLE gcovr REQUIRED)

add_custom_target(hysj-coverage-html
  COMMAND ${CMAKE_COMMAND} -E rm -rf ${CMAKE_CURRENT_BINARY_DIR}/hysj-coverage/html
  COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_CURRENT_BINARY_DIR}/hysj-coverage/html
  COMMAND ${GCOVR_EXECUTABLE} --html --html-details
  -r ${PROJECT_SOURCE_DIR} -f ${PROJECT_SOURCE_DIR}/sources/*
  --object-directory=${PROJECT_BINARY_DIR}
  -o ${CMAKE_CURRENT_BINARY_DIR}/hysj-coverage/html/index.html
  BYPRODUCTS ${CMAKE_CURRENT_BINARY_DIR}/hysj-coverage/html
  WORKING_DIRECTORY ${PROJECT_BINARY_DIR}
  VERBATIM)

add_custom_target(hysj-coverage-cobertura
  COMMAND ${CMAKE_COMMAND} -E rm -f ${CMAKE_CURRENT_BINARY_DIR}/hysj-coverage-cobertura.xml
  COMMAND ${GCOVR_EXECUTABLE} -x -s
    -r ${PROJECT_SOURCE_DIR} -f ${PROJECT_SOURCE_DIR}/sources/*
    --object-directory=${PROJECT_BINARY_DIR}
    -o ${CMAKE_CURRENT_BINARY_DIR}/hysj-coverage-cobertura.xml
  BYPRODUCTS ${CMAKE_CURRENT_BINARY_DIR}/hysj-coverage-cobertura.xml
  WORKING_DIRECTORY ${PROJECT_BINARY_DIR}
  VERBATIM)

hysj_install_reports(REPORTS ${CMAKE_CURRENT_BINARY_DIR}/hysj-coverage-cobertura.xml
  COMPONENT reports)
