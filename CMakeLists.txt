cmake_minimum_required(VERSION 3.24 FATAL_ERROR)

include(${CMAKE_CURRENT_LIST_DIR}/tools/cmake/HysjPrologue.cmake)

project(
  hysj
  LANGUAGES CXX C
  VERSION ${HYSJ_VERSION_MAJOR}.${HYSJ_VERSION_MINOR}.${HYSJ_VERSION_PATCH}
  DESCRIPTION ${HYSJ_DESCRIPTION}
  HOMEPAGE_URL ${HYSJ_URL})

include(HysjConfig)

add_subdirectory(sources)

if(BUILD_TESTING)
  add_subdirectory(tests)
endif()

if(HYSJ_WITH_BENCHMARKS)
  add_subdirectory(benchmarks)
endif()

if(HYSJ_WITH_DOCUMENTATION)
  add_subdirectory(documentation)
endif()

if(HYSJ_WITH_PYTHON)
  add_subdirectory(python)
endif()

if(HYSJ_WITH_COVERAGE)
  include(HysjCoverage)
endif()

if(HYSJ_WITH_CONTAINERS)
  include(HysjContainers)
endif()

if(HYSJ_WITH_CMAKE_PACKAGE_CONFIG)
  hysj_install_cmake_package_configs()
endif()

hysj_install_licenses()

if(PROJECT_IS_TOP_LEVEL AND HYSJ_WITH_CPACK)
  include(HysjCPack)
endif()

include(${CMAKE_CURRENT_LIST_DIR}/tools/cmake/HysjEpilogue.cmake)
