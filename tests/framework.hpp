#include<concepts>
#include<ranges>
#include<stdfloat>
#include<type_traits>

#include<doctest/doctest.h>
#include<fmt/format.h>
#include<fmt/ranges.h>
#include<fmt/std.h>

#include<hysj/tools/ranges/map.hpp>
#include<hysj/tools/ranges/pins.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/reflection.hpp>
#include<hysj/tools/type_traits.hpp>
#include<hysj/tools/types.hpp>

namespace hysj{

#define HYSJ_TEST_FIXTURE(fixture_name)         \
  using am_test_fixture = void;                 \
  static constexpr auto name(){ return fixture_name; }

template<typename T>
concept test_fixture = requires{
  typename T::am_test_fixture;
  { std::string_view{T::name()} };
};

}

#define FIXTURE(fixture) SUBCASE(std::string_view{fixture.name()}.data())

namespace doctest {

template<hysj::test_fixture T>
String toString(){
  return String{std::string_view{T::name()}.data()};
}

template<hysj::is_constant T>
String toString(){
  return String{fmt::format("{}", T::value).c_str()};
}

template<typename U>
inline constexpr bool is_float = 
  std::is_same_v<U, std::float16_t> ||
  std::is_same_v<U, std::float32_t> ||
  std::is_same_v<U, std::float64_t> ||
  std::is_same_v<U, std::float128_t>;

template<typename T>
requires (!std::is_pointer_v<T> && !std::is_member_pointer_v<T>)
struct StringMaker<T>{
  template<typename U>
  static String convert_impl(const U &value){
    if constexpr(std::is_enum_v<std::remove_cvref_t<U>>){
      if constexpr(hysj::reflected_enum<std::remove_cvref_t<U>>)
        return fmt::format("{}",hysj::enumerator_name(value)).c_str();
      else
        return convert(etoi(value));
    }
    else if constexpr(hysj::is_constant<U>)
      return StringMaker<typename std::remove_cvref_t<U>::value_type>::convert(value());
    else if constexpr(is_float<U>)
      return fmt::format("{}", (double)value).c_str(); //FIXME: damn - jeh
    else if constexpr(std::ranges::input_range<const U &>){
      using V = std::ranges::range_value_t<const U &>;
      if constexpr(is_float<V>)
        return convert_impl(hysj::views::cast<double>(value));
      else if constexpr(fmt::is_formattable<U>::value)
        return fmt::format("{}",value).c_str();
    }
    else if constexpr(fmt::is_formattable<U>::value)
      return fmt::format("{}",value).c_str();

    return detail::StringMakerBase<detail::has_insertion_operator<U>::value>::convert(value);
  }
  template<typename... U>
  static String convert_impl(const std::tuple<U...>& value){
    if constexpr((fmt::is_formattable<U>::value && ...))
      return fmt::format("{}",value).c_str();
    else
      return detail::StringMakerBase<detail::has_insertion_operator<std::tuple<U...>>::value>::convert(value);
  }
  static String convert(const T &value){
    return convert_impl(value);
  }
};

}

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

constexpr auto approx(auto f){
  return doctest::Approx(f);
}
template<auto W>
constexpr auto approx(auto f, constant_t<W>){
  if constexpr(W == 128)
    return approx(f).epsilon(
      static_cast<double>(std::numeric_limits<std::float128_t>::epsilon()) * 100);
  else if constexpr(W == 64)
    return approx(f).epsilon(
      static_cast<double>(std::numeric_limits<std::float64_t>::epsilon()) * 100);
  else if constexpr(W == 32)
    return approx(f).epsilon(
      static_cast<double>(std::numeric_limits<std::float32_t>::epsilon()) * 100);
  else{
    static_assert(W == 16);
    return approx(f).epsilon(
      static_cast<double>(std::numeric_limits<std::float16_t>::epsilon()) * 100);
  }
}

namespace ranges{
  template<typename T,typename U, auto W>
  constexpr bool approx(T &&t, U &&u, constant_t<W> w){
    return std::ranges::equal(fwd(t), fwd(u), [=](const auto &l,const auto &r){
      return l == hysj::approx(r, w);
    });
  }
}

#define CHECK_RANGE_APPROX(a, b, w)                       \
  CHECK_MESSAGE(ranges::approx(a, b, w),                  \
                fmt::format("{} != {}",                   \
                            doctest::toString(a).c_str(), \
                            doctest::toString(b).c_str()));

} //hysj
#include<hysj/tools/epilogue.hpp>
