#include<algorithm>
#include<functional>
#include<ranges>
#include<vector>

#include<hysj/codes.hpp>
#include<hysj/constructions.hpp>
#include<hysj/executions.hpp>
#include<hysj/devices/device.hpp>
#include<hysj/devices/gcc.hpp>
#include<hysj/devices/vulkan.hpp>
#include<hysj/tools/ranges/generate.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/types.hpp>

#include<framework.hpp>
#include<automata/sawtooth.hpp>
#include<constructions.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::constructions::hybrid{

template<natural N>
struct vk_test_fixture{
  HYSJ_TEST_FIXTURE((fmt::format("vk{}", N)));

  static constexpr natural width = N;

  const codes::code &code;
  codes::devsym dev;

  static vkenv make_env(codes::devsym dev){
    auto e = vkenv::make();
    auto it = std::ranges::find(e.devs, devices::vulkan::devtype::cpu, &devices::vulkan::dev::type);
    HYSJ_ASSERT(it != std::ranges::end(e.devs));
    it->sym = dev;
    return e;
  }

  vkenv env = make_env(dev);

  auto exe(codes::apisym a){
    return devs::exe(env, code, a);
  }
};

#define VK_TEST_FIXTURES \
  vk_test_fixture<64>    \

template<natural N, natural M = 0>
struct gcc_test_fixture{
  HYSJ_TEST_FIXTURE((fmt::format("gcc{}", N, M == 0 ? std::string{"hw"} : std::to_string(M))));

  static constexpr natural width = N;

  const codes::code &code;
  codes::devsym dev;

  gccenv env{
    {},
    dev
  };

  auto exe(codes::apisym a){
    return devs::exe(env, code, a);
  }
};

#define GCC_TEST_FIXTURES   \
  gcc_test_fixture<64, 1>

TEST_CASE_TEMPLATE("constructions.hybrid", test_fixture, VK_TEST_FIXTURES, GCC_TEST_FIXTURES){
  using namespace codes::factories;

  codes::code code;

  auto dev = codes::dev(code);

  test_fixture fixture{code, dev};

  auto &env = fixture.env;

  static constexpr auto width = constant_c<fixture.width>;
  auto w = code.builtins.widths[width];
  auto one = code.builtins.lits[codes::literal::one, width];
  auto t = rvar(code, w);
  auto z = ivar(code, w);

  auto top = code.builtins.lits[codes::literal::top, width];

  auto dt = rcst(code, 1.0e-3, w);
  auto t_stop = rcst(code, 1.0e3, w);

  hexecs::config spec{dev, {.dev=dev, .step_size=dt, .stop_time=t_stop}, {dev}};

  SUBCASE("0"){

    auto Q = depvar(code, {z}, 1, {var(code, typ(code, one, w))});

    real a_constant = 0.5;
    auto a = rcst(code, a_constant, w);

    auto X = depvar(code,{t},1,{rvar(code, w)});
    auto xd = X[1];

    std::vector F{
      codes::expr(sub(code,xd,a))
    };
    std::vector H{
      top
    };

    hautomaton automaton{
      .discrete_automaton{
        .variables{
          .independent = z,
          .dependent{Q},
        },
        .transitions{},
        .parameters{}
      },
      .continuous_variables{
        .independent = t,
        .dependent = {X}
      },
      .continuous_parameters{},
      .activities{
        {
          .equation = F[0],
          .guard    = H[0]
        }
      },
      .actions{},
    };

    auto exec = hexecs::cc(code, automaton, spec);
    auto [mem, exe] = devs::exe(env, code, exec.api);
    auto run = hruns::cc(exec, mem, exe, env);

    auto t_mem = run.continuous_time(width);
    auto X_mem = run.continuous_state(width);

    std::ranges::fill(t_mem,rcell<width>{0});
    std::ranges::fill(X_mem[0][0],rcell<width>{0});
    std::ranges::fill(X_mem[0][1],rcell<width>{0});

    auto trace = hybrid::trace(run, width);

    auto e0 = trace();
    REQUIRE(e0);
    REQUIRE(std::holds_alternative<hruns::trace_event<width>>(*e0));
    CHECK(std::get<hruns::trace_event<width>>(*e0).tag == hevent::step);

    auto H_mem = ranges::vec(run.activity_guard(width));
    run.get();
    REQUIRE(H_mem.size() == 1);
    REQUIRE(H_mem[0].size() == 1);
    REQUIRE(H_mem[0][0] == 1_i);

    REQUIRE(run.discrete_construction);
    REQUIRE(run.continuous_construction);
    auto &crun = *run.continuous_construction;
    REQUIRE(crun.exec->equations.size() == 1);
    REQUIRE(crun.exec->roots.container.size() == 0);

    auto e1 = trace();
    REQUIRE(e1);
    REQUIRE(std::holds_alternative<cruns::trace_event<width>>(*e1));
    CHECK(std::get<cruns::trace_event<width>>(*e1).tag == cevent::step);

  }

  if constexpr(width == 64){

    SUBCASE("sawtooth"){

      spec.continuous.stop_time = {codes::rcst(code, 10.0, w)};

      auto automaton = hautomata::sawtooth(code, {1_r,0.5_r,-1_r});

      auto exec = hexecs::cc(code, automaton, spec);
      auto [mem, exe] = devs::exe(env, code, exec.api);
      auto run = hruns::cc(exec, mem, exe, env);

      auto t_mem = run.continuous_time(width);
      auto x_mem = run.continuous_state(width,0,0);
      auto z_mem = run.discrete_time(width);
      auto q_mem = run.discrete_istate(width,0,0);

      std::ranges::fill(t_mem,0.0);
      std::ranges::fill(x_mem,0.0);
      std::ranges::fill(z_mem,0);
      std::ranges::fill(q_mem,0);

      auto actual_trace = hybrid::trace_n(run,width,10);
      CHECK(!actual_trace.empty());
      CHECK(hruns::trace_ccount(actual_trace,width,cevent::root) == 1);
      CHECK(!hybrid::trace_has_failed(actual_trace,width));

    }
  }

}

} //hysj::constructions::hybrid
#include<hysj/tools/epilogue.hpp>
