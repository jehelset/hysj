#include<functional>
#include<ranges>
#include<vector>

#include<hysj/codes.hpp>
#include<hysj/executions.hpp>
#include<hysj/executions/discrete.hpp>
#include<hysj/constructions/discrete.hpp>
#include<hysj/executions.hpp>
#include<hysj/devices/vulkan.hpp>
#include<hysj/devices/gcc.hpp>
#include<hysj/tools/ranges/generate.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/types.hpp>

#include<framework.hpp>
#include<constructions.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::constructions::discrete{

template<natural N>
struct vk_test_fixture{
  HYSJ_TEST_FIXTURE((fmt::format("vk{}", N)));

  static constexpr natural width = N;

  const codes::code &code;
  codes::devsym dev;

  static vkenv make_env(codes::devsym dev){
    auto e = vkenv::make();
    auto it = std::ranges::find(e.devs, devices::vulkan::devtype::cpu, &devices::vulkan::dev::type);
    HYSJ_ASSERT(it != std::ranges::end(e.devs));
    it->sym = dev;
    return e;
  }

  vkenv env = make_env(dev);

  auto exe(codes::apisym a){
    return devs::exe(env, code, a);
  }
};

#define VK_TEST_FIXTURES \
  vk_test_fixture<32>,   \
  vk_test_fixture<64>    \

template<natural N>
struct gcc_test_fixture{
  HYSJ_TEST_FIXTURE((fmt::format("gcc{}", N)));

  static constexpr natural width = N;

  const codes::code &code;
  codes::devsym dev;

  gccenv env{
    {},
    dev
  };

  auto exe(codes::apisym a){
    return devs::exe(env, code, a);
  }
};

#define GCC_TEST_FIXTURES \
  gcc_test_fixture<32>,   \
  gcc_test_fixture<64>


TEST_CASE_TEMPLATE("constructions.discrete", test_fixture, VK_TEST_FIXTURES, GCC_TEST_FIXTURES){
  using namespace codes::factories;

  codes::code code{};

  auto dev = codes::dev(code);
  test_fixture fixture{code, dev};

  auto &env = fixture.env;

  static constexpr auto width = constant_c<fixture.width>;

  auto w = code.builtins.widths[width];
  auto t = ivar(code, w);

  auto zero = code.builtins.lits[codes::literal::zero, width];
  auto one = code.builtins.lits[codes::literal::one, width];
  auto two = code.builtins.lits[codes::literal::two, width];
  auto bot = code.builtins.lits[codes::literal::bot, width];
  auto top = code.builtins.lits[codes::literal::top, width];

  SUBCASE("0"){

    auto Z = icst(code, 2, w);
    auto N = icst(code, 1, w);
    auto i = itr(code, N);
    auto q = var(code, typ(code, Z, w), {i});
    auto Q = depvar(code, {t}, 1, {q});

    dautomaton automaton{.variables{.independent = t, .dependent{Q}},
      .transitions{
        {
          .variable = Q[1],
          .function = one,
          .guard    = top
        }
      }
    };

    auto config = dexecs::config{dev};
    auto exec = dexecs::cc(code, automaton, config);

    auto [mem, exe] = fixture.exe(exec.api);
    auto run = druns::cc(exec, mem, exe);

    run.time(width)[0][0] = 0;
    run.istate(width, 0,0)[0][0] = 0;

    run.init();
    CHECK(std::ranges::equal(run(),make_ring(0_i, 1_i, config.buffer_capacity)));
    run.get();
    CHECK(run.time(width)[0][0] == 0);
    CHECK(run.istate(width,0,0)[0][0] == 0);
    CHECK(run.transition_guard(width,0)[0] == 1);
    CHECK(run.itransition_function(width,0)[0] == 1);

    CHECK(std::ranges::equal(run(),make_ring(0_i, 1_i, config.buffer_capacity)));
    CHECK(run.events[0] == etoi(devent::step));
    run.get();
    CHECK(run.istate(width,0,1)[0][0] == 1);
    CHECK(run.time(width)[0][0] == 1);
    CHECK(run.istate(width,0,0)[0][0] == 1);
    CHECK(run.istate(width,0,1)[0][0] == 1);

  }
  SUBCASE("1"){

    auto Z = icst(code, 2, w);
    auto N = icst(code, 1, w);
    auto i = itr(code, N);
    auto q = var(code, typ(code, Z, w), {i});
    auto Q = depvar(code, {t}, 1, {q});

    dautomaton automaton{
      .variables{.independent = t, .dependent{Q}},
      .transitions{
        {
          .variable = Q[1],
          .function = one,
          .guard = bot
        }
      }
    };

    auto config = dexecs::config{dev};
    auto exec = dexecs::cc(code, automaton, config);
    auto [mem, exe] = fixture.exe(exec.api);
    auto run = druns::cc(exec, mem, exe);
    
    run.time(width)[0][0] =  0;
    run.istate(width,0,0)[0][0] = 0;
    run.put();
    CHECK(std::ranges::equal(run(), make_ring(0_i, 1_i, config.buffer_capacity)));
    CHECK(run.events[0] == etoi(devent::stop));
    run.get();
    CHECK(run.transition_guard(width,0)[0] == 0);
  }
  SUBCASE("2"){
    auto Z = icst(code, 2, w);
    auto N = icst(code, 1, w);
    auto i = itr(code, N);
    auto q = var(code, typ(code, Z, w), {i});
    auto Q = depvar(code, {t}, 1, {q});

    dautomaton automaton{
      .variables{.independent = t, .dependent{Q}},
      .transitions{
        {
          .variable = Q[1],
          .function = one,
          .guard = eq(code,t,icst(code,2,w))
        }
      }
    };

    auto config = dexecs::config{dev};
    auto exec = dexecs::cc(code, automaton, config);
    auto [mem, exe] = fixture.exe(exec.api);
    auto run = druns::cc(exec, mem, exe);
    
    run.time(width)[0][0] =  0;
    run.istate(width,0,0)[0][0] = 0;
    run.put();
    CHECK(std::ranges::equal(run(), make_ring(0_i, 1_i, config.buffer_capacity)));
    CHECK(run.events[0] == etoi(devent::stop));
  }
  SUBCASE("3"){

    auto Z = icst(code, 2, w);
    auto N = icst(code, 1, w);
    auto i = itr(code, N);
    auto q = var(code, typ(code, Z, w), {i});
    auto Q = depvar(code, {t}, 1, {q});

    dautomaton automaton{
      .variables{
        .independent = t,
        .dependent{Q}
      },
      .transitions{
        {
          .variable = Q[1],
          .function = one,
          .guard = top
        }
      }
    };
    auto config = dexecs::config{dev};
    auto exec = dexecs::cc(code, automaton, config);
    auto [mem, exe] = fixture.exe(exec.api);
    auto run = druns::cc(exec, mem, exe);

    run.put();
    CHECK(std::ranges::equal(run(), make_ring(0_i, 1_i, config.buffer_capacity)));
    CHECK(std::ranges::equal(run(), make_ring(0_i, 1_i, config.buffer_capacity)));
    run.get();
    CHECK(run.time(width)[0][0] == 1);
    CHECK(run.istate(width,0,0)[0][0] == 1);
    CHECK(std::ranges::equal(run(), make_ring(0_i, 1_i, config.buffer_capacity)));
    run.get();
    CHECK(run.istate(width,0,0)[0][0] == 0);
  }
  SUBCASE("4"){

    auto Z = icst(code, 2, w);
    auto q = var(code, typ(code, Z, w), {itr(code, icst(code, 1, w))});
    auto Q = depvar(code,{t},1,{q});

    dautomaton automaton{
      .variables{
        .independent = t,
        .dependent{Q}
      },
      .transitions{
        {
          .variable = Q[1],
          .function = one,
          .guard    = bot
        }
      }
    };
    auto config = dexecs::config{dev};
    auto exec = dexecs::cc(code, automaton, config);
    auto [mem, exe] = fixture.exe(exec.api);
    auto run = druns::cc(exec, mem, exe);

    run.time(width)[0][0] = 0;
    run.istate(width,0,0)[0][0] = 0;

    run.put();
    CHECK(std::ranges::equal(run(), make_ring(0_i, 1_i, config.buffer_capacity)));
    run.get();
    CHECK(run.events[0] == etoi(devent::stop));
    CHECK(run.transition_guard(width,0)[0] == 0);
  }
  SUBCASE("4"){

    auto Z = icst(code, 2, w);
    auto q = var(code, typ(code, Z, w), {itr(code, icst(code, 1, w))});
    auto Q = depvar(code,{t},1,{q});

    dautomaton automaton{
      .variables{
        .independent = t,
        .dependent{Q}
      },
      .transitions{}
    };

    auto config = dexecs::config{dev};
    auto exec = dexecs::cc(code, automaton, config);
    auto [mem, exe] = fixture.exe(exec.api);
    auto run = druns::cc(exec, mem, exe);

    CHECK(std::ranges::equal(run(), make_ring(0_i, 1_i, config.buffer_capacity)));
    CHECK(run.events[0] == etoi(devent::stop));
  }
  SUBCASE("5"){
    auto counting_automaton = [&](integer N_value){

      auto N = icst(code, N_value, w);
      auto i = itr(code, N);
      auto Z_bit   = two;
      auto Z_count = icst(code, N_value + 1, w);

      auto q_bit = var(code,typ(code, Z_bit, w),{i});
      auto Q_bit = depvar(code,{t},1,{q_bit});

      auto q_count = var(code,typ(code, Z_count, w));
      auto Q_count = depvar(code,{t},1,{q_count});

      dautomaton automaton{
        .variables{
          .independent = t,
          .dependent = {Q_bit,Q_count}
        }
      };

      auto f_bit_count = con(code,add(code,{q_bit}),{i});
      auto f_count = sub(code,f_bit_count,q_count);
      auto g_count = lnot(code,eq(code,q_count,f_bit_count));
      automaton.transitions.push_back({
          .variable = Q_count[1],
          .function = f_count,
          .guard = g_count
        });
      return automaton;
    };

    for(integer signal_count = 1;signal_count != 4;++signal_count){

      auto automaton = counting_automaton(static_cast<natural>(signal_count));
      auto config = dexecs::config{dev};
      auto exec = dexecs::cc(code, automaton, config);
      auto [mem, exe] = fixture.exe(exec.api);

      std::vector<icell<width>> initial_signal_state(signal_count,0);
      auto signal_begin = initial_signal_state.begin();
      auto signal_end   = signal_begin + signal_count;

      for(integer expected_count = 0;expected_count != signal_count;++expected_count){
        if(expected_count > 0)
          initial_signal_state[expected_count - 1] = 1;
        do{

          auto run = druns::cc(exec, mem, exe);
          auto q_bit_mem = run.istate(width,0,0);
          auto q_count_mem = run.istate(width,1,0);

          REQUIRE(q_bit_mem[0].size() == signal_count);
          REQUIRE(q_count_mem[0].size() == 1);

          std::ranges::copy(initial_signal_state,q_bit_mem[0].data());
          q_count_mem[0][0] = 0;

          run.put();
          if(expected_count == 0){
            CHECK(std::ranges::equal(run(), make_ring(0_i, 1_i, config.buffer_capacity)));
            CHECK(run.events[0] == etoi(devent::stop));
          }
          else{
            CHECK(std::ranges::equal(run(), make_ring(0_i, 1_i, config.buffer_capacity)));
            CHECK(run.events[0] == etoi(devent::step));
            CHECK(std::ranges::equal(run(), make_ring(0_i, 1_i, config.buffer_capacity)));
            CHECK(run.events[0] == etoi(devent::stop));
          }
          run.get();
          CHECK_MESSAGE(expected_count == q_count_mem[0][0],
                        fmt::format("wrong count, {} != {}, with initial signal state {} and final signal state {}",
                                    expected_count,q_count_mem[0][0],initial_signal_state,q_count_mem));
        }while(std::next_permutation(signal_begin,signal_end));
      }
    }
  }
  SUBCASE("6"){

    auto Q = depvar(code, {t}, 1, {var(code, typ(code, two, w))});

    dautomaton automaton{
      .variables{.independent = t, .dependent{Q}},
      .transitions{
        {
          .variable = Q[1],
          .function = one,
          .guard    = top
        }
      }
    };

    std::vector<std::vector<std::vector<icell<width>>>> dependent_states{
      {{0}, {1}},
      {{1}, {1}}
    };
    std::vector<std::vector<bcell<width>>> guards{{1}};

    std::vector<trace_event<width>> expected_trace{
      {devent::step, 0, {dependent_states[0]}, {guards[0]}},
      {devent::step, 1, {dependent_states[1]}, {guards[0]}},
      {devent::step, 2, {dependent_states[0]}, {guards[0]}},
      {devent::step, 3, {dependent_states[1]}, {guards[0]}}
    };


    auto config = dexecs::config{dev};
    auto exec = dexecs::cc(code, automaton, config);
    auto [mem, exe] = fixture.exe(exec.api);
    auto run = druns::cc(exec, mem, exe);

    auto actual_trace =
      trace_n(run, width, expected_trace.size());

    CHECK_MESSAGE(std::ranges::equal(expected_trace,actual_trace),
                  fmt::format("{} != {}",expected_trace,actual_trace));
  }
  SUBCASE("7"){

    auto Q = depvar(code, {t}, 1, {var(code, typ(code, icst(code, 3, w),w ))});

    dautomaton automaton{
      .variables{.independent = t, .dependent = {Q}},
      .transitions{
        {
          .variable = Q[1],
          .function = one,
          .guard = eq(code, Q[0], zero)
        },
        {
          .variable = Q[1],
          .function = one,
          .guard = eq(code, Q[0], one)
        }
      }
    };

    std::vector<std::vector<std::vector<icell<width>>>> dependent_states{
      {{0}, {1}},
      {{1}, {1}},
      {{2}, {0}}
    };
    std::vector<std::vector<std::vector<bcell<width>>>> guards{
      {{1}, {0}},
      {{0}, {1}},
      {{0}, {0}}
    };

    std::vector<trace_event<width>> expected_trace{
      {devent::step, 0, {dependent_states[0]}, guards[0]},
      {devent::step, 1, {dependent_states[1]}, guards[1]},
      {devent::stop, 2, {dependent_states[2]}, guards[2]}
    };

    auto config = dexecs::config{dev};
    auto exec = dexecs::cc(code, automaton, config);
    auto [mem, exe] = fixture.exe(exec.api);
    auto run = druns::cc(exec, mem, exe);

    auto t_mem = run.time(width);
    std::ranges::fill(t_mem[0],0);
    auto q_mem = run.istate(width,0,0);
    std::ranges::fill(q_mem[0],0);
    auto dqdt_mem = run.istate(width,0,1);
    std::ranges::fill(dqdt_mem[0],0);

    auto actual_trace = trace_n(run, width, expected_trace.size());

    CHECK_MESSAGE(std::ranges::equal(expected_trace,actual_trace),
                  fmt::format("{} != {}",expected_trace,actual_trace));
  }
  SUBCASE("8"){
    std::vector Q{
      depvar(code, {t}, 1, {var(code, typ(code, two, w))}),
      depvar(code, {t}, 1, {var(code, typ(code, two, w))})
    };
    dautomaton automaton{
      .variables{
        .independent = t,
        .dependent = Q
      },
      .transitions{
        {
          .variable = Q[0][1],
          .function = one,
          .guard = top
        },
        {
          .variable = Q[1][1],
          .function = copy(code,one),
          .guard = copy(code,top)
        }
      }
    };

    std::vector<std::vector<std::vector<icell<width>>>> dependent_states{
      {{0}, {0}},
      {{0}, {1}},
      {{1}, {1}}
    };
    std::vector<std::vector<std::vector<bcell<width>>>> guards{
      {{0},{0}},
      {{1},{1}}
    };

    std::vector<trace_event<width>> expected_trace{
      {devent::step, 0, {dependent_states[1],dependent_states[1]}, guards[1]},
      {devent::step, 1, {dependent_states[2],dependent_states[2]}, guards[1]},
      {devent::step, 2, {dependent_states[1],dependent_states[1]}, guards[1]},
      {devent::step, 3, {dependent_states[2],dependent_states[2]}, guards[1]}
    };

    auto config = dexecs::config{dev};
    auto exec = dexecs::cc(code, automaton, config);
    auto [mem, exe] = fixture.exe(exec.api);
    auto run = druns::cc(exec, mem, exe);
    auto actual_trace = trace_n(run, width, expected_trace.size());

    CHECK_MESSAGE(std::ranges::equal(expected_trace,actual_trace),
                  fmt::format("{} != {}",expected_trace,actual_trace));
  }
  SUBCASE("9"){

    auto n = icst(code, 2, width);
    auto i = itr(code, n);
    std::vector Q{depvar(code, {t}, 1, {var(code, typ(code, two, w), i)})};
    dautomaton automaton{
      .variables{.independent = t, .dependent = {Q}},
      .transitions{
        {
          .variable = Q[1],
          .function = one,
          .guard = top
        }
      }
    };

    std::vector<std::vector<std::vector<icell<width>>>> dependent_states{
      {{0, 0}, {0, 0}},
      {{0, 0}, {1, 1}},
      {{1, 1}, {1, 1}}
    };
    std::vector<std::vector<std::vector<bcell<width>>>> guards{
      {{0}},
      {{1}}
    };
    std::vector<trace_event<width>> expected_trace{
      {devent::step, 0, {dependent_states[1]}, guards[1]},
      {devent::step, 1, {dependent_states[2]}, guards[1]},
      {devent::step, 2, {dependent_states[1]}, guards[1]}
    };

    for(auto buffer_capacity:std::array{1,2,3,6})
      SUBCASE(fmt::format("buffer_capacity = {}", buffer_capacity).c_str()){
      
        auto config = dexecs::config{dev, buffer_capacity};
        auto exec = dexecs::cc(code, automaton, config);
        auto [mem, exe] = fixture.exe(exec.api);
        auto run = druns::cc(exec, mem, exe);
        auto actual_trace = trace_n(run, width, expected_trace.size());

        CHECK_MESSAGE(std::ranges::equal(expected_trace,actual_trace),
                      fmt::format("{} != {}",expected_trace,actual_trace));
      }

  }
  SUBCASE("10"){

    const auto N_lit = 2;
    const auto N = icst(code, N_lit, width);
    const auto i = itr(code, N), j = itr(code, N);

    const auto M_lit = 3;
    const auto M = icst(code, M_lit, width);
    
    std::vector Q{depvar(code, {t}, 1, {var(code, typ(code, M, w), {i, j})})};
    dautomaton automaton{
      .variables{.independent = t, .dependent = {Q}},
      .transitions{
        {
          .variable = Q[1],
          .function = one,
          .guard = top
        }
      }
    };

    std::vector<std::vector<std::vector<icell<width>>>> dependent_states{
      {
        {{0, 1, 2, 0}, {1, 1, 1, 1}},
        {{1, 2, 0, 1}, {1, 1, 1, 1}},
        {{2, 0, 1, 2}, {1, 1, 1, 1}}
      },
    };
    std::vector<std::vector<std::vector<bcell<width>>>> guards{
      {{1}}
    };
    std::vector<trace_event<width>> expected_trace{
      {devent::step, 0, {dependent_states[0]}, guards[0]},
      {devent::step, 1, {dependent_states[1]}, guards[0]},
      {devent::step, 2, {dependent_states[2]}, guards[0]},
      {devent::step, 3, {dependent_states[0]}, guards[0]}
    };

    for(auto buffer_capacity:std::array{1,2,3,6})
      SUBCASE(fmt::format("buffer_capacity = {}", buffer_capacity).c_str()){
      
        auto config = dexecs::config{dev, buffer_capacity};
        auto exec = dexecs::cc(code, automaton, config);
        auto [mem, exe] = fixture.exe(exec.api);
        auto run = druns::cc(exec, mem, exe);
        {
          auto Q_mem = run.istate(width, 0, 0);
          std::ranges::copy(dependent_states[0][0], Q_mem[0].data());
        }
        auto actual_trace = trace_n(run, width, expected_trace.size());

        CHECK_MESSAGE(std::ranges::equal(expected_trace,actual_trace),
                      fmt::format("{} != {}",expected_trace,actual_trace));
      }

  }
  if constexpr(std::is_same_v<test_fixture, gcc_test_fixture<32>>)
    SUBCASE("11"){

      const auto N_lit = 2;
      const auto N = icst(code, N_lit, width);
      const auto i = itr(code, N);

      auto Q = depvar(code, {t}, 1, {ivar(code, width, {i})});
      dautomaton automaton{
        .variables{.independent = t, .dependent = {Q}},
        .transitions{
          {
            .variable = Q[1],
            .function = one,
            .guard = top
          }
        }
      };

      auto config = dexecs::config{dev, 2};
      auto exec = dexecs::cc(code, automaton, config);
      auto [mem, exe] = fixture.exe(exec.api);
      auto run = druns::cc(exec, mem, exe);
      auto t_mem = run.time(width);
      auto q_mem = run.istate(width, 0, 0);
      auto dqdt_mem = run.istate(width, 0, 1);
    
      t_mem[0][0] = 0;
      q_mem[0][0] = 0;
      q_mem[0][1] = 1;

      run.init();
      auto ring = run();
      REQUIRE(std::ranges::size(ring) == 2);
      run.get();
      CHECK(t_mem[0][0] == 0);
      CHECK(t_mem[1][0] == 1);
      CHECK(q_mem[0][0] == 0);
      CHECK(q_mem[0][1] == 1);
      CHECK(q_mem[1][0] == 1);
      CHECK(q_mem[1][1] == 2);

      CHECK(dqdt_mem[0][0] == 1);
      CHECK(dqdt_mem[0][1] == 1);
      CHECK(dqdt_mem[1][0] == 1);
      CHECK(dqdt_mem[1][1] == 1);

    }
}

} //hysj::constructions::discrete
#include<hysj/tools/epilogue.hpp>
