#include<algorithm>
#include<ranges>
#include<vector>

#include<hysj/codes.hpp>
#include<hysj/executions.hpp>
#include<hysj/constructions.hpp>
#include<hysj/devices/gcc.hpp>
#include<hysj/devices/vulkan.hpp>
#include<hysj/tools/ranges/generate.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/types.hpp>

#include<framework.hpp>
#include<constructions.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::constructions::continuous{

template<natural N>
struct vk_test_fixture{
  HYSJ_TEST_FIXTURE((fmt::format("vk{}", N)));

  static constexpr natural width = N;

  const codes::code &code;
  codes::devsym dev;

  static vkenv make_env(codes::devsym dev){
    auto e = vkenv::make();
    auto it = std::ranges::find(e.devs, devices::vulkan::devtype::cpu, &devices::vulkan::dev::type);
    HYSJ_ASSERT(it != std::ranges::end(e.devs));
    it->sym = dev;
    return e;
  }

  vkenv env = make_env(dev);

  auto exe(codes::apisym a){
    return devs::exe(env, code, a);
  }
};

#define VK_TEST_FIXTURES \
  vk_test_fixture<32>,   \
  vk_test_fixture<64>    \

template<natural N, natural M = 0>
struct gcc_test_fixture{
  HYSJ_TEST_FIXTURE((fmt::format("gcc{}", N, M == 0 ? std::string{"hw"} : std::to_string(M))));

  static constexpr natural width = N;

  const codes::code &code;
  codes::devsym dev;

  gccenv env{
    {},
    dev
  };

  auto exe(codes::apisym a){
    return devs::exe(env, code, a);
  }
};

#define GCC_TEST_FIXTURES   \
  gcc_test_fixture<32, 1>,  \
  gcc_test_fixture<64, 1>

TEST_CASE_TEMPLATE("constructions.continuous.root", test_fixture, VK_TEST_FIXTURES, GCC_TEST_FIXTURES){
  using namespace codes::factories;

  codes::code code{};

  auto dev = codes::dev(code);
  test_fixture fixture{code, dev};

  static constexpr auto width = constant_c<fixture.width>;

  using r = rcell<width()>;
  using rvec = std::vector<r>;

  auto w = code.builtins.widths[width];

  const auto cc = [&](codes::exprfam g, std::optional<codes::declfam> a = {}){
    auto r = cexecs::cc_root(code, dev, g, cexecs::cc_root_sign(code, g));

    r.evaluate_function = {codes::on(code, dev, r.evaluate_function)};
    r.evaluate_sign_next = {codes::on(code, dev, r.evaluate_sign_next)};
    r.evaluate_sign_flip = {codes::on(code, dev, r.evaluate_sign_flip)};
    r.evaluate = {codes::on(code, dev, r.evaluate)};

    const auto bin = codes::api(code, {
        r.get, r.put,
        r.evaluate_function,
        r.evaluate_sign_next,
        r.evaluate_sign_flip,
        r.evaluate
      });
    compile(code, bin);

    REQUIRE(oext(code, r.sign) == oext(code, g));
    REQUIRE(oext(code, r.sign_function) == oext(code, g));
    REQUIRE(oext(code, r.sign_next) == oext(code, r.sign));
    REQUIRE(oext(code, r.sign_flip) == 1);

    const auto api = (!a) ? bin : codes::api(code, {bin, *a});

    auto [mem, exe] = fixture.exe(api);

    auto root_mem = mem.r(width, r.residual);
    auto sign_mem = mem.i64(r.sign);
    auto flip_mem = mem.b64(r.sign_flip);

    REQUIRE(root_mem.size() == oext(code, g));
    REQUIRE(sign_mem.size() == oext(code, g));
    REQUIRE(flip_mem.size() == 1);

    return kumi::make_tuple(std::move(mem), std::move(exe), r,
                            root_mem, sign_mem, flip_mem);
  };

  SUBCASE("g(x) = a")
    for(const auto a_lit:rvec{-1, 0, 1})
      SUBCASE(fmt::format("a = {}", static_cast<double>(a_lit)).c_str()){
        const auto g = rcst(code, a_lit, w);
        auto [mem, exe, root, root_mem, sign_mem, flip_mem] = cc({g});
        const auto s = std::signbit(a_lit) ? -1 : 1;
        for(const auto s_init:std::array{-1, 0, 1}){
          sign_mem[0] = s_init;
          exe.run(root.put);
          exe.run(root.evaluate);
          exe.run(root.get);
          CHECK(root_mem[0] == approx(a_lit, width));
          CHECK(sign_mem[0] == s);
          CHECK(flip_mem[0] == (s_init != s));
        }
      }

  SUBCASE("g(X) = a - X"){
    const auto N = codes::ncst(code, 10, width);
    const auto i = itr(code, N);
    const auto X = codes::rvar(code, width, {i});
    const auto put_X = codes::task(codes::send(code, dev, X));

    for(const auto a_lit:rvec{-1, 0, 1})
      SUBCASE(fmt::format("a = {}", static_cast<double>(a_lit)).c_str()){
        const auto g = sub(code, rcst(code, a_lit, w), X);
        auto [mem, exe, root, root_mem, sign_mem, flip_mem] = cc({g}, codes::decl(put_X));
        auto X_mem = mem.r(width, X);

        std::ranges::fill(X_mem, 0);

        exe.run(put_X);

        const auto s = std::signbit(a_lit) ? -1 : 1;
        for(const auto s_init:std::array{-1, 0, 1}){
          std::ranges::fill(sign_mem, s_init);
          exe.run(root.put);
          exe.run(root.evaluate);
          exe.run(root.get);
          CHECK_RANGE_APPROX(root_mem, std::views::repeat(a_lit, root_mem.size()), width);
          CHECK(std::ranges::equal(sign_mem, std::views::repeat(s, sign_mem.size())));
          CHECK(std::ranges::equal(flip_mem, std::views::repeat(s_init != s, flip_mem.size())));
        }
      }
  }

}

TEST_CASE_TEMPLATE("constructions.continuous.roots", test_fixture, VK_TEST_FIXTURES, GCC_TEST_FIXTURES){
  using namespace codes::factories;

  codes::code code{};

  auto dev = codes::dev(code);
  test_fixture fixture{code, dev};

  static constexpr auto width = constant_c<fixture.width>;

  using r = rcell<width()>;
  using rvec = std::vector<r>;

  auto w = code.builtins.widths[width];

  const auto cc = [&](std::vector<codes::exprfam> G){

    auto e = cexecs::cc_root_event(code);
    auto R = ranges::vec(
      G,
      [&](auto f){
        return cexecs::cc_root(code, dev, f, cexecs::cc_root_sign(code, f));
      });

    auto r = cexecs::cc_roots(code, dev, R, e);

    r.evaluate = {codes::on(code, dev, r.evaluate)};
    r.clear = {codes::on(code, dev, r.clear)};

    const auto bin = codes::api(code, {r.get, r.put,
        r.evaluate,
        r.clear,
      });
    auto [mem, exe] = fixture.exe(bin);

    auto root_mem = ranges::vec(
      R,
      [&](const auto &r){
        return mem.r(width, r.residual);
      });
    auto sign_mem = ranges::vec(
      r.container,
      [&](const auto &r){
        return mem.i64(r.sign);
      });
    auto flip_mem = ranges::vec(
      r.container,
      [&](const auto &r){
        return mem.b64(r.sign_flip);
      });

    auto event_mem = mem.b64(r.event);
    REQUIRE(event_mem.size() == 1);

    return kumi::make_tuple(std::move(mem),
                            std::move(exe), std::move(r),
                            event_mem,
                            std::move(root_mem),
                            std::move(sign_mem),
                            std::move(flip_mem));
  };

  SUBCASE("G(x) = [a]")
    for(const auto a_lit:rvec{-1, 0, 1})
      SUBCASE(fmt::format("a = {}", static_cast<double>(a_lit)).c_str()){
        const auto g = codes::expr(rcst(code, a_lit, w));
        auto [mem, exe, roots, event_mem, root_mem, sign_mem, flip_mem] = cc({g});
        const auto s = std::signbit(a_lit) ? -1 : 1;
        for(const auto s_init:std::array{-1, 0, 1}){
          sign_mem[0][0] = s_init;
          exe.run(roots.put);
          exe.run(roots.evaluate);
          exe.run(roots.get);
          CHECK(event_mem[0] == flip_mem[0][0]);
          exe.run(roots.clear);
          exe.run(roots.get);
          CHECK(event_mem[0] == 0);
        }
      }

  SUBCASE("G(x) = [a0, a1]")
    for(const auto [a0_lit, a1_lit]:std::views::cartesian_product(rvec{-1, 0, 1}, rvec{-1, 0, 1}))
      SUBCASE(fmt::format("a0 = {}, a1 = {}",
                          static_cast<double>(a0_lit),
                          static_cast<double>(a1_lit)).c_str()){

        const auto
          g0 = codes::expr(rcst(code, a0_lit, w)),
          g1 = codes::expr(rcst(code, a1_lit, w));
          
        auto [mem, exe, roots, event_mem, root_mem, sign_mem, flip_mem] = cc({g0, g1});
        const auto s0 = std::signbit(a0_lit) ? -1 : 1,
          s1 = std::signbit(a1_lit) ? -1 : 1;
        for(const auto [s0_init, s1_init]:std::views::cartesian_product(rvec{-1, 0, 1}, rvec{-1, 0, 1})){
          sign_mem[0][0] = s0_init;
          sign_mem[1][0] = s1_init;
          exe.run(roots.put);
          exe.run(roots.evaluate);
          exe.run(roots.get);
          CHECK(event_mem[0] == (flip_mem[0][0] || flip_mem[1][0]));
          exe.run(roots.clear);
          exe.run(roots.get);
          CHECK(event_mem[0] == 0);
        }
      }

}

TEST_CASE_TEMPLATE("constructions.continuous", Width, constant_t<32_n>, constant_t<64_n>){

  using namespace codes::factories;

  static constexpr Width width{};

  codes::code code{};

  auto dev = codes::dev(code);

  auto env = vkdevs::env::make();
  REQUIRE(!env.devs.empty());
  env.devs.front().sym = dev;

  auto w = code.builtins.widths[width];
  auto one = code.builtins.lits[codes::literal::one, width];
  auto t = rvar(code, w);

  constexpr auto ccs = kumi::pop_back(
    kumi::make_tuple(
      #define HYSJ_LAMBDA(cc) kumi::make_tuple(#cc, lift((cexecs::cc)), type_c<cexecs::cc##_config>),
      HYSJ_MAP_LAMBDA((fe)(be))
  #undef HYSJ_LAMBDA
      0));

  kumi::for_each(
    [&](auto cc_param){
      auto [cc_name, cc, cc_spec_type] = cc_param;
      using cc_spec = typename autotype(cc_spec_type)::type;
      auto spec = cc_spec::make(code, dev);
      SUBCASE(cc_name){

        SUBCASE("dx = 1 - x"){

          auto N = icst(code, 1, w);
          auto i = itr(code, N);
          auto x = rvar(code, w, {i});
          auto X = depvar(code, {t}, 1, {x});

          auto f = sub(code,one,X[0]);

          cautomaton automaton{
            .variables{.independent = t, .dependent{X}},
            .equations{
              {f}
            },
            .roots{}
          };

          auto t_step = rcst(code, 0.1, w);
          auto t_stop = rcst(code, 0.19, w);

          spec.step_size = codes::expr(t_step);
          spec.stop_time = codes::expr(t_stop);
          auto exec = cc(code, automaton, spec);
          auto [mem, exe] = devs::exe(env, code, exec.api);
          auto run = cruns::cc(exec, mem, exe);

          auto t_mem = run.time(width);
          auto x_mem = run.state(width,0,0);
          auto dxdt_mem = run.state(width,0,1);

          t_mem[0][0] = 0;
          x_mem[0][0] = 1;
          run.put();

          run();
          CHECK(run.last_event == cevent::step);
          run.get();
          CHECK(t_mem[0][0] == approx(0.0));
          CHECK(x_mem[0][0] == approx(1.0));
          CHECK(dxdt_mem[0][0] == approx(0.0));
          run();
          CHECK(run.last_event == cevent::step);
          run.get();
          CHECK(t_mem[0][0] == approx(0.1));
          CHECK(x_mem[0][0] == approx(1.0));
          CHECK(dxdt_mem[0][0] == approx(0.0));
          run();
          CHECK(run.last_event == cevent::stop);
          run.get();
          CHECK(t_mem[0][0] == approx(0.2));
          CHECK(x_mem[0][0] == approx(1.0));
          CHECK(dxdt_mem[0][0] == approx(0.0));

        }

        SUBCASE("dx = c"){

          auto c = 0.5;
          auto x0 = rcell<width>{-1};

          auto x = rvar(code, w);
          auto X = depvar(code, {t}, 1, {x});
          auto f = rcst(code, c, w);

          cautomaton automaton{
            .variables{.independent = t, .dependent{X}},
            .equations{
              {f}
            },
            .roots{}
          };

          auto dt = 0.1;

          auto t_step = rcst(code, dt, w);
          auto t_stop = rcst(code, 1.0, w);
          spec.step_size = codes::expr(t_step);
          spec.stop_time = codes::expr(t_stop);
          auto exec = cc(code, automaton, spec);

          auto [mem, exe] = devs::exe(env, code, exec.api);
          auto run = cruns::cc(exec, mem, exe);

          auto t_mem = run.time(width);
          auto x_mem = run.state(width,0, 0);
          auto dxdt_mem = run.state(width,0, 1);

          REQUIRE(t_mem.size() == 1);
          REQUIRE(x_mem.size() == 1);
          REQUIRE(dxdt_mem.size() == 1);

          t_mem[0][0] = 0;
          std::ranges::fill(x_mem[0], x0);
          run.put();
          run();
          CHECK(run.last_event == cevent::step);
          run.get();
          CHECK(t_mem[0][0] == approx(0.0));
          CHECK_MESSAGE(x_mem[0][0] == approx(x0),
                        fmt::format("{} != {}", static_cast<double>(x_mem[0][0]), static_cast<double>(x0)));
          CHECK_MESSAGE(dxdt_mem[0][0] == approx(c),
                        fmt::format("{} != {}", static_cast<double>(dxdt_mem[0][0]), static_cast<double>(c)));
          run();
          CHECK(run.last_event == cevent::step);
          run.get();
          CHECK(t_mem[0][0] == approx(dt));
          CHECK_MESSAGE(x_mem[0][0] == approx(x0 + dt*c),
                        fmt::format("{} != {}", static_cast<double>(x_mem[0][0]), static_cast<double>(x0 + dt*c)));
          CHECK_MESSAGE(dxdt_mem[0][0] == approx(c),
                        fmt::format("{} != {}", static_cast<double>(dxdt_mem[0][0]), static_cast<double>(c)));

        }

        SUBCASE("dx = c·x"){

          auto c = -0.5;
          auto x0 = rcell<width>{1};

          auto x = rvar(code, w);
          auto X = depvar(code, {t}, 1, {x});
          auto f = mul(code, {rcst(code, c, w), x});

          cautomaton automaton{
            .variables{.independent = t, .dependent{X}},
            .equations{
              {f}
            },
            .roots{}
          };

          auto dt = 0.0001;
          auto t_step = rcst(code, dt, w);
          auto t_stop = rcst(code, 1.0, w);

          spec.step_size = codes::expr(t_step);
          spec.stop_time = codes::expr(t_stop);
          auto exec = cc(code, automaton, spec);

          auto [mem, exe] = devs::exe(env, code, exec.api);
          auto run = cruns::cc(exec, mem, exe);

          auto t_mem = run.time(width);
          auto x_mem = run.state(width,0, 0);
          auto dxdt_mem = run.state(width,0, 1);

          REQUIRE(t_mem.size() == 1);
          REQUIRE(x_mem.size() == 1);
          REQUIRE(dxdt_mem.size() == 1);

          t_mem[0][0] = 0;
          std::ranges::fill(x_mem[0], x0);
          CHECK(t_mem[0][0] == 0);
          CHECK(x_mem[0][0] == approx(x0));
          run.put();
          run();
          CHECK(run.last_event == cevent::step);
          run.get();
          CHECK(t_mem[0][0] == approx(0.0));
          CHECK_MESSAGE(x_mem[0][0] == approx(x0).epsilon(0.1),
                        fmt::format("{} != {}", static_cast<double>(x_mem[0][0]), static_cast<double>(x0)));
          CHECK_MESSAGE(dxdt_mem[0][0] == approx(c*x0).epsilon(0.1),
                        fmt::format("{} != {}", static_cast<double>(dxdt_mem[0][0]), static_cast<double>(c)));
          run();
          CHECK(run.last_event == cevent::step);
          run.get();
          CHECK(t_mem[0][0] == approx(dt));
          CHECK_MESSAGE(x_mem[0][0] == approx(x0 + dt*x0*c).epsilon(0.1),
                        fmt::format("{} != {}", static_cast<double>(x_mem[0][0]), static_cast<double>(x0 + dt*c)));
          CHECK_MESSAGE(dxdt_mem[0][0] == approx(c * x_mem[0][0]).epsilon(0.1),
                        fmt::format("{} != {}", static_cast<double>(dxdt_mem[0][0]), static_cast<double>(c)));

        }

      }
    },
    ccs);

}

} //hysj::constructions::continuous
#include<hysj/tools/epilogue.hpp>
