#include<utility>

#include<kumi/tuple.hpp>

#include"framework.hpp"

#include<hysj/codes/code.hpp>
#include<hysj/codes/factories.hpp>
#include<hysj/codes/grammar.hpp>
#include<hysj/devices/vulkan.hpp>
#include<hysj/devices/gcc.hpp>
#include<hysj/numerics/newtons_method.hpp>
#include<hysj/numerics/fixed_point_iteration.hpp>
#include<hysj/numerics/bisection_method.hpp>
#include<hysj/tools/types.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::numerics{

template<natural N>
struct vk_test_fixture{
  HYSJ_TEST_FIXTURE((fmt::format("vk{}", N)));

  static constexpr natural width = N;

  const codes::code &code;
  codes::devsym dev;

  static vkenv make_env(codes::devsym dev){
    auto e = vkenv::make();
    auto it = std::ranges::find(e.devs, devices::vulkan::devtype::cpu, &devices::vulkan::dev::type);
    HYSJ_ASSERT(it != std::ranges::end(e.devs));
    it->sym = dev;
    return e;
  }

  vkenv env = make_env(dev);

  auto exe(codes::apisym a){
    return devs::exe(env, code, a);
  }
};

#define VK_TEST_FIXTURES \
  vk_test_fixture<32>,   \
  vk_test_fixture<64>    \

template<natural N, natural M = 0>
struct gcc_test_fixture{
  HYSJ_TEST_FIXTURE((fmt::format("gcc{}", N, M == 0 ? std::string{"hw"} : std::to_string(M))));

  static constexpr natural width = N;

  const codes::code &code;
  codes::devsym dev;

  gccenv env{
    {}, dev
  };

  auto exe(codes::apisym a){
    return devs::exe(env, code, a);
  }
};

#define GCC_TEST_FIXTURES   \
  gcc_test_fixture<32, 1>,  \
  gcc_test_fixture<64, 1>

TEST_CASE_TEMPLATE("numerics.fixed_point_iteration", test_fixture, VK_TEST_FIXTURES, GCC_TEST_FIXTURES){

  using namespace codes::factories;

  codes::code code{};
  auto dev = codes::dev(code);
  test_fixture fixture{code, dev};

  static constexpr auto width = constant_c<fixture.width>;

  auto w = code.builtins.widths[width];

  const auto maxiter = 10;
  auto N = icst(code, maxiter, w);

  auto A = 1.0e-3;
  auto abstol = rcst(code, A, w);

  auto run = [&](const fixed_point_iteration &fp){

    auto send = codes::send(code, dev, fp.variable);
    auto recv = codes::when(code, {
        codes::recv(code, dev, fp.variable),
        codes::recv(code, dev, fp.residual),
        codes::recv(code, dev, fp.last_function),
        codes::recv(code, dev, fp.status)
      });

    auto algo = codes::then(code, {
        send,
        codes::on(code, dev, fp.run),
        recv
      });

    auto bin = codes::api(code, {algo});
    auto [mem, exe] = fixture.exe(bin);

    auto variable = mem.r(width, fp.variable);
    auto status = mem.i32(fp.status);
    return kumi::make_tuple(std::move(mem), std::move(exe), algo, variable, status);
  };

  SUBCASE("x"){

    auto x = rvar(code, w);
    auto f = x;

    auto fp_func = numerics::fp_func(code, {f}, {x}, {N});
    auto fp = numerics::fp(code, fp_func, {lt(code, rms(code, {fp_func.residual}), abstol)});

    auto [mem, exe, algo, x_mem, s_mem] = run(fp);

    for(auto x_init:std::array{0, 4, -8})
      SUBCASE(fmt::format("x₀ = {}", x_init).c_str()){
        x_mem[0] = x_init;
        exe.run({algo});
        CHECK(s_mem[0] == 0);
        CHECK(x_mem[0] == approx(x_init));
      }

  }
  SUBCASE("x + x"){

    auto x = rvar(code, w);
    auto f = add(code, {x, x});

    auto fp_func = numerics::fp_func(code, {f}, {x}, {N});
    auto fp = numerics::fp(code, fp_func, {lt(code, rms(code, {fp_func.residual}), abstol)});

    auto [mem, exe, algo, x_mem, s_mem] = run(fp);

    for(auto x_init:std::array{0})
      SUBCASE(fmt::format("x₀ = {}", x_init).c_str()){
        x_mem[0] = x_init;
        exe.run({algo});
        CHECK(s_mem[0] == 0);
        CHECK(x_mem[0] == approx(x_init));
      }

    for(auto x_init:std::array{-2, 2})
      SUBCASE(fmt::format("x₀ = {}", x_init).c_str()){
        x_mem[0] = x_init;
        exe.run({algo});
        auto f_mem = mem.r(width, fp.last_function);
        auto r_mem = mem.r(width, fp.residual);
        CHECK(s_mem[0] == maxiter);
      }

  }
  SUBCASE("c·x"){

    for(auto c_value:std::array{-0.9, -0.4, -0.2, 0.2, 0.4, 0.9}){

      auto c = rcst(code, c_value, w);
      auto x = rvar(code, w);
      auto f = mul(code, {c, x});

      auto fp_func = numerics::fp_func(code, {f}, {x}, {N});
      auto fp = numerics::fp(code, fp_func, {lt(code, rms(code, {fp_func.residual}), abstol)});

      auto [mem, exe, algo, x_mem, s_mem] = run(fp);

      for(auto x_init:std::array{-10, -1, 0, 1, 10})
        SUBCASE(fmt::format("c = {}, x₀ = {}", c_value, x_init).c_str()){
          x_mem[0] = x_init;
          exe.run({algo});
          if(std::abs(std::pow(c_value, maxiter)*x_init) < A)
            CHECK_MESSAGE(s_mem[0] != maxiter,
                          fmt::format("s = {} == {}, x = {}",
                                      s_mem[0], maxiter, (double)x_mem[0]));
          else
            CHECK_MESSAGE(s_mem[0] == maxiter,
                          fmt::format("s = {} != {}, x = {}",
                                      s_mem[0], maxiter, (double)x_mem[0]));
        }

    }

  }
}

TEST_CASE_TEMPLATE("numerics.newtons_method", test_fixture, VK_TEST_FIXTURES, GCC_TEST_FIXTURES){

  using namespace codes::factories;

  codes::code code{};
  auto dev = codes::dev(code);
  test_fixture fixture{code, dev};

  static constexpr auto width = constant_c<fixture.width>;

  auto w = code.builtins.widths[width];

  const auto maxiter = 32;
  auto N = icst(code, maxiter, w);

  const auto A = 1.0e-3;
  auto abstol = rcst(code, A, w);

  auto run = [&](const newtons_method &nm){

    auto send = codes::send(code, dev, nm.variable);
    auto recv = codes::when(code, {
        codes::recv(code, dev, nm.variable),
        codes::recv(code, dev, nm.current_derivative),
        codes::recv(code, dev, nm.newton_direction),
        codes::recv(code, dev, nm.residual),
        codes::recv(code, dev, nm.status)
      });

    auto algo = codes::then(code, {
        send,
        codes::on(code, dev, nm.run),
        recv
      });

    auto bin = codes::api(code, {algo});
    auto [mem, exe] = fixture.exe(bin);

    auto variable = mem.r(width, nm.variable),
      current_derivative = mem.r(width, nm.current_derivative),
      newton_direction = mem.r(width, nm.newton_direction),
      residual = mem.r(width, nm.residual);
    auto status = mem.i32(nm.status);
    return kumi::make_tuple(std::move(mem), std::move(exe), algo, variable, status,
                            current_derivative, newton_direction, residual);
  };

  //FIXME:
  //  add error-handling to linear-solve & propagate to newtons-method,
  //  or require that nm function is of x.
  //  - jeh
  // SUBCASE("f(x) = a"){
  //   auto l = rcst(code, 0.5, w);
  //   for(auto [n, x_init, a_init]:std::views::cartesian_product(std::vector{1, 2, 10},
  //                                            std::vector{-4, 0, 4},
  //                                            std::vector{-1, 0, 1}))
  //     SUBCASE(fmt::format("n = {},x₀ = {}, a = {}", n, x_init, a_init).c_str()){
  //       auto i = itr(code, ncst(code, n, w));
  //       auto x = rvar(code, w, {i});
  //       auto a = rcst(code, a_init, w);

  //       auto f = a;
  //       auto dfdt = code.builtins.lits[codes::literal::zero, width];

  //       auto nm_func = numerics::nm_func(code, {f}, {dfdt}, {x}, {l}, {N});
  //       auto nm = numerics::nm(code, nm_func, {lt(code, rms(code, {nm_func.residual}), abstol)});

  //       auto [mem, exe, algo, x_mem, s_mem, dxdt_mem, newt_mem, f_mem] = run(nm);

  //       x_mem[0] = x_init;
  //       exe.run({algo});

  //       if(a_init == 0)
  //         CHECK(s_mem[0] == 0);
  //       else
  //         CHECK(s_mem[0] == maxiter);
  //     }
  // }
  //FIXME: sign of newton direction seems to be "one step behind"
  //       see ../newton_bug.mp4
  //       - jeh
  SUBCASE("f(x) = a·x + b"){
    for(auto [l_init, n, x_init, a_init, b_init]:std::views::cartesian_product(std::vector{0.3, 0.5},
                                                             std::vector{1, 2, 16}, //FIXME: bug 10 fails - jeh
                                                             std::vector{-4, 0, 4},
                                                             std::vector{-1, 1},
                                                             std::vector{-1, 0, 1}))
      SUBCASE(fmt::format("l = {}, n = {}, x₀ = {}, a = {}, b = {}", l_init, n, x_init, a_init, b_init).c_str()){
        auto i = itr(code, ncst(code, n, w)),
          j = itr(code, ncst(code, n, w));
        auto x = rvar(code, w, {i});

        auto a = rcst(code, a_init, w), b = rcst(code, b_init, w);
        auto f = codes::add(code, {codes::mul(code, {a, x}), b});
        auto dfdt = codes::mul(code, {a, codes::eq(code, i, j)});
        auto l = rcst(code, l_init, w);

        auto nm_func = numerics::nm_func(code, {f}, {dfdt}, {x}, {l}, {N});
        auto nm = numerics::nm(code, nm_func, {lt(code, rms(code, {nm_func.residual}), abstol)});

        auto [mem, exe, algo, x_mem, s_mem, dxdt_mem, newt_mem, f_mem] = run(nm);

        std::ranges::fill(x_mem, x_init);
        exe.run({algo});

        REQUIRE(s_mem[0] < maxiter);
        CHECK(x_mem[0] == approx(-b_init / a_init).epsilon(A * n));

      }
  }

}

TEST_CASE_TEMPLATE("numerics.bisection_method", test_fixture, VK_TEST_FIXTURES, GCC_TEST_FIXTURES){

  using namespace codes::factories;

  codes::code code{};
  auto dev = codes::dev(code);
  test_fixture fixture{code, dev};

  static constexpr auto width = constant_c<fixture.width>;

  using r = rcell<width()>;

  auto w = code.builtins.widths[width];

  const auto maxiter = 24;
  auto N = icst(code, maxiter, w);

  auto A = 1.0e-3;
  auto abstol = rcst(code, A, w);

  auto run = [&](const bisection_method &bm, std::optional<codes::declfam> bm_api = {}){

    auto send = codes::send(code, dev, bm.variable);
    auto recv = codes::when(code, {
        codes::recv(code, dev, bm.variable),
        codes::recv(code, dev, bm.bracket_left),
        codes::recv(code, dev, bm.bracket_right),
        codes::recv(code, dev, bm.residual),
        codes::recv(code, dev, bm.status)
      });

    auto algo = codes::then(code, {
        send,
        codes::on(code, dev, bm.run),
        recv
      });

    auto bin = codes::api(code, {algo});
    if(bm_api)
      bin = codes::api(code, {bin, *bm_api});
    auto [mem, exe] = fixture.exe(bin);

    auto variable = mem.r(width, bm.variable);
    auto residual = mem.r(width, bm.residual);
    auto status = mem.i32(bm.status);
    return kumi::make_tuple(std::move(mem), std::move(exe), algo, variable, residual, status);
  };

  SUBCASE("diverge"){

    auto x = rvar(code, w);

    auto bsm_func = numerics::bsm_func(
      code, {code.builtins.lits[codes::literal::zero, width]}, {x},
      {code.builtins.lits[codes::literal::negone, width]},
      {code.builtins.lits[codes::literal::one, width]},
      {N});
    auto bsm = numerics::bsm(code, bsm_func, {code.builtins.lits[codes::literal::bot, width]});
    auto [mem, exe, algo, x_mem, f_mem, s_mem] = run(bsm);

    exe.run({algo});
    CHECK(s_mem[0] == maxiter);
  }

  SUBCASE("x"){

    auto x = rvar(code, w),
      x_min = rvar(code, w),
      x_max = rvar(code, w);
    auto f = x;

    auto bsm_func = numerics::bsm_func(
      code, {f}, {x},
      {x_min}, {x_max},
      {N});
    auto bsm = numerics::bsm(code, bsm_func, {lt(code, rms(code, {bsm_func.residual}), abstol)});

    auto send_limits = codes::when(
      code,
      {
        codes::send(code, dev, x_min),
        codes::send(code, dev, x_max)
      });

    auto [mem, exe, algo, x_mem, f_mem, s_mem] = run(bsm, codes::declfam(codes::taskfam(send_limits)));

    auto x_min_mem = mem.r(width, x_min),
      x_max_mem = mem.r(width, x_max);

    for(auto [x_left, x_right]:std::array{
        std::array{-1.0,  0.5},
        std::array{-1.0,  0.1},
        std::array{-0.1,  0.5},
        std::array{-1.0,  0.0},
        std::array{ 0.0,  0.5}
      })
      SUBCASE(fmt::format("x₀ = {}, x₁ = {}", x_left, x_right).c_str()){
        x_min_mem[0] = r(x_left);
        x_max_mem[0] = r(x_right);
        exe.run({send_limits});
        x_mem[0] = r(x_left);
        exe.run({algo});
        CHECK(s_mem[0] < maxiter);
        CHECK(x_mem[0] == approx(r(0)).epsilon(A));
      }
  }
}

}
#include<hysj/tools/epilogue.hpp>
