#include<type_traits>

#include"../framework.hpp"

#include<hysj/tools/utility.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

enum test_enum : int { A, B };

TEST_CASE("utility"){
  CHECK(to_underlying(A) == 0);
  CHECK(to_underlying(std::integral_constant<test_enum, B>{})() == 1);
}

} //hysj
#include<hysj/tools/epilogue.hpp>
