#include<vector>

#include"../framework.hpp"

#include<hysj/tools/preprocessor.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

TEST_CASE("preprocessor"){
  SUBCASE("concat"){
    REQUIRE(HYSJ_CAT(1,0) == 10);
  }
  #define TEST_SEQUENCE_0 (3)(2)(4)
  #define TEST_SEQUENCE_1 (0,1)(2,3)(4,4)
  #define TEST_SEQUENCE_2 (0,1,0)(2,3,2)(4,4,4)
  SUBCASE("size"){
    REQUIRE(HYSJ_SIZE() == 0);
    REQUIRE(HYSJ_SIZE(TEST_SEQUENCE_0) == 3);
  }
  SUBCASE("at"){
    REQUIRE(HYSJ_AT(0,TEST_SEQUENCE_0) == 3);
    REQUIRE(HYSJ_AT(1,TEST_SEQUENCE_0) == 2);
    REQUIRE(HYSJ_AT(2,TEST_SEQUENCE_0) == 4);
  }
  SUBCASE("map"){
    #define F(_,a) ,(_ + a)
    std::vector X{ 0 HYSJ_MAP(F,1,TEST_SEQUENCE_0) },
      Y{0,4,3,5};
    REQUIRE(X == Y);
    #undef F
  }
  SUBCASE("splat"){
    std::vector
      X0{ HYSJ_SPLAT(0,HYSJ_PROJECT(1,TEST_SEQUENCE_1)) },
      X1{ 1,3,4 };
    REQUIRE(X0 == X1);
  }
  SUBCASE("project"){
    std::vector
      X0{ HYSJ_SPLAT(0,HYSJ_PROJECT(1,TEST_SEQUENCE_1)) },
      X1{ 1,3,4 };
    REQUIRE(X0 == X1);
  }
  SUBCASE("select"){
    #define HYSJ_SEQUENCE_3 HYSJ_SELECT((0)(2),TEST_SEQUENCE_2)
    std::vector
      X0{ HYSJ_SPLAT(0,HYSJ_SELECT((0)(2),TEST_SEQUENCE_2)) },
      X1{ 0,2,4 };
    REQUIRE(X0 == X1);
    #undef HYSJ_SEQUENCE_3
  }
  #undef TEST_SEQUENCE_0

}

} //hysj
#include<hysj/tools/epilogue.hpp>
