#include<algorithm>
#include<array>
#include<functional>
#include<ranges>
#include<span>
#include<tuple>
#include<type_traits>

#include<kumi/tuple.hpp>

#include"../framework.hpp"

#include<hysj/tools/tag_invoke.hpp>
#include<hysj/tools/functional/always.hpp>
#include<hysj/tools/functional/any_of.hpp>
#include<hysj/tools/functional/bind.hpp>
#include<hysj/tools/functional/combine.hpp>
#include<hysj/tools/functional/compose.hpp>
#include<hysj/tools/functional/fold.hpp>
#include<hysj/tools/functional/hash.hpp>
#include<hysj/tools/functional/ignore.hpp>
#include<hysj/tools/functional/invoke.hpp>
#include<hysj/tools/functional/none_of.hpp>
#include<hysj/tools/functional/on.hpp>
#include<hysj/tools/functional/optional.hpp>
#include<hysj/tools/functional/overload.hpp>
#include<hysj/tools/functional/unwrap.hpp>
#include<hysj/tools/functional/variant.hpp>
#include<hysj/tools/functional/with.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

TEST_SUITE_BEGIN("tools.functional");

namespace hooks{
  inline constexpr
  struct hook_fn{

    friend constexpr auto
    tag_invoke(hook_fn,auto &a)
      arrow(((a.hook)))

    constexpr auto
    operator()(auto &&a)const
      arrow((tag_invoke(*this,unwrap(a))))

  }
    hook{};
}
using hooks::hook;

struct ok{
  int hook = 2;
};
struct also_ok{
  friend auto tag_invoke(hooks::hook_fn,also_ok){ return 4; }
};

TEST_CASE("tools.default"){
  REQUIRE(hook(ok{}) == 2);
  REQUIRE(hook(also_ok{}) == 4);
}

TEST_CASE("tools.always"){
  static_assert(std::is_same_v<decltype(always()(0,1,2)),void>);
  static_assert(noexcept(always(2)()));
  REQUIRE(always(2)() == 2);
  REQUIRE(always(2)(0) == 2);
}

TEST_CASE("tools.combine"){
  auto sum = [](auto... x){ return (x + ... + 0); };

  REQUIRE(
    combine<0>(sum,
               always(2))(0) == 2);
  REQUIRE(
    combine<1>(sum,
               always(3))(1,2,3) == 7);
  REQUIRE(
    combine<-1>(sum,
                always(2))(2,1) == 4);
  REQUIRE(
    combine<>(sum,
              always(2))(0,0) == 4);
  REQUIRE(
    combine<0,-1>(sum,
                  always(5))(3,2,1) == 12);

}

TEST_CASE("tools.compose"){
  auto sum = [](auto... x){ return (x + ... + 0); };
  auto minus = std::minus<>{};
  auto plus = std::plus<>{};
  
  auto add_1 = bind<>(plus,1);

  REQUIRE(compose<>(add_1,sum)(1,1,2) == 5);
  REQUIRE(compose<>(add_1,add_1)(0) == 2);
  REQUIRE(compose<>(add_1,add_1,add_1)(0) == 3);
  REQUIRE(compose<>(add_1,sum)(1,1,2) == 5);

  REQUIRE(compose<-2>(plus,minus)(1,2,3) == 0);
  REQUIRE(compose<2>(plus,minus)(1,2,3) == 2);
  REQUIRE(compose<-2>(minus,minus)(1,8,4) == -3);

  struct X{
    int y;
  };

  auto f = [] -> X &{
    static X x{};
    return x;
  };
  auto g = compose<>(&X::y, f);

  static_assert(std::is_same_v<decltype(g()), int &>);
}

TEST_CASE("tools.on"){
  auto sum = [](auto... x){ return (x + ... + 0); };
  auto add_1 = std::bind_front(std::plus<>{},1);

  REQUIRE(on<0,-1>(sum,add_1,add_1)(1,1,2) == 6);
  REQUIRE(on<0>(add_1,add_1)(0) == 2);
}

TEST_CASE("tools.unwrap"){
  auto i = 0;
  auto j = std::ref(i);

  REQUIRE(&unwrap(i) == &i);
  REQUIRE(&unwrap(j) == &i);
}

TEST_CASE("tools.apply"){
  auto j = apply([](auto i,auto j){ return i + j; },kumi::tuple{2,3});
  REQUIRE(j == 5);
}

TEST_CASE("tools.bind"){
  auto sum = [](auto... x){ return (x + ... + 0); };
  auto minus = std::minus<>{};
  REQUIRE(bind<>(sum,1,2)(3) == 6);
  REQUIRE(bind<-1,0>(sum,1,2)(3) == 6);
  REQUIRE(bind<-1>(minus,2)(1) == -1);
  REQUIRE(bind<-1,0>(minus,2,1)() == -1);

  static_assert(bind<>(std::minus<>{},2)(3) == -1);
  static_assert(bind<-1,-2>(std::minus<>{},3,2)() == -1);
  static_assert(bind<-1>(std::minus<>{},2)(3) == 1);
}

TEST_CASE("tools.overload"){

  REQUIRE(overload([](constant_t<0>){ return 'a'; },
                   [](constant_t<1>){ return 0;  })
          (constant_c<0>) == 'a');
  static_assert(overload([](int){ return 0; },
                         [](float){ return 1; })(float{}) == 1);
}

TEST_CASE("tools.at"){
  std::vector v{0,1,2};
  REQUIRE(std::bind_front(at,v)(0) == 0);
  REQUIRE(std::bind_front(at,v)(2) == 2);
}

TEST_CASE("tools.variant"){
  std::variant<int,char> v = 3, u = 'x';

  REQUIRE(std::get<0>(v) == 3);
  REQUIRE(visitor([](int){ return true; },
                  [](char){ return false; })(v));
  REQUIRE(visitor([](int){ return false; },
                  [](char){ return true; })(u));
  
}

enum class myenum : unsigned int {a,b};

HYSJ_MIRROR_ENUM(myenum,(a)(b));

TEST_CASE("tools.enum"){
  REQUIRE(enumerator_count<myenum> == 2);
  constexpr auto EN = enumerator_names<myenum>;
  REQUIRE(std::ranges::equal(EN,std::array{"a","b"}));
  REQUIRE(enum_cast<myenum>(2) == std::nullopt);
}

TEST_SUITE_END();


}
#include<hysj/tools/epilogue.hpp>
