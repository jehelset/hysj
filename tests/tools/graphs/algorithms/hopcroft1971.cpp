#include<ranges>
#include<unordered_set>
#include<utility>
#include<vector>

#include"framework.hpp"
#include"tools/graphs.hpp"

#include<hysj/tools/types.hpp>
#include<hysj/tools/graphs/directions.hpp>
#include<hysj/tools/graphs/elements.hpp>
#include<hysj/tools/graphs/incidence_graphs.hpp>
#include<hysj/tools/graphs/indirected_graphs.hpp>
#include<hysj/tools/graphs/partitions.hpp>
#include<hysj/tools/graphs/algorithms/hopcroft1971.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::graphs{
  
using graph = incidence_graph<bool,natural>;
using partitioning = prt::partitioning;

TEST_CASE("graphs.hopcroft1971"){

  SUBCASE("A"){
    auto make_automaton =
      [&](auto n){
        graph g{};

        for(auto i:views::indices(n))
          g.vertex(i == 0);
        for(auto i:graphs::vertex_ids(g)){
          if(i == 0)
            g.edge(i,i,natural{0});
          else
            g.edge(i,vertex_id{i - 1},natural{0});
          g.edge(i,i,natural{1});
        }
        partitioning p = prt::construct(graphs::vertex_ids(g),
                             [&](const auto& v0,const auto &v1){
                               return g[v0] < g[v1];
                             });
        auto a = 2_n;

        return std::make_tuple(
          g,a,p,
          ranges::vec(
            views::indices(n),
            [](auto i){ return std::vector{vertex_id{i}}; }));
      };

    for(auto n:views::indices(0_n,10_n)){
      auto c = fmt::format("{}",n);
      SUBCASE(c.c_str()){
        auto [g,a,P_initial,P_expected] = make_automaton(n);
        auto P_actual = hop71::construct(std::cref(g),a,std::cref(g),P_initial);
        REQUIRE_MESSAGE(sorted_equal(P_actual,P_expected),
                        fmt::format("{} != {}",P_actual,P_expected));
                
      }
    }
  }
  SUBCASE("C"){
    auto make_automaton =
      [&](auto n){
        graph g{};
        for(auto i:views::indices(n))
          g.vertex(i == 0);
        for(auto i:graphs::vertex_ids(g))
          g.edge(i,i,natural{0});
        auto a = 2_n;
        partitioning p = prt::construct(graphs::vertex_ids(g),
                                        [&](const auto& v0,const auto &v1){
                                          return g[v0] < g[v1];
                                        });
        return std::make_tuple(
          g,a,p,
          std::vector{
            std::vector{vertex_id{0}},
              ranges::vec(
                views::indices(1_n,n),
                [](auto i){ return vertex_id{i}; })
              });
      };

    for(auto n:views::indices(2_n,10_n)){
      auto c = fmt::format("C({})",n);
      SUBCASE(c.c_str()){
        auto [g,a,P_initial,P_expected] = make_automaton(n);
        auto P_actual = hop71::construct(std::cref(g),a,std::cref(g),P_initial);
        REQUIRE_MESSAGE(sorted_equal(P_actual,P_expected),
                        fmt::format("{} != {}",P_actual,P_expected));
      }
    }
  }
}

} //hysj::graphs
#include <hysj/tools/epilogue.hpp>
