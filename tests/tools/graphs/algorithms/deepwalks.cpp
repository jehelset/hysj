#include<ranges>
#include<unordered_set>
#include<utility>
#include<vector>

#include"framework.hpp"
#include"tools/graphs.hpp"

#include<hysj/tools/types.hpp>
#include<hysj/tools/graphs/directions.hpp>
#include<hysj/tools/graphs/elements.hpp>
#include<hysj/tools/graphs/algorithms/deepwalks.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::graphs{

TEST_CASE("graphs.deepwalks"){
  
  constexpr auto has_consistent_trace =
    [&](const auto &graph,const auto &trace){

      const auto vertex_count = order(graph);
      if(vertex_count && trace.empty())
        return false;

      auto count = bind<>(std::ranges::count_if,trace);

      const auto start_count = count([](auto e){ return e.tag == dw::start; }),
        stop_count = count([](auto e){ return e.tag == dw::stop; });
      if(start_count != stop_count)
        return false;
      const auto edge_count = size(graph);
      const auto 
        down_count = count([](auto e){ return e.tag == dw::down; }),
        right_count = count([](auto e){ return e.tag == dw::right; });

      return edge_count == down_count && edge_count == right_count;
    };

  SUBCASE("line_graphs"){

    SUBCASE("in"){
      for(auto n:views::indices(1_n,10_n)){
        CAPTURE(n);
        const auto graph = make_line_graph(n);
        {
          const auto trace = ranges::vec(dw::trace(o,graph,views::pin(vertex_id{0_n})));
          CHECK_MESSAGE(has_consistent_trace(graph,trace),trace);
        }
        {
          const auto trace = ranges::vec(dw::trace(i,graph,views::pin(vertex_id{n - 1_n})));
          CHECK_MESSAGE(has_consistent_trace(graph,trace),trace);
        }
      }
    }
  }
}
  
} // hysj::graphs
#include <hysj/tools/epilogue.hpp>
