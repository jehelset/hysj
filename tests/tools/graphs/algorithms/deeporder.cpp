#include<functional>
#include<ranges>
#include<utility>
#include<vector>

#include"framework.hpp"
#include"tools/graphs.hpp"

#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/types.hpp>
#include<hysj/tools/graphs/directions.hpp>
#include<hysj/tools/graphs/elements.hpp>
#include<hysj/tools/graphs/incidence_graphs.hpp>
#include<hysj/tools/graphs/algorithms/deeporders.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::graphs::deeporders{

TEST_CASE("graphs.deeporder"){
  SUBCASE("flattening"){
    auto graph = make_graph(
      adjacency_list{
        {1,2},{2},{3},{}
      });
      
    std::vector<vertex_id> expected_ordering{
      {0},{1},{2},{3},{2},{3}
    };

    auto actual_ordering =
      ranges::vec(opreorder(std::cref(graph),vertex_id{0}));
    REQUIRE(expected_ordering == actual_ordering);
  }
}

} //hysj::graphs::deeporders
#include<hysj/tools/epilogue.hpp>
