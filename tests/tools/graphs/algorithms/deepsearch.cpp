#include<ranges>
#include<unordered_set>
#include<utility>
#include<vector>

#include"framework.hpp"
#include"tools/graphs.hpp"

#include<hysj/tools/types.hpp>
#include<hysj/tools/graphs/directions.hpp>
#include<hysj/tools/graphs/elements.hpp>
#include<hysj/tools/graphs/incidence_graphs.hpp>
#include<hysj/tools/graphs/indirected_graphs.hpp>
#include<hysj/tools/graphs/algorithms/deepsearch.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::graphs{

TEST_CASE("graphs.deepsearch"){

  auto tag_count = [](const auto &trace,auto pred){
    return std::ranges::count_if(trace,[=](auto e){ return pred(e.tag); });
  };

  auto has_consistent_trace =
    [&](const auto &graph,const auto &trace)->bool{

      const auto vertex_count = order(graph);
      if(vertex_count && trace.empty())
        return false;

      const auto root_count = tag_count(trace,any_of(dfs::root_tag)),
        palm_count = tag_count(trace,any_of(dfs::palm_tag)),
        snag_count = tag_count(trace,any_of(dfs::snag_tag));

      if(root_count != (palm_count + snag_count))
        return false;

      const auto branch_count = tag_count(trace,any_of(dfs::branch_tag)),
        bough_count = tag_count(trace,any_of(dfs::bough_tag));

      if(branch_count != bough_count)
        return false;

      const auto edge_event_count = 
        tag_count(trace,any_of(dfs::branch_tag, dfs::frond_tag,
                               dfs::vine_tag, dfs::liana_tag, dfs::weed_tag)),
        edge_count = size(graph);
      return edge_event_count == edge_count;
    };
    
  SUBCASE("line_graphs"){
    for(auto n:views::indices(1_n,10_n)){
      CAPTURE(n);
      const auto graph = make_line_graph(n);
      {
        const auto trace = ranges::vec(dfs::trace(o,graph,views::pin(0_v)));
        CHECK_MESSAGE(has_consistent_trace(graph,trace),trace);
      }
      {
        const auto trace = ranges::vec(dfs::trace(i,graph,views::pin(vertex_id{n - 1_n})));
        CHECK_MESSAGE(has_consistent_trace(graph,trace),trace);
      }
      {
        const auto trace = ranges::vec(dfs::trace(u,graph,views::pin(0_v)));
        CHECK_MESSAGE(has_consistent_trace(graph,trace),trace);
      }
    }
  }
  SUBCASE("crashes"){
    const auto graph = make_graph(9,
                   edge_list{{0, 1},{0, 2},{1, 2},
                             {1, 7},{2, 3},{2, 6},
                             {3, 4},{4, 2},{4, 5},
                             {6, 3},{6, 5},{7, 0},
                             {7, 6},{8, 0}});

    const auto trace = ranges::vec(dfs::trace(u,graph,views::pin(0_v)));
    CHECK_MESSAGE(has_consistent_trace(graph,trace),trace);
  }
  SUBCASE("liana"){
    auto graph = make_graph(2, edge_list{{1, 0}});
    const auto trace = ranges::vec(dfs::trace(o,std::ref(graph),views::pin(0_v,1_v)));
    CHECK_MESSAGE(has_consistent_trace(graph,trace),trace);
    CHECK(tag_count(trace,any_of(dfs::root_tag)) == 2);
    CHECK(tag_count(trace,any_of(dfs::liana_tag)) == 1);
  }
  SUBCASE("self_edge"){
    auto graph = make_graph(1, edge_list{{0, 0}});
    const auto trace = ranges::vec(dfs::trace(o,graph,views::pin(0_v)));
    CHECK_MESSAGE(has_consistent_trace(graph,trace),trace);
    CHECK(tag_count(trace,any_of(dfs::root_tag)) == 1);
    CHECK(tag_count(trace,any_of(dfs::frond_tag)) == 1);
  }
 SUBCASE("reduced_tarjan"){
    auto graph = make_graph(
      adjacency_list{{1}, {2}, {3, 6}, {4}, {2, 5}, {}, {3, 5}, {0, 6}});
    const auto trace = ranges::vec(dfs::trace(u,graph,graphs::vertex_ids(graph)));
    CHECK_MESSAGE(has_consistent_trace(graph,trace),trace);
    CHECK(tag_count(trace,any_of(dfs::palm_tag)) == 1);
  }
  SUBCASE("new_root"){

    SUBCASE("0"){
      auto graph = make_graph(5,edge_list{{0_n,1_n},{0_n,2_n},{1_n,3_n},{2_n,3_n},{4_n,1_n}});
      vertex_id new_root{4};

      auto state = dfs::make_state(o,std::cref(graph),{0_n});
      std::vector<vertex_id>
        expected_roots{{0},{4}},
        actual_roots{};

      auto it = expected_roots.begin();

      dsearch(
        co_none{},
        state,
        [&]<typename DFSGet>(DFSGet,auto event,auto dfs_ref) -> co::api_t<DFSGet> {
          auto &dfs_co = HYSJ_CO(DFSGet);
          auto &dfs = unwrap(dfs_ref);
          if constexpr(event == dfs::root){
            dfs.dw.root = *it++;
            co_final_await co::ret<1>(dfs_co);
          }
          else if constexpr(event == dfs::palm){
            actual_roots.push_back(dfs.dw.root);
            if(dfs.dw.root == new_root)
              co_final_await co::nil<0>(dfs_co);
            else
              co_final_await co::nil<1>(dfs_co);
          }
          else
            co_final_await co::nil<1>(dfs_co);
        })();
      CHECK(std::ranges::equal(expected_roots,actual_roots));
    }
    SUBCASE("1"){
      auto graph = make_graph(3,edge_list{{0_n,1_n},{0_n,2_n}});
      auto state = dfs::make_state(o,std::ref(graph),{0_n});
      vertex_id old_root{0};
      std::optional<vertex_id> new_root;
      std::optional<edge_id> new_edge;
      std::vector<vertex_id>
        expected_roots{old_root,{3}},
        actual_roots{};

      dsearch(
        co_none{},
        state,
        [&]<typename DFSGet>(DFSGet,auto dfs_event,auto dfs_) -> co::api_t<DFSGet> {
          auto &dfs_co = HYSJ_CO(DFSGet);
          auto &dfs = unwrap(dfs_);
          if constexpr(dfs_event == dfs::root)
            co_final_await co::nil<1>(dfs_co);
          else if constexpr(dfs_event == dfs::palm){
            actual_roots.push_back(dfs.dw.root);
            if(dfs.dw.root == old_root){
              auto &graph = unwrap(dfs.dw.graph);
              new_root = graph.vertex();
              new_edge = graph.edge(*new_root,old_root);
              dfs.resize();
              dfs.dw.root = *new_root;
              co_final_await co::nil<1>(dfs_co);
            }
            else
              co_final_await co::nil<0>(dfs_co);
          }else
            co_final_await co::nil<1>(dfs_co);
        })();

      REQUIRE(new_root.has_value());
      REQUIRE(new_edge.has_value());
      CHECK(std::ranges::equal(expected_roots,actual_roots));
    }      
  }
  SUBCASE("new_target"){

    SUBCASE("0"){
      auto graph = make_graph(3,edge_list{{0_n,1_n}});
      
      vertex_id new_target{2};
      dfs::concepts::state auto state = dfs::make_state(o,indirected_graph{std::ref(graph)},{0_n});
      std::vector<vertex_id>
        expected_sprouts{{1},{2}},
        actual_sprouts{};
      
      dsearch(
        co_none{},
        state,
        [&]<typename DFSGet>(DFSGet,auto dfs_event,auto dfs_) -> co::api_t<DFSGet>{
          auto &dfs_co = HYSJ_CO(DFSGet);
          auto &dfs = unwrap(dfs_);
          if constexpr(dfs_event == dfs::bough){
            if(dfs.dw.head() != new_target){
              unwrap(dfs.dw.graph.base).oreset(dfs.dw.edge(),new_target);
              co_final_await co::ret<1>(dfs_co,dfs::branch);
            }else
              co_final_await co::nil<1>(dfs_co);
          }
          else if constexpr(dfs_event == dfs::branch){
            actual_sprouts.push_back(dfs.dw.head());
            co_final_await co::nil<1>(dfs_co);
          }
          else if constexpr(dfs_event == dfs::palm)
            co_final_await co::nil<0>(dfs_co);
          else
            co_final_await co::nil<1>(dfs_co);
        })();

      CHECK_MESSAGE(std::ranges::equal(expected_sprouts,actual_sprouts),
                    fmt::format("{} != {}",expected_sprouts,actual_sprouts));
    }
  }
}
  
} //hysj::graphs
#include <hysj/tools/epilogue.hpp>
