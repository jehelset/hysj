#include<ranges>
#include<unordered_set>
#include<utility>
#include<vector>

#include<hysj/tools/graphs/degree.hpp>
#include<hysj/tools/graphs/adjacency.hpp>
#include<hysj/tools/graphs/incidence.hpp>
#include<hysj/tools/graphs/incidence_graphs.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/types.hpp>
#include<hysj/tools/functional/overload.hpp>
#include<hysj/tools/functional/always.hpp>

#include"framework.hpp"
#include"tools/graphs.hpp"

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::graphs{

TEST_CASE("graphs.incidence_graphs"){
  SUBCASE("make_graph"){
    auto bg = incidence_graph<>::make(3,4);
    CHECK(order(bg) == 3);
    CHECK(size(bg) == 4);
  }
  SUBCASE("vertices"){
    incidence_graph<int,none> graph{};
    CHECK(graph.vertex() == vertex_id{0});
    CHECK(graph.vertex(3) == vertex_id{1});
    CHECK(graph[vertex_id{1}] == 3);
    graph[vertex_id{1}] = 4;
    CHECK(graph[vertex_id{1}] == 4);
    CHECK(graphs::order(graph) == 2);
    for(auto v:vertex_ids(graph))
      CHECK(odegree(graph,v) == 0);
  }
  SUBCASE("edges.bidirectional"){
    incidence_graph<none,int> graph{};
    auto vertices = ranges::vec(views::indices(4),[&](auto){ return graph.vertex(); });
    std::vector edges{
      graph.edge(vertices[0],vertices[1],0),
      graph.edge(vertices[0],vertices[2],1),
      graph.edge(vertices[1],vertices[2],2),
      graph.edge(vertices[2],vertices[1],3),
      graph.try_edge(vertices[0],vertices[2],always(4)),
      graph.try_edge(vertices[0],vertices[3],always(5))
    };
    CHECK(edges[1] == edges[4]);
    CHECK(graph[edges[1]] == 1);
    graph[edges[1]] = -2;
    CHECK(graph[edges[1]] == -2);
    CHECK(order(graph) == 4);
    CHECK(size(graph) == 5);
      
    SUBCASE("incidence"){

      CHECK(ranges::vec(graphs::vertex_ids(graph)).size() == 4);
      CHECK(equal_skin(i,graph,{{0}}));
      CHECK(equal_skin(o,graph,{{3}}));
        
      auto [utail,uhead] = graphs::ports(graph,edge_id{0});
      CHECK(utail == vertices[0]);
      CHECK(uhead == vertices[1]);

      auto [dotail,dohead] =
        graphs::ports(graph,dynamic_edge_id{0,out_tag});

      CHECK(dotail == vertices[0]);
      CHECK(dohead == vertices[1]);

      auto [ditail,dihead] =
        graphs::ports(graph,dynamic_edge_id{0,in_tag});
        
      CHECK(ditail == vertices[1]);
      CHECK(dihead == vertices[0]);

      auto [otail,
            ohead] =
        graphs::ports(graph,out_edge_id{0});

      CHECK(otail == vertices[0]);
      CHECK(ohead == vertices[1]);

      auto [itail,ihead] =
        graphs::ports(graph,in_edge_id{0});
        
      CHECK(itail == vertices[1]);
      CHECK(ihead == vertices[0]);

    }
    SUBCASE("adjacency"){

      CHECK(std::ranges::equal(graphs::viadjacent(graph,vertices[0]),
                               std::views::empty<vertex_id>));
      CHECK(std::ranges::equal(graphs::voadjacent(graph,vertices[0]),
                               std::vector{vertices[1],vertices[2],vertices[3]}));
      CHECK(std::ranges::equal(graphs::voadjacent(graph,vertices[0]),
                               graphs::vuadjacent(graph,vertices[0])));
      CHECK(std::ranges::equal(graphs::viadjacent(graph,vertices[2]),
                               std::vector{vertices[0],vertices[1]}));
      CHECK(std::ranges::equal(graphs::voadjacent(graph,vertices[2]),
                               views::one(vertices[1])));
      CHECK(std::ranges::equal(graphs::viadjacent(graph,vertices[3]),
                               views::one(vertices[0])));
      CHECK(std::ranges::equal(graphs::voadjacent(graph,vertices[3]),
                               std::views::empty<vertex_id>));

      CHECK(sorted_equal(
        graphs::euadjacent(graph,edges[0]),
        std::vector{
          dynamic_edge_id{edges[1],out_tag},
          dynamic_edge_id{edges[2],out_tag},
          dynamic_edge_id{edges[3],in_tag},
          dynamic_edge_id{edges[5],out_tag}
        }));
        
      CHECK(sorted_equal(
        graphs::eoadjacent(graph,edges[0]),
        std::vector{
          out_edge_id(edges[2])
        }));
      CHECK(sorted_equal(
        graphs::eiadjacent(graph,edges[0]),
        std::vector<in_edge_id>{}));
        
      std::vector A{
        ranges::vec(graphs::viadjacent(graph,vertices[2])),
        ranges::vec(graphs::voadjacent(graph,vertices[2]))
      };
      CHECK(std::ranges::equal(
        views::join(A),
        graphs::vuadjacent(graph,vertices[2])));
    }
  }
}

}
#include <hysj/tools/epilogue.hpp>
