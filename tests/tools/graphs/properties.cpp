#include"../../framework.hpp"

#include<hysj/tools/graphs/elements.hpp>
#include<hysj/tools/graphs/properties.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::graphs{

TEST_CASE("graphs.properties"){
  SUBCASE("constant_property"){
    constexpr auto v = 9,
      u = 11;
    constant_property c{v};
    REQUIRE(graphs::get(c,vertex_id{0}) == v);
    graphs::put(c,vertex_id{0},u);
    REQUIRE(graphs::get(c,vertex_id{0}) == v);
    REQUIRE(graphs::get(c,edge_id{1}) == v);
    REQUIRE(graphs::get(c,dynamic_edge_id{2,in_tag}) == v);
    REQUIRE(graphs::get(c,out_edge_id{3}) == v);
  }
  SUBCASE("static_property"){
    static constexpr auto v = 8,
      u = 11;
    static_property<v> c{};
    REQUIRE(graphs::get(c,vertex_id{0}) == v);
    graphs::put(c,vertex_id{0},u);
    REQUIRE(graphs::get(c,vertex_id{0}) == v);
    REQUIRE(graphs::get(c,edge_id{1}) == v);
    REQUIRE(graphs::get(c,dynamic_edge_id{2,in_tag}) == v);
    REQUIRE(graphs::get(c,out_edge_id{3}) == v);
  }
}

}
#include <hysj/tools/epilogue.hpp>
