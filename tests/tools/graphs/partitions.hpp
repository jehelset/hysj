#include<ranges>
#include<unordered_set>
#include<utility>
#include<vector>

#include"framework.hpp"
#include"tools/graphs.hpp"

#include<hysj/tools/types.hpp>
#include<hysj/tools/graphs/directions.hpp>
#include<hysj/tools/graphs/elements.hpp>
#include<hysj/tools/graphs/incidence_graphs.hpp>
#include<hysj/tools/graphs/indirected_graphs.hpp>
#include<hysj/tools/graphs/partitions.hpp>
#include<hysj/tools/graphs/algorithms/hopcroft1971.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::graphs{

auto require_trace_consistency =
  [&](const auto &t){
    if(t.size() < 2)
      return false;
    if(t.front().second.tag != prf::start_tag())
      return false;
    if(t.back().second.tag == prf::stop_tag())
      return false;
    for(auto i:views::indices(t.size() - 2))
      REQUIRE(t[i + 1].second.tag == prf::refine_tag());
  };

auto require_partitions =
  [&](const auto &t,const auto P_expected_){
    const auto &s = t.back().first;
    auto P_actual = ranges::vec(
      prf::partition_ids(s.partitioning),
      bind<>(prf::members,std::ref(s.partitioning)));
    auto P_expected = ranges::vec(P_expected_);
    std::ranges::sort(P_actual);
    std::ranges::for_each(P_actual,std::ranges::sort);
    std::ranges::sort(P_expected);
    std::ranges::for_each(P_expected,std::ranges::sort);
    REQUIRE_MESSAGE(std::ranges::equal(P_actual,P_expected),
                    fmt::format("{} != {}",
                                fmt::join(P_actual,","),
                                fmt::join(P_expected,",")));
  };


} //hysj::graphs
#include <hysj/tools/epilogue.hpp>
