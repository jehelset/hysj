#include"../../framework.hpp"

#include<hysj/tools/graphs/directions.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::graphs{

TEST_CASE("graphs.direction"){
  REQUIRE(other(in_tag) == out_tag);
  REQUIRE(other(out_tag) == in_tag);
  REQUIRE(other(in_tag()) == out_tag);
  REQUIRE(other(out_tag()) == in_tag);
}

}
#include <hysj/tools/epilogue.hpp>
