#include<ranges>
#include<unordered_set>
#include<utility>
#include<vector>

#include"../../framework.hpp"

#include<hysj/tools/types.hpp>
#include<hysj/tools/graphs/directions.hpp>
#include<hysj/tools/graphs/elements.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::graphs{

TEST_CASE("graphs.elements"){
  vertex_id v0{0},
    v1{1};
  REQUIRE(v0 < v1);
  REQUIRE(fmt::format("{}",v0) == "v0");
  std::unordered_set<vertex_id> v{v0};
  REQUIRE(v.count(v0) == 1);
  REQUIRE(v.count(v1) == 0);
  edge_id e0{0},e1{1};
  REQUIRE(e0 < e1);
  REQUIRE(fmt::format("{}",e0) == "e0");
  std::unordered_set<edge_id> e{e0};
  REQUIRE(e.count(e0) == 1);
  REQUIRE(e.count(e1) == 0);
  dynamic_edge_id
    dde00{e0,in_tag()},
    dde01{e0,out_tag()},
    dde10{e1,in_tag()},
    dde11{e1,out_tag()};
  std::unordered_set<dynamic_edge_id> dde{dde00};
  REQUIRE(dde.count(dde00) == 1);
  REQUIRE(dde.count(dde01) == 0);
  REQUIRE(undirect(dde00) == e0);
  REQUIRE(dde00 < dde10);
  REQUIRE(dde00 < dde01);
  REQUIRE(dde01 < dde10);
  REQUIRE(dde10 < dde11);
  REQUIRE(fmt::format("{}",dde00) == "ie0");
  REQUIRE(fmt::format("{}",dde11) == "oe1");
  struct dummy_graph{} graph{};
  REQUIRE(direction(graph,dde00) == in_tag());
  out_edge_id
    oe0{e0},
    oe1{e1};
  REQUIRE(fmt::format("{}",oe0) == "oe0");
  REQUIRE(undirect(oe0) == e0);
  REQUIRE(oe0 < oe1);
  REQUIRE(direction(graph,oe0) == out_tag());
  std::unordered_set<out_edge_id> oe{oe0};
  REQUIRE(oe.count(oe0) == 1);
  REQUIRE(oe.count(oe1) == 0);
  in_edge_id
    ie0{e0},
    ie1{e1};
  natural e0v = ie0;
  REQUIRE(undirect(ie1) == 1);
  REQUIRE(e0v == 0);
  REQUIRE(fmt::format("{}",ie0) == "ie0");
  REQUIRE(ie0 < ie1);
  REQUIRE(direction(graph,ie0) == in_tag());
  std::unordered_set<in_edge_id> ie{ie0};
  REQUIRE(ie.count(ie0) == 1);
  REQUIRE(ie.count(ie1) == 0);
}

}
#include <hysj/tools/epilogue.hpp>
