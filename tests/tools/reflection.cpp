#include<array>
#include<ranges>
#include<span>
#include<string>
#include<string_view>
#include<tuple>
#include<type_traits>

#include"../framework.hpp"

#include<kumi/tuple.hpp>
#include<kumi/algorithm/cat.hpp>

#include<hysj/tools/graphs/directions.hpp>
#include<hysj/tools/reflection.hpp>
#include<hysj/tools/types.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

enum class X{a,b,c};
HYSJ_MIRROR_ENUM(X,(a)(b)(c));

struct reflected_0{
  std::array<int,enumerator_count<X>> ok;
  X value_of_b = std::get<1>(enumerator_values<X>);
  const char *name_of_b = std::get<1>(enumerator_names<X>).data();
  X another_value_of_b = this->*kumi::get<0>(members(reflect(*this))).pointer;

  friend HYSJ_MIRROR_STRUCT(reflected_0,
                            (value_of_b,
                             name_of_b,
                             another_value_of_b));
};
struct reflected_1{
  int x,y;
  friend HYSJ_MIRROR_STRUCT(reflected_1,(x,y))
};
struct reflected_2:reflected_1{
  int z;

  friend HYSJ_MIRROR_STRUCT(reflected_2,(z)(reflected_1))
};
struct reflected_3:reflected_2{
  friend HYSJ_MIRROR_STRUCT(reflected_3,()(reflected_2))
};

}

HYSJ_ENUM_FORMATTER(hysj::_HYSJ_VERSION_NAMESPACE::X);

template<typename C>
HYSJ_STRUCT_FORMATTER((hysj::_HYSJ_VERSION_NAMESPACE::reflected_1), C);

namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

TEST_CASE("reflected.struct_and_nested_enum"){
  constexpr reflected_0 d{{1,2,3}};

  static_assert(enumerator_count<X> == 3);
  static_assert(d.value_of_b == X::b);
  static_assert(std::string_view{d.name_of_b} == std::string_view{"b"});
  static_assert(d.another_value_of_b == X::b);
  constexpr auto struct_mirror = reflect<reflected_0>();
  constexpr auto member_0 = (d.*(member_at(struct_mirror,0_N).pointer));
  CHECK(member_0 == d.value_of_b);
  CHECK(member_count(struct_mirror) == 3);
  static_assert(std::is_same_v<decltype(member_indices(struct_mirror)),std::index_sequence<0,1,2>>);
  constexpr auto reflected_1_mirror = reflect<reflected_1>();
  constexpr auto reflected_2_mirror = reflect<reflected_2>();
  constexpr auto reflected_3_mirror = reflect<reflected_3>();
  static_assert(base_count(reflected_1_mirror) == 0);
  static_assert(base_count(reflected_2_mirror) == 1);
  static_assert(base_count(reflected_3_mirror) == 1);
  static_assert(std::is_same_v<reflected_1,reify(base_at(reflected_2_mirror,0_N))>);
  static_assert(std::is_same_v<reflected_2,reify(base_at(reflected_3_mirror,0_N))>);
}

TEST_CASE("reflected.graphs.direction"){
  CHECK(enumerator_count<graphs::direction_tag> == 2);
  std::string in = "in";
  bool same_constants = enumerator_values<graphs::direction_tag> == std::array{graphs::in_tag(),graphs::out_tag()};
  CHECK(same_constants);
  CHECK(std::ranges::equal(std::get<0>(enumerator_names<graphs::direction_tag>),in));
}

TEST_CASE("reflected.formatters"){
  CHECK_NOTHROW((void)fmt::format("{}", reflected_1{}));
  CHECK_NOTHROW((void)fmt::format("{}", X::a));
}

}
#include<hysj/tools/epilogue.hpp>
