#include<array>
#include<map>
#include<ranges>
#include<span>
#include<string>
#include<type_traits>
#include<utility>
#include<list>
#include<vector>

#include"../framework.hpp"

#include<hysj/tools/ranges/cat.hpp>
#include<hysj/tools/ranges/fold.hpp>
#include<hysj/tools/ranges/generate.hpp>
#include<hysj/tools/ranges/join.hpp>
#include<hysj/tools/ranges/keep.hpp>
#include<hysj/tools/ranges/linear_span.hpp>
#include<hysj/tools/ranges/map.hpp>
#include<hysj/tools/ranges/pins.hpp>
#include<hysj/tools/ranges/span.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/functional/always.hpp>
#include<hysj/tools/functional/compose.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

template <typename... R>
concept catable = requires(R&&... r) {
  views::cat((R &&) r...);
};

template <typename R>
R f(int);

template <typename R>
using make_view_of = decltype(std::views::iota(0) | std::views::transform(f<R>));

struct throwing_iter {
  int* i = nullptr;
  int& operator*() const { return *i; }

  throwing_iter& operator++() { return *this; }
  throwing_iter operator++(int) { return {}; }

  bool operator==(throwing_iter) const { return true; }

  using value_type = int;
  using difference_type = std::ptrdiff_t;
  friend int&& iter_move(const throwing_iter& x){ return std::move(*(x.i)); }
};

struct throwing_range {
  throwing_iter begin() const { return {}; }
  throwing_iter end() const { return {}; }
};

struct throw_on_move {
  throw_on_move() = default;
  throw_on_move(throw_on_move const&) noexcept {}
  throw_on_move(throw_on_move&&){}
};

TEST_CASE("ranges.box"){
  struct irregular{
    int v;
    explicit
    irregular(int v):v{v}{}
    
    irregular() = delete; 
    irregular(irregular &&) = default;
    irregular(const irregular &) = default;
    
    irregular &operator=(const irregular &)=delete;
    irregular &operator=(irregular &&)=delete;
  };
  struct semiregular{
    int v;
  };
  {
    ranges::box<irregular> b0{irregular(3)},b1{},b2{};
    REQUIRE(b0.has_value());
    CHECK(b0.value().v == 3);
    CHECK(!b1.has_value());
    b1 = b0;
    REQUIRE(b1.has_value());
    CHECK(b1.value().v == 3);
    b1 = b2;
    CHECK(!b1.has_value());
  }
  {
    ranges::box<semiregular> b0{3},b1{};
    REQUIRE(b0.has_value());
    CHECK(b0->v == 3);
    CHECK(b1.has_value());
    b1 = b0;
    REQUIRE(b1.has_value());
    CHECK(b1->v == 3);
    {
      semiregular & v0 = *b0;
      CHECK(&v0 == &b0.value);
    }
    {
      const auto &cb0 = b0;
      const semiregular & cv0 = *cb0;
      CHECK(cb0->v == 3);
      CHECK(&cv0 == &b0.value);
    }
  }
}

TEST_CASE("ranges.to_map"){
  using V = std::pair<int,char>;
  auto
    X0 = ranges::to_map(std::vector{V{0,'a'},V{1,'b'}}),
    X1 = std::map{V{0,'a'},V{1,'b'}};
  CHECK(X0 == X1);
  auto
    Y0 = ranges::to_map(views::indices(2),[](auto i){ return V{i,'a'}; }),
    Y1 = std::map{V{0,'a'},V{1,'a'}};
  CHECK(Y0 == Y1);
}

TEST_CASE("ranges.first_and_last"){
  {
    std::vector X{0,1,2,56,76};
    CHECK(ranges::first(X) == 0);
    CHECK(ranges::last(X) == 76);
  }
  {
    std::list X{6};
    CHECK(ranges::first(X) == 6);
    CHECK(ranges::last(X) == 6);
  }
}

namespace views{

  TEST_CASE("views.one"){
    int x = 2;
    auto one_span = one(x);
    static_assert(std::is_same_v<decltype(one_span),std::span<int,1>>);
    auto one_single = one(int{});
    static_assert(std::is_same_v<decltype(one_single),
                  std::ranges::single_view<int>>);
    const int y = 4;
    auto one_const_span = one(y);
    static_assert(std::is_same_v<decltype(one_const_span),
                  std::span<const int,1>>);
  }

  TEST_CASE("views.maps"){
    std::array a{0,1,3};
    CHECK(std::ranges::equal(a,map(a,std::identity{})));
    {
      const auto b = map(a,std::identity{});
      CHECK(std::ranges::equal(a,b));
    }
    
    CHECK(a[1] == map(a,std::identity{})[1]);
    {
      int b = 42;
      const auto z = [b = std::make_shared<int>(b)](int a){ return a + *b; };
      auto w = [z = z](auto x) arrow((z(x)));
      static_assert(std::is_same_v<int,decltype(invoke(z,std::declval<int &>()))>);
      auto y =
        map(
          a,
          [b = std::make_shared<int>(b)](int a){
            return a + *b;
          });
      {
        auto y_it = std::ranges::begin(y);
      }
      static_assert(std::ranges::borrowed_range<decltype(y)>);
      auto [it,end] =
        [](auto R){
          return std::make_pair(std::ranges::begin(R),std::ranges::end(R));
        }(map(
            a,
            [b = std::make_shared<int>(b)](int a){
              return *b + a;
            }));
      CHECK(std::ranges::equal(views::map(a,std::bind_front(std::plus{},b)),
                               std::ranges::subrange(it,end)));
    }
    {
      std::vector b{0,0,0},
        c{2,1,0};
      std::ranges::copy(a,std::ranges::begin(map(c,[&](auto i)->auto &{ return b[i]; })));
      std::ranges::reverse(b);
      CHECK(std::ranges::equal(a,b));
    }
    {
      std::vector b{1,2,3};
      auto c = map(b,[](auto i){ return 2*i; });
      auto it = std::ranges::begin(c);

      CHECK(*(it + 2) == 6);
      CHECK(*(1 + it) == 4);

      auto it2 = std::ranges::next(it,2);
      CHECK(*(it2 - 2) == 2);
      CHECK(it2 - it == 2);
      
    }
    {
      std::vector x{0,1,2};
      auto n = std::ranges::distance(map(indices(std::ranges::size(x)),lift((auto))));
      CHECK(n == 3);
    }

    {
      struct X{ int y; };
      std::vector<X> xs(100,X{.y = 4});
      auto ys = map(xs,&X::y);
      for(auto y:std::views::reverse(ys))
        CHECK(y == 4);
    }

  }
  
  TEST_CASE("views.keeps"){
    {
      auto two = indices(2);
      auto none = keep(two,always(false));
      static_assert(
        std::is_same_v<
        std::ranges::range_reference_t<decltype(none)>,
        int>);
      CHECK(ranges::vec(none).empty());
    }
    {
      std::vector X{0,1,2,3};

      auto Y = keep(X,[](auto i){ return i % 2; });
      auto it = std::ranges::begin(Y);
      CHECK(*it == 1);
      ++it;
      CHECK(*it == 3);
      --it;
      CHECK(*it == 1);
      ++it;
      ++it;
      CHECK(it == std::ranges::end(Y));
    }
    {
      int a = 3;
      std::vector<int *> X{&a, nullptr, &a};
      CHECK(std::ranges::distance(views::keep(X)) == 2);
    }
  }
  
  TEST_CASE("views.joins"){
    {
      std::array inner{indices(2),indices(2)};
      auto view = join(inner);
      static_assert(
        std::ranges::borrowed_range<decltype(view)>);
      std::array expected_inner{0,1,0,1};
      CHECK(std::ranges::equal(expected_inner,view));
      CHECK(std::ranges::equal(expected_inner,std::as_const(view)));
      {
        auto it = std::ranges::begin(view);
        CHECK(*it == 0);
        ++it;
        ++it;
        CHECK(*it == 0);
        --it;
        CHECK(*it == 1);
        ++it;
        ++it;
        ++it;
        --it;
        CHECK(*it == 1);
      }
    }
    {
      auto X = join(
        map(indices(3),
                   indices));
      static_assert(
        std::ranges::borrowed_range<decltype(X)>);
      std::vector Y{0,0,1};

      auto Z0 = ranges::vec(X),
        Z1 = ranges::vec(Y);
      CHECK_MESSAGE(std::ranges::equal(Z0,Z1),fmt::format("{} != {}",Z0,Z1));
    }
  }

  
  TEST_CASE("views.linear_span"){
    std::vector<int>
      a{0,1,2,3,4,5},
      b{};
    int c[] = {0,2,3};
    int d[2] = {0,1};
    constexpr std::array e{0,1,3};
    SUBCASE("aliasing"){
      auto lspan = linear_span;
      CHECK(std::ranges::equal(a,lspan(a)));
      CHECK(std::ranges::equal(b,lspan(b)));
      CHECK(std::ranges::equal(c,lspan(c)));
      CHECK(std::ranges::equal(d,lspan(d)));
      CHECK(std::ranges::equal(e,lspan(e)));

      static_assert(lspan(c).extent == 3);
      static_assert(lspan(d).extent == 2);
      static_assert(lspan(e).extent == 3);
      static_assert(lspan(e,static_ptrdiff<5>{}).extent == 1);
      static_assert(lspan(e,static_ptrdiff<1>{},static_ptrdiff<2>{}).extent == 2);
    }
    SUBCASE("reversed"){
      auto rlspan =
        compose<>(lift((reverse)),linear_span);
      static_assert(rlspan(e).extent == 3);
      CHECK(rlspan(e,dynamic_ptrdiff{1},static_ptrdiff<2>{}).size() == 2);
      CHECK(std::ranges::equal(std::views::reverse(a),rlspan(a)));
      CHECK(std::ranges::equal(std::views::reverse(b),rlspan(b)));
      CHECK(std::ranges::equal(std::views::reverse(c),rlspan(c)));
      CHECK(std::ranges::equal(std::views::reverse(d),rlspan(d)));
      CHECK(std::ranges::equal(std::views::reverse(e),rlspan(e)));
      CHECK(std::ranges::equal(std::views::reverse(std::views::take(e,2)),
                               rlspan(e,dynamic_ptrdiff{1},static_ptrdiff<2>{})));
    }
  }

  TEST_CASE("views.cats"){
    
    SUBCASE("0"){
      std::vector<int> v1{1, 2, 3}, v2{4, 5}, v3{};
      std::array a{6, 7, 8};

      auto s = std::views::single(9);
      auto cv = views::cat(v1, v2, v3, a, s);
      auto it = std::ranges::begin(cv);
      auto end =  std::ranges::end(cv);
      CHECK(it != end);
      std::vector expected{1, 2, 3, 4, 5, 6, 7, 8, 9};
      for(auto i = 0; i != 9; ++i)
        CHECK(cv[i] == expected[i]);

      CHECK((std::same_as<std::ranges::range_reference_t<decltype(cv)>, int&>));
    }

    struct Foo {};

    struct Bar : Foo {};
    struct Qux : Foo {};

    struct MoveOnly {
      MoveOnly(MoveOnly const&) = delete;
      MoveOnly& operator=(MoveOnly const&) = delete;
      MoveOnly(MoveOnly&&) = default;
      MoveOnly& operator=(MoveOnly&&) = default;
    };

    static_assert(std::movable<MoveOnly>);
    static_assert(!std::copyable<MoveOnly>);

    struct BigCopyable {
      int bigdata[1024];
    };

    SUBCASE("1"){
      using V = std::vector<int>;
      V v1{1, 2, 3}, v2{4, 5};
      auto cv = views::cat(v1,v2);
      static_assert(std::ranges::range<decltype(cv)>);
      CHECK(std::ranges::size(cv) == 5);
    }

    SUBCASE("2"){

      using IntV = std::vector<int>;
      using IntL = std::list<int>;
      using FooV = std::vector<Foo>;
      using BarV = std::vector<Bar>;
      using QuxV = std::vector<Qux>;

      CHECK(catable<IntV&>);
      CHECK(catable<IntV&>);
      CHECK(catable<IntV&, IntV&>);
      CHECK(catable<IntV&, IntV const&>);
      CHECK(catable<IntV&, std::vector<std::reference_wrapper<int>>&>);
      CHECK(catable<IntV&, IntL&, IntV&>);
      CHECK(catable<FooV&, BarV&>);
      CHECK(catable<BarV&, FooV&>);
      CHECK(catable<FooV&, BarV&, QuxV const&>);
      CHECK(catable<IntV&, make_view_of<int&>>);
      CHECK(catable<make_view_of<int&>, make_view_of<int&&>, make_view_of<int>>);
      CHECK(catable<make_view_of<MoveOnly>, make_view_of<MoveOnly>&&>);
      CHECK(!catable<IntV&, FooV&>);
      CHECK(!catable<make_view_of<MoveOnly>, make_view_of<MoveOnly&>>);
      CHECK(!catable<BarV&, QuxV&, FooV&>);
      CHECK(catable<make_view_of<BigCopyable>, make_view_of<BigCopyable&>>);
    }

    SUBCASE("3"){
      std::vector v1{10, 11};
      std::array a1{-10, -11};
      auto c = views::cat(v1, a1);
      auto it = c.begin();
      static_assert(noexcept(std::ranges::iter_move(it)));
    }

    SUBCASE("4"){
      throwing_range a, b;
      auto c = views::cat(a, b);
      auto it = c.begin();
      static_assert(!noexcept(std::ranges::iter_move(it)));
    }

    SUBCASE("5"){
      std::vector<throw_on_move> v;
      auto tv =
        std::views::iota(0) | std::views::transform([](auto){ return throw_on_move{}; });
    
      auto cv = views::cat(v,tv);

      using vector_rvalue_ref = std::ranges::range_rvalue_reference_t<decltype(v)>;
      using common_rvalue_ref = std::ranges::range_rvalue_reference_t<decltype(cv)>;

      auto it = cv.begin();
      static_assert(!noexcept(std::ranges::iter_move(it)));
      static_assert(std::same_as<vector_rvalue_ref, throw_on_move&&>);
      static_assert(std::same_as<common_rvalue_ref, throw_on_move>);
      static_assert(!std::is_nothrow_convertible_v<throw_on_move&&, throw_on_move>);
    }

    SUBCASE("6"){
      std::vector v{1, 2, 3};
      auto tv = v | std::views::transform([](auto){ return 5; });
      auto cv = views::cat(tv, tv);
      using Cv = decltype(cv);
      using It = std::ranges::iterator_t<decltype(cv)>;
      static_assert(!std::indirectly_swappable<It>);
      static_assert(!std::ranges::borrowed_range<Cv>);
      CHECK(std::ranges::equal(cv,std::vector{5,5,5,5,5,5}));

      auto cv2 = views::cat(v, v);
      using Cv2 = decltype(cv2);
      using It2 = std::ranges::iterator_t<decltype(cv2)>;
      static_assert(std::ranges::borrowed_range<Cv2>);
      static_assert(std::indirectly_swappable<It2>);
      CHECK(std::ranges::equal(cv2,std::vector{1,2,3,1,2,3}));
    }

    SUBCASE("7"){
      std::vector 
        x{2},
        u{1,2},
        v{2,3},
        w{1,2,3};
      CHECK(std::ranges::equal(u,views::prepend(1,x)));
      CHECK(std::ranges::equal(v,views::append(x,3)));
      CHECK(std::ranges::equal(w,views::bookend(1,x,3)));
    }

    SUBCASE("8"){
      std::vector 
        a{2,3},
        b{1,2,3};
      std::array c{5};
      auto d0 = views::cat(a,b);
      auto d1 = views::cat(a,b,b);
      auto d2 = views::cat(a,c);
      auto d3 = views::cat(d0,b);
      CHECK(is_std_array<decltype(d0.bases)>);
      CHECK(is_std_array<decltype(d1.bases)>);
      CHECK(!is_std_array<decltype(d2.bases)>);
      CHECK(is_std_array<decltype(d3.bases)>);
      CHECK(std::ranges::equal(d1,d3));
    }
    SUBCASE("9"){
      std::vector<int>
        A0{0,1,2},
        A1{},
        A2{3,4,5};
      auto A = cat(A0,A1,A2);
      CHECK(A.size() == A0.size() + A1.size() + A2.size());
      CHECK(std::ranges::equal(A,indices(6)));
      CHECK(std::ranges::equal(A,A));
      auto
        it0 = std::ranges::begin(A),
        it1 = ++std::ranges::begin(A);

      CHECK(it0 == it0);
      CHECK(it0 != it1);
      CHECK(it0 < it1);
      CHECK(A[0] == A0[0]);
      CHECK(A[3] == A2[0]);
      --it1;
      CHECK(it0 == it1);
      std::advance(it1,3);
      CHECK(*it1 == 3);
      --it1;
      CHECK(*it1 == 2);
      std::advance(it1,3);
      CHECK(*it1 == 5);
      it1++;
      CHECK(it1 == std::ranges::end(A));
    }
    SUBCASE("10"){
      std::vector
        a{0,1,2},
        b{3,4},
        c{5};
      CHECK(std::ranges::equal(cat(a,b,c),std::views::iota(0,6)));
    }
    SUBCASE("11"){
      std::vector<int> a{0,4,2}, b{-1, 10};
      std::ranges::sort(cat(a,b));
      CHECK(std::ranges::equal(a, std::vector{-1,0,2}));
      CHECK(std::ranges::equal(b, std::vector{4,10}));
    }
  }
}

}
#include<hysj/tools/epilogue.hpp>
