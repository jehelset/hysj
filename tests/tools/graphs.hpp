#pragma once
#include<array>
#include<algorithm>
#include<vector>
#include<tuple>
#include<utility>

#include<fmt/format.h>
#include<fmt/ranges.h>
#include<kumi/tuple.hpp>

#include<hysj/tools/graphs/incidence.hpp>
#include<hysj/tools/graphs/degree.hpp>
#include<hysj/tools/graphs/partitions.hpp>
#include<hysj/tools/graphs/incidence_graphs.hpp>
#include<hysj/tools/ranges/utility.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::graphs{

using adjacency_list = std::vector<std::vector<natural>>;
using edge_list = std::vector<kumi::tuple<natural,natural>>;

inline constexpr auto make_graph = overload(
  [](adjacency_list input){
    auto output = incidence_graph<>::make(input.size());
    for(auto v_in:views::indices(static_cast<natural>(graphs::order(output))))
      for(auto v_out:input[v_in])
        output.edge({v_in},{v_out});
    return output;
  },
  [](natural order,edge_list input){
    auto output = incidence_graph<>::make(order);
    for(auto [v_in,v_out]:input)
      output.edge({v_in},{v_out});
    return output;
  });

inline constexpr auto make_line_graph =
  [](auto n){
    if (n == 0_n)
      return make_graph(0,edge_list{});
    return make_graph(
      n,ranges::vec(views::indices(n - 1),
                          [&](auto i){
                            return kumi::make_tuple(i, i + 1);
                          }));
  };

inline constexpr auto sorted_equal =
  [](auto range_0,auto range_1){
    auto vector_0 = ranges::vec(range_0);
    auto vector_1 = ranges::vec(range_1);
    std::ranges::sort(vector_0);
    std::ranges::sort(vector_1);
    return std::ranges::equal(vector_0,vector_1);
  };

inline constexpr auto u = undirected_tag;
inline constexpr auto o = out_tag;
inline constexpr auto i = in_tag;

inline constexpr auto equal_skin =
  [](auto direction,const auto &graph,std::vector<vertex_id> expected_skin){

    auto actual_skin =
      ranges::vec(graphs::skin(direction,graph));

    std::ranges::sort(actual_skin);
    return std::ranges::equal(expected_skin,actual_skin);
  };

inline constexpr auto has_base_consistency =
  [](const auto &graph){
    for(auto e:graphs::edge_ids(graph))
      if(!std::ranges::equal(graphs::euincident(graph,e),
                             graphs::euincident(graph.base(),e)))
        return false;
    return true;
  };


inline constexpr auto has_ports =
  [](const auto &graph,
     auto edge,kumi::tuple<vertex_id,vertex_id> expected_ports){

    auto actual_ports = graphs::ports(graph,edge);
    return expected_ports == actual_ports;
  };

inline constexpr auto has_vincidence =
  [](auto direction,const auto &graph,
     vertex_id vertex,std::vector<edge_id> expected_edges){

    auto actual_edges =
      ranges::vec(graphs::vincident(direction,graph,vertex));

    return sorted_equal(expected_edges,actual_edges);
  };
  
inline constexpr auto has_vadjacency =
  [](auto direction,const auto &graph,
     vertex_id vertex,std::vector<vertex_id> expected_vertices){

    auto actual_vertices =
      ranges::vec(graphs::vadjacent(direction,graph,vertex));

    return sorted_equal(actual_vertices,expected_vertices);
  };
  
inline constexpr auto has_eadjacency =
  [](auto direction,const auto &graph,
     auto edge,auto expected_edges){

    auto actual_edges =
      ranges::vec(graphs::eadjacent(direction,graph,edge));

    return sorted_equal(actual_edges,expected_edges);
  };

inline constexpr auto has_consistent_components = [](const auto &graph,const auto &components){
  for(auto vertex:graphs::vertex_ids(graph)){
    auto components_with_vertex = std::ranges::count_if(
      graphs::vertex_ids(components.graph), [&](auto subgraph){
        return std::ranges::count_if(
          components.graph[subgraph].vertices(),
          [&](auto component){
            return components.graph[subgraph][component] == vertex;
          }) != 0;
      });
    if(components_with_vertex != 1)
      return false;
  }

  for(auto subgraph:graphs::vertex_ids(components.graph))
    if(!std::ranges::none_of(
         components.graph[subgraph].vertices(), [&](auto component){
           return components.graph[subgraph][component] >=
             graphs::order(graph);
         }))
      return false;
  for(auto edge:graphs::edge_ids(graph)){
    auto [vertex_in, vertex_out] = graphs::ports(graph,edge);
    auto component_in = kumi::get<0>(components.map(vertex_in)),
      component_out = kumi::get<0>(components.map(vertex_out));
    if(is_internal(components.map(edge))){
      if(component_in != component_out)
        return false;
    }
    else if(component_in == component_out)
        return false;
  }
  for(auto external_edge:graphs::edge_ids(components.graph)){
    auto edge = components.graph[external_edge];
    if(!(edge < graphs::size(graph)))
      return false;
  }
  for(auto component_vertex:graphs::vertex_ids(components.graph)){
    auto component = components.graph[component_vertex];
    if(!std::ranges::all_of(component.edges(), [&](auto internal_edge){
      return component[internal_edge] < graphs::size(graph);
    }))
      return false;
  }

  auto internal_edge_count =
    [&](auto edge, auto component_vertex){
      auto component = components.graph[component_vertex];
      return std::ranges::count(
        graphs::edge_ids(component), edge,
        [&](auto internal_edge){ return component[internal_edge]; });
    };

  for(auto edge:graphs::edge_ids(graph)){
    if(!(std::ranges::count(components.graph.edges(),
                            edge,
                            [&](auto id){
                              return components.graph[id];
                            })
         < 2))
      return false;

    if(!(std::ranges::none_of(graphs::vertex_ids(components.graph),
                              bind<>(std::ranges::less{}, 1),
                              bind<>(internal_edge_count, edge))))
      return false;

    if(!(
         (std::ranges::count(graphs::edge_ids(components.graph), edge,
                             [&](auto id){ return components.graph[id]; }))
         != (std::ranges::count_if(
               graphs::vertex_ids(components.graph),
               bind<>(std::ranges::equal_to{}, 1),
               bind<>(internal_edge_count, edge)) == 1)))
      return false;
  }
  return true;
};

inline constexpr auto has_component_vertices =
  [](const auto &components, std::vector<std::vector<vertex_id>> expected){
    const auto expected_component_count = expected.size(),
      actual_component_count = graphs::order(components.graph);

    auto actual =
      ranges::vec(graphs::vertex_ids(components.graph), [&](auto component){
        return ranges::vec(
          components.graph[component].vertices(),
          [&](auto component_vertex){
            return components.graph[component][component_vertex];
          });
      });
    std::ranges::for_each(actual, std::ranges::sort);
    std::ranges::for_each(expected, std::ranges::sort);
    std::ranges::sort(expected);
    std::ranges::sort(actual);
    if(!(expected_component_count == actual_component_count))
      return false;
    return std::ranges::equal(actual, expected);
  };

inline constexpr auto has_component_external_edges =
  [](const auto &components,
     std::vector<std::array<natural, 2>> external_edges){
    const auto actual_external_edge_count = graphs::size(components.graph),
      expected_external_edge_count = external_edges.size();
    if(expected_external_edge_count != actual_external_edge_count)
      return false;

    for(auto [source, target]:external_edges){
      auto source_component = kumi::get<0>(components.map(vertex_id{source})),
        target_component = kumi::get<0>(components.map(vertex_id{target}));
      if(source_component == target_component)
        return false;
      if(!components.graph.has_edge(source_component,target_component))
        return false;
    }
    return true;
  };

} //hysj::graphs
#include<hysj/tools/epilogue.hpp>
