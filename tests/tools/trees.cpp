#include<algorithm>
#include<ranges>
#include<vector>

#include<framework.hpp>

#include<hysj/tools/trees.hpp>
#include<hysj/tools/functional/ignore.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

TEST_CASE("trees"){

    SUBCASE("0"){
      using input_range = std::vector<int>;

      input_range input{0,1,2,3,4};
      
      constexpr auto grammar = trees::var(trees::one);

      CHECK(grammar.rule_len(0) == 2);

      auto iter_tree = trees::parse<grammar>(input);
      auto value_tree = trees::map<grammar>(
        iter_tree,
        [](ignore_t,auto iter){
          return *iter;
        });
      REQUIRE(std::ranges::equal(value_tree, input));
    }

    SUBCASE("1"){
      using input_range = std::vector<bool>;

      input_range input{true,false,true,false,true};
      
      constexpr auto grammar = trees::tup(trees::one, trees::var(trees::one));
      CHECK(grammar.rule_len(0) == 4);

      auto iter_tree = trees::parse<grammar>(input);
    
      auto value_tree = trees::map<grammar>(
        iter_tree,
        [](ignore_t,auto itr) -> bool{
          return *itr;
        });

      REQUIRE(kumi::get<0>(value_tree) == input[0]);
      REQUIRE(std::ranges::equal(kumi::get<1>(value_tree), std::views::drop(input, 1)));
    }

  }
}
#include<hysj/tools/epilogue.hpp>
