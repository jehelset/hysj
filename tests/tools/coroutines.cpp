#include<optional>
#include<ranges>

#include"../framework.hpp"

#include<hysj/tools/coroutines.hpp>
#include<hysj/tools/coroutines/trees.hpp>
#include<hysj/tools/types.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::coroutines::builtin{

co_api<bool,none> branch_algorithm(co_get<bool,none>){
  auto &co_this = HYSJ_CO(co_get<bool,none>);
  co_final_await co::ret<1>(co_this,true);
}

co_api<none> root_algorithm(co_get<none>,bool &entered,bool &exited){
  auto &co_this = HYSJ_CO(co_get<none>);
  CHECK(!entered);
  entered = true;
  bool yielded = *co_await branch_algorithm(co::get<bool>(co_this));
  CHECK(yielded);
  exited = true;
  co_final_await co::nil<0>(co_this);
}

TEST_CASE("coroutines.resume"){
  bool
    entered = false,
    exited  = false;
  root_algorithm(co_get<none>{},entered,exited)();
  CHECK(entered);
  CHECK(exited);
}

TEST_CASE("coroutines.get.0"){

  int x = 0,y = 3, z = 5;

  void *p0_ptr = nullptr,*p1_ptr = nullptr,*p2_ptr = nullptr;

  auto f1 = [&]<typename PP1>(PP1,auto g2) -> api_t<PP1> {
    auto &p1 = HYSJ_CO(PP1);
    p1_ptr = static_cast<void*>(&p1);

    CHECK(p1_ptr == &co::ancestor<0>(p1));
    CHECK(p0_ptr == &co::ancestor<1>(p1));
    CHECK(p0_ptr == &co::ancestor<-1>(p1));
    co_await co::put<0>(p1,false);
    co_await g2(co::get<none>(p1));
    x = z;
    co_final_await co::nil<1>(p1);
  };
  auto f0 = [&]<typename PP0>(PP0) -> api_t<PP0> {
    auto &p0 = HYSJ_CO(PP0);
    p0_ptr = static_cast<void*>(&p0);
    CHECK(p0_ptr == &co::ancestor<0>(p0));
    auto g2 = [&]<typename PP2>(PP2) -> api_t<PP2> {
      auto &p2 = HYSJ_CO(PP2);
      p2_ptr = static_cast<void*>(&p2);
      CHECK(p2_ptr == &co::ancestor<0>(p2));
      CHECK(p1_ptr == &co::ancestor<1>(p2));
      CHECK(p0_ptr == &co::ancestor<2>(p2));
      CHECK(p0_ptr == &co::ancestor<-1>(p2));
      CHECK(p1_ptr == &co::ancestor<-2>(p2));

      co_await co::put<0>(p2,true);
      co_await co::ret<2>(p2,y);
    };
      
    x = *co_await f1(co::get<int>(p0),g2);
    co_final_await co::ret<0>(p0,true);
  };

  auto c0 = f0(co_get<bool>{});
  CHECK(!c0().value());
  CHECK(c0().value());
  CHECK(c0().value());
  CHECK(p0_ptr != nullptr);
  CHECK(p1_ptr != nullptr);
  CHECK(p2_ptr != nullptr);
  CHECK(x == y);
}

template<typename CoGet>
co::api_t<CoGet> f0(CoGet){
  auto &co_this = HYSJ_CO(CoGet);

  co_await co::put<>(co_this,'a');
  co_await co::put<>(co_this,'b');
  co_await co::ret<>(co_this,'c');
}
co_api<int> f1(co_get<int>){
  auto &co_this = HYSJ_CO(co_get<int>);

  for(auto C = f0(co::get<char>(co_this));C;){
    co_await C;
    co_await co::put<0>(co_this,2);
  }
  co_await co::nil<0>(co_this);
}

TEST_CASE("coroutines.get.1"){
  auto f = f1({});
  std::size_t n = 0;
  for(auto i:f){
    ++n;
    CHECK(i == 2);
  }
  CHECK(n == 3);
}

struct dummy{
  int g = 33;
};

co_api<dummy> f2(co_get<dummy>){
  auto &co_this = HYSJ_CO(co_get<dummy>);

  co_await co::put<0>(co_this,dummy{});
  co_await co::ret<0>(co_this,dummy{});
}

TEST_CASE("coroutines.iterator"){
  auto f = f2({});
  auto it = std::ranges::begin(f);
  REQUIRE(it->g == 33);
  ++it;
  REQUIRE(it->g == 33);
  ++it;
  REQUIRE(it == std::ranges::end(f));
  auto g = f2({});
  it = std::ranges::begin(g);
  REQUIRE(it->g == 33);
}

TEST_CASE("coroutines.empty"){
  {
    auto f = f1({});
    f.chop();
    REQUIRE(f.stump());
  }
  {
    auto t0 = f1({});
    auto t1 = std::move(t0);
    CHECK(t1() != std::nullopt);
  }
  {
    co_api<int> t0{};
    auto t1 = f1({});
    t1 = std::move(t0);
    CHECK(t1.stump());
  }
}
co_api<int> f3(co_get<int>){
  throw std::runtime_error("error");
  co_return none{};
}
TEST_CASE("coroutines.throw"){
  auto t = f3({});
  CHECK_THROWS(t());
}

} //tools/coroutines::builtin
#include<hysj/tools/epilogue.hpp>
