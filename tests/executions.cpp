#include<functional>
#include<ranges>
#include<vector>

#include<hysj/codes.hpp>
#include<hysj/executions.hpp>
#include<hysj/executions.hpp>
#include<hysj/devices/vulkan.hpp>
#include<hysj/devices/gcc.hpp>
#include<hysj/tools/ranges/generate.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/types.hpp>

#include<framework.hpp>
#include<constructions.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::executions{

template<natural N>
struct vk_test_fixture{
  HYSJ_TEST_FIXTURE((fmt::format("vk{}", N)));

  static constexpr natural width = N;

  const codes::code &code;
  codes::devsym dev;

  static vkenv make_env(codes::devsym dev){
    auto e = vkenv::make();
    auto it = std::ranges::find(e.devs, devices::vulkan::devtype::cpu, &devices::vulkan::dev::type);
    HYSJ_ASSERT(it != std::ranges::end(e.devs));
    it->sym = dev;
    return e;
  }

  vkenv env = make_env(dev);

  auto exe(codes::apisym a){
    return devs::exe(env, code, a);
  }
};

#define VK_TEST_FIXTURES \
  vk_test_fixture<32>,   \
  vk_test_fixture<64>    \

template<natural N>
struct gcc_test_fixture{
  HYSJ_TEST_FIXTURE((fmt::format("gcc{}", N)));

  static constexpr natural width = N;

  const codes::code &code;
  codes::devsym dev;

  gccenv env{
    {},
    dev
  };

  auto exe(codes::apisym a){
    return devs::exe(env, code, a);
  }
};

#define GCC_TEST_FIXTURES \
  gcc_test_fixture<32>,   \
  gcc_test_fixture<64>


TEST_CASE_TEMPLATE("executions.buffer", test_fixture, VK_TEST_FIXTURES, GCC_TEST_FIXTURES){
  using namespace codes::factories;

  codes::code code{};

  auto dev = codes::dev(code);
  test_fixture fixture{code, dev};

  static constexpr auto width = constant_c<fixture.width>;

  const auto N = 4;
  auto ring = buffer::make(code, N);

  const auto M = 3;
  const auto m = icst(code, M, width);
  const auto i = itr(code, m);
  const auto j = itr(code, m);

  auto x0 = rvar(code, width);
  auto xi = rvar(code, width, {i});
  auto xij = rvar(code, width, {i, j});

  auto is_full = bvar(code, width);

  auto b0 = cc_buffer(code, ring, {x0});
  auto bi = cc_buffer(code, ring, {xi});
  auto bij = cc_buffer(code, ring, {xij});

  auto get_is_full = codes::task(
    then(
      code,
      {
        codes::on(code, dev, put(code, is_full, ring.is_full)),
        recv(code, dev, is_full)
      }));

  auto get_begin = codes::task(recv(code, dev, ring.begin));
  auto get_size = codes::task(recv(code, dev, ring.size));
  auto put_begin = codes::task(send(code, dev, ring.begin));
  auto put_size = codes::task(send(code, dev, ring.size));

  const std::array V{ 4, 8, 9};
  auto v = ranges::vec(V, [&](auto v){ return codes::icst(code, v, width); });

  auto
    current_b0 = cc_current(code, ring, b0),
    current_bi = cc_current(code, ring, bi),
    current_bij = cc_current(code, ring, bij);

  auto
    next_b0 = cc_next(code, ring, b0),
    next_bi = cc_next(code, ring, bi),
    next_bij = cc_next(code, ring, bij);

  auto
    prev_b0 = cc_prev(code, ring, b0),
    prev_bi = cc_prev(code, ring, bi),
    prev_bij = cc_prev(code, ring, bij);

  auto fill_current_b0 = codes::put(code, current_b0, v[0]),
    fill_current_bi = codes::put(code, current_bi, v[0]),
    fill_current_bij = codes::put(code, current_bij, v[0]);

  auto fill_next_b0 = codes::put(code, next_b0, v[1]),
    fill_next_bi = codes::put(code, next_bi, v[1]),
    fill_next_bij = codes::put(code, next_bij, v[1]);

  auto fill_prev_b0 = codes::put(code, prev_b0, v[2]),
    fill_prev_bi = codes::put(code, prev_bi, v[2]),
    fill_prev_bij = codes::put(code, prev_bij, v[2]);

  auto fill_b_current = codes::task(
    codes::on(code, dev,
              codes::then(code,
                          {
                            fill_current_b0,
                            fill_current_bi,
                            fill_current_bij
                          })));
  auto fill_b_next = codes::task(
    codes::on(code, dev,
              codes::then(code,
                          {
                            fill_next_b0,
                            fill_next_bi,
                            fill_next_bij
                          })));
  auto fill_b_prev = codes::task(
    codes::on(code, dev,
              codes::then(code,
                          {
                            fill_prev_b0,
                            fill_prev_bi,
                            fill_prev_bij
                          })));

  auto clear = codes::task(codes::on(code, dev, ring.clear));
  auto cycle = codes::task(codes::on(code, dev, ring.cycle));
  auto push = codes::task(codes::on(code, dev, ring.push));

  auto get_b0 = codes::task(recv(code, dev, b0));
  auto get_bi = codes::task(recv(code, dev, bi));
  auto get_bij = codes::task(recv(code, dev, bij));
  auto get_b = codes::task(codes::when(code, {get_b0, get_bi, get_bij}));

  auto api = codes::api(
    code,
    views::pin(push, clear, cycle, get_begin, get_size, put_begin, put_size,
               get_is_full, get_b, fill_b_current, fill_b_next, fill_b_prev));

  compile(code);

  CHECK(orank(code, b0) == orank(code, x0) + 1);
  CHECK(orank(code, bi) == orank(code, xi) + 1);
  CHECK(orank(code, bij) == orank(code, xij) + 1);

  auto [mem, exe] = fixture.exe(api);

  auto begin_mem = mem.i64(ring.begin), size_mem = mem.i64(ring.size);
  auto is_full_mem = mem.b(width, is_full);

  REQUIRE(begin_mem.size() == 1);
  REQUIRE(size_mem.size() == 1);
  REQUIRE(is_full_mem.size() == 1);

  auto b0_mem = mem.r(width, b0),
    bi_mem = mem.r(width, bi),
    bij_mem = mem.r(width, bij);

  REQUIRE(b0_mem.size() == N);
  REQUIRE(bi_mem.size() == N * M);
  REQUIRE(bij_mem.size() == N * M * M);
  
  auto ring_is = [&](auto b, auto e){
    exe.run(get_begin);
    exe.run(get_size);
    return begin_mem[0] == b && size_mem[0] == e;
  };

  exe.run(clear);
  exe.run(get_begin);
  exe.run(get_size);
  CHECK(ring_is(0, 0));
  exe.run(get_is_full);
  CHECK(!is_full_mem[0]);
  exe.run(push);
  CHECK(ring_is(0, 1));

  exe.run(fill_b_current);
  exe.run(get_b);
  CHECK_RANGE_APPROX(b0_mem.subspan(0, 1), std::views::repeat(V[0], 1), width);
  CHECK_RANGE_APPROX(bi_mem.subspan(0, M), std::views::repeat(V[0], M), width);
  CHECK_RANGE_APPROX(bij_mem.subspan(0, M * M), std::views::repeat(V[0], M * M), width);
  exe.run(fill_b_prev);
  exe.run(get_b);
  CHECK_RANGE_APPROX(b0_mem.subspan(N - 1, 1), std::views::repeat(V[2], 1), width);
  CHECK_RANGE_APPROX(bi_mem.subspan((N - 1) * M, M), std::views::repeat(V[2], M), width);
  CHECK_RANGE_APPROX(bij_mem.subspan((N - 1) * M * M, M * M), std::views::repeat(V[2], M * M), width);


  begin_mem[0] = N - 1;
  size_mem[0] = N - 1;
  exe.run(put_begin);
  exe.run(put_size);
  exe.run(clear);
  CHECK(ring_is(0, 0));

  exe.run(push);
  CHECK(ring_is(0, 1));
  exe.run(get_is_full);
  CHECK(!is_full_mem[0]);

  exe.run(push);
  CHECK(ring_is(0, 2));
  exe.run(get_is_full);
  CHECK(!is_full_mem[0]);
  exe.run(fill_b_next);
  exe.run(get_b);
  CHECK_RANGE_APPROX(b0_mem.subspan(2, 1), std::views::repeat(V[1], 1), width);
  CHECK_RANGE_APPROX(bi_mem.subspan(2*M, M), std::views::repeat(V[1], M), width);
  CHECK_RANGE_APPROX(bij_mem.subspan(2*M * M, M * M), std::views::repeat(V[1], M * M), width);

  exe.run(push);
  CHECK(ring_is(0, 3));
  exe.run(get_is_full);
  CHECK(!is_full_mem[0]);

  exe.run(cycle);
  CHECK(ring_is(3, 0));
  exe.run(get_is_full);
  CHECK(!is_full_mem[0]);
  for(auto i = 0;i != N; ++i)
    exe.run(push);
  CHECK(ring_is(3, N));
  exe.run(get_is_full);
  CHECK(is_full_mem[0]);

  exe.run(clear);
  CHECK(ring_is(0, 0));

  for(auto i = 0;i != N; ++i)
    exe.run(push);
  CHECK(ring_is(0, N));
  exe.run(get_is_full);
  CHECK(is_full_mem[0]);

}

namespace discrete{
  TEST_CASE_TEMPLATE("executions.discrete.transitions", test_fixture, VK_TEST_FIXTURES, GCC_TEST_FIXTURES){
    using namespace codes::factories;

    codes::code code{};

    auto dev = codes::dev(code);
    test_fixture fixture{code, dev};

    static constexpr auto width = constant_c<fixture.width>;

    const auto M = 2;

    SUBCASE("0"){

      auto N = icst(code, 1, width);
      auto i = itr(code, N);
      auto t = ivar(code, width);
      auto q = ivar(code, width, {i});
      auto Q = depvar(code, {t}, 1, {q});

      auto F = ivar(code, width, {i});
      auto G = ivar(code, width, {i});

      auto R = executions::buffer::make(code, M);
      dautomaton A{
        .variables{
          .independent = t,
          .dependent{Q}
        },
        .transitions{
          {
            .variable{Q[1]},
            .function{F},
            .guard{G}
          }
        }
      };
      const auto &d = A.transitions[0];

      auto put_begin = codes::task(send(code, dev, R.begin));
      auto put_size = codes::task(send(code, dev, R.size));

      auto X_exec = cc_state(code, A.variables, R);
      REQUIRE(X_exec.buffer.dependent.size() == 1);
      REQUIRE(X_exec.buffer.dependent[0].size() == 2);

      const auto t_exec = X_exec.buffer.independent;
      const auto q_exec = X_exec.buffer.dependent[0][0];
      const auto dq_exec = X_exec.buffer.dependent[0][1];

      {
        auto F = cc_substitute_current(code, X_exec);
        for(auto &d:A.transitions)
          d = cc_substitute_transition(F, d);
      }
      

      auto D = cc_transitions(code,
                              ranges::vec(A.transitions,
                                          bind<>(&cc_transition, std::ref(code), dev)));
      REQUIRE(D.container.size() == 1);

      const auto d_exec = D.container[0];

      auto put_guard = codes::task(send(code, dev, d.guard));
      auto put_function = codes::task(send(code, dev, d.function));

      auto get_exec_guard = codes::task(recv(code, dev, d_exec.guard));
      auto get_exec_function = codes::task(recv(code, dev, d_exec.function));

      D.compute_guards = { codes::on(code, dev, D.compute_guards) };
      D.compute_functions = { codes::on(code, dev, D.compute_functions) };
      D.compute_variables = { codes::on(code, dev, D.compute_variables) };
      D.compute_event = { codes::on(code, dev, D.compute_event) };

      auto get_event = codes::task(recv(code, dev, D.event));
      auto get_state = codes::task(
        when(
          code,
          {
            recv(code, dev, t_exec),
            recv(code, dev, q_exec),
            recv(code, dev, dq_exec)
          }));
      auto put_state = codes::task(
        when(
          code,
          {
            send(code, dev, t_exec),
            send(code, dev, q_exec),
            send(code, dev, dq_exec)
          }));

      auto api = codes::api(
        code,
        views::pin(D.compute_guards, D.compute_functions, D.compute_variables, D.compute_event,
                   put_begin, put_size, put_guard, put_function, get_exec_guard, get_exec_function,
                   get_event, get_state, put_state));

      auto [mem, exe] = fixture.exe(api);

      auto begin_mem = mem.i64(R.begin), size_mem = mem.i64(R.size);
      auto t_mem = mem.i(width, t_exec);
      auto q_mem = mem.i(width, q_exec);
      auto dq_mem = mem.i(width, dq_exec);
      auto guard_mem = mem.i(width, d.guard);
      auto exec_guard_mem = mem.i(width, d_exec.guard);
      auto function_mem = mem.i(width, d.function);
      auto exec_function_mem = mem.i(width, d_exec.function);
      auto event_mem = mem.b64(D.event);

      REQUIRE(begin_mem.size() == 1);
      REQUIRE(size_mem.size() == 1);
      REQUIRE(t_mem.size() == M);
      REQUIRE(q_mem.size() == M);
      REQUIRE(dq_mem.size() == M);
      REQUIRE(guard_mem.size() == 1);
      REQUIRE(exec_guard_mem.size() == 1);
      REQUIRE(function_mem.size() == 1);
      REQUIRE(exec_function_mem.size() == 1);
      REQUIRE(event_mem.size() == 1);

      std::ranges::fill(t_mem, 0);
      std::ranges::fill(q_mem, 0);

      exe.run(put_state);

      SUBCASE("guards_and_event"){
        begin_mem[0] = 0;
        exe.run(put_begin);
        for(auto [z, g]:std::views::cartesian_product(views::indices(M), views::indices(2)))
          SUBCASE(fmt::format("z = {}, g = {}", z, g).c_str()){
            size_mem[0] = z + 1;
            exe.run(put_size);
            guard_mem[0] = g;
            exe.run(put_guard);
            exe.run(D.compute_guards);
            exe.run(get_exec_guard);
            CHECK(exec_guard_mem[0] == g);
            exe.run(D.compute_event);
            exe.run(get_event);
            CHECK(event_mem[0] == g);
          }
      }

      SUBCASE("functions_and_variable"){
        begin_mem[0] = 0;
        size_mem[0] = 1;
        exe.run(put_begin);
        exe.run(put_size);
        guard_mem[0] = 1;
        function_mem[0] = 2;
        exe.run(put_guard);
        exe.run(put_function);
        exe.run(D.compute_guards);
        exe.run(D.compute_functions);
        exe.run(D.compute_variables);
        exe.run(get_exec_function);
        exe.run(get_state);
        CHECK(exec_function_mem[0] == function_mem[0]);
        CHECK(t_mem[0] == 0);
        CHECK(q_mem[0] == 0);
        CHECK(dq_mem[0] == function_mem[0]);
      }
    }

  }
}

} //hysj::executions
#include<hysj/tools/epilogue.hpp>
