#pragma once

#include<vector>

#include<hysj/automata.hpp>
#include<hysj/codes.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/types.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::automata::hybrid{

inline hautomaton sawtooth(code &code,std::array<real,3> A_value){

  using namespace codes::factories;

  auto negone = code.builtins.lits.negone64;
  auto top = code.builtins.lits.top64;

  auto t = rvar64(code);
  auto u = ivar64(code);
  auto r = ivar64(code);
  auto x = rvar64(code);

  auto A = ranges::vec(A_value, [&](auto a){ return rcst64(code, a); });
  auto X = depvar(code, {t}, 1, {x});

  auto F = sub(code,A[0], X[1]);
  auto G = sub(code,A[1], X[0]);

  return hautomaton{
    .discrete_automaton{
      .variables{
        .independent = u,
        .dependent{
          {{r}}
        }
      },
      .transitions{}
    },
    .continuous_variables{
      .independent = t,
      .dependent{X},
    },
    .activities{
      {.equation=F, .guard = top},
    },
    .actions{
      {.variable{X[0]}, .function{A[2]}, .guard{eq(code, r, negone)}}
    },
    .roots{
      {.variable{r}, .function{G}, .guard{top}}
    }
  };
}

}
#include<hysj/tools/epilogue.hpp>
