#include<algorithm>
#include<functional>
#include<ranges>
#include<variant>
#include<vector>

#include<hysj/codes.hpp>
#include<hysj/automata.hpp>
#include<hysj/executions.hpp>
#include<hysj/constructions.hpp>

#include<hysj/devices/vulkan.hpp>
#include<hysj/tools/coroutines.hpp>
#include<hysj/tools/ranges/map.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/types.hpp>
#include<hysj/tools/functional/bind.hpp>

#include<automata/sawtooth.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::constructions{

namespace discrete{

  template<natural width>
  struct trace_event{
    devent tag;

    icell<width> independent;
    std::vector<std::vector<std::vector<icell<width>>>> dependent;
    std::vector<std::vector<bcell<width>>> guards;

    auto operator<=>(const trace_event &) const = default;

    static trace_event make(auto &run,auto index){
      auto &&buffer = unwrap(run.exec).state.buffer;
      run.get();
      return {
        .tag = run.event(index),
        .independent = run.time(constant_c<width>)[index][0],
        .dependent = ranges::vec(
          views::indices(buffer.dependent),
          [&](auto i) -> std::vector<std::vector<icell<width>>>{
            return ranges::vec(run.istate(constant_c<width>, i),
                               [&](auto q){
                                 return ranges::vec(q[index]);
                               });
          }),
        .guards = ranges::vec(
          views::indices(unwrap(run.exec).transitions.container),
          [&](auto i) -> std::vector<bcell<width>> {
            return ranges::vec(run.transition_guard(constant_c<width>, i));
          })
      };
    }
  };
  template<natural W>
  auto trace_n(auto &run,constant_t<W> w,natural n){
    run.init();
    std::vector<trace_event<W>> trace;
    do{
      auto ring = run();
      for(auto index:ring){
        trace.push_back(trace_event<W>::make(run, index));
        if(trace.size() == n)
          break;
      }
    }while(run && trace.size() < n);
    return trace;
  }

}

namespace continuous{

  template<natural width>
  struct trace_event{
    cevent tag;

    rcell<width> independent;
    std::vector<std::vector<std::vector<rcell<width>>>> dependent;

    auto operator<=>(const trace_event &) const = default;

    static trace_event make(cevent tag,auto &run){
      auto &state = unwrap(run.exec).state.buffer;
      run.get();

      auto rmap = [&](auto i){
        return ranges::vec(run.mem->r(constant_c<width>, i));
      };
      return {
        .tag = tag,
        .independent = rmap(state.independent)[0],
        .dependent = ranges::vec(state.dependent, bind<1>(ranges::vec,rmap))
      };
    }
  };

  template<natural W>
  auto trace(auto &run,constant_t<W>){
    run.init();
    return run(co_get<trace_event<W>>{}, 
                        [&](auto co_get, auto tag){
                          if constexpr(tag == any_of(cevent::stop,cevent::fail))
                            return co::ret<0>(co::promise(co_get),
                                              trace_event<W>::make(tag, run));
                          else
                            return co::put<0>(co::promise(co_get),
                                              trace_event<W>::make(tag, run));
                        });
  }
  template<natural W>
  auto trace_n(auto &run,constant_t<W> w,natural n){
    auto events = trace(run, w);
    return ranges::vec(std::views::take(events,n));
  }
  auto trace_is_consistent(const auto &events,auto n){
    return events.size() >= 1 &&
      events.size() <= n &&
      std::ranges::count_if(events,[](auto e){ return e.tag == cevent::fail; }) == 0;
  }
  auto trace_was_started(const auto &events){
    return events.size() > 1;
  }
  auto trace_was_stopped(const auto &events){
    return events.size() && events.back().tag == cevent::stop;
  }
  auto trace_is_simple(const auto &events,auto n){
    return
      trace_is_consistent(events,n)
      && trace_was_started(events)
      && trace_was_stopped(events)
      && std::ranges::all_of(events, views::slice(events,0,-1),
                             [](auto e){ return e.tag == cevent::step; })
                             ;
  }
  auto trace_count(auto &&trace,cevent e0){
    return std::ranges::count_if(
      trace,
      [=](auto e1){
        return e1.tag == e0;
      });
  }

}

namespace hybrid{

  template<natural width>
  struct trace_event{
    hevent tag;

    auto operator<=>(const trace_event &) const = default;

    static trace_event make(hevent tag,auto &run){
      run.get();
      return {
        .tag = tag
      };
    }
  };

  template<natural width>
  using any_trace_event = std::variant<continuous::trace_event<width>,
                                       discrete::trace_event<width>,
                                       trace_event<width>>;

  template<natural W>
  auto trace(auto &run,constant_t<W>){
    run.init();
    return run(
      co_get<any_trace_event<W>>{}, 
      [&]<typename Event>(auto co_get, Event event,auto... tail){
        if constexpr(is_constant_of<Event,cevent>)
          return co::put<0>(co::promise(co_get),
                            continuous::trace_event<W>::make(
                              event, *run.continuous_construction));
        else if constexpr(is_constant_of<Event,devent>)
          return co::put<0>(co::promise(co_get),
                            discrete::trace_event<W>::make(
                              *run.discrete_construction, tail...));
        else
          return co::put<0>(co::promise(co_get),
                            hybrid::trace_event<W>::make(event, run));
      });
  }
  template<natural W>
  auto trace_n(auto &run,constant_t<W> w,natural n){
    auto events = hybrid::trace(run, w);
    return ranges::vec(std::views::take(events,n));
  }

  template<natural W>
  auto trace_ccount(auto &&trace,constant_t<W>,cevent e0){
    return std::ranges::count_if(
      trace,
      [=](auto e1){
        return std::holds_alternative<cruns::trace_event<W>>(e1)
          && std::get<cruns::trace_event<W>>(e1).tag == e0;
      });
  }
  template<natural W>
  auto trace_dcount(auto &&trace,constant_t<W>,devent e0){
    return std::ranges::count_if(
      trace,
      [=](auto e1){
        return std::holds_alternative<druns::trace_event<W>>(e1)
          && std::get<druns::trace_event<W>>(e1).tag == e0;
      });
  }
  template<natural W>
  auto trace_hcount(auto &&trace,constant_t<W>,hevent e0){
    return std::ranges::count_if(
      trace,
      [=](auto e1){
        return std::holds_alternative<hruns::trace_event<W>>(e1)
          && std::get<hruns::trace_event<W>>(e1).tag == e0;
      });
  }
  template<natural W>
  auto trace_has_failed(const auto &events,constant_t<W>){
    return ranges::contains_if(
      events,
      [](auto e){
        return std::holds_alternative<cruns::trace_event<W>>(e)
          && std::get<cruns::trace_event<W>>(e).tag == cevent::fail;
      });
  }


} //hybrid

} //hysj::constructions

template<hysj::_HYSJ_VERSION_NAMESPACE::natural width>
struct fmt::formatter<hysj::_HYSJ_VERSION_NAMESPACE::druns::trace_event<width>>{
  using T = hysj::_HYSJ_VERSION_NAMESPACE::druns::trace_event<width>;
  constexpr auto parse(format_parse_context& context)const{
    auto it  = context.begin(),
         end = context.end();
    if(it != end && *it != '}')
      throw fmt::format_error("invalid format");
    return it;
  }
  auto format(const T &e,format_context &context)const{
    return fmt::format_to(context.out(),"({}: t = {}, q = {}, g = {})",
                          e.tag,e.independent,e.dependent,e.guards);
  }
};

template<hysj::_HYSJ_VERSION_NAMESPACE::natural width>
struct fmt::formatter<hysj::_HYSJ_VERSION_NAMESPACE::cruns::trace_event<width>>{
  using T = hysj::_HYSJ_VERSION_NAMESPACE::cruns::trace_event<width>;
  constexpr auto parse(format_parse_context& context)const{
    auto it  = context.begin(),
         end = context.end();
    if(it != end && *it != '}')
      throw fmt::format_error("invalid format");
    return it;
  }
  auto format(const T &e,format_context &context)const{
    return fmt::format_to(context.out(),"({}: t = {}, x = {})",
                          e.tag,
                          static_cast<double>(e.independent),
                          hysj::_HYSJ_VERSION_NAMESPACE::ranges::vec(
                            e.dependent,
                            hysj::_HYSJ_VERSION_NAMESPACE::bind<-1>(
                              hysj::_HYSJ_VERSION_NAMESPACE::ranges::vec,
                              hysj::_HYSJ_VERSION_NAMESPACE::bind<-1>(
                                hysj::_HYSJ_VERSION_NAMESPACE::ranges::vec,
                                hysj::_HYSJ_VERSION_NAMESPACE::cast<double>))));
  }
};

template<hysj::_HYSJ_VERSION_NAMESPACE::natural width>
struct fmt::formatter<hysj::_HYSJ_VERSION_NAMESPACE::hruns::trace_event<width>>{
  using T = hysj::_HYSJ_VERSION_NAMESPACE::hruns::trace_event<width>;
  constexpr auto parse(format_parse_context& context)const{
    auto it  = context.begin(),
         end = context.end();
    if(it != end && *it != '}')
      throw fmt::format_error("invalid format");
    return it;
  }
  auto format(const T &e,format_context &context)const{
    return fmt::format_to(context.out(),"({})",e.tag);
  }
};
#include<hysj/tools/epilogue.hpp>
