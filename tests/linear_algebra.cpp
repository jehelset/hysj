#include<algorithm>
#include<functional>
#include<limits>
#include<random>
#include<ranges>
#include<utility>
#include<vector>
#include<span>

#include<kumi/tuple.hpp>
#include<fmt/format.h>

#include"framework.hpp"

#include<hysj/codes/cells.hpp>
#include<hysj/codes/code.hpp>
#include<hysj/codes/factories.hpp>
#include<hysj/codes/grammar.hpp>
#include<hysj/codes/sets.hpp>
#include<hysj/devices/gcc.hpp>
#include<hysj/devices/vulkan.hpp>
#include<hysj/linear_algebra/gaussian_elimination.hpp>
#include<hysj/tools/functional/bind.hpp>
#include<hysj/tools/ranges/span.hpp>
#include<hysj/tools/ranges/utility.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::linear_algebra{

template<natural N>
struct vk_test_fixture{
  HYSJ_TEST_FIXTURE((fmt::format("vk{}", N)));

  static constexpr natural width = N;

  const codes::code &code;
  codes::devsym dev;

  static auto make_env(codes::devsym dev){
    auto e = vkenv::make();
    auto it = std::ranges::find(e.devs, devices::vulkan::devtype::cpu, &devices::vulkan::dev::type);
    HYSJ_ASSERT(it != std::ranges::end(e.devs));
    it->sym = dev;
    it->spirv.max_local_size = 1;
    it->spirv.optimization_level = 0;
    return e;
  }

  vkenv env = make_env(dev);

  auto exe(codes::apisym a){
    return devs::exe(env, code, a);
  }
};

#define VK_TEST_FIXTURES \
  vk_test_fixture<32>,   \
  vk_test_fixture<64>    \

template<natural N, natural M = 0>
struct gcc_test_fixture{
  HYSJ_TEST_FIXTURE((fmt::format("gcc{}", N, M == 0 ? std::string{"hw"} : std::to_string(M))));

  static constexpr natural width = N;

  const codes::code &code;
  codes::devsym dev;

  gccenv env{
    {}, dev
  };

  auto exe(codes::apisym a){
    return devs::exe(env, code, a);
  }
};

#define GCC_TEST_FIXTURES   \
  gcc_test_fixture<32, 1>,  \
  gcc_test_fixture<64, 1>


TEST_CASE_TEMPLATE("linear_algebra.swap_rows", test_fixture, VK_TEST_FIXTURES, GCC_TEST_FIXTURES){

  using namespace codes::factories;

  codes::code code{};
  auto dev = codes::dev(code);
  test_fixture fixture{code, dev};

  static constexpr auto width = constant_c<fixture.width>;

  const auto cc = [&](codes::exprfam matrix, codes::exprfam row0, codes::exprfam row1){

    auto send = codes::send(code, dev, matrix);
    auto recv = codes::recv(code, dev, matrix);

    compile(code, matrix);
    auto run = swap_rows(code, matrix, row0, row1);

    auto algo = codes::task(
      codes::then(code, {
          send,
          codes::on(code, dev, run),
          recv
        }));

    auto bin = codes::api(code, {algo});
    auto [mem, exe] = fixture.exe(bin);

    auto matrix_mem = mem.r(width, matrix);
    return kumi::make_tuple(std::move(mem), std::move(exe), algo, matrix_mem);
  };

  const auto N_lit = 2;

  for(auto N_lit:views::indices(1, 10))
    SUBCASE(fmt::format("N = {}", N_lit).c_str()){

      const auto N = icst(code, N_lit, width);
      const auto A = rvar(code, width, {itr(code, N), itr(code, N)});

      for(auto [i0_lit, i1_lit]:std::views::cartesian_product(views::indices(0, N_lit), views::indices(0, N_lit)))
        SUBCASE(fmt::format("row₀ = {}, row₁ = {}", i0_lit, i1_lit).c_str()){
          auto i0 = icst(code, i0_lit, width);
          auto i1 = icst(code, i1_lit, width);
          auto [mem, exe, algo, A_mem] = cc({A}, {i0}, {i1});

          std::ranges::copy(views::indices(0, N_lit), A_mem.data());
          const auto A_mem_expected = [&]{
            std::vector<rcell<width>> A_mem_expected(N_lit * N_lit);
            std::ranges::copy(A_mem, A_mem_expected.data());
            std::ranges::swap_ranges(std::span{A_mem_expected}.subspan(i0_lit * N_lit, N_lit),
                                     std::span{A_mem_expected}.subspan(i1_lit * N_lit, N_lit));
            return A_mem_expected;
          }();

          exe.run(algo);

          CHECK_RANGE_APPROX(A_mem, A_mem_expected, width);
        }
    }
}
TEST_CASE_TEMPLATE("linear_algebra.find_pivot", test_fixture, VK_TEST_FIXTURES, GCC_TEST_FIXTURES){

  using namespace codes::factories;

  codes::code code{};
  auto dev = codes::dev(code);
  test_fixture fixture{code, dev};

  static constexpr auto width = constant_c<fixture.width>;

  const auto cc = [&](codes::exprfam matrix, codes::exprfam first_row, codes::exprfam col, codes::exprfam pivot){

    auto send = codes::send(code, dev, matrix);
    auto recv = codes::recv(code, dev, pivot);

    compile(code, matrix);
    auto run = find_pivot(code, matrix, first_row, col, pivot);

    auto algo = codes::task(
      codes::then(code, {
          send,
          codes::on(code, dev, run),
          recv
        }));

    auto bin = codes::api(code, {algo});
    auto [mem, exe] = fixture.exe(bin);

    auto matrix_mem = mem.r(width, matrix);
    auto pivot_mem = mem.i(width, pivot);
    return kumi::make_tuple(std::move(mem), std::move(exe), algo, matrix_mem, pivot_mem);
  };

  static std::uniform_real_distribution<rcell<width>> random_distribution{-100, 100};
  static std::random_device random_device;
  static std::mt19937 random_engine{random_device()};

  for(auto N_lit:views::indices(1, 10))
    SUBCASE(fmt::format("N = {}", N_lit).c_str()){

      const auto M = icst(code, 1, width);
      const auto N = icst(code, N_lit, width);
      const auto A = rvar(code, width, {itr(code, N), itr(code, M)});
      const auto col = icst(code, 0, width);
      const auto p = ivar(code, width);

      for(auto r_lit:views::indices(0, N_lit))
        SUBCASE(fmt::format("row₀ = {}", r_lit).c_str()){
          auto r = icst(code, r_lit, width);
          auto [mem, exe, algo, A_mem, p_mem] = cc({A}, {r}, {col}, {p});

          std::ranges::fill(A_mem.first(r_lit), std::numeric_limits<rcell<width>>::infinity());
          auto A_col = A_mem.subspan(r_lit, N_lit - r_lit);
          std::ranges::generate(A_col, [&]{ return random_distribution(random_engine); });
          icell<width> p_mem_expected = [&]{
            auto A_max = std::ranges::max_element(A_col, std::ranges::less{}, lift((std::abs)));
            HYSJ_ASSERT(A_max != std::ranges::end(A_col));
            return r_lit + std::ranges::distance(std::ranges::begin(A_col), A_max);
          }();
          exe.run(algo);

          CHECK(p_mem[0] == p_mem_expected);
        }
    }

}

TEST_CASE_TEMPLATE("linear_algebra.gaussian_elimination_with_partial_pivoting",
                   test_fixture, VK_TEST_FIXTURES, GCC_TEST_FIXTURES){

  using namespace codes::factories;

  codes::code code{};
  const auto dev = codes::dev(code);
  test_fixture fixture{code, dev};

  static constexpr auto width = constant_c<fixture.width>;
  using r = rcell<width()>;
  using rvec = std::vector<r>;
  using i = icell<width()>;
  using ivec = std::vector<i>;

  const auto cc = [&](codes::exprfam A, codes::exprfam p){

    compile(code, std::array{A, p});
    auto gepp = gaussian_elimination_with_partial_pivoting(code, A, p);

    auto send = codes::send(code, dev, A);
    auto recv = codes::when(code,{
        codes::recv(code, dev, p),
        codes::recv(code, dev, A)
      });

    auto algo = codes::task(
      codes::then(code, {
          send,
          codes::on(code, dev, gepp),
          recv
        }));

    auto bin = codes::api(code, {algo});
    auto [mem, exe] = fixture.exe(bin);

    auto A_mem = mem.r(width, A);
    auto p_mem = mem.i(width, p);

    return kumi::make_tuple(std::move(mem), std::move(exe), algo, A_mem, p_mem);
  };

  SUBCASE("N = 1"){
    const auto N = icst(code, 1, width);
    const auto A = rvar(code, width, {itr(code, N), itr(code, N)});
    const auto p = ivar(code, width, {itr(code, N)});

    auto [mem, exe, algo, A_mem, p_mem] = cc({A}, {p});

    REQUIRE(A_mem.size() == 1);
    REQUIRE(p_mem.size() == 1);

    A_mem[0] = 2;

    exe.run(algo);

    CHECK(A_mem[0] == approx(2, width));
    CHECK(p_mem[0] == 0);
  }

  SUBCASE("N = 2"){

    const auto N_lit = 2;
    const auto N = icst(code, N_lit, width);
    const auto A = rvar(code, width, {itr(code, N), itr(code, N)});
    const auto p = ivar(code, width, {itr(code, N)});

    auto [mem, exe, algo, A_mem, p_mem] = cc({A},{p});

    REQUIRE(A_mem.size() == N_lit * N_lit);
    REQUIRE(p_mem.size() == N_lit);

    SUBCASE("0"){
      std::ranges::copy(rvec{1, 0, 0, 1}, A_mem.data());
      exe.run(algo);

      rvec A_mem_expected{1, 0, 0, 1};
      ivec p_mem_expected{0, 1};

      CHECK_RANGE_APPROX(A_mem, A_mem_expected, width);
      CHECK(p_mem == p_mem_expected);
    }
    SUBCASE("1"){
      std::ranges::copy(rvec{0, 1, 1, 0}, A_mem.data());
      exe.run(algo);

      rvec A_mem_expected{1, 0, 0, 1};
      ivec p_mem_expected{1, 1};

      CHECK_RANGE_APPROX(A_mem, A_mem_expected, width);
      CHECK(p_mem == p_mem_expected);
    }
    SUBCASE("2"){
      std::ranges::copy(rvec{2, 1, 1, 3}, A_mem.data());
      exe.run(algo);

      rvec A_mem_expected{r(2), r(1), r(0.5), r(2.5)};
      ivec p_mem_expected{0, 1};

      CHECK_RANGE_APPROX(A_mem, A_mem_expected, width);
      CHECK(p_mem == p_mem_expected);
    }
  }
  SUBCASE("N = 3"){

    const auto N_lit = 3;
    const auto N = icst(code, N_lit, width);
    const auto A = rvar(code, width, {itr(code, N), itr(code, N)});
    const auto p = ivar(code, width, {itr(code, N)});

    auto [mem, exe, algo, A_mem, p_mem] = cc({A},{p});

    REQUIRE(A_mem.size() == N_lit * N_lit);
    REQUIRE(p_mem.size() == N_lit);

    SUBCASE("0"){
      std::ranges::copy(
        rvec{
           2, 1, 7,
          -4, 2, 0,
           4, 1, 8
        },
        A_mem.data());
      exe.run(algo);

      rvec A_mem_expected{
        r(-4),       r(2),       r(0),
        r(-1),       r(3),       r(8),
        r(-1.0/2.0), r(2.0/3.0), r(5.0 / 3.0),
      };
      ivec p_mem_expected{1, 2, 2};

      CHECK_RANGE_APPROX(A_mem, A_mem_expected, width);
      CHECK(p_mem == p_mem_expected);
    }
  }

}
TEST_CASE_TEMPLATE("linear_algebra.pivot_rows",
                   test_fixture, VK_TEST_FIXTURES, GCC_TEST_FIXTURES){

  using namespace codes::factories;

  codes::code code{};
  const auto dev = codes::dev(code);
  test_fixture fixture{code, dev};

  static constexpr auto width = constant_c<fixture.width>;
  using r = rcell<width()>;
  using rvec = std::vector<r>;
  using i = icell<width()>;
  using ivec = std::vector<i>;

  const auto cc = [&](codes::exprfam matrix, codes::exprfam pivots){

    auto send = codes::when(
      code,
      {
        codes::send(code, dev, matrix),
        codes::send(code, dev, pivots)
      });
    auto recv = codes::recv(code, dev, matrix);

    compile(code, std::array{matrix, pivots});
    auto run = pivot_rows(code, matrix, pivots);

    auto algo = codes::task(
      codes::then(code, {
          send,
          codes::on(code, dev, run),
          recv
        }));

    auto bin = codes::api(code, {algo});
    auto [mem, exe] = fixture.exe(bin);

    auto matrix_mem = mem.r(width, matrix);
    auto pivots_mem = mem.i(width, pivots);
    return kumi::make_tuple(std::move(mem), std::move(exe), algo, matrix_mem, pivots_mem);
  };

  SUBCASE("N = 1"){
    const auto N_lit = 1;
    const auto N = icst(code, N_lit, width);
    const auto b = rvar(code, width, {itr(code, N)});
    const auto p = ivar(code, width, {itr(code, N)});

    auto [mem, exe, algo, b_mem, p_mem] = cc({b}, {p});
    
    REQUIRE(b_mem.size() == N_lit);
    REQUIRE(p_mem.size() == N_lit);

    std::ranges::copy(rvec{2}, b_mem.data());
    std::ranges::copy(ivec{0}, p_mem.data());

    exe.run(algo);

    const rvec b_mem_expected{2};
    CHECK_RANGE_APPROX(b_mem, b_mem_expected, width);
  }

  SUBCASE("N = 2"){
    const auto N_lit = 2;
    const auto N = icst(code, N_lit, width);
    const auto b = rvar(code, width, {itr(code, N)});
    const auto p = ivar(code, width, {itr(code, N)});

    auto [mem, exe, algo, b_mem, p_mem] = cc({b}, {p});
    
    REQUIRE(b_mem.size() == N_lit);
    REQUIRE(p_mem.size() == N_lit);

    std::ranges::copy(rvec{2, 1}, b_mem.data());
    SUBCASE("0"){
      std::ranges::copy(ivec{0, 1}, p_mem.data());
      exe.run(algo);
      const rvec b_mem_expected{2, 1};
      CHECK_RANGE_APPROX(b_mem, b_mem_expected, width);
    }
    SUBCASE("1"){
      std::ranges::copy(ivec{1, 0}, p_mem.data());
      exe.run(algo);
      const rvec b_mem_expected{2, 1};
      CHECK_RANGE_APPROX(b_mem, b_mem_expected, width);
    }
    SUBCASE("2"){
      std::ranges::copy(ivec{1, 1}, p_mem.data());
      exe.run(algo);
      const rvec b_mem_expected{1, 2};
      CHECK_RANGE_APPROX(b_mem, b_mem_expected, width);
    }
    SUBCASE("3"){
      std::ranges::copy(ivec{0, 0}, p_mem.data());
      exe.run(algo);
      const rvec b_mem_expected{1, 2};
      CHECK_RANGE_APPROX(b_mem, b_mem_expected, width);
    }

  }

}
TEST_CASE_TEMPLATE("linear_algebra.upper_triangular_solve",
                   test_fixture, VK_TEST_FIXTURES, GCC_TEST_FIXTURES){

  using namespace codes::factories;

  codes::code code{};
  const auto dev = codes::dev(code);
  test_fixture fixture{code, dev};

  static constexpr auto width = constant_c<fixture.width>;
  using r = rcell<width()>;
  using rvec = std::vector<r>;

  const auto cc = [&](codes::exprfam A, codes::exprfam x){

    auto send = codes::when(
      code,
      {
        codes::send(code, dev, A),
        codes::send(code, dev, x)
      });
    auto recv = codes::recv(code, dev, x);

    compile(code, A);
    compile(code, x);
    auto run = upper_triangular_solve(code, A, x);

    auto algo = codes::task(
      codes::then(code, {
          send,
          codes::on(code, dev, run),
          recv
        }));

    auto bin = codes::api(code, {algo});
    auto [mem, exe] = fixture.exe(bin);

    auto A_mem = mem.r(width, A);
    auto x_mem = mem.r(width, x);
    return kumi::make_tuple(std::move(mem), std::move(exe), algo, A_mem, x_mem);
  };

  SUBCASE("N = 0"){
    const auto A = rvar(code, width);
    const auto x = rvar(code, width);

    auto [mem, exe, algo, A_mem, x_mem] = cc({A}, {x});

    REQUIRE(A_mem.size() == 1);
    REQUIRE(x_mem.size() == 1);

    SUBCASE("0"){
      std::ranges::copy(rvec{2}, A_mem.data());
      std::ranges::copy(rvec{0}, x_mem.data());
      exe.run(algo);
      const rvec x_mem_expected{0};
      CHECK_RANGE_APPROX(x_mem, x_mem_expected, width);
    }
    SUBCASE("1"){
      std::ranges::copy(rvec{2}, A_mem.data());
      std::ranges::copy(rvec{2}, x_mem.data());
      exe.run(algo);
      const rvec x_mem_expected{1};
      CHECK_RANGE_APPROX(x_mem, x_mem_expected, width);
    }
    SUBCASE("2"){
      std::ranges::copy(rvec{-2}, A_mem.data());
      std::ranges::copy(rvec{ 1}, x_mem.data());
      exe.run(algo);
      const rvec x_mem_expected{r(1.0 / -2.0)};
      CHECK_RANGE_APPROX(x_mem, x_mem_expected, width);
    }
    SUBCASE("3"){
      std::ranges::copy(rvec{-1}, A_mem.data());
      std::ranges::copy(rvec{ 2}, x_mem.data());
      exe.run(algo);
      const rvec x_mem_expected{r(-2.0)};
      CHECK_RANGE_APPROX(x_mem, x_mem_expected, width);
    }
  }

  SUBCASE("N = 1"){
    const auto N_lit = 1;
    const auto N = icst(code, N_lit, width);
    const auto i = itr(code, N), j = itr(code, N);
    const auto A = rvar(code, width, {i, j});
    const auto x = rvar(code, width, {j});

    auto [mem, exe, algo, A_mem, x_mem] = cc({A}, {x});

    REQUIRE(A_mem.size() == N_lit * N_lit);
    REQUIRE(x_mem.size() == N_lit);

    SUBCASE("0"){
      std::ranges::copy(rvec{2}, A_mem.data());
      std::ranges::copy(rvec{0}, x_mem.data());
      exe.run(algo);
      const rvec x_mem_expected{0};
      CHECK_RANGE_APPROX(x_mem, x_mem_expected, width);
    }
    SUBCASE("1"){
      std::ranges::copy(rvec{2}, A_mem.data());
      std::ranges::copy(rvec{2}, x_mem.data());
      exe.run(algo);
      const rvec x_mem_expected{1};
      CHECK_RANGE_APPROX(x_mem, x_mem_expected, width);
    }
    SUBCASE("2"){
      std::ranges::copy(rvec{-2}, A_mem.data());
      std::ranges::copy(rvec{ 1}, x_mem.data());
      exe.run(algo);
      const rvec x_mem_expected{r(1.0 / -2.0)};
      CHECK_RANGE_APPROX(x_mem, x_mem_expected, width);
    }
    SUBCASE("3"){
      std::ranges::copy(rvec{-1}, A_mem.data());
      std::ranges::copy(rvec{ 2}, x_mem.data());
      exe.run(algo);
      const rvec x_mem_expected{r(-2.0)};
      CHECK_RANGE_APPROX(x_mem, x_mem_expected, width);
    }
  }
  SUBCASE("N = 2"){
    const auto N_lit = 2;
    const auto N = icst(code, N_lit, width);
    const auto i = itr(code, N), j = itr(code, N);
    const auto A = rvar(code, width, {i, j});
    const auto x = rvar(code, width, {j});

    auto [mem, exe, algo, A_mem, x_mem] = cc({A}, {x});

    REQUIRE(A_mem.size() == N_lit * N_lit);
    REQUIRE(x_mem.size() == N_lit);

    SUBCASE("0"){
      std::ranges::copy(rvec{1, 0, 0, 1}, A_mem.data());
      std::ranges::copy(rvec{3, 4}, x_mem.data());
      exe.run(algo);
      const rvec x_mem_expected{3, 4};
      CHECK_RANGE_APPROX(x_mem, x_mem_expected, width);
    }
    SUBCASE("1"){
      std::ranges::copy(rvec{ 1, 2, 0, 1}, A_mem.data());
      std::ranges::copy(rvec{-1, 2}, x_mem.data());
      exe.run(algo);
      const rvec x_mem_expected{-5, 2};
      CHECK_RANGE_APPROX(x_mem, x_mem_expected, width);
    }
    SUBCASE("2"){
      std::ranges::copy(rvec{ r(-2),  r(2), r(0), r(-0.5)}, A_mem.data());
      std::ranges::copy(rvec{ r(0.5), r(2)}, x_mem.data());
      exe.run(algo);
      const rvec x_mem_expected{r(8.5 / -2.0), r(-4)};
      CHECK_RANGE_APPROX(x_mem, x_mem_expected, width);
    }
  }

}

TEST_CASE_TEMPLATE("linear_algebra.lower_triangular_solve",
                   test_fixture, VK_TEST_FIXTURES, GCC_TEST_FIXTURES){

  using namespace codes::factories;

  codes::code code{};
  const auto dev = codes::dev(code);
  test_fixture fixture{code, dev};

  static constexpr auto width = constant_c<fixture.width>;
  using r = rcell<width()>;
  using rvec = std::vector<r>;

  const auto cc = [&](codes::exprfam A, codes::exprfam x, codes::exprfam b){

    auto send = codes::when(
      code,
      {
        codes::send(code, dev, A),
        codes::send(code, dev, b)
      });
    auto recv = codes::recv(code, dev, x);

    compile(code, std::array{A, x, b});
    auto run = lower_triangular_solve(code, A, x, b);

    auto algo = codes::task(
      codes::then(code, {
          send,
          codes::on(code, dev, run),
          recv
        }));

    auto bin = codes::api(code, {algo});
    auto [mem, exe] = fixture.exe(bin);

    auto A_mem = mem.r(width, A);
    auto x_mem = mem.r(width, x);
    auto b_mem = mem.r(width, b);
    return kumi::make_tuple(std::move(mem), std::move(exe), algo, A_mem, x_mem, b_mem);
  };

  SUBCASE("N = 0"){
    const auto N_lit = 1;
    const auto N = icst(code, N_lit, width);
    const auto i = itr(code, N), j = itr(code, N);
    const auto A = rvar(code, width, {i, j});
    const auto x = rvar(code, width, {j});
    const auto b = rvar(code, width, {j});

    auto [mem, exe, algo, A_mem, x_mem, b_mem] = cc({A}, {x}, {b});

    REQUIRE(A_mem.size() == N_lit * N_lit);
    REQUIRE(x_mem.size() == N_lit);

    SUBCASE("0"){
      std::ranges::copy(rvec{2}, A_mem.data());
      std::ranges::copy(rvec{0}, b_mem.data());
      exe.run(algo);
      const rvec x_mem_expected{0};
      CHECK_RANGE_APPROX(x_mem, x_mem_expected, width);
    }
    SUBCASE("1"){
      std::ranges::copy(rvec{ 0}, A_mem.data());
      std::ranges::copy(rvec{-2}, b_mem.data());
      exe.run(algo);
      const rvec x_mem_expected{-2};
      CHECK_RANGE_APPROX(x_mem, x_mem_expected, width);
    }
  }
  SUBCASE("N = 1"){
    const auto N_lit = 1;
    const auto N = icst(code, N_lit, width);
    const auto i = itr(code, N), j = itr(code, N);
    const auto A = rvar(code, width, {i, j});
    const auto x = rvar(code, width, {j});
    const auto b = rvar(code, width, {j});

    auto [mem, exe, algo, A_mem, x_mem, b_mem] = cc({A}, {x}, {b});

    REQUIRE(A_mem.size() == N_lit * N_lit);
    REQUIRE(x_mem.size() == N_lit);

    SUBCASE("0"){
      std::ranges::copy(rvec{2}, A_mem.data());
      std::ranges::copy(rvec{0}, b_mem.data());
      exe.run(algo);
      const rvec x_mem_expected{0};
      CHECK_RANGE_APPROX(x_mem, x_mem_expected, width);
    }
    SUBCASE("1"){
      std::ranges::copy(rvec{ 0}, A_mem.data());
      std::ranges::copy(rvec{-2}, b_mem.data());
      exe.run(algo);
      const rvec x_mem_expected{-2};
      CHECK_RANGE_APPROX(x_mem, x_mem_expected, width);
    }
  }
  SUBCASE("N = 2"){
    const auto N_lit = 2;
    const auto N = icst(code, N_lit, width);
    const auto i = itr(code, N), j = itr(code, N);
    const auto A = rvar(code, width, {i, j});
    const auto x = rvar(code, width, {j});
    const auto b = rvar(code, width, {j});

    auto [mem, exe, algo, A_mem, x_mem, b_mem] = cc({A}, {x}, {b});

    REQUIRE(A_mem.size() == N_lit * N_lit);
    REQUIRE(x_mem.size() == N_lit);

    SUBCASE("0"){
      std::ranges::copy(rvec{0, 0, 0, 0}, A_mem.data());
      std::ranges::copy(rvec{3, 4}, b_mem.data());
      exe.run(algo);
      const rvec x_mem_expected{3, 4};
      CHECK_RANGE_APPROX(x_mem, x_mem_expected, width);
    }
    SUBCASE("1"){
      std::ranges::copy(rvec{ 0, 0, 1, 0}, A_mem.data());
      std::ranges::copy(rvec{-1, 2}, b_mem.data());
      exe.run(algo);
      const rvec x_mem_expected{-1, 3};
      CHECK_RANGE_APPROX(x_mem, x_mem_expected, width);
    }
  }

}

TEST_CASE_TEMPLATE("linear_algebra.lower_upper_solve",
                   test_fixture, VK_TEST_FIXTURES, GCC_TEST_FIXTURES){

  using namespace codes::factories;

  codes::code code{};
  const auto dev = codes::dev(code);
  test_fixture fixture{code, dev};

  static constexpr auto width = constant_c<fixture.width>;
  using r = rcell<width()>;
  using rvec = std::vector<r>;

  const auto cc = [&](codes::exprfam LU, codes::exprfam x, codes::exprfam Pb){
    auto send = codes::when(
      code,
      {
        codes::send(code, dev, LU),
        codes::send(code, dev, Pb)
      });
    auto recv = codes::recv(code, dev, x);

    compile(code, std::array{LU, x, Pb});
    auto run = lower_upper_solve(code, LU, x, Pb);

    auto algo = codes::task(
      codes::then(code, {
          send,
          codes::on(code, dev, run),
          recv
        }));

    auto bin = codes::api(code, {algo});
    auto [mem, exe] = fixture.exe(bin);

    auto LU_mem = mem.r(width, LU);
    auto x_mem = mem.r(width, x);
    auto Pb_mem = mem.r(width, Pb);
    return kumi::make_tuple(std::move(mem), std::move(exe), algo, LU_mem, x_mem, Pb_mem);
  };

  SUBCASE("N = 1"){
    const auto N_lit = 1;
    const auto N = icst(code, N_lit, width);
    const auto i = itr(code, N), j = itr(code, N);
    const auto A = rvar(code, width, {i, j});
    const auto x = rvar(code, width, {j});
    const auto Pb = rvar(code, width, {j});

    auto [mem, exe, algo, A_mem, x_mem, Pb_mem] = cc({A}, {x}, {Pb});

    REQUIRE(A_mem.size() == N_lit * N_lit);
    REQUIRE(x_mem.size() == N_lit);
    REQUIRE(Pb_mem.size() == N_lit);

    std::ranges::copy(rvec{2}, A_mem.data());
    std::ranges::copy(rvec{1}, Pb_mem.data());
    exe.run(algo);
    const rvec x_mem_expected{r(0.5)};
    CHECK_RANGE_APPROX(x_mem, x_mem_expected, width);

  }

}

TEST_CASE_TEMPLATE("linear_algebra.linear_solve",
                   test_fixture, VK_TEST_FIXTURES, GCC_TEST_FIXTURES){

  using namespace codes::factories;

  std::random_device random_device;
  std::mt19937 random_generator{random_device()};

  codes::code code{};
  const auto dev = codes::dev(code);
  test_fixture fixture{code, dev};

  static constexpr auto width = constant_c<fixture.width>;
  using r = rcell<width()>;
  using rvec = std::vector<r>;

  const auto cc = [&](integer N_lit){

    const auto N = icst(code, N_lit, width);
    const auto i = itr(code, N), j = itr(code, N);
    const auto A = codes::expr(N_lit != 0 ? rvar(code, width, {i, j}) : rvar(code, width));
    const auto x = codes::expr(N_lit != 0 ? rvar(code, width, {j}) : rvar(code, width));
    const auto b = codes::expr(N_lit != 0 ? rvar(code, width, {j}) : rvar(code, width));

    auto send = codes::when(
      code,
      {
        codes::send(code, dev, A),
        codes::send(code, dev, b)
      });
    auto recv = codes::recv(code, dev, x);

    compile(code, std::array{A, x, b});
    auto run = linear_solve(code, A, x, b);

    auto algo = codes::task(
      codes::then(code, {
          send,
          codes::on(code, dev, run),
          recv
        }));

    auto bin = codes::api(code, {algo});
    auto [mem, exe] = fixture.exe(bin);

    auto A_mem = mem.r(width, A);
    auto x_mem = mem.r(width, x);
    auto b_mem = mem.r(width, b);
    const auto M_lit = std::max(N_lit, 1_i);
    REQUIRE(A_mem.size() == M_lit * M_lit);
    REQUIRE(x_mem.size() == M_lit);
    REQUIRE(b_mem.size() == M_lit);
    return kumi::make_tuple(std::move(mem), std::move(exe), algo, A_mem, x_mem, b_mem);
  };

  SUBCASE("N = 0"){
    auto [mem, exe, algo, A_mem, x_mem, b_mem] = cc(0);
    SUBCASE("0"){
      std::ranges::copy(rvec{2}, A_mem.data());
      std::ranges::copy(rvec{1}, b_mem.data());
      exe.run(algo);
      const rvec x_mem_expected{r(0.5)};
      CHECK_RANGE_APPROX(x_mem, x_mem_expected, width);
    }
    SUBCASE("1"){
      std::ranges::copy(rvec{2}, A_mem.data());
      std::ranges::copy(rvec{0}, b_mem.data());
      exe.run(algo);
      const rvec x_mem_expected{0};
      CHECK_RANGE_APPROX(x_mem, x_mem_expected, width);
    }
    SUBCASE("2"){
      std::ranges::copy(rvec{-1}, A_mem.data());
      std::ranges::copy(rvec{ 1}, b_mem.data());
      exe.run(algo);
      const rvec x_mem_expected{-1};
      CHECK_RANGE_APPROX(x_mem, x_mem_expected, width);
    }
    SUBCASE("3"){
      std::ranges::copy(rvec{   8}, A_mem.data());
      std::ranges::copy(rvec{ -64}, b_mem.data());
      exe.run(algo);
      const rvec x_mem_expected{-8};
      CHECK_RANGE_APPROX(x_mem, x_mem_expected, width);
    }
    if constexpr(width == 32){
      std::ranges::copy(rvec{ r(-2.3)}, A_mem.data());
      std::ranges::copy(rvec{ r(-9.2)}, b_mem.data());
      exe.run(algo);
      const rvec x_mem_expected{r(-9.2) / r(-2.3)};
      CHECK_RANGE_APPROX(x_mem, x_mem_expected, width);
    }
  }
  SUBCASE("N = 1"){
    auto [mem, exe, algo, A_mem, x_mem, b_mem] = cc(1);
    SUBCASE("0"){
      std::ranges::copy(rvec{2}, A_mem.data());
      std::ranges::copy(rvec{1}, b_mem.data());
      exe.run(algo);
      const rvec x_mem_expected{r(0.5)};
      CHECK_RANGE_APPROX(x_mem, x_mem_expected, width);
    }
    SUBCASE("1"){
      std::ranges::copy(rvec{2}, A_mem.data());
      std::ranges::copy(rvec{0}, b_mem.data());
      exe.run(algo);
      const rvec x_mem_expected{0};
      CHECK_RANGE_APPROX(x_mem, x_mem_expected, width);
    }
    SUBCASE("2"){
      std::ranges::copy(rvec{-1}, A_mem.data());
      std::ranges::copy(rvec{ 1}, b_mem.data());
      exe.run(algo);
      const rvec x_mem_expected{-1};
      CHECK_RANGE_APPROX(x_mem, x_mem_expected, width);
    }
    SUBCASE("3"){
      std::ranges::copy(rvec{   8}, A_mem.data());
      std::ranges::copy(rvec{ -64}, b_mem.data());
      exe.run(algo);
      const rvec x_mem_expected{-8};
      CHECK_RANGE_APPROX(x_mem, x_mem_expected, width);
    }
    if constexpr(width == 32){
      std::ranges::copy(rvec{ r(-2.3)}, A_mem.data());
      std::ranges::copy(rvec{ r(-9.2)}, b_mem.data());
      exe.run(algo);
      const rvec x_mem_expected{r(-9.2) / r(-2.3)};
      CHECK_RANGE_APPROX(x_mem, x_mem_expected, width);
    }
  }
  SUBCASE("N = 2"){
    auto [mem, exe, algo, A_mem, x_mem, b_mem] = cc(2);
    SUBCASE("0"){
      std::ranges::copy(rvec{1, 0, 0, 1}, A_mem.data());
      std::ranges::copy(rvec{3, 4}, b_mem.data());
      exe.run({algo});
      const rvec x_mem_expected{3, 4};
      CHECK_RANGE_APPROX(x_mem, x_mem_expected, width);
    }
    SUBCASE("1"){
      std::ranges::copy(rvec{0, 1, 1, 0}, A_mem.data());
      std::ranges::copy(rvec{3, 4}, b_mem.data());
      exe.run({algo});
      const rvec x_mem_expected{4, 3};
      CHECK_RANGE_APPROX(x_mem, x_mem_expected, width);
    }
    SUBCASE("2"){
      std::ranges::copy(rvec{1, 1, 2, 0}, A_mem.data());
      std::ranges::copy(rvec{1, 2}, b_mem.data());
      exe.run({algo});
      const rvec x_mem_expected{1, 0};
      CHECK_RANGE_APPROX(x_mem, x_mem_expected, width);
    }
    SUBCASE("3"){
      std::ranges::copy(rvec{1, 2, 3, 5}, A_mem.data());
      std::ranges::copy(rvec{1, 2}, b_mem.data());
      exe.run({algo});
      const rvec x_mem_expected{-1, 1};
      CHECK_RANGE_APPROX(x_mem, x_mem_expected, width);
    }
  }

  SUBCASE("permutation_matrix"){

    const auto N = 10u;

    auto [mem, exe, algo, A_mem, x_mem, b_mem] = cc(N);

    for([[maybe_unused]] auto i:views::indices(N)){

      auto I = ranges::vec(views::indices(N));

      std::ranges::shuffle(I, random_generator);

      std::ranges::fill(A_mem, 0);
      for(auto j:views::indices(N)){
        A_mem[j * N + I[j]] = 1;
        b_mem[j] = static_cast<r>(I[j]);
      }

      exe.run({algo});

      for(auto j:views::indices(N))
        CHECK(x_mem[I[j]] == approx(static_cast<r>(I[j]), width));

    }

  }

  if constexpr(width == 64)
    SUBCASE("armadillo.fn_solve_1"){
      auto [mem, exe, algo, A_mem, x_mem, b_mem] = cc(5);
      std::ranges::copy(
        rvec{
           0.061198,   0.201990,   0.019678,  -0.493936,  -0.126745,
           0.437242,   0.058956,  -0.149362,  -0.045465,   0.296153,
          -0.492474,  -0.031309,   0.314156,   0.419733,   0.068317,
           0.336352,   0.411541,   0.458476,  -0.393139,  -0.135040,
           0.239585,  -0.428913,  -0.406953,  -0.291020,  -0.353768,
        },
        A_mem.data());
      std::ranges::copy(
        rvec{
          0.906104,
          0.372084,
          0.826970,
          0.911125,
          0.764937
        },
        b_mem.data());
      exe.run({algo});
      const rvec x_mem_expected{
        -1.486404584527645e+00,
        -1.298396730449162e+01,
         9.470697159029561e+00,
        -9.356796987754391e+00,
         9.375696966670487e+00
      };
      CHECK_RANGE_APPROX(x_mem, x_mem_expected, width);
    }

}

}
#include<hysj/tools/epilogue.hpp>
