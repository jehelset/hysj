#include<functional>
#include<random>
#include<ranges>

#include"framework.hpp"

#include<hysj/codes.hpp>
#include<hysj/automata.hpp>
#include<hysj/tools/types.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::automata{

TEST_CASE("automata.discrete"){

  using namespace codes::factories;

  codes::code code{};
  
  auto t = ivar64(code);

  SUBCASE("0"){
    auto Q = depvar(code,{t},1,{ivar64(code)});

    dautomaton automaton{
      .variables{
        .independent = t,
        .dependent{
          Q
        }
      },
      .transitions{
        {
          .variable = Q[1],
          .function = code.builtins.lits.one64,
          .guard = code.builtins.lits.top64
        }
      }
    };

    REQUIRE_NOTHROW(compile(code));
    CHECK(dorder(automaton) == 1);
    CHECK(dorder(code,automaton) == 1);
    CHECK(drank(automaton) == 1);
    CHECK(drank(code,automaton) == 1);

    {
      auto Q_also = automaton.variables.dependent[0_n];
      CHECK(Q_also[0] == Q[0]);
    }
    {
      auto [v,d,g] = automaton.transitions[0];
      CHECK(v == Q[1]);
      CHECK((d == codes::exprfam(code.builtins.lits.one64)));
      CHECK((g == codes::exprfam(code.builtins.lits.top64)));
    }
  }
  SUBCASE("1"){

    std::vector Q{
      depvar(code,{t},1,{ivar64(code)}),
      depvar(code,{t},1,{ivar64(code)})
    };
    dautomaton automaton{
      .variables{
        .independent = t,
        .dependent = Q
      },
      .transitions{
        {
          .variable = Q[0][0],
          .function = code.builtins.lits.one64,
          .guard = code.builtins.lits.top64
        },
        {
          .variable = Q[0][0],
          .function = code.builtins.lits.negone64,
          .guard = code.builtins.lits.top64
        },
        {
          .variable = Q[1][0],
          .function = code.builtins.lits.one64,
          .guard = code.builtins.lits.top64
        }
      }
    };

    REQUIRE_NOTHROW(compile(code));
    CHECK(dorder(code,automaton) == 2);
    CHECK(drank(code,automaton) == 3);
  }
  SUBCASE("2"){

    auto N = icst64(code,10_i);
    auto i = itr(code, N);
    auto Q = depvar(code,{t},1,{ivar64(code,{i})});

    dautomaton automaton{
      .variables{
        .independent = t,
        .dependent{
          Q
        }
      },
      .transitions{
        {
          .variable = Q[0],
          .function = code.builtins.lits.one64,
          .guard    = code.builtins.lits.top64
        }
      }
    };

    REQUIRE_NOTHROW(compile(code));
    CHECK(dorder(code,automaton) == 10);
    CHECK(drank(code,automaton) == 10);

  }
  SUBCASE("3"){

    auto Z = icst64(code, 2);
    auto q = var(code, typ64(code, Z));
    auto Q = depvar(code,{t},1,{q});

    dautomaton automaton{
      .variables{
        .independent = t,
        .dependent{
          Q
        }
      },
      .transitions{
        {
          .variable = Q[0],
          .function = code.builtins.lits.one64,
          .guard    = code.builtins.lits.top64
        }
      }
    };

    REQUIRE_NOTHROW(compile(code));
    CHECK(dcardinality(code,automaton) == 2);

  }
  
}
TEST_CASE("automata.continuous"){

  using namespace codes::factories;

  codes::code code{};

  auto t = rvar64(code);

  SUBCASE("0"){
    auto X = depvar(code,{t},1,{rvar64(code)});
    cautomaton automaton{
      .variables{
        .independent = t,
        .dependent = {X}
      },
      .equations{
        {sub(code,X[1],code.builtins.lits.one64)}
      },
      .roots{
        {sub(code,X[0],code.builtins.lits.one64)},
        X[0]
      }
    };

    REQUIRE_NOTHROW(compile(code));
    CHECK(corder(code,automaton) == 1);
    CHECK(crank(code,automaton) == 1);
    CHECK(csize(code,automaton) == 2);
    CHECK(corder(automaton) == 1);
    CHECK(crank(automaton) == 1);
    CHECK(csize(automaton) == 2);
    CHECK(cautomata::regular(code, automaton));
  }
}
TEST_CASE("automata.hybrid"){

  using namespace codes::factories;

  codes::code code{};

  auto t = rvar64(code);
  auto z = ivar64(code);
  
  SUBCASE("0"){

    auto Q = depvar(code,{z},1,{ivar64(code)});
    auto X = depvar(code,{t},1,{ivar64(code)});

    hautomaton automaton{
      .discrete_automaton{
        .variables{
          .independent = z,
          .dependent{
            Q
          }
        },
        .transitions{
          {
            .variable = Q[0],
            .function = code.builtins.lits.one64,
            .guard = eq(code,X[0],code.builtins.lits.one64)
          }
        }
      },
      .continuous_variables{
        .independent = t,
        .dependent = {X}
      },
      .activities{
        {
          .equation = sub(code,X[1],code.builtins.lits.one64),
          .guard = code.builtins.lits.top64
        }
      },
      .actions{
        {
          .variable = X[0],
          .function = code.builtins.lits.zero64,
          .guard    = eq(code,X[0],code.builtins.lits.one64)
        }
      }
    };

    REQUIRE_NOTHROW(compile(code));
  }
  SUBCASE("1"){

    auto Q = depvar(code,{z},1,{ivar64(code)});
    auto X = depvar(code,{t},1,{rvar64(code)});

    hautomaton automaton{
      .discrete_automaton{
        .variables{
          .independent = z,
          .dependent = {Q},
        },
        .transitions{
          
        }
      },
      .continuous_variables{
        .independent = t,
        .dependent = {X}
      },
      .activities{
        {
          .equation = sub(code,X[1],code.builtins.lits.one64),
          .guard = eq(code,X[0],code.builtins.lits.one64)
        },
        {
          .equation = sub(code,X[1],code.builtins.lits.negone64),
          .guard = eq(code,X[0],code.builtins.lits.zero64)
        }
      },
      .actions{
        {
          .variable = X[0],
          .function = X[1],
          .guard = eq(code, Q[0], Q[1])
        }
      },
      .roots{
        {
          .variable = Q[0],
          .function = X[0],
          .guard = code.builtins.lits.top64
        }
      }
    };

    REQUIRE_NOTHROW(compile(code));

    CHECK(hrank(automaton) == 2);
    CHECK(hrank(code, automaton) == 2);
    CHECK(hsize(automaton) == 1);
    CHECK(hsize(code, automaton) == 1);
    CHECK(hdegree(automaton) == 1);
    CHECK(hdegree(code, automaton) == 1);
  }
}

} //hysj::automata
#include<hysj/tools/epilogue.hpp>
