#include<vector>

#include<fmt/core.h>

#include<hysj/codes/code.hpp>
#include<hysj/codes/factories.hpp>
#include<hysj/codes/compile.hpp>
#include<hysj/codes/access.hpp>
#include<hysj/codes/to_string.hpp>
#include<hysj/tools/ranges/utility.hpp>

#include<framework.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

TEST_CASE("codes.access.strides_of"){

  codes::code code{};

  auto msg0 = [&](auto f,auto R0,auto R1){
    return fmt::format("strides({}) → {} ≠ {}",
                       to_string(code,f),
                       ranges::vec(R1,[&](auto d){ return to_string(code,d); }),
                       ranges::vec(R0,[&](auto d){ return to_string(code,d); }));
  };
  auto msg1 = [&](auto f,auto i,auto r0,auto r1){
    return fmt::format("strides({},{}) → {} ≠ {}",
                       to_string(code,f),
                       i,
                       to_string(code,r1),
                       to_string(code,r0));
  };

  #define CHECK_STRIDES(f0,R0)                                \
    {                                                         \
      REQUIRE_NOTHROW(compile(code,f0));                      \
      auto R1 = strides_of(code, f0);                         \
      REQUIRE_MESSAGE(R1.size() == R0.size(),msg0(f0,R0,R1)); \
      for(auto i:views::indices(R0)){                         \
        auto check = equal(code,R0[i],R1[i]);                 \
        CHECK_MESSAGE(check,msg1(f0,i,R0[i],R1[i]));          \
      }                                                       \
    }

  SUBCASE("0"){
    auto c = rcst16(code,{});
    CHECK_THROWS(strides_of(code, c));
  }
  SUBCASE("1"){
    auto n = ncst16(code, 8);
    auto i = itr(code, n);
    auto c = rvar16(code, {i});
    std::vector<id> s{code.builtins.lits.one16};
    CHECK_STRIDES(c, s);
  }
  SUBCASE("2"){
    auto n = ncst16(code, 8);
    auto i = itr(code, n), j = itr(code, n);
    auto c = rvar16(code, {i, j});
    std::vector<id> s{mul(code, {code.builtins.lits.one16, n}), code.builtins.lits.one16};
    CHECK_STRIDES(c, s);
  }
  SUBCASE("3"){
    auto m = ncst32(code, 4);
    auto n = ncst16(code, 8);
    auto i = itr(code, m);
    auto j = itr(code, n);
    auto x = ivar16(code, {i, j});
    std::vector<id> s{mul(code, {code.builtins.lits.one32, n}), code.builtins.lits.one32};
    CHECK_STRIDES(x, s);
  }
  SUBCASE("4"){
    auto m = ncst32(code, 4);
    auto n = ncst16(code, 8);
    auto i = itr(code, m);
    auto j = itr(code, n);
    auto x = ivar16(code, {i, j});
    auto y = rfl(code, x);
    std::vector<id> s{mul(code, {code.builtins.lits.one32, n}), code.builtins.lits.one32};
    CHECK_STRIDES(y, s);
  }
  #undef CHECK_STRIDES

}
TEST_CASE("codes.access.needs_wrap"){
  codes::code code{};
  for(auto e:views::indices(2, 10)){
    auto n = ncst64(code, e);
    auto i = itr(code, n);
    auto x = rvar64(code, {i});
    auto f = rot(code, x, {code.builtins.lits.one64});
    compile(code, f);
    auto j = oitrs(code, f)[0];
    CHECK(!needs_wrap(code, i, j));
  }
}
TEST_CASE("codes.access.index_of"){

  codes::code code{};

  auto msg = [&](auto f,auto i0,auto i1){
    return fmt::format("index({}) → {} ≠ {}",
                       to_string(code,f),
                       to_string(code,i1),
                       to_string(code,i0));
  };

  #define CHECK_INDEX(f0,i0)                   \
    {                                          \
      REQUIRE_NOTHROW(compile(code,f0));       \
      auto S1 = strides_of(code,f0);           \
      auto i1 = index_of(code,f0,S1);          \
      auto check = equal(code,i0,i1);          \
      CHECK_MESSAGE(check,msg(f0,i0,i1));      \
    }

  SUBCASE("0"){
    auto c = rcst16(code,{});
    CHECK_THROWS(index_of(code, c));
  }
  SUBCASE("1"){
    auto n = ncst16(code, 8);
    auto i = itr(code, n);
    auto c = rvar16(code, {i});
    std::vector<id> s{code.builtins.lits.one16};
    auto j = add(code, {mul(code, {s[0], i})});
    CHECK_INDEX(c, j);
  }
  SUBCASE("2"){
    auto n0 = ncst32(code, 4);
    auto n1 = ncst32(code, 8);
    auto i0 = itr(code, n0);
    auto i1 = itr(code, n1);
    auto x = ivar32(code, {i0, i1});
    std::vector<id> s{mul(code, {code.builtins.lits.one32, n1}), code.builtins.lits.one32};
    auto j = add(code, {mul(code, {s[0], i0}), mul(code, {s[1], i1})});
    CHECK_INDEX(x, j);
  }
  SUBCASE("3"){

    auto n = icst32(code, 3);
    auto i = itr(code, n);
    auto j = itr(code, n);
    auto k = itr(code, n);

    auto x = ivar32(code, {i, j});
    auto f = rot(code,x,{k, k});
    
    std::vector<id> s{mul(code, {code.builtins.lits.one32, n}), code.builtins.lits.one32};

    auto h = add(code, {mul(code, {s[0], k}), mul(code, {s[1], k})});

    CHECK_INDEX(f, h);
  }

  #undef CHECK_INDEX

}
TEST_CASE("codes.access.accessess"){

  codes::code code;

  auto cpu = dev(code),
    gpu = dev(code);

  auto x = ivar64(code), y = ivar64(code);

  auto m0 = put(code, x, add(code, {x, y}));

  auto api =
    codes::api(
      code,
      {
        to(code, cpu, gpu, x),
        on(code, gpu, m0),
        to(code, gpu, cpu, x)
      });

  auto accesses = codes::accesses::make(code, api.id());

  CHECK(std::ranges::equal(accesses.inputs(cpu), views::pin(expr(x))));
  CHECK(std::ranges::equal(accesses.outputs(cpu), views::pin(expr(x))));
  CHECK(std::ranges::equal(accesses.inputs(gpu), views::pin(expr(x), expr(y))));
  CHECK(std::ranges::equal(accesses.outputs(gpu), views::pin(expr(x))));
}


} //hysj::codes
#include<hysj/tools/epilogue.hpp>
