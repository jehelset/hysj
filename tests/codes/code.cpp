#include<vector>
#include<ranges>
#include<optional>

#include<hysj/codes/code.hpp>
#include<hysj/codes/factories.hpp>

#include<framework.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

TEST_CASE("codes.code"){

  codes::code code{};

  auto K = cst64(code, cstdat{set::integers});
  auto W = ncst64(code, 32_n);
  auto t = typ(code, K, W);

  SUBCASE("ops_and_args"){

    std::vector x{
      var(code, t),
      var(code, t)
    };

    auto y = var(code, t);

    std::vector c{
      var(code, t),
      var(code, t)
    };

    CHECK(exprcast(id{etoi(lortag()),0}).has_value());
    CHECK(exprcast(id{etoi(vartag()),0}).has_value());
    CHECK(exprcast(id{etoi(lortag()),0}).has_value());

    auto e = eq(code, y, add(code, {x[0],x[1]}));

    {
      auto [lhs,rhs] = args(code, e);
      CHECK(lhs == exprfam(y));
    }

    std::vector g{
      lt(code, c[0],y),
      lt(code, y   ,c[1])
    };

    {
      auto N = ncst64(code, 10);
      auto i = itr(code, N);
      auto j = itr(code, N);
      auto R = typ(code, cst64(code, set::reals), ncst64(code, 64_n));
      auto A = var(code, R, {i, j});

      auto A_ii = rot(code, A, {i, i});

      auto [_,I] = rels(code, A_ii);

      REQUIRE(std::ranges::size(I) == 2);

      auto ai0 = I[0];
      CHECK(relargid(code, ai0) == id(i));
      auto ai1 = I[1];
      CHECK(relargid(code, ai1) == id(i));
    }

  }

  SUBCASE("is_arg"){
    std::vector x{
      rot(code, K),
      rot(code, K)
    };
    auto f = rot(code, x[0], {x[1]});
    auto g = rot(code, icst64(code, 0),{x[1]});
    CHECK(is_arg(code,f,x[0]));
    CHECK(is_arg(code,f,x[1]));
    CHECK(!is_arg(code,g,x[0]));
    CHECK(is_arg(code,g,x[1]));
  }

  SUBCASE("equal"){
    std::vector x{
      rot(code, K),
      rot(code, K),
      rot(code, K)
    };
    std::vector y{
      icst64(code,0)
    };
    auto f = add(code,views::pin(x[0],x[1]));
    CHECK(equal(code,f,f));
    CHECK(!equal(code,x[0],f));
    CHECK(!equal(code,x[0],y[0]));
    CHECK(equal(code,x[0],x[1]));
    CHECK(equal(code,x[0],x[2]));
  }

  SUBCASE("identities"){

    auto K = typ(code, cst64(code, litdat{set::integers}), ncst64(code,32));
    auto x = var(code,K);
    {
      CHECK(is_zero(code,code.builtins.lits.zero64).value_or(false));
      CHECK(is_zero(code,icst64(code,0)).value_or(false));
      CHECK(is_zero(code,rcst64(code,0)).value_or(false));
      CHECK(!is_zero(code,code.builtins.lits.two64).value_or(false));
      CHECK(!is_zero(code,code.builtins.lits.negone64).value_or(false));
      CHECK(!is_zero(code,code.builtins.lits.pi64).value_or(false));
      CHECK(!is_zero(code,code.builtins.lits.e64).value_or(false));
      CHECK(!is_zero(code,code.builtins.lits.inf64).value_or(false));
      CHECK(!is_zero(code,icst64(code,-2)).value_or(false));
      CHECK(!is_zero(code,rcst64(code,0.9999999999)).value_or(false));
    }
    {
      CHECK(is_one(code,code.builtins.lits.one64).value_or(false));
      CHECK(is_one(code,icst64(code,1)).value_or(false));
      CHECK(is_one(code,rcst64(code,1.0)).value_or(false));
      CHECK(!is_one(code,code.builtins.lits.zero64).value_or(false));
      CHECK(!is_one(code,code.builtins.lits.two64).value_or(false));
      CHECK(!is_one(code,code.builtins.lits.negone64).value_or(false));
      CHECK(!is_one(code,code.builtins.lits.pi64).value_or(false));
      CHECK(!is_one(code,code.builtins.lits.e64).value_or(false));
      CHECK(!is_one(code,code.builtins.lits.inf64).value_or(false));
      CHECK(!is_one(code,icst64(code,-2)).value_or(false));
      CHECK(!is_one(code,rcst64(code,0.9999999999)).value_or(false));
    }
    {
      CHECK(is_negone(code,code.builtins.lits.negone64).value_or(false));
      CHECK(is_negone(code,icst64(code,-1)).value_or(false));
      CHECK(is_negone(code,rcst64(code,-1.0)).value_or(false));
      CHECK(!is_negone(code,code.builtins.lits.zero64).value_or(false));
      CHECK(!is_negone(code,code.builtins.lits.two64).value_or(false));
      CHECK(!is_negone(code,code.builtins.lits.e64).value_or(false));
      CHECK(!is_negone(code,code.builtins.lits.pi64).value_or(false));
      CHECK(!is_negone(code,code.builtins.lits.e64).value_or(false));
      CHECK(!is_negone(code,code.builtins.lits.inf64).value_or(false));
      CHECK(!is_negone(code,icst64(code,-2)).value_or(false));
      CHECK(!is_negone(code,rcst64(code,0.9999999999)).value_or(false));
    }
    {
      CHECK(is_odd(code,code.builtins.lits.one64).value_or(false));
      CHECK(is_odd(code,code.builtins.lits.one64).value_or(false));
      CHECK(is_odd(code,code.builtins.lits.negone64).value_or(false));
      CHECK(is_odd(code,code.builtins.lits.pi64) == std::nullopt);
      CHECK(is_odd(code,code.builtins.lits.e64) == std::nullopt);
      CHECK(is_odd(code,code.builtins.lits.inf64) == std::nullopt);
      auto o = icst64(code,1);
      auto p = rcst64(code,1.0);
      auto q = rcst64(code,0.5);
      CHECK(is_odd(code,o).value_or(false));
      CHECK(is_odd(code,p) == std::nullopt);
      CHECK(is_odd(code,q) == std::nullopt);
      CHECK(is_odd(code,x) == std::nullopt);
    }
  }


}

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
