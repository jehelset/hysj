#include<vector>

#include<fmt/core.h>

#include<hysj/codes/code.hpp>
#include<hysj/codes/factories.hpp>
#include<hysj/codes/compile.hpp>
#include<hysj/codes/compile/shapes.hpp>
#include<hysj/codes/to_string.hpp>
#include<hysj/tools/ranges/utility.hpp>

#include<framework.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

TEST_CASE("codes.compile.shape"){

  codes::code code{};

  auto msg0 = [&](auto f,auto R0,auto R1){
    return fmt::format("dim({}) → {} ≠ {}",
                       to_string(code,f),
                       ranges::vec(R1,[&](auto d){ return to_string(code,d); }),
                       ranges::vec(R0,[&](auto d){ return to_string(code,d); }));
  };
  auto msg1 = [&](auto f,auto i,auto r0,auto r1){
    return fmt::format("dim({},{}) → {} ≠ {}",
                       to_string(code,f),
                       i,
                       to_string(code,r1),
                       to_string(code,r0));
  };

  #define CHECK_SHAPE(f0,R0)                                \
    {                                                       \
      REQUIRE_NOTHROW(compile_shape(code,f0));              \
      auto R1 = oitrs(code.statics.shapes,f0);              \
      CHECK_MESSAGE(R1.size() == R0.size(),msg0(f0,R0,R1)); \
      if(R1.size() == R0.size())                            \
        for(auto i:views::indices(R0)){                     \
          auto check = equal(code,R0[i],R1[i]);             \
          CHECK_MESSAGE(check,msg1(f0,i,R0[i],R1[i]));      \
        }                                                   \
    }
  #define CHECK_IMPLICIT_SHAPE(f0,R0)                         \
    {                                                         \
      REQUIRE_NOTHROW(compile_shape(code,f0));                \
      auto R1 = oitrs(code.statics.shapes,f0);                \
      CHECK_MESSAGE(R1.size() == R0.size(),msg0(f0,R0,R1));   \
      if(R1.size() == R0.size())                              \
        for(auto i:views::indices(R0)){                       \
          auto check = equal(code,R0[i],itrdim(code, R1[i])); \
          CHECK_MESSAGE(check,msg1(f0,i,R0[i],R1[i]));        \
        }                                                     \
    }

  auto cZ10 = ncst64(code,10);

  std::vector<id>
    d0{},
    d1{code.builtins.lits.one64},
    d1x1{code.builtins.lits.one64,code.builtins.lits.one64},
    d1x2{code.builtins.lits.one64,code.builtins.lits.two64},
    d2x1{code.builtins.lits.two64,code.builtins.lits.one64},
    d2x2{code.builtins.lits.two64,code.builtins.lits.two64},
    d1x2x2{code.builtins.lits.one64,code.builtins.lits.two64,code.builtins.lits.two64},
    d2x2x1{code.builtins.lits.two64,code.builtins.lits.two64,code.builtins.lits.one64},
    d2x2x2{code.builtins.lits.two64,code.builtins.lits.two64,code.builtins.lits.two64},
    d10{cZ10},
    d10x10{cZ10,cZ10};

  std::vector<id>
    I0 = ranges::vec(d0, [&](auto d){ return itr(code, d).id(); }),
    I1 = ranges::vec(d1, [&](auto d){ return itr(code, d).id(); }),
    I1x1 = ranges::vec(d1x1, [&](auto d){ return itr(code, d).id(); }),
    I1x2 = ranges::vec(d1x2, [&](auto d){ return itr(code, d).id(); }),
    I2x1 = ranges::vec(d2x1, [&](auto d){ return itr(code, d).id(); }),
    I2x2 = ranges::vec(d2x2, [&](auto d){ return itr(code, d).id(); }),
    I1x2x2 = ranges::vec(d1x2x2, [&](auto d){ return itr(code, d).id(); }),
    I2x2x1 = ranges::vec(d2x2x1, [&](auto d){ return itr(code, d).id(); }),
    I2x2x2 = ranges::vec(d2x2x2, [&](auto d){ return itr(code, d).id(); }),
    I10 = ranges::vec(d10, [&](auto d){ return itr(code, d).id(); }),
    I10x10 = ranges::vec(d10x10, [&](auto d){ return itr(code, d).id(); });

  auto
    oR0     = rvar64(code,I0),
    oR1     = ivar64(code,I1),
    oR1x1   = ivar64(code,I1x1),
    oR1x2   = rvar64(code,I1x2),
    oR2x1   = rvar64(code,I2x1),
    oR2x2   = rvar64(code,I2x2),
    oR2x2x2 = rvar64(code,I2x2x2),
    oR10    = rvar64(code,I10),
    oR10x10 = rvar64(code,I10x10);

  auto
    oB2x2 = bvar64(code,I2x2);

  auto cR = rcst64(code,2.0);
  auto sB = code.builtins.lits.top64;

  SUBCASE("cst"){
    CHECK_IMPLICIT_SHAPE(cR,d0);
  }
  SUBCASE("var"){
    CHECK_SHAPE(oR0,I0);
    CHECK_SHAPE(oR1,I1);
    CHECK_SHAPE(oR1x1,I1x1);
    CHECK_SHAPE(oR1x2,I1x2);
    CHECK_SHAPE(oR2x1,I2x1);
    CHECK_SHAPE(oR2x2,I2x2);
  }
  SUBCASE("itr"){
    auto N = ncst64(code,10);
    auto i = itr(code,N);
    compile_shape(code,i);
    CHECK(oext(code,i) == 10);
    CHECK(islot(code.statics.shapes, i) == 0_n);
  }
  SUBCASE("cur"){
    auto N = ncst64(code,10);
    auto i = itr(code,N);
    SUBCASE("0"){
      auto f = cur(code, i);
      compile_shape(code, f);
      CHECK(std::ranges::empty(oitrs(code, f)));
    }
    SUBCASE("1"){
      auto x = rvar64(code, {i});
      auto f = cur(code, x);
      compile_shape(code, f);
      CHECK(std::ranges::empty(oitrs(code, f)));
    }
  }
  SUBCASE("add"){
    auto N = ncst64(code,10);
    auto i = itr(code,N);
    auto c = rcst64(code,2.0);
    auto x = rvar64(code,{i});
    auto o = add(code,{add(code,{x,c}),x});
    compile_shape(code,o);
    CHECK(oext(code,o) == 10);
    CHECK(islot(code.statics.shapes, i) == 0_n);
  }
  SUBCASE("rot"){
    SUBCASE("0"){
      auto d = 10;
      auto N = icst64(code,d);
      auto i = itr(code,N);
      auto j = itr(code,N);
      auto x = rvar64(code,{i,j});
      auto f = rot(code,x,{i,code.builtins.lits.one64});
      compile_shape(code,f);
      CHECK(oext(code,f) == d);
    }
    SUBCASE("1"){
      auto d = 10;
      auto N = icst64(code,d);
      auto i = itr(code,N);
      auto x = rvar64(code,{i});
      auto f = rot(code,x,{code.builtins.lits.one64});
      compile_shape(code,f);
      CHECK(oext(code,f) == 1);
    }
    SUBCASE("2"){

      auto i0 = itr(code, code.builtins.lits.two64);
      auto i1 = itr(code, code.builtins.lits.two64);
      auto x = rvar64(code,{i0, i1});
      auto j = itr(code,code.builtins.lits.one64);
      auto k = itr(code,code.builtins.lits.two64);
      auto f = rot(code,x,{add(code,{j,code.builtins.lits.one64}),k});

      compile_shape(code,f);
      CHECK(orank(code.statics.shapes,x,0_N) == 2);
      CHECK(oext(code,x) == 4);
      CHECK(oext(code,f) == 2);
      CHECK(orank(code.statics.shapes,f,0_N) == 2);
      CHECK(orank(code.statics.shapes,f,1_N) == 2);

      auto x_itrs_0 = oitrs(code.statics.shapes,x,0_N);

      CHECK(x_itrs_0[0] == i0.id());
      CHECK(x_itrs_0[1] == i1.id());

      auto f_itrs_0 = oitrs(code.statics.shapes,f,0_N);
      auto f_itrs_1 = oitrs(code.statics.shapes,f,1_N);
      CHECK(id(itrcast(f_itrs_0[0]).value()) == id(j));
      CHECK(id(itrcast(f_itrs_0[1]).value()) == id(k));
      CHECK(f_itrs_1[0] == i0.id());
      CHECK(f_itrs_1[1] == i1.id());

    }
  }
  SUBCASE("lor"){
    auto N = icst64(code,10), M = icst64(code,5);
    auto i = itr(code,N), j = itr(code,M);
    auto A_ij = bvar64(code,{i,j});
    auto f = lor(code,{con(code,A_ij)});
    compile_shape(code,f);
    CHECK(oext(code,f) == 1);
  }
  SUBCASE("merges"){

    auto i0 = itr(code,cZ10),
      i1 = itr(code,cZ10);

    CHECK_SHAPE(i0,views::one(i0));
    SUBCASE("outer_rot"){
      auto f0 = rot(code,oR10,{i0}),
        f1 = rot(code,oR10,{i0});

      auto g0 = add(code,{f0,f1});

      CHECK_SHAPE(f0,views::one(i0));
      CHECK_SHAPE(g0,views::one(i0));

      auto g1 = rot(code,oR10,{add(code,{i0,code.builtins.lits.one64})});

      CHECK_SHAPE(g1,views::one(i0));
    }
    SUBCASE("inner_rot"){
      auto f0 = rot(code,oR10,{i0}),
        f1 = rot(code,oR10,{i0});
      auto g0 = add(code,{f0,f1});

      CHECK_SHAPE(f0,views::one(i0));
      CHECK_SHAPE(g0,views::one(i0));
    }
    SUBCASE("inner_var"){
      auto x0 = ivar64(code,{i0,i1}),
        x1 = ivar64(code,{i0,i1});
      auto f = add(code,{x0,x1});
      auto g = mul(code,{f,f});
      CHECK_SHAPE(f,views::pin(i0,i1));
      CHECK_SHAPE(g,views::pin(i1,i1));
    }
  }
  SUBCASE("cubemul"){

    auto t = rvar64(code);

    auto i = itr(code,code.builtins.lits.two64);
    auto j = itr(code,code.builtins.lits.two64);
    auto k = itr(code,code.builtins.lits.two64);

    auto A = rvar64(code,{i,j,k});
    auto q = var(code,typ(code, code.builtins.lits.two64, code.builtins.widths.w32));
    auto x = rvar64(code,{k});

    auto X = std::vector<exprfam>{
      {x},
      {der(code,x,t)}
    };

    auto A_p = rot(code,A,{q,j,k});

    auto F_qj = add(code,{con(code,mul(code,{A_p,x}),{k})});

    auto dxdt_j = rot(code,X[1],{j});
    auto G_qj = sub(code,F_qj,dxdt_j);

    {
      compile_shape(code,X[1]);
      CHECK(oext(code,X[1]) == 2);
      CHECK(islot(code.statics.shapes, k) == 0_n);
      auto D = oitrops(code.statics.shapes,X[1]);
      CHECK(D.size() == 1);
      CHECK(D[0] == id(k));
    }

    {
      compile_shape(code,F_qj);
      CHECK(oext(code,F_qj) == 2);
      auto D = oitrops(code.statics.shapes,F_qj);
      CHECK(D.size() == 1);
      CHECK(D[0] == id(j));
    }

    {
      compile_shape(code,G_qj);
      CHECK(oext(code,G_qj) == 2);
    }

  }
  SUBCASE("contractions"){
    auto i0 = itr(code,cZ10);

    CHECK_SHAPE(i0,views::one(i0));

    SUBCASE("trace"){
      auto f = rot(code,oR10x10,{i0,i0});
      CHECK_SHAPE(f,views::one(i0));
    }

  }
  SUBCASE("expansions"){

    std::vector<id> F2x2{
      neg(code,oR2x2),
      sin(code,oR2x2),
      sin(code,sin(code,oR2x2)),
      lnot(code,oB2x2),
      eq(code,oR2x2,oR2x2),
      lor(code,{sB,oB2x2,sB}),
      lt(code,cR,oR2x2),
      land(code,{oB2x2,sB})
    };
    for(auto f:F2x2)
      CHECK_IMPLICIT_SHAPE(f,d2x2);

    std::vector<id> F1x2x2{
      sub(code,oR1,oR2x2)
    };
    for(auto f:F1x2x2)
      CHECK_IMPLICIT_SHAPE(f,d1x2x2);

    std::vector<id> F2x2x1{
      add(code,{oR2x2,oR1,oR0,cR})
    };
    for(auto f:F2x2x1)
      CHECK_IMPLICIT_SHAPE(f,d2x2x1);

  }
  SUBCASE("rfl"){
    SUBCASE("0"){
      auto o0 = oR2x2;
      auto o1 = var(code, typ(code, code.builtins.sets.reals, icst64(code, 32_i)), rfl(code, o0));
      auto o2 = rfl(code, o1);
      auto o3 = rot(code, oR2x2, {code.builtins.lits.zero64, rfl(code, oR1x2)});
      auto o4 = rot(code, oR2x2x2, {code.builtins.lits.zero64, rfl(code, oR2x2)});
      CHECK_IMPLICIT_SHAPE(o0,d2x2);
      CHECK_IMPLICIT_SHAPE(o1,d2x2);
      CHECK_IMPLICIT_SHAPE(o2,d2x2);
      CHECK_IMPLICIT_SHAPE(o3,d1x2);
      CHECK_IMPLICIT_SHAPE(o4,d2x2);
    }
  }
  SUBCASE("put"){
    auto N = ncst64(code,10);
    auto i = itr(code,N), j = itr(code,N);
    SUBCASE("0"){
      auto f = put(code, cur(code, i), N);
      compile_shape(code, f);
      CHECK(std::ranges::empty(oitrs(code, f)));
    }
    SUBCASE("1"){
      auto f = put(code, cur(code, j), N);
      compile_shape(code, f);
      CHECK(std::ranges::empty(oitrs(code, f)));
    }
    SUBCASE("2"){
      auto f = put(code, cur(code, i), cur(code, j));
      compile_shape(code, f);
      CHECK(std::ranges::empty(oitrs(code, f)));
    }
  }
  SUBCASE("noop"){
    auto f = noop(code);
    CHECK_IMPLICIT_SHAPE(f, d0);
  }
  SUBCASE("loop"){
    auto f = loop(code, noop(code));
    CHECK_IMPLICIT_SHAPE(f, d0);
  }
  SUBCASE("exit"){
    auto f = exit(code, code.builtins.lits.zero8);
    CHECK_IMPLICIT_SHAPE(f, d0);
  }
  SUBCASE("fork"){
    auto f = fork(code, code.builtins.lits.zero8, noop(code));
    CHECK_IMPLICIT_SHAPE(f, d0);
  }
  #undef CHECK_SHAPE
  #undef CHECK_IMPLICIT_SHAPE

}

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
