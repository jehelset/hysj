#include<vector>

#include<fmt/core.h>

#include<hysj/codes/code.hpp>
#include<hysj/codes/factories.hpp>
#include<hysj/codes/compile/implicit_iterators.hpp>
#include<hysj/codes/to_string.hpp>
#include<hysj/tools/ranges/utility.hpp>

#include<framework.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

TEST_CASE("codes.compile.implicit_iterators"){

  codes::code code{};

  SUBCASE("detect"){
    auto n = ncst32(code, 8_n);
    auto i = itr(code, n);
    auto x1 = ivar32(code, {i});
    CHECK(!has_implicit_iterator(code, x1));
    auto f0 = rfl(code, x1);
    CHECK(has_implicit_iterator(code, f0));
    auto f1 = der(code, f0, x1);
    CHECK(has_implicit_iterator(code, f1));
    auto f2 = der(code, x1, x1);
    CHECK(!has_implicit_iterator(code, f2));
  }

  SUBCASE("0"){
    SUBCASE("2"){
      auto n0 = ncst32(code, 8_n);
      auto itr0 = itr(code, n0);
      auto var0 = ivar32(code, {itr0});
      auto rfl0 = rfl(code, var0);
      auto f = con(code, lor(code, {rfl0}));
      auto u = concast(compile_implicit_iterators(code, f)).value();

      auto [o0, O1] = args(code, u);
      REQUIRE(std::ranges::size(O1) == 1);
      CHECK(ranges::first(O1) == itr0);
      auto o2 = lorcast(o0).value();
      auto [O3] = args(code, o2);
      REQUIRE(std::ranges::size(O3) == 1);
      auto o4 = ranges::first(O3);
      CHECK(optag_of(o4) == optag::rot);
      auto o5 = rotcast(o4).value();
      auto [o6, O7] = args(code, o5);
      CHECK(o6 == expr(rfl0));
      REQUIRE(std::ranges::size(O7) == 1);
      CHECK(ranges::first(O7) == expr(itr0));
    }
    SUBCASE("3"){
      auto one = code.builtins.lits.one32;
      auto i = itr(code, one);
      auto x = rvar32(code, {i});
      auto t = rvar32(code);
      auto dxdt = der(code, x, t);
      auto c = rcst32(code, 0.1);
      auto f = sub(code, c, dxdt);
      auto rf = rfl(code, f);
      auto p = put(code, rf, f);
      auto u = putcast(compile_implicit_iterators(code, p)).value();
      CHECK(u != p);
      auto [o0, o1] = args(code, u);
      REQUIRE(rotcast(o0));
      auto [o2, O3] = args(code, rotcast(o0).value());
      CHECK(o2 == expr(rf));
      REQUIRE(std::ranges::size(O3) == 1);
      CHECK(ranges::first(O3) == expr(i));
      REQUIRE(subcast(o1));
      auto [o3, O4] = args(code, subcast(o1).value());
      CHECK(o3 == expr(c));
      REQUIRE(std::ranges::size(O4) == 1);
      REQUIRE(rotcast(ranges::first(O4)));
      auto [o5, O6] = args(code, rotcast(ranges::first(O4)).value());
      REQUIRE(std::ranges::size(O6) == 1);
      CHECK(ranges::first(O6) == expr(i));
      REQUIRE(dercast(o5));
      CHECK(dxdt == dercast(o5).value());
    }
    SUBCASE("4"){
      auto n0 = ncst32(code, 8_n);
      auto itr0 = itr(code, n0);
      auto var0 = rvar32(code, {itr0});
      auto fn0 = sub(code, rcst32(code, 0), var0);
      auto rfl0 = rfl(code, fn0);
      auto rfl1 = rfl(code, rfl0);
      auto f = con(code, lor(code, {rfl1}));
      auto u = concast(compile_implicit_iterators(code, f)).value();

      auto [o0, O1] = args(code, u);
      REQUIRE(std::ranges::size(O1) == 1);
      CHECK(ranges::first(O1) == itr0);
    }
    SUBCASE("5"){
      auto n0 = ncst32(code, 8_n);
      auto itr0 = itr(code, n0);
      auto var0 = rvar32(code, {itr0});
      auto fn0 = sub(code, rcst32(code, 0), var0);
      auto rfl0 = rfl(code, fn0);
      auto rfl1 = rfl(code, rfl0);
      auto f = con(code, lor(code, {rfl1}));
      auto u = concast(compile_implicit_iterators(code, f)).value();

      auto [o0, O1] = args(code, u);
      REQUIRE(std::ranges::size(O1) == 1);
      CHECK(ranges::first(O1) == itr0);
    }
    SUBCASE("6"){
      auto n0 = ncst32(code, 8_n);
      auto itr0 = itr(code, n0);
      auto var0 = rvar32(code, {itr0});
      auto fn0 = sub(code, rcst32(code, 0), var0);
      auto rfl0 = rfl(code, fn0);
      auto var1 = ivar32(code, {rfl(code, rfl0)});
      auto f = con(code, lor(code, {var1}));
      auto u = concast(compile_implicit_iterators(code, f)).value();

      auto [o0, O1] = args(code, u);
      REQUIRE(std::ranges::size(O1) == 1);
      CHECK(ranges::first(O1) == itr0);
    }
  }
  SUBCASE("1"){
    auto N = ncst32(code, 8);
    auto i = itr(code, N);
    auto x = rvar32(code, {i});
    auto y = tmp_dup(code, x);
    auto f = put(code, y, x);
    auto o0 = putcast(compile_implicit_iterators(code, f)).value();
    auto [o1, o2] = args(code, o0);
    CHECK(o2 == codes::expr(x));
    REQUIRE(rotcast(o1));
    auto [o3, O4] = args(code, rotcast(o1).value());
    CHECK(o3 == codes::expr(y));
    REQUIRE(std::ranges::size(O4) == 1);
    CHECK(itrcast(ranges::first(O4)) == i);
  }
}

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
