#include<vector>

#include<fmt/core.h>

#include<hysj/codes/code.hpp>
#include<hysj/codes/factories.hpp>
#include<hysj/codes/compile/contractions.hpp>
#include<hysj/codes/compile/rotations.hpp>
#include<hysj/codes/compile/implicit_iterators.hpp>
#include<hysj/codes/to_string.hpp>
#include<hysj/tools/ranges/utility.hpp>

#include<framework.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

TEST_CASE("codes.compile.rotations"){

  codes::code code{};

  auto msg = [&](id f,id g,id h){
    return fmt::format("{} → {} ≠ {}",to_string(code, f),to_string(code, g),
                       to_string(code, h));
  };
  #define CHECK_ROT(f,g,h)             \
    {                                  \
      bool check = equal(code,g,h);    \
      CHECK_MESSAGE(check,msg(f,g,h)); \
    }
  auto n = ncst32(code, 8_n);
  auto j = itr(code, n);
  auto x = nvar32(code, {j});
  auto i = itr(code, n);
  auto y = nvar32(code, {i});
  SUBCASE("0"){
    SUBCASE("0"){
      auto f = i;
      auto g = compile_rotations(code, f);
      CHECK_ROT(f, g, f);
    }
    SUBCASE("1"){
      auto j = itr(code, n);
      auto f = add(code, {i, j});
      auto g = compile_rotations(code, f);
      CHECK_ROT(f, g, f);
    }
    SUBCASE("2"){
      auto f = n;
      auto g = compile_rotations(code, f);
      CHECK_ROT(f, g, f);
    }
    SUBCASE("3"){
      auto f = x;
      auto g = compile_rotations(code, f);
      CHECK_ROT(f, g, f);
    }
  }
  SUBCASE("1"){
    auto f = rot(code, x, {i});
    auto g = compile_rotations(code, f);
    CHECK_ROT(f, g, f);
  }
  SUBCASE("2"){
    auto f = rot(code, x, {add(code, {i, i})});
    auto g = compile_rotations(code, f);
    CHECK_ROT(f, g, f);
  }
  SUBCASE("3"){
    auto j = itr(code, n);
    auto k = itr(code, n);
    auto s = add(code, {i, j});
    auto f0 = rot(code, x, {s});
    auto f1 = rot(code, f0, {k, k});
    auto g = compile_rotations(code, f1);
    auto h = rot(code, x, add(code, {k, k}));
    CHECK_ROT(f1, g, h);
  }
  SUBCASE("4"){
    SUBCASE("0"){
      auto f = rot(code, add(code, {rfl(code, x), x}), {i});
      CHECK_THROWS(compile_rotations(code, f));
    }
    SUBCASE("1"){
      auto k = itr(code, n);
      auto f = rot(code, add(code, {y, y}), {k});
      auto g = compile_rotations(code, f);
      auto h = add(code, {rot(code, y, {k}), rot(code, y, {k})});
      CHECK_ROT(f, g, h);
    }
  }
  SUBCASE("5"){
    auto j = itr(code, n);
    auto k = icst32(code, 2);
    auto f = rot(code, add(code, {rot(code, x, {add(code, {i, j})}), y}), {k, k});
    auto g = compile_rotations(code, f);
    auto h = add(code, {rot(code, x, {add(code, {k, k})}), rot(code, y, {k})});
    CHECK_ROT(f, g, h);
  }
  SUBCASE("7"){
    auto f0 = rot(code, x,  {add(code, {i, code.builtins.lits.one64})}).id();
    f0 = codes::compile_implicit_iterators(code, f0);
    auto g = compile_rotations(code, f0);
    auto h = rot(code, x,  {add(code,{i, code.builtins.lits.one64})});
    CHECK_ROT(f0, g, h);
  }
  SUBCASE("8"){
    auto j = itr(code, n);
    auto y = rot(code, x, {j});
    auto f0 = rot(code, y,  {i});
    auto f1 = rot(code, f0, {i});
    auto g = compile_rotations(code, f1);
    auto h = rot(code, x, {i});
    CHECK_ROT(f1, g, h);
  }
  SUBCASE("9"){
    auto e = 4_n;
    auto c = ncst64(code, e);
    auto i = itr(code, c);
    auto i2 = itr(code, c);
    auto x = ivar64(code, {i});
    auto j = add(code, {i, c});
    auto a0 = rot(
      code,
      add(code,
          {
            x,
            con(code, add(code, {rot(code, x, {i2})}), i2),
            i
          }),
      j).id();
    auto a1 = compile_rotations(code, a0);
    CHECK(a1 != a0);
  }
  SUBCASE("10"){
    auto e = 2_n;
    auto c = ncst64(code, e);
    auto i0 = itr(code, c), i1 = itr(code, c);
    auto x = ivar64(code, {i0, i1});
    auto q = ivar64(code);
    auto z = icst64(code, 0);
    auto f0 = rot(code, x, {q, i1});
    auto f1 = rot(code, f0, {z}).id();
    auto g = compile_rotations(code, f1);
    auto h = rot(code, x, {q, z});
    CHECK_ROT(f1, g, h);
  }
  #undef CHECK_ROT
}

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
