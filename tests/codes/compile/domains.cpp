#include<fmt/core.h>

#include<hysj/codes/code.hpp>
#include<hysj/codes/compile.hpp>
#include<hysj/codes/factories.hpp>
#include<hysj/codes/statics.hpp>
#include<hysj/codes/to_string.hpp>

#include<framework.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

TEST_CASE("codes.compile.domains"){

  codes::code code;

  auto msg = [&](auto f,
                 auto d0_min,auto d0_max,
                 auto d1_min,auto d1_max){
    return fmt::format("{} → [{}, {}] ≠ [{}, {}]", 
                       to_string(code,f),
                       to_string(code,d0_min), to_string(code,d0_max),
                       to_string(code,d1_min), to_string(code,d1_max));
  };
 
  #define CHECK_DOMAIN(f0,r0_min,r0_max)                                   \
    {                                                                      \
      REQUIRE_NOTHROW(compile(code,f0));                                   \
      REQUIRE(has_odomain(code.statics.domains, f0));                      \
      auto [r1_min, r1_max] = *odomain(code.statics.domains,f0);           \
      auto check = equal(code,r0_min,r1_min) && equal(code,r0_max,r1_max); \
      CHECK_MESSAGE(check, msg(f0,r1_min,r1_max,r0_min,r0_max));           \
    }

  #define CHECK_EQUAL_DOMAIN(f0,f1)                                        \
    {                                                                      \
      REQUIRE_NOTHROW(compile(code,f0));                                   \
      REQUIRE_NOTHROW(compile(code,f1));                                   \
      REQUIRE(has_odomain(code.statics.domains, f0));                      \
      REQUIRE(has_odomain(code.statics.domains, f1));                      \
      auto [r0_min, r0_max] = *odomain(code.statics.domains,f0);           \
      auto [r1_min, r1_max] = *odomain(code.statics.domains,f1);           \
      auto check = equal(code,r0_min,r1_min) && equal(code,r0_max,r1_max); \
      CHECK_MESSAGE(check,msg(f0,r0_min,r0_max,r1_min,r1_max));            \
    }

  SUBCASE("lit"){
    auto oN = nlit(code,0);
    CHECK_DOMAIN(oN, oN, oN);
  }
  SUBCASE("typ"){
    CHECK_DOMAIN(code.builtins.types.b8, code.builtins.lits.bot8, code.builtins.lits.top8);
    CHECK_DOMAIN(code.builtins.types.n32, code.builtins.lits.zero32, code.builtins.lits.inf32);
    CHECK_DOMAIN(code.builtins.types.i64, code.builtins.lits.neginf64, code.builtins.lits.inf64);
    CHECK_DOMAIN(code.builtins.types.r128, code.builtins.lits.neginf128, code.builtins.lits.inf128);
    {
      auto n = ncst8(code, 4);
      auto t = typ8(code, n);
      CHECK_DOMAIN(t, code.builtins.lits.zero8, n);
    }
    {
      auto n = icst16(code, 4);
      auto t = typ16(code, n);
      CHECK_DOMAIN(t, code.builtins.lits.zero16, n);
    }
  }
  SUBCASE("itr"){
    SUBCASE("0"){
      auto n = ncst32(code, 5);
      auto i = itr(code, n);
      CHECK_DOMAIN(i, code.builtins.lits.zero32, n);
    }
    SUBCASE("1"){
      auto n = ncst16(code, 5);
      auto i = itr(code, n);
      CHECK_DOMAIN(i, code.builtins.lits.zero16, n);
    }
    SUBCASE("2"){
      auto n = ncst8(code, 5);
      auto i = itr(code, n);
      CHECK_DOMAIN(i, code.builtins.lits.zero8, n);
    }
    SUBCASE("3"){
      auto n = ncst16(code, 5);
      auto i = itr(code, n);
      auto j = itr_dup(code, i);
      CHECK_DOMAIN(j, code.builtins.lits.zero16, n);
    }
  }
  SUBCASE("con"){

  }
  SUBCASE("cst"){
    SUBCASE("0"){
      auto x = bcst64(code, 0);
      CHECK_DOMAIN(x, x, x);
    }
    SUBCASE("1"){
      auto x = ncst64(code,0);
      CHECK_DOMAIN(x, x, x);
    }
    SUBCASE("2"){
      auto x = icst64(code,0);
      CHECK_DOMAIN(x, x, x);
    }
    SUBCASE("3"){
      auto x = rcst64(code,0);
      CHECK_DOMAIN(x, x, x);
    }
  }
  SUBCASE("var"){
    SUBCASE("0"){
      auto x = bvar64(code);
      CHECK_DOMAIN(x, code.builtins.lits.bot64, code.builtins.lits.top64);
    }
    SUBCASE("1"){
      auto x = nvar32(code);
      CHECK_DOMAIN(x, code.builtins.lits.zero32, code.builtins.lits.inf32);
    }
    SUBCASE("2"){
      auto x = ivar16(code);
      CHECK_DOMAIN(x, code.builtins.lits.neginf16, code.builtins.lits.inf16);
    }
    SUBCASE("3"){
      auto x = rvar8(code);
      CHECK_DOMAIN(x, code.builtins.lits.neginf8, code.builtins.lits.inf8);
    }
    SUBCASE("4"){
      auto n = ncst128(code, 8);
      auto x = var128(code, n);
      CHECK_DOMAIN(x, code.builtins.lits.zero128, n);
    }
  }
  SUBCASE("cvt"){
    SUBCASE("0"){
      auto x = ivar32(code);
      auto y = icvt64(code, x);
      CHECK_DOMAIN(y, code.builtins.lits.neginf32, code.builtins.lits.inf32);
    }
    SUBCASE("1"){
      auto x = ivar32(code);
      auto y = rcvt32(code, x);
      CHECK_DOMAIN(y, code.builtins.lits.neginf32, code.builtins.lits.inf32);
    }
  }
  SUBCASE("rot"){
    SUBCASE("0"){
      auto n = icst32(code, 10);
      auto j = itr(code, n);
      auto x = ivar32(code, {j});
      auto i = itr(code, n);
      auto f = rot(code, x, {add(code, {i, code.builtins.lits.one32})});
      CHECK_EQUAL_DOMAIN(f, x);
    }
  }
  SUBCASE("neg"){
    SUBCASE("0"){
      auto x = icst32(code, 10);
      auto f = neg(code, x);
      CHECK_DOMAIN(f, f, f);
    }
    SUBCASE("1"){
      auto n = icst32(code, 10);
      auto x = ivar32(code, {itr(code, n)});
      auto f = neg(code, x);
      auto d_min = neg(code, code.builtins.lits.inf32);
      auto d_max = neg(code, code.builtins.lits.neginf32);
      CHECK_DOMAIN(f, d_min, d_max);
    }
  }
  SUBCASE("sin"){
    auto x = rvar32(code);
    auto f = sin(code, x);
    CHECK_DOMAIN(f, code.builtins.lits.negone32, code.builtins.lits.one32);
  }
  SUBCASE("mod"){
    SUBCASE("0"){
      auto a = ncst32(code, 1);
      auto b = ncst32(code, 2);
      auto f = mod(code, a, b);
      CHECK_EQUAL_DOMAIN(f, a);
    }
    SUBCASE("1"){
      auto a = ncst32(code, 3);
      auto b = ncst32(code, 2);
      auto f = mod(code, a, b);
      auto z = code.builtins.lits.zero32;
      CHECK_DOMAIN(f, z, b);
    }
    SUBCASE("1"){
      auto a = rvar16(code);
      auto b = ncst32(code, 2);
      auto f = mod(code, a, b);
      auto z = code.builtins.lits.zero32;
      CHECK_DOMAIN(f, z, b);
    }
  }
  SUBCASE("fct"){
    auto n = icst32(code, 10);
    auto x = var32(code, n);
    auto f = fct(code, x);
    auto d_min = fct(code, code.builtins.lits.zero32);
    auto d_max = fct(code, n);
    CHECK_DOMAIN(f, d_min, d_max);
  }
  SUBCASE("add"){
    SUBCASE("0"){
      auto x = rvar32(code);
      auto f = add(code, {x, x});
      auto ninf = code.builtins.lits.neginf32;
      auto inf = code.builtins.lits.inf32;
      auto d_min = add(code, {ninf, ninf});
      auto d_max = add(code, {inf, inf});
      CHECK_DOMAIN(f, d_min, d_max);
    }
    SUBCASE("1"){
      auto n = icst32(code, 10);
      auto x = var32(code, n);
      auto f = add(code, {x, x});
      auto zero = code.builtins.lits.zero32;
      auto d_min = add(code, {zero, zero});
      auto d_max = add(code, {n, n});
      CHECK_DOMAIN(f, d_min, d_max);
    }
  }
  SUBCASE("sub"){
    SUBCASE("0"){
      auto x = rvar32(code);
      auto f = sub(code, x, {x});
      auto ninf = code.builtins.lits.neginf32;
      auto inf = code.builtins.lits.inf32;
      auto d_min = sub(code, ninf, {inf});
      auto d_max = sub(code, inf, {ninf});
      CHECK_DOMAIN(f, d_min, d_max);
    }
    SUBCASE("1"){
      auto n = icst32(code, 10);
      auto x = var32(code, n);
      auto f = sub(code, x, {x});
      auto zero = code.builtins.lits.zero32;
      auto d_min = sub(code, zero, {n});
      auto d_max = sub(code, n, {zero});
      CHECK_DOMAIN(f, d_min, d_max);
    }
  }
  SUBCASE("mul"){
    auto n = icst32(code, 10);
    auto x = var32(code, n);
    auto f = mul(code, {x, x});
    auto d_min = code.builtins.lits.neginf32;
    auto d_max = code.builtins.lits.inf32;
    CHECK_DOMAIN(f, d_min, d_max);
  }
  SUBCASE("div"){
    auto x = rvar32(code);
    auto f = div(code, x, x);
    auto d_min = code.builtins.lits.neginf32;
    auto d_max = code.builtins.lits.inf32;
    CHECK_DOMAIN(f, d_min, d_max);
  }
  SUBCASE("pow"){
    auto x = rvar32(code);
    auto f = pow(code, x, x);
    auto d_min = code.builtins.lits.neginf32;
    auto d_max = code.builtins.lits.inf32;
    CHECK_DOMAIN(f, d_min, d_max);
  }
  SUBCASE("log"){
    auto x = rvar32(code);
    auto f = log(code, x, x);
    auto d_min = code.builtins.lits.neginf32;
    auto d_max = code.builtins.lits.inf32;
    CHECK_DOMAIN(f, d_min, d_max);
  }
  SUBCASE("der"){
    auto t = rvar32(code);
    auto x = rvar32(code);
    auto dxdt = der(code, x, t);
    CHECK_EQUAL_DOMAIN(x, dxdt);
  }
  SUBCASE("bnot"){
    auto n = ncst32(code, 10);
    auto x = var32(code, n);
    auto f = bnot(code, x);
    auto zero = code.builtins.lits.zero32;
    auto d_min = bnot(code, n);
    auto d_max = bnot(code, zero);
    CHECK_DOMAIN(f, d_min, d_max);
  }
  SUBCASE("blsh"){
    auto n = ncst32(code, 10);
    auto x = var32(code, n);
    auto y = var32(code, n);
    auto f = blsh(code, x, y);
    auto zero = code.builtins.lits.zero32;
    auto d_min = blsh(code, zero, zero);
    auto d_max = blsh(code, n, n);
    CHECK_DOMAIN(f, d_min, d_max);
  }
  SUBCASE("brsh"){
    auto n = ncst32(code, 10);
    auto x = var32(code, n);
    auto y = var32(code, n);
    auto f = brsh(code, x, y);
    auto zero = code.builtins.lits.zero32;
    auto d_min = brsh(code, zero, n);
    auto d_max = brsh(code, n, zero);
    CHECK_DOMAIN(f, d_min, d_max);
  }
  SUBCASE("bor"){
    auto n = ncst32(code, 10);
    auto x = var32(code, n);
    auto y = var32(code, n);
    auto f = bor(code, {x, y});
    auto d_min = code.builtins.lits.zero32;
    auto d_max = code.builtins.lits.inf32;
    CHECK_DOMAIN(f, d_min, d_max);
  }
  SUBCASE("band"){
    auto n = ncst32(code, 10);
    auto x = var32(code, n);
    auto y = var32(code, n);
    auto f = bor(code, {x, y});
    auto d_min = code.builtins.lits.zero32;
    auto d_max = code.builtins.lits.inf32;
    CHECK_DOMAIN(f, d_min, d_max);
  }
  SUBCASE("bxor"){
    auto n = ncst32(code, 10);
    auto x = var32(code, n);
    auto y = var32(code, n);
    auto f = bor(code, {x, y});
    auto d_min = code.builtins.lits.zero32;
    auto d_max = code.builtins.lits.inf32;
    CHECK_DOMAIN(f, d_min, d_max);
  }
  SUBCASE("eq"){
    auto x = ivar32(code), y = ivar32(code);
    auto bot = code.builtins.lits.bot32;
    auto top = code.builtins.lits.top32;
    auto f = eq(code, x, y);
    CHECK_DOMAIN(f, bot, top);
  }
  SUBCASE("lt"){
    auto x = rvar16(code), y = rvar16(code);
    auto bot = code.builtins.lits.bot16;
    auto top = code.builtins.lits.top16;
    auto f = lt(code, x, y);
    CHECK_DOMAIN(f, bot, top);
  }
  SUBCASE("lnot"){
    auto x = bvar32(code);
    auto f = bnot(code, x);
    auto bot = code.builtins.lits.bot32;
    auto top = code.builtins.lits.top32;
    auto d_min = bnot(code, top);
    auto d_max = bnot(code, bot);
    CHECK_DOMAIN(f, d_min, d_max);
  }
  SUBCASE("land"){
    auto x = bvar8(code);
    auto y = bvar8(code);
    auto f = land(code, {x, y});
    auto bot = code.builtins.lits.bot8;
    auto top = code.builtins.lits.top8;
    auto d_min = land(code, {bot, bot});
    auto d_max = land(code, {top, top});
    CHECK_DOMAIN(f, d_min, d_max);
  }
  SUBCASE("lor"){
    auto x = bvar8(code);
    auto y = bvar8(code);
    auto f = lor(code, {x, y});
    auto bot = code.builtins.lits.bot8;
    auto top = code.builtins.lits.top8;
    auto d_min = lor(code, {bot, bot});
    auto d_max = lor(code, {top, top});
    CHECK_DOMAIN(f, d_min, d_max);
  }
  SUBCASE("sel"){
    auto x = bvar8(code);
    auto y = ivar8(code);
    auto z = ivar16(code);
    auto f = sel(code, x, {y, z});
    auto d_min = code.builtins.lits.neginf16;
    auto d_max = code.builtins.lits.inf16;
    CHECK_DOMAIN(f, d_min, d_max);
  }
  SUBCASE("put"){
    auto x = ivar16(code);
    auto y = ivar16(code);
    auto f = put(code, x, y);
    auto d_min = code.builtins.lits.zero8;
    auto d_max = code.builtins.lits.inf8;
    CHECK_DOMAIN(f, d_min, d_max);
  }
  SUBCASE("noop"){
    auto f = noop(code);
    auto d_min = code.builtins.lits.zero8;
    auto d_max = code.builtins.lits.inf8;
    CHECK_DOMAIN(f, d_min, d_max);
  }
  SUBCASE("loop"){
    auto f = loop(code, noop(code));
    auto d_min = code.builtins.lits.zero8;
    auto d_max = code.builtins.lits.inf8;
    CHECK_DOMAIN(f, d_min, d_max);
  }
  SUBCASE("exit"){
    auto f = exit(code, code.builtins.lits.zero8);
    auto d_min = code.builtins.lits.zero8;
    auto d_max = code.builtins.lits.inf8;
    CHECK_DOMAIN(f, d_min, d_max);
  }
  SUBCASE("fork"){
    auto g = noop(code);
    auto f = fork(code, code.builtins.lits.zero8, g);
    auto d_min = code.builtins.lits.zero8;
    auto d_max = code.builtins.lits.inf8;
    CHECK_DOMAIN(f, d_min, d_max);
  }
  SUBCASE("then"){
    auto x = ivar8(code);
    auto y = ivar8(code);
    auto p = put(code, x, y);
    auto f = then(code, {p});
    auto d_min = code.builtins.lits.zero8;
    auto d_max = code.builtins.lits.inf8;
    CHECK_DOMAIN(f, d_min, d_max);
  }
  SUBCASE("when"){
    auto x = ivar16(code);
    auto y = ivar16(code);
    auto p = put(code, x, y);
    auto f = when(code, {p});
    auto d_min = code.builtins.lits.zero8;
    auto d_max = code.builtins.lits.inf8;
    CHECK_DOMAIN(f, d_min, d_max);
  }
  #undef CHECK_DOMAIN

}


} //hysj::codes
#include<hysj/tools/epilogue.hpp>
