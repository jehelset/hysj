#include<vector>

#include<fmt/core.h>

#include<hysj/codes/code.hpp>
#include<hysj/codes/factories.hpp>
#include<hysj/codes/compile/contractions.hpp>
#include<hysj/codes/to_string.hpp>
#include<hysj/tools/ranges/utility.hpp>

#include<framework.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

TEST_CASE("codes.compile.contractions"){

  codes::code code{};

  auto msg = [&](auto f,auto g, auto h){
    return fmt::format("{} → {} ≠ {}",
                       to_string(code,f),
                       to_string(code,h),
                       to_string(code,g));
  };

  auto is_itrdup_for = [&](id i,id j){
    auto o0 = itrcast(i).value();
    auto [o1] = args(code, o0);
    auto o4 = rflcast(o1).value();
    auto [o5] = args(code, o4);
    return o5.id() == j;
  };


  #define CHECK_CONTRACTION(f, g)               \
    {                                           \
      auto h = compile_contractions(code, f);   \
      auto check = equal(code, g, h);           \
      CHECK_MESSAGE(check, msg(f, g, h));       \
    }

  #define CHECK_NO_CONTRACTION(f)               \
    CHECK_CONTRACTION(f, f);

  SUBCASE("0"){
    auto n0 = ncst32(code, 8_n);
    auto i0 = itr(code, n0);
    auto x0 = nvar32(code, {i0});
    SUBCASE("0"){
      auto f0 = con(code, add(code, {x0}));
      auto y0 = rfl(code, f0);
      CHECK_NO_CONTRACTION(y0);
    }
    SUBCASE("1"){
      auto f0 = con(code, add(code, {x0}), i0);
      auto y0 = rfl(code, f0);
      CHECK_NO_CONTRACTION(y0);
    }
  }
  SUBCASE("2"){
    auto n0 = ncst32(code, 8_n), n1 = ncst32(code, 16_n);
    auto i0 = itr(code, n0), i1 = itr(code, n1);
    auto x0 = nvar32(code, {i0, i1});
    SUBCASE("0"){
      auto f0 = con(code, add(code, {x0}));
      auto h0 = concast(compile_contractions(code, f0));
      REQUIRE(h0);
      auto [o0, O1] = args(code, *h0);
      REQUIRE(std::ranges::empty(O1));
      auto o2 = addcast(o0);
      REQUIRE(o2);
      auto [O3] = args(code, *o2);
      REQUIRE(std::ranges::size(O3) == 1);
      auto o4 = rotcast(ranges::first(O3));
      REQUIRE(o4);
      auto [o5, O6] = args(code, *o4);
      CHECK(o5.id() == x0.id());
      REQUIRE(std::ranges::size(O6) == 2);
      for(auto [ix,o7]:std::views::zip(std::vector{i0, i1}, O6)){
        auto o7_ = itrcast(o7).value();
        auto [o8] = args(code, o7_);
        auto o11 = rflcast(o8);
        REQUIRE(o11);
        auto [o12] = args(code, *o11);
        CHECK(o12.id() == ix.id());
      }
    }
    SUBCASE("1"){

      for(auto i2:std::array{i0, i1}){
        auto f0 = con(code, add(code, {x0}), {i2});
        auto h0 = concast(compile_contractions(code, f0));
        REQUIRE(h0);

        auto [o0, O1] = args(code, *h0);
        REQUIRE(std::ranges::size(O1) == 1);

        auto o2 = ranges::first(O1);
        CHECK(o2 != i2);

        auto [o3] = args(code, o2);
        auto o6 = rflcast(o3);
        REQUIRE(o6);
        auto [o7] = args(code, *o6);
        CHECK(o7.id() == i2.id());
      }
    }
    SUBCASE("2"){
      auto f0 = con(code, add(code, {x0}), {i0, i1});
      auto h0 = concast(compile_contractions(code, f0));
      REQUIRE(h0);

      auto [o0, O1] = args(code, *h0);
      REQUIRE(std::ranges::size(O1) == 2);

      for(auto [o2, i2]:std::views::zip(O1, std::vector{i0, i1})){
        CHECK(o2 != i2);
        auto [o3] = args(code, o2);
        auto o6 = rflcast(o3);
        REQUIRE(o6);
        auto [o7] = args(code, *o6);
        CHECK(o7.id() == i2.id());
      }
    }

  }
  SUBCASE("3"){
    auto n0 = ncst32(code, 16_n);
    auto i0 = itr(code, n0);
    auto x0 = nvar32(code, {i0});
    SUBCASE("0"){
      auto g0 = con(code, add(code, {x0}), i0);
      auto f0 = mul(code, {i0, g0});
      auto h0 = mulcast(compile_contractions(code, f0)).value();
      auto [O0] = args(code, h0);
      REQUIRE(std::ranges::size(O0) == 2);
      CHECK(ranges::first(O0).id() == i0.id());
      auto o1 = concast(ranges::last(O0)).value();
      auto O2 = arg(code, o1, 1_N);
      REQUIRE(std::ranges::size(O2) == 1);
      CHECK(ranges::last(O2).id() != i0.id());
    }
    SUBCASE("1"){
      auto g0 = con(code, add(code, {x0}), i0);
      auto f0 = mul(code, {g0, i0});
      auto h0 = mulcast(compile_contractions(code, f0)).value();
      auto [O0] = args(code, h0);
      REQUIRE(std::ranges::size(O0) == 2);
      CHECK(ranges::last(O0).id() == i0.id());
      auto o1 = concast(ranges::first(O0)).value();
      auto O2 = arg(code, o1, 1_N);
      REQUIRE(std::ranges::size(O2) == 1);
      CHECK(ranges::last(O2).id() != i0.id());
    }
    SUBCASE("2"){
      auto g0 = con(code, add(code, {x0}));
      auto f0 = mul(code, {g0, i0});
      auto h0 = mulcast(compile_contractions(code, f0)).value();
      auto [O0] = args(code, h0);
      REQUIRE(std::ranges::size(O0) == 2);
      CHECK(ranges::last(O0).id() == i0.id());
      auto o1 = concast(ranges::first(O0)).value();
      auto [o2, O3] = args(code, o1);
      CHECK(std::ranges::empty(O3));
      auto o4 = addcast(o2).value();
      auto [O5] = args(code, o4);
      REQUIRE(std::ranges::size(O5) == 1);
      auto o6 = rotcast(ranges::first(O5)).value();
      auto [o7, O8] = args(code, o6);
      CHECK(o7.id() == x0.id());
      REQUIRE(std::ranges::size(O8) == 1);
      auto o9 = itrcast(ranges::first(O8)).value();
      auto [o10] = args(code, o9);
      auto o13 = rflcast(o10).value();
      auto [o14] = args(code, o13);
      CHECK(o14.id() == i0.id());
    }
    SUBCASE("3"){
      auto g0 = rot(code, x0, add(code, {i0, code.builtins.lits.one32}));
      auto f0 = con(code, add(code, {g0}), i0);
      auto h0 = concast(compile_contractions(code, f0)).value();
      auto [o0, O1] = args(code, h0);
      auto o2 = addcast(o0).value();
      REQUIRE(std::ranges::size(O1) == 1);
      {
        auto o3 = itrcast(O1[0]).value();
        auto [o4] = args(code, o3);
        auto o7 = rflcast(o4).value();
        auto [o8] = args(code, o7);
        CHECK(o8.id() == i0.id());
      }
      auto [O3] = args(code, o2);
      REQUIRE(std::ranges::size(O3) == 1);
      auto o4 = rotcast(ranges::first(O3)).value();
      auto [o5, O6] = args(code, o4);
      auto o7 = rotcast(o5).value();
      auto [o8, O9] = args(code, o7);
      auto o10 = varcast(o8).value();
      CHECK(o10 == x0);
      REQUIRE(std::ranges::size(O9) == 1);
      auto o9_ = itrcast(ranges::first(O9)).value();

      auto [o11] = args(code, o9_);
      auto o14 = rflcast(o11).value();
      auto [o15] = args(code, o14);
      CHECK(o15.id() == i0.id());
    }
      
  }
  SUBCASE("4"){
    auto n0 = ncst32(code, 4_n);
    auto i0 = itr(code, n0), i1 = itr(code, n0);
    auto x0 = nvar32(code, {i0}), y0 = nvar32(code, {i1});

    auto g0 = rot(code, x0, {y0});
    auto f0 = con(code, add(code, g0), i0);
    auto h0 = concast(compile_contractions(code, f0)).value();

    auto [o0, O1] = args(code, h0);

    REQUIRE(std::ranges::size(O1) == 1);
    CHECK(ranges::first(O1) != i0);

    auto o2 = addcast(o0).value();
    auto [O3] = args(code, o2);
    REQUIRE(std::ranges::size(O3) == 1);

    auto o4 = rotcast(ranges::first(O3)).value();
    auto [o5, O6] = args(code, o4);

    REQUIRE(std::ranges::size(O6) == 1);
  }
  SUBCASE("5"){
    auto n = ncst64(code, 4_n);
    auto i = itr(code, n);
    auto x = ivar64(code, {i});
    auto f = con(code, add(code, {x}), i);
    auto o0 = concast(compile_contractions(code, f)).value();
    auto [o1, O2] = args(code, o0);
    REQUIRE(std::ranges::size(O2) == 1);
    CHECK(is_itrdup_for(ranges::first(O2), i));
    auto o3 = addcast(o1).value();
    auto [O4] = args(code, o3);
    REQUIRE(std::ranges::size(O4) == 1);
    auto o5 = rotcast(ranges::first(O4)).value();
    auto [o6, O7] = args(code, o5);
    CHECK(o6.id() == x.id());
    REQUIRE(std::ranges::size(O7) == 1);
    CHECK(ranges::first(O2).id() == ranges::first(O7).id());
  }
  SUBCASE("6"){
    auto one = code.builtins.lits.one32;
    auto two = code.builtins.lits.two32;

    auto i0 = itr(code, two);
    auto i1 = itr(code, two);
    auto x = rvar32(code, {i0,i1});
    auto j = itr(code,one);
    auto k = itr(code,two);

    auto f = rot(code,x,{add(code,{j,one}),k});
    auto [t, b] = obs(code, f);
    CHECK_NO_CONTRACTION(t);
  }
  #undef CHECK_NO_CONTRACTION
  #undef CHECK_CONTRACTION

}

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
