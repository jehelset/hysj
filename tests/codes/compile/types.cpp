#include<fmt/core.h>

#include<hysj/codes/code.hpp>
#include<hysj/codes/compile/types.hpp>
#include<hysj/codes/factories.hpp>
#include<hysj/codes/to_string.hpp>

#include<framework.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

TEST_CASE("codes.compile.type"){

  codes::code code;

  auto msg = [&](auto f,auto r0,auto r1){
    return fmt::format("{} → {} ≠ {}", to_string(code,f),
                       to_string(code,r1), to_string(code,r0));
  };
  auto msg1 = [&](auto f,auto w0,auto w1){
    return fmt::format("{} → {} ≠ {}", to_string(code,f),
                       w1, w0);
  };

  #define CHECK_OSET(f0,r0)                        \
    {                                              \
      compile_type(code,f0);                       \
      auto r1 = code.builtins.sets[oset(code,f0)]; \
      auto check = equal(code,r0,r1);              \
      CHECK_MESSAGE(check,msg(f0,r0,r1));          \
    }
  #define CHECK_OWIDTH(f0,w0)                         \
    {                                                 \
      auto w1 = owidth(code,f0);                      \
      auto check = (w0 == w1);                        \
      CHECK_MESSAGE(check,msg1(f0,w0,w1));            \
    }
  #define CHECK_RSET(f0,r0,s0)                     \
    {                                              \
      auto s1 = code.builtins.sets[rset(code,r0)]; \
      auto check = equal(code,s0,s1);              \
      CHECK_MESSAGE(check,msg(f0,s0,s1));          \
    }
  #define CHECK_RWIDTH(f0,r0,w0)                      \
    {                                                 \
      auto w1 = rwidth(code,r0);                      \
      auto check = (w0 == w1);                        \
      CHECK_MESSAGE(check,msg1(f0,w0,w1));            \
    }
  #define CHECK_DOM(f0,r0,r0_min,r0_max)              \
    {                                                 \
      compile_type(code,f0);                          \
      auto r1 = odom(code,f0);                        \
      auto check0 = equal(code,r0,r1);                \
      CHECK_MESSAGE(check0,msg(f0,r0,r1));            \
      auto r1_min = omin(code,f0);                    \
      auto check1 = equal(code,r0_min,r1_min);        \
      CHECK_MESSAGE(check1,msg(f0,r0_min,r1_min));    \
      auto r1_max = omax(code,f0);                    \
      auto check2 = equal(code,r1_max,r0_max);        \
      CHECK_MESSAGE(check2,msg(f0,r0_max,r1_max));    \
    }

  SUBCASE("cst"){
    auto oB = bcst64(code,true);
    CHECK_OSET(oB,code.builtins.sets.booleans);
    auto oZ = icst64(code,0);
    CHECK_OSET(oZ,code.builtins.sets.integers);
    auto oR = rcst64(code,0.0);
    CHECK_OSET(oR,code.builtins.sets.reals);
  }
  SUBCASE("itr"){
    auto oZ = itr(code, icst64(code, 10));
    CHECK_OSET(oZ, code.builtins.sets.integers);
  }
  SUBCASE("var"){
    auto oB = bvar64(code);
    CHECK_OSET(oB,code.builtins.sets.booleans);
    auto oZ = ivar64(code);
    CHECK_OSET(oZ,code.builtins.sets.integers);
    auto oR = rvar64(code);
    CHECK_OSET(oR,code.builtins.sets.reals);
  }
  SUBCASE("cur"){
    auto oZ = itr(code, icst64(code, 10));
    auto fZ = cur(code, oZ);
    CHECK_OSET(fZ, code.builtins.sets.integers);
    auto oR = rvar64(code, {oZ});
    auto fR = cur(code, oR);
    CHECK_OSET(fR, code.builtins.sets.reals);
  }
  SUBCASE("rot"){
    auto N = icst64(code,10);
    auto i = itr(code,N);
    auto oZ = rot(code,ivar64(code,{itr(code, N)}),{i});
    CHECK_OSET(oZ,code.builtins.sets.integers);
  }
  SUBCASE("add"){
    auto N = icst64(code,10);
    auto i = itr(code,N);
    auto oR = rvar64(code,{itr(code, N)});
    auto oZ = rot(code,oR,{i});
    auto o = add(code,{oR,oZ});
    CHECK_OSET(o,code.builtins.sets.reals);
  }
  SUBCASE("mod"){
    auto a = icst32(code,0);
    auto b = icst16(code,1);
    auto f = mod(code, a, b);
    CHECK_OSET(f,code.builtins.sets.integers);
    auto [r0,r1] = rels(code, f);
    CHECK_OWIDTH(f,32);
    CHECK_RWIDTH(f,r0,32);
    CHECK_RWIDTH(f,r1,32);
  }
  SUBCASE("lims"){
    SUBCASE("cst"){
      SUBCASE("Ｂ"){
        auto D = code.builtins.sets.booleans;
        auto x = var(code,typ(code, D, code.builtins.widths.w8));
        CHECK_DOM(x,D,code.builtins.lits.bot8,code.builtins.lits.top8);
      }
      SUBCASE("Ｚ"){
        auto D = code.builtins.sets.integers;
        auto x = var(code, typ(code, D, code.builtins.widths.w16));
        CHECK_DOM(x,D,code.builtins.lits.neginf16,code.builtins.lits.inf16);
      }
      SUBCASE("Ｒ"){
        auto D = code.builtins.sets.reals;
        auto x = var(code, typ(code, D, code.builtins.widths.w32));
        CHECK_DOM(x,D,code.builtins.lits.neginf32,code.builtins.lits.inf32);
      }
      auto D_min = code.builtins.lits.zero64;
      auto D_max = icst64(code,10);
      auto x = var(code, typ(code, D_max, code.builtins.widths.w64));
      CHECK_DOM(x,D_max,D_min,D_max);
    }
  }
  SUBCASE("rfl"){

    auto o0 = icst64(code,10);
    auto o1 = itr(code,o0);
    auto o2 = rvar64(code,{o1});
    auto o3 = var(code,rfl(code, o2),{o1});
    CHECK_DOM(o3, code.builtins.sets.reals, code.builtins.lits.neginf64, code.builtins.lits.inf64);

    auto o4 = rfl(code,o3);
    CHECK_DOM(o4, code.builtins.sets.reals, code.builtins.lits.neginf64, code.builtins.lits.inf64);

    auto o8 = itr(code, o0),
      o9 = itr(code, o0),
      o10 = itr(code, o0);
    auto o5 = nvar64(code,{o8, o9});
    auto o6 = nvar64(code,{o10});
    auto o7 = rot(code, o5, {code.builtins.lits.zero64, rfl(code, o6)});
    CHECK_DOM(o7, code.builtins.sets.naturals, code.builtins.lits.zero64, code.builtins.lits.inf64);

  }
  SUBCASE("loops"){
    auto O0 = std::to_array<id>({
        noop(code),
        loop(code, noop(code)),
        exit(code, code.builtins.lits.zero8)
      });
    for(auto o0:O0)
      CHECK_DOM(o0, code.builtins.sets.naturals, code.builtins.lits.zero8, code.builtins.lits.inf8);
  }
  SUBCASE("fork"){
    std::array O0 = std::to_array({
        fork(code, code.builtins.lits.zero8, {code.builtins.noop}),
        fork(code, code.builtins.lits.one16, {code.builtins.noop, code.builtins.noop}),
        fork(code, code.builtins.lits.two32, {code.builtins.noop, code.builtins.noop}),
        fork(code, code.builtins.lits.two64, {code.builtins.noop, code.builtins.noop})
    });
    for(auto o0:O0){
      CHECK_DOM(o0, code.builtins.sets.naturals, code.builtins.lits.zero8, code.builtins.lits.inf8);
      auto [r1, R2] = rels(code, o0);
      auto [o1, O2] = args(code, o0);
      CHECK_RSET(o0, r1, code.builtins.sets.naturals);
      CHECK_RWIDTH(o0, r1, 32);
      for(auto o2:O2){
        CHECK_OSET(o2, code.builtins.sets.naturals);
        CHECK_OWIDTH(o2, 8);
      }
    }
  }
  #undef CHECK_OSET
  #undef CHECK_RSET
  #undef CHECK_DOM

}


} //hysj::codes
#include<hysj/tools/epilogue.hpp>
