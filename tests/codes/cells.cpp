#include<algorithm>
#include<cmath>
#include<initializer_list>
#include<limits>
#include<random>
#include<ranges>
#include<utility>
#include<vector>

#include"framework.hpp"

#include<hysj/codes.hpp>
#include<hysj/tools/types.hpp>
#include<hysj/tools/ranges/span.hpp>
#include<hysj/tools/functional/apply.hpp>
#include<hysj/tools/ranges/utility.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

TEST_CASE("codes.cells"){

  using namespace factories;

  static constexpr constant_t<32_n> width{};
  static constexpr bool enable_reals = has_cell<set::reals, width()>;

  code code{};

  auto w = code.builtins.widths[width];
  auto n0 = 16,
    n1 = 8,
    n2 = 1;
  auto e0 = ncst(code, n0, w),
    e1 = ncst(code, n1, w),
    e2 = ncst(code, n2, w);
  auto i0 = itr(code, e0),
    i1 = itr(code, e1),
    i2 = itr(code, e2);

  SUBCASE("integers"){
    auto x0 = bvar(code, w, {i0}),
      x1 = nvar(code, w, {i0, i1}),
      x2 = ivar(code, w, {i2});

    auto p0 = pin(code, x0),
      p1 = pin(code, x1),
      p2 = pin(code, x2);
    auto b = api(code, {p0, p1, p2});
    compile(code, b);

    auto h = alloc(code, b);

    auto x0_mem = h.b(width, x0);
    auto x1_mem = h.n(width, x1);
    auto x2_mem = h.i(width, x2);

    REQUIRE(x0_mem.size() == n0);
    REQUIRE(x1_mem.size() == n0 * n1);
    REQUIRE(x2_mem.size() == n2);

    x0_mem[4] = static_cast<ncell<width>>(4);
    x1_mem[2] = static_cast<icell<width>>(2);
    x2_mem[0] = static_cast<bcell<width>>(1);

    CHECK(x0_mem[4] == static_cast<ncell<width>>(4));
    CHECK(x1_mem[2] == static_cast<icell<width>>(2));
    CHECK(x2_mem[0] == static_cast<bcell<width>>(1));

  }
  if constexpr(enable_reals){
    SUBCASE("reals"){
      auto x3 = rvar(code, w);
      auto p0 = pin(code, x3);
      auto b = api(code, {p0});
      compile(code, b);
      auto h = alloc(code, b);

      auto x3_mem = h.r(width, x3);
      REQUIRE(x3_mem.size() == 1);
      x3_mem[0] = static_cast<rcell<width>>(8);
      CHECK(x3_mem[0] == static_cast<rcell<width>>(8));
    }
  }
}

}// hysj::codes
#include<hysj/tools/epilogue.hpp>
