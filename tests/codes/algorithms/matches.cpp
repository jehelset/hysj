#include<optional>
#include<limits>

#include"../../framework.hpp"

#include<hysj/codes.hpp>
#include<hysj/codes/algorithms/matches.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{
  
TEST_CASE("codes.matches"){

  codes::code code{};
    
  auto matches_to =
    [&](id f,auto t,auto G){
      auto F = ranges::vec(match1(code,f,t));
      return std::ranges::equal(F,G);
    };

  auto no_match =
    [&](auto f,auto t){
      auto F = ranges::vec(match1(code,f,t));
      return std::ranges::empty(F);
    };

  auto x = ivar64(code);
  auto y = ivar64(code);
  auto f = add(code,{x,y});
  auto g = add(code,{f,y});

  CHECK(no_match(g,codes::subtag));
  CHECK(matches_to(g,codes::addtag,std::vector{g}));
  CHECK(matches_to(f,codes::vartag,std::vector{x,y}));
  CHECK(matches_to(g,codes::vartag,std::vector{x,y}));

}

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
