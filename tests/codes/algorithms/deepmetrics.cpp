#include<algorithm>
#include<functional>
#include<tuple>
#include<vector>

#include"../../framework.hpp"

#include<hysj/codes.hpp>
#include<hysj/codes/algorithms/deepmetrics.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

TEST_CASE("codes.deepmetrics"){

  codes::code code{};

  auto x = ivar64(code);
  auto y = ivar64(code);
    
  auto f0 = mul(code,{add(code,{y,x}),x});
  auto f1 = mul(code,{add(code,{y,x}),x});
  auto f2 = mul(code,{f0,f1});

  std::vector<id> F{f0,f1,f2};
  auto state = dmetric(code,views::all(F));
  
  auto tour = [&](auto i){
    return state.tour_of(objvert(code,i));
  };
  CHECK(std::ranges::distance(tour(f0)) == 26);
  CHECK(std::ranges::distance(tour(f1)) == 26);
  CHECK(std::ranges::distance(tour(f2)) ==
        std::ranges::distance(tour(f0)) +
        std::ranges::distance(tour(f1)) + 4);

  auto cmp =
    [&](grammars::edge a0,grammars::edge a1){
      return code.cmp(relargid(code, relid(code,a0)),
                      relargid(code, relid(code,a1)))
        == std::partial_ordering::equivalent;
    };

  CHECK(!std::ranges::equal(tour(f0),tour(f1)));
  CHECK(std::ranges::equal(tour(f0),tour(f1),cmp));
  CHECK(!std::ranges::equal(tour(f2),tour(f0)));
  CHECK(!std::ranges::equal(tour(f2),tour(f0),cmp));

}

}// hysj::codes
#include<hysj/tools/epilogue.hpp>
