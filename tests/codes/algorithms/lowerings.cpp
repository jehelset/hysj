#include<optional>

#include"../../framework.hpp"

#include<hysj/codes.hpp>
#include<hysj/codes/algorithms/lowerings.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

TEST_CASE("codes.lowerings"){

  codes::code code{};

  auto lowers_to =
    [&](id f0,id g){
      auto f1 = lower1(code,f0);
      if(f1)
        return equal(code,*f1,g);
      return equal(code,f0,g);
    };

  auto x = ivar64(code);
  auto y = ivar64(code);

  auto zero = code.builtins.lits.zero64;
  auto one = code.builtins.lits.one64;
  auto negone = code.builtins.lits.negone64;
  auto two = code.builtins.lits.two64;
  auto inf = code.builtins.lits.inf64;
  
  CHECK(lowers_to(neg(code,zero),zero));
  CHECK(lowers_to(neg(code,x),mul(code,{negone,x})));
  CHECK(lowers_to(sub(code,x,y),add(code,{x,mul(code,{negone,y})})));
  CHECK(lowers_to(add(code,{zero,zero}),zero));
  CHECK(lowers_to(add(code,{x,zero}),x));
  CHECK(lowers_to(add(code,{zero,x,x}),add(code,{x,x})));
  CHECK(lowers_to(mul(code,{one,x,one}),x));
  CHECK(lowers_to(mul(code,{one,one}),one));
  CHECK(lowers_to(mul(code,{x,x}),mul(code,{x,x})));
  CHECK(lowers_to(mul(code,{x,zero}),zero));
  CHECK(lowers_to(div(code,x,y),mul(code,{x,pow(code,y,negone)})));
  CHECK(lowers_to(div(code,x,one),x));
  CHECK(lowers_to(div(code,x,zero),inf));
  CHECK(lowers_to(div(code,zero,x),zero));
  CHECK(lowers_to(pow(code,zero,one),zero));
  CHECK(lowers_to(pow(code,x,one),x));
  CHECK(lowers_to(pow(code,x,zero),one));
  CHECK(lowers_to(pow(code,one,two),one));
  CHECK(lowers_to(pow(code,negone,icst64(code,3_i)),negone));
  CHECK(lowers_to(pow(code,negone,icst64(code,4_i)),one));
}

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
