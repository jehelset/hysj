#include<optional>
#include<vector>

#include<hysj/codes/algorithms/addfolds.hpp>
#include<hysj/codes.hpp>
#include<hysj/tools/ranges/generate.hpp>
#include<hysj/tools/ranges/utility.hpp>

#include"../../framework.hpp"

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes::algorithms{

TEST_CASE("codes.addfolds"){

  codes::code code{};

  auto x = ranges::vec(views::generate_n([&]{ return ivar64(code); },5));

  auto collect =
    [&](std::vector<id> x,id f){
      if(x.empty())
        return addfold1(code,f);
      else
        return addfold1(code,f,x);
    };

  auto str = [&](std::optional<id> g){
    return g ? to_string(code,*g) : std::string{"∅"};
  };
  auto msg2 = [&](id f,std::optional<id> g){
    return fmt::format("{} → {}",str(f),str(g));
  };
  auto msg3 = [&](id f,std::optional<id> g0,std::optional<id> g1){
    return fmt::format("{} → {} ≠ {}",str(f),str(g1),str(g0));
  };
  
  #define CHECK_NO_COLLECTION(X,f)          \
     {                                       \
       auto check_g = collect(X,f);          \
       auto check = check_g == std::nullopt; \
       CHECK_MESSAGE(check,msg2(f,check_g)); \
     }
 
  #define CHECK_COLLECTION(X,f)             \
     {                                       \
       auto check_g = collect(X,f);          \
       auto check = check_g != std::nullopt; \
       CHECK_MESSAGE(check,msg2(f,check_g)); \
     }

  #define CHECK_COLLECTS_TO(X,f,g0)                                      \
     {                                                                    \
       auto check_g1 = collect(X,f);                                      \
       auto check = check_g1 && equal(code,g0,*check_g1); \
       CHECK_MESSAGE(check,msg3(f,g0,check_g1));                          \
     }

  auto one = code.builtins.lits.one64;
  auto two = code.builtins.lits.two64;

  SUBCASE("0"){
    std::vector<id> D{};
    auto f = add(code,{x[0],x[1]});
    CHECK_NO_COLLECTION(D,f);
  }
  SUBCASE("1"){
    std::vector<id> D{x[0],x[1]};
    auto f = add(code,{x[0],x[1]});
    CHECK_NO_COLLECTION(D,f);
  }
  SUBCASE("2"){
    std::vector<id> D{};
    auto f = pow(code,x[0],two);
    CHECK_NO_COLLECTION(D,f);
  }
  SUBCASE("3"){
    std::vector<id> D{};
    auto f = add(code,{x[0],x[0]});
    auto g = mul(code,{add(code,{one,one}),x[0]});
    CHECK_COLLECTS_TO(D,f,g);
  }
  SUBCASE("4"){
    std::vector<id> D{};
    auto f = add(code,{x[0],mul(code,{two,x[0]})});
    auto g = mul(code,{add(code,{one,two}),x[0]});
    CHECK_COLLECTS_TO(D,f,g);
  }
  SUBCASE("5"){
    std::vector<id> D{};
    auto f = add(code,{one,x[0],mul(code,{two,x[0]})});
    auto g = add(code,{one,mul(code,{add(code,{one,two}),x[0]})});
    CHECK_COLLECTS_TO(D,f,g);
  }
  SUBCASE("6"){
    std::vector<id> D{x[0]};
    auto f = add(code,{x[0],mul(code,{x[0],x[1]})});
    auto g = mul(code,{add(code,{one,x[1]}),x[0]});
    CHECK_COLLECTS_TO(D,f,g);
  }
  SUBCASE("7"){
    std::vector<id> D{x[0],x[1]};
    auto f = add(code,{
        mul(code,{x[2],x[3],x[4],x[0],x[1]}),
        mul(code,{x[2],x[3],x[0],x[1]})
      });
    auto g = mul(code,{
        add(code,{
            mul(code,{x[2],x[3],x[4]}),
            mul(code,{x[2],x[3]})}),
        mul(code,{x[0],x[1]})
      });
    CHECK_COLLECTS_TO(D,f,g);
  }
  SUBCASE("8"){
    std::vector<id> D{x[0]};
    auto h0 = pow(code,x[0],two);
    auto f = add(code,{h0,h0});
    auto g = mul(code,{add(code,{one,one}),h0});
    CHECK_COLLECTS_TO(D,f,g);
  }
  #undef CHECK_NO_COLLECTION
  #undef CHECK_COLLECTION
  #undef CHECK_COLLECTS_TO
}

} // hysj::codes
#include<hysj/tools/epilogue.hpp>
