#include<optional>

#include"../../framework.hpp"

#include<hysj/codes.hpp>
#include<hysj/codes/algorithms/lexpands.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

TEST_CASE("codes.lexpands"){

  codes::code code{};

  auto x = ivar64(code);
  auto y = ivar64(code);
  auto z = ivar64(code);

  auto D =
    [](codes::varsym i)->std::optional<integer>{
      switch(i.idx){
      case 0_n: return {1_i};
      case 1_n: return {2_i};
      case 2_n: return {3_i};
      default: return std::nullopt;
      }
    };

  auto msg = [&](id f,std::optional<id> g0,std::optional<id> g1){
    return fmt::format("{} → {} ≠ {}",
                       to_string(code,f),
                       g0 ? to_string(code,*g0) : "∅",
                       g1 ? to_string(code,*g1) : "∅");
  };

  #define CHECK_NO_EXPANSION(f)                   \
    {                                             \
      auto g = lexpand1(D,code,f);                \
      bool check = g == std::nullopt;             \
      CHECK_MESSAGE(check,msg(f,g,std::nullopt)); \
    }
  #define CHECK_EXPANDS_TO(f,g1)             \
    {                                        \
      auto g0 = lexpand1(D,code,f);          \
      bool check = g0 && equal(code,*g0,g1); \
      CHECK_MESSAGE(check,msg(f,g0,g1));     \
    }

  {
    auto f = eq(code,x,icst64(code,3_i));
    CHECK_NO_EXPANSION(f);
  }
  {
    auto f = lnot(code,eq(code,x,icst64(code,0_i)));
    auto g1 = code.builtins.lits.bot64;
    CHECK_EXPANDS_TO(f,g1);
  }
  {
    auto f = lnot(code,eq(code,x,icst64(code,-1_i)));
    auto g1 = eq(code,x,icst64(code,0_i));
    CHECK_EXPANDS_TO(f,g1);
  }
  {
    auto f = lnot(code,eq(code,y,icst64(code,0_i)));
    auto g1 = eq(code,y,icst64(code,1_i));
    CHECK_EXPANDS_TO(f,g1);
  }
  {
    auto f = lnot(code,eq(code,y,icst64(code,1_i)));
    auto g1 = eq(code,y,icst64(code,0_i));
    CHECK_EXPANDS_TO(f,g1);
  }
  {
    auto f = lnot(code,eq(code,z,icst64(code,0_i)));
    auto g1 = lor(code,{
        eq(code,z,icst64(code,1_i)),
        eq(code,z,icst64(code,2_i))
      });
    CHECK_EXPANDS_TO(f,g1);
  }
  {
    auto f = lnot(code,eq(code,z,icst64(code,1_i)));
    auto g1 = lor(code,{
        eq(code,z,icst64(code,0_i)),
        eq(code,z,icst64(code,2_i))
      });
    CHECK_EXPANDS_TO(f,g1);
  }
  {
    auto f = lnot(code,eq(code,z,icst64(code,2_i)));
    auto g1 = lor(code,{
        eq(code,z,icst64(code,0_i)),
        eq(code,z,icst64(code,1_i))
      });
    CHECK_EXPANDS_TO(f,g1);
   }
  {
    auto f = land(code,{
        lnot(code,eq(code,x,icst64(code,0_i))),
        lnot(code,eq(code,y,icst64(code,0_i))),
        lnot(code,eq(code,z,icst64(code,0_i)))
      });
    auto g1 = land(code,{
        code.builtins.lits.bot64,
        eq(code,y,icst64(code,1_i)),
        lor(code,{
            eq(code,z,icst64(code,1_i)),
            eq(code,z,icst64(code,2_i))})
      });
    CHECK_EXPANDS_TO(f,g1);
  }
  {
    auto f = land(code,{
        lnot(code,eq(code,icst64(code,0_i),x)),
        lnot(code,eq(code,icst64(code,0_i),y)),
        lnot(code,eq(code,icst64(code,0_i),z))
      });
    auto g1 = land(code,{
        code.builtins.lits.bot64,
        eq(code,icst64(code,1_i),y),
        lor(code,{
            eq(code,icst64(code,1_i),z),
            eq(code,icst64(code,2_i),z)
          })
      });
    CHECK_EXPANDS_TO(f,g1);
  }
  {
    auto f = eq(code,x,y);
    auto g1 = land(code,{
        eq(code,x,icst64(code,0_i)),
        eq(code,y,icst64(code,0_i))
      });
    CHECK_EXPANDS_TO(f,g1);
  }
  {
    auto f = lnot(code,eq(code,y,z));
    auto g1 = lor(code,{
        land(code,{
            eq(code,y,icst64(code,0_i)),
            eq(code,z,icst64(code,1_i))
          }),
        land(code,{
            eq(code,y,icst64(code,0_i)),
            eq(code,z,icst64(code,2_i))
          }),
        land(code,{
            eq(code,y,icst64(code,1_i)),
            eq(code,z,icst64(code,0_i))
          }),
        land(code,{
            eq(code,y,icst64(code,1_i)),
            eq(code,z,icst64(code,2_i))
          })
      });
    CHECK_EXPANDS_TO(f,g1);
  }
  #undef CHECK_NO_EXPANSION
  #undef CHECK_EXPANDS_TO

}

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
