#include<optional>

#include"../../framework.hpp"

#include<hysj/codes.hpp>
#include<hysj/codes/algorithms/argfolds.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

TEST_CASE("codes.argfolds"){

  codes::code code{};

  auto no_fold =
    [&](id f){
      return argfold1(code,f) == std::nullopt;
    };
  auto folds_to =
    [&](id f0,id g){
      auto f1 = argfold1(code,f0);
      if(f1)
        return equal(code,*f1,g);
      return equal(code,f0,g);
    };

  auto x = ivar64(code);
  auto y = ivar64(code);
  auto z = ivar64(code);
  auto w = ivar64(code);

  auto zero = code.builtins.lits.zero64;

  CHECK(no_fold(neg(code,zero)));
  CHECK(folds_to(neg(code,neg(code,zero)),zero));
  CHECK(folds_to(lnot(code,lnot(code,x)),x));
  CHECK(folds_to(lnot(code,lnot(code,x)),x));
  CHECK(folds_to(add(code,{x,add(code,{x,x})}),add(code,{x,x,x})));
  CHECK(folds_to(mul(code,{mul(code,{x,x}),mul(code,{x,y})}),mul(code,{x,x,x,y})));
  CHECK(folds_to(land(code,{land(code,{x,y}),x}),land(code,{x,x,y})));
  CHECK(folds_to(lor(code,{lor(code,{y,lor(code,{y,y})}),y}),lor(code,{y,y,y,y})));
  CHECK(no_fold(div(code,x,x)));
  CHECK(folds_to(div(code,div(code,x,y),z),div(code,x,mul(code,{y,z}))));
  CHECK(folds_to(div(code,x,div(code,y,z)),div(code,mul(code,{x,z}),y)));
  CHECK(folds_to(div(code,div(code,x,y),div(code,z,w)),div(code,mul(code,{x,w}),mul(code,{y,z}))));
  CHECK(folds_to(pow(code,pow(code,x,y),z),pow(code,x,mul(code,{y,z}))));

}

} //hysj::codes::algorithms
#include<hysj/tools/epilogue.hpp>
