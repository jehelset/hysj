#include<algorithm>
#include<tuple>

#include"../../framework.hpp"

#include<hysj/codes/algorithms/deepmaps.hpp>
#include<hysj/codes.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes::algorithms{

struct dmaps_test_event{
  deepmaps::part part;
  deepmaps::step step;
  deepmaps::action action;
  std::optional<id> op;

  static dmaps_test_event make(const auto &pass, auto action) {
    if constexpr (pass.part == dmaps::prog_tag)
      return {pass.part(), pass.step(), action, std::nullopt};
    else
      return {pass.part(), pass.step(), action, pass.op.index};
  }
};

auto dmaps_test_trace(auto state){
  return (dmaps::map)(co_get<dmaps_test_event>{}, state,
                      []<typename DMapGet, dmaps::action A>(
                          DMapGet, const auto &pass,
                          constant_t<A> action) -> co::api_t<DMapGet> {
                        auto &dmap_co = HYSJ_CO(DMapGet);
                        co_await co::put<0>(
                            dmap_co, dmaps_test_event::make(pass, action));
                        co_await co::nil<1>(dmap_co);
                      });
}

TEST_CASE("codes.deepmaps"){

  codes::code code{};

  auto require_trace_consistency = [&](const auto &t){
    CHECK(t.size() >= 2);
    CHECK(t.front().part == dmaps::prog_tag());
    CHECK(t.front().step == dmaps::pre_tag());
    {
      const auto root_pre_count = std::ranges::count_if(
                     t,
                     [](auto t){
                       return t.part == dmaps::root_tag() && t.action == dmaps::noop_tag();
                     }),
                 root_noop_count = std::ranges::count_if(t, [](auto t){
                   return t.part == dmaps::root_tag() && t.action == dmaps::done_tag();
                 });
      CHECK(root_pre_count == root_noop_count);
    }
    CHECK(t.back().part == dmaps::prog_tag());
    CHECK(t.back().step == dmaps::post_tag());
  };

  auto x = ivar64(code);
  auto y = ivar64(code);
  {
    auto f = mul(code, {add(code, {y, x}), x});
    auto t = dmaps_test_trace(
        dmaps::make_state(code, views::pin(f), kumi::tuple{}));
    require_trace_consistency(ranges::vec(t));
  }
}

} // hysj::codes::algorithms
#include <hysj/tools/epilogue.hpp>
