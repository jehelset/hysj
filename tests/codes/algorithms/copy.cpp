#include<optional>
#include<string>
#include<vector>

#include"../../framework.hpp"

#include<fmt/core.h>

#include<hysj/codes/algorithms/copy.hpp>
#include<hysj/codes.hpp>
#include<hysj/tools/ranges/generate.hpp>
#include<hysj/tools/ranges/utility.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

TEST_CASE("codes.copy"){

  codes::code code{};

  auto check_msg = [&](id f,id g){
    return fmt::format("{} ≠ {}",to_string(code, f),to_string(code, g));
  };

  #define CHECK_COPY_EQUAL(f)                         \
    {                                                 \
       auto g = copy1(code,f);                        \
       CHECK_MESSAGE(equal(code,f,g),check_msg(f,g)); \
     }

  {
    auto x = ivar64(code);
    auto f = pow(code,x,code.builtins.lits.two64);
    CHECK_COPY_EQUAL(f);
  }

  #undef CHECK_COPY_EQUAL

  #define CHECK_CLONE_EQUAL(f)                        \
    {                                                 \
      auto g = clone1(code,f);                        \
      CHECK_MESSAGE(equal(code,f,g),check_msg(f,g));  \
    }

  {
    auto x = ivar64(code);
    auto f = pow(code,x,code.builtins.lits.two64);
    CHECK_CLONE_EQUAL(f);
  }

  #undef CHECK_CLONE_EQUAL


}

}// hysj::codes
#include<hysj/tools/epilogue.hpp>
