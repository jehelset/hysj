#include<algorithm>
#include<optional>
#include<utility>
#include<vector>

#include"../../framework.hpp"

#include<hysj/codes.hpp>
#include<hysj/codes/algorithms/merges.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/ranges/generate.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

TEST_CASE("codes.merges"){

  codes::code code{};

  auto t = ityp64(code);
  auto X = ranges::vec(views::generate_n([&]{ return var(code, t); },10_n));
  auto y = var(code, t);

  std::map<natural,int> varsem;
  for(auto x:X)
    varsem[x.idx] = 0;
  varsem[y.idx] = 1;

  auto N = icst64(code,10);

  std::map<natural,int> itrsem;
  auto i = itr(code,N);
  auto j = itr(code,N);
  auto k = itr(code,N);
  auto l = itr(code,N);
  itrsem[i.idx] = 0;
  itrsem[j.idx] = 1;
  itrsem[k.idx] = 2;
  itrsem[l.idx] = 3;

  using G = std::vector<kumi::tuple<id,id>>;
  auto eliminate =
    [&](id f)->G{
      return ranges::vec(
        merge1(code,
              [&](optag sup,natural sub0,natural sub1)->std::partial_ordering{
                if(sup == optag::var)
                  return varsem[sub0] <=> varsem[sub1];
                else if(sup == optag::itr)
                  return itrsem[sub0] <=> itrsem[sub1];
                return code.subcmp(sup,sub0,sub1);
              },
              f));
    };
  auto msg = [&](id f,const G &G0,const G &G1){
    return fmt::format("{} → {} ≠ {}",to_string(code,f),G1,G0);
  };

  #define CHECK_ELIMINATIONS(f,G0)                   \
    {                                                \
       G check_G1 = eliminate(f);                    \
       std::ranges::sort(G0);                        \
       std::ranges::sort(check_G1);                  \
       auto check = std::ranges::equal(G0,check_G1); \
       CHECK_MESSAGE(check,msg(f,G0,check_G1));      \
     }

  auto two = code.builtins.lits.two64;

  SUBCASE("0"){
    auto f = sub(code,X[0],X[1]);
    G G0{{X[0],X[1]}};
    CHECK_ELIMINATIONS(f,G0);
  }
  SUBCASE("1"){
    auto f0 = mul(code,{two,X[0],X[0]});
    auto f1 = mul(code,{two,X[1],X[1]});
    auto g0 = add(code,{y,f0,f1});
    auto f = g0;
    G G0{{f0,f1},{X[0],X[1]}};
    CHECK_ELIMINATIONS(f,G0);
  }
  SUBCASE("2"){
    auto f0 = mul(code,{two,X[0],X[0]});
    auto f1 = mul(code,{two,X[1],X[1]});
    auto f2 = mul(code,{two,X[1]});
    auto g0 = add(code,{y,f0,f1,f2});
    auto f = g0;
    G G0{{f0,f1},{X[0],X[1]}};
    CHECK_ELIMINATIONS(f,G0);
  }
  SUBCASE("3"){
    auto f0 = mul(code,{two,X[0],X[1]});
    auto f1 = mul(code,{two,X[2],X[3]});
    auto f2 = mul(code,{two,X[4]});
    auto g0 = add(code,{y,f0,f1,f2});
    auto f = g0;
    G G0{{f0,f1},{X[0],X[1]},{X[0],X[2]},{X[0],X[3]},{X[0],X[4]}};
    CHECK_ELIMINATIONS(f,G0);
  }
  SUBCASE("4"){
    auto f0 = mul(code,{two,X[0],X[0]});
    auto f1 = mul(code,{two,X[0],X[0]});
    auto f2 = mul(code,{two,two,X[0]});
    auto g0 = add(code,{y,f0,f1,f2});
    auto f = g0;
    G G0{{f0,f1}};
    CHECK_ELIMINATIONS(f,G0);
  }
  SUBCASE("5"){
    auto f0 = mul(code,{two,X[0],X[0]});
    auto f1 = mul(code,{two,X[0],two});
    auto g0 = add(code,{y,f0,f1});
    auto f = g0;
    G G0{};
    CHECK_ELIMINATIONS(f,G0);
  }
  SUBCASE("6"){
    SUBCASE("0"){
      auto t0 = two64(code);
      auto t1 = two64(code);
      auto h0 = pow(code,X[0],t0);
      auto h1 = pow(code,X[0],t1);
      auto f = add(code,{h0,h1});
      G G0{{h0,h1},{t0,t1}};
      CHECK_ELIMINATIONS(f,G0);
    }
    SUBCASE("1"){
      auto t = two64(code);
      auto h0 = pow(code,X[0],t);
      auto h1 = pow(code,X[0],t);
      auto f = add(code,{h0,h1});
      G G0{{h0,h1}};
      CHECK_ELIMINATIONS(f,G0);
    }
  }
  SUBCASE("7"){
    auto x = ivar64(code,{k, l});
    auto r0 = rot(code,x,{i,j});
    auto r1 = rot(code,x,{j,i});
    auto f = add(code,{r0,r1});
    G G0{};
    CHECK_ELIMINATIONS(f,G0);
  }
}

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
