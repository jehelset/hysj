#include<algorithm>
#include<optional>
#include<ranges>
#include<vector>

#include"../../framework.hpp"

#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/ranges/generate.hpp>
#include<hysj/codes.hpp>
#include<hysj/codes/algorithms/normforms.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

TEST_CASE("codes.normforms"){

  codes::code code{};

  auto X = ranges::vec(views::generate_n([&]{ return ivar64(code); },3_n));
  auto a = ivar64(code);

  std::map<natural,int> semvar;

  for(auto x:X)
    semvar[x.idx] = 0;
  semvar[a.idx] = 1;

  auto O =
    [&](optag i,natural j0,natural j1) -> std::partial_ordering {
      if(i == optag::var)
        return semvar[j0] <=> semvar[j1];
      return code.subcmp(i,j0,j1);
    };

  auto str = [&](std::optional<id> op){
    return op ? to_string(code,*op) : std::string{"∅"};
  };
  auto msg1 = [&](auto in,auto out0,auto out1){
    return fmt::format("{} → {} ≠ {}",str(in),str(out1),str(out0));
  };
  
  auto msg2 = [&](auto in0,auto in1,auto out0,auto out1){
    return fmt::format("{} → {} ≠ {} → {}",str(in0),str(out0),str(in1),str(out1));
  };

  auto simplify =
    [&](id f){
      return normform1(code,O,f);
    };

  auto is_equal_simplified =
    [&](auto f0,auto f1,auto g0,auto g1){
      return equal(code,g0.value_or(f0),g1.value_or(f1));
    };

  #define CHECK_SIMPLIFIES(f)                      \
    {                                              \
      auto g = simplify(f);                        \
      auto check = g != std::nullopt;              \
      CHECK_MESSAGE(check,msg1(f,std::nullopt,g)); \
    }

  #define CHECK_SIMPLIFIES_TO(f,g0)          \
    {                                        \
      auto g1 = simplify(f);                 \
      auto check = g1 && equal(code,*g1,g0); \
      CHECK_MESSAGE(check,msg1(f,g0,g1));    \
    }

  #define CHECK_EQUAL_SIMPLIFIED(f0,f1)               \
    {                                                 \
      auto g0 = simplify(f0);                         \
      auto g1 = simplify(f1);                         \
      auto check = is_equal_simplified(f0,f1,g0,g1);  \
      CHECK_MESSAGE(check,msg2(f0,f1,g0,g1));         \
    }

  {
    auto f = add(code,{
        add(code,{two64(code),X[0]}),
        mul(code,{
            two64(code),
            add(code,{X[0],X[0]})})});
    CHECK_SIMPLIFIES(f);
  }
  {
    auto f = div(code,sub(code,X[0],a),X[0]);
    auto g = add(code,{
        one64(code),
        mul(code,{
            negone64(code),a,
            pow(code,X[0],negone64(code))})});
    CHECK_SIMPLIFIES_TO(f,g);
  }
  {
    auto f0 = mul(code,{X[0],X[1]});
    auto f1 = mul(code,{X[2],X[1]});
    CHECK_EQUAL_SIMPLIFIED(f0,f1);
  }
  {
    auto f0 = pow(code,X[0],sub(code,two64(code),one64(code)));
    auto f1 = X[1];
    CHECK_EQUAL_SIMPLIFIED(f0,f1);
  }
  {
    auto f = mul(code,{X[0],X[0]});
    auto g = pow(code,X[0],two64(code));
    CHECK_SIMPLIFIES_TO(f,g);
  }
  {
    auto f0 = mul(code,{
        two64(code),
        pow(code,
            two64(code),
            negone64(code))});
    auto f1 = one64(code);
    CHECK_EQUAL_SIMPLIFIED(f0,f1);
  }
  {
    auto f = add(code,{add(code,{X[0],X[0]}),X[0]});
    auto g = mul(code,{icst64(code,3),X[0]});
    CHECK_SIMPLIFIES_TO(f,g);
  }
  {
    auto f = add(code,{
        X[0],add(code,{mul(code,{two64(code),a}),X[0]})});
    auto g = add(code,{
        mul(code,{two64(code),X[0]}),mul(code,{two64(code),a})});
    CHECK_SIMPLIFIES_TO(f,g);
  }
  {
    auto f = mul(code,{
        add(code,{mul(code,{negone64(code),a}),X[0]}),
        mul(code,{two64(code),a})
      });
    auto g = add(code,{
        mul(code,{
            negone64(code),
            two64(code),
            pow(code,a,two64(code))}),
        mul(code,{two64(code),X[0],a})});
    CHECK_SIMPLIFIES_TO(f,g);
  }
  {
    auto f0 = mul(code,{
        add(code,{mul(code,{negone64(code),a}),X[0]}),
        mul(code,{two64(code),a})
      });
    auto f1 = add(code,{
        mul(code,{negone64(code),a,two64(code),a}),
        mul(code,{X[0],two64(code),a})
      });
    CHECK_EQUAL_SIMPLIFIED(f0,f1);
  }
  {
    auto f0 = mul(code,{
        mul(code,{negone64(code),X[0]}),
        X[1]
      });
    auto f1 = mul(code,{negone64(code),X[0],X[1]});
    CHECK_EQUAL_SIMPLIFIED(f0,f1);
  }
  {
    auto f = add(code,{X[0],mul(code,{two64(code),a}),X[0]});
    auto g = add(code,{mul(code,{two64(code),X[0]}),mul(code,{two64(code),a})});
    CHECK_SIMPLIFIES_TO(f,g);
  }
  {
    auto f0 = mul(code,{two64(code),pow(code,add(code,{X[0],X[0]}),two64(code))});
    auto f1 = mul(code,{pow(code,icst64(code,2),icst64(code,3)),pow(code,X[0],two64(code))});
    CHECK_EQUAL_SIMPLIFIED(f0,f1);
  }
  {
    auto f0 = add(code,{
        X[0],
        add(code,{mul(code,{two64(code),a}),X[0]}),
        mul(code,{two64(code),pow(code,add(code,{X[0],X[0]}),two64(code))}),
        zero64(code)
      });
    auto f1 = add(code,{
        mul(code,{two64(code),X[0]}),
        mul(code,{two64(code),a}),
        mul(code,{pow(code,icst64(code,2),icst64(code,3)),pow(code,X[0],two64(code))})
      });
    CHECK_EQUAL_SIMPLIFIED(f0,f1);
  }
  {
    auto f0 = add(code,{X[0],add(code,{X[0],X[0]}),zero64(code)});
    auto f1 = mul(code,{icst64(code,3),X[0]});
    CHECK_EQUAL_SIMPLIFIED(f0,f1);
  }
  {
    auto f = pow(code,add(code,{X[0],X[0]}),two64(code));
    auto g = mul(code,{
        pow(code,
            two64(code),
            two64(code)),
        pow(code,
            X[0],
            two64(code))});
    CHECK_SIMPLIFIES_TO(f,g);
  }
  {
    auto f = add(code,{
        mul(code,{negone64(code),pow(code,X[0],two64(code))}),
        pow(code,X[0],icst64(code,2))
      });
    auto g = zero64(code);
    CHECK_SIMPLIFIES_TO(f,g);
  }
  {
    auto f = div(
      code,
      mul(code,{
          two64(code),
          pow(code,
              add(code,{X[0],neg(code,a)}),
              icst64(code,2))
        }),
      two64(code));
    CHECK_SIMPLIFIES(f)
  }
  {
    auto h = pow(code,X[0],two64(code));
    auto f = add(code,{h,h});
    auto g = mul(code,{two64(code),h});
    CHECK_SIMPLIFIES_TO(f,g);
  }
}

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
