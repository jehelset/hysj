#include<vector>

#include"../../framework.hpp"

#include<hysj/codes/algorithms/substitute.hpp>
#include<hysj/codes.hpp>
#include<hysj/tools/ranges/utility.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

TEST_CASE("codes.substitute"){

  using namespace factories;

  codes::code code{};

  auto msg = [&](id f,std::optional<id> g0,id g1){
    return fmt::format("{} → {} ≠ {}",
                       to_string(code,f),
                       g0 ? to_string(code, *g0) :  std::string{"∅"},
                       to_string(code, g1));
  };

  auto substitutes_to =
    [&](id f0,std::optional<id> g0,id g1){
      if(g0)
        return equal(code,*g0,g1);
      return equal(code,f0,g1);
    };

  #define CHECK_SUBSTITUTES_TO(s, f, g)             \
    {                                               \
      auto g_sub__ = s(f);                          \
      auto check__ = substitutes_to(f, g_sub__, g); \
      CHECK_MESSAGE(check__, msg(f, g_sub__, g));   \
    }

  SUBCASE("x == 2"){
    auto t = ivar32(code);

    auto c = icst32(code, 2);
    auto g = eq(code, t, c);

    auto n = icst32(code, 1);
    auto i = itr(code, n);
    auto h = ivar32(code);

    auto T = ivar32(code, {i});
    auto t_now = rot(code, T, {h});

    substitute s{
      .code = code,
      .subs{
        kumi::make_tuple(t.id(), t_now.id())
      }
    };

    auto g_sub = eq(code, t_now, c);
    CHECK_SUBSTITUTES_TO(s, g.id(), g_sub.id());

  }
}

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
