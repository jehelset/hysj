#include <compare>
#include <functional>
#include <string>
#include <vector>

#include <fmt/core.h>

#include <hysj/codes/code.hpp>
#include <hysj/codes/to_string.hpp>
#include <hysj/codes/factories.hpp>
#include <hysj/codes/algorithms/symdiffs.hpp>

#include"../../framework.hpp"

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

TEST_CASE("codes.symdiffs"){

  codes::code code{};

  auto t = rvar64(code);

  auto x = rvar64(code);
  auto dxdt = der(code,x,t);

  auto y = rvar64(code);
  auto dydt = der(code,y,t);

  auto z = rvar64(code);
  auto dzdt = der(code,z,t);

  auto dxdy = der(code,x,y);
  auto dxdydt = der(code, der(code,x,t),y);

  auto msg = [&](auto f,auto dfdt0,auto dfdt1){
    return fmt::format("{} → {} ≠ {}",
                       to_string(code,f),
                       (dfdt1 ? to_string(code,*dfdt1) : std::string{"∅"}),
                       to_string(code,dfdt0));
  };

  #define CHECK_DIFFERENTIATES_TO(f,dfdt0,x,...)          \
    {                                                     \
      auto dI = std::invoke(                              \
        [&](int,auto... i){                               \
          return std::vector<exprfam>{expr(i)...};        \
        },                                                \
        0 __VA_OPT__(,) __VA_ARGS__);                     \
      auto g = der(code, f, x, dI);                       \
      auto dfdt1 = symdiff(code,g);                       \
      auto check = dfdt1 && equal(code,*dfdt1,dfdt0);     \
      CHECK_MESSAGE(check,msg(f,dfdt0,dfdt1));            \
    }

  auto zero = code.builtins.lits.zero64;
  auto one = code.builtins.lits.one64;
  auto two = code.builtins.lits.two64;
  auto pi = code.builtins.lits.pi64;

  CHECK_DIFFERENTIATES_TO(one, zero, t);
  CHECK_DIFFERENTIATES_TO(t,one, t);
  CHECK_DIFFERENTIATES_TO(x,dxdt, t);
  CHECK_DIFFERENTIATES_TO(dxdy,dxdydt, t);
  {
    auto f = neg(code,x);
    auto dfdt = neg(code,dxdt);
    CHECK_DIFFERENTIATES_TO(f,dfdt, t);
  }
  {
    auto f = add(code,{x,x,y,z});
    auto dfdt = add(code,{dxdt,dxdt,dydt,dzdt});
    CHECK_DIFFERENTIATES_TO(f,dfdt, t);
  }
  {
    auto f = sub(code,x,{y,z});
    auto dfdt = sub(code,dxdt,{dydt,dzdt});
    CHECK_DIFFERENTIATES_TO(f,dfdt, t);
  }
  {
    auto f = mul(code,{x,y,z});
    auto dfdt = add(code,{mul(code,{dxdt,y,z}),mul(code,{x,dydt,z}),mul(code,{x,y,dzdt})});
    CHECK_DIFFERENTIATES_TO(f,dfdt, t);
  }
  {
    auto f = div(code,x,y);
    auto dfdt = div(code,
                    add(code,
                        {
                          mul(code,{dxdt,y}),
                          mul(code,{x,dydt})
                        }),
                     mul(code,{y,y}));
    CHECK_DIFFERENTIATES_TO(f,dfdt, t);
  }
  {
    auto f = pow(code,x,y);
    auto dfdt = add(code,{
        mul(code,{y,dxdt,pow(code,x,sub(code,y,one))}),
        mul(code,{pow(code,x,y),ln(code,64,x),dydt})});
    CHECK_DIFFERENTIATES_TO(f,dfdt, t);
  }
  {
    auto f = log(code,x,y);
    auto dfdt = div(
      code,
      sub(code,
          div(code,mul(code,{ln(code,64,x),dydt}),y),
          div(code,mul(code,{dxdt,ln(code,64,y)}),x)),
      pow(code,ln(code,64,x),two));
    CHECK_DIFFERENTIATES_TO(f,dfdt, t);
  }
  {
    auto f = sin(code,x);
    auto dfdt = mul(code,{dxdt,sin(code,sub(code,x,div(code,pi,two)))});
    CHECK_DIFFERENTIATES_TO(f,dfdt, t);
  }
  {
    auto f = eq(code,x,y);
    auto dfdt = eq(code,dxdt,dydt);
    CHECK_DIFFERENTIATES_TO(f,dfdt, t);
  }
  {
    auto f = lt(code,x,y);
    auto dfdt = lt(code,dxdt,dydt);
    CHECK_DIFFERENTIATES_TO(f,dfdt, t);
  }
  //FIXME:
   // figure out how to deal - jeh
  // {
  //   auto f = bcst64(code,true);
  //   auto dfdt = bcst64(code,true);
  //   CHECK_DIFFERENTIATES_TO(f,dfdt, t);
  // }
  // {
  //   auto f = lnot(code,eq(code,x,y));
  //   auto dfdt = lnot(code,eq(code,dxdt,dydt));
  //   CHECK_DIFFERENTIATES_TO(f,dfdt, t);
  // }
  // {
  //   auto f = land(code,{eq(code,x,y),bcst64(code,true)});
  //   auto dfdt = land(code,{eq(code,dxdt,dydt),bcst64(code,true)});
  //   CHECK_DIFFERENTIATES_TO(f,dfdt, t);
  // }
  // {
  //   auto f = lor(code,{eq(code,x,y),bcst64(code,true)});
  //   auto dfdt = lor(code,{eq(code,dxdt,dydt),bcst64(code,true)});
  //   CHECK_DIFFERENTIATES_TO(f,dfdt, t);
  // }
  {
    auto n = icst32(code, {8});
    auto i0 = itr(code, n),
      i1 = itr(code, n);

    auto x = rvar32(code, {i0});

    auto f = x;
    auto dfdxi0 = land(code, {eq(code, i0, i0)});
    auto dfdxi1 = land(code, {eq(code, i0, i1)});

    CHECK_DIFFERENTIATES_TO(f, dfdxi0, x, i0);
    CHECK_DIFFERENTIATES_TO(f, dfdxi1, x, i1);
  }
  {
    auto n = icst32(code, {8});
    auto i0 = itr(code, n),
      i1 = itr(code, n),
      i2 = itr(code, n),
      i3 = itr(code, n);

    auto x = rvar32(code, {i0, i1});

    auto f = x;
    auto dfdxi0i1 = land(code, {eq(code, i0, i0), eq(code, i1, i1)});
    auto dfdxi2i3 = land(code, {eq(code, i0, i2), eq(code, i1, i3)});
    auto dfdxi2i1 = land(code, {eq(code, i0, i2), eq(code, i1, i1)});
    auto dfdxi0i3 = land(code, {eq(code, i0, i0), eq(code, i1, i3)});

    CHECK_DIFFERENTIATES_TO(f, dfdxi0i1, x, i0, i1);
    CHECK_DIFFERENTIATES_TO(f, dfdxi2i3, x, i2 ,i3);
    CHECK_DIFFERENTIATES_TO(f, dfdxi2i1, x, i2, i1);
    CHECK_DIFFERENTIATES_TO(f, dfdxi0i3, x, i0 ,i3);
  }
  {
    auto n = icst32(code, {8});
    auto i0 = itr(code, n),
      i1 = itr(code, n),
      i2 = itr(code, n),
      i3 = itr(code, n);

    auto x = rvar32(code, {i0, i1});
    auto r = rot(code, x, {i1, i0});
    auto f = mul(code, {x, r});

    {
      auto dfdxi0i1 =
        add(code, {
            mul(code,{
                land(code, {eq(code, i0, i0), eq(code, i1, i1)}),
                r
              }),
            mul(code,{
                x,
                land(code, {eq(code, i1, i0), eq(code, i0, i1)}),
              })
          });
      CHECK_DIFFERENTIATES_TO(f, dfdxi0i1, x, i0, i1);
    }
    {
      auto dfdxi2i3 =
        add(code, {
            mul(code,{
                land(code, {eq(code, i0, i2), eq(code, i1, i3)}),
                r
              }),
            mul(code,{
                x,
                land(code, {eq(code, i1, i2), eq(code, i0, i3)}),
              })
          });
      CHECK_DIFFERENTIATES_TO(f, dfdxi2i3, x, i2, i3);
    }
    {
      auto dfdxi2i1 =
        add(code, {
            mul(code,{
                land(code, {eq(code, i0, i2), eq(code, i1, i1)}),
                r
              }),
            mul(code,{
                x,
                land(code, {eq(code, i1, i2), eq(code, i0, i1)}),
              })
          });
      CHECK_DIFFERENTIATES_TO(f, dfdxi2i1, x, i2, i1);
    }
    {
      auto dfdxi0i3 =
        add(code, {
            mul(code,{
                land(code, {eq(code, i0, i0), eq(code, i1, i3)}),
                r
              }),
            mul(code,{
                x,
                land(code, {eq(code, i1, i0), eq(code, i0, i3)}),
              })
          });
      CHECK_DIFFERENTIATES_TO(f, dfdxi0i3, x, i0, i3);
    }
  }
  {
    auto n = icst32(code, {8});
    auto i0 = itr(code, n);
    auto x = rvar32(code, {i0}),
      q = ivar32(code);
    auto f = rot(code, x, {q});
    
    auto dfdxi0 = land(code, {eq(code, q, i0)});
    CHECK_DIFFERENTIATES_TO(f, dfdxi0, x, i0);
  }
}

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
