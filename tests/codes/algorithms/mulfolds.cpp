#include<optional>
#include<vector>

#include"../../framework.hpp"

#include<hysj/codes/algorithms/mulfolds.hpp>
#include<hysj/codes.hpp>
#include<hysj/tools/ranges/generate.hpp>
#include<hysj/tools/ranges/utility.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

TEST_CASE("codes.mulfolds"){

  codes::code code{};

  auto O =
    [&](optag i,natural j0,natural j1) -> std::partial_ordering {
      if(i == optag::var)
        return j0 <=> j1;
      return code.subcmp(i,j0,j1);
    };

  auto str = [&](std::optional<id> o){
    return (o ? to_string(code,*o) : std::string{"∅"});
  };

  auto msg2 = [&](auto in,auto out){
    return fmt::format("{} → {}",str(in),str(out));
  };
  auto msg3 = [&](auto in,auto out0,auto out1){
    return fmt::format("{} → {} ≠ {}",str(in),str(out1),str(out0));
  };
  
  auto collect  = 
    [&](auto f0){
      return mulfold1(code,O,f0);
    };
  
  #define CHECK_NO_COLLECTION(in)                      \
    {                                                  \
      auto out = collect(in);                          \
      CHECK_MESSAGE(out == std::nullopt,msg2(in,out)); \
    }
    
  #define CHECK_COLLECTS_TO(in,out0)               \
    {                                              \
      auto out1 = collect(in);                     \
      auto check = out1 && equal(code,*out1,out0); \
      CHECK_MESSAGE(check,msg3(in,out0,out1));     \
    }
    
  auto X = ranges::vec(views::generate_n([&]{ return ivar64(code); },5_n));

  auto one = code.builtins.lits.one64;
  auto two = code.builtins.lits.two64;
  {
    auto f = div(code,X[0],X[0]);
    auto g = mul(code,{f,pow(code,f,f)});
    CHECK_NO_COLLECTION(g);
  }
  CHECK_NO_COLLECTION(sub(code,X[0],X[1]));
  CHECK_NO_COLLECTION(mul(code,{X[0],X[1]}));
  CHECK_COLLECTS_TO(mul(code,{X[0],X[0]}),
                    pow(code,X[0],add(code,{one,one})));
  CHECK_COLLECTS_TO(mul(code,{pow(code,X[0],two),X[0]}),
                    pow(code,X[0],add(code,{two,one})));
  CHECK_COLLECTS_TO(mul(code,{X[0],X[0],X[1],X[1],two}),
                    mul(code,{two,
                        pow(code,X[0],add(code,{one,one})),
                        pow(code,X[1],add(code,{one,one}))}));
  #undef CHECK_NO_COLLECTION
  #undef CHECK_COLLECTS_TO
}

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
