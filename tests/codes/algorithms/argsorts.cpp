#include<vector>

#include"../../framework.hpp"

#include<hysj/codes/algorithms/argsorts.hpp>
#include<hysj/codes.hpp>
#include<hysj/tools/ranges/generate.hpp>
#include<hysj/tools/ranges/utility.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

TEST_CASE("codes.argsorts"){
  codes::code code{};

  auto X = ranges::vec(views::generate_n([&]{ return rvar64(code); },3_n));
  auto x0 = rvar64(code);
  auto a = rcst64(code,1.0), b = rcst64(code, 2.0);

  std::map<natural,int> varsem{
    {X[0].idx,0},
    {x0.idx  ,0},
    {X[1].idx,1},
    {X[2].idx,2}
  };
  auto suborder =
    [&](optag sup,natural sub0,natural sub1)->std::partial_ordering{
      if(sup == optag::var)
        return varsem[sub0] <=> varsem[sub1];
      return code.subcmp(sup,sub0,sub1);
    };
  
  auto reorder =
    [&](std::vector<id> F){
      return ranges::vec(argsort(code,suborder,views::all(F)));
    };

  auto reordered_to =
    [&](std::vector<id> F,std::vector<std::optional<id>> G){
      if(std::ranges::size(G) != std::ranges::size(F))
        return false;
      auto I = views::indices(F);
      return 
        std::ranges::adjacent_find(
          I,
          [&](auto l,auto r){
            return !equal(code,G[l].value_or(F[l]),G[r].value_or(F[r]));
          })
        == std::ranges::end(I);
    };
  auto no_reorder =
    [&](const std::vector<id> &F,const std::vector<std::optional<id>> &G){
      return F.size() == G.size() && std::ranges::all_of(G,[](auto g){ return g == std::nullopt; });
    };

  auto str = [&](std::optional<id> o){
    return (o ? to_string(code,*o) : std::string{"∅"});
  };

  auto msg = [&](std::vector<id> F,std::vector<std::optional<id>> G){
    return fmt::format("{} → {}",
                       fmt::join(views::map(F,str),","),
                       fmt::join(views::map(G,str),","));
  };

  #define CHECK_NO_REORDER(...)                     \
   {                                                \
     auto in = std::vector<id>{__VA_ARGS__};        \
     auto out = reorder(in);                        \
     CHECK_MESSAGE(no_reorder(in,out),msg(in,out)); \
   }
    
  #define CHECK_REORDERS_TO(...)                        \
     {                                                  \
       auto in = std::vector<id>{__VA_ARGS__};          \
       auto out = reorder(in);                          \
       INFO(msg(in,out));                               \
       CHECK_MESSAGE(reordered_to(in,out),msg(in,out)); \
     }

  auto negone = code.builtins.lits.negone64;

  {
    auto f = mul(code,{X[0],add(code,{X[0],X[1]})});
    CHECK_NO_REORDER(f);
  }
  {
    auto f = sub(code,X[0],X[1]);
    CHECK_NO_REORDER(f);
  }
  {
    auto f0 = mul(code,{X[0],add(code,{X[1],X[0]})});
    auto f1 = mul(code,{add(code,{X[0],X[1]}),X[0]});
    CHECK_REORDERS_TO(f0,f1);
  }
  {
    auto f0 = mul(code,{add(code,{X[1],X[0]}),X[0]});
    auto f1 = mul(code,{add(code,{X[0],X[1]}),X[0]});
    auto f2 = mul(code,{X[0],add(code,{X[0],X[1]})});
    auto f3 = mul(code,{X[0],add(code,{X[1],X[0]})});
    CHECK_REORDERS_TO(f0,f1,f2,f3);
  }
  CHECK_NO_REORDER({X[0],X[1]});
  CHECK_REORDERS_TO(X[0],x0);
  {
    auto f0 = add(code,{X[2],X[0],X[1]});
    auto f1 = add(code,{X[0],X[1],X[2]});
    CHECK_REORDERS_TO(f0,f1);
  }
  {
    auto f0 = add(code,{mul(code,{X[0],X[1]}),X[2]});
    auto f1 = add(code,{X[2],mul(code,{X[0],X[1]})});
    CHECK_REORDERS_TO(f0,f1);
  }
  {
    auto mul3_0 = mul(code,{X[0],negone,X[1]});
    auto mul3_1 = mul(code,{negone,X[0],X[1]});
    CHECK_REORDERS_TO(mul3_0,mul3_1);
  }
  {
    auto f0 = mul(code,{add(code,{X[1],X[0]}),X[0]});
    auto f1 = mul(code,{add(code,{X[0],X[1]}),X[0]});
    auto f2 = mul(code,{X[0],add(code,{X[1],X[0]})});
    auto f3 = mul(code,{X[0],add(code,{X[0],X[1]})});
    auto g = mul(code,{X[0],add(code,{X[0],X[1]})});
    CHECK_REORDERS_TO(f0,g);
    CHECK_REORDERS_TO(f1,g);
    CHECK_REORDERS_TO(f2,g);
    CHECK_NO_REORDER(f3);
  }
  {
    auto f = add(code,{b,mul(code,{X[0],a})});
    auto g = add(code,{mul(code,{a,X[0]}),b});
    CHECK_REORDERS_TO(f,g);
  }
  #undef CHECK_NO_REORDER
  #undef CHECK_REORDERS_TO
}

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
