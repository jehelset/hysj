#include<optional>
#include<vector>

#include"../../framework.hpp"

#include<hysj/codes/algorithms/dists.hpp>
#include<hysj/codes.hpp>
#include<hysj/tools/ranges/generate.hpp>
#include<hysj/tools/ranges/utility.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

TEST_CASE("codes.dists"){

  codes::code code{};

  auto x = ranges::vec(views::generate_n([&]{ return ivar64(code); },5_n));

  auto check_msg = [&](id f,std::optional<id> g0,std::optional<id> g1){
    auto str = [&](std::optional<id> g){
      return g ? to_string(code,*g) : std::string{"∅"};
    };
    return fmt::format("{} → {} ≠ {}",str(f),str(g1),str(g0));
  };

  #define CHECK_NO_DIST(d,f)                             \
    {                                                    \
      auto g1 = d(code,f);                               \
      bool check = g1 == std::nullopt;                   \
      CHECK_MESSAGE(check,check_msg(f,std::nullopt,g1)); \
    }
  #define CHECK_DIST_TO(d,f,g0)                \
    {                                          \
      auto g1 = d(code,f);                     \
      bool check = g1 && equal(code,g0,*g1);   \
      CHECK_MESSAGE(check,check_msg(f,g0,g1)); \
    }

  auto one = code.builtins.lits.one64;
  auto two = code.builtins.lits.two64;

  SUBCASE("mul"){
    CHECK_NO_DIST(muldist1,mul(code,{x[0],x[1]}));

    CHECK_DIST_TO(muldist1,mul(code,{x[0],add(code,{x[1],x[2]})}),
                  add(code,{mul(code,{x[0],x[1]}),mul(code,{x[0],x[2]})}));
    CHECK_DIST_TO(muldist1,mul(code,{add(code,{x[0],x[1]}),mul(code,{x[2],x[3]})}),
                  add(code,{mul(code,{x[0],x[2],x[3]}),
                      mul(code,{x[1],x[2],x[3]})}));
    CHECK_DIST_TO(muldist1,mul(code,{add(code,{x[1],x[2]}),x[0]}),
                  add(code,{mul(code,{x[1],x[0]}),mul(code,{x[2],x[0]})}));
    CHECK_DIST_TO(muldist1,
                  mul(code,{
                      add(code,{x[1],x[2]}),
                      mul(code,{add(code,{x[3],x[4]}),x[0]})
                    }),
                  add(code,{
                      mul(code,{x[1],x[3],x[0]}),
                      mul(code,{x[1],x[4],x[0]}),
                      mul(code,{x[2],x[3],x[0]}),
                      mul(code,{x[2],x[4],x[0]})
                    })
      );
    CHECK_DIST_TO(muldist1,
                  pow(code,add(code,{x[0],x[1]}),two64(code)),
                  add(code,{
                      mul(code,{x[0],x[0]}),
                      mul(code,{x[0],x[1]}),
                      mul(code,{x[1],x[0]}),
                      mul(code,{x[1],x[1]})}));
    CHECK_NO_DIST(muldist1,pow(code,x[0],two64(code)));
    CHECK_NO_DIST(muldist1,pow(code,x[0],rcst64(code,1.0_r/3.0_r)));
  }

  SUBCASE("pow"){
    CHECK_NO_DIST(powdist1,pow(code,x[0],one));
    CHECK_DIST_TO(powdist1,pow(code,
                               mul(code,{two,x[0]}),two),
                  mul(code,{
                      pow(code,two,two),
                      pow(code,x[0],two)}));
    CHECK_DIST_TO(powdist1,pow(code,mul(code,{x[0],x[0]}),one),
                  mul(code,{
                      pow(code,x[0],one),
                      pow(code,x[0],one)}));
    CHECK_DIST_TO(powdist1,add(code,{one,pow(code,mul(code,{x[0],one,one}),one)}),
                  add(code,{one,mul(code,{pow(code,mul(code,{one,one}),one),pow(code,x[0],one)})}));
    {
      auto f = pow(code,mul(code,{pow(code,mul(code,{x[0],x[0]}),one),x[0]}),one);
      auto g =
        mul(code,{
            pow(code,pow(code,x[0],one),one),
            pow(code,pow(code,x[0],one),one),
            pow(code,x[0],one)
          });
      CHECK_DIST_TO(powdist1,f,g);
    }
  }

  SUBCASE("land"){
    CHECK_NO_DIST(landdist1,bcst64(code,true));
    CHECK_NO_DIST(landdist1,land(code,{bcst64(code,true),bcst64(code,true)}));
    CHECK_DIST_TO(landdist1,
                  land(code,{lor(code,{x[0],x[1]}),x[2]}),
                  lor(code,{land(code,{x[0],x[2]}),land(code,{x[1],x[2]})}));
    CHECK_DIST_TO(landdist1,land(code,{x[2],lor(code,{x[0],x[1]})}),
                  lor(code,{land(code,{x[2],x[0]}),land(code,{x[2],x[1]})}));
    CHECK_DIST_TO(landdist1,
                  land(code,{lor(code,{land(code,{lor(code,{x[0],x[1]}),x[2]}),x[3]}),x[4]}),
                  lor(code,{
                      land(code,{x[0],x[2],x[4]}),
                      land(code,{x[1],x[2],x[4]}),
                      land(code,{x[3],x[4]})
                    }));
  }

  SUBCASE("lor"){
    CHECK_NO_DIST(lordist1,bcst64(code,true));
    CHECK_NO_DIST(lordist1,lor(code,{bcst64(code,true),bcst64(code,true)}));
    CHECK_DIST_TO(lordist1,lor(code,{land(code,{x[0],x[1]}),x[2]}),
                  land(code,{lor(code,{x[0],x[2]}),lor(code,{x[1],x[2]})}));
    CHECK_DIST_TO(lordist1,lor(code,{x[2],land(code,{x[0],x[1]})}),
                  land(code,{lor(code,{x[2],x[0]}),lor(code,{x[2],x[1]})}));
  }

  SUBCASE("lnot"){
    CHECK_NO_DIST(lnotdist1,bcst64(code,true));
    CHECK_DIST_TO(lnotdist1,
                  lnot(code,lor(code,{bcst64(code,true),bcst64(code,true)})),
                  land(code,{lnot(code,bcst64(code,true)),lnot(code,bcst64(code,true))}));
    CHECK_DIST_TO(lnotdist1,
                  lnot(code,land(code,{x[0],x[1],x[2]})),
                  lor(code,{lnot(code,x[0]),lnot(code,x[1]),lnot(code,x[2])}));
  }

  #undef CHECK_NO_DIST
  #undef CHECK_DIST_TO
 
}
 
} //hysj::codes
#include<hysj/tools/epilogue.hpp>
