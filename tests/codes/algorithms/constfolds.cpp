#include<limits>
#include<optional>

#include"../../framework.hpp"

#include<hysj/codes/algorithms/constfolds.hpp>
#include<hysj/codes.hpp>
#include<hysj/tools/functional/optional.hpp>

#include <hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes {

TEST_CASE("codes.constfolds"){

  codes::code code{};

  auto msg = [&](auto f, auto g0, auto g1, const char *o = "≠"){
    return fmt::format(
        "{} → {} {} {}", to_string(code, f),
        (g0 ? to_string(code, *g0) : std::string{"∅"}), o,
        (g1 ? to_string(code, *g1) : std::string{"∅"}));
  };

#define CHECK_FOLD(f)                             \
  {                                               \
    auto g = constfold1(code, f);                 \
    auto check = g != std::nullopt;               \
    CHECK_MESSAGE(check, msg(f, no<id>, g, "=")); \
  }
#define CHECK_NO_FOLD(f)                     \
  {                                          \
    auto g = constfold1(code, f);            \
    auto check = g == std::nullopt;          \
    CHECK_MESSAGE(check, msg(f, g, no<id>)); \
  }
#define CHECK_FOLDS_TO(f, g0)                   \
  {                                             \
    auto g1 = constfold1(code, f);              \
    auto check = g1 && equal(code, g0, *g1);    \
    CHECK_MESSAGE(check, msg(f, g1, just(g0))); \
  }

  auto x = rvar64(code);
  auto zero = code.builtins.lits.zero64;
  auto one = code.builtins.lits.one64;
  auto two = code.builtins.lits.two64;
  auto top = code.builtins.lits.top8;
  auto inf = code.builtins.lits.inf64;
  auto negone = code.builtins.lits.negone64;

  SUBCASE("0"){
    CHECK_NO_FOLD(mul(code, {x, x}));
  }
  SUBCASE("1"){
    CHECK_NO_FOLD(rcst64(code, 0.5));
  }
  SUBCASE("2"){
    auto f = mul(code,
                 {pow(code, icst64(code, 2),
                      icst64(code, 3)),
                  x});
    CHECK_FOLD(f);
  }
  SUBCASE("3"){
    auto f =
        pow(code, add(code, {one, one}),
            rcst64(code, 0.34_r));
    auto g0 =
        pow(code, two, rcst64(code, 0.34_r));
    CHECK_FOLDS_TO(f, g0);
  }
  SUBCASE("4"){
    CHECK_FOLDS_TO(mul(code, {one, one}), one);
  }
  SUBCASE("5"){
    CHECK_FOLDS_TO(
        add(code,
            {icst64(code, -1),
             rcst64(code, std::numeric_limits<real>::infinity())}),
        inf);
  }
  SUBCASE("6"){
    CHECK_FOLDS_TO(add(code, {icst64(code, 0),icst64(code, -1)}),
                   negone);
  }
  SUBCASE("7"){
    CHECK_FOLDS_TO(add(code, {rcst64(code, 2.0),rcst64(code, 2.0)}),
                   icst64(code, 4));
  }
  SUBCASE("8"){
    CHECK_FOLDS_TO(add(code, {one, one}), two);
  }
  SUBCASE("9"){
    CHECK_FOLDS_TO(
        sub(code,
            add(code, {one, one}),
            two),
        zero);
  }
  SUBCASE("10"){
    CHECK_FOLDS_TO(mul(code,
                       {x, add(code, {one, one})}),
                   mul(code, {x, two}));
  }
  SUBCASE("11"){
    CHECK_FOLDS_TO(mul(code,
                       {one, div(code, one, one)}),
                   one);
  }
  SUBCASE("12"){
    CHECK_FOLDS_TO(land(code, {top, top}), top);
  }
  SUBCASE("13"){
    CHECK_FOLDS_TO(eq(code,
                      add(code, {one, one}), x),
                   eq(code, two, x));
  }
  SUBCASE("14"){
    CHECK_FOLDS_TO(
        pow(code, rcst64(code, 0.34_r),
            add(code, {one, one})),
        pow(code, rcst64(code, 0.34_r), two));
  }
  SUBCASE("15"){
    auto one = code.builtins.lits.one32;
    auto t = ivar32(code);
    auto N = icst32(code, 2);
    auto M = icst32(code, 2);

    CHECK_FOLDS_TO(mul(code, {t, inc(code, div(code, dec(code, N), M))}),
                   mul(code, {t, one}));
  }
}

} // hysj::codes
#include <hysj/tools/epilogue.hpp>
