#include<vector>

#include"../../framework.hpp"

#include<hysj/codes/algorithms/opsubs.hpp>
#include<hysj/codes.hpp>
#include<hysj/tools/ranges/generate.hpp>
#include<hysj/tools/ranges/utility.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

TEST_CASE("codes.opsubs"){

  codes::code code{};

  SUBCASE("0"){
    auto x = ranges::vec(views::generate_n([&]{ return ivar64(code); },3_n));
    auto y = ivar64(code);
    std::vector f{
      add(code,{x[0],x[1]}),
      add(code,{x[0],x[1]}),
      add(code,{x[2],x[2]}),
      add(code,{x[2],x[2]})
    };

    total_opsub(code,x[0],y);
    CHECK(partial_opsub(code,f[1],x[1],y));

    using exprs = std::vector<codes::exprfam>;
    CHECK(ranges::vec(arg(code, f[0], 0_N)) == exprs{{y},{x[1]}});
    CHECK(ranges::vec(arg(code, f[1], 0_N)) == exprs{{y},{y}});

    total_opsub(code,f[0],f[0]);
    CHECK(ranges::vec(arg(code, f[0], 0_N)) == exprs{{y},{x[1]}});
    CHECK(partial_opsub(code,f[0],y,y));
    CHECK(ranges::vec(arg(code, f[0], 0_N)) == exprs{{y},{x[1]}});

    CHECK(partial_opsub(code,f[2],x[2],y));
    CHECK(ranges::vec(arg(code, f[2], 0_N)) == exprs{{y},{y}});

    CHECK(!partial_opsub(code,f[3],x[0],y));
  }
  SUBCASE("1"){
    auto x = ivar64(code);
    auto a = ivar64(code);
    auto f = pow(code,x,code.builtins.lits.two64);
    auto d = sub(code,x,a);
    partial_opsub(code,f,x,d);
    auto [fb,fe] = args(code, f);
    CHECK(fb == codes::exprfam(d));
    auto [xa,xb] = args(code, d);
    CHECK(xa == codes::exprfam(x));
    REQUIRE(xb.size() == 1);
    CHECK(xb[0] == codes::exprfam(a));
  }
  SUBCASE("2"){
    auto f = add(code,{code.builtins.lits.one64,code.builtins.lits.one64});
    auto g = code.builtins.lits.two64;
    auto h = opsub1(code,f,opsubs::pairwise(views::pin(opsubs::pair{f,g})));
    REQUIRE(h == id(g));
  }

}

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
