#include<array>
#include<ranges>

#include<fmt/core.h>

#include<hysj/codes/code.hpp>
#include<hysj/codes/eval.hpp>
#include<hysj/codes/to_string.hpp>
#include<hysj/codes/loops.hpp>
#include<hysj/codes/factories.hpp>

#include<framework.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

TEST_CASE("codes.loops"){

  using namespace factories;

  codes::code c{};

  auto equal_msg = [&](id a, id b){
    return fmt::format("{} ≠ {}", to_string(c, a), to_string(c, b));
  };

  #define CHECK_EQUAL_SYM(a,b)                            \
    {                                                     \
      CHECK_MESSAGE(equal(c, a, b), equal_msg(a, b));     \
    }

  #define CHECK_EQUAL_ITERATOR(i0,i1)    \
    {                                    \
      CHECK_EQUAL_SYM(i0.sym, i1.sym);   \
      CHECK_EQUAL_SYM(i0.test, i1.test); \
      CHECK_EQUAL_SYM(i0.init, i1.init); \
      CHECK_EQUAL_SYM(i0.head, i1.head); \
      CHECK_EQUAL_SYM(i0.tail, i1.tail); \
    }

  #define CHECK_EQUAL_LOOP(l0,l1)                       \
    {                                                   \
      CHECK(l0.size() == l1.size());                    \
      for(const auto &[i0, i1]:std::views::zip(l0, l1)) \
        CHECK_EQUAL_ITERATOR(i0, i1);                   \
    }


  SUBCASE("iterator"){
    auto n0 = icst32(c, 10);
    auto i0 = itr(c, n0);
    loop_iterator li0_expected{
      i0,
      {lt(c, cur(c, i0), n0)},
      {put(c, cur(c, i0), ozero(c, i0))},
      c.builtins.noop,
      {put(c, cur(c, i0), inc(c, cur(c, i0)))}
    };
    auto li0 = make_basic_loop_iterator(c, i0);
    CHECK_EQUAL_ITERATOR(li0, li0_expected);
  }
  SUBCASE("basic"){
    SUBCASE("0"){
      auto x = ivar32(c);
      CHECK(make_basic_loop(c, {x}).empty());
      auto f = add(c, {x,x});
      CHECK(make_basic_loop(c, {f}).empty());
    }
    SUBCASE("1"){
      auto n0 = icst32(c, 10), n1 = icst32(c, 10);
      auto i0 = itr(c, n0), i1 = itr(c, n1);
      auto x0 = ivar32(c), x1 = ivar32(c, {i0}), x2 = ivar32(c, {i1}),
        x3 = ivar32(c, {i0, i1});

      CHECK(make_basic_loop(c, {x0}).empty());
      {
        auto l1 = make_basic_loop(c, {x1});
        std::vector l1_expected{make_basic_loop_iterator(c, i0)};
        CHECK_EQUAL_LOOP(l1, l1_expected);
      }
      {
        auto l2 = make_basic_loop(c, {x2});
        std::vector l2_expected{make_basic_loop_iterator(c, i1)};
        CHECK_EQUAL_LOOP(l2, l2_expected);
      }
      {
        auto l3 = make_basic_loop(c, {x3});
        std::vector l3_expected{
          make_basic_loop_iterator(c, i0),
          make_basic_loop_iterator(c, i1)
        };
        CHECK_EQUAL_LOOP(l3, l3_expected);
        auto f = add(c, {x1, x1, x2});
        auto l4 = make_basic_loop(c, {f});
        CHECK_EQUAL_LOOP(l4, l3_expected);
      }
    }
  }
  SUBCASE("inner"){
    auto n0 = icst32(c, 10), n1 = icst32(c, 10);
    auto i0 = itr(c, n0), i1 = itr(c, n1);
    auto x0 = ivar32(c, {i0, i1});
    auto f0 = con(c, add(c, {x0}), {i1}),
      f1 = con(c, add(c, {x0}), {i0}),
      f2 = con(c, add(c, {x0}), {i0, i1}),
      f3 = con(c, add(c, {x0}), {i1, i0});

    {
      auto l0 = make_inner_loop(c, {f0});
      std::vector l0_expected{make_basic_loop_iterator(c,i1)};
      CHECK_EQUAL_LOOP(l0, l0_expected);
    }
    {
      auto l1 = make_inner_loop(c, {f1});
      std::vector l1_expected{make_basic_loop_iterator(c,i0)};
      CHECK_EQUAL_LOOP(l1, l1_expected);
    }
    {
      auto l2 = make_inner_loop(c, {f2});
      std::vector l2_expected{make_basic_loop_iterator(c,i0), make_basic_loop_iterator(c,i1)};
      CHECK_EQUAL_LOOP(l2, l2_expected);
    }
    {
      auto l3 = make_inner_loop(c, {f3});
      std::vector l3_expected{make_basic_loop_iterator(c,i1), make_basic_loop_iterator(c,i0)};
      CHECK_EQUAL_LOOP(l3, l3_expected);
    }
  }
  SUBCASE("bulk"){
    SUBCASE("0"){
      //rank = 0
    }
    SUBCASE("1"){
      //rank = 1, thread_count = 1
    }
  }

}

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
