#include<algorithm>
#include<vector>
#include<ranges>
#include<optional>
#include<stdexcept>

#include<hysj/codes/code.hpp>
#include<hysj/codes/schedules.hpp>
#include<hysj/codes/factories.hpp>
#include<hysj/tools/graphs/elements.hpp>

#include<framework.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

TEST_CASE("codes.schedules"){

  using namespace factories;
  using graphs::operator ""_v;

  codes::code c{};
  codes::sched s{};

  auto x = ivar32(c);
  std::vector P{
    put(c, x, c.builtins.lits.zero32),
    put(c, x, c.builtins.lits.zero32),
    put(c, x, c.builtins.lits.zero32)
  };
  SUBCASE("1"){
    s = sched::make(c, {P[0]});
    CHECK(graphs::order(s.graph) == 1);
    CHECK(graphs::size(s.graph) == 0);
    CHECK(s.graph[graphs::vertex_id{0}].id() == P[0].id());
  }
  SUBCASE("2"){
    auto w = when(c, P);
    s = sched::make(c, {w});
    CHECK(graphs::order(s.graph) == 3);
    CHECK(graphs::size(s.graph) == 0);
    for(std::size_t i:views::indices(P))
      CHECK(s.graph[graphs::vertex_id{i}].id() == P[i].id());
  }
  SUBCASE("3"){
    auto w = then(c, P);
    s = sched::make(c, {w});
    CHECK(graphs::order(s.graph) == 3);
    CHECK(graphs::size(s.graph) == 2);
    for(std::size_t i:views::indices(P))
      CHECK(s.graph[graphs::vertex_id{i}].id() == P[i].id());
    CHECK(s.graph.has_edge(0_v, 1_v));
    CHECK(s.graph.has_edge(1_v, 2_v));
  }
  SUBCASE("4"){
    auto w = when(c, {then(c, {P[0], P[1]}), P[2]});
    s = sched::make(c, {w});
    CHECK(graphs::order(s.graph) == 3);
    CHECK(graphs::size(s.graph) == 1);
    for(std::size_t i:views::indices(P))
      CHECK(s.graph[graphs::vertex_id{i}].id() == P[i].id());
    CHECK(s.graph.has_edge(0_v, 1_v));
  }
  SUBCASE("5"){
    auto w = then(c, {P[0], P[1], P[0]});
    s = sched::make(c, {w});
    CHECK(graphs::order(s.graph) == 3);
    CHECK(graphs::size(s.graph) == 2);
    for(std::size_t i:views::indices(P))
      CHECK(s.graph[graphs::vertex_id{i}].id() == P[i % 2].id());
    CHECK(s.graph.has_edge(0_v, 1_v));
    CHECK(s.graph.has_edge(1_v, 2_v));
  }
  SUBCASE("6"){
    auto w = when(c, {when(c, {P[0]}), when(c, {P[0]})});
    s = sched::make(c, {w});
    CHECK(graphs::order(s.graph) == 1);
    CHECK(graphs::size(s.graph) == 0);
    CHECK(s.graph[graphs::vertex_id{0}].id() == P[0].id());
  }
  SUBCASE("7"){
    auto w = when(c, {});
    s = sched::make(c, {w});
    CHECK(graphs::order(s.graph) == 0);
    CHECK(graphs::size(s.graph) == 0);
  }
  SUBCASE("8"){
    auto d = dev(c);
    auto t = on(c, d, when(c));
    s = sched::make(c, {t});
    CHECK(graphs::order(s.graph) == 1);
    CHECK(graphs::size(s.graph) == 0);
    CHECK(s.graph[graphs::vertex_id{0}].id() == t.id());
  }
  SUBCASE("9"){
    auto d = dev(c);
    auto t = to(c, d, d, x);
    s = sched::make(c, {t});
    CHECK(graphs::order(s.graph) == 1);
    CHECK(graphs::size(s.graph) == 0);
    CHECK(s.graph[graphs::vertex_id{0}].id() == t.id());
  }

}

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
