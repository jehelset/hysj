#include<cmath>
#include<functional>
#include<limits>
#include<optional>
#include<numbers>
#include<numeric>

#include<hysj/codes/code.hpp>
#include<hysj/codes/factories.hpp>
#include<hysj/codes/eval.hpp>

#include<framework.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

TEST_CASE("codes.eval"){

  codes::code code;

  SUBCASE("invalid"){
    auto n = icst64(code, 1);
    auto i = itr(code, n);
    auto x = rvar64(code, {itr(code, n)});
    auto t = rvar64(code);
    auto p = put(code, x, rcst64(code, 0.0));
    CHECK(bcceval(code, code.builtins.types.r16) == std::nullopt);
    CHECK(icceval(code, i) == std::nullopt);
    CHECK(rcceval(code, con(code, x)) == std::nullopt);
    CHECK(rcceval(code, rfl(code, x)) == std::nullopt);
    CHECK(rcceval(code, x) == std::nullopt);
    CHECK(rcceval(code, rot(code, x, {i})) == std::nullopt);
    CHECK(rcceval(code, der(code, x, t)) == std::nullopt);
    CHECK(bcceval(code, p) == std::nullopt);
    CHECK(bcceval(code, then(code, {p})) == std::nullopt);
    CHECK(bcceval(code, when(code, {p})) == std::nullopt);
  }
  SUBCASE("builtins"){
    for(auto w:widthtab){
      SUBCASE(fmt::format("w{}",w).c_str()){};
      CHECK(ncceval(code, code.builtins.widths[w]) == w);
      using enum literal;
      CHECK(bcceval(code, code.builtins.lits[bot, w]) == 0);
      CHECK(bcceval(code, code.builtins.lits[top, w]) == 1);
      CHECK(icceval(code, code.builtins.lits[zero, w]) == 0);
      CHECK(icceval(code, code.builtins.lits[one, w]) == 1);
      CHECK(icceval(code, code.builtins.lits[two, w]) == 2);
      CHECK(icceval(code, code.builtins.lits[negone, w]) == -1);
      CHECK(rcceval(code, code.builtins.lits[inf, w]) == std::numeric_limits<rcell<>>::infinity());
      CHECK(rcceval(code, code.builtins.lits[neginf, w]) == -std::numeric_limits<rcell<>>::infinity());
      CHECK(rcceval(code, code.builtins.lits[pi, w]) == approx(std::numbers::pi_v<rcell<>>));
      CHECK(rcceval(code, code.builtins.lits[e, w]) == approx(std::numbers::e_v<rcell<>>));
    }
  }
  SUBCASE("cst"){

    CHECK(bcceval(code, bcst64(code, false)) == 0);
    CHECK(ncceval(code, ncst64(code, 4)) == 4);
    CHECK(icceval(code, icst64(code, -44)) == -44);
    CHECK(rcceval(code, rcst64(code, 0.4)) == 0.4);
    
  }
  SUBCASE("cvt"){

    CHECK(bcceval(code, bcvt64(code, ncst64(code,  0))) == 0);
    CHECK(bcceval(code, bcvt64(code, ncst64(code,  1))) == 1);
    CHECK(bcceval(code, bcvt64(code, icst64(code, -1))) == 1);

    CHECK(ncceval(code, ncvt64(code, bcst64(code, 0))) == 0);
    CHECK(ncceval(code, ncvt64(code, bcst64(code, 1))) == 1);
    CHECK(ncceval(code, ncvt64(code, icst64(code, 4))) == 4);
    CHECK(ncceval(code, ncvt32(code, ncst64(code, 4))) == 4);

    CHECK(icceval(code, icvt32(code, icst64(code, 4))) == 4);
    CHECK(icceval(code, icvt64(code, ncst64(code, 4))) == 4);

    CHECK(ncceval(code, cvt(code, rfl(code, ncst64(code, 4)), icst64(code, 4))) == 4);
    CHECK(icceval(code, cvt(code, rfl(code, icst64(code, 4)), ncst64(code, 4))) == 4);
  }
  SUBCASE("neg"){

    CHECK(icceval(code, neg(code, icst64(code,  2))) == -2);
    CHECK(icceval(code, neg(code, icst64(code, -2))) == 2);

    CHECK(rcceval(code, neg(code, rcst64(code,  2.0))) == -2.0);
    CHECK(rcceval(code, neg(code, rcst64(code, -2.0))) ==  2.0);

  }
  SUBCASE("sin"){
    CHECK(rcceval(code, sin(code, rcst64(code,  2.0))) == approx(std::sin(2.0)));
  }
  SUBCASE("bnot"){
    CHECK(ncceval(code, bnot(code, ncst64(code, 0b101))) == ~0b101);
    CHECK(ncceval(code, bnot(code, ncst64(code, 0b0))) == ~0b0);
  }
  SUBCASE("lnot"){
    CHECK(bcceval(code, lnot(code, bcst64(code, 0))) == !0);
    CHECK(bcceval(code, lnot(code, bcst64(code, 1))) == !1);
  }
  SUBCASE("fct"){
    CHECK(ncceval(code, fct(code, ncst64(code, 4))) == 4*3*2*1);
    CHECK(icceval(code, fct(code, icst64(code, 5))) == 5*4*3*2*1);
  }
  SUBCASE("div"){
    CHECK(ncceval(code, div(code, ncst64(code, 4), ncst64(code, 2))) == 4 / 2);
    CHECK(icceval(code, div(code, icst64(code, 7), icst64(code, 3))) == 7 / 3);
    CHECK(rcceval(code, div(code, rcst64(code, 2.4), rcst64(code, 6.9))) == 2.4 / 6.9);
  }
  SUBCASE("log"){
    CHECK(rcceval(code, log(code, rcst64(code, 2.4), rcst64(code, 6.9))) == std::log(2.4) / std::log(6.9));
  }
  SUBCASE("blsh"){
    CHECK(ncceval(code, blsh(code, ncst64(code, 4), ncst64(code, 2))) == 4 << 2);
    CHECK(icceval(code, blsh(code, icst64(code, 7), icst64(code, 3))) == 7 << 3);
  }
  SUBCASE("brsh"){
    CHECK(ncceval(code, brsh(code, ncst64(code, 4), ncst64(code, 2))) == 4 >> 2);
    CHECK(icceval(code, brsh(code, icst64(code, 7), icst64(code, 3))) == 7 >> 3);
  }
  SUBCASE("eq"){
    CHECK(bcceval(code, eq(code, bcst64(code, 0), bcst64(code, 0))) == (0 == 0));
    CHECK(bcceval(code, eq(code, ncst64(code, 4), ncst64(code, 2))) == (4 == 2));
    CHECK(bcceval(code, eq(code, icst64(code, 7), icst64(code, 3))) == (7 == 3));
    CHECK(bcceval(code, eq(code, rcst64(code, 2.4), rcst64(code, 6.9))) == (2.4 == 6.9));
  }
  SUBCASE("lt"){
    CHECK(bcceval(code, lt(code, bcst64(code, 0), bcst64(code, 0))) == (0 < 0));
    CHECK(bcceval(code, lt(code, ncst64(code, 4), ncst64(code, 2))) == (4 < 2));
    CHECK(bcceval(code, lt(code, icst64(code, 7), icst64(code, 3))) == (7 < 3));
    CHECK(bcceval(code, lt(code, rcst64(code, 2.4), rcst64(code, 6.9))) == (2.4 < 6.9));
  }
  SUBCASE("mod"){
    CHECK(ncceval(code, mod(code, ncst64(code, 4), ncst64(code, 2))) == 4 % 2);
    CHECK(icceval(code, mod(code, icst64(code, 7), icst64(code, 3))) == 7 % 3);
    CHECK(rcceval(code, mod(code, rcst64(code, 2.4), rcst64(code, 6.9))) == std::fmod(2.4, 6.9));
    CHECK(icceval(code, mod(code, icst64(code, 7), icst64(code, -3))) == ((7 % 3) + 3) % 3);
  }
  SUBCASE("pow"){
    CHECK(ncceval(code, pow(code, ncst64(code, 4), ncst64(code, 2))) == 4 * 4);
    CHECK(icceval(code, pow(code, icst64(code, 7), icst64(code, 3))) == 7 * 7 * 7);
    CHECK(rcceval(code, pow(code, rcst64(code, 2.4), rcst64(code, 6.9))) == std::pow(2.4, 6.9));
  }
  SUBCASE("add"){
    CHECK(ncceval(code, add(code, {ncst64(code, 4)})) == 4);
    CHECK(icceval(code, add(code, {icst64(code, 7)})) == 7);
    CHECK(rcceval(code, add(code, {rcst64(code, 2.4)})) == 2.4);
    CHECK(ncceval(code, add(code, {ncst64(code, 4), ncst64(code, 2)})) == 4 + 2);
    CHECK(icceval(code, add(code, {icst64(code, 7), icst64(code, 3)})) == 7 + 3);
    CHECK(rcceval(code, add(code, {rcst64(code, 2.4), rcst64(code, 6.9)})) == 2.4 + 6.9);
  }
  SUBCASE("mul"){
    CHECK(ncceval(code, mul(code, {ncst64(code, 4)})) == 4);
    CHECK(icceval(code, mul(code, {icst64(code, 7)})) == 7);
    CHECK(rcceval(code, mul(code, {rcst64(code, 2.4)})) == 2.4);
    CHECK(ncceval(code, mul(code, {ncst64(code, 4), ncst64(code, 2)})) == 4 * 2);
    CHECK(icceval(code, mul(code, {icst64(code, 7), icst64(code, 3)})) == 7 * 3);
    CHECK(rcceval(code, mul(code, {rcst64(code, 2.4), rcst64(code, 6.9)})) == 2.4 * 6.9);
  }
  SUBCASE("bor"){
    CHECK(ncceval(code, bor(code, {ncst64(code, 0)})) == 0);
    CHECK(ncceval(code, bor(code, {ncst64(code, 12), ncst64(code, 32)})) == (12 | 32));
    CHECK(icceval(code, bor(code, {icst64(code, 8)})) == 8);
    CHECK(icceval(code, bor(code, {icst64(code, -1), icst64(code, 23)})) == (-1 | 23));
  }
  SUBCASE("band"){
    CHECK(ncceval(code, band(code, {ncst64(code, 0)})) == 0);
    CHECK(ncceval(code, band(code, {ncst64(code, 12), ncst64(code, 32)})) == (12 & 32));
    CHECK(icceval(code, band(code, {icst64(code, 8)})) == 8);
    CHECK(icceval(code, band(code, {icst64(code, -1), icst64(code, 23)})) == (-1 & 23));
  }
  SUBCASE("bxor"){
    CHECK(ncceval(code, bxor(code, {ncst64(code, 0)})) == 0);
    CHECK(ncceval(code, bxor(code, {ncst64(code, 12), ncst64(code, 32)})) == (12 ^ 32));
    CHECK(icceval(code, bxor(code, {icst64(code, 8)})) == 8);
    CHECK(icceval(code, bxor(code, {icst64(code, -1), icst64(code, 23)})) == (-1 ^ 23));
  }
  SUBCASE("lor"){
    CHECK(bcceval(code, lor(code, {bcst64(code, 0)})) == 0);
    CHECK(bcceval(code, lor(code, {bcst64(code, 1), bcst64(code, 0)})) == (1 || 0));
  }
  SUBCASE("land"){
    CHECK(bcceval(code, land(code, {bcst64(code, 0)})) == 0);
    CHECK(bcceval(code, land(code, {bcst64(code, 1), bcst64(code, 0)})) == (1 && 0));
  }
  SUBCASE("sub"){
    CHECK(ncceval(code, sub(code, ncst64(code, 4))) == 4);
    CHECK(icceval(code, sub(code, icst64(code, 7))) == 7);
    CHECK(rcceval(code, sub(code, rcst64(code, 2.4))) == 2.4);
    CHECK(ncceval(code, sub(code, ncst64(code, 4), {ncst64(code, 2)})) == 4 - 2);
    CHECK(icceval(code, sub(code, icst64(code, 7), {icst64(code, 3)})) == 7 - 3);
    CHECK(rcceval(code, sub(code, rcst64(code, 2.4), {rcst64(code, 6.9), rcst64(code, 8.8)})) == 2.4 - 6.9 - 8.8);
  }
  SUBCASE("sel"){
    CHECK(bcceval(code, sel(code, ncst64(code, 0), {bcst64(code, 0), bcst64(code, 1)})) == 0);
    CHECK(ncceval(code, sel(code, ncst64(code, 1), {ncst64(code, 2), ncst64(code, 3)})) == 3);
    CHECK(icceval(code, sel(code, ncst64(code, 0), {icst64(code, 3), icst64(code, 2), icst64(code, 1)})) == 3);
    CHECK(rcceval(code, sel(code, ncst64(code, 0), {rcst64(code, 6.9)})) == 6.9);
  }

}

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
