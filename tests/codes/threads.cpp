#include<functional>
#include<vector>
#include<stdexcept>

#include<fmt/core.h>

#include<hysj/codes/code.hpp>
#include<hysj/codes/factories.hpp>
#include<hysj/codes/threads.hpp>
#include<hysj/codes/to_string.hpp>
#include<hysj/tools/functional/bind.hpp>
#include<hysj/tools/functional/optional.hpp>
#include<hysj/tools/functional/ignore.hpp>
#include<hysj/tools/ranges/utility.hpp>

#include<framework.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

TEST_CASE("codes.threads.thread_partitions"){
  std::vector<integer> extents;
  CHECK(thread_partition(1, std::span{extents = {1}}) == 1);
  CHECK(thread_partition(2, std::span{extents = {1}}) == 1);
  CHECK(thread_partition(1, std::span{extents = {2}}) == 1);
  CHECK(thread_partition(1, std::span{extents = {1}}) == 1);
  CHECK(thread_partition(2, std::span{extents = {1}}) == 1);
  CHECK(thread_partition(1, std::span{extents = {2}}) == 1);
  CHECK(thread_partition(1, std::span{extents = {1, 1}}) == 1);
  CHECK(thread_partition(1, std::span{extents = {1, 2}}) == 1);
  CHECK(thread_partition(1, std::span{extents = {2, 1}}) == 1);
  CHECK(thread_partition(2, std::span{extents = {1, 2}}) == 2);
  CHECK(thread_partition(2, std::span{extents = {2, 1}}) == 2);
  CHECK(thread_partition(1, std::span{extents = {1, 3}}) == 1);
  CHECK(thread_partition(3, std::span{extents = {1, 3}}) == 3);
  CHECK(thread_partition(4, std::span{extents = {1, 3}}) == 3);
  CHECK(thread_partition(2, std::span{extents = {2, 3}}) == 2);
  CHECK(thread_partition(4, std::span{extents = {2, 3}}) == 2);
  CHECK(thread_partition(4, std::span{extents = {2, 3*2}}) == 4);
  CHECK(thread_partition(8, std::span{extents = {2, 2*4}}) == 8);
}
TEST_CASE("codes.threads.thread_pivot"){
  std::vector<integer> extents;
  CHECK(thread_pivot(1, std::span{extents = {1}}) == 0);
  CHECK(thread_pivot(2, std::span{extents = {1}}) == 0);
  CHECK(thread_pivot(1, std::span{extents = {2}}) == 0);
  CHECK(thread_pivot(1, std::span{extents = {1}}) == 0);
  CHECK(thread_pivot(2, std::span{extents = {1}}) == 0);
  CHECK(thread_pivot(1, std::span{extents = {2}}) == 0);
  CHECK(thread_pivot(1, std::span{extents = {1, 1}}) == 1);
  CHECK(thread_pivot(1, std::span{extents = {1, 2}}) == 1);
  CHECK(thread_pivot(1, std::span{extents = {2, 1}}) == 0);
  CHECK(thread_pivot(2, std::span{extents = {1, 2}}) == 1);
  CHECK(thread_pivot(2, std::span{extents = {2, 1}}) == 1);
  CHECK(thread_pivot(1, std::span{extents = {1, 3}}) == 1);
  CHECK(thread_pivot(3, std::span{extents = {1, 3}}) == 1);
  CHECK(thread_pivot(4, std::span{extents = {1, 3}}) == 1);
  CHECK(thread_pivot(2, std::span{extents = {2, 3}}) == 1);
  CHECK(thread_pivot(4, std::span{extents = {2, 3}}) == 1);
  CHECK(thread_pivot(4, std::span{extents = {2, 3*2}}) == 1);
  CHECK(thread_pivot(8, std::span{extents = {2, 2*4}}) == 1);
}
TEST_CASE("codes.threads.thread"){

  using namespace factories;
  code c{};

  const auto N = 16_i;

  std::vector M{
          1_i,
      N / 2_i,
          N  ,
    2_i * N
  };
  auto E = ranges::vec(M, bind<>(lift((icst32)), std::ref(c)));
  auto X = ranges::vec(E, [&](auto e){ return rvar32(c, {itr(c, e)}); });
  auto b = rvar32(c);
  auto P = ranges::vec(
    X,
    bind<0, -1>(put, std::ref(c), icst32(c, 0)));

  SUBCASE("put"){
    auto T = thread_info::make(c, N);

    SUBCASE("0"){
      for(auto i:views::indices(M))
        CHECK(thread(c, T, {P[i]}) == std::min(N, M[i]));
    }
    SUBCASE("1"){
      auto f = put(c, ivar32(c), icst32(c, 0));
      CHECK(thread(c, T, {f}) == 1_i);
    }
  }
  SUBCASE("noop"){
    auto x = noop(c);
    auto T = thread_info::make(c, N);
    CHECK(thread(c, T, {x}) == std::nullopt);
  }
  SUBCASE("exit"){
    auto x = exit(c, icst32(c, 0));
    auto T = thread_info::make(c, N);
    CHECK(thread(c, T, {x}) == std::nullopt);
  }
  SUBCASE("then"){
    SUBCASE("0"){
      for(auto i:views::indices(M)){
        auto x = then(c, {P[i]});
        auto T = thread_info::make(c, N);
        CHECK(thread(c, T, {x}) == std::min(N, M[i]));
      }
    }
    SUBCASE("1"){
      auto x = then(c, {P[2], P[3]});
      auto T = thread_info::make(c, N);
      CHECK(thread(c, T, {x}) == std::min(N, std::max(M[2], M[3])));
    }
    SUBCASE("2"){
      auto x = then(c, {P[1], P[2]});
      auto T = thread_info::make(c, N);
      CHECK(thread(c, T, {x}) == std::min(N, std::max(M[1], M[2])));
    }
    SUBCASE("3"){
      auto x = then(c, {P[0], P[1]});
      auto T = thread_info::make(c, N);
      CHECK(thread(c, T, {x}) == std::min(N, std::max(M[0], M[1])));
    }
  }
  SUBCASE("when"){
    SUBCASE("0"){
      for(auto i:views::indices(M)){
        auto x = when(c, {P[i]});
        auto T = thread_info::make(c, N);
        CHECK(thread(c, T, {x}) == std::min(N, M[i]));
      }
    }
    SUBCASE("1"){
      auto x = when(c, {P[2], P[3]});
      auto T = thread_info::make(c, N);
      CHECK(thread(c, T, {x}) == std::min(N, std::max(M[2], M[3])));
    }
    SUBCASE("2"){
      auto x = when(c, {P[1], P[2]});
      auto T = thread_info::make(c, N);
      CHECK(thread(c, T, {x}) == std::min(N, std::max(M[1], M[2])));
    }
    SUBCASE("3"){
      auto x = when(c, {P[0], P[1]});
      auto T = thread_info::make(c, N);
      CHECK(thread(c, T, {x}) == std::min(N, std::max(M[0], M[1])));
    }
  }
  SUBCASE("loop"){
    SUBCASE("0"){
      auto x = loop(c, noop(c));
      auto T = thread_info::make(c, N);
      CHECK(thread(c, T, {x}) == std::nullopt);
    }
    SUBCASE("1"){
      auto x = loop(c, P[1]);
      auto T = thread_info::make(c, N);
      CHECK(thread(c, T, {x}) == std::min(N, M[1]));
    }
  }
  SUBCASE("fork"){
    auto x = fork(c, b, {P[1], c.builtins.noop});
    auto T = thread_info::make(c, N);
    CHECK(thread(c, T, {x}) == std::min(N, M[1]));
  }
  SUBCASE("on"){
    auto d = dev(c);
    SUBCASE("0"){
      auto x = on(c, d, noop(c));
      auto T = thread_info::make(c, N);
      CHECK(thread(c, T, {x}) == std::nullopt);
    }
    SUBCASE("1"){
      auto x = on(c, d, P[1]);
      auto T = thread_info::make(c, N);
      CHECK(thread(c, T, {x}) == std::min(N, M[1]));
    }
  }
  SUBCASE("to"){
    auto d0 = dev(c), d1 = dev(c);
    auto x = to(c, d0, d1, X[0]);
    auto T = thread_info::make(c, N);
    CHECK(thread(c, T, {x}) == std::nullopt);
  }
}

TEST_CASE("codes.threads.fork"){

  using namespace factories;
  code c{};

  const auto N = 16;

  thread_info T{
    .global_count = N
  };


  std::vector M{
        1,
    N / 2,
        N,
    2 * N
  };
  auto E = ranges::vec(M, bind<>(lift((icst32)), std::ref(c)));
  auto X = ranges::vec(E, [&](auto e){ return rvar32(c, {itr(c, e)}); });
  auto b = rvar32(c);
  auto P = ranges::vec(
    X,
    bind<0, -1>(put, std::ref(c), icst32(c, 0)));

  auto t = ivar32(c);

  auto forked_to = [&](std::optional<taskfam> y,std::optional<taskfam> z = {}){
    if(!y && !z)
      return true;
    else if(y && z)
      return equal(c, *y, *z);
    else
      return false;
  };
  auto thread_and_fork = [&](taskfam a){
    thread(c, T, a);
    return codes::thread_fork(c, T, {t}, a);
  };
  auto thread_fork = [&](taskfam a, exprfam n){
    return codes::thread_fork(c, {t}, a, n);
  };

  auto forky_task = [&]{
    auto x0 = P[0];
    auto x1 = P[1];
    auto x2 = when(c, {x0, x1});
    auto z0 = thread_fork({x0}, {E[0]});
    auto z1 = when(c, {z0, x1});
    return kumi::make_tuple(x2, z1);
  };

  auto msg = [&](taskfam a,std::optional<taskfam> y,std::optional<taskfam> z = {}){
    auto s = bind<>(lift((to_string)), std::ref(c));
    return fmt::format("{} → {} ≄ {}", s(a), y.transform(s), z.transform(s));
  };

  #define CHECK_FORKS_TO(a, y, z) \
    CHECK_MESSAGE(forked_to(y, z), msg(a, y, z));
  #define CHECK_NO_FORK(a, y) \
    CHECK_FORKS_TO(a, y, {});
  
  SUBCASE("put"){
    for(auto [x, m, e]:std::views::zip(P, M, E))
      SUBCASE(fmt::format("m = {}", m).c_str()){
        auto y = thread_and_fork({x});
        CHECK_NO_FORK({x}, y);
      }
  }
  SUBCASE("noop"){
    auto x = noop(c);
    auto z = thread_and_fork({x});
    CHECK_NO_FORK({x}, z);
  }
  SUBCASE("exit"){
    auto x = exit(c, icst32(c, 0));
    auto z = thread_and_fork({x});
    CHECK_NO_FORK({x}, z);
  }
  SUBCASE("then"){
    SUBCASE("0"){
      auto x0 = P[1];
      auto x1 = when(c, {x0});
      auto y = thread_and_fork({x1});
      CHECK_NO_FORK({x1}, y);
    }
    SUBCASE("1"){
      auto x0 = P[2];
      auto x1 = P[3];
      auto x2 = then(c, {x0, x1});
      auto y = thread_and_fork({x2});
      CHECK_NO_FORK({x2}, y);
    }
    SUBCASE("2"){
      for(auto i:std::array{0,1}){
        auto x0 = P[i];
        auto x1 = P[i + 1];
        auto x2 = then(c, {x0, x1});
        auto y = thread_and_fork({x2});
        auto z0 = thread_fork({x0}, {E[i]});
        taskfam z1{then(c, {z0, x1})};
        CHECK_FORKS_TO({x2}, y, {z1});
      }
    }
  }
  SUBCASE("when"){
    SUBCASE("0"){
      auto x0 = P[1];
      auto x1 = when(c, {x0});
      auto y = thread_and_fork({x1});
      CHECK_NO_FORK({x1}, y);
    }
    SUBCASE("1"){
      auto x0 = P[2];
      auto x1 = P[3];
      auto x2 = when(c, {x0, x1});
      auto y = thread_and_fork({x2});
      CHECK_NO_FORK({x2}, y);
    }
    SUBCASE("2"){
      for(auto i:std::array{0,1}){
        auto x0 = P[i];
        auto x1 = P[i + 1];
        auto x2 = when(c, {x0, x1});
        auto y = thread_and_fork({x2});
        auto z0 = thread_fork({x0}, {E[i]});
        taskfam z1{when(c, {z0, x1})};
        CHECK_FORKS_TO({x2}, y, {z1});
      }
    }
  }
  SUBCASE("loop"){
    SUBCASE("0"){
      auto x = loop(c, noop(c));
      auto y = thread_and_fork({x});
      CHECK_NO_FORK({x}, y);
    }
    SUBCASE("1"){
      auto [x0, z0] = forky_task();
      auto x1 = loop(c, x0);
      auto y = thread_and_fork({x1});
      taskfam z1{loop(c, z0)};
      CHECK_FORKS_TO({x1}, y, {z1});
    }
  }
  SUBCASE("fork"){
    auto [x0, z0] = forky_task();
    auto x1 = fork(c, b, {x0, c.builtins.noop});
    auto y = thread_and_fork({x1});
    taskfam z1{fork(c, b, {z0, c.builtins.noop})};
    CHECK_FORKS_TO({x1}, y, {z1});
  }
  SUBCASE("on"){
    auto d = dev(c);
    SUBCASE("0"){
      auto x = on(c, d, noop(c));
      auto y = thread_and_fork({x});
      CHECK_NO_FORK({x}, y);
    }
    SUBCASE("1"){
      auto [x0, z0] = forky_task();
      auto x1 = on(c, d, x0);
      auto y = thread_and_fork({x1});
      taskfam z1{on(c, d, z0)};
      CHECK_FORKS_TO({x1}, y, {z1});
    }
  }
  SUBCASE("to"){
    auto d0 = dev(c), d1 = dev(c);
    auto x = to(c, d0, d1, X[0]);
    auto y = thread_and_fork({x});
    CHECK_NO_FORK({x}, y);
  }

  SUBCASE("bug"){
    SUBCASE("0"){
      std::vector I{
        itr(c, icst32(c, 2)).id(),
        itr(c, icst32(c, 2)).id()
      };
      auto v0 = ivar32(c, I);
      auto v1 = ivar32(c);

      auto p0 = put(c, v0, v0);
      auto p1 = put(c, v1, v1);

      auto x0 = then(c, {p1});
      auto x1 = when(c,
           {
             p0,
             x0
           });
      auto y = thread_and_fork({x1});

      auto z0 = thread_fork({x0}, {icst32(c, 1)});
      taskfam z1{
        when(c,
             {
               p0,
               z0
             })
      };
      CHECK_FORKS_TO({x1}, y, {z1});
    }
    SUBCASE("1"){
      auto i = itr(c, icst32(c, 2));
      auto v0 = ivar32(c, {i});
      auto v1 = ivar32(c);
      auto v2 = ivar32(c);

      auto p0 = put(c, v1, v1);
      auto p1 = put(c, v0, v0);
      auto p2 = put(c, v2, v2);

      auto x0 = then(c, {
          p0,
          p1,
          p2
      });
      auto x1 = loop(c, x0);

      auto y = thread_and_fork({x1});

      auto z0 = thread_fork({p0}, {icst32(c, 1)});
      auto z2 = thread_fork({p2}, {icst32(c, 1)});
      taskfam z1{
        loop(c,
             then(c,
                  {
                    z0,
                    p1,
                    z2
                  }))
      };
      CHECK_FORKS_TO({x1}, y, {z1});

    }
  }
}

} //hysj::codes
#include<hysj/tools/epilogue.hpp>
