#include<cmath>
#include<limits>

#include"framework.hpp"

#include<fmt/core.h>
#include<fmt/std.h>

#include<hysj/codes.hpp>
#include<hysj/tools/ranges/span.hpp>
#include<hysj/tools/types.hpp>
#include<hysj/tools/functional/apply.hpp>
#include<hysj/binaries/spirv.hpp>
#include<hysj/binaries/gcc.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::binaries{

TEST_CASE("binaries.spv.dis"){
  codes::code code{};
  spvenv env{.optimization_level = 0};
  auto n = icst64(code,64);
  auto [t, b] = obs(code, n);
  compile(code);
  auto bin = spvcc(env, code, codes::task(t));
  REQUIRE(bin.tape.size() > 0);
  bin.tape[0] = 0;
  CHECK_THROWS(spverr(bin));
}

struct spirv_test_fixture{
  HYSJ_TEST_FIXTURE(("spv"));

  static constexpr auto integral_widths = std::array{8, 16, 32, 64};
  static constexpr auto floating_point_widths = std::array{16, 32, 64};

  spvenv env{.optimization_level = 0};

  auto cc(codes::code &code, codes::taskfam task){
    return spvcc(env, code, task);
  }

  auto err(const spvbin &bin){
    spverr(bin);
  }
};

struct gcc_test_fixture{
  HYSJ_TEST_FIXTURE(("gcc"));

  static constexpr auto integral_widths = std::array{8, 16, 32, 64};
  static constexpr auto floating_point_widths = std::array{32, 64};

  gcccfg cfg{.optimization_level = 0};

  auto cc(codes::code &code, codes::taskfam task){
    return gcccc(cfg, code, task);
  }

  static auto err(const gccbin &){}
};


TEST_CASE_TEMPLATE("binaries", test_fixture, spirv_test_fixture, gcc_test_fixture){

  test_fixture fixture{};
  FIXTURE(fixture){

    using namespace codes::factories;

    codes::code code{};

    auto cc = [&](codes::taskfam task){
      compile(code);
      return fixture.cc(code, task);
    };

    #define WITH_ENVS(envid, fixture_widths)              \
      natural width_c{};                                  \
      id width{};                                         \
      for(auto width_:fixture_widths)                     \
        SUBCASE(fmt::format("w{}",width_).c_str()){       \
          width = code.builtins.widths[width_c = width_]; \
        }
    #define WITH_IENVS(envid) WITH_ENVS(envid, fixture.integral_widths)
    #define WITH_FENVS(envid) WITH_ENVS(envid, fixture.floating_point_widths)

    SUBCASE("cst"){
      SUBCASE("0"){
        WITH_IENVS(env);
        auto n = icst(code,64,width);
        auto [t, b] = obs(code, n);
        auto bin = cc({t});
        CHECK_NOTHROW(fixture.err(bin));
      }
      SUBCASE("1"){
        WITH_FENVS(env);
        auto n = rcst(code,32.0,width);
        auto [t, b] = obs(code, n);
        auto bin = cc({t});
        CHECK_NOTHROW(fixture.err(bin));
      }
      SUBCASE("2"){
        WITH_FENVS(env);
        auto n = icst(code,64,width);
        auto a = rvar(code,width,{itr(code, n)});
        auto f = add(code,{a,a});

        auto [t, b] = obs(code, f);
        auto bin = cc({t});
        CHECK_NOTHROW(fixture.err(bin));
      }
    }

    SUBCASE("add"){
      SUBCASE("0"){
        WITH_FENVS(env);

        auto a = icst(code, -1, width),
          b = rcst(code, std::numeric_limits<real>::infinity(), width);
        auto f = add(code,{a,b});
        auto [t, u] = obs(code, f);

        auto bin = cc({t});
        CHECK_NOTHROW(fixture.err(bin));
      }
      SUBCASE("1"){
        WITH_FENVS(env);

        auto n = icst(code,4, width);
        auto i = itr(code, n), j = itr(code, n);

        auto a = rvar(code, width,{i,j});
        auto b = rvar(code, width,{i,j});
        auto c = add(code,{a,a});
        auto f = put(code,b,c);

        auto bin = cc({f});
        CHECK_NOTHROW(fixture.err(bin));
      }
      SUBCASE("2"){
        WITH_IENVS(env);
        auto n = icst(code, 64, width);
        auto m = icst(code, 4, width);
        auto i = itr(code, n), j = itr(code, m);
        auto a = ivar(code, width, {i,j});
        auto f = add(code,{a,a});
        auto [t,b] = obs(code, f);
        auto bin = cc({t});
        CHECK_NOTHROW(fixture.err(bin));
      }
    }

    SUBCASE("sin"){
      WITH_FENVS(env);

      auto a = rcst(code,0.0_r, width);
      auto f = sin(code,a);
      auto [t, b] = obs(code, f);

      auto bin = cc({t});
      CHECK_NOTHROW(fixture.err(bin));
    }

    SUBCASE("con"){

      SUBCASE("0"){
        WITH_FENVS(env);

        auto n = icst(code, 4, width);
        auto i = itr(code, n);
        auto a = rvar(code, width, {i});

        auto f = con(code, add(code, {a}), {i});
        auto [t,b] = obs(code, f);
        auto bin = cc({t});
        CHECK_NOTHROW(fixture.err(bin));
      }
      SUBCASE("1"){
        WITH_FENVS(env);
      
        auto n = icst(code, 4, width);
        auto i = itr(code, n);
        auto a = rvar(code, width, {i});

        auto f = con(code, add(code, {a}), {i});
        auto [t,b] = obs(code, f);
        auto bin = cc({t});
        CHECK_NOTHROW(fixture.err(bin));
      }
      SUBCASE("2"){

        WITH_IENVS(env);

        auto n = icst(code, 64, width);
        auto i = itr(code, n);
        auto m = icst(code, 4, width);
        auto j = itr(code, m);
        auto a = ivar(code, width, {i,j});

        auto f = con(code,add(code,{a}),{i});
        auto [t,b] = obs(code, f);
        auto bin = cc({t});
        CHECK_NOTHROW(fixture.err(bin));

      }
      SUBCASE("3"){
        WITH_IENVS(env);
        auto n0 = ncst(code, 4_n, width);
        auto i0 = itr(code, n0), i1 = itr(code, n0);
        auto x0 = nvar(code, width, {i0});
        auto y0 = nvar(code, width, {i1});
        auto g0 = rot(code, x0, {y0});
        auto f0 = con(code, add(code, {g0}), i1);
        auto [t,b] = obs(code, f0);
        auto bin = cc({t});
        CHECK_NOTHROW(fixture.err(bin));
      }

    }

    SUBCASE("rot"){

      SUBCASE("0"){
        WITH_IENVS(env);

        auto n = icst(code,3, width);
        auto i = itr(code,n);
        auto x = ivar(code, width,{itr(code, n)});
        auto f = rot(code,x,{i});

        auto [t,b] = obs(code, f);
        auto bin = cc({t});
        CHECK_NOTHROW(fixture.err(bin));
      }
      SUBCASE("1"){
        WITH_FENVS(env);

        auto x = rvar(code, width,{
            itr(code, code.builtins.lits[codes::literal::two, width_c]),
            itr(code, code.builtins.lits[codes::literal::two, width_c])
          });
        auto j = itr(code,code.builtins.lits[codes::literal::one, width_c]);
        auto k = itr(code,code.builtins.lits[codes::literal::two, width_c]);

        auto h = add(code,{j,code.builtins.lits[codes::literal::one, width_c]});
        auto f = rot(code,x,{h,k});

        auto [t,b] = obs(code, f);
        auto bin = cc({t});
        CHECK_NOTHROW(fixture.err(bin));
      }
      SUBCASE("2"){
        WITH_FENVS(env);

        const auto n = 10_i;
        auto e = icst(code, n, width);
        auto x = rvar(code, width,{ itr(code, e) });
        auto a = itr(code,e);
        auto b = itr(code,e);

        auto x_a = rot(code,x,{a});
        auto x_b = rot(code,x,{b});

        auto f = add(code,{x_a,x_b});

        auto [t,u] = obs(code, f);
        auto bin = cc({t});
        CHECK_NOTHROW(fixture.err(bin));
      }
      SUBCASE("3"){
        WITH_IENVS(env);

        auto n = icst(code,3, width);
        auto i = itr(code,n);
        auto c = icst(code,4, width);
        auto f = rot(code,mul(code,{c,i}),{i});

        auto [t,b] = obs(code, f);
        auto bin = cc({t});
        CHECK_NOTHROW(fixture.err(bin));
      }

    }

    SUBCASE("sel"){
      WITH_IENVS(env);

      auto w = icst(code,0, width);
      auto a = icst(code,1, width);
      auto b = icst(code,2, width);
      auto f = sel(code,w,{a,b});

      auto [t,u] = obs(code, f);
      auto bin = cc({t});
      CHECK_NOTHROW(fixture.err(bin));
    }

    SUBCASE("der"){
      WITH_FENVS(env);

      auto x = rvar(code, width);
      auto t = rvar(code, width);
      auto dxdt = der(code, x, t);

      auto g = put(code, x, dxdt);

      auto bin = cc({g});
      CHECK_NOTHROW(fixture.err(bin));
    }
    SUBCASE("fct"){
      WITH_IENVS(env);
      auto n = icst(code,8, width);
      auto i = itr(code, n);
      auto x = ivar(code, width, {i});
      auto f = fct(code, x);
      auto [t,b] = obs(code, f);
      auto bin = cc({t});
      CHECK_NOTHROW(fixture.err(bin));
    }
    SUBCASE("pow"){
      SUBCASE("0"){
        WITH_FENVS(env);
        auto n = icst(code,8, width);
        auto i = itr(code, n);
        auto r = ivar(code, width,{i});
        auto two = code.builtins.lits[literal::two, width_c];
        auto f = pow(code, r, two);
        auto [t,b] = obs(code, f);
        auto bin = cc({t});
        CHECK_NOTHROW(fixture.err(bin));
      }
      SUBCASE("1"){
        WITH_FENVS(env);
        auto n = icst(code,8, width);
        auto i = itr(code, n);
        auto z = rvar(code, width,{i});
        auto two = code.builtins.lits[literal::two, width_c];
        auto f = pow(code, z, two);
        auto [t,b] = obs(code, f);
        auto bin = cc({t});
        CHECK_NOTHROW(fixture.err(bin));
      }
      SUBCASE("2"){
        WITH_FENVS(env);
        auto n = icst(code,8, width);
        auto i = itr(code, n);
        auto r = ivar(code, width,{i});
        auto z = rvar(code, width,{i});
        auto f = pow(code, r, z);
        auto [t,b] = obs(code, f);
        auto bin = cc({t});
        CHECK_NOTHROW(fixture.err(bin));
      }
    }
    SUBCASE("rfl"){
      WITH_IENVS(env);
      auto o0 = icst(code,4, width);
      auto o1 = itr(code, o0);
      auto o2 = ivar(code, width,{o1});
      auto o3 = rfl(code, o2);
      auto o4 = rfl(code, o3);
      auto o5 = put(code, o4, add(code, {o0,o2}));
      auto bin = cc({o5});
      CHECK_NOTHROW(fixture.err(bin));
    }
    SUBCASE("cur"){
      SUBCASE("0"){
        WITH_IENVS(env);
        auto x0 = nvar(code, width);
        auto [t,b] = obs(code, cur(code, x0));
        auto bin = cc({t});
        CHECK_NOTHROW(fixture.err(bin));
      }
      SUBCASE("1"){
        WITH_IENVS(env);
        auto n0 = ncst(code, 4_n, width);
        auto i0 = itr(code, n0);
        auto t = put(code, cur(code, i0), ncst(code, 1_n, width));
        auto bin = cc({t});
        CHECK_NOTHROW(fixture.err(bin));
      }
      SUBCASE("2"){
        WITH_IENVS(env);
        auto n0 = ncst(code, 4_n, width);
        auto i0 = itr(code, n0);
        auto t = put(code, cur(code, i0), inc(code, cur(code, i0)));
        auto bin = cc({t});
        CHECK_NOTHROW(fixture.err(bin));
      }
    }
    SUBCASE("lor"){

      SUBCASE("0"){

        WITH_IENVS(env);

        integer N = 2;
        auto z = var(code, typ(code, icst(code,N, width), code.builtins.widths.w8));
        std::vector<codes::exprfam> G;
        for(integer i:views::indices(N))
          G.push_back({eq(code, z, icst(code, i, width))});

        auto G_dup = ranges::vec(
          G,
          bind<>(rfl,
                 std::ref(code)));
        auto G_put = ranges::vec(
          std::views::zip(G_dup, G),
          bind<>(
            apply,
            bind<>(codes::put, std::ref(code))));

        auto H = ranges::vec(
          G_dup,
          [&](auto g){
            return codes::exprfam(lor(code,{g}));
          });
        auto H_dup = ranges::vec(
          G,
          bind<>(rfl,
                 std::ref(code)));
        auto H_put = ranges::vec(
          std::views::zip(H_dup, H),
          bind<>(
            apply,
            bind<>(codes::put, std::ref(code))));

        auto f = lor(code,H_dup);
        auto f_var = rfl(code, f);
        auto r = then(code, {when(code, G_put), when(code, H_put), put(code, f_var, f)});

        auto bin = cc({r});
        CHECK_NOTHROW(fixture.err(bin));
      }
      SUBCASE("1"){
        WITH_IENVS(env);
        auto n = icst(code,8, width);
        auto i = itr(code,n);
        auto a = bvar(code, width,{i});
        auto f = con(code,lor(code,{a}), {i});
        auto [t,b] = obs(code, f);
        auto bin = cc({t});
        CHECK_NOTHROW(fixture.err(bin));
      }
      SUBCASE("2"){
        WITH_IENVS(env);
        auto f = con(code,lor(code,{}));
        auto f_var = nvar(code, width);
        auto r = put(code,f_var,f);
        auto bin = cc({r});
        CHECK_NOTHROW(fixture.err(bin));
      }
      SUBCASE("3"){
        WITH_FENVS(env);
        auto n = icst(code, 8, width);
        auto i = itr(code,n);
        auto x = rvar(code, width,{i});
        auto g = lt(code,x,rcst(code,0.0, width));
        auto gl = bvar(code, width,{i});
        auto ga = con(code,lor(code,{eq(code,gl,g)}), {i});
        auto [t,bga] = obs(code, ga);
        auto bin = cc({t});
        CHECK_NOTHROW(fixture.err(bin));
      }
    }
    SUBCASE("put"){

      SUBCASE("0"){
        auto make = [&](auto width){
          auto e0 = icst(code, 64, width), e1 = icst(code, 128, width);
          auto i0 = itr(code, e0), i1 = itr(code, e1);
          auto x0 = ivar(code, width, {i0, i1}),
            x1 = ivar(code, width, {i1}),
            x2 = nvar(code, width, {});

          auto z0 = rfl(code, x1);
          auto y0 = rot(code, x0, {x2, z0});
          auto t = put(code, y0, x1);
          return std::make_tuple(t, x0, x1, x2);
        };

        SUBCASE("0"){
          WITH_IENVS(env);
          auto [t, x0, x1, x2] = make(width);
          auto bin = cc({t});
          CHECK_NOTHROW(fixture.err(bin));
        }
        SUBCASE("1"){
          WITH_IENVS(env);
          auto [t, x0, x1, x2] = make(width);
          auto bin = cc({t});
          CHECK_NOTHROW(fixture.err(bin));
        }
        SUBCASE("2"){
          WITH_IENVS(env);
          auto [t, x0, x1, x2] = make(width);
          auto bin = cc({t});
          CHECK_NOTHROW(fixture.err(bin));
        }
      }
      SUBCASE("1"){
        WITH_IENVS(env);
        auto n = icst(code,8,width);
        auto i = itr(code,n);
        auto a = ivar(code,width,{i});
        auto c = bcst(code,true,width);
        auto f = put(code,a,c);

        auto bin = cc({f});
        CHECK_NOTHROW(fixture.err(bin));
      }
      SUBCASE("2"){
        WITH_IENVS(env);
        auto n = icst(code,8,width);
        auto i = itr(code,n);
        auto a = bvar(code,width,{i});
        auto c = icst(code,2,width);
        auto f = put(code,a,c);

        auto bin = cc({f});
        CHECK_NOTHROW(fixture.err(bin));
      }
      SUBCASE("3"){
        WITH_IENVS(env);
        auto n = icst(code,8, width);
        auto i = itr(code,n);
        auto a = bvar(code, width,{i});
        auto c = icst(code,2, width);
        auto f = put(code,a,c);

        auto bin = cc({f});
        CHECK_NOTHROW(fixture.err(bin));
      }

    }
    SUBCASE("noop"){
      WITH_IENVS(env);
      auto f = noop(code);
      auto bin = cc({f});
      CHECK_NOTHROW(fixture.err(bin));
    }
    SUBCASE("loop"){
      SUBCASE("0"){
        WITH_IENVS(env);
        auto f = loop(code, code.builtins.brk);
        auto bin = cc({f});
        CHECK_NOTHROW(fixture.err(bin));
      }
      SUBCASE("1"){
        WITH_IENVS(env);
        auto f = loop(code, code.builtins.ret);
        auto bin = cc({f});
        CHECK_NOTHROW(fixture.err(bin));
      }
      SUBCASE("2"){
        WITH_IENVS(env);
        auto f = loop(code, loop(code, code.builtins.ret));
        auto bin = cc({f});
        CHECK_NOTHROW(fixture.err(bin));
      }
      SUBCASE("3"){
        WITH_IENVS(env);
        auto f = loop(code, then(code, {loop(code, code.builtins.brk), code.builtins.brk}));
        auto bin = cc({f});
        CHECK_NOTHROW(fixture.err(bin));
      }
      SUBCASE("4"){
        WITH_IENVS(env);
        auto x0 = ivar(code, width);
        auto s0 = fork(code, lt(code, x0, icst(code, 10, width)), {code.builtins.noop, code.builtins.brk});
        auto s1 = put(code, x0, add(code, {x0, code.builtins.lits[codes::literal::one, width_c]}));
        auto s2 = then(code, {s0, s1});
        auto f = loop(code, s2);
        auto bin = cc({f});
        CHECK_NOTHROW(fixture.err(bin));
      }
      SUBCASE("5"){
        WITH_IENVS(env);
        auto f = loop(code, code.builtins.cnt);
        auto bin = cc({f});
        CHECK_NOTHROW(fixture.err(bin));
      }
    }
    SUBCASE("fork"){
      SUBCASE("0"){
        WITH_IENVS(env);
        auto f = fork(code, code.builtins.lits.zero32, code.builtins.noop);
        auto bin = cc({f});
        CHECK_NOTHROW(fixture.err(bin));
      }
      SUBCASE("1"){
        WITH_IENVS(env);
        auto f = loop(code, fork(code, code.builtins.lits.one32, {code.builtins.brk, code.builtins.ret}));
        auto bin = cc({f});
        CHECK_NOTHROW(fixture.err(bin));
      }
      SUBCASE("2"){
        WITH_IENVS(env);
        auto f = loop(code, fork(code, code.builtins.lits.zero32, {code.builtins.brk}));
        auto bin = cc({f});
        CHECK_NOTHROW(fixture.err(bin));
      }
      SUBCASE("3"){
        WITH_IENVS(env);
        auto f = loop(code, fork(code, code.builtins.lits.zero32, {exit(code, icst32(code, 3))}));
        CHECK_THROWS(cc({f}));
      }
    }
    SUBCASE("then"){
      WITH_FENVS(env);
      auto N = icst(code, 8, width);
      auto i0 = itr(code, N), i1 = itr(code, N);

      auto A = rvar(code, width, {i0, i1});
      auto B = rvar(code, width, {i0, i1});
      auto x = rvar(code, width, {i0});
      auto b = rvar(code, width, {i0});
      auto f0 = put(code, x, b);
      auto f1 = put(code, A, B);
      auto m = then(code, {f0, f1});
      auto bin = cc({m});
      CHECK_NOTHROW(fixture.err(bin));
    }

    #undef WITH_IENVS
    #undef WITH_FENVS
    #undef WITH_ENVS

  }
}

}// hysj::binaries
#include<hysj/tools/epilogue.hpp>
