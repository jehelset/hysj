#include<algorithm>
#include<cmath>
#include<initializer_list>
#include<limits>
#include<random>
#include<ranges>
#include<utility>
#include<vector>

#include"framework.hpp"

#include<hysj/binaries/spirv.hpp>
#include<hysj/codes.hpp>
#include<hysj/devices/device.hpp>
#include<hysj/devices/gcc.hpp>
#include<hysj/devices/vulkan.hpp>
#include<hysj/tools/functional/apply.hpp>
#include<hysj/tools/ranges/span.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/types.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::devices{

template<natural N>
struct vulkan_test_fixture{
  HYSJ_TEST_FIXTURE((fmt::format("vk{}", N)));

  static constexpr natural width = N;
  static constexpr bool enable_reals = codes::has_cell<set::reals, width>;

  const codes::code &code;
  codes::devsym dev;

  static vkenv make_env(codes::devsym dev){
    auto e = vkenv::make();
    auto it = std::ranges::find(e.devs, vulkan::devtype::cpu, &vulkan::dev::type);
    HYSJ_ASSERT(it != std::ranges::end(e.devs));
    it->sym = dev;
    return e;
  }

  struct subfixture{
    const codes::code &code;
    vkenv env;

    auto exe(codes::apisym a){
      return devs::exe(env, code, a);
    }
    static std::string_view name(){ return "default"; }
  };

  auto subfixtures(){
    return std::views::single(subfixture{code, make_env(dev)});
  }
};

#define VULKAN_TEST_FIXTURES \
  vulkan_test_fixture<8>,    \
  vulkan_test_fixture<16>,   \
  vulkan_test_fixture<32>,   \
  vulkan_test_fixture<64>    \

template<natural N, natural M = 0>
struct gcc_test_fixture{
  HYSJ_TEST_FIXTURE((fmt::format("gcc{}", N, M == 0 ? std::string{"hw"} : std::to_string(M))));

  static constexpr natural width = N;
  static constexpr bool enable_reals = binaries::gcc::is_supported_type({set::reals, width});

  const codes::code &code;
  codes::devsym dev;


  struct subfixture{
    const codes::code &code;
    gccenv env;

    auto exe(codes::apisym a){
      return devs::exe(env, code, a);
    }
    std::string name()const{
      return fmt::format("thread{}", env.thread_count);
    }
  };

  auto subfixtures(){
    std::vector<natural> thread_counts{1, 2, 7, 10, 16, 32, 64, 256, std::thread::hardware_concurrency()};
    ranges::vecset(thread_counts);
    return std::views::transform(
      std::move(thread_counts),
      [&](auto thread_count){
        return subfixture{
          .code = code,
          .env{
            {
              .optimization_level = 2,
              .thread_count = thread_count
            },
            dev
          }
        };
      });
  }
};

#define GCC_TEST_FIXTURES \
  gcc_test_fixture<8>,    \
  gcc_test_fixture<16>,   \
  gcc_test_fixture<32>,   \
  gcc_test_fixture<64>

TEST_CASE_TEMPLATE("devices.compute", test_fixture, VULKAN_TEST_FIXTURES, GCC_TEST_FIXTURES){

  using namespace codes::factories;
  std::random_device random_device;
  std::mt19937 random_generator{random_device()};

  codes::code code{};
  auto dev = codes::dev(code);

  test_fixture fixture{code, dev};

  static constexpr auto width = constant_c<fixture.width>;
  auto bot = code.builtins.lits[literal::bot, width];
  auto zero = code.builtins.lits[literal::zero, width];
  auto one = code.builtins.lits[literal::one, width];
  auto negone = code.builtins.lits[literal::negone, width];
  auto two = code.builtins.lits[literal::two, width];

  auto w = code.builtins.widths[width];

  for(auto &&subfixture:fixture.subfixtures())
    SUBCASE(subfixture.name().data()){

      auto cc = [&]<typename T = id,
                    typename U = std::initializer_list<id>,
                    typename V = std::initializer_list<id>>(
                      T task,U &&inputs, V &&outputs){

        auto send = codes::when(
          code,
          ranges::vec(inputs,
                      bind<>(lift((codes::send)), std::ref(code), dev)));

        auto main = codes::on(code, dev, task);

        auto recv = codes::when(
          code,
          ranges::vec(outputs,
                      bind<>(lift((codes::recv)), std::ref(code), dev)));

        auto alg = codes::then(code, {send, main, recv});
        auto bin = codes::api(code, {alg});
        auto [mem, exe] = subfixture.exe(bin);
        return std::make_tuple(std::move(mem), std::move(exe), alg);
      };

      auto eval = [&]<codes::set S>(constant_t<S> s,id f){
        auto [t, b] = obs(code, f, w);
        auto [host, exe, alg] = cc(t, {}, {b});
        exe.run({alg});
        auto c = host.get(s, width, b);
        return std::make_tuple(std::move(host), std::move(exe), c);
      };

      auto eq_msg = [&](auto l, auto r){
        return fmt::format("{} ≠ {}\n", static_cast<double>(l),
                           static_cast<double>(r));
      };

      SUBCASE("cst"){

        #define CHECK_CST(c_set,c_in,c_out_expected)                    \
          {                                                             \
            auto [host, exe, c_out_actual] = eval(c_set,cst(code, c_in, w));  \
            CHECK(c_out_actual.size() == 1);                            \
            auto msg = eq_msg(c_out_expected, c_out_actual[0]);         \
            CHECK_MESSAGE(c_out_actual[0] == c_out_expected, msg);      \
          }
        #define CHECK_CST_APPROX(c_set,c_in,c_out_expected)             \
          {                                                             \
            auto [host, exe, c_out_actual] = eval(c_set,cst(code, c_in, w));  \
            CHECK(c_out_actual.size() == 1);                            \
            auto msg = eq_msg(c_out_expected, c_out_actual[0]);         \
            CHECK_MESSAGE(c_out_actual[0] == approx(c_out_expected, width), msg); \
          }

        CHECK_CST(codes::booleans_c,codes::bvalue{true},1);
        CHECK_CST(codes::booleans_c,codes::bvalue{false},0);
        CHECK_CST(codes::integers_c,codes::ivalue{2},2);
        CHECK_CST(codes::integers_c,literal::zero,0);
        CHECK_CST(codes::integers_c,literal::one,1);
        CHECK_CST(codes::integers_c,literal::negone,-1);
        CHECK_CST(codes::integers_c,literal::two,2);

        if constexpr(fixture.enable_reals){
          CHECK_CST_APPROX(codes::reals_c,codes::rvalue{1000.3},1000.3);
          CHECK_CST_APPROX(codes::reals_c,codes::rvalue{4.0},4.0);
          CHECK_CST(codes::reals_c,literal::inf,std::numeric_limits<real>::infinity());
          CHECK_CST_APPROX(codes::reals_c,literal::pi,std::numbers::pi_v<real>);
          CHECK_CST_APPROX(codes::reals_c,literal::e,std::numbers::e_v<real>);
        }

        #undef CHECK_APPROX
        #undef CHECK_CST

      }
      SUBCASE("var"){

        const auto n = 10_i;
        auto e = icst(code, n, w);
        auto i = itr(code, e);

        SUBCASE("booleans"){
          auto x = bvar(code, width,{i}),
            y = bvar(code, width,{i});
          auto f = codes::put(code, y, x);

          auto [host, exe, alg] = cc(f,{x},{y});

          auto x_mem = host.b(width, x);
          auto y_mem = host.b(width, y);
          REQUIRE(x_mem.size() == n);
          REQUIRE(y_mem.size() == n);

          std::ranges::copy(
            views::map(views::indices(0_n,static_cast<natural>(n)),
                       [](auto n){
                         return n % 2;
                       }),
            x_mem.data());


          exe.run({alg});

          CHECK(x_mem == y_mem);
        }
        SUBCASE("integers"){
          auto x = ivar(code, width,{i}),
            y = ivar(code, width,{i});
          auto f = codes::put(code, y, x);

          auto [host, exe, alg] = cc(f,{x},{y});

          auto x_mem = host.i(width, x);
          auto y_mem = host.i(width, y);
          REQUIRE(x_mem.size() == n);
          REQUIRE(y_mem.size() == n);

          std::ranges::iota(x_mem, 0_i);

          exe.run({alg});

          CHECK(x_mem == y_mem);
        }
        if constexpr(fixture.enable_reals){
          SUBCASE("reals"){
            auto x = rvar(code, width, {i}),
              y = rvar(code, width, {i});
            auto f = codes::put(code, y, x);

            auto [host, exe, alg] = cc(f,{x},{y});

            auto x_mem = host.r(width, x);
            auto y_mem = host.r(width, y);
            REQUIRE(x_mem.size() == n);
            REQUIRE(y_mem.size() == n);

            std::ranges::copy(
              views::generate_n(
                [r = rcell<width>{0}]mutable{
                  auto u = r;
                  r += 1;
                  return u;
                },
                n),
              x_mem.data());

            exe.run({alg});

            CHECK(x_mem == y_mem);
          }
        }
      }
      SUBCASE("con"){

        std::vector n{
          1,4,16
        };
        auto d = ranges::vec(n,[&](auto n){ return icst(code,n,w); });
        auto i = ranges::vec(d,[&](auto d){ return itr(code,d); });

        SUBCASE("wrap"){
          auto n = icst(code, 2, width);
          auto i = itr(code, n), i_ = itr(code, n);
          auto x = ivar(code, width, {i});
          auto g = add(code, {i_, one});
          auto h = add(code, rot(code, x, {g}));
          auto f = con(code, h, {i_});
          auto [t, b] = obs(code, f, w);

          auto [host, exe, alg] = cc(t,{x},{b});

          auto x_mem = host.i(width, x);

          x_mem[0] = 2;
          x_mem[1] = 0;

          auto b_mem = host.i(width, b);

          exe.run({alg});

          CHECK(b_mem[0] == 2);
        }

        SUBCASE("add"){
          if constexpr(fixture.enable_reals){

            SUBCASE("reals"){
              SUBCASE("0"){

                auto a = rvar(code, width,{i[0]});
                auto f = con(code,add(code,{a}),{i[0]});
                auto [t, b] = obs(code, f, w);

                auto [host, exe, alg] = cc(t,{a},{b});

                auto a_mem = host.r(width, a);
                rcell<width> a_val{4};
                std::ranges::fill(a_mem,a_val);

                auto b_mem = host.r(width, b);

                exe.run({alg});

                CHECK(b_mem[0] == approx(a_val * n[0]));

              }
              SUBCASE("1"){

                auto a = rvar(code, width,{i[1]});
                auto f = con(code,add(code,{a}), {i[1]});
                auto [t, b] = obs(code, f, w);

                auto [host, exe, alg] = cc(t,{a},{b});

                auto a_mem = host.r(width, a);
                rcell<width> a_val{4};
                std::ranges::fill(a_mem,a_val);

                auto b_mem = host.r(width, b);
                CHECK(b_mem.size() == 1);

                exe.run({alg});

                CHECK(b_mem[0] == approx(a_val * n[1]));
              }

              SUBCASE("2"){

                auto a = rvar(code, width,{i[2]});
                auto f = con(code,add(code,{a}),{i[2]});
                auto [t, b] = obs(code, f, w);

                auto [host, exe, alg] = cc(t,{a},{b});

                auto a_mem = host.r(width, a);
                rcell<width> a_val{4};
                std::ranges::fill(a_mem,a_val);

                auto b_mem = host.r(width, b);

                exe.run({alg});

                CHECK(b_mem.size() == 1);
                CHECK(b_mem[0] == approx(a_val * n[2]));

              }
            }
          }
          SUBCASE("integers"){

            auto n0 = 8_i;
            auto d0 = icst(code, {n0}, w);
            auto i0 = itr(code, d0);
            auto q = ivar(code, width,{i0});

            auto n1 = 3_i;
            auto d1 = icst(code, {n1}, w);
            auto i1 = itr(code, d1);

            auto o = icst(code, -(n1 / 2), w);

            auto f = con(code,add(code,{rot(code, q, {add(code, {i0, i1, o})})}), i1);
            auto [t, b] = obs(code, f, w);
            auto [host, exe, alg] = cc(t,{q},{b});

            auto q_mem = host.i(width, q),
              b_mem = host.i(width, b);

            CHECK(q_mem.size() == n0);
            CHECK(b_mem.size() == n0);
            for(auto m:views::indices(n0)){

              std::ranges::fill(q_mem, 0_i);
              std::ranges::fill(std::span{q_mem}.first(m), 1_i);
              std::ranges::shuffle(q_mem, random_generator);

              exe.run({alg});

              auto b_mem_expected =
                ranges::vec(views::indices(n0),
                            [&](auto i){
                              icell<width> b_expected{0};
                              for(auto j:views::indices(n1)){
                                auto i_con = ((i + (j - n1 / 2) % n0) + n0) % n0;
                                b_expected += q_mem[i_con];
                              }
                              return b_expected;
                            });
              REQUIRE(b_mem == b_mem_expected);
            }
          }
        }
        SUBCASE("lor"){
          SUBCASE("0"){

            integer N = 2;
            auto z = var(code, typ(code, lit(code,codes::ivalue{N}), w));
            std::vector<codes::eqsym> G;
            for(integer i:views::indices(N))
              G.push_back(eq(code,z,icst(code,i, w)));

            auto G_dup = ranges::vec(
              G,
              bind<0, -1>(lift((tmp_dup)), std::ref(code), w));
            auto G_put = ranges::vec(
              std::views::zip(G_dup, G),
              bind<>(
                apply,
                bind<>(codes::put, std::ref(code))));

            auto H = ranges::vec(
              G_dup,
              [&](auto g){
                return codes::exprfam(lor(code,{g}));
              });
            auto H_dup = ranges::vec(
              G,
              bind<0, -1>(lift((tmp_dup)), std::ref(code), w));
            auto H_put = ranges::vec(
              std::views::zip(H_dup, H),
              bind<>(
                apply,
                bind<>(codes::put, std::ref(code))));

            auto f = lor(code,H_dup);
            auto f_var = tmp_dup(code, f, w);

            auto r = then(code, {when(code, G_put), when(code, H_put), codes::put(code, f_var, f)});

            auto [host, exe, alg] = cc(r,
                                 views::one(z),
                                 views::catcast<id>(views::one(f_var),G_dup,H_dup));

            auto z_mem = host.i(width, z);
            auto f_mem = host.b(width, f_var);

            auto G_mem = ranges::vec(G_dup,[&](auto g){ return host.b(width, g); });

            REQUIRE(z_mem.size() == 1);
            REQUIRE(f_mem.size() == 1);
            for(const auto &g_mem:G_mem)
              REQUIRE(g_mem.size() == 1);

            for(icell<width> i:views::indices(G)){

              z_mem[0] = i;

              exe.run({alg});

              CHECK(f_mem[0] == 1_i);
              for(icell<width> j:views::indices(G))
                CHECK_MESSAGE(G_mem[j][0] == (i == j),
                              fmt::format("{},{} = {}",i,j,G_mem[j]));
            }
          }
        }
      }

      SUBCASE("rot"){

        auto n = icst(code,3, w);
        auto i = itr(code,n);

        SUBCASE("0"){
          auto x = ivar(code, width,{itr(code, n)});
          auto f = rot(code,x,{i});

          auto [t, b] = obs(code, f, w);

          auto [host, exe, alg] = cc(t,{x},{b});

          auto x_mem = host.i(width, x);
          CHECK(x_mem.size() == 3);
          std::ranges::copy(
            std::initializer_list<icell<width>>{0,1,2},
            std::ranges::begin(x_mem));

          auto b_mem = host.i(width, b);

          exe.run({alg});

          CHECK(x_mem == b_mem);
        }
        SUBCASE("1"){
          auto x = ivar(code, width,{itr(code, n),itr(code, n)});
          auto f = rot(code,x,{i,i});
          auto [t, b] = obs(code, f, w);

          auto [host, exe, alg] = cc(t,{x},{b});

          auto x_mem = host.i(width, x);
          auto b_mem = host.i(width, b);

          REQUIRE(x_mem.size() == 9);
          std::ranges::copy(
            std::initializer_list<icell<width>>{
              0,1,2,
              3,4,5,
              6,7,8
            },
            std::ranges::begin(x_mem));

          exe.run({alg});

          std::vector<icell<width>> b_mem_expected{0,4,8};

          CHECK(b_mem_expected == b_mem);
        }
        if constexpr(fixture.enable_reals){
          SUBCASE("4"){
            auto x = rvar(code, width,{itr(code, two),itr(code, two)});
            auto j = itr(code,one);
            auto k = itr(code,two);

            auto f = rot(code,x,{add(code,{j,one}),k});
            auto [t, b] = obs(code, f, w);

            auto [host, exe, alg] = cc(t,{x},{b});

            auto x_mem = host.r(width, x);
            REQUIRE(x_mem.size() == 4);
            std::ranges::copy(std::vector<rcell<width>>{1,2,3,4},
                              std::ranges::begin(x_mem));
            auto b_mem = host.r(width, b);

            exe.run({alg});

            REQUIRE(b_mem.size() == 2);
            std::vector<rcell<width>> b_mem_expected{3,4};
            CHECK(b_mem == b_mem_expected);
          }
          SUBCASE("5"){
            auto n = icst(code,2, w);
            auto i = itr(code,n), j = itr(code,n);

            auto x = rvar(code, width,{i,j});

            auto f = rot(code,x,{i,add(code,{j,one})});
            auto [t, b] = obs(code, f, w);

            auto [host, exe, alg] = cc(t,{x},{b});

            auto x_mem = host.r(width, x);
            std::ranges::copy(std::vector<rcell<width>>{1,2,3,4},
                              std::ranges::begin(x_mem));
            auto b_mem = host.r(width, b);

            exe.run({alg});

            std::vector<rcell<width>> b_mem_expected{2,1,4,3};
            CHECK(b_mem == b_mem_expected);
          }
          SUBCASE("6"){

            auto j = itr(code,one);
            auto x = rvar(code, width,{i});

            auto f = rot(code,x,{add(code,{j,one})});
            auto [t, b] = obs(code, f, w);

            auto [host, exe, alg] = cc(t,{x},{b});

            auto x_mem = host.r(width, x);
            std::ranges::copy(std::vector<rcell<width>>{1,2,3},
                              std::ranges::begin(x_mem));
            auto b_mem = host.r(width, b);

            exe.run({alg});

            REQUIRE(b_mem.size());
            std::vector<rcell<width>> b_mem_expected{2};

            CHECK(b_mem == b_mem_expected);
          }
        }
      }

      #define CHECK_UNOP(s,f_id,f_op,c_val)           \
        {                                             \
          auto f_in = cst(code, c_val, w);            \
          auto f = f_id(code,f_in);                   \
          auto [host, exe, f_mem] = eval(s, f);             \
          CHECK(f_mem.size() == 1);                   \
          auto f_actual  = f_mem[0];                  \
          auto f_expected = f_op(c_val);              \
          auto msg = eq_msg(f_expected, f_actual);    \
          CHECK_MESSAGE(f_actual == f_expected, msg); \
        }

      SUBCASE("neg"){
        if constexpr(fixture.enable_reals){
          CHECK_UNOP(codes::reals_c,neg,std::negate{}, codes::rvalue{1.0});
          CHECK_UNOP(codes::reals_c,neg,std::negate{}, codes::rvalue{-1.0});
        }
        CHECK_UNOP(codes::integers_c,neg,std::negate{},codes::ivalue{1});
        CHECK_UNOP(codes::integers_c,neg,std::negate{},codes::ivalue{-1});
      }
      SUBCASE("bnot"){
        CHECK_UNOP(codes::integers_c,bnot,std::bit_not{},codes::ivalue{0});
        CHECK_UNOP(codes::integers_c,bnot,std::bit_not{},codes::ivalue{1});
        CHECK_UNOP(codes::integers_c,bnot,std::bit_not{},codes::ivalue{2});
        CHECK_UNOP(codes::integers_c,bnot,std::bit_not{},codes::ivalue{-2});
      }
      SUBCASE("lnot"){
        auto lnot_fn =
          [](const auto &v){
            return bcell<width>{std::logical_not{}(v)};
          };
        CHECK_UNOP(codes::booleans_c,lnot,lnot_fn,codes::bvalue{0});
        CHECK_UNOP(codes::booleans_c,lnot,lnot_fn,codes::bvalue{1});
      }
      #undef CHECK_UNOP

      SUBCASE("fct"){
        const auto n = 10_i;
        auto e = icst(code, n, w);
        auto i = itr(code, e);
        auto x = ivar(code, width,{i});
        auto f = fct(code,x);
        auto [t, b] = obs(code, f, w);

        auto [host, exe, alg] = cc(t,{x},{b});

        auto x_mem = host.i(width, x);
        std::ranges::copy(views::indices(0_i,n),x_mem.data());
        auto b_mem = host.i(width, b);

        REQUIRE(b_mem.size() == n);
        REQUIRE(x_mem.size() == n);

        exe.run({alg});

        auto b_mem_expected = ranges::vec(
          views::indices(0_i,n),
          [&](integer i) -> icell<width>{
            return std::ranges::fold_left(
              views::indices(1_i, i + 1),
              1_i,
              [](auto l,auto r){
                return l * r;
              });
          });
        CHECK(b_mem == b_mem_expected);
      }
      if constexpr(fixture.enable_reals){
        SUBCASE("sin"){
          const integer n = 64;

          auto d = icst(code, n, w);
          auto i = itr(code, d);
          auto x = rvar(code, width,{i});
          auto [t, b] = obs(code, sin(code,x));

          auto [host, exe, alg] = cc(t,{x},{b});

          auto x_mem = host.r(width, x);
          std::ranges::copy(
            views::generate_n(
              [t = rcell<width>{-4},
               t_delta = rcell<width>{8} / rcell<width>{n - 1}]mutable{
                auto t_now = t;
                t += t_delta;
                return t_now;
              },
              n),
            x_mem.data());
          auto b_mem = host.r(width, b);

          exe.run({alg});

          REQUIRE(x_mem.size() == n);
          REQUIRE(x_mem.size() == b_mem.size());

          for(auto i = 0_i;i != n;++i)
            CHECK(std::sin(x_mem[i]) == approx(b_mem[i]));
        }
      }

      #define CHECK_BINOP(s,i0,i1,f_id,f_op)                          \
        {                                                             \
          auto x0 = cst(code,i0, w);                                  \
          auto x1 = cst(code,i1, w);                                  \
          auto o = f_id(code,std::vector<id>{x0,x1});                 \
          auto [t, b] = obs(code, o, w);                              \
          auto [host, exe, alg] = cc(t,{},{b});                             \
          exe.run({alg});                                             \
          auto b_mem = host.get(s, width, b);                          \
          REQUIRE(b_mem.size() == 1);                                 \
          auto r = f_op(i0,i1);                                       \
          auto msg = fmt::format("{} ≠ {}\n", static_cast<double>(r), \
                                 static_cast<double>(b_mem[0]));      \
          CHECK_MESSAGE(r == b_mem[0], msg);                          \
        }
      #define CHECK_FIXED_BINOP(s,i0,i1,f_id,f_op)                    \
        {                                                             \
          auto x0 = cst(code, i0, w);                                 \
          auto x1 = cst(code, i1, w);                                 \
          auto o = f_id(code,x0,x1);                                  \
          auto [t, b] = obs(code, o, w);                              \
          auto [host, exe, alg] = cc(t,{},{b});                             \
          exe.run({alg});                                             \
          auto b_mem = host.get(s, width, b);                          \
          REQUIRE(b_mem.size() == 1);                                 \
          auto r = f_op(i0,i1);                                       \
          auto msg = fmt::format("{} ≠ {}\n",static_cast<double>(r),  \
                                 static_cast<double>(b_mem[0]));      \
          CHECK_MESSAGE(r == b_mem[0], msg);                          \
        }

      SUBCASE("add"){
        SUBCASE("0"){
          if constexpr(fixture.enable_reals){
            CHECK_BINOP(codes::reals_c,codes::rvalue{1.0},codes::rvalue{1.0},add,std::plus{});
          }
          CHECK_BINOP(codes::integers_c,codes::ivalue{10},codes::ivalue{10},add,std::plus{});
        }
        if constexpr(fixture.enable_reals){
          SUBCASE("1"){
            const auto n = 10_i;
            auto e = icst(code,n, w);
            auto x = rvar(code, width,{itr(code, e)});
            SUBCASE("0"){
              auto a = itr(code, e);
              auto b = itr(code, e);

              auto x_a = rot(code,x,{a});
              auto x_b = rot(code,x,{b});

              auto f = add(code,{x_a,x_b});
              auto [t, c] = obs(code, f, w);

              auto [host, exe, alg] = cc(t,{x},{c});

              auto x_mem = host.r(width, x);
              auto c_mem = host.r(width, c);
              std::ranges::copy(views::indices(0_i,n),std::ranges::begin(x_mem));

              exe.run({alg});

              REQUIRE(x_mem.size() == n);
              REQUIRE(x_mem.size() * x_mem.size() == c_mem.size());
              for(auto [i,j]:std::views::cartesian_product(views::indices(n), views::indices(n)))
                CHECK((x_mem[i] + x_mem[j]) == approx(c_mem[i * n + j]));
            }
            SUBCASE("1"){
              auto i = itr(code,e);
              auto x_i = rot(code,x,{i});
              auto f = add(code,{x_i,x_i});
              auto [t, b] = obs(code, f, w);

              auto [host, exe, alg] = cc(t,{x},{b});

              auto x_mem = host.r(width, x);
              auto b_mem = host.r(width, b);
              std::ranges::copy(views::indices(n,n + n),x_mem.data());

              exe.run({alg});

              CHECK(x_mem.size() == n);
              REQUIRE(x_mem.size() == b_mem.size());
              for(auto i:views::indices(n))
                CHECK(x_mem[i] + x_mem[i] == approx(b_mem[i]));
            }
            SUBCASE("2"){
              const auto n = 10_i;
              auto e = icst(code,n, w);
              auto i = itr(code,e);
              auto x = rvar(code, width,{itr(code, e)});
              auto x_i = rot(code,x,{i});
              auto f = con(code,add(code,{x_i}),{i});
              auto [t, b] = obs(code, f, w);

              auto [host, exe, alg] = cc(t,{x},{b});

              auto x_mem = host.r(width, x);
              auto b_mem = host.r(width, b);
              std::ranges::copy(views::indices(0_i,n),x_mem.data());

              exe.run({alg});

              REQUIRE(x_mem.size() == n);
              REQUIRE(b_mem.size() == 1);
              CHECK(b_mem[0] == approx(std::ranges::fold_left(x_mem,0.0_r,std::plus{})));
            }
          }
        }
      }
      SUBCASE("sub"){
        if constexpr(fixture.enable_reals){
          CHECK_FIXED_BINOP(codes::reals_c,codes::rvalue{1.0},codes::rvalue{1.0},sub,std::minus{});
        }
        CHECK_FIXED_BINOP(codes::integers_c,codes::ivalue{5},codes::ivalue{5},sub,std::minus{});
      }
      SUBCASE("mul"){
        if constexpr(fixture.enable_reals){
          CHECK_BINOP(codes::reals_c,codes::rvalue{1.0},codes::rvalue{1.0},mul,std::multiplies{});
        }
        CHECK_BINOP(codes::integers_c,codes::ivalue{5},codes::ivalue{5},mul,std::multiplies{});
      }
      SUBCASE("div"){
        if constexpr(fixture.enable_reals){
          CHECK_FIXED_BINOP(codes::reals_c,codes::rvalue{0.0},codes::rvalue{1.0},div,std::divides{});
          CHECK_FIXED_BINOP(codes::reals_c,codes::rvalue{4.0},codes::rvalue{8.0},div,std::divides{});
        }
      }
      SUBCASE("pow"){
        if constexpr(fixture.enable_reals){
          CHECK_FIXED_BINOP(codes::reals_c,codes::rvalue{0.0},codes::rvalue{1.0},pow,lift((std::pow)));
          CHECK_FIXED_BINOP(codes::reals_c,codes::rvalue{1.0},codes::rvalue{0.0},pow,lift((std::pow)));
          CHECK_FIXED_BINOP(codes::reals_c,codes::rvalue{4.0},codes::rvalue{2.0},pow,lift((std::pow)));
        }

        SUBCASE("integer_exponents"){

          const auto n = icell<width>{10};
          auto e = icst(code,n, w);
          auto i = itr(code, e);
          auto y = ivar(code, width,{i});
          SUBCASE("ii"){
            auto x = ivar(code, width,{i});

            auto f = pow(code,x,y);
            auto [t, b] = obs(code, f, w);

            auto [host, exe, alg] = cc(t,{x,y},{b});

            auto x_mem = host.i(width, x);
            auto y_mem = host.i(width, y);
            std::ranges::copy(views::repeat_n(icell<width>{2},n),x_mem.data());
            std::ranges::copy(views::indices(icell<width>{0},n),y_mem.data());
            auto b_mem = host.i(width, b);

            REQUIRE(b_mem.size() == n);
            REQUIRE(x_mem.size() == n);
            REQUIRE(y_mem.size() == n);

            exe.run({alg});

            auto b_mem_expected = ranges::vec(
              views::indices(icell<width>{0},n),
              [&](icell<width> i) -> icell<width> {
                return ranges::prod(views::repeat_n(icell<width>{2}, i));
              });

          }
          if constexpr(fixture.enable_reals){
            SUBCASE("ri"){
              auto x = rvar(code, width, {i});

              auto f = pow(code,x,y);
              auto [t, b] = obs(code, f, w);

              auto [host, exe, alg] = cc(t,{x,y},{b});

              auto x_mem = host.r(width, x);
              auto y_mem = host.i(width, y);
              std::ranges::copy(views::repeat_n(rcell<width>{2},n),x_mem.data());
              std::ranges::copy(views::indices(icell<width>{0},n),y_mem.data());
              auto b_mem = host.r(width, b);

              REQUIRE(b_mem.size() == n);
              REQUIRE(x_mem.size() == n);
              REQUIRE(y_mem.size() == n);

              exe.run({alg});

              auto b_mem_expected = ranges::vec(
                views::indices(icell<width>{0},n),
                [&](icell<width> i) -> rcell<width> {
                  return ranges::prod(views::repeat_n(rcell<width>{2}, i));
                });

              CHECK(b_mem == b_mem_expected);
            }
          }
        }
      }
      if constexpr(fixture.enable_reals){
        SUBCASE("log"){
          auto f_log = [](const auto &v0,const auto &v1){
            return std::log(v1) / std::log(v0);
          };
          CHECK_FIXED_BINOP(codes::reals_c,codes::rvalue{1.0},codes::rvalue{2.0},log,f_log);
          CHECK_FIXED_BINOP(codes::reals_c,codes::rvalue{2.0},codes::rvalue{1.0},log,f_log);
          CHECK_FIXED_BINOP(codes::reals_c,codes::rvalue{2.0},codes::rvalue{4.0},log,f_log);
          CHECK_FIXED_BINOP(codes::reals_c,codes::rvalue{4.0},codes::rvalue{2.0},log,f_log);
        }
      }
      SUBCASE("mod"){

        auto f_mod = overload(
          [](integer v0,integer v1){
            return (v0 % v1 + v1) % v1;
          },
          [](real v0,real v1){
            return std::fmod((std::fmod(v0,v1) + v1),v1);
          });

        CHECK_FIXED_BINOP(codes::integers_c,codes::ivalue{5},codes::ivalue{8}, mod,f_mod);
        CHECK_FIXED_BINOP(codes::integers_c,codes::ivalue{-1},codes::ivalue{4},mod,f_mod);
        if constexpr(fixture.enable_reals){
          CHECK_FIXED_BINOP(codes::reals_c,codes::rvalue{ 5},codes::rvalue{8},mod,f_mod);
          CHECK_FIXED_BINOP(codes::reals_c,codes::rvalue{-1},codes::rvalue{4},mod,f_mod);
        }
      }
      SUBCASE("brsh"){
        auto f_brsh = [](const auto &x0,const auto &x1){
          return x0 >> x1;
        };
        CHECK_FIXED_BINOP(codes::integers_c,codes::ivalue{0},codes::ivalue{0},brsh,f_brsh);
        CHECK_FIXED_BINOP(codes::integers_c,codes::ivalue{0},codes::ivalue{1},brsh,f_brsh);
        CHECK_FIXED_BINOP(codes::integers_c,codes::ivalue{1},codes::ivalue{0},brsh,f_brsh);
        CHECK_FIXED_BINOP(codes::integers_c,codes::ivalue{1},codes::ivalue{1},brsh,f_brsh);
        CHECK_FIXED_BINOP(codes::integers_c,codes::ivalue{2},codes::ivalue{2},brsh,f_brsh);
      }
      SUBCASE("blsh"){
        auto f_blsh = [](const auto &x0,const auto &x1){
          return x0 << x1;
        };
        CHECK_FIXED_BINOP(codes::integers_c,codes::ivalue{0},codes::ivalue{0},blsh,f_blsh);
        CHECK_FIXED_BINOP(codes::integers_c,codes::ivalue{0},codes::ivalue{1},blsh,f_blsh);
        CHECK_FIXED_BINOP(codes::integers_c,codes::ivalue{1},codes::ivalue{0},blsh,f_blsh);
        CHECK_FIXED_BINOP(codes::integers_c,codes::ivalue{1},codes::ivalue{1},blsh,f_blsh);
        CHECK_FIXED_BINOP(codes::integers_c,codes::ivalue{2},codes::ivalue{2},blsh,f_blsh);
      }
      SUBCASE("band"){
        CHECK_BINOP(codes::integers_c,codes::ivalue{0},codes::ivalue{0},band,std::bit_and<>{});
        CHECK_BINOP(codes::integers_c,codes::ivalue{0},codes::ivalue{1},band,std::bit_and<>{});
        CHECK_BINOP(codes::integers_c,codes::ivalue{1},codes::ivalue{0},band,std::bit_and<>{});
        CHECK_BINOP(codes::integers_c,codes::ivalue{1},codes::ivalue{1},band,std::bit_and<>{});
      }
      SUBCASE("bor"){
        CHECK_BINOP(codes::integers_c,codes::ivalue{0},codes::ivalue{0},bor,std::bit_or<>{});
        CHECK_BINOP(codes::integers_c,codes::ivalue{0},codes::ivalue{1},bor,std::bit_or<>{});
        CHECK_BINOP(codes::integers_c,codes::ivalue{1},codes::ivalue{0},bor,std::bit_or<>{});
        CHECK_BINOP(codes::integers_c,codes::ivalue{1},codes::ivalue{1},bor,std::bit_or<>{});
      }
      SUBCASE("bxor"){
        CHECK_BINOP(codes::integers_c,codes::ivalue{0},codes::ivalue{0},bxor,std::bit_xor<>{});
        CHECK_BINOP(codes::integers_c,codes::ivalue{0},codes::ivalue{1},bxor,std::bit_xor<>{});
        CHECK_BINOP(codes::integers_c,codes::ivalue{1},codes::ivalue{0},bxor,std::bit_xor<>{});
        CHECK_BINOP(codes::integers_c,codes::ivalue{1},codes::ivalue{1},bxor,std::bit_xor<>{});
      }
      SUBCASE("land"){
        auto f_land =
          [](const auto &v0,const auto &v1)->bcell<width>{
          return bcell<width>{std::logical_and{}(v0,v1)};
        };

        CHECK_BINOP(codes::booleans_c,codes::bvalue{0},codes::bvalue{0},land,f_land);
        CHECK_BINOP(codes::booleans_c,codes::bvalue{0},codes::bvalue{1},land,f_land);
        CHECK_BINOP(codes::booleans_c,codes::bvalue{1},codes::bvalue{0},land,f_land);
        CHECK_BINOP(codes::booleans_c,codes::bvalue{1},codes::bvalue{1},land,f_land);
      }
      SUBCASE("lor"){
        auto f_lor =
          [](const auto &v0,const auto &v1)->bcell<width>{
          return bcell<width>{std::logical_or{}(v0,v1)};
        };

        CHECK_BINOP(codes::booleans_c,codes::bvalue{0},codes::bvalue{0},lor,f_lor);
        CHECK_BINOP(codes::booleans_c,codes::bvalue{0},codes::bvalue{0},lor,f_lor);
        CHECK_BINOP(codes::booleans_c,codes::bvalue{0},codes::bvalue{0},lor,f_lor);
        CHECK_BINOP(codes::booleans_c,codes::bvalue{0},codes::bvalue{0},lor,f_lor);

        SUBCASE("0"){
          auto f = lnot(code, con(code, lor(code,{bot})));
          auto [t, b] = obs(code, f, w);
          auto [host, exe, alg] = cc(t,{},{b});
          auto b_mem = host.b(width, b);

          exe.run({alg});
          REQUIRE(b_mem.size() == 1);
          CHECK(b_mem[0] == 1);
        }
      }
      SUBCASE("eq"){
        if constexpr(fixture.enable_reals){
          CHECK_FIXED_BINOP(codes::booleans_c,codes::rvalue{0.0},codes::rvalue{1.0},eq,std::equal_to{});
          CHECK_FIXED_BINOP(codes::booleans_c,codes::rvalue{1.0},codes::rvalue{0.0},eq,std::equal_to{});
        }
        CHECK_FIXED_BINOP(codes::booleans_c,codes::ivalue{4},codes::ivalue{2},eq,std::equal_to{});
        auto f_eq = [](const auto &v0,const auto &v1){
          return bcell<width>{v0 == v1};
        };
        CHECK_FIXED_BINOP(codes::booleans_c,codes::ivalue{1},codes::ivalue{1},eq,f_eq);
        CHECK_FIXED_BINOP(codes::booleans_c,codes::ivalue{0},codes::ivalue{0},eq,std::equal_to{});
        if constexpr(fixture.enable_reals){
          CHECK_FIXED_BINOP(codes::booleans_c,codes::rvalue{1.0},codes::ivalue{0},eq,std::equal_to{});
        }
      }
      SUBCASE("lt"){
        CHECK_FIXED_BINOP(codes::booleans_c,codes::ivalue{4},codes::ivalue{2},lt,std::less{});
        CHECK_FIXED_BINOP(codes::booleans_c,codes::ivalue{-1},codes::ivalue{0},lt,std::less{});
        CHECK_FIXED_BINOP(codes::booleans_c,codes::ivalue{0},codes::ivalue{0},lt,std::less{});
        if constexpr(fixture.enable_reals){
          CHECK_FIXED_BINOP(codes::booleans_c,codes::rvalue{0.0},codes::rvalue{1.0},lt,std::less{});
          CHECK_FIXED_BINOP(codes::booleans_c,codes::rvalue{1.0},codes::ivalue{0},lt,std::less{});
          CHECK_FIXED_BINOP(codes::booleans_c,codes::rvalue{4.0},codes::rvalue{5.0},lt,std::less{});
        }
      }

      #undef CHECK_FIXED_BINOP
      #undef CHECK_BINOP

      SUBCASE("sel"){

        const auto N_val = 5_i;
        auto N = icst(code,N_val, w);
        auto i = itr(code,N);

        if constexpr(fixture.enable_reals){

          SUBCASE("0"){
            auto c = icst(code,1_i, w);
            auto a = rcst(code, 2.0_r, w);
            auto b_val = 3.0_r;
            auto b = rcst(code,b_val, w);

            auto f = sel(code,c,{a,b});
            auto [t, u] = obs(code, f, w);
            auto [host, exe, alg] = cc(t,{},{u});

            auto u_mem = host.r(width, u);
            exe.run({alg});

            REQUIRE(u_mem.size() == 1);
            CHECK(u_mem[0] == approx(b_val));
          }
          SUBCASE("1"){

            auto c = ivar(code, width,{i});

            auto a_val = 2.0_r,
              b_val = 3.0_r;
            auto a = rcst(code,a_val, w);
            auto b = rcst(code,b_val, w);

            auto f = sel(code,c,{a,b});
            auto [t, u] = obs(code, f, w);

            auto [host, exe, alg] = cc(t,{c},{u});

            auto c_mem = host.i(width, c);
            REQUIRE(c_mem.size() == 5);
            std::ranges::copy(
              std::initializer_list{0_i,1_i,0_i,1_i,0_i},
              std::ranges::begin(c_mem));

            auto u_mem = host.r(width, u);
            REQUIRE(u_mem.size() == N_val);

            exe.run({alg});

            for(auto i:views::indices(N_val))
              if(c_mem[i] == 0_i)
                CHECK(u_mem[i] == approx(a_val));
              else
                CHECK(u_mem[i] == approx(b_val));
          }
        }
      }
      SUBCASE("put"){

        if constexpr(fixture.enable_reals){
          SUBCASE("0"){
            const auto n = 5_i;
            auto d = icst(code,n, w);
            auto i = itr(code,d);

            auto x = rvar(code, width,{i});
            auto c_val = rcell<width>{1};
            auto c = rcst(code,c_val, w);

            auto f = codes::put(code,x,c);
            auto [host, exe, alg] = cc(f,{},{x});

            exe.run({alg});

            std::vector<rcell<width>> x_mem_expected(5_n,c_val);
            auto x_mem_actual = host.r(width, x);

            CHECK(x_mem_actual == x_mem_expected);
          }
        }
        SUBCASE("1"){
          const auto n0 = 5_n, n1 = 10_n;
          auto d0 = ncst(code,n0, w), d1 = ncst(code, n1, w);
          auto i0 = itr(code,d0), i1 = itr(code, d1);

          auto x0 = nvar(code, width,{i0, i1}),
            x1 = nvar(code, width,{i1}),
            x2 = nvar(code, width);

          auto l0 = rot(code, x0, {x2, i1});//BUG: rfl(code, x1)});
          auto r0 = x1;
          auto t0 = codes::put(code, l0, r0);

          auto [host, exe, alg] = cc(t0,{x1, x2},{x0});

          auto x0_mem = host.n(width, x0);
          auto x1_mem = host.n(width, x1);
          auto x2_mem = host.n(width, x2);

          std::ranges::iota(x1_mem, 5_n);
          x2_mem[0] = 4_n;
          exe.run({alg});
          CHECK(std::span{x0_mem}.subspan(n1 * x2_mem[0], n1) == x1_mem);

          std::ranges::iota(x1_mem, 20_n);
          x2_mem[0] = 2_n;
          exe.run({alg});
          CHECK(std::span{x0_mem}.subspan(n1 * x2_mem[0], n1) == x1_mem);

        }
        if constexpr(fixture.enable_reals){
          SUBCASE("2"){
            auto x = rvar(code, w);
            auto f = sub(code, rcst(code, 1.0_r, w), x);
            auto a = bvar(code, width, {itr(code, one)});
            auto b = ivar(code, width, {rfl(code, f)});
            auto c = rfl(code, b);
            auto p = codes::put(code,
                                a,
                                con(code,
                                    lor(code,{
                                        neq(code,b,c)
                                      })));
            auto [host, exe, alg] = cc(p,{b,c},{a,b,c});

            auto a_mem = host.b(width, a);
            auto b_mem = host.i(width, b);
            auto c_mem = host.i(width, c);

            REQUIRE(a_mem.size() == 1);
            REQUIRE(b_mem.size() == 1);
            REQUIRE(c_mem.size() == 1);

            a_mem[0] = 0;
            b_mem[0] = 1;
            c_mem[0] = 0;

            exe.run({alg});

            CHECK(a_mem[0] == 1);
            CHECK(b_mem[0] == 1);
            CHECK(c_mem[0] == 0);

          }
        }
        if constexpr(width >= 32){
          SUBCASE("3"){
            for(auto N:std::array{1, 3, 64, 65, 64*32, 65*35})
              SUBCASE(fmt::format("N={}", N).c_str()){
                auto M = N / 2;
                auto n = ncst(code, N, w);
                auto i = itr(code, n);
                auto q = ivar(code, width, {i}), p = ivar(code, width, {i});
                auto t = put(code, p, q);
                auto [host, exe, alg] = cc(t, {q}, {p});

                auto q_mem = host.i(width, q),
                  p_mem = host.i(width, p);

                std::ranges::fill(q_mem.first(M), 0);
                std::ranges::fill(q_mem.last(N - M), 1);

                exe.run({alg});

                CHECK(q_mem == p_mem);
              }
          }
        }
      }
      SUBCASE("when"){
        if constexpr(fixture.enable_reals){
          SUBCASE("0"){
            auto x = ivar(code, width);
            auto y = rvar(code, width);

            auto h = codes::put(code,x,one);
            auto g = codes::put(code,y,two);

            auto f = when(code,{h,g});

            auto [host, exe, alg] = cc(f,{},{x,y});

            exe.run({alg});

            auto x_mem = host.i(width, x);
            auto y_mem = host.r(width, y);

            REQUIRE(x_mem.size() == 1);
            CHECK(x_mem[0] == 1_i);
            REQUIRE(y_mem.size() == 1);
            CHECK(y_mem[0] == 2_r);
          }
        }
      }

      SUBCASE("then"){

        SUBCASE("0"){

          auto x = ivar(code, width);
          auto y = ivar(code, width);
          auto z = ivar(code, width);
          auto f = then(code,{codes::put(code,y,x), codes::put(code,z,y)});

          auto [host, exe, alg] = cc(f,{x},{y,z});

          auto x_mem = host.i(width, x);
          auto y_mem = host.i(width, y);
          auto z_mem = host.i(width, z);

          REQUIRE(x_mem.size() == 1);
          REQUIRE(y_mem.size() == 1);
          REQUIRE(z_mem.size() == 1);

          x_mem[0] = 2_i;

          exe.run({alg});

          CHECK(y_mem[0] == 2_i);
          CHECK(z_mem[0] == 2_i);
        }

        SUBCASE("1"){

          auto X0 = ivar(code, width), X1 = ivar(code, width), X2 = ivar(code, width);

          auto A0 = when(code, {codes::put(code, X0, one)});
          auto A1 = when(code, {codes::put(code, X1, add(code, {X0, X0}))});
          auto A2 = when(code, {codes::put(code, X1, add(code, {X1, X0}))});
          auto A3 = when(code, {codes::put(code, X2, add(code, {X0, X0}))});
          auto A4 = when(code, {codes::put(code, X2, add(code, {X2, X2, X0}))});

          auto B = then(code,
                        {
                          A0,
                          when(code, {
                              then(code, {A1, A2}),
                              then(code, {A3, A4})
                            }),
                        });

          auto [host, exe, alg] = cc(B, {}, {X0, X1, X2});

          auto X0_mem = host.i(width, X0);
          auto X1_mem = host.i(width, X1);
          auto X2_mem = host.i(width, X2);

          REQUIRE(X0_mem.size() == 1);
          REQUIRE(X1_mem.size() == 1);
          REQUIRE(X2_mem.size() == 1);

          for([[maybe_unused]] auto i:views::indices(10)){
            exe.run({alg});
            CHECK(X0_mem[0] == 1_i);
            CHECK(X1_mem[0] == 3_i);
            CHECK(X2_mem[0] == 5_i);
          }

        }
      }

      if constexpr(fixture.enable_reals){
        SUBCASE("0"){

          auto n = icst(code,2, w);
          auto i0 = itr(code, n), i1 = itr(code, n);
          auto a = rvar(code, width,{i0,i1});
          auto b = rvar(code, width,{i0,i1});
          auto f = add(code,{a,a});
          auto e = codes::put(code,b,f);

          auto [host, exe, alg] = cc(e,{a},{b});

          auto a_mem = host.r(width, a);
          auto b_mem = host.r(width, b);
          std::ranges::fill(a_mem,rcell<width>{2});

          exe.run({alg});

          std::vector<rcell<width>> b_mem_expected(b_mem.size(),rcell<width>{4});
          CHECK(b_mem == b_mem_expected);

        }
        SUBCASE("1"){

          auto n = icst(code,8, w);
          auto i = itr(code, n);

          auto a = rvar(code, width, {i});
          auto b = rvar(code, width, {i});
          auto c = rvar(code, width, {i});
          auto f = add(code,{a,b});
          auto e = codes::put(code,c,f);

          auto [host, exe, alg] = cc(e,{a,b},{c});

          auto a_mem = host.r(width, a);
          auto b_mem = host.r(width, b);
          auto c_mem = host.r(width, c);

          std::ranges::fill(a_mem,rcell<width>{2});
          std::ranges::fill(b_mem,rcell<width>{5});

          exe.run({alg});

          std::vector<rcell<width>> c_mem_expected(c_mem.size(),rcell<width>{7});
          CHECK(c_mem == c_mem_expected);
        }
        SUBCASE("2"){

          auto n = icst(code,16, w);
          auto i = itr(code, n);

          auto a = rvar(code, width,{i});
          auto b = rvar(code, width,{i});
          auto c = rvar(code, width,{i});
          auto f = mul(code,{a,b});
          auto e = codes::put(code,c,f);

          auto [host, exe, alg] = cc(e,{a,b},{c});

          auto a_mem = host.r(width, a);
          auto b_mem = host.r(width, b);
          auto c_mem = host.r(width, c);

          std::ranges::fill(a_mem,rcell<width>{2});
          std::ranges::fill(b_mem,rcell<width>{8});

          exe.run({alg});

          std::vector<rcell<width>> c_mem_expected(c_mem.size(),rcell<width>{16});
          CHECK(c_mem == c_mem_expected);

        }
      }
      SUBCASE("3"){
        auto n = icst(code,16, w);
        auto i = itr(code, n);

        auto a = ivar(code, width,{i});
        auto b = icst(code,4, w);
        auto c = ivar(code, width,{i});
        auto f = mod(code,a,b);
        auto e = codes::put(code,c,f);

        auto [host, exe, alg] = cc(e,{a},{c});

        auto a_mem = host.i(width, a);
        auto c_mem = host.i(width, c);

        std::ranges::fill(a_mem,5);

        exe.run({alg});

        std::vector<icell<width>> c_mem_expected(c_mem.size(),1);
        CHECK(c_mem == c_mem_expected);

      }

      SUBCASE("4"){

        auto n = icst(code,5, w);
        auto i = itr(code,n);
        auto c = ivar(code, width,{i});
        auto e = codes::put(code,c,i);

        auto [host, exe, alg] = cc(e,{},{c});

        auto c_mem = host.i(width, c);

        exe.run({alg});

        std::vector<icell<width>> c_mem_expected{0,1,2,3,4};
        CHECK(c_mem == c_mem_expected);

      }

      SUBCASE("5"){

        SUBCASE("0"){
          auto n = icst(code,3, w);
          auto i = itr(code,n);
          auto b = bvar(code, width,{i});
          auto e = codes::put(code,b,eq(code,i,i));
          auto [host, exe, alg] = cc(e,{},{b});
          auto b_mem = host.b(width, b);
          exe.run({alg});
          std::vector<bcell<width>> b_mem_expected{
            1,1,1,
          };
          CHECK(b_mem == b_mem_expected);
        }
        SUBCASE("1"){
          auto n = icst(code,3, w);
          auto i = itr(code,n),
            j = itr(code,n);
          auto b = ivar(code, width,{i,j});

          auto e = codes::put(code,b,n);

          auto [host, exe, alg] = cc(e,{},{b});

          auto i_mem = host.i(width, b);

          exe.run({alg});

          std::vector<icell<width>> i_mem_expected{
            3,3,3,
            3,3,3,
            3,3,3
          };
          CHECK(i_mem == i_mem_expected);
        }
        SUBCASE("2"){
          auto n = icst(code,3, w);
          auto i = itr(code,n),
            j = itr(code,n);
          auto b = bvar(code, width,{i,j});

          auto e = codes::put(code,b,eq(code,i,j));

          auto [host, exe, alg] = cc(e,{},{b});

          auto b_mem = host.b(width, b);

          exe.run({alg});

          std::vector<bcell<width>> b_mem_expected{
            1,0,0,
            0,1,0,
            0,0,1
          };
          CHECK(b_mem == b_mem_expected);
        }

      }

      SUBCASE("6"){

        auto n = icst(code,2, w);
        auto [t, b] = obs(code, n, w);

        auto [host, exe, alg] = cc(t,{},{b});

        auto b_mem = host.i(width, b);

        exe.run({alg});

        std::vector<icell<width>> b_mem_expected{2};
        CHECK(b_mem == b_mem_expected);

      }

      SUBCASE("7"){

        auto n = icst(code,0, w),
          m = icst(code,1, w);
        auto f = lor(code,{n,m});

        auto [t, b] = obs(code, f, w);
        auto [host, exe, alg] = cc(t,{},{b});

        auto b_mem = host.b(width, b);

        exe.run({alg});

        std::vector<bcell<width>> b_mem_expected{1};
        CHECK(b_mem == b_mem_expected);

      }

      if constexpr(fixture.enable_reals){
        SUBCASE("8"){

          auto n = icst(code,64, w);
          auto m = icst(code,2, w);
          auto i = itr(code, n);
          auto j = itr(code, m);
          auto a = rvar(code, width,{i,j});
          auto f = add(code,{a,a});
          auto [t, b] = obs(code, f, w);
          auto [host, exe, alg] = cc(t,{a},{b});

          auto a_mem = host.r(width, a);
          std::ranges::fill(a_mem,rcell<width>{4});
          auto b_mem = host.r(width, b);

          exe.run({alg});

          std::vector<rcell<width>> b_mem_expected(b_mem.size(),rcell<width>{8});
          CHECK(b_mem == b_mem_expected);

        }
      }

      SUBCASE("9"){

        SUBCASE("0"){
          auto n = 16, m = n / 2;
          auto e = icst(code,n, w);
          auto i = itr(code,e);

          auto c = icst(code,m, w);

          auto f = lt(code,i,c);
          auto [t, b] = obs(code, f, w);

          auto [host, exe, alg] = cc(t,{b},{b});

          auto b_mem = host.b(width, b);

          std::ranges::copy(views::indices(b_mem),b_mem.data());
          CHECK(std::ranges::equal(b_mem, views::indices(b_mem)));

          exe.run({alg});

          auto b_mem_expected =
            ranges::vec(
              views::indices(n),
              [&](auto i) -> bcell<width>{
                return i < m;
              });

          CHECK(b_mem == b_mem_expected);
        }

        SUBCASE("1"){
          auto n0 = 16, n1 = 2;
          auto e0 = icst(code,n0, w), e1 = icst(code,n1, w);
          auto i0 = itr(code,e0), i1 = itr(code,e1);

          auto f = lt(code,i0,i1);
          auto [t, b] = obs(code, f, w);

          auto [host, exe, alg] = cc(t,{},{b});

          auto b_mem = host.b(width, b);
          REQUIRE(b_mem.size() == n0 * n1);

          exe.run({alg});

          auto b_mem_expected =
            ranges::vec(
              std::views::cartesian_product(views::indices(n0),views::indices(n1)),
              [&](auto i) -> bcell<width>{
                auto [i0,i1] = i;
                return i0 < i1;
              });

          CHECK(b_mem == b_mem_expected);
        }

        SUBCASE("2"){
          auto n0 = 2, n1 = 8, n2 = 2;
          auto e0 = icst(code,n0, w), e1 = icst(code,n1, w), e2 = icst(code,n2, w);
          auto i0 = itr(code,e0), i1 = itr(code,e1), i2 = itr(code,e2);

          auto f = lt(code,i0,add(code,{i1,i2}));
          auto [t, b] = obs(code, f, w);

          auto [host, exe, alg] = cc(t,{},{b});

          auto b_mem = host.b(width, b);
          REQUIRE(b_mem.size() == n0 * n1 * n2);

          exe.run({alg});


          auto b_mem_expected =
            ranges::vec(
              std::views::cartesian_product(views::indices(n0),
                                            views::indices(n1),
                          views::indices(n2)),
              [&](auto i) -> bcell<width>{
                auto [i0,i1,i2] = i;
                return i0 < i1 + i2;
              });

          CHECK(b_mem == b_mem_expected);
        }

      }

      SUBCASE("11"){

        auto e0 = icst(code,1, w);
        auto i0 = itr(code, e0),
          i0_ = itr(code, e0);
        auto x0 = ivar(code, width,{i0});
        auto x1 = ivar(code, width,{i0});
        auto y0 = ivar(code, width,{i0});
        auto y1 = ivar(code, width,{i0});
        auto z0 = bvar(code, width,{i0});
        auto z1 = bvar(code, width,{i0});
        auto w0 = bvar(code, width,{i0});

        auto f = [&](auto x,auto y,auto z){
          return then(
            code,{
              codes::put(code,
                         z,
                         con(code,
                             lor(code,{
                                 lnot(code,eq(code,rot(code, x, {i0_}),rot(code, y, {i0_})))
                               }),
                             {i0_})),
              codes::put(code,x,y)
            });
        };

        auto f0 = f(x0,y0,z0);
        auto f1 = f(x1,y1,z1);
        auto g0 =
          codes::put(code,
                     w0,
                     con(code,lor(code,{rot(code, z0, {i0_}),rot(code, z1, {i0_})}),{i0_}));

        auto h0 = then(
          code,
          {
            when(code, {f0,f1}),
            g0,
          });

        auto [host, exe, alg] = cc(h0,{x0,y0,x1,y1},{x0,z0,x1,z1,w0});

        auto x0_mem = host.i(width, x0);
        auto y0_mem = host.i(width, y0);
        auto x1_mem = host.i(width, x1);
        auto y1_mem = host.i(width, y1);

        auto z0_mem = host.b(width, z0);
        auto z1_mem = host.b(width, z1);
        auto w0_mem = host.b(width, w0);

        REQUIRE(y0_mem.size() == 1);
        REQUIRE(y1_mem.size() == 1);
        REQUIRE(z0_mem.size() == 1);
        REQUIRE(z1_mem.size() == 1);
        REQUIRE(w0_mem.size() == 1);

        auto write_x = [&](auto b0,auto b1){
          x0_mem[0] = b0;
          x1_mem[0] = b1;
        };

        auto write_y = [&](auto b0,auto b1){
          y0_mem[0] = b0;
          y1_mem[0] = b1;
        };

        write_x(false,false);
        write_y(false,false);

        exe.run({alg});

        CHECK(z0_mem[0] == false);
        CHECK(z1_mem[0] == false);
        CHECK(w0_mem[0] == false);

        write_y(true,false);
        exe.run({alg});

        CHECK(z0_mem[0] == true);
        CHECK(z1_mem[0] == false);
        CHECK(w0_mem[0] == true);

        exe.run({alg});

        CHECK(z0_mem[0] == false);
        CHECK(z1_mem[0] == false);
        CHECK(w0_mem[0] == false);

      }

      if constexpr(fixture.enable_reals){
        SUBCASE("12"){
          auto n = 3;
          auto e = icst(code, n, w);
          auto i = itr(code, e);

          auto x = rvar(code, width, {i});
          auto y = ivar(code, width, {i});
          auto z = rvar(code, width);

          auto g = eq(code,y,negone);

          auto f = codes::put(code, x, sel(code, g, {x, z}));

          auto [host, exe, alg] = cc(f,{x,y,z},{x,y,z});

          auto x_mem = host.r(width, x);
          auto y_mem = host.i(width, y);
          auto z_mem = host.r(width, z);

          REQUIRE(x_mem.size() == n);
          REQUIRE(y_mem.size() == n);
          REQUIRE(z_mem.size() == 1);

          x_mem[0] = 0;
          x_mem[1] = 1;
          x_mem[2] = 2;

          y_mem[0] =  0;
          y_mem[1] =  1;
          y_mem[2] = -1;

          z_mem[0] = 4;

          exe.run({alg});

          CHECK(x_mem[0] == 0);
          CHECK(x_mem[1] == 1);
          CHECK(x_mem[2] == 4);

        }
      }
      SUBCASE("13"){

        auto n = 8;
        auto e = icst(code, n, w);
        auto i = itr(code, e);
        auto D = typ(code, two, w);
        auto t = ivar(code, width);
        auto Q = var(code, D, {i});
        auto dQdt = der(code, Q, t);
        auto a = bvar(code, width);

        auto P = std::vector<id>{
          rot(code,  Q, {sub(code, i, one)}),
          Q,
          rot(code,  Q, {add(code, {i, one})}),
        };

        auto g = ranges::vec(
          P,
          [&](auto p){
            return ranges::vec(
              std::vector<id>{zero, one},
              [&](auto v){
                return eq(code, p, v);
              });
          });

        std::vector<id> H{
          land(code, {g[0][1],g[1][0],g[2][0]}),
          land(code, {g[0][0],g[1][0],g[2][1]})
        };

        SUBCASE("0"){

          auto [t0, b0] = obs(code, H[0], w);
          auto [t1, b1] = obs(code, H[1], w);
          auto a0 = then(code, {t0, t1});
          auto [host, exe, alg] = cc(a0,{Q},{b0, b1, Q});

          auto b0_mem = host.b(width, b0);
          auto b1_mem = host.b(width, b1);
          auto Q_mem = host.i(width, Q);

          REQUIRE(Q_mem.size() == n);
          REQUIRE(b0_mem.size() == n);
          REQUIRE(b1_mem.size() == n);

          Q_mem[0] = 1;

          exe.run({alg});

          std::vector<icell<width>> Q_mem_expected{
            1,0,0,0,
            0,0,0,0
          };
          REQUIRE(Q_mem == Q_mem_expected);

          std::vector<bcell<width>> b0_mem_expected{
            0,1,0,0,
            0,0,0,0
          };
          REQUIRE(b0_mem == b0_mem_expected);
          std::vector<bcell<width>> b1_mem_expected{
            0,0,0,0,
            0,0,0,1
          };
          REQUIRE(b1_mem == b1_mem_expected);
        }
        SUBCASE("1"){

          auto c = con(code, lor(code, {H[0]}), i);
          auto m = codes::put(code, a, c);

          auto [host, exe, alg] = cc(m,{Q},{a});

          auto Q_mem = host.i(width, Q);
          auto a_mem = host.b(width, a);

          REQUIRE(Q_mem.size() == n);
          REQUIRE(a_mem.size() == 1);

          Q_mem[0] = 1;

          exe.run({alg});

          std::vector<icell<width>> Q_mem_expected{
            1,0,0,0,
            0,0,0,0
          };
          REQUIRE(Q_mem == Q_mem_expected);

          std::vector<bcell<width>> a_mem_expected{1};
          REQUIRE(a_mem == a_mem_expected);

          Q_mem[0] = 0;

          exe.run({alg});

          Q_mem_expected[0] = 0;
          REQUIRE(Q_mem == Q_mem_expected);

          a_mem_expected[0] = 0;
          REQUIRE(a_mem == a_mem_expected);

        }
        SUBCASE("2"){

          auto s = codes::put(code, dQdt,
                              sel(code,
                                  H[0],
                                  {
                                    zero,
                                    one
                                  }));

          auto [host, exe, alg] = cc(s,{Q,dQdt},{Q,dQdt});

          auto Q_mem = host.i(width, Q);
          auto dQdt_mem = host.i(width, dQdt);

          REQUIRE(Q_mem.size() == n);
          REQUIRE(dQdt_mem.size() == n);

          Q_mem[0] = 1;

          exe.run({alg});

          std::vector<icell<width>> Q_mem_expected{
            1,0,0,0,
            0,0,0,0
          };
          REQUIRE(Q_mem == Q_mem_expected);

          std::vector<icell<width>> dQdt_mem_expected{
            0,1,0,0,
            0,0,0,0
          };
          REQUIRE(dQdt_mem == dQdt_mem_expected);

        }

      }

      SUBCASE("loop"){

        SUBCASE("0"){
          auto m = loop(code, code.builtins.brk);
          auto [host, exe, alg] = cc(m,{},{});
          CHECK_NOTHROW(exe.run({alg}));
        }
        SUBCASE("1"){
          auto m = loop(code, code.builtins.ret);
          auto [host, exe, alg] = cc(m,{},{});
          CHECK_NOTHROW(exe.run({alg}));
        }
        SUBCASE("2"){
          auto m = loop(code, exit(code, icst(code, 3, w)));
          CHECK_THROWS(cc(m,{},{}));
        }
        SUBCASE("3"){
          SUBCASE("0"){
            auto m = loop(code, exit(code, icst(code, -4, w)));
            CHECK_THROWS(cc(m,{},{}));
          }
          SUBCASE("1"){
            auto m = loop(code, exit(code, icst(code, -2, w)));
            auto [host, exe, alg] = cc(m,{},{});
            CHECK_NOTHROW(exe.run({alg}));
          }
        }
        SUBCASE("4"){
          auto x = codes::ivar(code, w);

          auto m = loop(
            code,
            then(
              code,
              {
                codes::put(code, x, icst(code, 2, w)),
                code.builtins.brk,
                codes::put(code, x, icst(code, 8, w))
              }));
          auto [host, exe, alg] = cc(m,{},{x});
          CHECK_NOTHROW(exe.run({alg}));
          auto x_mem = host.i(width, x);
          REQUIRE(x_mem.size() == 1);
          CHECK(x_mem[0] == 2);
        }
        SUBCASE("5"){
          auto x = codes::ivar(code, w),
            y = codes::ivar(code, w);

          auto m = loop(
            code,
            then(
              code,
              {
                codes::put(code, x, icst(code, 2, w)),
                loop(
                  code,
                  then(
                    code,
                    {
                      codes::put(code, x, icst(code, 4, w)),
                      code.builtins.brk,
                      codes::put(code, x, icst(code, -4, w))
                    })),
                codes::put(code, y, icst(code, 8, w)),
                code.builtins.brk
              }));
          auto [host, exe, alg] = cc(m,{},{x, y});

          CHECK_NOTHROW(exe.run({alg}));
          auto x_mem = host.i(width, x);
          auto y_mem = host.i(width, y);
          REQUIRE(x_mem.size() == 1);
          REQUIRE(y_mem.size() == 1);
          REQUIRE(x_mem[0] == 4);
          REQUIRE(y_mem[0] == 8);
        }
        SUBCASE("6"){
          auto x = codes::ivar(code, w);

          auto m = loop(
            code,
            then(
              code,
              {
                codes::put(code, x, icst(code, 2, w)),
                loop(
                  code,
                  then(
                    code,
                    {
                      codes::put(code, x, icst(code, 4, w)),
                      code.builtins.ret,
                      codes::put(code, x, icst(code, -4, w))
                    })),
                codes::put(code, x, icst(code, 8, w)),
                code.builtins.brk
              }));
          auto [host, exe, alg] = cc(m,{},{x});
          CHECK_NOTHROW(exe.run({alg}));
          auto x_mem = host.i(width, x);
          REQUIRE(x_mem.size() == 1);
          CHECK(x_mem[0] == 4);
        }
        SUBCASE("7"){
          auto x = codes::ivar(code, w);

          auto m = loop(
            code,
            then(
              code,
              {
                codes::put(code, x, icst(code, 2, w)),
                loop(
                  code,
                  then(
                    code,
                    {
                      codes::put(code, x, icst(code, 4, w)),
                      exit(code, icst(code, -3, w)),
                      codes::put(code, x, icst(code, -4, w))
                    })),
                codes::put(code, x, icst(code, 8, w)),
                code.builtins.brk
              }));
          auto [host, exe, alg] = cc(m,{},{x});
          CHECK_NOTHROW(exe.run({alg}));
          auto x_mem = host.i(width, x);
          REQUIRE(x_mem.size() == 1);
          CHECK(x_mem[0] == 8);
        }
      }

      SUBCASE("fork"){

        SUBCASE("0"){
          auto x = codes::ivar(code, w),
            y = codes::ivar(code, w);

          auto m =
            fork(
              code,
              x,
              {
                put(code, y, icst(code, 0, w)),
                put(code, y, icst(code, 1, w))
              });

          auto [host, exe, alg] = cc(m,{x},{y});
          auto x_mem = host.i(width, x),
            y_mem = host.i(width, y);
          REQUIRE(x_mem.size() == 1);
          REQUIRE(y_mem.size() == 1);

          x_mem[0] = 0;
          CHECK_NOTHROW(exe.run({alg}));
          CHECK(y_mem[0] == 0);

          x_mem[0] = 1;
          CHECK_NOTHROW(exe.run({alg}));
          CHECK(y_mem[0] == 1);
        }
      }

      if constexpr(fixture.enable_reals)
        SUBCASE("rms"){

          auto a = rvar(code, width);
          auto f = rms(code, {add(code,{a, a})});

          SUBCASE("0"){
            auto [t, b] = obs(code, f, w);

            auto [host, exe, alg] = cc(t,{a},{b});

            auto a_mem = host.r(width, a);
            for(auto a_val:std::vector<rcell<width>>{-4, -2, 0, 2, 4}){
              std::ranges::fill(a_mem,a_val);
              auto b_mem = host.r(width, b);
              exe.run({alg});
              CHECK(b_mem[0] == approx(std::abs(2*a_val)));
            }
          }
          SUBCASE("1"){

            auto g = lnot(code, lt(code, f, rcst(code, 5.0_r, w)));
            auto t = loop(
              code,
              then(
                code,{
                  if_(code, {g}, {code.builtins.brk}),
                  put(code, a, add(code, {a, rcst(code, 10.0_r, w)}))
                }));

            auto [host, exe, alg] = cc(t,{a},{a});
            auto a_mem = host.r(width, a);
            std::ranges::fill(a_mem,0);
            exe.run({alg});
            CHECK(a_mem[0] == approx(std::abs(10.0_r)));
          }

        }
    }

}

}// hysj::devices
#include<hysj/tools/epilogue.hpp>
