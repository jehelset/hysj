#include<algorithm>
#include<ranges>
#include<vector>

#include<kumi/tuple.hpp>

#include<hysj/grammars.hpp>
#include<hysj/tools/trees.hpp>
#include<hysj/tools/type_traits.hpp>
#include<hysj/tools/types.hpp>
#include<hysj/tools/functional/fold.hpp>

#include<framework.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::grammars{

#define HYSJ_GRAMMARS_TEST_OBJS \
  (cst)                         \
  (add)                         \
  (mul)                         \
  (blk)

enum class testobjtag : natural {
    HYSJ_SPLAT(0,HYSJ_GRAMMARS_TEST_OBJS)
};
HYSJ_MIRROR_ENUM(testobjtag,HYSJ_GRAMMARS_TEST_OBJS)

template<> constexpr bool is_obj<testobjtag> = true;

#define HYSJ_GRAMMARS_TEST_RELS \
  (oprn)

enum class testreltag : natural {
    HYSJ_SPLAT(0,HYSJ_GRAMMARS_TEST_RELS)
};
HYSJ_MIRROR_ENUM(testreltag,HYSJ_GRAMMARS_TEST_RELS)

template<> constexpr bool is_rel<testreltag> = true;

#define HYSJ_GRAMMARS_TEST_FAMS \
  (expr)                        \
  (stmt)

enum class testfamtag : natural {
    HYSJ_SPLAT(0,HYSJ_GRAMMARS_TEST_FAMS)
};
HYSJ_MIRROR_ENUM(testfamtag,HYSJ_GRAMMARS_TEST_FAMS)

template<> constexpr bool inline is_fam<testfamtag> = true;

template<testobjtag O>
constexpr auto objgram_of<O> = []{
  if constexpr(O == testobjtag::cst)
    return objgram{
      .tree = trees::tup(),
      .rels = kumi::tuple{},
      .args = kumi::tuple{}
    };
  if constexpr(O == testobjtag::add)
    return objgram{
      .tree = trees::tup(trees::one, trees::one),
      .rels = kumi::tuple{testreltag::oprn,testreltag::oprn},
      .args = kumi::tuple{testfamtag::expr,testfamtag::expr}
    };
  if constexpr(O == testobjtag::mul)
    return objgram{
      .tree = trees::tup(trees::var(trees::one)),
      .rels = kumi::tuple{testreltag::oprn},
      .args = kumi::tuple{testfamtag::expr}
    };
  if constexpr(O == testobjtag::blk)
    return objgram{
      .tree = trees::tup(trees::var(trees::one)),
      .rels = kumi::tuple{testreltag::oprn},
      .args = kumi::tuple{testfamtag::stmt}
    };
}();
template<testobjtag O>
constexpr auto symdat_of<O> = []{
  if constexpr(O == testobjtag::cst)
    return type_c<int>;
  else
    return type_c<void>;
}();

template<testreltag R>
constexpr auto symdat_of<R> = []{
  return type_c<void>;
}();

template<testfamtag F>
constexpr auto famgram_of<F> = []{
  if constexpr(F == testfamtag::expr)
    return famgram{
      .objs = std::array{
        testobjtag::cst, testobjtag::add, testobjtag::mul
      }
    };
  if constexpr(F == testfamtag::stmt)
    return famgram{
      .objs = std::tuple{
        testobjtag::blk, testfamtag::expr
      }
    };
}();

using testsyn = syntax<testobjtag, testreltag>;

#define HYSJ_LAMBDA(testobj)\
  constexpr auto testobj = objadd<testobjtag, testreltag, testobjtag::testobj>;
HYSJ_MAP_LAMBDA(HYSJ_GRAMMARS_TEST_OBJS)
#undef HYSJ_LAMBDA

#define HYSJ_LAMBDA(testobj)\
  using testobj##obj = sym<testobjtag::testobj>;
HYSJ_MAP_LAMBDA(HYSJ_GRAMMARS_TEST_OBJS)
#undef HYSJ_LAMBDA

#define HYSJ_LAMBDA(testrel)\
  using testrel##rel = sym<testreltag::testrel>;
HYSJ_MAP_LAMBDA(HYSJ_GRAMMARS_TEST_RELS)
#undef HYSJ_LAMBDA

#define HYSJ_LAMBDA(testfam)\
  using testfam##fam = fam<testfamtag::testfam>;
HYSJ_MAP_LAMBDA(HYSJ_GRAMMARS_TEST_FAMS)
#undef HYSJ_LAMBDA

}

HYSJ_ENUM_FORMATTER(hysj::_HYSJ_VERSION_NAMESPACE::grammars::testobjtag);
HYSJ_ENUM_FORMATTER(hysj::_HYSJ_VERSION_NAMESPACE::grammars::testreltag);
HYSJ_ENUM_FORMATTER(hysj::_HYSJ_VERSION_NAMESPACE::grammars::testfamtag);

namespace hysj::inline _HYSJ_VERSION_NAMESPACE::grammars{

TEST_CASE("grammars"){

  testsyn syn{};

  constexpr auto csttree = objgram_of<testobjtag::cst>.tree;

  static_assert(csttree.tag() == trees::rule::tup);
  static_assert(csttree.size() == 0);
  static_assert(csttree.len() == 2);

  cstobj o0 = cst(syn, 4);
  REQUIRE(symdat(syn, o0) == 4);

  auto A0 = args(syn, o0);
  auto R0 = rels(syn, o0);
  static_assert(std::is_same_v<kumi::tuple<>,autotype(A0)>);
  static_assert(std::is_same_v<kumi::tuple<>,autotype(R0)>);
  static_assert(!grammars::objvararg<testobjtag::add, 0>);
  static_assert(grammars::_::objarg_init<cstobj, testobjtag::add, 0>);
  static_assert(grammars::_::objarg_init<cstobj, testobjtag::add, 1>);
  constexpr auto addtree = objgram_of<testobjtag::add>.tree;

  static_assert(addtree.tag() == trees::rule::tup);
  static_assert(addtree.size() == 2);
  static_assert(addtree.subrule_count(0) == 2);
  static_assert(addtree.subrule_tag(0, 0) == trees::rule::one);
  static_assert(addtree.subrule_tag(0, 1) == trees::rule::one);
  static_assert(addtree.len() == 4);

  addobj o1 = add(syn, o0, o0);
  auto [a1,a2] = args(syn, o1);
  auto [r1,r2] = rels(syn, o1);

  static_assert(std::is_same_v<exprfam,autotype(a1)>);
  static_assert(std::is_same_v<exprfam,autotype(a2)>);
  static_assert(std::is_same_v<oprnrel,autotype(r1)>);
  static_assert(std::is_same_v<oprnrel,autotype(r2)>);

  auto d1 = symdat(syn, o1);
  static_assert(std::is_same_v<none,autotype(d1)>);

  CHECK(a1.id() == o0.id());
  CHECK(a2.id() == o0.id());

  mulobj o2 = mul(syn, std::vector<exprfam>{{o1},{o0},{o0}});

  constexpr auto multree = objgram_of<testobjtag::mul>.tree;

  static_assert(multree.tag() == trees::rule::tup);
  static_assert(multree.size() == trees::none);
  static_assert(multree.subrule_count(0) == 1);
  static_assert(multree.subrule_tag(0, 0) == trees::rule::var);
  static_assert(multree.len() == 4);
  static_assert(multree.subrule_count(0, 0) == 1);
  static_assert(multree.subrule_tag(0, 0, 0) == trees::rule::one);

  auto [A2] = args(syn, o2);
  auto [R2] = rels(syn, o2);

  REQUIRE(std::ranges::size(A2) == 3);
  REQUIRE(std::ranges::size(R2) == 3);

  static_assert(std::is_same_v<std::ranges::range_value_t<autotype(A2)>,exprfam>);
  static_assert(std::is_same_v<std::ranges::range_value_t<autotype(R2)>,oprnrel>);

  CHECK(A2[0].id() == o1.id());
  CHECK(A2[1].id() == o0.id());
  CHECK(A2[2].id() == o0.id());

  blkobj o3 = blk(syn);
  auto [A3] = args(syn, o3);
  auto [R3] = rels(syn, o3);
  REQUIRE(std::ranges::empty(A3));
  REQUIRE(std::ranges::empty(R3));
  blkobj o4 = blk(syn, std::vector<stmtfam>{ stmtfam{o3}, stmtfam{exprfam{o2}} });
  auto [A4] = args(syn, o4);
  auto [R4] = rels(syn, o4);
  REQUIRE(std::ranges::size(A4) == 2);
  REQUIRE(std::ranges::size(R4) == 2);

  static_assert(objdepth<testobjtag::add> == 0);
  static_assert(objdepth<testfamtag::expr> == 1);
  {
    auto S = famcastseq<testfamtag::expr>(testobjtag::mul);
    REQUIRE(S.back() == 1);
    REQUIRE(S[0] == 2);
  }
  {
    auto S = famcastseq<testfamtag::expr>(testobjtag::blk);
    REQUIRE(S.back() == 0);
  }
  static_assert(objdepth<testfamtag::stmt> == 2);
  {
    auto S = famcastseq<testfamtag::stmt>(testobjtag::blk);
    REQUIRE(S.back() == 1);
    REQUIRE(S[0] == 0);
  }
  {
    auto S = famcastseq<testfamtag::stmt>(testobjtag::add);
    REQUIRE(S.back() == 2);
    REQUIRE(S[0] == 1);
    REQUIRE(S[1] == 1);
  }
  static_assert(!can_famcast<testfamtag::expr>(testobjtag::blk));
  static_assert(can_famcast<testfamtag::stmt>(testobjtag::blk));
  static_assert(can_famcast<testfamtag::stmt>(testobjtag::cst));

  REQUIRE(famcast<testfamtag::expr>(cstobj{2}) == just(exprfam(cstobj{2})));
  REQUIRE(famcast<testfamtag::expr>(addobj{7}) == just(exprfam(addobj{7})));
  REQUIRE(famcast<testfamtag::expr>(mulobj{4}) == just(exprfam(mulobj{4})));
  REQUIRE(famcast<testfamtag::expr>(blkobj{8}) == no<exprfam>);

  REQUIRE(famcast<testfamtag::stmt>(blkobj{10}) == just(stmtfam(blkobj{10})));
  REQUIRE(famcast<testfamtag::stmt>(cstobj{14}) == just(stmtfam(exprfam(cstobj{14}))));

}

}
#include<hysj/tools/epilogue.hpp>
