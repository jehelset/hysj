from hysj import codes,devices,animations

import numpy as np
import subprocess as sp

width = 64*8
height = 64*8
colors = [

  0x2b2e25,
  0x258b5b,
  0xe5a251,
  0xdbda7b,

]
length = 64*4

try:

  vulkan = devices.vulkan

  code: codes.Code = codes.Code()
  Z = code.builtins.sets.integers

  N = [ length, height, width ]
  E = [ code.icst32(n) for n in N ]
  I = [ code.itr(e) for e in E ]
  J = [ code.sub(e,i)for (e,i) in zip(E,I) ]
  P = [ code.lt(i,code.div(e,code.icst32(2))) for (e,i) in zip(E,I) ]
  A = [ code.sel(p,[j,i]) for (p,i,j) in zip(P,I,J) ]
  B = [ code.sel(p,[i,j]) for (p,i,j) in zip(P,I,J) ]

  c0 = code.icst32(2)
  d = code.icst32(len(colors))

  f = [
    code.cvt(
      code.builtins.types.n8,
      code.mod(
        code.bor([
          code.div(
            code.bnot(code.bxor([code.bnot(A[1]),code.bnot(code.neg(A[2]))])),
            code.add([A[0],c0])),
          code.div(
            code.bnot(code.band([A[1],code.neg(A[2])])),
            code.add([A[0],c0]))
        ]),
        d))
  ]
  

  render = code.nvar8(I)

  color = code.itr(code.ncst32(len(colors) * 3))
  palette = code.nvar8([color])

  frame = code.put(render, f[0])

  mem = code.builtins.host
  dev = code.dev()

  main = code.then([
    code.to(mem, dev, palette),
    code.on(dev, frame),
    code.to(dev, mem, render)
  ])
  api = code.api([code.decl(main)])

  print('compiling...')
  code.compile(api.id())
  env = devices.vulkan.Env()
  env.devs[0].sym = dev
  mem = env.alloc(code, api)
  exe = env.load(mem, code, api)
  print('done...')

  print('run...')
  animations.write_rgb32_palette(mem, palette.id(), colors)
  exe.run(main)
  print('done...')

  print('render...')
  animations.write_gif(mem = mem,
                       palette = palette.id(),
                       frames = render.id(),
                       path = "devices_vulkan_1.gif",
                       delay = 20)
  print('done...')
except Exception as e:
  import traceback
  traceback.print_exc()
