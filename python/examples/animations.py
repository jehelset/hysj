import hysj
import numpy as np

deep_maze: list[int] = [
    0x00, 0x00, 0x00,
    0xFF, 0x00, 0x00,
    0x00, 0xFF, 0x00,
    0x00, 0x00, 0xFF,
    0xFF, 0xFF, 0xFF,
]

code = hysj.codes.Code()

palette_len = code.ncst32(len(deep_maze))
color = code.itr(palette_len)

frame_num, height, width = list(map(code.ncst32, (64, 128, 128)))
frame, row, col = list(map(code.itr, (frame_num, height, width)))



palette = code.nvar8([color])
frames = code.nvar8([frame, row, col])

mem = code.builtins.host

api = code.api(list(map(code.decl, (code.pin(mem, palette), code.pin(mem, frames)))))

code.compile(api.id())

mem = hysj.codes.alloc(code, api)

palette_mem = np.asarray(mem.n8(palette.id()))
palette_mem[:] = deep_maze[:]

frames_mem = np.asarray(mem.n8(frames.id()))
for i in range(code.dat(frame_num).cell):
    frames_mem[i,:,:] = i % (len(deep_maze) // 3)

hysj.animations.write_gif(mem = mem,
                          palette = palette.id(),
                          frames = frames.id(),
                          path = "animations_0.gif",
                          delay = 50)

with hysj.animations.GifWriter(width = 20,
                               height = 20,
                               palette = np.array([0, 0, 0, 255, 255, 255], dtype=np.uint8),
                               path = "animations_1.gif") as \
                               gif_writer:
    gif_writer(np.array([0] * 20 * 20, dtype = np.uint8))
    gif_writer(np.array([1] * 20 * 20 * 2, dtype = np.uint8), delay = 10)
    gif_writer(np.array([0] * 20 * 20 * 4, dtype = np.uint8))


with hysj.animations.MemGifWriter(mem = mem,
                                  palette = palette.id(),
                                  buffer = frames.id(),
                                  path = "animations_2.gif") as \
                                  gif_writer:
    gif_writer(offset = 0, count = 1)
    gif_writer(offset = 1, count = code.dat(frame_num).cell - 1, delay = 10)
