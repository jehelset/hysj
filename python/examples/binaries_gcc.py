from hysj import codes, binaries
import subprocess

try:

  gcc = binaries.gcc

  code: codes.Code = codes.Code()

  n = code.icst64(2 ** 24)
  i = code.itr(n)
  y = code.rvar64([i])
  a = code.rcst64(0.1)
  x = code.rvar64([i])
  t = code.put(y, code.add([code.mul([a, x]), y]));
  code.compile(t.id())
  config = gcc.Config()
  config.arch="znver4"
  config.alignment = 64
  config.thread_count = 64
  config.optimization_level = 3
  config.as_path="gcc"
  bin = gcc.cc(config, code, t)
except Exception as e:
  import traceback
  traceback.print_exc()
