import sys

import cairo

import dominate
from dominate.tags import *
from dominate.util import raw

from hysj import codes
import hysj.codes.latex

try:

  code: codes.Code = codes.Code()

  zero = code.builtins.lits.zero64
  one = code.builtins.lits.one64
  two = code.builtins.lits.two64

  x = code.rvar64()
  y = code.rvar64()
  a = code.rvar64()

  f = code.pow(code.add([x,y]),two)

  F = [f]
  S = [x]
  T = [a]

  def subst(fst):
    f,s,t = fst
    h, = codes.clone(code,[f.id()])
    codes.partial_opsub(code = code,root = h,old = s.id(),new = t.id())
    return code.objcast(h)
    
  G = list(map(subst,zip(F,S,T)))

  doc = dominate.document(title='opsubs')
  with doc.head:
    link(rel='stylesheet', href='style.css')
    script(type='text/javascript', src='script.js')
  with doc:
    def latex_symbol_renderer(s,d):
      return ['x','y','a'][s.idx]
    latex_renderer = codes.latex.Renderer(code,latex_symbol_renderer)  
    def render(o):
      if o is None:
        return '\\emptyset'
      return latex_renderer(o)
    for f,s,t,g in zip(F,S,T,G):
      f_latex = render(f)
      s_latex = render(s)
      t_latex = render(t)
      g_latex = render(g)
      raw(codes.latex.tex_to_svg(tex_content = f'${f_latex}[{s_latex} \\to {t_latex}] \\quad \\to \\quad {g_latex}$',tex_fontsize = 12) + r'<br>')
  with open('codes_opsubs.html', 'w') as doc_file:
    doc_file.write(str(doc))

except Exception as e:
  import traceback
  print(traceback.format_exc())
