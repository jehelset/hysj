import sys

import dominate
from dominate.tags import *
from dominate.util import raw

from hysj import codes
import hysj.codes.latex

try:
  code: codes.Code = codes.Code()

  zero = code.builtins.lits.zero64
  one = code.builtins.lits.one64
  two = code.builtins.lits.two64
  minue_one = code.builtins.lits.negone64
  e = code.builtins.lits.e64
  
  x = [ code.rvar64() for i in range(5) ]

  f0 = code.mul([
    code.add([x[0],x[1]]),
    x[2]
  ])
  f1 = code.pow(code.mul([two,two,x[0],x[1]]),two)
  f2 = code.lor([x[0],code.land([x[1],x[2]])])
  f3 = code.land([
    code.lor([
      code.land([
        code.lor([x[0],x[1]]),
        x[2]
      ]),
      x[3]]),
    x[4]
  ])

  D = [codes.muldist,
       codes.powdist,
       codes.lordist,
       codes.landdist]
  F = [f0,f1,f2,f3]

  def dist(df):
    d,f = df
    g, = d(code = code, roots = codes.clone(code, [f.id()]))
    return code.objcast(g)

  G = list(map(dist,zip(D,F)))
  
  doc = dominate.document(title='distribution')
  with doc.head:
    link(rel='stylesheet', href='style.css')
    script(type='text/javascript', src='script.js')
  with doc:
    def latex_symbol_renderer(s,d):
      return f'x_{{{s.idx}}}'
    latex_renderer = codes.latex.Renderer(code,latex_symbol_renderer)  
    def render(o):
      if o is None:
        return '\\emptyset'
      return latex_renderer(o)
    for f,g in zip(F,G):
      f_latex = render(f)
      g_latex = render(g)
      raw(codes.latex.tex_to_svg(tex_content = f'${f_latex} \\quad \\to \\quad {g_latex}$',tex_fontsize = 12) + r'<br>')
  with open('codes_dists.html', 'w') as doc_file:
    doc_file.write(str(doc))

except Exception as e:
  import traceback
  print(traceback.format_exc())
 
