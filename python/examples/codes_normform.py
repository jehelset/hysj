import sys

import dominate
from dominate.tags import *
from dominate.util import raw

from hysj import codes
import hysj.codes.latex

try:

  code: codes.Code = codes.Code()

  zero = code.builtins.lits.zero64
  one = code.builtins.lits.one64
  two = code.builtins.lits.two64
  minue_one = code.builtins.lits.negone64
  e = code.builtins.lits.e64
  
  t = code.rvar64()
  x = code.rvar64()
  y = code.rvar64()
  a = code.rvar64()

  f0 = code.add([code.mul([code.negone(code.builtins.widths.w64),code.pow(x,two)]),
                     code.pow(x,code.icst64(2))])
  f1 = code.div(
    code.mul([
      two,
      code.pow(
        code.add([t,code.neg(a)]),
        code.icst64(2))
    ]),
    two)
  f2 = code.div(
    code.sub(t,a),
    code.pow(code.sub(one,a),two))
  f3 = code.div(
    code.sub(x,a),
    x)
  f4 = code.mul([
    two,
    code.pow(
      code.add([x,x]),
      two
    )
  ])
  f5 = code.mul([
    two,
    code.pow(
      code.mul([two,x]),
      two
    )
  ])
                                                  
  def latex_symbol_renderer(s,d):
    return ['t','x','y','a'][s.idx]
  latex_renderer = codes.latex.Renderer(code,latex_symbol_renderer)  
  def latex(f):
    return latex_renderer(f)

  doc = dominate.document(title='normform')
  with doc.head:
    link(rel='stylesheet', href='style.css')
    script(type='text/javascript', src='script.js')

  with doc:
    for f in [f0,f1,f2,f3,f4,f5]:
      (_,g), = codes.normform(code,codes.clone(code,[f.id()]),[t.id(),x.id(),y.id(),a.id()])
      f_latex = latex(f)
      if g == None:
        g_latex = '\\emptyset'
      else:
        g_latex = latex(code.objcast(g))
      raw(codes.latex.tex_to_svg(tex_content = f'${f_latex} \\quad \\to \\quad {g_latex}$',tex_fontsize = 12) + r'<br>')
  with open('codes_normform.html', 'w') as doc_file:
    doc_file.write(str(doc))
  
except Exception as e:
  import traceback
  print(traceback.format_exc())
