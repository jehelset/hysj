import sys

import dominate
from dominate.tags import *
from dominate.util import raw

from hysj import codes
import hysj.codes.latex

try:
  code: codes.Code = codes.Code()

  X = [code.rvar64(),code.rvar64()]

  F = [
    code.mul([code.add([X[1],X[0]]),X[0]]),
    code.mul([code.add([X[0],X[1]]),X[0]]),
    code.mul([X[0],code.add([X[0],X[1]])]),
    code.mul([X[0],code.add([X[1],X[0]])])
  ]

  def reorder(f):
    f_clone = codes.clone(code,[f.id()])
    g, = codes.argsort(code,f_clone)
    if g is None:
      return f
    return code.objcast(g)

  G = list(map(reorder,F))

  doc = dominate.document(title='codes_argsorts')
  with doc.head:
    link(rel='stylesheet', href='style.css')
    script(type='text/javascript', src='script.js')
  with doc:
    def latex_symbol_renderer(s,d):
      return {X[0]: 'x_0',X[1]: 'x_1'}.get(s,'?')

    latex_renderer = codes.latex.Renderer(code,latex_symbol_renderer)  
    def render(o):
      if o is None:
        return '\\emptyset'
      return latex_renderer(o)
    for f,g in zip(F,G):
      f_latex = render(f)
      g_latex = render(g)
      raw(codes.latex.tex_to_svg(tex_content = f'${f_latex} \\quad \\to \\quad {g_latex}$',tex_fontsize = 12) + r'<br>')
  with open('codes_argsorts.html', 'w') as doc_file:
    doc_file.write(str(doc))

except Exception as e:
  import traceback
  print(traceback.format_exc())
