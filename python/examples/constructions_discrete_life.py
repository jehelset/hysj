from dataclasses import dataclass, field
from itertools import chain
from typing import Optional, ClassVar, Callable

import numpy as np
import subprocess as sp

from hysj import *

dautomata = automata.discrete
dexec = executions.discrete
druns = constructions.discrete


@dataclass
class Birth:
    rad: int
    new_min: int
    new_max: int


@dataclass
class Rule:
    rad: int
    min: int
    max: int

    @staticmethod
    def skip():
        return Rule(rad=1, min=0, max=0)


@dataclass
class Board:
    extent: int
    stop: int

    def frame(self):
        return (self.extent, self.extent)


class Palettes:
    bw: list[int] = [0x000000, 0xFFFFFF]
    # NOTE: stolen from lospec.com! - jeh
    soapy: list[int] = [
        0x0D2147,
        0x054B84,
        0x0C6987,
        0x2BA4A6,
        0x54CEA7,
        0xFFB0BF,
        0xFF82BD,
        0xD74AC7,
        0xA825BA,
        0x682B9C,
    ]
    chasm: list[int] = [
        0x32313B,
        0x463C5E,
        0x5D4776,
        0x855395,
        0xAB58A8,
        0xCA60AE,
        0x444774,
        0x4C60AA,
        0x5F6EE7,
        0x5FA1E7,
        0x5FC9E7,
        0x85DAEB,
        0xF3A787,
        0xF5DAA7,
        0x8DD894,
        0x5DC190,
        0x4AB9A3,
        0x4593A5,
        0x5EFDF7,
        0xFF5DCC,
        0xFDFE89,
        0xFFFFFF,
    ]
    deep_maze: list[int] = [
        0x001D2A,
        0x085562,
        0x009A98,
        0x00BE91,
        0x38D88E,
        0x9AF089,
        0xF2FF66,
    ]
    curiosities: list[int] = [
        0x46425E,
        0x15788C,
        0x00B9BE,
        0xFFEECC,
        0xFFB0A3,
        0xFF6973,
    ]
    mushrooms: list[int] = [
        0x2E222F,
        0x45293F,
        0x7A3045,
        0x993D41,
        0xCD683D,
        0xFBB954,
        0xF2EC8B,
        0xB0A987,
        0x997F73,
        0x665964,
        0x443846,
        0x576069,
        0x788A87,
        0xA9B2A2,
    ]


@dataclass
class RandomInit:
    min: int
    max: int

    def __call__(self, board: Board):
        return np.where(
            np.random.randint(self.max, size=board.frame()) > self.min, 1, 0
        )


class Cell:
    rank: ClassVar[int] = 2

    @staticmethod
    def values():
        return list(range(Cell.rank))

    dead: int = 0
    alive: int = 1


@dataclass
class Fixture:
    cells: list[tuple[int, int]] = field(default_factory=[(0, 0)])

    def __call__(self, board: Board):
        width, height = board.frame()
        init = np.zeros(shape=board.frame(), dtype=np.int32)
        for x, y in self.cells:
            init[(width // 2) + x, (height // 2) + y] = Cell.alive
        return init


@dataclass
class Life:
    name: str = "conway"
    board: Board = field(default_factory=lambda: Board(extent=64 * 32, stop=64))
    order: int = 1

    births: list[list[Rule]] = field(
        default_factory=lambda: [[Rule(rad=3, min=3, max=4)]]
    )
    deaths: list[list[Rule]] = field(
        default_factory=lambda: [
            [Rule(rad=3, min=0, max=2), Rule(rad=3, min=4, max=3**2)]
        ]
    )
    palette: list[int] = field(default_factory=lambda: Palettes.soapy)

    init: Callable[[Board], np.array] = field(
        default_factory=lambda: RandomInit(min=58, max=64)
    )

    def cells(self):
        return self.init(self.board)

    def rules(self):
        return chain(*self.births, *self.deaths)

    def rads(self):
        return set(map(lambda r: r.rad, self.rules()))

    def max_rad(self):
        return max(self.rads())

    @staticmethod
    def conway():
        return Life()

    @staticmethod
    def switched_conway():
        life = Life.conway()
        life.init = Fixture(
            cells=[
                (0, 3),
                (-1, 2),
                (0, 2),
                (1, 2),
                (-2, 1),
                (-1, 1),
                (1, 1),
                (2, 1),
                (-3, 0),
                (-2, 0),
                (2, 0),
                (3, 0),
                (-2, -1),
                (-1, -1),
                (1, -1),
                (2, -1),
                (-1, -2),
                (0, -2),
                (1, -2),
                (0, -3),
            ]
        )
        life.name = "switched_conway"
        life.births = [[Rule(rad=3, min=3, max=4)], [Rule(rad=5, min=5, max=15)]]
        life.palette = Palettes.curiosities
        return life

    @staticmethod
    def switched_conway2():
        life = Life.conway()
        life.init = Fixture(
            cells=[
                (-3, 4),
                (-2, 4),
                (-3, 3),
                (0, 3),
                (3, 3),
                (-1, 2),
                (0, 2),
                (1, 2),
                (-2, 1),
                (-1, 1),
                (1, 1),
                (2, 1),
                (-3, 0),
                (-2, 0),
                (0, 0),
                (2, 0),
                (3, 0),
                (-2, -1),
                (-1, -1),
                (1, -1),
                (2, -1),
                (-1, -2),
                (0, -2),
                (1, -2),
                (0, -3),
                (-2, -3),
            ]
        )
        life.name = "switched_conway_asymmetry"
        life.births = [
            [Rule(rad=3, min=3, max=5)],
            [Rule(rad=3, min=3, max=4)],
            [Rule(rad=5, min=4, max=7)],
        ]
        life.palette = Palettes.mushrooms
        return life

    @staticmethod
    def switched_conway3():
        life = Life.conway()
        life.init = Fixture(
            cells=[
                (-3, 3),
                (0, 3),
                (3, 3),
                (-1, 2),
                (1, 2),
                (-2, 1),
                (2, 1),
                (-3, 0),
                (-2, 0),
                (0, 0),
                (2, 0),
                (3, 0),
                (-2, -1),
                (2, -1),
                (-1, -2),
                (1, -2),
                (-3, -3),
                (0, -3),
                (3, -3),
            ]
        )

        life.name = "switched_conway_symmetry"
        life.births = [
            [Rule(rad=3, min=3, max=5)],
            [Rule(rad=3, min=3, max=6)],
            [Rule(rad=5, min=4, max=14)],
            [Rule(rad=3, min=3, max=6)],
        ]
        life.palette = Palettes.mushrooms
        return life

    @staticmethod
    def bugs():
        life = Life()
        life.name = "bugs"
        life.init.min = 32
        life.deaths = [[Rule(rad=11, min=0, max=32), Rule(rad=11, min=58, max=122)]]
        life.births = [[Rule(rad=11, min=34, max=46)]]

        life.palette = Palettes.chasm
        return life

    @staticmethod
    def games():
        return [
            Life.conway(),
            Life.switched_conway(),
            Life.bugs(),
            Life.switched_conway2(),
            Life.switched_conway3(),
        ]


def model(code: codes.Code, life: Life = Life()):
    v_dead, v_alive = Cell.values()

    rads = life.rads()

    exts = list(map(code.icst32, life.board.frame()))
    w, h = exts

    dims = range(len(exts))

    itrs = list(map(code.itr, exts))
    x, y = itrs

    negone, zero, one, two = (
        code.builtins.lits.negone32,
        code.builtins.lits.zero32,
        code.builtins.lits.one32,
        code.builtins.lits.two32,
    )

    t = code.ivar32()

    q_states = list(map(code.icst32, Cell.values()))
    q_dead, q_alive = q_states

    q = code.var(code.typ32(code.icst32(life.max_rad() ** 2)), itrs)
    Q = codes.depvar(code, t, life.order, q)
    dqdt = Q[-1]

    m_state = list(
        map(lambda q_state: code.sel(code.eq(q, q_state), [zero, one]), q_states)
    )
    m_dead, m_alive = m_state

    ctrs = dict(
        map(
            lambda rad: (rad, list(map(lambda _: code.itr(code.icst32(rad)), dims))),
            rads,
        )
    )

    ctr_offsets = dict(map(lambda rad: (rad, code.icst32(-(rad // 2))), rads))

    adjs = dict(
        map(
            lambda rad: (
                rad,
                list(
                    map(
                        lambda i: code.add([itrs[i], ctrs[rad][i], ctr_offsets[rad]]),
                        dims,
                    )
                ),
            ),
            rads,
        )
    )

    m_state_adj = dict(
        map(
            lambda rad: (rad, list(map(lambda m: code.rot(m, adjs[rad]), m_state))),
            rads,
        )
    )

    N_state = dict(
        map(
            lambda rad: (
                rad,
                list(
                    map(
                        lambda v: code.sub(
                            code.con(code.add([m_state_adj[rad][v]]), ctrs[rad]),
                            m_state[v],
                        ),
                        Cell.values(),
                    )
                ),
            ),
            rads,
        )
    )

    z_death = code.mod(t, code.icst32(len(life.deaths)))
    z_birth = code.mod(t, code.icst32(len(life.births)))

    S_death = [code.eq(z_death, code.icst32(z)) for z in range(len(life.deaths))]
    S_birth = [code.eq(z_birth, code.icst32(z)) for z in range(len(life.births))]

    N_death_min = list(
        map(lambda D: list(map(lambda d: code.icst32(d.min), D)), life.deaths)
    )
    N_death_max = list(
        map(lambda D: list(map(lambda d: code.icst32(d.max), D)), life.deaths)
    )

    N_birth_min = list(
        map(lambda B: list(map(lambda b: code.icst32(b.min), B)), life.births)
    )
    N_birth_max = list(
        map(lambda B: list(map(lambda b: code.icst32(b.max), B)), life.births)
    )

    N_death_alive = list(
        map(lambda D: list(map(lambda d: N_state[d.rad][v_alive], D)), life.deaths)
    )
    N_birth_alive = list(
        map(lambda B: list(map(lambda b: N_state[b.rad][v_alive], B)), life.births)
    )

    G_death_lim = list(
        map(
            lambda i: list(
                map(
                    lambda j: code.land(
                        [
                            code.lt(
                                N_death_min[i][j], code.add([one, N_death_alive[i][j]])
                            ),
                            code.lt(N_death_alive[i][j], N_death_max[i][j]),
                        ]
                    ),
                    range(len(life.deaths[i])),
                )
            ),
            range(len(life.deaths)),
        )
    )
    G_birth_lim = list(
        map(
            lambda i: list(
                map(
                    lambda j: code.land(
                        [
                            code.lt(
                                N_birth_min[i][j], code.add([one, N_birth_alive[i][j]])
                            ),
                            code.lt(N_birth_alive[i][j], N_birth_max[i][j]),
                        ]
                    ),
                    range(len(life.births[i])),
                )
            ),
            range(len(life.births)),
        )
    )

    G_death = list(
        map(
            lambda i: list(
                map(
                    lambda j: code.land([m_alive, S_death[i], G_death_lim[i][j]]),
                    range(len(life.deaths[i])),
                )
            ),
            range(len(life.deaths)),
        )
    )

    G_birth = list(
        map(
            lambda i: list(
                map(
                    lambda j: code.land([m_dead, S_birth[i], G_birth_lim[i][j]]),
                    range(len(life.births[i])),
                )
            ),
            range(len(life.births)),
        )
    )

    g_death = list(chain(*G_death))
    g_birth = list(chain(*G_birth))

    G = g_death + g_birth

    f = list(map(lambda q_state: code.sub(q_state, q), q_states))
    f_death, f_birth = f

    F = [f_death] * len(g_death) + [f_birth] * len(g_birth)

    A = dautomata.Automaton(
        variables=codes.Vars(independent=t, dependent=[Q]),
        transitions=list(
            map(
                lambda i: dautomata.Transition(
                    variable=dqdt, function=F[i], guard=G[i]
                ),
                range(len(F)),
            )
        ),
    )

    return A


def simulate(code: codes.Code, automaton: dautomata.Automaton, life: Life = Life()):
    dev = code.dev()
 
    buffer_size = 128
    spec = dexec.Config(dev, buffer_size)
    exec = dexec.cc(code, automaton, spec)

    frame_itr = exec.buffer.itr
    color = code.itr(code.icst32(len(life.palette) * 3))

    q_exec = exec.state.buffer.dependent[0][0]
    t_exec, x, y = list(map(code.itrcast, code.itrs0(q_exec.id())))
    itrs = [x, y]

    frames = code.nvar8([t_exec, x, y])
    palette = code.nvar8([color])

    v_dead, v_alive = Cell.values()
    q_states = list(map(code.icst32, Cell.values()))
    q_dead, q_alive = q_states
    m_alive = code.eq(q_exec, q_alive)

    max_rad = life.max_rad()
    max_ctrs = [code.itr(code.icst32(max_rad)) for i in itrs]
    max_ctr_offset = code.icst32(-(max_rad // 2))
    max_adjs = [code.add([i, c, max_ctr_offset]) for i, c in zip(itrs, max_ctrs)]
    max_alive = code.sub(
        code.con(code.add([code.rot(m_alive, [t_exec] + max_adjs)]), max_ctrs), m_alive
    )

    frame = code.cvt(
        code.builtins.types.n8,
        code.mod(
            code.sel(m_alive, [code.builtins.lits.zero32, max_alive]),
            code.icst32(len(life.palette)),
        ),
    )

    put_palette = code.to(code.builtins.host, dev, palette)
    render = code.on(dev, code.put(frames, frame))
    get_frames = code.to(dev, code.builtins.host, frames)

    api = code.api(list(map(code.decl, (exec.api, put_palette, render, get_frames))))

    code.compile(api.id())

    env = devices.gcc.Env(binaries.gcc.Config(optimization_level = 3,
                                              thread_count = 64,
                                              alignment = 64,
                                              arch = "znver4"),
                          dev = dev)

    #FIXME: bug, ring-size == 0, -> negative event idnex
    # env = devices.vulkan.Env()
    # env.devs[0].sym = dev

    print("\t\tcompiling...")
    mem = env.alloc(code, api)
    exe = env.load(mem, code, api)

    run = druns.cc(exec, mem, exe)
    print("\t\tdone...")

    t_mem = np.asarray(run.time32())
    q_mem = np.asarray(run.istate32(0, 0))

    t_mem[0] = 0
    q_mem[0][:] = life.cells()

    animations.write_rgb32_palette(mem, palette.id(), life.palette)

    exe.run(put_palette)
    run.init()

    generation = 0
    with animations.MemGifWriter(
        mem=mem,
        palette=palette.id(),
        buffer=frames.id(),
        path=f"life_{life.name}.gif",
    ) as writer:
        event = dexec.step
        while event != dexec.stop and generation <= life.board.stop:
            run()
            exe.run(render)
            exe.run(get_frames)
            if run.ring_begin + run.ring_size <= run.ring_capacity:
                writer(offset = run.ring_begin, count = run.ring_size)
            else:
                right_size = run.ring_capacity - run.begin
                writer(offset = run.ring_begin, count = right_size)
                writer(offset = 0, count = run.ring_size)
            event = run.last_event
            generation = generation + run.ring_size
            print(f'\t\tgeneration {generation}...')

    print(f'\t\tdone')

try:
    for life in Life.games():
        code: codes.Code = codes.Code()
        print(f"executing {life.name} life automaton.")
        print("\n\tmodeling...")
        automaton = model(code=code, life=life)
        print("\tsimulating...")
        simulate(code=code, automaton=automaton, life=life)
        print("\ndone.")

except Exception as e:
    import traceback

    print(traceback.format_exc())
