import sys

import dominate
from dominate.tags import *
from dominate.util import raw

from hysj import codes
import hysj.codes.latex
 
try:
  code: codes.Code = codes.Code()

  two = code.builtins.lits.two64

  t = code.rvar64()
  x = code.rvar64()
  y = code.rvar64()

  dxdt = code.der(x,t)
  dydt = code.der(y,t)

  X = [t,x,y]
  dFdt = [
    code.der(code.pow(x,two),t),
    code.der(code.add([x,y]),t),
    code.der(code.mul([x,y]),t),
    code.der(code.log(x,y),t)
  ]

  def diff(dfdt):
    dfdt_2 = code.objcast(codes.clone(code,[dfdt.id()])[0])
    g = codes.symdiff(code,dfdt_2)
    if g is None:
      return None
    (_,g_simple), = codes.normform(code,[g.id()],[ x.id() for x in X ])
    if g_simple is None:
      return g.obj
    return code.objcast(g_simple)
  G = list(map(diff,dFdt))


  doc = dominate.document(title='symdiff')
  with doc.head:
    link(rel='stylesheet', href='style.css')
    script(type='text/javascript', src='script.js')
  with doc:
    def latex_symbol_renderer(s,d):
      return {t: 't',x: 'x',y: 'y'}.get(s, '?')
    latex_renderer = codes.latex.Renderer(code,latex_symbol_renderer)  
    def render(o):
      if o is None:
        return '\\emptyset'
      return latex_renderer(o)
    for dfdt,g in zip(dFdt,G):
      dfdt_latex = render(dfdt)
      g_latex = render(g)
      raw(codes.latex.tex_to_svg(tex_content = f'${dfdt_latex} \\quad \\to \\quad {g_latex}$',tex_fontsize = 12) + r'<br>')
  with open('codes_symdiff.html', 'w') as doc_file:
    doc_file.write(str(doc))

except Exception as e:
  import traceback
  traceback.print_exc()
