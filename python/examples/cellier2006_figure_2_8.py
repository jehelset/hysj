import numpy as np
import math
from matplotlib import pyplot as plot

from hysj import *
from hysj.codes import latex

try:

    cautomata = automata.continuous
    cexec     = executions.continuous
    cruns     = constructions.continuous

    code = codes.Code()

    order = 1
    count = 4

    n = code.ncst64(count)
    i = code.itr(n)

    t = code.rvar64()
    x,dxdt = codes.depvar(code, t, order, code.rvar64([i]))
    A = code.rvar64([i])
    F = code.mul([A, x])

    t_begin = 0.0
    t_delta = 1.0
    t_end   = 10.0

    automaton = cautomata.Automaton(
        variables = codes.Vars(independent=t, dependent=[[x,dxdt]]),
        equations=[F],
        roots=[],
        parameters=[A])

    dev = code.dev()

    exec = cexec.fe(
        code,
        automaton,
        cexec.Config(dev, code.rcst64(t_delta), code.rcst64(t_end), 1))

    sample_count = 2**8
    n_sample = code.ncst64(sample_count)
    i_sample = code.itr(n_sample)
    t_sample = code.rvar64([i_sample])
    y_sample = code.rvar64([i, i_sample])
    f_sample = code.pow(code.builtins.lits.e64, code.mul([A, t_sample]))

    sample = code.then([
        code.send(dev, t_sample),
        code.send(dev, A),
        code.on(dev, code.put(y_sample, f_sample)),
        code.recv(dev, y_sample)
    ])

    api = code.api([code.decl(t) for t in [exec.api, sample]])

    code.compile(api.id())

    env = devices.gcc.Env(dev = dev)
    mem = env.alloc(code, api)
    exe = env.load(mem, code, api)

    run = cruns.cc(exec, mem, exe)

    A_mem = np.asarray(mem.r64(A.id()))
    A_mem[:] = [-0.1, -1.0, -2.0, -3.0]


    t_trace = []
    x_trace = []

    t_mem = np.asarray(run.time64())
    x_mem = np.asarray(run.state64(0,0))

    t_mem[:] = 0.0
    x_mem[:] = 1.0

    print('simulating...')
    run.init()
    while True:
        run()
        run.get()
        match run.last_event:
          case cexec.fail:
            raise Exception('simulation failed')
          case cexec.stop:
              break
          case _:
            run.get()
            t_trace.append(float(t_mem[0]))
            x_trace.append(np.copy(x_mem[0]))
    print('done...')

    print('sampling...')

    t_sample_mem = np.asarray(mem.r64(t_sample.id()))
    y_sample_mem = np.asarray(mem.r64(y_sample.id()))

    t_sample_mem[:] = np.linspace(t_begin, t_end, num=sample_count)
    exe.run(sample)

    print('done...')

    print('rendering...')

    plot.title('FIGURE 2.8. Numerical experiment using Forward Euler.')

    figure, ((axis00, axis01), (axis10, axis11)) = plot.subplots(2, 2, sharex=True, layout='tight')

    axis00.set_xlim(0, 10)
    axis00.set_xticks(np.arange(0, 11, 2))

    axis00.set_ylim(0.2,  1.2)
    axis00.set_yticks(np.arange(0.2, 1.4, 0.2))

    axis01.set_ylim(-0.5, 1.5)
    axis01.set_yticks(np.arange(-0.5, 2.0, 0.5))

    axis10.set_ylim(-2, 2)
    axis10.set_yticks(np.arange(-2, 3, 1))

    axis11.set_ylim(-1000, 1500)
    axis11.set_yticks(np.arange(-1000, 1500, 500))

    axes = [axis00, axis01, axis10, axis11]
    for i in range(count):
        axis = axes[i]
        axis.yaxis.set_tick_params(labelsize=8,length=0)
        axis.xaxis.set_tick_params(labelbottom=True, labelsize=8,length=0)
        axis.grid(linestyle='dotted')
        axis.title.set_text(f'a = {A_mem[i]}')
        axis.title.set_fontsize(11)
        axis.plot(t_trace, [ x_trace[t][i] for t in range(len(x_trace)) ], color='black', linestyle='--', lw=0.9, dashes=(5,8))
        axis.plot(t_sample_mem, y_sample_mem[i], color='black', lw=0.9)

    plot.suptitle('FIGURE 2.8. Numerical experiment using Forward Euler.')
    figure.savefig('cellier2006-figure-2.8.png')

    print('done...')

except Exception as e:
    import traceback
    traceback.print_exc()
