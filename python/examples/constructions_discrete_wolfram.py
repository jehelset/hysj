import numpy as np
import subprocess as sp
import matplotlib.pyplot as plot

from hysj import *

dautomata = automata.discrete
dexec     = executions.discrete
druns     = constructions.discrete

width  = 64
height = 32

def model(code, number):

    t = code.ivar32()
    w = code.icst32(width)
    i = code.itr(w)
    D = code.builtins.lits.two32
    Q = code.var(code.typ32(D), [i])

    # NOTE: P = [ Q_{i-1}, Q_i, Q_{i+1} ] - jeh
    P = [
        code.rot(Q, [code.sub(i, code.builtins.lits.one32)]),
        Q,
        code.rot(Q, [code.add([i, code.builtins.lits.one32])])
    ]
    J = range(len(P))

    zero = code.builtins.lits.zero32
    one = code.builtins.lits.one32
    two = code.builtins.lits.two32

    f = [code.sub(zero, Q), code.sub(one, Q)]

    g = [[code.eq(p, v) for v in [zero, one]] for p in P]

    def make_rule():
        match number:
            case 30:
                return (
                    [
                        code.land([g[j][k] for (j, k) in zip(J, K)])
                        for K in [
                            [1, 1, 1],
                            [1, 1, 0],
                            [1, 0, 1],
                            [1, 0, 0],
                            [0, 1, 1],
                            [0, 1, 0],
                            [0, 0, 1],
                            [0, 0, 0],
                        ]
                    ],
                    [f[k] for k in [0, 0, 0, 1, 1, 1, 1, 0]],
                )
            case 90:
                return (
                    [
                        code.land([g[j][k] for (j, k) in zip(J, K)])
                        for K in [
                            [1, 1, 1],
                            [1, 1, 0],
                            [1, 0, 1],
                            [1, 0, 0],
                            [0, 1, 1],
                            [0, 1, 0],
                            [0, 0, 1],
                            [0, 0, 0],
                        ]
                    ],
                    [f[k] for k in [0, 1, 0, 1, 1, 0, 1, 0]],
                )
            case 110:
                g0 = code.lor(
                    [
                        code.land([g[j][k] for (j, k) in zip(J, K)])
                        for K in [[0, 0, 0], [1, 1, 1], [1, 0, 0]]
                    ]
                )
                g1 = code.lnot(g0)
                return ([g0, g1], [f[0], f[1]])
            case 254:
                return ([code.lor([g[j][1] for j in J])], [f[1]])

    G, F = make_rule()
    dQdt = codes.depvar(code, t, 1, Q)
    return dautomata.Automaton(
        variables = codes.Vars(independent = t, dependent = [dQdt]),
        transitions = [dautomata.Transition(variable = dQdt[-1], function = f, guard = g) for f, g in zip(F, G)])

def simulate(code,automaton):

    dev = code.dev()

    exec = dexec.cc(code,automaton,dexec.Config(dev, height))

    code.compile(exec.api.id())

    env = devices.gcc.Env(dev = dev)
    mem = env.alloc(code, exec.api)
    exe = env.load(mem, code, exec.api)

    run = druns.cc(exec, mem, exe)

    t_mem = np.asarray(run.time32())
    q_mem = np.asarray(run.istate32(0,0))

    t_mem[:] = 0
    q_mem[:] = 0
    q_mem[0, (width - 1) // 2] = 1

    states = []

    run.init()

    event = dexec.step
    while len(states) != height and event != dexec.stop:
        ring = run()
        run.get()
        for index in ring:
          event = run.event(index)
          states.append((t_mem[index],np.copy(q_mem[index])))
          if len(states) == height:
              break

    return states

def render(states, name):

    colors = [0x000000, 0xFFFFFF]

    def color(i):
        c = colors[i]
        return np.array([(c >> 16) & 0xFF, (c >> 8) & 0xFF, c & 0xFF], dtype=np.uint8)

    def make_rgb24_frame(i):
        Q = [ s[1] for s in states[0 : i + 1] ]
        F = np.vstack(Q[0 : i + 1] + [np.zeros((height - (i + 1), width), dtype=np.int32)])
        return np.vectorize(color, signature="()->(3)")(F)

    command = [
        "ffmpeg",
        "-y",
        "-hide_banner",
        "-loglevel",
        "error",
        "-f",
        "rawvideo",
        "-vcodec",
        "rawvideo",
        "-vb",
        "20M",
        "-s",
        f"{width}x{height}",
        "-pix_fmt",
        "rgb24",
        "-r",
        f"{height // 3}",
        "-i",
        "-",
        "-an",
        f"constructions_discrete_wolfram_{name}.gif",
    ]
    pipe = sp.Popen(command, stdin=sp.PIPE, stderr=sp.STDOUT)
    for h in range(height):
        rgb24 = make_rgb24_frame(h)
        pipe.stdin.write(rgb24.tobytes())
    pipe.stdin.close()
    pipe.wait()

try:

    print("executing automata from Wolfram2002.")
    for n in [30,90,110,254]:
        code: codes.Code = codes.Code()
        print(f"\n\trule {n}:")
        print(f"\t\tconstructing...")
        automaton = model(code, n)
        print(f"\t\tsimulating...")
        states = simulate(code, automaton)
        print(f"\t\trendering...")
        render(states, f"{n}")
        print(f"\t\tdone.")
    print("\ndone.")

except Exception as e:
    import traceback
    print(traceback.format_exc())
