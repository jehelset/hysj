from copy import deepcopy

import numpy as np
import matplotlib.pyplot as plot

from hysj import *

cautomata = automata.continuous
dautomata = automata.discrete
hautomata = automata.hybrid

cexec = executions.continuous
dexec = executions.discrete
hexec = executions.hybrid

cruns = constructions.continuous
druns = constructions.discrete
hruns = constructions.hybrid

order = 1

t_begin = 0.0
t_end = 3.0
t_count = 200 + 1
t_delta = (t_end - t_begin) / (t_count - 1)

code: codes.Code = codes.Code()

def make_automaton(A_value):

    t = code.rvar64()
    u = code.ivar64()
    r = code.ivar64()
    x = code.rvar64()

    A = [code.rcst64(a) for a in A_value]
    X = codes.depvar(code, t, order, x)

    F = A[0]
    G = code.sub(A[1], X[0])

    hautomaton = hautomata.Automaton(
        discrete_automaton=dautomata.Automaton(
            variables=codes.Vars(
                independent=u,
                dependent=[[r]]
            ),
            transitions=[]
        ),
        continuous_variables=codes.Vars(independent=t, dependent=[X]),
        activities=[hautomata.Activity(equation=F, guard=code.builtins.lits.top64)],
        actions=[
            hautomata.Action(
                variable=X[0], function=A[2], guard=code.eq(r, code.builtins.lits.negone64)
            )
        ],
        roots=[hautomata.Root(variable=r, function=G, guard=code.builtins.lits.top64)])

    return hautomaton

def simulate(A):

    automaton = make_automaton(A)
    dev = code.dev()

    exec = hexec.cc(
        code,
        automaton,
        hexec.Config(
            dev = dev,
            continuous = cexec.Config(dev = dev,
                                      step_size = code.rcst64(t_delta),
                                      stop_time = code.rcst64(t_end),
                                      buffer_capacity = 1),
            discrete = dexec.Config(dev = dev,
                                    buffer_capacity=1)))

    env = devices.gcc.Env(dev=dev)

    mem = env.alloc(code, exec.api)
    exe = env.load(mem, code, exec.api)

    run = hruns.cc(exec, mem, exe, env)

    t_mem = np.asarray(run.continuous_time64())
    q_mem = np.asarray(run.discrete_istate64(0,0))
    x_mem = np.asarray(run.continuous_state64(0,0))
    dx_mem = np.asarray(run.continuous_state64(0,1))

    q_mem[:] = 0
    t_mem[:] = 0.0
    x_mem[:] = 0.0

    g0_mem = np.asarray(mem.i64(exec.actions.container[0].guard.id()))
    e0_mem = np.asarray(mem.b64(exec.actions.container[0].event.id()))
    e_mem = np.asarray(mem.b64(exec.actions.event.id()))

    events = run(control = lambda e:e)

    trajectories = []
    print("simulating...")
    run.init()
    while event := events():
        match event:
            case hexec.step:
                run.get()
                trajectories.append([])
            case cexec.step:
                run.continuous_construction.get()
                trajectories[-1].append((t_mem[0],np.copy(x_mem)))
            case cexec.stop:
                break
            case _:
                pass

    print("done...")
    return exec, trajectories

def illustrate(A, i):

    exec, trajectories = simulate(A=A)

    figure, axis = plot.subplots()

    axis.set_title(f"A = {A}")
    axis.set_ylabel("x")
    axis.set_xlabel("t")

    axis.axhline(y=A[1], color="#17DCD4", label="a1")
    axis.axhline(y=A[2], color="#DC8317", label="a2")

    last = None
    for j, trajectory in enumerate(trajectories):
        if len(trajectory) == 0:
            continue
        x_axis = [t for (t,x) in trajectory]
        y_axis = [x[0] for (t,x) in trajectory]
        if j == 0:
            axis.plot(x_axis, y_axis, c="#dc1717", label="x",linewidth=2)
        else:
            axis.plot(x_axis, y_axis, c="#dc1717")

        if not last is None:
            last_x, last_y = last
            axis.plot(
                [last_x, x_axis[0]],
                [last_y, y_axis[0]],
                c="#dc1717",
                ls="--",
                markevery=[0, 1],
                marker="o",
            )
        last = (x_axis[-1], y_axis[-1])
    axis.legend(loc="upper left")
    figure.tight_layout()
    figure.savefig(f"sawtooth_{i}.svg")

    return exec, trajectories

try:
    illustrate([1.0, 1.0, 0.0], 0)
    illustrate([2.0, 1.0, 0.5], 1)
    illustrate([0.1, 1.0, 0.0], 2)
except Exception as e:
    import traceback

    print(traceback.format_exc())
