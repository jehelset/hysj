from vtkmodules.vtkCommonColor import vtkNamedColors
from vtkmodules.vtkCommonDataModel import (
    vtkCellArray,
    vtkMultiBlockDataSet
)
from vtkmodules.vtkFiltersSources import vtkCubeSource
from vtkmodules.vtkRenderingCore import (
    vtkActor,
    vtkDataSetMapper,
    vtkCompositeDataDisplayAttributes,
    vtkRenderWindow,
    vtkWindowToImageFilter,
    vtkRenderer,
)
from vtkmodules.vtkRenderingOpenGL2 import vtkCompositePolyDataMapper2 as vtkCompositePolyDataMapper
from vtkmodules.vtkIOOggTheora import vtkOggTheoraWriter

import numpy as np
from itertools import product

from hysj import *

dautomata = automata.discrete
dexec     = executions.discrete
druns     = constructions.discrete

width  = 8
height = 8
depth  = 3

order = 1

rank = 4

v = list(range(rank))
v_board,v_conductor,v_tail,v_head = v

def model(code):

    t = code.ivar32()
    w,h,d = list(map(code.icst32,[width,height,depth]))
    i,j,k = list(map(code.itr,[w,h,d]))

    negone,zero,one,two = (code.builtins.lits.negone32,code.builtins.lits.zero32,code.builtins.lits.one32,code.builtins.lits.two32)

    N = 3
    
    q = code.var(code.typ32(code.icst32(rank)), [i, j, k])
    dqdt = codes.depvar(code, t, order, q)
    
    q_state = list(map(code.icst32,v))
    q_board,q_conductor,q_tail,q_head = q_state

    m_state = list(map(lambda q_state:code.sel(code.eq(q,q_state),[zero,one]),q_state))
    m_board,m_conductor,m_tail,m_head = m_state

    k0 = code.itr(code.icst32(min(N, width)))
    k1 = code.itr(code.icst32(min(N, height)))
    k2 = code.itr(code.icst32(min(N, depth)))
    o  = code.icst32(-(N // 2))
    i_adj = code.add([i,o,k0])
    j_adj = code.add([j,o,k1])
    k_adj = code.add([k,o,k2])

    i_mask = code.lor([code.lt(i_adj,zero),
                       code.lt(w,code.add([i_adj,one]))])
    j_mask = code.lor([code.lt(j_adj,zero),
                       code.lt(h,code.add([j_adj,one]))])
    k_mask = code.lor([code.lt(k_adj,zero),
                       code.lt(d,code.add([k_adj,one]))])
    m_head_clip = code.lor([i_mask,j_mask,k_mask])

    m_head_adj = code.rot(m_head,[i_adj,j_adj,k_adj])
    m_head_adj_clipped = code.sel(m_head_clip,[m_head_adj,zero])
    m_head_adj_sum = code.sub(
        code.con(
            code.add([m_head_adj_clipped]),
            [k0,k1,k2]),
        m_head)

    g_board = code.builtins.lits.bot32
    g_conductor = m_tail
    g_tail = m_head
    g_head = code.land([
        m_conductor,
        code.lor([code.eq(m_head_adj_sum,one),
                      code.eq(m_head_adj_sum,two)])])
    g_state = [ g_board, g_conductor, g_tail, g_head ]

    g_transition = list(map(lambda i:code.land([code.lnot(m_state[i]),g_state[i]]),v))
    f_transition = list(map(lambda q_state:code.sub(q_state,q),q_state))

    A_transitions = list(map(lambda i:dautomata.Transition(variable = dqdt[-1],
                                                           function = f_transition[i],
                                                           guard = g_transition[i]),v))

    A = dautomata.Automaton(
        variables = codes.Vars(independent = t,dependent = [dqdt]),
        transitions = A_transitions)

    return A

def logical_or_board(left_on,right_on):
    board = np.zeros((width,height,depth),dtype=np.int32)
        
    board[:] = v_board

    l_in = 3
    for x in range(0,l_in):
        board[x,3,1] = v_conductor
        board[x,5,1] = v_conductor
    for z in range(0,3):
        board[l_in,4,z] = v_conductor
    board[l_in + 1,4,0] = v_conductor
    for x in range(l_in + 2,width):
        board[x,4,1] = v_conductor

    board[0,3,1] = v_head if left_on else v_conductor
    board[0,5,1] = v_head if right_on else v_conductor
    return (board,16)

def logical_or_boards():
    return [ logical_or_board(left_on,right_on) for left_on,right_on in [(True,False),(False,True),(True,True)] ]

def logical_and_board(left_on,right_on):
    board = np.zeros((width,height,depth),dtype=np.int32)
        
    board[:] = v_board

    l_in = 3
    for x in range(0, l_in):
        board[x,3,1] = v_conductor
        board[x,5,1] = v_conductor

    for z in [0,2]:
        board[l_in - 1,5,z] = v_conductor

    for x in range(l_in, l_in + 2):
        for y in [2,4]:
            board[x, y, 1] = v_conductor

    for z in [0,2]:
      board[l_in + 1, 4, z] = v_conductor

    for x in range(l_in + 2, width):
        board[x,3,1] = v_conductor

    board[0,3,1] = v_head if left_on else v_conductor
    board[0,5,1] = v_head if right_on else v_conductor
    return (board,16)

def logical_and_boards():
    return [ logical_and_board(left_on,right_on) for left_on,right_on in [(True,True),(True,False),(False,True)] ]

def logical_xor_board(left_on,right_on):
    board = np.zeros((width,height,depth),dtype=np.int32)
        
    board[:] = v_board

    l_in = 1

    for x in range(0, l_in):
        for y in [3,5]:
            board[x,y,1] = v_conductor

    for x in range(l_in, l_in + 2):
        for y in [2,6]:
            board[x,y,1] = v_conductor

    for y in [3,5]:
        for z in range(0,depth):
            board[l_in + 2,y,z] = v_conductor

    for x in range(l_in + 1, l_in + 4):
        for z in [0,2]:
            board[x,4,z] = v_conductor

    for x in range(l_in + 4, width):
        board[x,4,1] = v_conductor

    board[0,3,1] = v_head if left_on else v_conductor
    board[0,5,1] = v_head if right_on else v_conductor
        
    return (board,16)

def logical_xor_boards():
    return [ logical_xor_board(left_on,right_on) for left_on,right_on in [(True,True),(True,False),(False,True)] ]

def simulate(code, automaton, boards):

    dev = code.dev()
    
    exec = dexec.cc(code,automaton,dexec.Config(dev, 16))

    env = devices.gcc.Env(dev = dev)
    mem = env.alloc(code, exec.api)
    exe = env.load(mem, code, exec.api)

    run = druns.cc(exec, mem, exe)

    t_mem = np.asarray(run.time32())
    q_mem = np.asarray(run.istate32(0,0))

    runs = []

    for (q_init,max_steps) in boards:

        t_mem[:] = 0
        q_mem[:] = q_init[:]

        dqdt_mem = np.asarray(run.istate32(0,1))
        g_mem = list(map(np.asarray,run.transition_guards32()))
        f_mem = list(map(np.asarray,run.transition_ifunctions32()))

        states = []

        run.init()
        event = dexec.step
        while len(states) != max_steps and event != dexec.stop:
            ring = run()
            run.get()
            for index in ring:
                event = run.event(index)
                states.append(np.copy(q_mem[index]))
                if len(states) == max_steps:
                    break
        runs.append(states)

    return runs

def render(filename,states):

    named_colors = vtkNamedColors()
    def make_color(html_color):
        rgb_uc = named_colors.HTMLColorToRGB(html_color)
        return [ rgb_uc.GetRed() / 255.0,
                 rgb_uc.GetGreen() / 255.0,
                 rgb_uc.GetBlue() / 255.0 ]
    colors = list(map(make_color,
                      ('#46425e',
                       '#5b768d',
                       '#d17c7c',
                       '#f6c6a8')))
    cubes = [ [ [ vtkCubeSource() for z in range(depth) ] for y in range(height) ] for x in range(width) ]

    for x,y,z in product(range(width), range(height), range(depth)):
        c = cubes[x][y][z]
        c.SetBounds(x, x + 1, y, y + 1, z, z + 1)
        c.Update()

    multi_block = vtkMultiBlockDataSet()
    multi_block.SetNumberOfBlocks(width * height * depth)

    def sub2idx(x,y,z):
        return x * (height * depth) + y * depth + z
    for x,y,z in product(range(width), range(height), range(depth)):
        multi_block.SetBlock(sub2idx(x,y,z), cubes[x][y][z].GetOutput())

    mapper = vtkCompositePolyDataMapper()
    mapper.SetInputDataObject(multi_block)

    def sub2block(x,y,z):
        return sub2idx(x,y,z) + 1

    attributes = vtkCompositeDataDisplayAttributes()
    mapper.SetCompositeDataDisplayAttributes(attributes)

    actor = vtkActor()
    actor.SetMapper(mapper)
    actor_property = actor.GetProperty()
    
    actor_property.EdgeVisibilityOn()
    actor_property.LightingOn()
    actor_property.SetAmbient(0.55)
    actor_property.SetDiffuse(0.3)

    renderer = vtkRenderer()
    renderer.AddActor(actor)

    renderer.SetBackground(colors[0])

    window = vtkRenderWindow()
    window.AddRenderer(renderer)
    window.SetSize(300, 300)
    window.SetOffScreenRendering(True)

    camera = renderer.GetActiveCamera()

    x_center = width  / 2.0
    y_center = height / 2.0
    z_center = depth  / 2.0

    camera.SetFocalPoint(x_center,y_center,z_center)

    x_distance = - width  * 0.2
    y_distance = - height * 0.8
    z_distance =   depth  * 5.2

    camera.SetPosition(x_center + x_distance,
                       y_center + y_distance,
                       z_center + z_distance)
    camera.SetFocalPoint(x_center,
                         y_center,
                         z_center)

    image = vtkWindowToImageFilter()
    image.SetInput(window)
    image.SetInputBufferTypeToRGB()
    image.ReadFrontBufferOff()
    image.Update()

    movie = vtkOggTheoraWriter() 
    movie.SetInputConnection(image.GetOutputPort())
    movie.SetFileName(filename)
    movie.Start()
    movie.SetRate(2)
    movie.SetQuality(2)

    for x,y,z in product(range(width), range(height), range(depth)):
        mapper.SetBlockVisibility(sub2block(x,y,z), not (states[0][x,y,z] == v_board))

    def set_color_state(state):
        for x,y,z in product(range(width), range(height), range(depth)):
            mapper.SetBlockColor(sub2block(x,y,z), colors[state[x,y,z]])

    for state in states:
        set_color_state(state)
        window.Render()
        image.Modified()
        movie.Write()

    movie.End()

try:

    code: codes.Code = codes.Code()
    automaton = model(code)

    for i,run in enumerate(simulate(code, automaton, logical_or_boards())):
      render(f'wireworld_or_{i}.avi',run)
    for i,run in enumerate(simulate(code, automaton, logical_and_boards())):
      render(f'wireworld_and_{i}.avi',run)
    for i,run in enumerate(simulate(code, automaton, logical_xor_boards())):
      render(f'wireworld_xor_{i}.avi',run)

except Exception as e:
    import traceback
    print(traceback.format_exc())
