import numpy as np
import math
from matplotlib import pyplot as plot
from matplotlib.animation import FuncAnimation as Animation
from matplotlib.animation import writers as animation_writers
from copy import deepcopy

plot.style.use("dark_background")

from hysj import *
from hysj.codes import latex

try:
    cautomata = automata.continuous
    cexec = executions.continuous
    cruns = constructions.continuous

    code: codes.Code = codes.Code()

    order = 1

    c = code.rcst32(-2.3)

    t = code.rvar32()
    x, dxdt = codes.depvar(code, t, order, code.rvar32())

    f = code.mul([c, x])

    t_begin = 0.0
    t_end = 5.0
    dt = 0.01

    automaton = cautomata.Automaton(
        variables=codes.Vars(independent=t, dependent=[[x,dxdt]]),
        equations=[f],
        roots=[],
        parameters=[],
    )

    dev = code.dev()

    exec = cexec.fe(code, automaton, cexec.Config(dev, code.rcst32(dt), code.rcst32(t_end), 1))

    env = devices.vulkan.Env()
    env.devs[0].sym = dev
    mem = env.alloc(code, exec.api)
    exe = env.load(mem, code, exec.api)

    run = cruns.cc(exec, mem, exe)

    t_data = []
    x_data = []

    t_mem = np.asarray(run.time32())
    x_mem = np.asarray(run.state32(0, 0))

    t_mem[:] = 0.0
    x_mem[:] = 1.0

    print("simulating...")
    run.init()
    while True:
        run()
        run.get()
        t_data.append(float(t_mem[0]))
        x_data.append(np.copy(x_mem[0]))
        match run.last_event:
            case cexec.fail:
                raise Exception("simulation failed")
            case cexec.stop:
                break
            case _:
                pass

    print("done...")

    print("animating...")
    frame_step = 1
    frame_count = len(x_data) // frame_step

    figure = plot.figure("sines")
    axes = plot.axes()

    def symbol_labeler(s, d):
        return {tuple(t.id()): "t", tuple(x.id()): "x"}.get(tuple(s.id()), f"?")

    latex_renderer = latex.Renderer(code, symbol_labeler)
    axes.set_title(f"${latex_renderer(dxdt)} = {latex_renderer(f)}$.")

    axes.set_xlabel(f"${latex_renderer(t)}$")
    axes.set_ylabel(f"${latex_renderer(x)}$")

    line, = plot.plot([], [])

    def animate(frame_index):
        first, last = 0, min((frame_index + 1) * frame_step, len(x_data) - 1)
        line.set_data(t_data[first:last], x_data[first:last])
        axes.relim()
        axes.autoscale_view()
        return [line]

    animation = Animation(figure, animate, frames=frame_count, interval=1, blit=True)
    animation.save(
        "constructions_continuous_fe_0.mp4", writer=animation_writers["ffmpeg"](fps=30), dpi=150.0
    )
    print("done...")

except Exception as e:
    import traceback

    traceback.print_exc()
