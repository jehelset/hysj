import numpy as np

from matplotlib import pyplot as plot
from matplotlib.animation import FuncAnimation as Animation
from matplotlib.animation import writers as animation_writers

from hysj import *
from hysj.codes import latex

try:

    code: codes.Code = codes.Code()

    iteration_max = code.ncst32(2**10)
    iteration = code.itr(iteration_max)
    N = code.ncst32(2**6)
    I = range(code.dat(N).cell)
    i = code.itr(N)
    x = code.rvar32([i])
    f = code.add([code.sin(x), code.mul([code.rcst32(-1.2), code.cos(code.neg(x))])])

    fp_func = numerics.fp_func(code, f, x, iteration_max)

    error = code.rms(fp_func.residual)
    test = code.lt(error, code.rcst32(1.0e-3))

    fp = numerics.fp(code, fp_func, test)

    

    x_trace = code.rvar32([i, iteration])
    y_trace = code.rvar32([i, iteration])
    e_trace = code.rvar32([iteration])
    tracepoint = code.when([
        code.put(code.rot(x_trace, [i, fp.status]), code.rot(fp.variable, [i])),
        code.put(code.rot(y_trace, [i, fp.status]), code.rot(fp.last_function, [i])),
        code.put(code.rot(e_trace, [fp.status]), error)
    ])
    
    fixed_point_iteration = code.then([
        fp.init,
        code.loop(
            code.then([
                fp.eval,
                tracepoint,
                fp.test_brk,
                fp.step,
                fp.fail_brk
            ]))
    ])

    sample_count = code.ncst32(256)
    sample_itr = code.itr(sample_count)
    x_sample = code.rvar32([sample_itr])
    y_sample = code.rvar32([sample_itr])
    f_sample = code.expr(codes.clone(code, [f.id()])[0])
    codes.partial_opsub(code, f_sample.id(), x.id(), x_sample.id())
    sample = code.put(y_sample, f_sample)

    host, dev = code.builtins.host, code.dev()

    algo_fixed_point_iteration = code.then([
        code.to(host, dev, x),
        code.on(dev, fixed_point_iteration),
        code.when([ code.to(dev, host, b) for b in (fp.status, x, x_trace, y_trace, e_trace) ])
    ])
    algo_sample = code.then([
        code.to(host, dev, x_sample),
        code.on(dev, sample),
        code.to(dev, host, y_sample)
    ])

    api = code.api([code.decl(a) for a in (algo_fixed_point_iteration, algo_sample)])

    code.compile(api.id())

    env = devices.vulkan.Env()

    next(d for d in env.devs if d.type == devices.vulkan.dgpu).sym = dev
    
    mem = env.alloc(code, api)
    exe = env.load(mem, code, api)

    x_mem, x_trace_mem, y_trace_mem, e_trace_mem, x_sample_mem, y_sample_mem = \
        (np.asarray(mem.r32(b.id())) for b in (x, x_trace, y_trace, e_trace, x_sample, y_sample))
    status_mem = np.asarray(mem.i32(fp.status.id()))

    x_mem[:] = (2.0*np.random.rand(code.dat(N).cell) - 1.0) * np.pi

    print("running fixed point iteration...")
    exe.run(algo_fixed_point_iteration)

    if status_mem[0] == code.dat(iteration_max).cell:
        print("diverged...")

    iteration_stop = status_mem[0] + 1

    print("sampling function...")

    x_max = x_trace_mem.T[:iteration_stop].max()
    x_min = x_trace_mem.T[:iteration_stop].min()
    x_sample_mem[:] = np.linspace(x_min - 0.1, x_max + 0.1, num=code.dat(sample_count).cell)
    exe.run(algo_sample)

    print("rendering...")
    plot.style.use("dark_background")
    plot.xticks(fontsize="small")
    plot.yticks(fontsize="small")

    figure, (f_axes, e_axes) = plot.subplots(2, 1)

    def latex_labeler(s, d):
        return { tuple(x.id()): "x_i" }.get(tuple(s.id()), f"?")
    latex_renderer = latex.Renderer(code, latex_labeler)

    figure.suptitle(f"$f({latex_renderer(x)}) = {latex_renderer(f)}, \\; 0 \\leq i < {latex_renderer(N)}$",
                    fontsize="small")

    f_axes.set_ylabel(f"$f_i$", fontsize="small")
    f_axes.set_xlabel(f"${latex_renderer(x)}$", fontsize="small")
    e_axes.set_ylabel(f"$error$", fontsize="small")
    e_axes.set_xlabel(f"$iteration$", fontsize="small")
    f_axes.ticklabel_format(style="sci",scilimits=(0,0))
    e_axes.ticklabel_format(axis="y", style="sci",scilimits=(0,0))
    for a in [ f_axes, e_axes]:
        a.grid(alpha=0.2)
        a.tick_params(axis="both", labelsize="small")
        a.yaxis.get_offset_text().set_fontsize("small")
        a.xaxis.get_offset_text().set_fontsize("small")
    f_axes.set_xlim([x_sample_mem.min(), x_sample_mem.max()])
    f_axes.set_ylim([y_sample_mem.min() - 0.1, y_sample_mem.max() + 0.1])
    e_axes.set_ylim([0, np.abs(e_trace_mem[:iteration_stop]).max()])
    e_axes.set_xlim([0, iteration_stop])

    head = {r"marker": "o", "markersize": 1.7, "lw": 0.8, "markevery": [-1]}
    tail = {"alpha": 0.4, "lw": 0.6}
    sample = {"alpha": 0.4, "lw": 0.8}
    f_sample_line = f_axes.plot(x_sample_mem[:], y_sample_mem[:], **sample)[0]
    f_line = [ f_axes.plot([], [], **head)[0] for i in I ]
    f_line_tail = [ f_axes.plot([], [], **tail)[0] for i in I ]
    e_line = e_axes.plot([], [], **head)[0]
    e_line_tail = e_axes.plot([], [], **tail)[0]
    head_num = 3
    tail_num = 5

    figure.tight_layout(pad=1.2)
    def render_init():
        pass
    def render(iteration):
        last = iteration + 1
        first_head = max(last - head_num, 0)
        first_tail = max(last - tail_num, 0)
        e_axes.set_xlabel(f"$iteration = {iteration}$", fontsize="small")
        if first_head == last:
            return []
        for i in I:
            f_line[i].set_data(x_trace_mem[i][first_head:last], y_trace_mem[i][first_head:last])
            f_line_tail[i].set_data(x_trace_mem[i][first_tail:last], y_trace_mem[i][first_tail:last])
        e_line.set_data(range(first_tail, last), e_trace_mem[first_tail:last])
        e_line_tail.set_data(range(0, last), e_trace_mem[0:last])
        return f_line + f_line_tail + [e_line, e_line_tail]

    animation = Animation(figure, render, frames=iteration_stop, interval=1, blit=True, init_func=render_init)
    animation.save(
        "fixed_point_iteration.mp4", writer=animation_writers["ffmpeg"](fps=8), dpi=150.0
    )
    print("done...")

except Exception as e:
    import traceback
    traceback.print_exc()
