from itertools import product
import numpy as np
from hysj import *
from hysj.codes import latex

cautomata = automata.continuous
cexec = executions.continuous
cruns = constructions.continuous

t_begin, t_end, t_count = (0.0, 10.0, 2000 + 1)
t_delta = (t_end - t_begin) / (t_count - 1)

dim = 2
order = 1

code: codes.Code = codes.Code()

A_param = [[-1.0, 1.0], [-0.2, -0.7]]

def make_automaton():
    zero = code.builtins.lits.zero64

    tc = code.rvar64()
    td = code.ivar64()

    i = code.itr(code.builtins.lits.two64)
    j = code.itr(code.builtins.lits.two64)
    A = code.rvar64([i, j])

    x = code.rvar64([i])
    X = codes.depvar(code, tc, 1, x)

    F = code.con(code.add([code.mul([A, code.rot(x, [j])])]),[j])
    automaton = cautomata.Automaton(
        variables=codes.Vars(independent=tc, dependent=[X]),
        equations=[F],
        roots=[],
        parameters=[A],
    )
    automaton.system_matrices = A
    automaton.activity_function = F
    return automaton


# NOTE: fixed-step simulation of automaton for t e [0,20.0]
def simulate(automaton):
    dev = code.dev()

    exec = cexec.fe(
        code,
        automaton,
        config = cexec.Config(dev = dev,
                              step_size = code.rcst64(t_delta),
                              stop_time = code.rcst64(t_end),
                              buffer_capacity = 1))

    env = devices.gcc.Env(dev = dev)

    mem = env.alloc(code, exec.api)
    exe = env.load(mem, code, exec.api)

    run = cruns.cc(exec, mem, exe)

    x_mem = np.asarray(run.state64(0,0))
    dx_mem = np.asarray(run.state64(0,1))
    t_mem = np.asarray(run.time64())

    x_mem[:] = np.array([2.0, 4.0], dtype=np.float64) 

    A_mem = np.asarray(run.rparam64(0))
    A_mem[:] = A_param[:]

    trajectory = []
    print("simulating...")
    run.init()
    while True:
        run()
        run.get()
        trajectory.append(np.copy(x_mem[0][:]))
        match run.last_event:
            case cexec.step:
                pass
            case _:
                break
    print("done...")
    return exec, trajectory


def animate(automaton, trajectory):
    import matplotlib.pyplot as plot
    figure, axes = plot.subplots()
    axes.plot([ x[0] for x in trajectory ],[ x[1] for x in trajectory])
    figure.savefig("continuous_linear.svg")

try:
    automaton = make_automaton()
    exec, trajectory = simulate(automaton)
    animate(automaton, trajectory)
except Exception as e:
    import traceback

    print(traceback.format_exc())
