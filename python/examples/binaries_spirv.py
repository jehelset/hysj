import sys

from hysj import codes,binaries

try:

  spirv = binaries.spirv

  code: codes.Code = codes.Code()
  x = code.ivar32()
  t = code.loop(
    code.then([
      code.put(x, code.icst32(2)),
      code.builtins.brk,
      code.put(x, code.icst32(8))
    ]))
  code.compile(t.id())
  env = spirv.env()
  env.optimization_level = 2
  bin = spirv.ccopt(env, code, t)
  print(spirv.dis(bin))
  
except Exception as e:
  import traceback
  traceback.print_exc()

