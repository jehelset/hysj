import numpy as np
import math
from matplotlib import pyplot as plot
from matplotlib.animation import FuncAnimation as Animation
from matplotlib.animation import writers as animation_writers
from copy import deepcopy

plot.style.use("dark_background")

from hysj import *
from hysj.codes import latex

try:
    cautomata = automata.continuous
    cexec     = executions.continuous
    cruns     = constructions.continuous

    code = codes.Code()

    order = 1
    count = 1024

    pi = code.builtins.lits.pi64
    c = code.mul([pi, code.rcst64(2.0)])

    a_space = np.linspace(0.0, 1.0, num=count, endpoint=True)

    n = code.ncst64(count)
    i = code.itr(n)

    A = code.rvar64([i])
    P = code.mul([A, c])
    t = code.rvar64()
    theta = code.sub(t, P)
    S = code.mul([code.rcst64(8*count),code.sin(theta)])
    x,dxdt = codes.depvar(code, t, order, code.rvar64([i]))

    t_begin = 0.0
    t_size  = 2
    t_end   = t_size * math.pi
    t_count = t_size * 1028
    t_delta = (t_end - t_begin) / (t_count - 1)

    t_space = np.linspace(t_begin, t_end, num=t_count)

    automaton = cautomata.Automaton(
        variables = codes.Vars(independent=t, dependent=[[x,dxdt]]),
        equations=[S],
        roots=[],
        parameters=[A])

    dev = code.dev()

    exec = cexec.be(
        code,
        automaton,
        cexec.BeConfig(
            cexec.Config(
                dev,
                code.rcst64(t_delta),
                code.rcst64(t_end),
                1),
            code.rcst64(1.0e-1),
            code.icst64(128),
            code.rcst64(1.0e-3)))

    env = devices.gcc.Env(dev = dev)
    mem = env.alloc(code, exec.api)
    exe = env.load(mem, code, exec.api)

    run = cruns.cc(exec, mem, exe)

    A_mem = np.asarray(mem.r64(A.id()), dtype=np.float64)
    A_mem[:] = a_space

    xdata = []
    ydata = []

    t_mem = np.asarray(run.time64())
    x_mem = np.asarray(run.state64(0,0))

    t_mem[:] = 0.0
    x_mem[:] = 0.0

    print("simulating...")
    run.init()
    while True:
        run()
        match run.last_event:
          case cexec.fail:
            raise Exception("simulation failed")
          case _:
            run.get()
            xdata.append(float(t_mem[0]))
            ydata.append(np.copy(x_mem[0]))
        if run.last_event == cexec.stop:
            break

    print("done...")

    print("animating...")
    frame_step = 5
    frame_count = len(ydata) // frame_step

    figure = plot.figure("sines")
    axes = plot.axes()

    def symbol_labeler(s, d):
        return {
            tuple(t.id()): "t",
            tuple(x.id()): "x",
            tuple(A.id()): "A"
        }.get(tuple(s.id()), f"?")
    latex_renderer = latex.Renderer(code, symbol_labeler)
    axes.set_title(f"${latex_renderer(dxdt)}$ = ${latex_renderer(S)}$, {count} waves.")

    axes.set_xlabel(f"${latex_renderer(t)}$")
    axes.set_ylabel(f"${latex_renderer(x)}$")

    colormap = plot.get_cmap('rainbow', count + 1)
    lines = [plot.plot([], [], color=colormap(i))[0] for i in range(count)]

    def animate(frame_index):
        first, last = 0, min((frame_index + 1) * frame_step, len(ydata) - 1)
        for i in range(count):
            lines[i].set_data(xdata[first:last], [ y[i] for y in ydata[first:last] ])
        axes.relim()
        axes.autoscale_view()
        return lines

    animation = Animation(figure, animate, frames=frame_count, interval=1, blit=True)
    animation.save("constructions_continuous_be_1.mp4", writer=animation_writers["ffmpeg"](fps=30), dpi=150.0)
    print("done...")

except Exception as e:
    import traceback
    traceback.print_exc()
