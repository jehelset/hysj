from hysj import codes,devices,animations

import numpy as np
import subprocess as sp

width = 64*8
height = 64*8
colors = [

  0xff6973,
  0xffb0a3,
  0xffeecc,
  0x00b9be,
  0x15788c,
  0x46425e,

]

length = 64*8

try:

  code: codes.Code = codes.Code()

  N = [ length, height, width ]
  E = [ code.icst32(n) for n in N ]
  I = [ code.itr(e) for e in E ]
  T = [ code.icst32(n - 1) for n in N ]
  J = [ code.sub(t,i) for (t,i) in zip(T,I) ]
  P = [ code.lt(i,code.div(e,code.icst32(2))) for (e,i) in zip(E,I) ]
  A = [ code.sel(p,[j,i]) for (p,i,j) in zip(P,I,J) ]
  B = [ code.sel(p,[i,j]) for (p,i,j) in zip(P,I,J) ]

  c1 = code.icst32(1)
  c2 = code.icst32(2)
  c3 = code.icst32(3)
  c9 = code.icst32(9)
  c14 = code.icst32(14)
  c23 = code.icst32(23)
  cN = code.icst32(len(colors))

  g = [
    code.div(
      code.bxor([code.bnot(code.add([code.div(A[1],c3),c14])),
                     code.add([code.div(B[2],c3),c14])]),
      code.add([c2,code.div(A[0],c9)]))
  ]
  f = [
    code.cvt(
      code.builtins.types.n8,
      code.mod(
        code.add([code.bxor([c23,code.bnot(g[0])]),code.div(g[0],c14)]),
        cN))
  ]
  render = code.nvar8(I)

  color = code.itr(code.ncst32(len(colors) * 3))
  palette = code.nvar8([color])

  frame = code.put(render, f[0])

  mem = code.builtins.host
  dev = code.dev()

  main = code.then([
    code.to(mem, dev, palette),
    code.on(dev, frame),
    code.to(dev, mem, render)
  ])
  api = code.api([code.decl(main)])

  print('compiling...')
  code.compile(api.id())
  env = devices.vulkan.Env()
  env.devs[0].sym = dev
  mem = env.alloc(code, api)
  exe = env.load(mem, code, api)
  
  print('done...')

  print('run...')
  animations.write_rgb32_palette(mem, palette.id(), colors)
  exe.run(main)
  print('done...')

  print('render...')
  animations.write_gif(mem = mem,
                       palette = palette.id(),
                       frames = render.id(),
                       path = "devices_vulkan_0.gif",
                       delay = 1)
  print('done...')

except Exception as e:
  import traceback
  traceback.print_exc()
