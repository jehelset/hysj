import numpy as np

from matplotlib import pyplot as plot
from matplotlib.animation import FuncAnimation as Animation
from matplotlib.animation import writers as animation_writers

from hysj import *
from hysj.codes import latex

try:

    code: codes.Code = codes.Code()

    N = code.ncst64(64)
    I = range(code.dat(N).cell)
    i = code.itr(N)
    x = code.rvar64([i])

    C = [ code.rcst64(-1.2), code.rcst64(2.4)]
    #FIXME: bug - jeh
    # f = code.add([code.sin(x), code.mul([code.rcst32(-1.2), code.cos(code.neg(x))])])
    # (_, f) = codes.normform(code, [f.id()], [x.id()])[0]
    f = code.add([code.mul([c, code.pow(x, code.ncst64(p + 1))]) for p,c in enumerate(C)])
    (_, f) = codes.normform(code, [f.id()], [x.id()])[0]
    f = code.expr(f)

    j = code.itr(N)
    dfdx = codes.symdiff(code, code.der(f, x, [j]))
    (_, dfdx) = codes.normform(code, [codes.clone(code, [dfdx.id()])[0]], [x.id()])[0]
    dfdx = code.expr(dfdx)
    
    newton_step_size = code.rcst64(1.0e-1)
    iteration_max = code.icst64(2**8)
    iteration = code.itr(iteration_max)

    nm_func = numerics.nm_func(code, f, dfdx, x, newton_step_size, iteration_max)

    error = code.rms(nm_func.residual)
    test = code.lt(error, code.rcst64(1.0e-3))

    nm = numerics.nm(code, nm_func, test)

    x_trace = code.rvar64([i, iteration])
    y_trace = code.rvar64([i, iteration])
    e_trace = code.rvar64([iteration])
    tracepoint = code.when([
        code.put(code.rot(x_trace, [i, nm.status]), x),
        code.put(code.rot(y_trace, [i, nm.status]), nm.residual),
        code.put(code.rot(e_trace, [nm.status]), error)
    ])
    
    newtons_method = code.then([
        nm.init,
        code.loop(
            code.then([
                nm.eval,
                tracepoint,
                nm.test_brk,
                nm.step,
                nm.fail_brk
            ]))
    ])

    sample_count = code.ncst64(2**8)
    sample_itr = code.itr(sample_count)
    x_sample = code.rvar64([sample_itr])
    y_sample = code.rvar64([sample_itr])
    f_sample = code.expr(codes.clone(code, [f.id()])[0])
    codes.partial_opsub(code, f_sample.id(), x.id(), x_sample.id())
    sample = code.put(y_sample, f_sample)

    host, dev = code.builtins.host, code.dev()

    algo_newtons_method = code.then([
        code.to(host, dev, x),
        code.on(dev, newtons_method),
        code.when([code.to(dev, host, b) for b in (nm.status, x, x_trace, y_trace, e_trace)])
    ])
    algo_sample = code.then([
        code.to(host, dev, x_sample),
        code.on(dev, sample),
        code.to(dev, host, y_sample)
    ])

    api = code.api([code.decl(a) for a in (algo_newtons_method, algo_sample)])

    env = devices.gcc.Env(dev=dev)

    mem = env.alloc(code, api)
    exe = env.load(mem, code, api)

    x_mem, x_trace_mem, y_trace_mem, e_trace_mem, x_sample_mem, y_sample_mem = \
        (np.asarray(mem.r64(b.id())) for b in (x, x_trace, y_trace, e_trace, x_sample, y_sample))
    status_mem = np.asarray(mem.i32(nm.status.id()))

    print("running newtons method...")
    x_init_mem = (2.0*np.random.rand(code.dat(N).cell) - 1.0) * 1.0
    x_mem[:] = x_init_mem
    exe.run(algo_newtons_method)

    if status_mem[0] == code.dat(iteration_max).cell:
        print("diverged...")

    iteration_stop = status_mem[0] + 1

    print("sampling function...")

    x_max = x_trace_mem.T[:iteration_stop].max()
    x_min = x_trace_mem.T[:iteration_stop].min()
    x_sample_mem[:] = np.linspace(x_min - 0.1, x_max + 0.1, num=code.dat(sample_count).cell)
    exe.run(algo_sample)

    print("rendering...")
    plot.style.use("dark_background")
    plot.xticks(fontsize="small")
    plot.yticks(fontsize="small")

    figure, (f_axes, e_axes) = plot.subplots(2, 1)

    def latex_labeler(s, d):
        return {tuple(x.id()): "x_i"}.get(tuple(s.id()), f"?")
    latex_renderer = latex.Renderer(code, latex_labeler)

    figure.suptitle(f"$f({latex_renderer(x)}) = {latex_renderer(f)}, \\; \\gamma = {latex_renderer(newton_step_size)}, \\; 0 \\leq i < {latex_renderer(N)}$",
                    fontsize="small")

    f_axes.set_ylabel(f"$f(x_i)$", fontsize="small")
    f_axes.set_xlabel(f"$x_i$", fontsize="small")
    e_axes.set_ylabel(f"$rms(f({latex_renderer(x)}))$", fontsize="small")
    e_axes.set_xlabel(f"$iteration$", fontsize="small")
    f_axes.ticklabel_format(style="sci",scilimits=(0,0))
    e_axes.ticklabel_format(axis="y", style="sci",scilimits=(0,0))
    for a in [ f_axes, e_axes]:
        a.grid(alpha=0.2)
        a.tick_params(axis="both", labelsize="small")
        a.yaxis.get_offset_text().set_fontsize("small")
        a.xaxis.get_offset_text().set_fontsize("small")
    f_axes.set_xlim([x_sample_mem.min() - 0.1, x_sample_mem.max() + 0.1])
    f_axes.set_ylim([y_sample_mem.min() - 0.1, y_sample_mem.max() + 0.1])
    e_axes.set_ylim([0, np.abs(e_trace_mem[:iteration_stop]).max()])
    e_axes.set_xlim([0, iteration_stop])

    head = {r"marker": "o", "markersize": 1.5, "lw": 0.8, "markevery": [-1]}
    tail = {"alpha": 0.4, "lw": 0.6}
    sample = {"alpha": 0.4, "lw": 0.8}
    f_sample_line = f_axes.plot(x_sample_mem[:], y_sample_mem[:], **sample)[0]
    f_line = [ f_axes.plot([], [], **head)[0] for i in I ]
    f_line_tail = [ f_axes.plot([], [], **tail)[0] for i in I ]
    e_line = e_axes.plot([], [], **head)[0]
    e_line_tail = e_axes.plot([], [], **tail)[0]
    head_num = 3
    tail_num = 5

    figure.tight_layout(pad=1.2)
    def render_init():
        pass
    def render(iteration):
        last = iteration + 1
        first_head = max(last - head_num, 0)
        first_tail = max(last - tail_num, 0)
        e_axes.set_xlabel(f"$iteration = {iteration}$", fontsize="small")
        if first_head == last:
            return []
        for i in I:
            f_line[i].set_data(x_trace_mem[i][first_head:last], y_trace_mem[i][first_head:last])
            f_line_tail[i].set_data(x_trace_mem[i][first_tail:last], y_trace_mem[i][first_tail:last])
        e_line.set_data(range(first_tail, last), e_trace_mem[first_tail:last])
        e_line_tail.set_data(range(0, last), e_trace_mem[0:last])
        return f_line + f_line_tail + [e_line, e_line_tail]

    animation = Animation(figure, render, frames=iteration_stop, interval=1, blit=True, init_func=render_init)
    animation.save(
        "newtons_method.mp4", writer=animation_writers["ffmpeg"](fps=16), dpi=150.0
    )
    print("done...")

except Exception as e:
    import traceback

    traceback.print_exc()
