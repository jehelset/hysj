import numpy as np
import subprocess as sp

from hysj import *

dautomata = automata.discrete
dexec     = executions.discrete
druns     = constructions.discrete

height = 64*8
width = 64*8
patches = 64
patch_width = width // patches
patch_height = height // patches

colors = [0xFF8274, 0xD53C6A, 0x7C183C, 0x460E2B, 0x31051E, 0x1F0510, 0x130208]
steps = len(colors)
order = 1

def model(code):
    t = code.ivar32()
    h = code.ncst32(height)
    w = code.ncst32(width)
    i = code.itr(h)
    j = code.itr(w)
    D = code.ncst32(len(colors))
    q = code.var(code.typ32(D), [i, j])
    Q = codes.depvar(code, t, order, q)
    f = code.builtins.lits.one32
    g = code.builtins.lits.top32
    A = dautomata.Automaton(
        variables = codes.Vars(independent = t,dependent = [Q]),
        transitions = [dautomata.Transition(variable = Q[-1], function = f, guard = g)])

    A.i = i
    A.j = j
    return A

def simulate(code, automaton):

    dev = code.dev()

    config = dexec.Config(dev = dev, buffer_capacity = 1)
    exec = dexec.cc(code = code, automaton = automaton, config = config)

    automaton.frames  = code.nvar8([code.itr(code.ncst32(steps)), automaton.i, automaton.j])
    automaton.palette = code.nvar8([code.itr(code.ncst32(len(colors) * 3))])
    automaton.render = code.put(
        code.rot(automaton.frames, [code.rot(exec.state.buffer.independent, [exec.buffer.itr]), automaton.i, automaton.j]),
        code.cvt(code.builtins.types.n8, code.rot(exec.state.buffer.dependent[0][0], [ exec.buffer.itr, automaton.i, automaton.j ])))

    put_palette = code.to(code.builtins.host, dev, automaton.palette)
    get_frames = code.to(dev, code.builtins.host, automaton.frames)
    render = code.on(dev, automaton.render)

    api = code.api(list(map(code.decl, (exec.api, put_palette, get_frames,render))))
    
    code.compile(api.id())

    env = devices.gcc.Env(dev = dev)
    mem = env.alloc(code, api)
    exe = env.load(mem, code, api)

    run = druns.cc(exec, mem, exe)

    initial_patches = np.arange(patches**2).reshape(patches, patches) % len(colors)
    initial_dependent = np.repeat(
        np.repeat(initial_patches, width // patches, 1), height // patches, 0)

    t_mem = np.asarray(run.time32())
    q_mem = np.asarray(run.istate32(0,0))

    t_mem[:] = 0
    q_mem[:] = initial_dependent[:]
    F_mem = np.asarray(mem.n8(automaton.frames.id()))

    run.init()
    event = dexec.step
    n_events = 0
    while event != dexec.stop and n_events != steps:
        ring = run()
        for index in ring:
            event = run.event(index)
            exe.run(render)
            n_events += 1
            if event == dexec.stop or n_events == steps:
                break
    exe.run(get_frames)
    return mem, exe, run

def render(automaton, mem):
    animations.write_rgb32_palette(mem,
                                   automaton.palette.id(),
                                   colors)
    animations.write_gif(mem = mem,
                         palette = automaton.palette.id(),
                         frames = automaton.frames.id(),
                         path = "constructions_discrete_patchwork.gif",
                         delay = 8)

try:

    code: codes.Code = codes.Code()
    print("executing a patchwork automaton.")
    print(f"\n\tmodeling...")
    automaton = model(code)
    print(f"\tsimulating...")
    mem, exe, run = simulate(code, automaton)
    print(f"\trendering...")
    render(automaton, mem)
    print(f"\ndone.")

except Exception as e:
    import traceback
    print(traceback.format_exc())
