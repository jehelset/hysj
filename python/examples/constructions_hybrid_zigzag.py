from copy import deepcopy
from matplotlib import pyplot as plot
import numpy as np

from hysj import *
from hysj.codes import latex

try:
    cautomata = automata.continuous
    dautomata = automata.discrete
    hautomata = automata.hybrid

    cexec = executions.continuous
    dexec = executions.discrete
    hexec = executions.hybrid

    cruns = constructions.continuous
    druns = constructions.discrete
    hruns = constructions.hybrid

    code: codes.Code = codes.Code()

    t = code.rvar64()
    u = code.ivar64()

    QZ = [code.icst64(i) for i in range(2)]
    N = code.typ64(code.icst64(len(QZ)))


    q = code.var(N)
    Q = codes.depvar(code, u, 1, q)

    x = code.rvar64()
    X = codes.depvar(code, t, 1, x)

    L_value = [-1.0, 1.0]
    L = [code.rcst64(l) for l in L_value]

    F = [code.neg(l) for l in L]
    
    RQ = [code.ivar64() for f in F]
    rz = code.icst64(-1)

    RF = [code.sub(L[1], X[0]), code.sub(X[0], L[0])]

    TGZ = [code.eq(q, z) for z in QZ]
    TGR = [code.eq(rq, rz) for rq in RQ]

    TG = [code.land([rgz, rgs]) for (rgz, rgs) in zip(TGZ, TGR)]

    t_begin = 0.0
    t_end = 10.0
    t_count = 100 + 1
    t_delta = (t_end - t_begin) / (t_count - 1)

    cvariables = codes.Vars(independent=t, dependent=[X])
    dautomaton = dautomata.Automaton(
        variables=codes.Vars(
            independent=u,
            dependent=[Q]
            + [
                codes.depvar(code, u, 0, rq) for rq in RQ
            ],
        ),
        transitions=[
            dautomata.Transition(variable=Q[-1], function=code.builtins.lits.one64, guard=tg) for tg in TG
        ],
    )
    hautomaton = hautomata.Automaton(
        discrete_automaton=dautomaton,
        continuous_variables=cvariables,
        activities=[
            hautomata.Activity(equation=F[i], guard=TGZ[i]) for i in range(len(QZ))
        ],
        actions=[],
        roots=[
            hautomata.Root(variable=rq, function=rf, guard=code.builtins.lits.top64) for rq, rf in zip(RQ, RF)
        ])

    dev = code.dev()

    exec = hexec.cc(code, hautomaton, 
                    hexec.Config(
                        dev = dev,
                        continuous = cexec.Config(dev = dev,
                                                  step_size = code.rcst64(t_delta),
                                                  stop_time = code.rcst64(t_end),
                                                  buffer_capacity = 1),
                        discrete = dexec.Config(dev = dev,
                                                buffer_capacity=1)))

    env = devices.vulkan.Env()
    env.devs[0].sym = dev

    mem = env.alloc(code, exec.api)
    exe = env.load(mem, code, exec.api)

    run = hruns.cc(exec, mem, exe, env)

    q_mem = np.asarray(run.discrete_istate64(0,0))
    t_mem = np.asarray(run.continuous_time64())
    x_mem = np.asarray(run.continuous_state64(0,0))
    dxdt_mem = np.asarray(run.continuous_state64(0,1))
    x_mem[:] = L_value[0]

    hevents = run(lambda e: e)

    trajectories = {}
    current_trajectory = None
    events = []

    run.init()
    while xevent := hevents():
        events.append(xevent)
        match xevent:
            case hexec.step:
                run.get()
                current_trajectory = trajectories.setdefault(tuple(q_mem),[])
                current_trajectory.append([])
            case cexec.step | cexec.root | cexec.stop:
                run.continuous_construction.get()
                current_trajectory[-1].append((t_mem[0],np.copy(x_mem),np.copy(dxdt_mem)))
                if xevent == cexec.stop:
                    break
                pass
        if len(events) > 100:
            break

    latex_renderer = latex.Renderer(code, lambda s, d: {tuple(x.id()): "x", tuple(t.id()): "t"}.get(tuple(s.id())))
    latex_X = [f"{latex_renderer(x)}" for x in X]
    latex_F = [f"{latex_renderer(f)}" for f in F]
    latex_G = [f"{latex_renderer(tgz)}" for tgz in TGZ]

    figure, axis = plot.subplots(nrows=2, sharex=True)
    location_colors = plot.cm.get_cmap("hsv", len(QZ) + 1)

    for i in range(2):
        axis[i].set_ylabel(f"${latex_X[i]}$", fontsize=20)
    axis[-1].set_xlabel(f"${latex_renderer(t)}$", fontsize=20)
    
    for (q,q_trajectories) in trajectories.items():
        for trajectory in q_trajectories:
          x_axis = [t for (t,x,dxdt) in trajectory ]
          for i in range(2):
              y_axis = [s[1 + i][0] for s in trajectory]
              axis[i].plot(x_axis, y_axis, c=location_colors(q[0]))
              axis[i].text(
                  x_axis[len(x_axis) // 2], 0.0, f"$l_{q[0]}$", backgroundcolor="1.0")

    figure.tight_layout()
    figure.savefig(f"zigzag.svg")

except Exception as e:
    import traceback

    print(traceback.format_exc())
