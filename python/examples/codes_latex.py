from hysj.codes import latex 
import cairo

try:
  tex = r'\begin{equation*}\begin{split}b &= \frac{a}{b} + c\\x  + \textcolor{blue}{y} &= z\end{split}\end{equation*}'
  surface_width = 700
  surface_height = 700
  with cairo.SVGSurface("codes_latex.svg", surface_width, surface_height) as surface:
    context = cairo.Context(surface)
    context.save()
    context.set_source_rgb(1,0,0)
    context.paint()
    context.restore()
    renderer = latex.cairo_tex_renderer(tex_content = tex)
    renderer.set_dpi(96.0)
    renderer(context = context,width = surface_width,height = surface_height)
except Exception as e:
  import traceback
  traceback.print_exc()

