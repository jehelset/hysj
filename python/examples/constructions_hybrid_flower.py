from itertools import product
import numpy as np
from hysj import *
from hysj.codes import latex

cautomata = automata.continuous
dautomata = automata.discrete
hautomata = automata.hybrid

cexec = executions.continuous
dexec = executions.discrete
hexec = executions.hybrid

cruns = constructions.continuous
druns = constructions.discrete
hruns = constructions.hybrid

t_begin, t_end, t_count = (0.0, 20.0, 2000 + 1)
t_delta = (t_end - t_begin) / (t_count - 1)

regions = range(2)
dim = 2
order = 1

code: codes.Code = codes.Code()

alpha = 5.0
omega = 1.0
epsilon = 0.1

def make_automaton():
    zero = code.builtins.lits.zero64

    D = code.icst64(len(regions))

    tc = code.rvar64()
    td = code.ivar64()

    i = code.itr(code.builtins.lits.two64)
    j = code.itr(code.builtins.lits.two64)
    k = code.itr(code.builtins.lits.two64)

    A = code.rvar64([i, j, k])

    r = code.ivar64()
    q = code.var(code.typ64(code.icst64(2)))
    
    Z = [code.icst64(i) for i in regions]
    W = [code.eq(q, Z[i]) for i in regions]

    x = code.rvar64([j])
    X = codes.depvar(code, tc, 1, x)

    A_q = code.rot(A, [q, j, k])
    F_q = code.con(code.add([code.mul([A_q, code.rot(x, [k])])]),[k])

    residual = F_q

    x_0 = code.rot(x, [code.builtins.lits.zero64])
    x_1 = code.rot(x, [code.builtins.lits.one64])

    root_function = code.sub(code.pow(x_1,code.builtins.lits.two64),
                             code.pow(x_0,code.builtins.lits.two64))

    dautomaton = dautomata.Automaton(
        variables=codes.Vars(
            independent=td,
            dependent=[[q], [r]],
        ),
        transitions=[
            dautomata.Transition(
                variable=q,
                function=Z[0],
                guard=code.land([W[1], code.lt(r, code.builtins.lits.zero64)])
            ),
            dautomata.Transition(
                variable=q,
                function=Z[1],
                guard=code.land([W[0], code.lnot(code.lt(r, code.builtins.lits.zero64))])
            )
        ])
    hautomaton = hautomata.Automaton(
        discrete_automaton=dautomaton,
        continuous_variables=codes.Vars(independent=tc, dependent=[X]),
        continuous_parameters=[A,q],
        activities=[
            hautomata.Activity(equation=residual, guard=code.builtins.lits.top64)
        ],
        actions=[],
        roots=[
            hautomata.Root(variable=r,
                           function=root_function,
                           guard=code.builtins.lits.top64)
        ],
    )

    hautomaton.system_matrices = A
    hautomaton.activity_function = F_q.id()
    hautomaton.rays = [x_0.id(), code.neg(x_0).id()]

    return hautomaton


# NOTE: fixed-step simulation of automaton for t e [0,20.0]
def simulate(automaton):
    dev = code.dev()

    exec = hexec.cc(
        code,
        automaton,
        hexec.Config(
            dev = dev,
            continuous = cexec.Config(dev = dev,
                                      step_size = code.rcst64(t_delta),
                                      stop_time = code.rcst64(t_end),
                                      buffer_capacity = 1),
            discrete = dexec.Config(dev = dev,
                                    buffer_capacity=1)))

    code.compile(exec.api.id())

    env = devices.gcc.Env(dev = dev)
    mem = env.alloc(code, exec.api)
    exe = env.load(mem, code, exec.api)

    run = hruns.cc(exec, mem, exe, env)

    q_mem = np.asarray(run.discrete_istate64(0,0))
    r_mem = np.asarray(run.discrete_istate64(1,0))
    x_mem = np.asarray(run.continuous_state64(0,0))
    dx_mem = np.asarray(run.continuous_state64(0,1))
    t_mem = np.asarray(run.continuous_time64())

    q_mem[:] = 0
    r_mem[:] = 0
    x_mem[:] = np.array([5.0, 0.0], dtype=np.float64) 

    A_mem = np.asarray(run.continuous_rparam64(0))

    A_mem[:] = np.array([[[-epsilon, alpha * omega], [-omega, epsilon         ]],
                         [[-epsilon, omega        ], [-alpha * omega, -epsilon]]])

    events = run(control=lambda xe:xe)

    trajectories = []
    print("simulating...")
    run.init()
    n_events = 0
    while xevent := events():
        match xevent:
            case hexec.step:
                run.get()
                if not trajectories or trajectories[-1][0] != tuple(q_mem[:]):
                    trajectories.append((tuple(q_mem[:]), []))
            case cexec.step | cexec.root:
                run.continuous_construction.get()
                trajectories[-1][-1].append(np.copy(x_mem[0][:]))
            case cexec.fail | hexec.stop | cexec.stop:
                break
            case _:
                pass
        pass

    print("done...")
    return exec, trajectories


def animate(automaton, trajectories):

    import matplotlib.pyplot as plot
    from matplotlib import lines
    from matplotlib.animation import FuncAnimation as Animation
    from matplotlib.animation import writers as animation_writers

    print("animating...")

    def symbol_labeler(s, d):
        return {
            tuple(automaton.continuous_variables.independent.id()): "t"
        }.get(tuple(s.id()), f"x")

    latex_renderer = latex.Renderer(code, symbol_labeler)
    latex_continuous_variables = [
        list(map(latex_renderer, X))
        for X in automaton.continuous_variables.dependent
    ]

    figure = plot.figure()
    axes = plot.axes()

    legend_lines = []
    legend_labels = []

    limit = [
        max([abs(s[i]) for t in trajectories for s in t[1]] + [1.0])
        for i in range(dim)
    ]
    margin = 0.1

    axes.set_xlim((-limit[0] - margin, limit[0] + margin))
    axes.set_ylim((-limit[1] - margin, limit[1] + margin))

    axes.grid(True)
    axes.set_xlabel(f"${latex_continuous_variables[0][0]}_0$")
    axes.set_ylabel(f"${latex_continuous_variables[0][0]}_1$")

    def colors(i):
        return [
            "#377eb8",
            "#ee6e00",
            "#4daf4a",
            "#f781bf",
            "#a65628",
            "#984ea3",
            "#999999",
            "#e41a1c",
            "#dede00",
        ][i]

    def legend_lines():
        R = [
            lines.Line2D([0], [0],
                         color=colors(len(regions) + r),
                         linestyle="--") for r, _ in enumerate(automaton.rays)
        ]
        L = [lines.Line2D([0], [0], color=colors(i)) for i in regions]
        return R + L

    def legend_labels():
        R = [f"$ray_{r}$" for r, _ in enumerate(automaton.rays)]
        L = [f"$state_{i}$" for i in regions]
        return R + L

    axes.legend(
        legend_lines(),
        legend_labels(),
        bbox_to_anchor=(0, 1.02, 1, 0.2),
        loc="lower center",
        borderaxespad=0,
        ncol=len(automaton.rays) + len(regions),
    )

    grid = [np.linspace(-limit[i], limit[i], num=10) for i in range(dim)]

    A = automaton.system_matrices
    x = automaton.continuous_variables.dependent[0][0]
    q = automaton.discrete_automaton.variables.dependent[0][0]
    F = code.obs(automaton.activity_function)

    dev = code.dev()

    R = [ code.obs(m) for m in automaton.rays ]
    I = [ A, x, q, ]

    F_main = code.then([
        code.to(code.builtins.host, dev, x),
        code.on(dev, F[0]),
        code.to(dev, code.builtins.host, F[1])
    ])
    R_main = [ code.then([
        code.to(code.builtins.host, dev, x),
        code.on(dev, r),
        code.to(dev, code.builtins.host, b)
    ]) for (r,b) in R ]
    I_main = [ code.to(code.builtins.host, dev, i) for i in I ] 
    
    api = code.api(list(map(code.decl, [F_main] + R_main + I_main)))
        
    code.compile(api.id())

    env = devices.gcc.Env(dev=dev)
    mem = env.alloc(code, api)
    exe = env.load(mem, code, api)

    q_mem = np.asarray(mem.i64(q.id()))
    x_mem = np.asarray(mem.r64(x.id()))
    A_mem = np.asarray(mem.r64(A.id()))
    A_mem[:] = np.array([[[-epsilon, alpha * omega], [-omega, epsilon]],
                         [[-epsilon, omega], [-alpha * omega, -epsilon]]])

    exe.run(I_main[0])

    ray_mem = [ np.asarray(mem.r64(r.id())) for _,r in R ]
    F_mem = np.asarray(mem.r64(F[1].id())) 

    # NOTE: compute the rays - jeh
    for i, ((_, output), main) in enumerate(zip(R, R_main)):
        xdata = []
        ydata = []

        for x_val in grid[0]:
            x_mem[:] = np.array([x_val, 0.0])
            exe.run(main)
            xdata.append(x_val)
            ydata.append(ray_mem[i][0])
        plot.plot(xdata, ydata, color=colors(len(regions) + i), linestyle="--")


    # NOTE: compute quiver of phase plane - jeh
    def make_quiver():
        quiver_grid = np.meshgrid(*grid)
        quiver_data = []

        for i in regions:
            quiver_data.append(([], []))
            q_mem[0] = i
            exe.run(I_main[2])
            for a in range(len(quiver_grid[0])):
                quiver_data[-1][0].append([])
                quiver_data[-1][1].append([])
                for b in range(len(quiver_grid[0][a])):
                    x_mem[:] = np.array([quiver_grid[k][a][b] for k in range(dim)])
                    exe.run(F_main)
                    for k in range(dim):
                        quiver_data[-1][k][-1].append(F_mem[k])

        quiver = plot.quiver(
            quiver_grid[0],
            quiver_grid[1],
            quiver_data[0][0],
            quiver_data[0][1],
            pivot="mid",
            color=colors(0),
            width=0.003,
        )
        return quiver_data, quiver

    quiver_data, quiver = make_quiver()

    # NOTE: set up animation - jeh
    state_coords = sum([[(i, j) for j in range(len(trajectories[i][1]))]
                        for i in range(len(trajectories))], [])
    frame_step = 10
    frame_count = len(state_coords) // frame_step

    def render(frame_index):
        coords = {}
        for i in range(
                max(frame_step * (frame_index - 1), 0),
                min(frame_step * frame_index + 1, len(state_coords)),
        ):
            j, k = state_coords[i]
            coords.setdefault(trajectories[j][0], []).append(
                (k, trajectories[j][-1][k]))

        if not coords:
            return []

        artists = []

        current_location = max(coords.keys())

        quiver.set_color(colors(current_location[0] % 2))
        quiver.set_UVC(quiver_data[current_location[0] % 2][0],
                       quiver_data[current_location[0] % 2][1])

        for l, S in coords.items():
            color = colors(l[0])

            xdata = [s[0] for k, s in S]
            ydata = [s[1] for k, s in S]
            if S[0][0] == 0:
                artists.append(axes.plot(xdata[0], ydata[0], marker=".", color=color)[0])
            artists.append(axes.plot(xdata, ydata, color=color)[0])
        return artists

    Animation(figure, render, frames=frame_count, interval=1,blit=True)\
      .save("flower_system.mp4",
            writer=animation_writers["ffmpeg"](fps=30),
            dpi=100.0)
    print("done...")

try:
    automaton = make_automaton()
    exec, trajectories = simulate(automaton)
    animate(automaton, trajectories)
except Exception as e:
    import traceback

    print(traceback.format_exc())
