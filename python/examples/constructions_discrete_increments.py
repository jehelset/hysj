import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plot

from hysj import *

def colors(k):
    return [
        "#377eb8",
        "#ee6e00",
        "#4daf4a",
        "#f781bf",
        "#a65628",
        "#984ea3",
        "#999999",
        "#e41a1c",
        "#dede00",
    ][k]

try:
    dautomata = automata.discrete
    dexec     = executions.discrete
    druns     = constructions.discrete

    code: codes.Code = codes.Code()

    t = code.ivar64()

    D = code.icst64(3)
    q = code.var(code.typ64(D))

    Q = codes.depvar(code,t,1,q)

    automaton = dautomata.Automaton(
        variables = codes.Vars(
            independent=t,
            dependent=[Q]
        ),
        transitions=[
            dautomata.Transition(variable=Q[1], function=code.builtins.lits.one64, guard=code.builtins.lits.top64)
        ])

    dev = code.dev()
    config = dexec.Config(dev = dev, buffer_capacity = 1)
    exec = dexec.cc(code = code,automaton = automaton, config = config)

    env = devices.gcc.Env(dev = dev)
    mem = env.alloc(code, exec.api)
    exe = env.load(mem, code, exec.api)
    run = druns.cc(exec, mem, exe)

    t_mem = np.asarray(run.time64())
    q_mem = np.asarray(run.istate64(0,0))

    steps = 10

    xdata = []
    ydata = []
    
    states = []
    run.init()
    event = dexec.step
    while event != dexec.stop and len(xdata) != steps:
        ring = run()
        run.get()
        for index in ring:
            event = run.event(index)
            xdata.append(t_mem[0])
            ydata.append(q_mem[0])
            if event == dexec.stop or len(xdata) == steps:
                break

    figure = plot.figure()
    axes = plot.axes()
    axes.grid(True)
    axes.set_xlabel(f"$t^D$")
    axes.set_xticks(np.arange(0, len(xdata), 1))
    axes.set_ylabel(f"$q_0$")
    axes.set_yticks(np.arange(0, max(ydata), 1))

    axes.fill_between(xdata, ydata, step="post", alpha=0.5, color=colors(0))
    axes.step(xdata, ydata, color=colors(0), where="post")
    figure.savefig("constructions_discrete_increments.svg")

except Exception as e:
    import traceback
    print(traceback.format_exc())

