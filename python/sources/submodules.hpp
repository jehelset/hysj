#pragma once
#include"bindings.hpp"

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE{

namespace animations{
  void export_submodule(python::module &);
}
namespace graphs{
  void export_submodule(python::module &);
}
namespace grammars{
  void export_submodule(python::module &);
}

namespace codes{
  void export_sets(python::module &);
  void export_constants(python::module &);
  void export_grammar(python::module &);
  void export_shapes(python::module &);
  void export_types(python::module &);
  namespace builtins{
    void export_submodule(python::module &);
  }
  namespace statics{
    void export_submodule(python::module &);
  }
  void export_code(python::module &);
  void export_cells(python::module &);
  namespace latex{ void export_submodule(python::module &); }
  void export_compile(python::module &);
  void export_addfolds(python::module &);
  void export_merges(python::module &);
  void export_lexpands(python::module &);
  void export_constfolds(python::module &);
  void export_copy(python::module &);
  void export_dists(python::module &);
  void export_argfolds(python::module &);
  void export_mulfolds(python::module &);
  void export_argsorts(python::module &);
  void export_lowerings(python::module &);
  void export_normforms(python::module &);
  void export_opsubs(python::module &);
  void export_substitute(python::module &);
  void export_symdiffs(python::module &);
  void export_submodule(python::module &);
  void export_vars(python::module &);
  void export_access(python::module &);
  void export_threads(python::module &);
}
namespace automata{ void export_submodule(python::module &); }
namespace executions{ void export_submodule(python::module &); }
namespace binaries{ void export_submodule(python::module &); }
namespace devices{
  namespace vulkan{
    void export_submodule(python::module &);
  }
  namespace gcc{
    void export_submodule(python::module &);
  }
  void export_submodule(python::module &);
}
namespace numerics{
  void export_submodule(python::module &);
}
namespace constructions{
  namespace discrete{ void export_submodule(python::module &); }
  namespace continuous{ void export_submodule(python::module &); }
  namespace hybrid{ void export_submodule(python::module &); }
  void export_submodule(python::module &);
}

} //hysj
#include<hysj/tools/epilogue.hpp>
