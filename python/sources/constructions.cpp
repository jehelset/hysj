#include<bindings.hpp>
#include<submodules.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::constructions{

void export_submodule(python::module &m_parent){
  auto m = m_parent.def_submodule("constructions");
  continuous::export_submodule(m);
  discrete::export_submodule(m);
  hybrid::export_submodule(m);
}

}//hysj::constructions
#include<hysj/tools/epilogue.hpp>
