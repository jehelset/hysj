#include<hysj/grammars.hpp>
#include<hysj/tools/reflection.hpp>

#include<bindings.hpp>
#include<submodules.hpp>
#include<tools/graphs.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::grammars{

void export_submodule(python::module &m_hysj){

  auto m = m_hysj.def_submodule("grammars");
  python::export_graph_type<idgraph>(m,"Id");

}

}
#include<hysj/tools/epilogue.hpp>
