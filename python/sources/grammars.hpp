#pragma once

#include<fmt/format.h>

#include<hysj/grammars.hpp>
#include<hysj/tools/reflection.hpp>

#include<bindings.hpp>
#include<tools/trees.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::python{

template<typename objtag,objtag O,std::size_t I>
using py_syntax_fixobjarg = grammars::objarg<O, I>;

template<typename objtag,objtag O,std::size_t I>
using py_syntax_objarg = std::conditional_t<
  grammars::objvararg<O, I>,
  std::vector<grammars::objarg<O,I>>,
  grammars::objarg<O, I>>;

template<typename objtag, typename reltag, typename famtag>
auto export_syntax(auto m,auto ... p){

  static_assert(grammars::concepts::objtag<objtag>
                && grammars::concepts::reltag<reltag>
                && grammars::concepts::famtag<famtag>);

  using syntax = grammars::syntax<objtag, reltag>;

  auto t_syntax = make_class<syntax,false>(m, p...);

  auto export_sym = [&]<auto T>(constant_t<T> tag){
    auto name = enumerator_name(T);

    auto t_sym =
      python::make_class<sym<T>>(
        m,python::with_name{fmt::format("{}",name)})
      ;
    t_sym.def("id",[](sym<T> sym){ return (id)sym; });
    python::reflected_repr(t_sym);
    python::comparison_operators(t_sym);
    python::hash(t_sym);
    t_sym.attr("tag") = T;

    auto cast_name = fmt::format("{}cast",name);
    t_syntax.def_static(cast_name.c_str(),
                        [=](id id){
                          return grammars::refcast(tag, id);
                        },
                        python::arg("id"));

  };
  each_enumerator<objtag>(export_sym);
  each_enumerator<reltag>(export_sym);

  t_syntax.def_static("objcast",
                      [](id id){
                        return grammars::with_sym(
                          type_c<objtag>,
                          [](auto sym){
                            return python::cast(sym);
                          }, id);
                      });
  t_syntax.def_static("relcast",
                      [](id id){
                        return grammars::with_sym(
                          type_c<reltag>,
                          [](auto sym){
                            return python::cast(sym);
                          },
                          id);
                      });

  auto export_fam = [&]<auto F>(constant_t<F> tag){
    auto name = enumerator_name(F);
    auto t_fam = python::make_class<grammars::fam<F>>(
      m,python::with_name{fmt::format("{}",name)})
    ;
    t_fam.def("id",
              [](const grammars::fam<F> &fam){
                return (id)fam;
              });
    python::reflected_repr(t_fam);
    python::comparison_operators(t_fam);
    python::hash(t_fam);
    t_fam.attr("tag") = F;

    t_syntax.def_static(name.data(),
                        [=](id id){
                          return grammars::refcast(tag,id);
                        },
                        python::arg("id"));

    python::implicitly_convertible<grammars::any_famobj<F>, fam<F>>();
    each(
      grammars::famseq<F>{},
      [&](auto I){
        using T = grammars::famval<grammars::famobj<F,I>>;
        t_syntax.def_static(name.data(),
                            [=](T t){
                              return fam<F>{t};
                            },
                            python::arg("val"));
      });

  };
  each_enumerator<famtag>(export_fam);

  auto export_sym_fns = [&]<auto T>(constant_t<T> tag){
    auto sym_name = enumerator_name(T);

    {
      auto name = fmt::format("{}syms",sym_name);
      t_syntax.def(name.c_str(),
                   [=](const syntax &s){
                     return ranges::vec(syms(s, tag));
                   });
    }
    {
      auto name = fmt::format("{}count",sym_name);
      t_syntax.def(name.c_str(),
                   [=](const syntax &s){
                     return symcount(s, tag);
                   });
    }
    t_syntax.def("dat",
                 [](const syntax &s,sym<T> y){
                   return symdat(s, y);
                 },
                 python::arg("sym"));

    if constexpr(grammars::is_obj<decltype(T)>){
      t_syntax.def("args",
                   [](const syntax &s,sym<T> y){
                     return pytree<grammars::objgram_of<T>.tree>(args(s, y));
                   },
                   python::arg("sym"));
      t_syntax.def("rels",
                   [](const syntax &s,sym<T> y){
                     return pytree<grammars::objgram_of<T>.tree>(rels(s, y));
                   },
                   python::arg("sym"));
      t_syntax.def("argids",
                   [](const syntax &s,id y){
                     return ranges::vec(argids(s, y));
                   },
                   python::arg("sym"));
      t_syntax.def("relids",
                   [](const syntax &s,sym<T> y){
                     return ranges::vec(relids(s, y));
                   },
                   python::arg("sym"));
      t_syntax.def("argobjids",
                   [](const syntax &s,id y){
                     return ranges::vec(argobjids(s, y));
                   },
                   python::arg("sym"));
      t_syntax.def("argrelids",
                   [](const syntax &s,sym<T> y){
                     return ranges::vec(argrelids(s, y));
                   },
                   python::arg("sym"));
    }
    else{
      t_syntax.def("relobjid",
                   [](const syntax &s,id y){
                     return relobjid(s, y);
                   });
      t_syntax.def("relargid",
                   [](const syntax &s,id y){
                     return relargid(s, y);
                   });
    }

  };
  each_enumerator<objtag>(export_sym_fns);
  each_enumerator<reltag>(export_sym_fns);

  auto export_obj_facts_inner = [&]<auto T,std::size_t... I,std::size_t... I_empty>(
    constant_t<T>,
    std::index_sequence<I...>,
    std::index_sequence<I_empty...>){

    constexpr bool objvar = grammars::objvar<T>;
    constexpr bool empty_symdat = grammars::empty_symdat<T>;

    using symdat = grammars::symdat_t<T>;
    auto sym_name = enumerator_name(T);

    std::array<std::string,sizeof...(I)> arg_names{
      fmt::format("arg{}", I)...
    };

    auto fixarg = [&]<std::size_t J>(std::integral_constant<std::size_t, J>){
      return python::arg(arg_names[J].c_str());
    };
    auto arg = [&]<std::size_t J>(std::integral_constant<std::size_t, J>){
      if constexpr(grammars::objvararg<T,J>)
        return (python::arg(arg_names[J].c_str()) = py_syntax_objarg<objtag, T, J>{});
      else
        return python::arg(arg_names[J].c_str());
    };

    if constexpr((grammars::empty_symdat<grammars::objreltag<T, I>> && ...)){

      if constexpr(empty_symdat){
        t_syntax.def(
          sym_name.data(),
          [=](syntax &s,py_syntax_objarg<objtag, T, I>... a){
            return grammars::objadd<objtag, reltag, T>(s, a...);
          },
          arg(std::integral_constant<std::size_t,I>{})...);
        if constexpr(objvar && (grammars::objnum<T> > 1)){ //FIXME: mismatch - jeh
          t_syntax.def(
            sym_name.data(),
            [=](syntax &s,py_syntax_fixobjarg<objtag, T, I>... a){
              return grammars::objadd<objtag, reltag, T>(s, a...);
            },
            fixarg(std::integral_constant<std::size_t,I>{})...);
          t_syntax.def(
            sym_name.data(),
            [=](syntax &s,py_syntax_objarg<objtag, T, I_empty>... a){
              return grammars::objadd<objtag, reltag, T>(s, a...);
            },
            fixarg(std::integral_constant<std::size_t,I_empty>{})...);
        }
      }
      else{
        t_syntax.def(
          sym_name.data(),
          [=](syntax &s,symdat d,py_syntax_objarg<objtag, T, I>... a){
            return grammars::objadd<objtag, reltag, T>(s, std::move(d), a...);
          },
          python::arg("dat"),
          arg(std::integral_constant<std::size_t,I>{})...);

        if constexpr(objvar && (grammars::objnum<T> > 1)){ //FIXME: mismatch - jeh
          t_syntax.def(
            sym_name.data(),
            [=](syntax &s,symdat d,py_syntax_fixobjarg<objtag, T, I>... a){
              return grammars::objadd<objtag, reltag, T>(s, std::move(d), a...);
            },
            fixarg(std::integral_constant<std::size_t,I>{})...);
          t_syntax.def(
            sym_name.data(),
            [=](syntax &s,symdat d,py_syntax_fixobjarg<objtag, T, I_empty>... a){
              return grammars::objadd<objtag, reltag, T>(s, std::move(d), a...);
            },
            fixarg(std::integral_constant<std::size_t,I_empty>{})...);
        }

      }
    }
    else{
      //FIXME: implement - jeh
    }
  };
  auto export_obj_facts = [&]<auto T>(constant_t<T> tag){
    return export_obj_facts_inner(tag,grammars::objseq<T>{},
                                  grammars::objseq_empty<T>{});
  };
  each_enumerator<objtag>(export_obj_facts);

  return t_syntax;
}



}
#include<hysj/tools/epilogue.hpp>
