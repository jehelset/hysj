from hysj._hysj._animations import *

def write_rgb32_palette(mem, palette, colors):
    palette_mem = mem.n8(palette)
    for i,c in enumerate(colors):
        palette_mem[3*i + 2] = (c >>  0) & 0xFF
        palette_mem[3*i + 1] = (c >>  8) & 0xFF
        palette_mem[3*i + 0] = (c >> 16) & 0xFF
