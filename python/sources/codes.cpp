#include<utility>
#include<vector>

#include<hysj/codes.hpp>
#include<hysj/grammars.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/reflection.hpp>
#include<hysj/tools/type_traits.hpp>
#include<hysj/tools/types.hpp>

#include<bindings.hpp>
#include<grammars.hpp>
#include<submodules.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

void export_submodule(python::module &m_hysj){

  auto m = m_hysj.def_submodule("_codes");

  export_sets(m);
  export_constants(m);
  export_grammar(m);
  builtins::export_submodule(m);
  statics::export_submodule(m);
  export_code(m);
  export_cells(m);
  latex::export_submodule(m);
  {
    export_addfolds(m);
    export_merges(m);
    export_lexpands(m);
    export_constfolds(m);
    export_copy(m);
    export_dists(m);
    export_argfolds(m);
    export_mulfolds(m);
    export_argsorts(m);
    export_lowerings(m);
    export_normforms(m);
    export_substitute(m);
    export_opsubs(m);
    export_symdiffs(m);
  }
  export_vars(m);
  export_access(m);
  export_threads(m);
  export_compile(m);
}

}//hysj::codes
#include<hysj/tools/epilogue.hpp>
