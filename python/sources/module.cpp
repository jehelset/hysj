#include"submodules.hpp"
#include<hysj/tools/types.hpp>
#include<hysj/tools/coroutines/trees.hpp>

#include<hysj/tools/prologue.hpp>
HYSJ_PYTHON_MODULE(_hysj,m){
  using namespace hysj;
  m.doc() = "hysj - modelling and simulating hybrid automata";

  {
    m.attr("__version__") =
      fmt::format("{}.{}.{}",version.major,version.minor,version.patch);
    m.attr("cpp_namespace") = "hysj::" HYSJ_STRINGIFY(_HYSJ_VERSION_NAMESPACE);
  }
  {
    auto t_none =
      python::make_class<none>(m,python::with_name{"Nonesuch"})
        .def("__str__",[](const none &){ return "none"; })
        .def("__repr__",[](const none &){ return "none"; })
      ;
    python::comparison_operators(t_none);
  }
  auto export_coroutine_tree =
    [&]<typename T>(type_t<T>,auto name){
      python::make_class_decl<co_api<T>>(
          m,python::pep8_typename(fmt::format("{}_coroutine_root",name)))
        .def("__call__",&co_api<T>::operator(),"Resume coroutine.")
        .def("done",&co_api<T>::stump,"Check if coroutine has completed.")
        ;
    };
  export_coroutine_tree(type_c<none>,"none");
  export_coroutine_tree(type_c<python::object>,"object");
  graphs::export_submodule(m);
  animations::export_submodule(m);

  grammars::export_submodule(m);
  codes::export_submodule(m);

  automata::export_submodule(m);
  executions::export_submodule(m);

  binaries::export_submodule(m);
  devices::export_submodule(m);
  numerics::export_submodule(m);
  constructions::export_submodule(m);
}
#include<hysj/tools/epilogue.hpp>
