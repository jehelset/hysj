#include<bindings.hpp>
#include<devices.hpp>
#include<submodules.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::devices{

void export_submodule(python::module &m_hysj){
  auto m_devices = m_hysj.def_submodule("devices");
  vulkan::export_submodule(m_devices);
  gcc::export_submodule(m_devices);
}

}//hysj::devices
#include<hysj/tools/epilogue.hpp>
