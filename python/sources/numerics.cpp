#include<hysj/codes.hpp>
#include<hysj/numerics/newtons_method.hpp>
#include<hysj/numerics/fixed_point_iteration.hpp>

#include<bindings.hpp>
#include<submodules.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::numerics{

void export_fixed_point_iteration(python::module &m){
    
    python::reflected_repr(python::make_class<fixed_point_iteration_function>(m));

    m.def("fp_func",
          &numerics::fp_func,
          python::arg("code"),
          python::arg("function"),
          python::arg("variable"),
          python::arg("maxiter"));
    
    python::reflected_repr(python::make_class<fixed_point_iteration>(m));

    m.def("fp",
          &numerics::fp,
          python::arg("code"),
          python::arg("fp_func"),
          python::arg("test"));

}

void export_newtons_method(python::module &m){
    
    python::reflected_repr(python::make_class<newtons_method_function>(m));

    m.def("nm_func",
          &numerics::nm_func,
          python::arg("code"),
          python::arg("function"),
          python::arg("derivative"),
          python::arg("variable"),
          python::arg("step_size"),
          python::arg("maxiter"));
    
    python::reflected_repr(python::make_class<newtons_method>(m));

    m.def("nm",
          &numerics::nm,
          python::arg("code"),
          python::arg("nm_func"),
          python::arg("test"));

}

void export_submodule(python::module &m_hysj){
  auto m_numerics = m_hysj.def_submodule("numerics");
  export_fixed_point_iteration(m_numerics);
  export_newtons_method(m_numerics);
}

}//hysj::numerics
#include<hysj/tools/epilogue.hpp>
