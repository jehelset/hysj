#pragma once
#include<compare>
#include<functional>

#include<bindings.hpp>
#include<hysj/codes.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

using python_suborder = std::function<python::object(optag,natural,natural)>;
inline auto make_suborder(python_suborder py){
  return [py](optag i,natural j0,natural j1)->std::partial_ordering{
    auto o = py(i,j0,j1);
    if(o.is_none())
      return std::partial_ordering::unordered;
    else
      return python::cast<int>(o) <=> 0;
  };
};
inline auto default_python_suborder(code &p){
  return [&](optag sup,natural sub0,natural sub1)->std::partial_ordering{
    if(sup == any_of(optag::var, optag::itr, optag::rfl, optag::der))
      return sub0 <=> sub1;
    return p.subcmp(sup,sub0,sub1);
  };
}

} //hysj
#include<hysj/tools/epilogue.hpp>
