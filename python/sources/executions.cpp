#include<functional>
#include<span>

#include<hysj/codes.hpp>
#include<hysj/automata.hpp>
#include<hysj/executions.hpp>
#include<hysj/tools/reflection.hpp>
#include<hysj/tools/types.hpp>

#include<bindings.hpp>
#include<submodules.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::executions{

void export_buffers(python::module &m){
  python::make_class<buffer>(m,python::module_local())
    .def(python::init(&buffer::make),
         python::arg("code"),
         python::arg("buffer_capacity"))
    ;
}

void export_state(python::module &m){
  python::make_class<state>(m,python::module_local());
}

namespace discrete{

  void export_submodule(python::module &m_parent){
    auto m = m_parent.def_submodule("discrete");

    python::make_enum<devent>(m,python::module_local());
    python::make_class<config>(m,python::module_local());
    python::make_class<transition>(m,python::module_local());
    python::make_class<transitions>(m,python::module_local());
    python::make_class<execution,false>(m,python::module_local());

    m.def(
      "cc",
      [](code &P,const dautomaton &A,config s){
        return cc(P,A,s);
      },
      python::arg("code"),python::arg("automaton"),python::arg("config"))
      ;
  }


} //discrete

namespace continuous{

  void export_submodule(python::module &m_parent){

    auto m = m_parent.def_submodule("continuous");

    #define HYSJ_LAMBDA(id,value)               \
      m.attr(#id) = value;
    HYSJ_MAP_LAMBDA(HYSJ_EXECUTIONS_CONTINUOUS_SIGNS)
    #undef HYSJ_LAMBDA

    python::make_enum<cevent>(m,python::module_local());

    kumi::for_each(
      [&]<typename T>(type_t<T>){
        auto t = python::make_class<T>(m,python::module_local());
        t.def_static("make", &T::make);
        python::reflected_repr(t);
      },
      kumi::make_tuple(type_c<config>, type_c<be_config>));

    python::make_class<root>(m,python::module_local());
    python::make_class<roots>(m,python::module_local());
    python::make_class<execution,false>(m,python::module_local());

    kumi::for_each(
      bind<>(
        apply,
        [&](auto name, auto fn){
          m.def(name,
                fn,
                python::arg("code"),python::arg("automaton"), python::arg("config"));
        }),
      kumi::make_tuple(kumi::make_tuple("fe", static_cast<execution(*)(code &,const cautomaton &,fe_config)>(&fe)),
                       kumi::make_tuple("be", be)));


  }

} //continuous

namespace hybrid{

  void export_submodule(python::module &m_parent){
    auto m = m_parent.def_submodule("hybrid");
    
    python::make_enum<hevent>(m,python::module_local());
    python::make_class<config>(m, python::module_local());
    python::make_class<activity>(m, python::module_local());
    python::make_class<activities>(m, python::module_local());
    python::make_class<action>(m, python::module_local());
    python::make_class<actions>(m, python::module_local());
    python::make_class<root>(m, python::module_local());
    python::make_class<roots>(m, python::module_local());
    python::make_class<hcexec>(m, python::module_local());
    python::make_class<execution>(m, python::module_local());
    m.def(
      "cc",
      [](code &c,const hautomaton &A,config C){
        return cc(c,A,std::move(C));
      },
      python::arg("code"), python::arg("automaton"), python::arg("config") = config{})
      ;
  }

} //hybrid

void export_submodule(python::module &m_parent){
  auto m = m_parent.def_submodule("executions");
  export_buffers(m);
  export_state(m);
  continuous::export_submodule(m);
  discrete::export_submodule(m);
  hybrid::export_submodule(m);
}

}//hysj::executions
#include<hysj/tools/epilogue.hpp>
