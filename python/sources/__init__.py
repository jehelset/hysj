from ._hysj import graphs,automata,executions,binaries,devices,numerics,constructions

from . import codes
from . import animations

__version__ = _hysj.__version__

def cpp_local_typename(Type):
  return Type.cpp_typename.replace(f'{cpp_namespace}::','')
def cpp_local_member_typename(Type,name: str):
  return Type.cpp_member_typename(name).replace(f'{cpp_namespace}::','')
