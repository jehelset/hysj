#include<cstdint>
#include<optional>
#include<functional>
#include<tuple>

#include<fmt/core.h>

#include<hysj/binaries/gcc.hpp>
#include<hysj/devices/gcc.hpp>

#include<bindings.hpp>
#include<devices.hpp>
#include<submodules.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::devices::gcc{

void export_submodule(python::module &m_devices){
  auto m = m_devices.def_submodule("gcc");
  auto t_exe = python::make_class<exe,false,false>(m);
  python::make_devexe(t_exe);
  auto t_env = python::make_class<env>(m);
  python::reflected_repr(t_env);
  python::make_devenv(t_env);
}

}//hysj::devices::gcc
#include<hysj/tools/epilogue.hpp>

