#include<cstdint>
#include<optional>
#include<functional>
#include<tuple>

#include<fmt/core.h>

#include<hysj/binaries/spirv.hpp>
#include<hysj/devices/vulkan.hpp>
#include<hysj/tools/reflection.hpp>
#include<hysj/tools/types.hpp>
#include<hysj/tools/functional/apply.hpp>

#include<bindings.hpp>
#include<devices.hpp>
#include<submodules.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::devices::vulkan{

void export_submodule(python::module &m_devices){
  auto m = m_devices.def_submodule("vulkan");

  python::make_class<bin,false,false>(m);
  python::make_class<ctrl,false,false>(m);
  python::make_class<submit,false,false>(m);
  python::make_class<main,false,false>(m);
  auto t_exe = python::make_class<exe,false,false>(m);
  python::make_devexe(t_exe);
  python::make_enum<devtype>(m);
  {
    auto t_dev = python::make_class<dev,false>(m);
    python::fmt_str(t_dev);
  }
  auto t_env = python::make_class<env,false,false>(m);
  python::make_devenv(t_env);
  t_env.def("min_alignment", &env::min_alignment);
  t_env.def(
    python::init([]{
      return env::make();
    }));
}

}//hysj::devices::vulkan
#include<hysj/tools/epilogue.hpp>
