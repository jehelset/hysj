#pragma once
#include<cctype>
#include<iterator>
#include<map>
#include<ranges>
#include<source_location>
#include<span>
#include<stdexcept>
#include<string>
#include<tuple>
#include<type_traits>
#include<unordered_map>
#include<utility>
#include<variant>
#include<vector>

#include<pybind11/cast.h>
#include<pybind11/functional.h>
#include<pybind11/numpy.h>
#include<pybind11/operators.h>
#include<pybind11/pybind11.h>
#include<pybind11/stl.h>
#include<kumi/tuple.hpp>
#include<fmt/format.h>
#include<fmt/ranges.h>

#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/ranges/linear_span.hpp>
#include<hysj/tools/ranges/join.hpp>
#include<hysj/tools/ranges/map.hpp>
#include<hysj/tools/ranges/cat.hpp>
#include<hysj/tools/reflection.hpp>
#include<hysj/tools/type_traits.hpp>
#include<hysj/tools/types.hpp>
#include<hysj/tools/functional/apply.hpp>
#include<hysj/tools/functional/unwrap.hpp>
#include<hysj/tools/functional/utility.hpp>
#include<hysj/tools/utility.hpp>

#include<hysj/tools/prologue.hpp>
namespace pybind11::detail{

template<class T>
constexpr auto auto_value(T t){
  if constexpr(std::is_enum_v<T> || hysj::_HYSJ_VERSION_NAMESPACE::is_scoped_enum_v<T>)
    return hysj::_HYSJ_VERSION_NAMESPACE::to_underlying(t);
  else
    return t;
}

template<auto value_>
struct type_caster<hysj::_HYSJ_VERSION_NAMESPACE::constant_t<value_>>{
  using integral = decltype(value_);
  using constant = hysj::_HYSJ_VERSION_NAMESPACE::constant_t<value_>;
  PYBIND11_TYPE_CASTER(
    constant,
    _("Constant[") + make_caster<integral>::name + _("(") + _<auto_value(value_)>() + _(")]"));

  bool load(handle source, bool convert) const {
    auto caster = make_caster<integral>();
    return caster.load(source,convert) && cast_op<integral>(caster) == value_;
  }
  static handle cast(const constant &source,return_value_policy policy,handle parent) {
    return make_caster<integral>::cast(source(),policy,parent);
  }
};

template <template <typename...> class Tuple, typename... Ts>
class kumi_tuple_caster {
    using type = Tuple<Ts...>;
    static constexpr auto size = sizeof...(Ts);
    using indices = make_index_sequence<size>;

public:
    bool load(handle src, bool convert) {
        if (!isinstance<sequence>(src)) {
            return false;
        }
        const auto seq = reinterpret_borrow<sequence>(src);
        if (seq.size() != size) {
            return false;
        }
        return load_impl(seq, convert, indices{});
    }

    template <typename T>
    static handle cast(T &&src, return_value_policy policy, handle parent) {
        return cast_impl(std::forward<T>(src), policy, parent, indices{});
    }

    // copied from the PYBIND11_TYPE_CASTER macro
    template <typename T>
    static handle cast(T *src, return_value_policy policy, handle parent) {
        if (!src) {
            return none().release();
        }
        if (policy == return_value_policy::take_ownership) {
            auto h = cast(std::move(*src), policy, parent);
            delete src;
            return h;
        }
        return cast(*src, policy, parent);
    }

    static constexpr auto name = const_name("tuple[")
                                 + ::pybind11::detail::concat(make_caster<Ts>::name...)
                                 + const_name("]");

    template <typename T>
    using cast_op_type = type;

    explicit operator type() & { return implicit_cast(indices{}); }
    explicit operator type() && { return std::move(*this).implicit_cast(indices{}); }

protected:
    template <size_t... Is>
    type implicit_cast(index_sequence<Is...>) & {
      return kumi::make_tuple(cast_op<Ts>(get<Is>(subcasters))...);
    }
    template <size_t... Is>
    type implicit_cast(index_sequence<Is...>) && {
      return kumi::make_tuple(cast_op<Ts>(get<Is>(std::move(subcasters)))...);
    }

    static constexpr bool load_impl(const sequence &, bool, index_sequence<>) { return true; }

    template <size_t... Is>
    bool load_impl(const sequence &seq, bool convert, index_sequence<Is...>) {
        if ((... || !get<Is>(subcasters).load(seq[Is], convert))) {
            return false;
        }
        return true;
    }

    /* Implementation: Convert a C++ tuple into a Python tuple */
    template <typename T, size_t... Is>
    static handle
    cast_impl(T &&src, return_value_policy policy, handle parent, index_sequence<Is...>) {
        PYBIND11_WORKAROUND_INCORRECT_MSVC_C4100(src, policy, parent);
        PYBIND11_WORKAROUND_INCORRECT_GCC_UNUSED_BUT_SET_PARAMETER(policy, parent);
        std::array<object, size> entries{{reinterpret_steal<object>(
            make_caster<Ts>::cast(get<Is>(std::forward<T>(src)), policy, parent))...}};
        for (const auto &entry : entries) {
            if (!entry) {
                return handle();
            }
        }
        tuple result(size);
        int counter = 0;
        for (auto &entry : entries) {
            PyTuple_SET_ITEM(result.ptr(), counter++, entry.release().ptr());
        }
        return result.release();
    }

    Tuple<make_caster<Ts>...> subcasters;
};


template <typename... Ts>
class type_caster<kumi::tuple<Ts...>> : public kumi_tuple_caster<kumi::tuple, Ts...> {};

template<typename V,typename F>
struct type_caster<hysj::_HYSJ_VERSION_NAMESPACE::views::maps::view<V,F>>:
    list_caster<hysj::_HYSJ_VERSION_NAMESPACE::views::maps::view<V,F>,
                std::ranges::range_value_t<
                  hysj::_HYSJ_VERSION_NAMESPACE::views::maps::view<V,F>>>{
  bool load(handle src, bool){
    return false;
  }
};
template<typename T,typename S,typename E>
struct type_caster<hysj::_HYSJ_VERSION_NAMESPACE::views::linear_spans::view<T,S,E>>:
    list_caster<hysj::_HYSJ_VERSION_NAMESPACE::views::linear_spans::view<T,S,E>,T>{
  bool load(handle src, bool){
    return false;
  }
};
template<typename... V>
struct type_caster<hysj::_HYSJ_VERSION_NAMESPACE::views::cats::view<V...>>:
    list_caster<
      hysj::_HYSJ_VERSION_NAMESPACE::views::cats::view<V...>,
      std::ranges::range_value_t<
        hysj::_HYSJ_VERSION_NAMESPACE::views::cats::view<V...>>>{
  bool load(handle src, bool){
    return false;
  }
};

template<typename Type,std::size_t Extent>
struct type_caster<std::span<Type, Extent>>:
    list_caster<std::span<Type, Extent>,Type>{

  //FIXME: buffer? - jeh
  bool load(handle src, bool){
    using Array = array_t<Type,array::c_style|array::forcecast>;
    if(auto array = Array::ensure(src)){
      std::size_t size = array.size();
      
      if(Extent != std::dynamic_extent && Extent != size)
        return false;

      this->value = std::span<Type,Extent>(array.mutable_data(),size);
      return true;
    }
    return false;
  }
};

}

namespace hysj::inline _HYSJ_VERSION_NAMESPACE::python{

#define HYSJ_PYTHON_MODULE(name,module_object) PYBIND11_MODULE(name,module_object) 

using pybind11::arg;
using pybind11::pos_only;
using pybind11::arithmetic;
using pybind11::cast;
using pybind11::class_;
using pybind11::cpp_function;
using pybind11::detail::get_type_handle;
using pybind11::detail::get_type_info;
using pybind11::dynamic_attr;
using pybind11::enum_;
using pybind11::handle;
using pybind11::hash;
using pybind11::implicitly_convertible;
using pybind11::init;
using pybind11::keep_alive;
using pybind11::is_method;
using pybind11::make_tuple;
using pybind11::module;
using pybind11::module_local;
using pybind11::object;
using pybind11::type;
using pybind11::dict;
using pybind11::reinterpret_borrow;
using pybind11::return_value_policy;
using pybind11::self;
using pybind11::make_iterator;
using pybind11::stop_iteration;
using pybind11::str;
using pybind11::tuple;
using pybind11::none;
using pybind11::buffer_info;
using pybind11::memoryview;
using pybind11::buffer_protocol;
using pybind11::detail::any_container;

template<class T>
auto maybe_cast(T &&t){
  return cast(std::forward<T>(t));
}
inline auto maybe_cast(const object &t){
  return t;
}
inline auto maybe_cast(object &t){
  return t;
}

template<class T>
object make_typeid(){
  auto h = get_type_handle(typeid(T),false);
  if(h)
    return reinterpret_borrow<object>(h);
  return object(); 
}

inline std::string pep8_typename(const std::string &typename_){
  auto parts  =
    typename_
    |std::views::split('_')
    ;
  auto pepify =
    [](auto part){
      std::vector<char> pepped_part{(char)std::toupper(*std::ranges::begin(part))};
      std::ranges::copy(std::views::drop(part,1),
                        std::back_inserter(pepped_part));
      return pepped_part;
    };

  std::vector<std::vector<char>> pepped_parts(std::ranges::distance(parts));
  std::ranges::transform(parts,
                         std::begin(pepped_parts),
                         pepify);
  auto joined_pepped_parts = views::join(pepped_parts);

  return std::string{std::ranges::begin(joined_pepped_parts),std::ranges::end(joined_pepped_parts)};
}
struct with_name_prefix{ std::string prefix; };
struct with_name{ std::string name; };
struct with_name_suffix{ std::string suffix; };
struct with_name_wrap{ std::string prefix, suffix; };

namespace _{
  template<typename T>
  constexpr auto class_name_impl(){
    if constexpr(is_std_reference_wrapper<T>)
      return fmt::format("{}_ref", class_name_impl<typename T::type>());
    else
      return reflect<T>().name;
  }
}

template<class T>
auto make_class_name(with_name_prefix w){
  return pep8_typename(fmt::format("{}_{}",w.prefix,_::class_name_impl<T>()));
}
template<class T>
auto make_class_name(with_name_suffix w){
  return pep8_typename(fmt::format("{}_{}",_::class_name_impl<T>(),w.suffix));
}
template<class T>
auto make_class_name(with_name_wrap w){
  return pep8_typename(fmt::format("{}_{}_{}",w.prefix,_::class_name_impl<T>(),w.suffix));
}
template<class T>
auto make_class_name(with_name w){
  return pep8_typename(fmt::format("{}",w.name));
}
template<class T>
auto make_class_name(){
  return pep8_typename(_::class_name_impl<T>());
}

namespace _{
  template<class T,class... U>
  auto make_class_decl_impl(kumi::tuple<U...>,auto m,const std::string &n,auto... p){
    return class_<T,reflection::base_type_t<U{}>...>(m,n.c_str(),dynamic_attr(),p...);
  }
}
template<class T>
auto make_class_decl(auto m,const std::string &n,auto... p){
  if constexpr(reflection::reflected_struct<T>)
    return _::make_class_decl_impl<T>(bases(reflect<T>()),m,n,p...);
  else 
    return _::make_class_decl_impl<T>(kumi::tuple{},m,n,p...);
}
template<reflected_struct T>
auto make_class_decl(auto m,with_name_prefix w,auto... p){
  return make_class_decl<T>(m,make_class_name<T>(w),p...);
}
template<typename T>
auto make_class_decl(auto m,with_name w,auto... p){
  return make_class_decl<T>(m,make_class_name<T>(w),p...);
}
template<typename T>
auto make_class_decl(auto m,with_name_suffix w,auto... p){
  return make_class_decl<T>(m,make_class_name<T>(w),p...);
}
template<typename T>
auto make_class_decl(auto m,with_name_wrap w,auto... p){
  return make_class_decl<T>(m,make_class_name<T>(w),p...);
}
template<reflected_struct T>
auto make_class_decl(auto m,auto... p){
  return make_class_decl<T>(m,make_class_name<T>(),p...);
}

template<class value_type>
struct reflect_iter{
  value_type &value;

  bool first = true;
  natural index = 0;

  auto &iter(){ return *this; }

  using member = kumi::tuple<std::string,object>;
  template<auto reflected_member>
  member next_impl(){
    return kumi::make_tuple(std::string{reflected_member.name},
                            maybe_cast(value.*reflected_member.pointer));
  }
  auto next(){
    constexpr auto mirror = reflect<value_type>();
    if constexpr(member_count(mirror) == 0)
      throw stop_iteration();
    else{
      static constexpr auto table =
        kumi::apply(
          [](auto... m){
            return std::array{&reflect_iter::next_impl<decltype(m){}>... };
          },
          members(mirror));
      if(first)
        first = false;
      else
        ++index;
      if(index == table.size())
        throw stop_iteration();
      return (this->*table[index])();
    }
  }
};

inline constexpr struct reflected_asdict_fn{

  template<reflected_struct T>
  static dict impl(const T &t){
    constexpr auto mirror = reflect<T>();
    dict a;
    kumi::for_each([&](auto m){ a[m.name] = impl(t.*m.pointer); },
                   members(mirror));
    return a;
  }
  template<reflected_enum T>
  static auto impl(const T &t){
    return cast(to_underlying(t));
  }

  template<class T>
  static auto impl(const T &t){
    return maybe_cast(t);
  }

  template<class... T>
  static dict impl(const std::variant<T...> &t){
    dict a;
    a["type"] = t.index();
    a["value"] = std::visit([](auto &u){ return object(impl(u)); },t);
    return a;
  }
  template<class T,class U>
  static auto impl(const std::pair<T,U> &t){
    return cast(std::make_pair(impl(t.first),impl(t.second)));
  }
  template<class... T>
  static auto impl(const std::tuple<T...> &t){
    return cast(apply([](const auto &... u){ return std::make_tuple(impl(u)...); },t));
  }
  template<class... T>
  static auto impl(const kumi::tuple<T...> &t){
    return cast(apply([](const auto &... u){ return kumi::make_tuple(impl(u)...); },t));
  }
  template<class T,class U>
  static auto impl(const std::vector<T,U> &t){
    return cast(ranges::vec(t,[](const auto &u){ return impl(u); }));
  }
  template<class T,std::size_t N>
  static auto impl(const std::array<T,N> &t){
    return cast(apply([](const auto &... u){ return std::array{impl(u)...}; }, t));
  }
  template<class K,class T,class H,class KE,class A>
  static dict impl(const std::unordered_map<K,T,H,KE,A> &t){
    dict a;
    for(const auto &[k,u]:t)
      a[impl(k)] = impl(u);
    return a;
  }
  template<class K,class T,class C,class A>
  static dict impl(const std::map<K,T,C,A> &t){
    dict a;
    for(const auto &[k,u]:t)
      a[impl(k)] = impl(u);
    return a;
  }

  template<reflected_struct T,class... O>
  auto operator()(class_<T,O...> p)const{
    return p.def("asdict",
                 [](const T &t){
                   return impl(t);
                 });
  }
}
  reflected_asdict{};

template<class T,class... O>
auto copy_semantics(class_<T,O...> p){
  return p.def("__copy__",
               [](const T &self){ return T(self); })
    .def("__deepcopy__",
         [](const T &self, dict){ return T(self); },arg("memo"));
}

template<class T>
constexpr auto reflected_member_default(){
  if constexpr(std::is_same_v<T,object>)
    return nullptr;
  else if constexpr(requires{T{};})
    return T{};
  else
    return nullptr;
}

constexpr auto reflected_init_base_arg(auto b,auto i){
  using B = reflection::base_type_t<b>;
  static const std::string name = fmt::format("base_{}", i());
  if constexpr(std::is_same_v<B,object> || requires{B{};})
    return arg(name.c_str()) = B{};
  else
  return arg(name.c_str());
}
constexpr auto reflected_init_member_arg(auto m){
  using T = uncvref_t<typename decltype(m)::type>;
  if constexpr(std::is_same_v<T,object> || requires{T{};})
    return arg(m.name) = reflected_member_default<T>();
  else
    return arg(m.name);
}

template<typename T>
[[nodiscard]] constexpr auto compute_typename()noexcept{
  std::string_view N{std::source_location::current().function_name()};
  N.remove_prefix(N.find("[with T = ") + 10);
  N.remove_suffix(N.size() - N.find_last_of(']'));
  return N;
}

template<typename T,typename... O>
auto reflect_cpp_typename(class_<T,O...> p){
  p.attr("cpp_typename") = compute_typename<T>();
  constexpr auto mirror = reflect<T>();

  if constexpr(member_count(mirror))
    kumi::apply(
      [&](auto... m) -> void{
        p.def_static(
          "cpp_member_typename",
          [=](std::string_view name) -> std::optional<std::string_view>{
            static constexpr std::array table{
              kumi::tuple<std::string_view,std::string_view>{
                m.name,
                compute_typename<reflection::member_type_t<m>>()
              }...
            };

            auto it = std::ranges::find(table,name,lift((kumi::get<0>)));
            if(it == std::ranges::end(table))
              return std::nullopt;
            return kumi::get<1>(*it);
          });
      },
      members(mirror));
};

template<class T,class... O>
requires reflected<T>
auto reflected_members_init(class_<T,O...> p){
  constexpr auto r = reflect<T>();
  apply(
    bind<>(
      [&]<std::size_t... b>(std::index_sequence<b...>,auto... m){
        if constexpr(sizeof...(b))
          p.def(
            init(
              [](reflection::base_type_t<base_at(r, constant_c<b>)>... u,
                 reflection::member_type_t<m>... v){
                return T{std::move(u)..., std::move(v)...};
              }),
            reflected_init_base_arg(base_at(r, constant_c<b>), constant_c<b>)...,
            pos_only(),
            reflected_init_member_arg(m)...);
        else
          p.def(
            init(
              [](decltype(m)::type... v){
                return T{v...};
              }),
            reflected_init_member_arg(m)...);
      },
      reflection::base_indices(r)),
    reflection::members(r));
}

template<bool with_init = true,bool with_write = true,class T,class... O>
requires reflected<T>
auto reflected_members(class_<T,O...> p){
  constexpr auto mirror = reflect<T>();
  using t_ = T;

  if constexpr (with_init)
    reflected_members_init(p);

  kumi::for_each(
    [&](auto m){
      using M = typename decltype(m)::type;
      if constexpr (with_write && requires(T &t,const M &v){ (t.*(m.pointer)) = v; })
        p.def_readwrite(m.name,m.pointer);
      else
        p.def_readonly(m.name,m.pointer);
    },
    members(mirror));

  reflect_cpp_typename(p);
  using reflect_iter_type = reflect_iter<t_>;
  if (!get_type_info(typeid(reflect_iter_type), false)) 
    class_<reflect_iter_type>(handle(), "iterator", module_local())
      .def("__iter__",&reflect_iter_type::iter)
      .def("__next__",&reflect_iter_type::next)
      ;
  p.def("__iter__",[](t_ &v){ return reflect_iter_type{v}; });

  reflected_asdict(p);
  
  return p;
}

template<typename T,class... O>
auto fmt_str(class_<T,O...> t){
  return t.def("__str__",[](const T &t){ return fmt::format("{}",t); });
}

template<bool with_init = true,bool with_copy = true,reflected_struct T,typename... O>
auto reflect_class(class_<T, O...> t){
  reflected_members<with_init, with_copy>(t);
  if constexpr(with_copy && requires(const T &t){ T{t}; })
    return copy_semantics(t);
  return t;
}

template<reflected_struct T,bool with_init = true,bool with_copy = true>
auto make_class(auto m,auto... p){
  return reflect_class<with_init,with_copy>(make_class_decl<T>(m,p...));
}

template<class E>
struct make_enumerator_names_fn{
  constexpr auto operator()()const{
    return enumerator_names<E>;
  }
};
template<class E>
constexpr make_enumerator_names_fn<E> make_enumerator_names{};

namespace _{
  template<typename E>
  auto make_enum_impl(auto pybind_module,const std::string &name,auto... options){
    static_assert(reflected_enum<E>);
    auto pybind_enum =
      enum_<E>(pybind_module,name.c_str(),arithmetic(),options...);
    auto values = enumerator_values<E>;
    auto names = make_enumerator_names<E>();
    for(auto i:views::indices(enumerator_count<E>))
      pybind_enum.value(names[i].data(),values[i]);
    pybind_enum.export_values();
    return pybind_enum;
  }
}
template<typename E>
auto make_enum(auto pybind_module,auto... options){
  return _::make_enum_impl<E>(pybind_module,
                                   pep8_typename(std::string(enum_name<E>)),
                                   options...);
}
template<typename E>
auto make_enum(auto pybind_module,const std::string &name,auto... options){
  return _::make_enum_impl<E>(pybind_module,
                                   pep8_typename(name),
                                   options...);
}

template<reflected_struct T,class... O>
auto reflected_repr(class_<T,O...> p){

  constexpr auto mirror = reflect<T>();
  //FIXME: probably not ok - jeh 
  auto s = str(p).cast<std::string>();
  s = s.substr(8,s.size() - 10);

  return p.def(
    "__repr__",
    [=](const T &t){
      if constexpr (member_count(mirror)){
        auto X = kumi::apply(
          [&](auto... m){
            return std::array{
              fmt::format("{} = {}",
                          m.name,
                          str(maybe_cast(t.*m.pointer)).cast<std::string>())...
            };
          },
          members(mirror));
        return fmt::format("{}({})",s,fmt::join(X,","));
      }
      return fmt::format("{}()",s);
    });
}

template<class G,class... O>
auto hash(class_<G,O...> t){
  return t.def(hash(self));
}

template<class G,class... O>
auto comparison_operators(class_<G,O...> t){
  return
    t.def(self == self)
     .def(self != self)
     .def(self <  self)
    ;
}

void make_attr(auto m,const std::string &n,auto t){
  m.attr(n.c_str()) = t;
}
void make_alias(auto m,const std::string &n,object t){
  m.attr(n.c_str()) = t;
}


} //hysj::python
#include<hysj/tools/epilogue.hpp>
