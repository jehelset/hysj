#include<cstdint>
#include<functional>
#include<utility>

#include<hysj/codes.hpp>
#include<hysj/automata.hpp>
#include<hysj/executions.hpp>
#include<hysj/constructions.hpp>
#include<hysj/devices/vulkan.hpp>
#include<hysj/devices/gcc.hpp>
#include<hysj/tools/coroutines/trees.hpp>
#include<hysj/tools/reflection.hpp>
#include<hysj/tools/types.hpp>

#include<bindings.hpp>
#include<codes/cells.hpp>
#include<submodules.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::constructions::hybrid{

using python_control_opaque = std::function<python::object(xevent)>;

struct python_control{
  python_control_opaque opaque;

  template<typename CoGet>
  co::api_t<CoGet> operator()(CoGet,auto event,auto... tail){

    auto &co_this = HYSJ_CO(CoGet);
    auto result = opaque(xevent{event()});

    if(!result.is_none())
      co_await co::put<0>(co_this,result);
    co_final_await co::nil<1>(co_this);
  }
};

template<typename T,typename... O>
auto export_basic_run(python::class_<T, O...> t){
  for(auto w:codes::widthtab)
    codes::with_width(
      [&](auto w){
        auto n = fmt::format("discrete_time{}", w());
        t.def(n.c_str(),
              [w](T &t){
                return codes::to_memoryview(
                  *t.mem,
                  codes::integers_c, w,
                  t.exec->discrete_execution.state.buffer.independent);
              });
      },
      w);

  for(auto t_:codes::typetab)
    codes::with_type(
      [&](auto d,auto w){
        auto d_name = enumerator_name(d())[0];
        auto n = fmt::format("discrete_{}state{}", d_name, w());
        t.def(n.c_str(),
              [d, w](T &t,std::size_t i,std::size_t j){
                return codes::to_memoryview(
                  *t.mem, d, w,
                  t.exec->discrete_execution.state.buffer.dependent[i][j]);
              },
              python::arg("index"),
              python::arg("order"));
        t.def(n.c_str(),
              [d, w](T &t,std::size_t i){
                return ranges::vec(
                  t.exec->discrete_execution.state.buffer.dependent[i],
                  [&, d, w](auto x){
                    return codes::to_memoryview(*t.mem, d, w, x);
                  });
              },
              python::arg("index"));
        t.def(n.c_str(),
              [d, w](T &t){
                return ranges::vec(
                  t.exec->discrete_execution.state.buffer.dependent,
                  bind<1>(
                    ranges::vec,
                    [&, d, w](auto x){
                      return codes::to_memoryview(*t.mem, d, w, x);
                    }));
              });
      },
      t_);

  for(auto w:codes::widthtab)
    codes::with_width(
      [&](auto w){
        {
          auto n = fmt::format("continuous_time{}", w());
          t.def(n.c_str(),
                [w](T &t){
                  return codes::to_memoryview(
                    *t.mem,
                    codes::reals_c, w, 
                    t.exec->continuous_execution.state.buffer.independent);
                });
        }
        {
          auto n = fmt::format("continuous_state{}", w());
          t.def(n.c_str(),
                [w](T &t,std::size_t i,std::size_t j){
                  return codes::to_memoryview(*t.mem, codes::reals_c, w, 
                                              t.exec->continuous_execution.state.buffer.dependent[i][j]);
                },
                python::arg("index"),
                python::arg("order"));
          t.def(n.c_str(),
                [w](T &t,std::size_t i){
                  return ranges::vec(
                    t.exec->continuous_execution.state.buffer.dependent[i],
                    [&](auto x){
                      return codes::to_memoryview(*t.mem, codes::reals_c, w, x);
                    });
                },
                python::arg("index"));
          t.def(n.c_str(),
                [w](T &t){
                  return ranges::vec(
                    t.exec->continuous_execution.state.buffer.dependent,
                    bind<1>(
                      ranges::vec,
                      [&](auto x){
                        return codes::to_memoryview(*t.mem, codes::reals_c, w, x);
                      }));
                });
        }
      },
      w);



  for(auto t_:codes::typetab)
    codes::with_type(
      [&](auto d,auto w){
        auto d_name = enumerator_name(d())[0];

        auto n0 = fmt::format("action_{}variable{}",d_name, w());
        t.def(n0.c_str(),
              [d,w](T &t,std::size_t i){
                return codes::to_memoryview(
                  *t.mem, d, w, 
                  accessed(t.exe->code, t.exec->actions.container[i].variable).value());
              },
              python::arg("index"));

        auto n2 = fmt::format("action_{}variables{}",d_name, w());
        t.def(n2.c_str(),
              [d,w](T &t){
                return ranges::vec(
                  t.exec->actions.container,
                  [&,d,w](const auto &a){
                    return codes::to_memoryview(*t.mem, d, w, accessed(t.exe->code, a.variable).value());
                  });
              });
      },
      t_);
  for(auto w:codes::widthtab)
    codes::with_width(
      [&](auto w){
        auto n0 = fmt::format("action_guard{}", w());
        t.def(n0.c_str(),
              [w](T &t,std::size_t i){
                return codes::to_memoryview(
                  *t.mem, codes::booleans_c, w, 
                  t.exec->actions.container[i].guard);
              },
              python::arg("index"));
        auto n1 = fmt::format("action_guards{}", w());
        t.def(n1.c_str(),
              [w](T &t){
                return ranges::vec(
                  t.exec->actions.container,
                  [&](const auto &a){
                    return codes::to_memoryview(*t.mem, codes::booleans_c, w, a.guard);
                  });
              });
      },
      w);

  for(auto w:codes::widthtab)
    codes::with_width(
      [&](auto w){
        auto n0 = fmt::format("activity_guard{}", w());
        t.def(n0.c_str(),
              [w](T &t,std::size_t i){
                return codes::to_memoryview(
                  *t.mem, codes::booleans_c, w, 
                  t.exec->activities.container[i].guard);
              },
              python::arg("index"));
        auto n1 = fmt::format("activity_guards{}", w());
        t.def(n1.c_str(),
              [w](T &t){
                return ranges::vec(
                  t.exec->activities.container,
                  [&](const auto &a){
                    return codes::to_memoryview(*t.mem, codes::booleans_c, w, a.guard);
                  });
              });
      },
      w);


  for(auto w:codes::widthtab)
    codes::with_width(
      [&](auto w){
        auto n0 = fmt::format("root_variable{}", w());
        t.def(n0.c_str(),
              [w](T &t,std::size_t i){
                return codes::to_memoryview(
                  *t.mem, codes::integers_c, w,
                  t.exec->roots.container[i].variable);
              },
              python::arg("index"));
        auto n1 = fmt::format("root_variables{}", w());
        t.def(n1.c_str(),
              [w](T &t){
                return ranges::vec(
                  t.exec->roots.container,
                  [&, w](const auto &a){
                    return codes::to_memoryview(*t.mem, codes::reals_c, w, a.variable);
                  });
              });
      },
      w);

  for(auto w:codes::widthtab)
    codes::with_width(
      [&](auto w){
        auto n0 = fmt::format("root_guard{}", w());
        t.def(n0.c_str(),
              [w](T &t,std::size_t i){
                return codes::to_memoryview(
                  *t.mem, codes::booleans_c, w,
                  t.exec->roots.container[i].guard);
              },
              python::arg("index"));
        auto n1 = fmt::format("root_guards{}", w());
        t.def(n1.c_str(),
              [w](T &t){
                return ranges::vec(
                  t.exec->roots.container,
                  [&,w](const auto &a){
                    return codes::to_memoryview(*t.mem, codes::booleans_c, w, a.guard);
                  });
              });
      },
      w);

  for(auto t_:codes::typetab)
    codes::with_type(
      [&](auto d,auto w){
        auto d_name = enumerator_name(d())[0];
        auto n = fmt::format("continuous_{}param{}", d_name, w());
        t.def(n.c_str(),
              [d, w](T &t,std::size_t i){
                return codes::to_memoryview(*t.mem, d, w,
                                      t.exec->automaton.continuous_parameters[i]);
              },
              python::arg("index"));
      },
      t_);

}

template<typename T,typename... O>
auto export_buffered_run(python::class_<T, O...> t){
  for(auto w:codes::widthtab)
    codes::with_width(
      [&](auto w){
        auto n = fmt::format("discrete_time{}", w());
        t.def(n.c_str(),
              [w](T &t){
                return codes::to_memoryview(
                  *t.mem,
                  codes::integers_c, w,
                  t.exec->discrete_execution.state.buffer.independent);
              });
      },
      w);

  for(auto t_:codes::typetab)
    codes::with_type(
      [&](auto d,auto w){
        auto d_name = enumerator_name(d())[0];
        auto n = fmt::format("discrete_{}state{}", d_name, w());
        t.def(n.c_str(),
              [d, w](T &t,std::size_t i,std::size_t j){
                return codes::to_memoryview(
                  *t.mem, d, w,
                  t.exec->discrete_execution.state.buffer.dependent[i][j]);
              },
              python::arg("index"),
              python::arg("order"));
        t.def(n.c_str(),
              [d, w](T &t,std::size_t i){
                return ranges::vec(
                  t.exec->discrete_execution.state.buffer.dependent[i],
                  [&, d, w](auto x){
                    return codes::to_memoryview(*t.mem, d, w, x);
                  });
              },
              python::arg("index"));
        t.def(n.c_str(),
              [d, w](T &t){
                return ranges::vec(
                  t.exec->discrete_execution.state.buffer.dependent,
                  bind<1>(
                    ranges::vec,
                    [&, d, w](auto x){
                      return codes::to_memoryview(*t.mem, d, w, x);
                    }));
              });
      },
      t_);

  for(auto w:codes::widthtab)
    codes::with_width(
      [&](auto w){
        {
          auto n = fmt::format("continuous_time{}", w());
          t.def(n.c_str(),
                [w](T &t){
                  return codes::to_memoryview(
                    *t.mem,
                    codes::reals_c, w, 
                    t.exec->continuous_execution.state.buffer.independent);
                });
        }
        {
          auto n = fmt::format("continuous_state{}", w());
          t.def(n.c_str(),
                [w](T &t,std::size_t i,std::size_t j){
                  return codes::to_memoryview(*t.mem, codes::reals_c, w, 
                                        t.exec->continuous_execution.state.buffer.dependent[i][j]);
                },
                python::arg("index"),
                python::arg("order"));
          t.def(n.c_str(),
                [w](T &t,std::size_t i){
                  return ranges::vec(
                    t.exec->continuous_execution.state.buffer.dependent[i],
                    [&](auto x){
                      return codes::to_memoryview(*t.mem, codes::reals_c, w, x);
                    });
                },
                python::arg("index"));
          t.def(n.c_str(),
                [w](T &t){
                  return ranges::vec(
                    t.exec->continuous_execution.state.buffer.dependent,
                    bind<1>(
                      ranges::vec,
                      [&](auto x){
                        return codes::to_memoryview(*t.mem, codes::reals_c, w, x);
                      }));
                });
        }
      },
      w);

  for(auto w:codes::widthtab)
    codes::with_width(
      [&](auto w){
        auto n0 = fmt::format("action_guard{}", w());
        t.def(n0.c_str(),
              [w](T &t,std::size_t i){
                return codes::to_memoryview(
                  *t.mem, codes::booleans_c, w, 
                  t.exec->actions.container[i].guard);
              },
              python::arg("index"));
        auto n1 = fmt::format("action_guards{}", w());
        t.def(n1.c_str(),
              [w](T &t){
                return ranges::vec(
                  t.exec->actions.container,
                  [&](const auto &a){
                    return codes::to_memoryview(*t.mem, codes::booleans_c, w, a.guard);
                  });
              });
      },
      w);

  for(auto w:codes::widthtab)
    codes::with_width(
      [&](auto w){
        auto n0 = fmt::format("activity_guard{}", w());
        t.def(n0.c_str(),
              [w](T &t,std::size_t i){
                return codes::to_memoryview(
                  *t.mem, codes::booleans_c, w, 
                  t.exec->activities.container[i].guard);
              },
              python::arg("index"));
        auto n1 = fmt::format("activity_guards{}", w());
        t.def(n1.c_str(),
              [w](T &t){
                return ranges::vec(
                  t.exec->activities.container,
                  [&](const auto &a){
                    return codes::to_memoryview(*t.mem, codes::booleans_c, w, a.guard);
                  });
              });
      },
      w);

  for(auto w:codes::widthtab)
    codes::with_width(
      [&](auto w){
        auto n0 = fmt::format("root_guard{}", w());
        t.def(n0.c_str(),
              [w](T &t,std::size_t i){
                return codes::to_memoryview(
                  *t.mem, codes::booleans_c, w,
                  t.exec->roots.container[i].guard);
              },
              python::arg("index"));
        auto n1 = fmt::format("root_guards{}", w());
        t.def(n1.c_str(),
              [w](T &t){
                return ranges::vec(
                  t.exec->roots.container,
                  [&,w](const auto &a){
                    return codes::to_memoryview(*t.mem, codes::booleans_c, w, a.guard);
                  });
              });
      },
      w);

  for(auto t_:codes::typetab)
    codes::with_type(
      [&](auto d,auto w){
        auto d_name = enumerator_name(d())[0];
        auto n = fmt::format("continuous_{}param{}", d_name, w());
        t.def(n.c_str(),
              [d, w](T &t,std::size_t i){
                return codes::to_memoryview(*t.mem, d, w,
                                      t.exec->continuous_execution.parameters[i]);
              },
              python::arg("index"));
      },
      t_);


}

void export_submodule(python::module &m_parent){
  auto m = m_parent.def_submodule("hybrid");
  each(
    kumi::make_tuple(
      kumi::make_tuple("Vk", type_c<vkenv>),
      kumi::make_tuple("Gcc", type_c<gccenv>)),
    unpack(
      [&]<typename Env>(auto prefix, type_t<Env>){
        using Exe = devices::exe_t<Env &>;
        using Run = run<Exe, Env>;
        auto t = python::make_class<Run, false>(m, python::with_name_prefix{prefix}, python::module_local());
        export_buffered_run(t);
        t.def("get",
              [](Run &t){
                t.get();
              });
        t.def("put",
              [](Run &t){
                t.put();
              });
        t.def("init",
              [](Run &t){
                t.init();
              });
        t.def("__call__",
              [](Run &run,python_control_opaque ctrl){
                return run(co_get<python::object>{}, python_control{ctrl});
              },
              python::arg("control"),
              python::keep_alive<0, 1>())
          ;
        m.def("cc",
              [](hexec &exec,
                 cells &mem,
                 Exe &exe,
                 Env &env){
                return cc(exec, mem, exe, env);
              },
              python::arg("exec"),
              python::arg("mem"),
              python::arg("exe"),
              python::arg("env"),
              python::keep_alive<0, 1>(),
              python::keep_alive<0, 2>(),
              python::keep_alive<0, 3>(),
              python::keep_alive<0, 4>());
        ;
      }));

}

}//hysj::constructions::hybrid
#include<hysj/tools/epilogue.hpp>
