#include<functional>
#include<span>

#include<fmt/core.h>

#include<hysj/codes.hpp>
#include<hysj/automata/discrete.hpp>
#include<hysj/executions/discrete.hpp>
#include<hysj/devices/gcc.hpp>
#include<hysj/devices/vulkan.hpp>
#include<hysj/constructions/discrete.hpp>
#include<hysj/tools/reflection.hpp>
#include<hysj/tools/types.hpp>
#include<hysj/tools/functional/apply.hpp>

#include<bindings.hpp>
#include<devices.hpp>
#include<submodules.hpp>
#include<constructions.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::constructions::discrete{

template<typename Exe>
auto export_run(python::module &m,auto prefix){
  using T = run<Exe>;

  export_basic_run<dexec, Exe>(m, prefix);

  auto t_name = 
    python::with_name{
      fmt::format("{}{}{}",
                  python::make_class_name<T>(),
                  prefix,
                  python::make_class_name<Exe>())
    };
  auto t = python::make_class<T,false>(m,t_name,python::module_local());

  t.def("put",
        [](T &t){
          t.put();
        });
  t.def("get",
        [](T &t){
          t.get();
        });
  t.def("init",
        [](T &t){
          t.init();
        });

  for(auto w:codes::widthtab)
    codes::with_width(
      [&](auto w){
        auto n = fmt::format("time{}", w());
        t.def(n.c_str(),
              [=](T &t){
                return codes::to_memoryview(
                  *t.mem, 
                  codes::integers_c, w,
                  t.exec->state.buffer.independent);
              });
      },
      w);
  for(auto t_:codes::typetab)
    codes::with_type(
      [&](auto d,auto w){
        auto d_name = enumerator_name(d())[0];
        auto n = fmt::format("{}state{}", d_name, w());
        t.def(n.c_str(),
              [d,w](T &t,std::size_t i,std::size_t j){
                return codes::to_memoryview(*t.mem, 
                                      d, w,
                                      t.exec->state.buffer.dependent[i][j]);
              },
              python::arg("index"),
              python::arg("order"));
        t.def(n.c_str(),
              [d,w](T &t,std::size_t i){
                return ranges::vec(
                  t.exec->state.buffer.dependent[i],
                  [&,d](auto x){
                    return codes::to_memoryview(*t.mem,d,w,x);
                  });
              },
              python::arg("index"));
        t.def(n.c_str(),
              [d,w](T &t){
                return ranges::vec(
                  t.exec->state.buffer.dependent,
                  bind<1>(
                    ranges::vec,
                    [&,d,w](auto x){
                      return codes::to_memoryview(*t.mem, d, w, x);
                    }));
              });
      },
      t_);

  for(auto w:codes::widthtab)
    codes::with_width(
      [&](auto w){
        {
          auto n = fmt::format("transition_guard{}", w());
          t.def(n.c_str(),
                [w](T &t,std::size_t i){
                  return codes::to_memoryview(*t.mem, codes::booleans_c,
                                        w, t.exec->transitions.container[i].guard);
                },
                python::arg("index"));
        }
        {
          auto n = fmt::format("transition_guards{}", w());
          t.def(n.c_str(),
                [w](T &t){
                  return ranges::vec(
                    views::map(t.exec->transitions.container, &dexecs::transition::guard),
                    [&](auto g){
                      return codes::to_memoryview(*t.mem, 
                                            codes::booleans_c, 
                                            w,
                                            g);
                    });
                });
        }
      },
      w);

  for(auto t_:codes::typetab)
    codes::with_type(
      [&](auto d,auto w){
        auto d_name = enumerator_name(d())[0];
        auto n_function = fmt::format("transition_{}function{}", d_name, w());
        t.def(n_function.c_str(),
              [d,w](T &t,std::size_t i){
                return codes::to_memoryview(*t.mem, d, w,
                                      t.exec->transitions.container[i].function);
              },
              python::arg("index"));
        auto n_functions = fmt::format("transition_{}functions{}", d_name, w());
        t.def(n_functions.c_str(),
              [d,w](T &t){
                return ranges::vec(
                  views::map(t.exec->transitions.container, &dexecs::transition::function),
                  [&,d](auto f){
                    return codes::to_memoryview(*t.mem, d, w, f);
                  });
              });
      },
      t_);

  t.def("event",
        [](T &run, std::size_t i){
          return run.event(i);
        });
  t.def("__call__",
        [](T &run){
          auto ring = run();
          return python::make_iterator(std::ranges::begin(ring), std::ranges::end(ring));
        });

}

void export_submodule(python::module &m_parent){
  auto m = m_parent.def_submodule("discrete");
  each(
    kumi::make_tuple(
      kumi::make_tuple("Vk", type_c<vkexe>),
      kumi::make_tuple("Gcc",type_c<gccexe>)),
    unpack(
      [&]<typename Exe>(auto prefix, type_t<Exe>){
        export_run<Exe>(m, prefix);
        m.def("cc",
              [](dexec &exec,cells &mem, Exe &exe){
                return druns::cc(exec, mem, exe);
              },
              python::arg("exec"),
              python::arg("mem"),
              python::arg("exe"),
              python::keep_alive<0, 1>(),
              python::keep_alive<0, 2>(),
              python::keep_alive<0, 3>());
       }));
}

}//hysj::constructions::discrete
#include<hysj/tools/epilogue.hpp>
