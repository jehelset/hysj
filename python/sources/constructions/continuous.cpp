#include<cstdint>
#include<functional>
#include<optional>
#include<utility>

#include<hysj/codes.hpp>
#include<hysj/automata/continuous.hpp>
#include<hysj/executions/continuous.hpp>
#include<hysj/devices/vulkan.hpp>
#include<hysj/devices/gcc.hpp>
#include<hysj/constructions/continuous.hpp>
#include<hysj/tools/coroutines/trees.hpp>
#include<hysj/tools/reflection.hpp>
#include<hysj/tools/types.hpp>
#include<hysj/tools/functional/apply.hpp>

#include<bindings.hpp>
#include<codes/cells.hpp>
#include<constructions.hpp>
#include<submodules.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::constructions::continuous{

using python_control_opaque = std::function<python::object(cevent)>;

struct python_control{
  python_control_opaque opaque;

  template<typename CoGet>
  co::api_t<CoGet> operator()(CoGet,cevent event){

    auto &co_this = HYSJ_CO(CoGet);
    auto result = opaque(event);

    if(!result.is_none())
      co_await co::put<0>(co_this,result);
    co_final_await co::nil<1>(co_this);
  }
};


template<class T,class... O>
auto export_run(python::class_<T, O...> t){
  for(auto w:codes::widthtab)
    codes::with_width(
      [&](auto w){
        {
          auto n = fmt::format("time{}", w());
          t.def(n.c_str(),
                [=](T &t){
                  return codes::to_memoryview(
                    *t.mem,
                    codes::reals_c, w,
                    t.exec->state.buffer.independent);
                });
        }
        {
          auto n = fmt::format("state{}", w());
          t.def(n.c_str(),
                [w](T &t,std::size_t i,std::size_t j){
                  return codes::to_memoryview(*t.mem, codes::reals_c, w,
                                        t.exec->state.buffer.dependent[i][j]);
                },
                python::arg("index"),
                python::arg("order"));
          t.def(n.c_str(),
                [w](T &t,std::size_t i){
                  return ranges::vec(
                    t.exec->state.buffer.dependent[i],
                    [&](auto x){
                      return codes::to_memoryview(*t.mem, codes::reals_c, w, x);
                    });
                },
                python::arg("index"));
          t.def(n.c_str(),
                [w](T &t){
                  return ranges::vec(
                    t.exec->state.buffer.dependent,
                    bind<1>(
                      ranges::vec,
                      [&](auto x){
                        return codes::to_memoryview(*t.mem, codes::reals_c, w, x);
                      }));
                });
        }
      },
      w);

  t.def("get",
        [](T &t){
          t.get();
        });
  t.def("put",
        [](T &t){
          t.put();
        });
  t.def("init",
        [](T &t){
          t.init();
        });
  return t;
}

template<typename Exe>
auto export_run(python::module &m,auto prefix){
 
  export_basic_run<cexec, Exe>(m, prefix);
  using T = run<Exe>;

  auto t_name = python::with_name{
    fmt::format("{}{}{}",
                python::make_class_name<T>(),
                prefix,
                python::make_class_name<Exe>())
  };
 
  auto t = python::make_class<T,false>(m,t_name,python::module_local());

  t.def("get_roots",
        [](T &t){
          t.get_roots();
        });
  t.def("put_roots",
        [](T &t){
          t.put_roots();
        });

  for(auto w:codes::widthtab)
    codes::with_width(
      [&](auto w){
        auto n = fmt::format("root_residual{}", w());
        t.def(n.c_str(),
              [w](T &t,std::size_t i){
                return codes::to_memoryview(*t.mem, codes::reals_c, w,
                                            codes::accessed(t.exe->code, t.exec->roots.container[i].residual).value());
              },
              python::arg("index"));
      },
      w);

  t.def("root_sign",
        [](T &t,std::size_t i){
          return codes::to_memoryview(*t.mem, codes::integers_c, 64_N,
                                      codes::accessed(t.exe->code, t.exec->roots.container[i].sign).value());
        },
        python::arg("index"));

  t.def("root_event",
        [](T &t){
          return codes::to_memoryview(*t.mem, codes::booleans_c, 64_N,
                                      t.exec->roots.event);
        });

  t.def("root_sign_flip",
        [](T &t,std::size_t i){
          return codes::to_memoryview(*t.mem, codes::booleans_c, 64_N,
                                      t.exec->roots.container[i].sign_flip);
        },
        python::arg("index"));

  t.def("event",
        [](T &run, std::size_t i){
          return run.event(i);
        });
  t.def("__call__",
        [](T &run){
          auto ring = run();
          return python::make_iterator(std::ranges::begin(ring), std::ranges::end(ring));
        });

  return export_run(t);
}

void export_submodule(python::module &m_parent){
  auto m = m_parent.def_submodule("continuous");
  each(
    kumi::make_tuple(
      kumi::make_tuple("Vk", type_c<vkexe>),
      kumi::make_tuple("Gcc", type_c<gccexe>)),
    unpack(
      [&]<typename Exe>(auto prefix, type_t<Exe>){
        export_run<Exe>(m, prefix);
        m.def("cc",
              [](cexec &exec,cells &mem,Exe &exe){
                return cc(exec, mem, exe);
              },
              python::arg("exec"),
              python::arg("mem"),
              python::arg("exe"),
              python::keep_alive<0, 1>(),
              python::keep_alive<0, 2>(),
              python::keep_alive<0, 3>());
      }));
}

}//hysj::constructions::continuous
#include<hysj/tools/epilogue.hpp>
