#include"submodules.hpp"

#include<hysj/codes.hpp>
#include<hysj/automata.hpp>

#include<hysj/tools/reflection.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::automata{

namespace discrete{

  void export_submodule(auto &m_automata){
    auto m = m_automata.def_submodule("discrete");
    
    python::reflected_repr(
      python::make_class<dtransition>(m))
      ;
    python::make_class<dautomaton>(m)
      .def("order",
           [](const dautomaton &a){ return dorder(a); })
      .def("order",
           [](const dautomaton &a,const code &p){ return dorder(p,a); },
           python::arg("code"))
      .def("rank",
           [](const dautomaton &a){ return drank(a); })
      .def("rank",
           [](const dautomaton &a,const code &p){ return drank(p,a); },
           python::arg("code"))
      .def("cardinality",
           [](const dautomaton &a,const code &p){ return dcardinality(p,a); },
           python::arg("code"))
      ;
  }

} //discrete

namespace continuous{

  void export_submodule(auto m_automata){

    auto m = m_automata.def_submodule("continuous");
    
    auto t_automaton = python::make_class<cautomaton>(m,python::module_local())
      .def("order",
           [](const cautomaton &a){ return corder(a); })
      .def("order",
           [](const cautomaton &a,const code &p){ return corder(p,a); },
           python::arg("code"))
      .def("rank",
           [](const cautomaton &a){ return crank(a); })
      .def("rank",
           [](const cautomaton &a,const code &p){ return crank(p,a); },
           python::arg("code"))
      .def("size",
           [](const cautomaton &a){ return csize(a); })
      .def("size",
           [](const cautomaton &a,const code &p){ return csize(p,a); },
           python::arg("code"))
      .def("regular",
           [](const cautomaton &a,const code &p){ return cregular(p,a); },
           python::arg("code"))
      ;

  }

  
} //continuous

namespace hybrid{
  
  void export_submodule(auto &m_automata){
    auto m = m_automata.def_submodule("hybrid");
    python::reflected_repr(
      python::make_class<hactivity>(m,python::module_local()))
      ;
    python::reflected_repr(
      python::make_class<haction>(m,python::module_local()))
      ;
    python::reflected_repr(
      python::make_class<hroot>(m,python::module_local()))
      ;
    python::make_class<hautomaton>(m)
      .def("rank",
           [](const hautomaton &a,const code &p){ return hrank(p,a); },
           python::arg("code"))
      .def("rank",
           [](const hautomaton &a){ return hrank(a); })
      .def("size",
           [](const hautomaton &a,const code &p){ return hsize(p,a); },
           python::arg("code"))
      .def("size",
           [](const hautomaton &a){ return hsize(a); })
      .def("degree",
           [](const hautomaton &a,const code &p){ return hdegree(p,a); },
           python::arg("code"))
      .def("degree",
           [](const hautomaton &a){ return hdegree(a); })
      ;
  }
  
} //hybrid

void export_submodule(python::module &m_hysj){
  auto m = m_hysj.def_submodule("automata");

  continuous::export_submodule(m);
  discrete::export_submodule(m);
  hybrid::export_submodule(m);
}

}//hysj::automata
#include<hysj/tools/epilogue.hpp>
