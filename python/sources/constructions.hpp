#pragma once

#include<cstdint>

#include<fmt/format.h>

#include<hysj/codes/sets.hpp>
#include<hysj/tools/reflection.hpp>

#include<bindings.hpp>
#include<codes/cells.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::constructions{

template<typename Exec, typename Exe>
auto export_basic_run(python::module &m,auto prefix){
  using T = basic_run<Exec, Exe>;

  auto t_name = 
    python::with_name{
      fmt::format("{}{}{}{}",
                  python::make_class_name<T>(),
                  python::make_class_name<Exec>(),
                  prefix,
                  python::make_class_name<Exe>())
    };
  auto t = python::make_class<T,false>(m,t_name,python::module_local());

  for(auto t_:codes::typetab)
    codes::with_type(
      [&](auto d,auto w){
        auto d_name = enumerator_name(d())[0];
        auto n = fmt::format("{}param{}", d_name, w());
        t.def(n.c_str(),
              [d, w](T &t,std::size_t i){
                return codes::to_memoryview(*t.mem, d, w, t.exec->parameters[i]);
              },
              python::arg("index"));
      },
      t_);
  return t;
}

}//hysj::constructions
#include<hysj/tools/epilogue.hpp>
