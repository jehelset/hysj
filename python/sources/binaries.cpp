#include<hysj/codes.hpp>
#include<hysj/tools/reflection.hpp>
#include<hysj/binaries/spirv.hpp>
#include<hysj/binaries/gcc.hpp>

#include<bindings.hpp>
#include<codes.hpp>
#include<submodules.hpp>
#include<tools/graphs.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::binaries{

namespace spirv{

  void export_submodule(python::module &m_binaries){

    auto m = m_binaries.def_submodule("spirv");

    python::reflected_repr(python::make_class<env>(m));

    python::make_enum<access>(m, python::arithmetic())
      .def("is_input", &is_input)
      .def("is_output", &is_output)
      ;

    python::make_class<buffer>(m);
    python::make_class<mem>(m);
    python::make_class<kernel>(m);
    python::make_class<bin>(m);

    m.def("env",
          []{
            return spvenv{};
          });
    m.def("cc",
          [](const env &e,code &c,codes::taskfam d){
            return cc(e, c, d);
          },
          python::arg("env"),
          python::arg("code"),
          python::arg("op"));
    m.def("ccopt",
          [](const env &e,code &c,codes::taskfam d){
            return ccopt(e, c, d);
          },
          python::arg("env"),
          python::arg("code"),
          python::arg("op"));
    m.def("err",
          [](const bin &a){
            return err(a);
          },
          python::arg("bin"));
    m.def("dis",
          [](const bin &a){
            return dis(a);
          },
          python::arg("bin"));

  }
}
namespace gcc{
  void export_submodule(python::module &m_binaries){

    auto m = m_binaries.def_submodule("gcc");

    python::reflected_repr(python::make_class<config>(m));
    python::make_class<bin, false>(m);

    m.def("cfg",
          []{
            return config{};
          });
    m.def("cc",
          [](const config &e,code c,codes::taskfam d){
            return cc(e, std::move(c), d);
          },
          python::arg("config"),
          python::arg("code"),
          python::arg("op"));
    m.def("cc",
          [](const config &e,code c,codes::cellinfos l, codes::taskfam d){
            return cc(e, std::move(c), std::move(l), d);
          },
          python::arg("config"),
          python::arg("code"),
          python::arg("layout"),
          python::arg("op"));
  }
}

void export_submodule(python::module &m_hysj){
  auto m_binaries = m_hysj.def_submodule("binaries");
  spirv::export_submodule(m_binaries);
  gcc::export_submodule(m_binaries);
}

}//hysj::binaries
#include<hysj/tools/epilogue.hpp>
