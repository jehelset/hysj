#include<cmath>
#include<cstdio>
#include<filesystem>
#include<fstream>
#include<string>
#include<sstream>

#include<bindings.hpp>

#include<cairo.h>
#include<fmt/format.h>
#include<fmt/ostream.h>
#include<librsvg/rsvg.h>
#include<pipeline.h>
#include<py3cairo.h>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes::latex{

std::string tex_to_svg(std::string_view tex_content,int tex_fontsize){
  
  constexpr std::string_view tex_template = 
    "\\documentclass[12pt]{{article}}"
    "\\usepackage[utf8]{{inputenc}}"
    "\\usepackage{{amsmath}}"
    "\\usepackage{{amsthm}}"
    "\\usepackage{{amssymb}}"
    "\\usepackage{{amsfonts}}"
    "\\usepackage{{anyfontsize}}"
    "\\usepackage{{bm}}"
    "\\usepackage{{xcolor}}"
    "\\pagestyle{{empty}}"
    ""
    "\\begin{{document}}"
    "\\fontsize{{{}}}{{0}}\\selectfont "
    "\\begin{{minipage}}[t][0pt]{{\\linewidth}}{}\\end{{minipage}}"
    ""  
    "\\end{{document}}";
  
  const auto tex = fmt::format(tex_template,tex_fontsize,tex_content);

  const auto tmpdir =
    [&]{
      auto tmpdir_root = std::filesystem::temp_directory_path();
      auto tmpdir_template = (tmpdir_root / "hysj-codes-tex_to_svg-XXXXXXX").string();
      auto mkdtempok = mkdtemp(tmpdir_template.data());
      HYSJ_ASSERT(mkdtempok);
      return std::filesystem::path(tmpdir_template);
    }();
  {
    const auto tex_file = tmpdir / "_.tex";
    std::ofstream tex_stream{tex_file};
    tex_stream << tex;
  }
  auto pipe_clean =
    [&]{
      auto cmd = pipecmd_new_args("rm","-rf",tmpdir.c_str(),NULL);
      pipecmd_discard_err(cmd,1);
      return pipeline_new_commands(cmd,NULL);
    }();

  {
    const auto stdout_file = tmpdir / "stdout";
    auto pipe =
      [&]{
        auto cmd_tex_to_dvi =
          [&]{
            auto cmd = pipecmd_new_args("latexmk","-dvi","-quiet","-silent","_.tex",NULL);
            pipecmd_chdir(cmd,tmpdir.c_str());
            pipecmd_discard_err(cmd,1);
            return cmd;
          }();
        auto cmd_dvi_to_svg =
          [&]{
            auto cmd = pipecmd_new_args("dvisvgm","--no-fonts","--exact","--output=_.svg","_.dvi",NULL);
            pipecmd_chdir(cmd,tmpdir.c_str());
            pipecmd_discard_err(cmd,1);
            return cmd;
          }();
        auto cmd_tex_to_svg =
          pipecmd_new_sequence("latexmk_dvisvgm",cmd_tex_to_dvi,cmd_dvi_to_svg,NULL);
        auto pipe = pipeline_new_commands(cmd_tex_to_svg,NULL);
        pipeline_want_outfile(pipe,stdout_file.c_str());
        return pipe;
      }();
    if(auto ec = pipeline_run(pipe);ec != 0){
      const auto log_file = tmpdir / "_.log";
      auto log = [&]{
        std::ifstream log_file_stream{log_file};
        std::ostringstream log_stream;
        log_stream
          << "ec: " << ec 
          << "\n\ntex:\n"
          << tex
          << "\n\nlog:\n"
          << log_file_stream.rdbuf();
        return log_stream.str();
      }();
      pipeline_run(pipe_clean);
      throw std::runtime_error(log);
    }
  }
  return [&]{
    auto str =
      [&]{
        const auto svg_file = tmpdir / "_.svg";
        std::ifstream svg_file_stream{svg_file};
        std::ostringstream svg_stream;
        svg_stream << svg_file_stream.rdbuf();
        return svg_stream.str();
      }();
    pipeline_run(pipe_clean);
    return str;
  }();
}

struct cairo_svg_renderer{
  using handle_deleter = decltype([](RsvgHandle *handle){ if(handle) g_object_unref(handle); });
  using handle_type = std::unique_ptr<RsvgHandle,handle_deleter>;
  
  handle_type handle = nullptr;

  cairo_svg_renderer(std::string_view svg):
    handle{
      rsvg_handle_new_from_data((const unsigned char *)svg.data(),
                                svg.size(),
                                nullptr),
      handle_deleter{}}
  {}

  void operator()(python::object pycairo_context,
                  double width,double height){
    auto cairo_context = PycairoContext_GET(pycairo_context.ptr());
    const auto rsvg_viewport =
      [&]{
        const auto size = get_size();
        if(!size)
          throw std::runtime_error("Could not compute size.");
        const auto [svg_iwidth,svg_iheight] = *size;

        const auto svg_width = static_cast<double>(svg_iwidth),
          svg_height = static_cast<double>(svg_iheight);
        HYSJ_ASSERT(svg_width > 0);
        HYSJ_ASSERT(svg_height > 0);
        const auto svg_ratio = std::min(width / svg_width,height / svg_height);

        const auto svg_scaled_width = svg_ratio * svg_width,
          svg_scaled_height = svg_ratio * svg_height,
          svg_x = (width - svg_scaled_width) / 2.0,
          svg_y = (height - svg_scaled_height) / 2.0;
        return RsvgRectangle{
          .x = svg_x,
          .y = svg_y,
          .width = svg_scaled_width,
          .height = svg_scaled_height
        };
      }();
    rsvg_handle_render_document(handle.get(), cairo_context, &rsvg_viewport, nullptr);
  }
  void operator()(python::object pycairo_context){
    const auto size = get_size();
    if(!size)
      throw std::runtime_error("Could not compute size.");
    const auto [w,h] = *size;
    return operator()(pycairo_context,
                      static_cast<double>(w),
                      static_cast<double>(h));
  }

  void set_dpi(double dpi){
    rsvg_handle_set_dpi(handle.get(),dpi);
  }
  std::optional<std::array<int,2>> get_size()const{
    std::optional<std::array<int,2>> size{};
    if(double w{},h{};
       rsvg_handle_get_intrinsic_size_in_pixels(handle.get(),&w,&h))
      size = std::array{
        static_cast<int>(std::round(w)),
        static_cast<int>(std::round(h))};
    return size;
  }

  friend HYSJ_MIRROR_STRUCT_ID(cairo_svg_renderer);
};

void export_submodule(python::module &m_codes){
  auto m = m_codes.def_submodule("_latex");
  m.def("tex_to_svg",
        &tex_to_svg,
        python::arg("tex_content"),
        python::arg("tex_fontsize") = 12);

  python::make_class_decl<cairo_svg_renderer>(m)
    .def(python::init<std::string_view>(),
         python::arg("svg"))
    .def("__call__",
         [](cairo_svg_renderer &renderer,
            python::object context,
            double width,
            double height){
           return renderer(context,width,height);
         },
         python::arg("context"),
         python::arg("width"),
         python::arg("height"))
    .def("__call__",
         [](cairo_svg_renderer &renderer,
            python::object context){
           return renderer(context);
         },
         python::arg("context"))
    .def("set_dpi",
         &cairo_svg_renderer::set_dpi,
         python::arg("dpi"))
    .def("get_size",
         &cairo_svg_renderer::get_size)
    ;

  m.def("cairo_svg_renderer",
        [](std::string_view svg){
          return cairo_svg_renderer{svg};
        },
        python::arg("svg"));
}

} //hysj::codes::latex
#include<hysj/tools/epilogue.hpp>
