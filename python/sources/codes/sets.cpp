#include<utility>
#include<vector>

#include<hysj/codes.hpp>
#include<hysj/grammars.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/reflection.hpp>
#include<hysj/tools/type_traits.hpp>
#include<hysj/tools/types.hpp>

#include<bindings.hpp>
#include<grammars.hpp>
#include<submodules.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

void export_sets(python::module &m){
  python::make_enum<set>(m);
  {
    auto t = python::make_class<type>(m);
    python::comparison_operators(t);
    python::reflected_repr(t);
    python::fmt_str(t);
  }
}

}//hysj::codes
#include<hysj/tools/epilogue.hpp>
