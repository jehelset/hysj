from math import isnan
import cairo

from hysj import codes
from hysj._hysj._codes._latex import *

def cairo_tex_renderer(tex_content: str, tex_fontsize: int = 12):
    return cairo_svg_renderer(svg=tex_to_svg(tex_content=tex_content, tex_fontsize=tex_fontsize))

def cairo_tex(
    context: cairo.Context,
    tex_content: str,
    dpi: float = 96.0,
    tex_fontsize: int = 12,
    width: float = None,
    height: float = None,
    **kwargs,
):
    renderer = cairo_tex_renderer(tex_content=tex_content, tex_fontsize=tex_fontsize)
    renderer.set_dpi(dpi)
    if width is None or height is None:
        renderer(context=context)
    else:
        renderer(context=context, width=width, height=height)


class Renderer:

    # FIXME: push new presendence list inside f.ex. div etc - jeh
    def presedence(self, id):
        match (id.tag):
            case codes.var | codes.lit | codes.cst | codes.log | codes.sin:
                return 0
            # codes.Sign:              0,
            case codes.add | codes.sub:
                return 2
            case codes.mul:
                return 3
            case codes.pow:
                return 4
            case codes.div:
                return 5
            case codes.neg:
                return 6
            case codes.der:
                return 10
            case codes.eq | codes.lt:
                return 100
            case codes.land | codes.lor:
                return 999
            case codes.lnot:
                return 1000
            case _:
                return 4

    def __init__(self, code: codes.Code, symbol_labeler=None, align: str = ""):
        self.code = code
        self.symbol_labeler = symbol_labeler
        self.presedences = []
        self.align = align

    def cst(self, id, datum, args, oprns):
        Literal = codes.Literal
        if type(datum) != Literal:
            return str(datum)  # FIXME: bool -> top / bot - jeh
        match (datum):
            case Literal.bot:
                return r"\mathbf{\bot}"
            case Literal.top:
                return r"\mathbf{\top}"
            case Literal.zero:
                return r"\mathbf{0}"
            case Literal.one:
                return r"\mathbf{1}"
            case Literal.negone:
                return r"-\mathbf{1}"
            case Literal.two:
                return r"\mathbf{2}"
            case Literal.inf:
                return r"\infty"
            case Literal.pi:
                return r"\pi"
            case Literal.e:
                return r"e"
            case _:
                return "?"
    def lit(self, id, datum, args, oprns):
        return self.cst(id, datum, args, oprns)

    def itr(self, id, datum, args, oprns):
        return f" i_{{{id.id()[1]}}}"

    def add(self, id, datum, args, oprns):
        if len(oprns[0]) == 1:
            self.presedences[-1] = 0
            return self(oprns[0][0])
        return " + ".join([self(op) for op in oprns[0]])

    def sub(self, id, datum, args, oprns):
        if oprns[1] == 0:
            self.presedences[-1] = 0
            return self(oprns[0])
        return " - ".join([self(op) for op in [oprns[0]] + oprns[1]])

    def div(self, id, datum, args, oprns):
        return f" \\frac{{{self(oprns[0])}}}{{{self(oprns[1])}}}"

    def mul(self, id, datum, args, oprns):
        return " \\cdot ".join([self(op) for op in oprns[0]])

    def der(self, id, datum, args, oprns):
        upper = "\\partial " + self(oprns[0])
        lower = "\\partial " + self(oprns[1])

        if oprns[2]:
            lower += ", " +" ,".join([self(o) for o in oprns[2]])
        return f"\\frac{{{upper}}}{{{lower}}}"

    def pow(self, id, datum, args, oprns):
        return f"{self(oprns[0])}^{{{self(oprns[1])}}}"

    def log(self, id, datum, args, oprns):
        return f"\\log_{{{self(oprns[0])}}}{{{self(oprns[1])}}}"

    def rot(self, id, datum, args, oprns):
        return f"{self(oprns[0])}_{{" + ", ".join([self(o) for o in oprns[1]]) + "}"

    def sin(self, id, datum, args, oprns):
        return f"\\sin ({self(oprns[0])})"

    def var(self, id, datum, args, oprns):
        if self.symbol_labeler and self.symbol_labeler(id, datum):
            return self.symbol_labeler(id, datum)
        base = "\\alpha"
        return f"{base}_{{{id.id()[1]}}}"
    def rfl(self, id, datum, args, oprns):
        if self.symbol_labeler and self.symbol_labeler(id, datum):
            return self.symbol_labeler(id, datum)
        base = "\\beta"
        return f"{base}_{{{id.id()[1]}}}"

    def eq(self, id, datum, args, oprns):
        return f"{self(oprns[0])} {self.align} = {self(oprns[1])}"

    def neg(self, id, datum, args, oprns):
        return f"-{self(oprns[0])}"

    def fct(self, id, datum, args, oprns):
        return f"!{self(oprns[0])}"

    def lt(self, id, datum, args, oprns):
        return f"{self(oprns[0])} {self.align}< {self(oprns[1])}"

    def lif(self, id, datum, args, oprns):
        return f"[{self(oprns[0])}]"

    def lnot(self, id, datum, args, oprns):
        return f"\\neg {self(oprns[0])}"

    def land(self, id, datum, args, oprns):
        return " \\wedge ".join([self(op) for op in oprns[0]])

    def lor(self, id, datum, args, oprns):
        return " \\vee ".join([self(op) for op in oprns[0]])

    def sel(self, id, datum, args, oprns):
        return f"{self(oprns[0])} ? " + ":".join([self(op) for op in oprns[1]])
    
    def put(self, id, datum, args, oprns):
        return f"{self(oprns[0])} ← {self(oprns[1])}"

    def then(self, id, datum, args, oprns):
        return "→(" + "," .join([self(op) for op in oprns[0]]) + ")"

    def when(self, id, datum, args, oprns):
        return "↩(" + ",".join([self(op) for op in oprns[0]]) + ")"

    def __call__(self, id):
        if id is None:
            return "\\emptyset"

        match id.tag if hasattr(id, "tag") else id:
            case codes.Famtag():
                id = id.obj
            case codes.Optag():
                pass
            case [x,y]:
                id = codes.Code.objcast(id)
            case _:
                raise TypeError(f"invalid type: {id}")

        def dispatch(id, datum, args, oprns):
            op = list(codes.Optag.__members__)[id.tag]

            parent_presedence = 0 if not self.presedences else self.presedences[-1]
            presedence = self.presedence(id)
            self.presedences.append(presedence)
            result = getattr(self, op)(id, datum, args, oprns)
            presedence = self.presedences[-1]
            self.presedences = self.presedences[:-1]
            if parent_presedence != 0 and presedence != 0 and presedence <= parent_presedence:
                return f"({result})"
            return result

        return dispatch(
            id, self.code.dat(id), self.code.rels(id), self.code.args(id)
        )
