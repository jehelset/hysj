#include<utility>
#include<vector>

#include<hysj/codes.hpp>
#include<hysj/grammars.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/reflection.hpp>
#include<hysj/tools/type_traits.hpp>
#include<hysj/tools/types.hpp>

#include<bindings.hpp>
#include<grammars.hpp>
#include<submodules.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

void export_code(python::module &m){

  auto t_code = python::make_class_decl<code>(m);

  python::reflected_members<false>(t_code);

  {
    auto expose_eval = [&](auto d){
      auto n = fmt::format("{}csteval",enumerator_name(d())[0]);
      t_code.def(
        n.c_str(),
        [v = constant_c<d.value>](const code &s,id o){
          return csteval(v,s,o);
        },
        python::arg("op"));
    };
    each_enumerator<set>(expose_eval);
  }

  t_code
    .def(python::init([]{ return code{}; }))
    .def("copy",
         [](code &m,id i){
           return copy(m,i);
         },
         python::arg("source"))
    .def("equal",
         [](const code &m,id lhs,id rhs){
           return equal(m,lhs,rhs);
         },
         python::arg("lhs"),
         python::arg("rhs"))
    .def("to_string",
         [](const code &m,id o){
           return to_string(m,o);
         },
         python::arg("op"))
    .def("resize",
         [](code &code){
           resize(code);
         })
    .def("compile",
         [](code &code,id op){
           compile(code,op);
         },
         python::arg("op"))
    .def("compile",
         [](code &code,const std::vector<id> &ops){
           compile(code,ops);
         },
         python::arg("ops"))
    .def("compile",
         [](code &code){
           compile(code);
         })
    .def("exts",
         [](code &code,id o){
           return ranges::vec(oexts(code,o));
         },
         python::arg("op"))
    .def("ext",
         [](code &code,id o){
           return oext(code,o);
         },
         python::arg("op"))
    .def("dom",
         [](code &code,id o){
           return odom(code,o);
         },
         python::arg("op"))
    .def("set",
         [](code &code,id o){
           return oset(code,o);
         },
         python::arg("op"))
    ;

  {
    auto expose_cst = [&](auto d){
      auto n = fmt::format("{}cst",enumerator_name(d())[0]);
      t_code.def(
        n.c_str(),
        [=](code &c,codes::cell<d.value> x,id w){
          return cst(c, constant{value<d.value>{x}}, w);
        },
        python::arg("value"),
        python::arg("width"));
    };
    each_enumerator<set>(expose_cst);
  }
  {
    auto expose_cst = [&](auto s,auto w){
      auto n = fmt::format("{}cst{}",enumerator_name(s())[0], w());
      t_code.def(
        n.c_str(),
        [=](code &c,codes::cell<s()> x){
          return cst(c, constant{value<s()>{x}}, c.builtins.widths[w()]);
        },
        python::arg("value"));
    };
    for(auto t:codes::typetab)
      with_type(expose_cst, t);
  }
  for(auto w:codes::widthtab){
    auto n = fmt::format("typ{}", w);
    t_code.def(
      n.c_str(),
      [=](code &c,id d){
        return typ(c, d, c.builtins.widths[w]);
      },
      python::arg("dom"));
    t_code.def(
      n.c_str(),
      [=](code &c,codes::domexprfam d){
        return typ(c, d, c.builtins.widths[w]);
      },
      python::arg("dom"));
  }

  t_code.def("shape",
             [](code &c,id o){
               return oshape(c.statics.shapes, o);
             },
             python::arg("op"));
  auto export_shape_fn = [&](auto n){
    {
      auto f_name = fmt::format("slots{}",n());
      t_code.def(f_name.c_str(),
                [=](const code &s,id o){ return oslots(s.statics.shapes,o,n);  },
                python::arg("op"));
    }
    {
      auto f_name = fmt::format("ext{}",n());
      t_code.def(f_name.c_str(),
                [=](const code &s,id o){ return oext(s,o,n);  },
                python::arg("op"));
    }
    {
      auto f_name = fmt::format("exts{}",n());
      t_code.def(f_name.c_str(),
                [=](const code &s,id o){ return ranges::vec(oexts(s,o,n));  },
                python::arg("op"));
    }
    {
      auto f_name = fmt::format("rank{}",n());
      t_code.def(f_name.c_str(),
                [=](const code &s,id o){ return orank(s,o,n);  },
                python::arg("op"));
    }
    {
      auto f_name = fmt::format("itrs{}",n());
      t_code.def(f_name.c_str(),
                [=](const code &s,id o){ return ranges::vec(oitrs(s.statics.shapes,o,n));  },
                python::arg("op"));
    }
    {
      auto f_name = fmt::format("itrops{}",n());
      t_code.def(f_name.c_str(),
                [=](const code &s,id o){ return ranges::vec(oitrops(s.statics.shapes,o,n));  },
                python::arg("op"));
    }
    {
      auto f_name = fmt::format("dims{}",n());
      t_code.def(f_name.c_str(),
                [=](const code &s,id o){ return ranges::vec(odims(s,s.statics.shapes,o,n));  },
                python::arg("op"));
    }
  };
  export_shape_fn(0_N);
  export_shape_fn(1_N);

  {
    t_code.def("pin",
               [](code &c,id d,id f){
                 return pin(c, d, f);
               },
               python::arg("dev"),
               python::arg("buf"));
    t_code.def("pin",
               [](code &c,codes::devsym d,codes::exprfam f){
                 return pin(c, d, f);
               },
               python::arg("dev"),
               python::arg("buf"));
    t_code.def("inc",
               [](code &c,id f){
                 return inc(c, f);
               },
               python::arg("op"));
    t_code.def("inc",
               [](code &c,codes::exprfam f){
                 return inc(c, f);
               },
               python::arg("op"));
    t_code.def("dec",
               [](code &c,id f){
                 return dec(c, f);
               },
               python::arg("op"));
    t_code.def("dec",
               [](code &c,codes::exprfam f){
                 return dec(c, f);
               },
               python::arg("op"));
    t_code.def("abs",
               [](code &c,id f){
                 return abs(c, f);
               },
               python::arg("op"));
    t_code.def("abs",
               [](code &c,codes::exprfam f){
                 return abs(c, f);
               },
               python::arg("op"));
    t_code.def("obs",
               [](code &c,id f){
                 return obs(c, f);
               },
               python::arg("op"));
    t_code.def("obs",
               [](code &c,codes::exprfam f, codes::exprfam w){
                 return obs(c, f, w);
               },
               python::arg("op"),
               python::arg("width"));
    t_code.def("obs",
               [](code &c,id f, id w){
                 return obs(c, f, w);
               },
               python::arg("op"),
               python::arg("width"));
    t_code.def("obs",
               [](code &c,codes::exprfam f){
                 return obs(c, f);
               },
               python::arg("op"));
    t_code.def("cos",
               [](code &c,id f){
                 return cos(c, f);
               },
               python::arg("op"));
    t_code.def("cos",
               [](code &c,codes::exprfam f){
                 return cos(c, f);
               },
               python::arg("op"));
    t_code.def("tan",
               [](code &c,id f){
                 return tan(c, f);
               },
               python::arg("op"));
    t_code.def("tan",
               [](code &c,codes::exprfam f){
                 return tan(c, f);
               },
               python::arg("op"));
    t_code.def("neq",
               [](code &c,id l,id r){
                 return neq(c, l, r);
               },
               python::arg("op0"),
               python::arg("op1"));
    t_code.def("neq",
               [](code &c,codes::exprfam l,codes::exprfam r){
                 return neq(c, l, r);
               },
               python::arg("op0"),
               python::arg("op1"));
    t_code.def("geq",
               [](code &c,id l,id r){
                 return geq(c, l, r);
               },
               python::arg("op0"),
               python::arg("op1"));
    t_code.def("geq",
               [](code &c,codes::exprfam l,codes::exprfam r){
                 return geq(c, l, r);
               },
               python::arg("op0"),
               python::arg("op1"));
    t_code.def("leq",
               [](code &c,id l,id r){
                 return leq(c, l, r);
               },
               python::arg("op0"),
               python::arg("op1"));
    t_code.def("leq",
               [](code &c,codes::exprfam l,codes::exprfam r){
                 return leq(c, l, r);
               },
               python::arg("op0"),
               python::arg("op1"));
    t_code.def("min",
               [](code &c,id l,id r){
                 return min(c, l, r);
               },
               python::arg("op0"),
               python::arg("op1"));
    t_code.def("min",
               [](code &c,codes::exprfam l,codes::exprfam r){
                 return min(c, l, r);
               },
               python::arg("op0"),
               python::arg("op1"));
    t_code.def("max",
               [](code &c,id l,id r){
                 return max(c, l, r);
               },
               python::arg("op0"),
               python::arg("op1"));
    t_code.def("max",
               [](code &c,codes::exprfam l,codes::exprfam r){
                 return max(c, l, r);
               },
               python::arg("op0"),
               python::arg("op1"));
    t_code.def("clamp",
               [](code &c,id l,id x,id h){
                 return clamp(c, l, x, h);
               },
               python::arg("low"),
               python::arg("op"),
               python::arg("high"));
    t_code.def("clamp",
               [](code &c,codes::exprfam l,codes::exprfam x,codes::exprfam r){
                 return clamp(c, l, x, r);
               },
               python::arg("low"),
               python::arg("op"),
               python::arg("high"));
    t_code.def("sqrt",
               [](code &c,codes::exprfam f){
                 return pow(c, f, ohalf(c, f));
               },
               python::arg("op"))
      ;
    t_code.def("sqrt",
               [](code &c,id f){
                 return pow(c, f, ohalf(c, f));
               },
               python::arg("op"))
      ;

    t_code.def("pow2",
               [](code &c,codes::exprfam f){
                 return pow2(c, f);
               },
               python::arg("op"))
      ;
    t_code.def("pow2",
               [](code &c,id f){
                 return pow2(c, codes::expr(f));
               },
               python::arg("op"))
      ;

    t_code.def("if_",
               [](code &c,codes::exprfam p,codes::taskfam t){
                 return fork(c, p, {c.builtins.noop, t});
               },
               python::arg("pred"),
               python::arg("body"))
      ;
    t_code.def("if_",
               [](code &c,id p,id t){
                 return fork(c, p, {c.builtins.noop, t});
               },
               python::arg("pred"),
               python::arg("body"))
      ;
    t_code.def("ifel",
               [](code &c,codes::exprfam p,codes::taskfam t,codes::taskfam f){
                 return fork(c, p, {f, t});
               },
               python::arg("pred"),
               python::arg("true_body"),
               python::arg("false_body"))
      ;
    t_code.def("ifel",
               [](code &c,id p,id t,id f){
                 return fork(c, p, {f, t});
               },
               python::arg("pred"),
               python::arg("true_body"),
               python::arg("false_body"))
      ;
    t_code.def("rms",
               [](code &c,codes::exprfam e){
                 return rms(c, e);
               },
               python::arg("expr"))
      ;
    t_code.def("rms",
               [](code &c,id e){
                 return rms(c, codes::expr(e));
               },
               python::arg("expr"))
      ;
    t_code.def("tmp_dup",
               [](code &c,codes::exprfam e){
                 return tmp_dup(c, e);
               },
               python::arg("expr"))
      ;
    t_code.def("tmp_dup",
               [](code &c,id e){
                 return tmp_dup(c, codes::expr(e));
               },
               python::arg("expr"))
      ;
    t_code.def("tmp_dup",
               [](code &c,codes::exprfam e,codes::domexprfam w){
                 return tmp_dup(c, e, w);
               },
               python::arg("expr"),
               python::arg("width"))
      ;
    t_code.def("tmp_dup",
               [](code &c,id e,id w){
                 return tmp_dup(c, codes::expr(e), codes::domexpr(w));
               },
               python::arg("expr"),
               python::arg("width"))
      ;

    t_code.def("send",
               [](code &c,codes::devsym d,codes::exprfam e){
                 return send(c, d, e);
               },
               python::arg("dev"),
               python::arg("expr"))
      ;
    t_code.def("send",
               [](code &c,id d,id e){
                 return send(c, d, e);
               },
               python::arg("dev"),
               python::arg("expr"))
      ;
    t_code.def("recv",
               [](code &c,codes::devsym d,codes::exprfam e){
                 return recv(c, d, e);
               },
               python::arg("dev"),
               python::arg("expr"))
      ;
    t_code.def("recv",
               [](code &c,id d,id e){
                 return recv(c, d, e);
               },
               python::arg("dev"),
               python::arg("expr"))
      ;

  }
  {
    auto expose_lit = [&](auto s){
      t_code.def(enumerator_name(s()).data(),
                 [=](code &m,id w){
                   return cst(m,s,w);
                 },
                 python::arg("width"));
      t_code.def(enumerator_name(s()).data(),
                 [=](code &m,domexprfam w){
                   return cst(m,s,w);
                 },
                 python::arg("width"));
    };
    each_enumerator<literal>(expose_lit);
  }
  for(auto [s, w]:codes::typetab){
    auto c = enumerator_name(s)[0];
    {
      auto n = fmt::format("{}var{}", c, w);
      t_code.def(n.c_str(),
                 [=](code &m,std::vector<id> itrs){
                   return var(m, m.builtins.types[s, w], std::move(itrs));
                 },
                 python::arg("itrs") = std::vector<id>{});
      t_code.def(n.c_str(),
                 [=](code &m,std::vector<codes::dimexprfam> itrs){
                   return var(m, m.builtins.types[s, w], std::move(itrs));
                 },
                 python::arg("itrs") = std::vector<codes::dimexprfam>{});
    }
    {
      auto n = fmt::format("{}typ{}", c, w);
      t_code.def(n.c_str(),
                 [=](code &m){
                   return m.builtins.types[s, w];
                 });
    }
    {
      auto n = fmt::format("{}cvt{}", c, w);
      t_code.def(n.c_str(),
                 [=](code &m,id o){
                   return cvt(m, m.builtins.types[s, w], o);
                 },
                 python::arg("op"));
      t_code.def(n.c_str(),
                 [=](code &m,exprfam o){
                   return cvt(m, m.builtins.types[s, w], o);
                 },
                 python::arg("op"));
    }
  }

  {
    auto expose_eval = [&](auto d){
      {
        auto n = fmt::format("{}eval",enumerator_name(d())[0]);
        t_code.def(
          n.c_str(),
          [v = constant_c<d.value>](const code &s,id o){
            return eval(s,v,o);
          },
          python::arg("op"));
      }
      {
        auto n = fmt::format("{}cceval",enumerator_name(d())[0]);
        t_code.def(
          n.c_str(),
          [v = constant_c<d.value>](code &s,id o){
            return cceval(s,v,o);
          },
          python::arg("op"));
      }
    };
    each_enumerator<set>(expose_eval);
  }


}

}//hysj::codes
#include<hysj/tools/epilogue.hpp>
