
#include"../submodules.hpp"

#include<hysj/tools/reflection.hpp>
#include<hysj/codes.hpp>
#include<hysj/codes/compile/contractions.hpp>
#include<hysj/codes/compile/implicit_iterators.hpp>
#include<hysj/codes/compile/rotations.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

void export_compile(python::module &m){
  m.def("compile_rotations",
        [](code &c, id o){
          return compile_rotations(c, o);
        },
        python::arg("code"),
        python::arg("root"));
  m.def("compile_implicit_iterators",
        [](code &c, id o){
          return compile_implicit_iterators(c, o);
        },
        python::arg("code"),
        python::arg("root"));
  m.def("compile_contractions",
        [](code &c, id o){
          return compile_contractions(c, o);
        },
        python::arg("code"),
        python::arg("root"));
}

}//hysj::codes
#include<hysj/tools/epilogue.hpp>
