#include<hysj/codes/statics.hpp>

#include<bindings.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes::statics{

void export_submodule(python::module &m_codes){
  auto m = m_codes.def_submodule("statics");
  python::make_class<types,false>(m);
  {
    auto t = python::make_class<shape>(m);
    python::reflected_repr(t);
    python::fmt_str(t);
  }
  python::make_class<shapes,false>(m);
  {
    auto t = python::make_class<domain,false>(m);
    python::reflected_repr(t);
    python::fmt_str(t);
  }
  python::make_class<domains,false>(m);
  python::make_class<all,false>(m);
}

}//hysj::codes::statics
#include<hysj/tools/epilogue.hpp>
