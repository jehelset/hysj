#include<hysj/codes/threads.hpp>

#include<bindings.hpp>
#include<tools/graphs.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

void export_threads(python::module &m){
  auto t = python::make_class<thread_info>(m);
}

}//hysj::codes
#include<hysj/tools/epilogue.hpp>
