#pragma once

#include<tuple>
#include<vector>
#include<span>

#include<hysj/codes/cells.hpp>
#include<hysj/codes/sets.hpp>
#include<hysj/tools/reflection.hpp>
#include<hysj/tools/types.hpp>

#include<bindings.hpp>
#include<devices.hpp>
#include<submodules.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

template<set S,natural W>
inline constexpr auto cell_format_descriptor =
  []{
    if constexpr(S == any_of(set::booleans, set::naturals)){
      if constexpr(W == 8)
        return "B";
      else if constexpr(W == 16)
        return "H";
      else if constexpr(W == 32)
        return "I";
      else if constexpr(W == 64)
        return "Q";
    }
    else if constexpr(S == set::integers){
      if constexpr(W == 8)
        return "b";
      else if constexpr(W == 16)
        return "h";
      else if constexpr(W == 32)
        return "i";
      else if constexpr(W == 64)
        return "q";
    }
    else{
      static_assert(S == set::reals);
      if constexpr(W == 16)
        return "e";
      else if constexpr(W == 32)
        return "f";
      else if constexpr(W == 64)
        return "d";
    }
  };

template<set S,natural W>
concept is_opaque_cell = std::is_same_v<decltype(cell_format_descriptor<S, W>()), void>;

template<set S,natural W>
auto to_memoryview(cells &M,constant_t<S> s,constant_t<W> w,id i,std::span<cell<S, W>> m){
  auto shape = ranges::vec(views::cast<ssize_t>(M.info(i).shape));
  auto strides = [&]{
    std::vector<ssize_t> strides(shape.size());
    ssize_t n = sizeof(cell<S, W>);
    for(ssize_t i = (shape.size() - 1);i >= 0;--i){
      strides[i] = n;
      n *= shape[i];
    }
    return strides;
  }();
  return python::memoryview::from_buffer(
    reinterpret_cast<void *>(m.data()),
    sizeof(cell<S, W>),
    cell_format_descriptor<S, W>(),
    python::any_container<ssize_t>(shape),
    python::any_container<ssize_t>(strides));
}

template<set S,natural W>
auto to_memoryview(cells &M,constant_t<S> s,constant_t<W> w,id i){
  if constexpr(!is_opaque_cell<S, W>)
    return to_memoryview(M, s, w, i, M.get(s, w, i));
  else
    return M.bytes(i);
}

}//hysj::codes
#include<hysj/tools/epilogue.hpp>
