#include<hysj/codes/builtins.hpp>

#include<bindings.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes::builtins{

void export_submodule(python::module &m_codes){

  auto m = m_codes.def_submodule("builtins");

  python::make_class<sets,false>(m)
    .def("__getitem__",
         [](const sets &S,set s){
           return S[s];
         },
         python::arg("set"))
    ;
  python::make_class<widths,false>(m)
    .def("__getitem__",
         [](const widths &W,natural w){
           return W[w];
         },
         python::arg("width"))
    ;
  python::make_class<types,false>(m)
    .def("__getitem__",
         [](const types &T,set s,natural w){
           return T[s, w];
         },
         python::arg("set"),
         python::arg("width"))
    ;
  python::make_class<lits,false>(m);
  python::make_class<all,false>(m);
}

}//hysj::codes::builtins
#include<hysj/tools/epilogue.hpp>
