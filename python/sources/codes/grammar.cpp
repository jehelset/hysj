#include<utility>
#include<vector>

#include<hysj/codes.hpp>
#include<hysj/grammars.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/reflection.hpp>
#include<hysj/tools/type_traits.hpp>
#include<hysj/tools/types.hpp>

#include<bindings.hpp>
#include<grammars.hpp>
#include<submodules.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

void export_grammar(python::module &m){

  python::make_enum<optag>(m);
  python::make_enum<reltag>(m);
  python::make_enum<famtag>(m);

  auto t_syntax = python::export_syntax<optag, reltag, famtag>(m);
}

}//hysj::codes
#include<hysj/tools/epilogue.hpp>
