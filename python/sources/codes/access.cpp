#include<hysj/codes/access.hpp>

#include<bindings.hpp>
#include<tools/graphs.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

void export_access(python::module &m){
  auto t = python::make_class_decl<accesses>(m);
  {
    python::export_graph_type<accesses::graph_type>(m, "Access");
  }
  python::reflect_class(t);
}

}//hysj::codes
#include<hysj/tools/epilogue.hpp>
