#include<hysj/codes/constants.hpp>

#include<bindings.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

void export_constants(python::module &m){
  python::make_enum<literal>(m);

  each_enumerator<set>(
    [&](auto set){
      auto t = python::make_class<codes::value<set.value>>(
        m,
        python::with_name_prefix{fmt::format("{}", enumerator_name(set())[0])});
      python::comparison_operators(t);
      python::reflected_repr(t);
      python::fmt_str(t);
    });

}

}//hysj::codes
#include<hysj/tools/epilogue.hpp>
