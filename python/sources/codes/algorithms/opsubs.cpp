#include"../../submodules.hpp"

#include<functional>
#include<optional>
#include<utility>
#include<vector>

#include<hysj/tools/reflection.hpp>
#include<hysj/codes.hpp>
#include<hysj/codes/algorithms/opsubs.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

void export_opsubs(python::module &m){
  m.def("total_opsub",
        [](code &p,id s,id t){
          return total_opsub(p,s,t);
        },
        python::arg("code"),
        python::arg("old"),
        python::arg("new"))
    .def("partial_opsub",
         [](code &p,id r,id s,id t){
           return partial_opsub(p,r,s,t);
         },
         python::arg("code"),
         python::arg("root"),
         python::arg("old"),
         python::arg("new")) 
    .def("opsub",
         [](code &p,
            std::vector<id> X,
            std::function<std::optional<id>(id)> F){
           return ranges::vec(opsub(p,X,F));
         },
         python::arg("code"),
         python::arg("roots"),
         python::arg("mapping")) 
    ;
}

}//hysj::codes
#include<hysj/tools/epilogue.hpp>
