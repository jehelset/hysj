#include"../../codes.hpp"
#include"../../submodules.hpp"

#include<utility>

#include<hysj/tools/reflection.hpp>
#include<hysj/codes.hpp>
#include<hysj/codes/algorithms/mulfolds.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

void export_mulfolds(python::module &m){

  m.def("mulfold",
        [](code &p,std::vector<id> o){
          return ranges::vec(mulfold(p,default_python_suborder(p),std::views::all(o)));
        },
        python::arg("code"),
        python::arg("roots"))
    ;
  m.def("mulfold",
        [](code &p,std::vector<id> o,python_suborder f){
          return ranges::vec(mulfold(p,make_suborder(f),std::views::all(o)));
        },
        python::arg("code"),
        python::arg("roots"),
        python::arg("suborder"))
    ;
  
}

}//hysj::codes
#include<hysj/tools/epilogue.hpp>
