#include"../../codes.hpp"
#include"../../submodules.hpp"

#include<utility>

#include<hysj/tools/reflection.hpp>
#include<hysj/codes.hpp>
#include<hysj/codes/algorithms/merges.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

void export_merges(python::module &m){
  m.def("merge",
        [](code &p,std::vector<id> o){
          return ranges::vec(merge(p,default_python_suborder(p),std::views::all(o)));
        },
        python::arg("code"),
        python::arg("roots"))
    ;
  m.def("merge",
        [](code &p,python_suborder f,std::vector<id> o){
          return ranges::vec(merge(p,make_suborder(f),std::views::all(o)));
        },
        python::arg("code"),
        python::arg("suborder"),
        python::arg("roots"))
    ;
}

}//hysj::codes
#include<hysj/tools/epilogue.hpp>
