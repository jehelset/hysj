#include"../../submodules.hpp"
#include"../../codes.hpp"

#include<utility>

#include<hysj/tools/reflection.hpp>
#include<hysj/codes.hpp>
#include<hysj/codes/algorithms/addfolds.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

void export_addfolds(python::module &m){
  m.def("addfold",
        [](code &p,std::vector<id> o,std::vector<id> D){
          return ranges::vec(addfold(p,std::views::all(o),D));
        },
        python::arg("code"),
        python::arg("roots"),
        python::arg("domain") = std::vector<id>{})
    ;
}

}//hysj::codes
#include<hysj/tools/epilogue.hpp>
