#include"../../codes.hpp"
#include"../../submodules.hpp"

#include<utility>

#include<hysj/tools/reflection.hpp>
#include<hysj/codes.hpp>
#include<hysj/codes/algorithms/argsorts.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

void export_argsorts(python::module &m){

  m.def("argsort",
        [](code &p,std::vector<id> o){
          return ranges::vec(
            argsort(p,default_python_suborder(p),std::views::all(o)));
        },
        python::arg("code"),
        python::arg("roots"))
    ;
  m.def("argsort",
        [](code &p,std::vector<id> o,python_suborder f){
          return ranges::vec(
            argsort(p,make_suborder(f),std::views::all(o)));
        },
        python::arg("code"),
        python::arg("roots"),
        python::arg("suborder"))
    ;

}

}//hysj::codes
#include<hysj/tools/epilogue.hpp>
