#include"../../codes.hpp"
#include"../../submodules.hpp"

#include<utility>

#include<hysj/tools/reflection.hpp>
#include<hysj/codes.hpp>
#include<hysj/codes/algorithms/normforms.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

void export_normforms(python::module &m){
  m.def("normform",
        [](code &p,std::vector<id> o,std::vector<id> X){
          return ranges::vec(normform(p,default_python_suborder(p),std::views::all(o),std::move(X)));
        },
        python::arg("code"),
        python::arg("roots"),
        python::arg("domain") = std::vector<id>{})
    ;
  m.def("normform",
        [](code &p,std::vector<id> o,std::vector<id> X,python_suborder f){
          return ranges::vec(normform(p,make_suborder(f),std::views::all(o),std::move(X)));
        },
        python::arg("code"),
        python::arg("roots"),
        python::arg("suborder"),
        python::arg("domain") = std::vector<id>{})
    ;
}

}//hysj::codes
#include<hysj/tools/epilogue.hpp>
