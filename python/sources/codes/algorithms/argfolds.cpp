#include"../../submodules.hpp"

#include<utility>

#include<hysj/tools/reflection.hpp>
#include<hysj/codes.hpp>
#include<hysj/codes/algorithms/argfolds.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

void export_argfolds(python::module &m){
  
  m.def("argfold",
        [](code &p,std::vector<id> o){
          return ranges::vec(argfold(p,std::views::all(o)));
        },
        python::arg("code"),
        python::arg("roots"))
    ;
  
}

}//hysj::codes
#include<hysj/tools/epilogue.hpp>
