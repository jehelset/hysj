#include"../../submodules.hpp"

#include<utility>

#include<hysj/tools/reflection.hpp>
#include<hysj/codes.hpp>
#include<hysj/codes/algorithms/lowerings.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

void export_lowerings(python::module &m){
  
  m.def("lower",
        [](code &p,std::vector<id> o){
          return ranges::vec(lower(p,std::views::all(o)));
        },
        python::arg("code"),
        python::arg("roots"))
    ;
  
}

}//hysj::codes
#include<hysj/tools/epilogue.hpp>
