#include"../../submodules.hpp"

#include<utility>

#include<hysj/tools/reflection.hpp>
#include<hysj/codes.hpp>
#include<hysj/codes/algorithms/dists.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

void export_dists(python::module &m){
  kumi::for_each(
    [&](auto tag){
      auto name = fmt::format("{}dist",enumerator_name(tag()));
      m.def(name.c_str(),
            [=](code &p,std::vector<id> o,std::vector<id> D){
              return ranges::vec(dist(tag,p,std::views::all(o),D));
            },
            python::arg("code"),
            python::arg("roots"),
            python::arg("domain") = dists::default_domain());
    },
    dists::distributor_tags);
}

}//hysj::codes
#include<hysj/tools/epilogue.hpp>
