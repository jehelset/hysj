
#include"../../submodules.hpp"

#include<utility>

#include<hysj/tools/reflection.hpp>
#include<hysj/codes.hpp>
#include<hysj/codes/algorithms/lexpands.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

void export_lexpands(python::module &m){
  
  m.def("lexpand",
        [](std::function<std::optional<integer>(codes::varsym)> y,code &p,std::vector<id> o){
          return ranges::vec(lexpand(y,p,std::views::all(o)));
        },
        python::arg("mapping"),
        python::arg("code"),
        python::arg("roots"))
    ;
  
}

}//hysj::codes
#include<hysj/tools/epilogue.hpp>
