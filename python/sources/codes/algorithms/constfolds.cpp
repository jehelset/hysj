#include"../../submodules.hpp"

#include<utility>

#include<hysj/tools/reflection.hpp>
#include<hysj/codes.hpp>
#include<hysj/codes/algorithms/constfolds.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

void export_constfolds(python::module &m){
  m.def("constfold",
        [](code &p,std::vector<id> o){
          return ranges::vec(constfold(p,std::views::all(o)));
        },
        python::arg("code"),
        python::arg("roots"))
    ;
}

}//hysj::codes
#include<hysj/tools/epilogue.hpp>
