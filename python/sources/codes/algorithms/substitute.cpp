#include"../../submodules.hpp"

#include<functional>
#include<optional>
#include<utility>
#include<vector>

#include<hysj/tools/reflection.hpp>
#include<hysj/codes.hpp>
#include<hysj/codes/algorithms/substitute.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

void export_substitute(python::module &m){
  python::make_class<substitute, false, false>(m)
    .def(
      python::init([](code &c, std::vector<kumi::tuple<id, id>> subs){
        return substitute{c, std::move(subs)};
      }),
      python::arg("code"),
      python::arg("subs"))
    .def("__call__", &substitute::operator())

    ;
}

}//hysj::codes
#include<hysj/tools/epilogue.hpp>
