#include"../../submodules.hpp"

#include<functional>
#include<utility>
#include<ranges>

#include<hysj/tools/reflection.hpp>
#include<hysj/codes.hpp>
#include<hysj/codes/algorithms/copy.hpp>
#include<hysj/tools/ranges/utility.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

void export_copy(python::module &m){

  m
    .def("copy",
        [](code &p,std::vector<id> o){
          return ranges::vec(copy(p,std::views::all(o)));
        },
        python::arg("code"),
        python::arg("roots"))
    .def("copy_if",
         [](code &p,std::vector<id> o,std::function<bool(id)> f){
           return ranges::vec(copy_if(p,std::views::all(o),overload(f, always(true))));
         },
         python::arg("code"),
         python::arg("roots"),
         python::arg("predicate"))
    .def("clone",
        [](code &p,std::vector<id> o){
          return ranges::vec(clone(p,std::views::all(o)));
        },
        python::arg("code"),
        python::arg("roots"))
    ;
  
}

}//hysj::codes
#include<hysj/tools/epilogue.hpp>
