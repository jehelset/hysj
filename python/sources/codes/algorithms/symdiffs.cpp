#include"../../submodules.hpp"
#include"../../codes.hpp"

#include<utility>

#include<hysj/tools/reflection.hpp>
#include<hysj/codes.hpp>
#include<hysj/codes/algorithms/symdiffs.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

void export_symdiffs(python::module &m){
  m.def("symdiff",
        &symdiff,
        python::arg("code"),
        python::arg("symbol"))
    ;

}

}//hysj::codes
#include<hysj/tools/epilogue.hpp>
