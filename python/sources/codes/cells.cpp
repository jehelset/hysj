#include<tuple>

#include<hysj/codes/cells.hpp>
#include<hysj/tools/reflection.hpp>
#include<hysj/tools/types.hpp>

#include<bindings.hpp>
#include<devices.hpp>
#include<submodules.hpp>
#include<codes/cells.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{


void export_cells(python::module &m){

  python::reflected_repr(python::fmt_str(python::make_class<cellinfo>(m)));

  python::make_class<cellinfos, false>(m)
    .def(python::init(&cellinfos::make),
         python::arg("code"),
         python::arg("accesses"),
         python::arg("dev"),
         python::arg("alignment") = no<std::size_t>)
    .def("contains",
         &cellinfos::contains,
         python::arg("id"))
    .def("size",
         &cellinfos::size,
         python::arg("type"))
    .def("empty",
         &cellinfos::empty,
         python::arg("type"))
    .def("__getitem__",
         &cellinfos::operator[],
         python::arg("id"))
    ;
 
  auto t_cells = python::make_class<cells, false>(m);

  for(auto t:typetab)
    with_type(
      [&](auto s,auto w){
        python::make_class<cellvec<s(), w()>, false>(
          m, python::with_name_wrap{
            fmt::format("{}", enumerator_name(s())[0]),
            fmt::format("{}", w())
          });
        {
          auto s_name = enumerator_name(s())[0];
          auto n = fmt::format("{}{}", s_name, w());
          t_cells.def(n.c_str(),
                      [s,w](cells &c,id i){
                        return to_memoryview(c, s, w, i);
                      },
                      python::arg("op"));
        }
      },
      t);

  m.def("alloc",
        &alloc,
        python::arg("code"),
        python::arg("api"),
        python::arg("alignment") = std::optional<std::size_t>{});
        
}

}//hysj::codes
#include<hysj/tools/epilogue.hpp>
