#include<utility>
#include<vector>

#include<hysj/codes.hpp>
#include<hysj/grammars.hpp>
#include<hysj/tools/ranges/utility.hpp>
#include<hysj/tools/reflection.hpp>
#include<hysj/tools/type_traits.hpp>
#include<hysj/tools/types.hpp>

#include<bindings.hpp>
#include<grammars.hpp>
#include<submodules.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::codes{

void export_vars(python::module &m){

  python::make_enum<depvartag>(m);
  python::fmt_str(python::make_class<vars>(m));

  m.def(
    "depvar",
    [](code &c,codes::varsym independent,natural dep_order,codes::varsym dependent){
      return depvar(c,{independent},dep_order,{dependent});
    },
    python::arg("code"),
    python::arg("independent_variable"),
    python::arg("order"),
    python::arg("dependent_variable"))
    ;

}

}//hysj::codes
#include<hysj/tools/epilogue.hpp>
