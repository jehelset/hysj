#include<algorithm>
#include<cstdint>
#include<ranges>
#include<span>
#include<stdexcept>
#include<vector>

#include<cgif.h>

#include<hysj/tools/ranges/utility.hpp>
#include<hysj/codes.hpp>
#include<hysj/grammars.hpp>

#include<bindings.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::animations{

using cgif_ptr = std::unique_ptr<CGIF, decltype(&cgif_close)>;

auto make_cgif_ptr(std::uint16_t width, std::uint16_t height, std::span<std::uint8_t> palette, std::string_view path){
  if(palette.size() % 3)
    throw std::runtime_error("invalid palette size");
  CGIF_Config config{
    .pGlobalPalette = palette.data(),
    .path = path.data(),
    .attrFlags = CGIF_ATTR_IS_ANIMATED,
    .genFlags{},
    .width = width,
    .height = height,
    .numGlobalPaletteEntries = static_cast<std::uint16_t>(palette.size() / 3)
  };
  return cgif_ptr(cgif_newgif(&config), &cgif_close);
}

struct gif_writer{
  
  std::uint16_t width, height;
  std::span<std::uint8_t> palette;
  std::string path;
  cgif_ptr cgif{nullptr, &cgif_close};

  friend HYSJ_MIRROR_STRUCT(gif_writer, (width, height, palette, path));

  auto enter(){
    cgif = make_cgif_ptr(width, height, palette, path);
  }
  auto operator()(std::span<std::uint8_t> frames, std::uint16_t delay){
    HYSJ_ASSERT((frames.size() % (width * height)) == 0);
    auto num_frames = frames.size() / (width * height);
    CGIF_FrameConfig frame_config{
      .pImageData = nullptr,
      .delay = delay
    };
    for(auto t:views::indices(num_frames)){
      frame_config.pImageData = frames.data() + t * (width * height);
      cgif_addframe(cgif.get(), &frame_config);
    }
  }
  auto exit(){
    HYSJ_ASSERT(cgif.get() != nullptr); //FIXME: miscompile without .get() - jeh
    cgif.reset();
  }
};

struct mem_gif_writer : gif_writer{
  cells &mem;
  id palette, buffer;
  friend HYSJ_MIRROR_STRUCT(mem_gif_writer, (palette, buffer)(gif_writer));

  auto operator()(std::size_t offset,std::optional<std::size_t> count, std::uint16_t delay){
    auto stride = width * height;

    count = count.transform([=](auto c){ return c * stride; });
    gif_writer::operator()(mem.n8(buffer).subspan(offset * stride, count.value_or(std::dynamic_extent)), delay);
  }

  static mem_gif_writer make(cells &mem,id palette,id buffer,std::string path){
    auto palette_mem = mem.n8(palette);
    const auto &palette_shape = mem.info(palette).shape;
    if(std::ranges::size(palette_shape) != 1
       || !palette_shape[0]
       || (palette_shape[0] % 3))
      throw std::runtime_error("invalid palette");

    const auto &buffer_shape = mem.info(buffer).shape;
    if(std::ranges::size(buffer_shape) != 3
       || std::ranges::any_of(buffer_shape, bind<>(std::ranges::equal_to{}, 0)))
      throw std::runtime_error("invalid buffer");

    auto palette_size = palette_shape[0] / 3;
    auto height = buffer_shape[1], width = buffer_shape[2];

    return {
      gif_writer{
        static_cast<std::uint16_t>(width),
        static_cast<std::uint16_t>(height),
        palette_mem,
        std::move(path)
      },
      mem,
      palette,
      buffer
    };
  }

};

void export_submodule(python::module &m_hysj){
  auto m = m_hysj.def_submodule("_animations");
  m.def("write_gif",
        +[](cells &mem,
            id palette,
            id frames,
            std::string path,
            std::uint16_t delay){
          auto writer = mem_gif_writer::make(mem, palette, frames, path);
          writer.enter();
          writer(0, {}, delay);
          writer.exit();
        },
        python::arg("mem"),
        python::arg("palette"), python::arg("frames"),
        python::arg("path"), python::arg("delay") = 5);

  python::make_class<gif_writer>(m)
    .def(
      "__enter__",
      [](gif_writer &g){
        g.enter();
        return &g;
      })
    .def("__call__",
         [](gif_writer &g, std::span<std::uint8_t> frames, std::uint16_t delay){
           g(frames, delay);
         },
         python::arg("frames"),
         python::arg("delay") = 8)
    .def(
      "__exit__",
      [](gif_writer &g,
         const std::optional<python::type> &,
         const std::optional<python::object> &,
         const std::optional<python::object> &){
        g.exit();
      },
      python::arg("exc_type"),
      python::arg("exc_value"),
      python::arg("traceback"))
      ;
    
  python::make_class_decl<mem_gif_writer>(m)
      .def(python::init(&mem_gif_writer::make),
           python::arg("mem"),
           python::arg("palette"),
           python::arg("buffer"),
           python::arg("path"))
    .def(
      "__enter__",
      [](mem_gif_writer &g){
        g.enter();
        return &g;
      })
    .def("__call__",
         [](mem_gif_writer &g, std::size_t offset, std::size_t count, std::uint16_t delay){
           g(offset, count, delay);
         },
         python::arg("offset") = std::size_t{0},
         python::arg("count") = no<std::size_t>,
         python::arg("delay") = std::uint16_t{8})
      ;
    
}

} //hysj::animations
#include<hysj/tools/epilogue.hpp>
