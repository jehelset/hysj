#include<submodules.hpp>
#include"../graphs.hpp"

#include<fmt/format.h>

#include<bindings.hpp>
#include<hysj/tools/graphs/partitions.hpp>
#include<hysj/tools/reflection.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::graphs::partitions{

template<typename T,typename... O>
auto export_partitions(python::class_<T,O...> t){
  t.def("enumerate",
        [](const T &t){
          return ranges::vec(prt::enumerate(t));
        })
   .def("members",
         [](const T &t,prt::partition_t<T> i){
           return prt::members(t,i);
         },
         python::arg("partition"))
    ;
  return t;
}

void export_submodule(python::module &m_graphs){
  auto m = m_graphs.def_submodule("partitions");
  python::copy_semantics(
    python::reflected_repr(
      export_partitions(python::make_class<partitioning,false>(m))));
}

} //hysj::graphs::partition_refinement
#include<hysj/tools/epilogue.hpp>
