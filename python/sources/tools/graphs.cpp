#include"submodules.hpp"
#include"graphs.hpp"

#include<bindings.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::graphs{

void export_graphs(auto &m){
  python::make_enum<direction_tag>(m);
  python::make_enum<undirected_monotag>(m);
  python::make_enum<element_tag>(m);
  auto export_element_id =
    [&]<typename T,typename... O>(python::class_<T,O...> t){
      python::implicitly_convertible<natural,T>();
      python::hash(t);
      python::comparison_operators(t);
      python::reflected_repr(t);
      python::fmt_str(t);
      if constexpr(T::tag == edge_tag)
        t.def("undirect",[](const T &t){ return undirect(t); });
      return t;
    };
  
  each_enumerator<element_tag>(
    [&](auto e){
      using T = element_id<e>;
      auto t =
        export_element_id(
          python::make_class<T>(
            m,
            python::pep8_typename(fmt::format("{}_id",enumerator_name(e())))));
    });
  
  export_element_id(python::make_class<dynamic_edge_id>(m));
  python::implicitly_convertible<dynamic_edge_id,edge_id>();

  each_enumerator<direction_tag>(
    [&](auto d){
      using T = static_edge_id<d>;
      export_element_id(
        python::make_class<T>(m,python::pep8_typename(fmt::format("{}_edge_id",enumerator_name(d())))))
        .attr("direction") = d;
      python::implicitly_convertible<T,edge_id>();
    });

  python::export_graph_type<incidence_graph<vertex_id,edge_id>>(m,python::pep8_typename("id"));

}

void export_submodule(python::module &m_hysj){
  auto m = m_hysj.def_submodule("graphs");
  export_graphs(m);
}

}//hysj::graphs
#include<hysj/tools/epilogue.hpp>
