#pragma once
#include<functional>
#include<ranges>
#include<string_view>
#include<type_traits>
#include<vector>

#include<fmt/format.h>

#include<bindings.hpp>

#include<hysj/tools/graphs/adjacency.hpp>
#include<hysj/tools/graphs/degree.hpp>
#include<hysj/tools/graphs/directions.hpp>
#include<hysj/tools/graphs/elements.hpp>
#include<hysj/tools/graphs/elements.hpp>
#include<hysj/tools/graphs/incidence.hpp>
#include<hysj/tools/graphs/incidence_graphs.hpp>
#include<hysj/tools/reflection.hpp>
#include<hysj/tools/functional/apply.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::python{

constexpr std::string_view pep8_attrname(graphs::direction_tag d){
  return d == graphs::in_tag ? "in_" : "out" ;
}
constexpr std::string_view pep8_attrname_plural(graphs::element_tag c){
  return c == graphs::vertex_tag ? "vertices" : "edges" ;
}
constexpr std::string_view pep8_prefix(graphs::direction_tag d){
  return d == graphs::in_tag ? "in" : "out";
}

template<>
struct make_enumerator_names_fn<graphs::direction_tag>{
  auto operator()()const{
    static constexpr auto names = apply(
      [](auto... d){
        return std::array{pep8_attrname(d)...};
      },
      enumerator_values<graphs::direction_tag>);
    return names;
  }
};

std::string export_skin_name(auto d){
  if(is_same_constant(d,graphs::undirected_tag))
    return "skin";
  else
    return fmt::format("{}_skin",enumerator_name(d()));
}

template<class T,class... O>
auto export_skin(class_<T,O...> c){
  auto export_fn = [&]<typename D>(D d){
    if constexpr(
      graphs::concepts::directional_incidence_graph<T,D>
      &&(requires(const T &v){
          {graphs::skin(d,v)};
        })){
      auto f_name = export_skin_name(d);
      c.def(f_name.c_str(),
            [&,d](const T& v){
              return ranges::vec(graphs::skin(d,v));
            });
    }
  };
  each_enumerator<graphs::direction_tag>(export_fn);
  each_enumerator<graphs::undirected_monotag>(export_fn);
  return c;
}


std::string export_is_skin_name(auto d){
  if(is_same_constant(d,graphs::undirected_tag))
    return "is_skin";
  else
    return fmt::format("is_{}_skin",enumerator_name(d()));
}

template<class I,class T,class... O>
auto export_is_skin(class_<T,O...> c){
  auto export_fn = [&]<typename D>(D d){
    if constexpr(
      graphs::concepts::directional_incidence_graph<T,D>
      &&(requires(const T &v,I i){
          {graphs::is_skin(d,v,i)};
        })){
      auto f_name = export_is_skin_name(d);
      c.def(f_name.c_str(),
            [&,d](const T& v,I i){
              return graphs::is_skin(d,v,i);
            });
    }
  };
  each_enumerator<graphs::direction_tag>(export_fn);
  each_enumerator<graphs::undirected_monotag>(export_fn);
  return c;
}

std::string export_incident_name(auto t,auto d){
  if constexpr(is_same_constant(d,graphs::undirected_tag))
    return fmt::format("{}_incident",enumerator_name(t()));
  return fmt::format("{}_{}_incident",enumerator_name(t()),enumerator_name(d()));
}
  
template<class I,graphs::element_tag T,class G,class... O>
auto export_incident(constant_t<T> t,class_<G,O...> c){

  auto export_fn = [&,t](auto d){
    if constexpr(requires(const G &g,I i){ graphs::incident(t,d,g,i); }){
      auto f_name = export_incident_name(t,d);
      c.def(
        f_name.c_str(),
        [t,d](const G &g,const I &i){
          return ranges::vec(graphs::incident(t,d,g,i));
        },
        arg("id"));
    }
  };
  each_enumerator<graphs::direction_tag>(export_fn);
  each_enumerator<graphs::undirected_monotag>(export_fn);

  if constexpr(T == graphs::edge_tag){
    if constexpr(requires(const G &g,I i){ graphs::head(g,i); })
      c.def("head",
            [](const G &g,const I &i){
              return graphs::head(g,i);
            },
            arg("id"));
    if constexpr(requires(const G &g,I i){ graphs::tail(g,i); })
      c.def("tail",
            [](const G &g,const I &i){
              return graphs::tail(g,i);
            },
            arg("id"));
    if constexpr(requires(const G &g,I i){ graphs::ports(g,i); })
      c.def("ports",
            [](const G &g,const I &i){
              return graphs::ports(g,i);
            },
            arg("id"));
  }
  return c;
}

std::string export_degree_name(auto d){
  if constexpr(is_same_constant(d,graphs::undirected_tag))
    return fmt::format("degree");
  else
    return fmt::format("{}_degree",enumerator_name(d()));
}
 
template<class I,class G,class... O>
auto export_degree(class_<G,O...> c){
  auto export_fn = [&](auto d){
    if constexpr(requires(const G &g,I i){ { graphs::degree(d,g,i) }; }){
      auto f_name = export_degree_name(d);
      c.def(
        f_name.c_str(),
        [d](const G &g,const I &i) -> natural{
          return graphs::degree(d,g,i);
        },
        arg("id"));
    }
  };
  each_enumerator<graphs::direction_tag>(export_fn);
  each_enumerator<graphs::undirected_monotag>(export_fn);
  return c;
}

std::string export_adjacent_name(auto d){
  if constexpr(is_same_constant(d,graphs::undirected_tag))
    return fmt::format("adjacent");
  else
    return fmt::format("{}_adjacent",enumerator_name(d()));
}

template<class I,graphs::element_tag T,class G,class... O>
auto export_adjacent(constant_t<T> t,class_<G,O...> c){
  auto export_fn = [&,t](auto d){
    if constexpr(requires(const G &g,I i){ graphs::adjacent(t,d,g,i); }){
      auto f_name = export_adjacent_name(d);
      c.def(
        f_name.c_str(),
        [t,d](const G& g,const I &i){
          return ranges::vec(graphs::adjacent(t,d,g,i));
        },
        arg("id"));
    }
  };
  each_enumerator<graphs::direction_tag>(export_fn);
  each_enumerator<graphs::undirected_monotag>(export_fn);
  return c;
}

template<class... A,class T,class... O>
auto element_container(class_<T,O...> c){
  c
    .def("__call__",[](const T &v){ return ranges::vec(v()); })
    ;
  return c;
}

inline constexpr struct no_prefix_t{} no_prefix{};

template<class G>
struct export_graph_type_fn;

template<class V,class E>
struct export_graph_type_fn<graphs::incidence_graph<V,E>>{
  using graph_ = graphs::incidence_graph<V,E>;
  auto operator()(auto scope,const std::string &prefix,auto... options)const{

    auto graph_typename = prefix + pep8_typename(reflect<graph_>().name);
    if(auto graph_type = make_typeid<graph_>())
      throw std::runtime_error(fmt::format("graph type \"{}\" already registered",graph_typename));
    auto graph_type = make_class_decl<graph_>(scope,graph_typename,options...);

    graph_type.def(
      init(
        [](natural order,
           natural size){
          return graphs::incidence_graph<V,E>::make(order,size);
        }),
      arg("order"),
      arg("size") = 0ull);

    each_enumerator<graphs::element_tag>(
      [&](auto e){
        using element_id = graphs::element_id<e.value>;
        
        using element  = graph_::template element<e.value>;
        using elements = graph_::template elements<e.value>;
        using value_ = typename element::value_type;

        auto element_typename = pep8_typename(fmt::format("{}_",enumerator_name(e())));
        if(auto element_typeid = make_typeid<element>())
          make_alias(graph_type,element_typename,element_typeid);
        else{
          auto element_type = make_class<element>(graph_type,element_typename,options...);
          reflected_members(element_type);
          reflected_repr(element_type);
        }
        
        auto elements_typename = pep8_typename(fmt::format("{}_",pep8_attrname_plural(e.value)));
        if(auto elements_typeid = make_typeid<elements>())
          make_alias(graph_type,elements_typename,elements_typeid);
        else{
          auto elements_type = make_class<elements>(graph_type,elements_typename,options...);
          element_container(elements_type);
          reflected_members(elements_type);
          reflected_repr(elements_type);
        }
        export_incident<element_id>(constant_c<e.value>,graph_type);
        if constexpr(e.value == graphs::vertex_tag)
          export_is_skin<element_id>(graph_type);
        if constexpr(e.value == graphs::edge_tag){
          export_incident<graphs::dynamic_edge_id>(
            constant_c<e.value>,graph_type);
          each_enumerator<graphs::direction_tag>(
            [&](auto d){
              export_incident<graphs::static_edge_id<d()>>(constant_c<e.value>,graph_type);
            });
        }
        if constexpr(e.value == graphs::vertex_tag){
          export_degree<element_id>(graph_type);
          export_adjacent<element_id>(constant_c<e.value>,graph_type);
        }
        graph_type
          .def(
            "__call__",
            [](graph_ &g,element_id k){
              return std::ref(g[k]);
            })
          .def(
            "__getitem__",
            [](graph_ &g,element_id k)->auto &{
              return g[k];
            },
            return_value_policy::reference_internal)
          .def(
            "__setitem__",
            [](graph_ &g,element_id k,const value_ &v){
              g[k] = v;
            })
          ;
      });

    reflected_members(graph_type);
    reflected_repr(graph_type);

    export_skin(graph_type);

    graph_type
      .def("vertex",[](graph_ &g,V v){ return g.vertex(std::move(v)); })
      .def("order",[](const graph_ &g){ return graphs::order(g); },"Number of vertices.")
      .def("size",[](const graph_ &g){ return graphs::size(g); },"Number of edges.")
      ;

    graph_type.def(
      "edge",
      [=](graph_ &g,graphs::vertex_id v_in,graphs::vertex_id v_out,E e){
        return g.edge(v_in,v_out,std::move(e));
      },
      arg("head"),
      arg("tail"),
      arg("edge") = reflected_member_default<E>());

    //NOTE: static? - jeh
    graph_type.def(
      "other_direction",
      [](const graph_ &,graphs::direction_tag d){
        return graphs::other(d);
      },
      arg("direction"),
      "Compute other direction");

    return graph_type;
  }
  
  auto operator()(auto scope,no_prefix_t,auto... options)const{
    return operator()(scope,std::string{},options...);
  }
};

template<class G>
constexpr export_graph_type_fn<G> export_graph_type{};

} //hysj::python
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::graphs{

inline std::string_view direction_attr_prefix(undirected_tag_type){
  return "undirected";
}
inline std::string_view direction_attr_prefix(auto direction){
  static const auto prefix = fmt::format("{}_directed",enumerator_name(direction()));
  return prefix;
}

inline std::string_view direction_desc(undirected_tag_type){
  return "undirected";
}
inline std::string_view direction_desc(auto direction){
  static const auto desc = fmt::format("{} directed",enumerator_name(direction()));
  return desc;
}

void export_components(python::module &);
namespace partition_refinement{
  void export_submodule(python::module &);
}

} //hysj::graphs
#include<hysj/tools/epilogue.hpp>
