#pragma once

#include<array>
#include<vector>
#include<tuple>

#include<fmt/format.h>

#include<hysj/grammars.hpp>
#include<hysj/tools/reflection.hpp>
#include<hysj/tools/functional/pick.hpp>
#include<hysj/tools/trees.hpp>
#include<hysj/tools/ranges/utility.hpp>

#include<bindings.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::python{

template<auto grammar>
auto pytree(auto &&subtree){
  return trees::with<grammar>(
    subtree,
    [](auto, auto tag, auto &&... subtrees){
      using enum trees::rule;
      if constexpr(tag() == one)
        return pick<0>(fwd(subtrees)...);
      if constexpr(tag() == tup)
        return std::make_tuple(fwd(subtrees)...);
      if constexpr(tag() == var)
        return ranges::vec(pick<0>(fwd(subtrees)...));
    });
}

}
#include<hysj/tools/epilogue.hpp>
