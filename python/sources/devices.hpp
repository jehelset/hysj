#pragma once

#include<hysj/codes/grammar.hpp>
#include<hysj/codes/cells.hpp>
#include<hysj/devices/device.hpp>

#include<bindings.hpp>

#include<hysj/tools/prologue.hpp>
namespace hysj::inline _HYSJ_VERSION_NAMESPACE::python{

template<typename E, typename... OE>
auto make_devexe(python::class_<E, OE...> t_exe){
  t_exe.def("run",
            [](E &e,codes::taskfam m){
              return e.run(m);
            },
            python::arg("main"))
    ;
}

template<typename E, typename... OE,typename... OD>
auto make_devenv(python::class_<E, OE...> t_env){
  t_env.def("alloc",
            [](E &e,code &c,codes::apisym a){
              return devices::alloc(e, c, a);
            },
            python::arg("code"),
            python::arg("api"),
            python::keep_alive<0, 1>());
  t_env.def("load",
            [](E &e,cells &m, code c,codes::apisym a){
              return e.load(m, std::move(c), a);
            },
            python::arg("memory"),
            python::arg("code"),
            python::arg("api"),
            python::keep_alive<0, 1>(),
            python::keep_alive<0, 2>());
  return t_env;
}

}
#include<hysj/tools/epilogue.hpp>
